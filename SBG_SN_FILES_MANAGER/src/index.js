import path from 'path';
import { server as _server } from 'hapi';
import inert from 'inert';
import browser from './file_manager/browser';
import config from './file_manager/manager/config';

const server = new _server({
    host: 'localhost',
    port: 3000,
    routes: {
        files: {
            /* eslint-disable */
            relativeTo: path.join(__dirname, 'public/browser')
        },
        cors: {
            origin: ['*'],
            additionalHeaders: ['Authorization', 'cache-control', 'x-requested-with'],
            credentials: true
        }
    }
});


const start = async () => {

    await server.register(inert);

    server.route({
        method: 'GET',
        path: '/undefined',
        handler: (request, h) => {
            return h.response('undefined');
        }
    });

    server.route({
        method: 'GET',
        path: '/{param*}',
        handler: {
            directory: {
                path: '.',
                redirectToSlash: true,
                index: true,
            }
        }
    });

    server.route({
        method: 'GET',
        path: '/',
        handler: (request, h) => {
            return h.redirect('/browser.html?Connector=/browse&langCode=vi');
        }
    });

    server.route({
        method: 'GET',
        path: '/browse/{param*}',
        handler: function (request, h) {
            return browser.browse(request, h);
        }
    });

    server.route({
        method: 'POST',
        path: '/browse/{param*}',
        config: {
            payload: {
                output: 'stream',
                allow: 'multipart/form-data',
                parse: true,
                maxBytes: parseInt(config.maxSize)
            }
        },
        handler: function (request, h) {
            return browser.browse(request, h);
        }
    });

    await server.start();
    /* eslint-disable */
    console.log('Server running at:', server.info.uri);
};

start();
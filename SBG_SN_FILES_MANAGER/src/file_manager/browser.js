import xml from './manager/xml_preparing';
import DiskFileManager from './manager/disk_file_manager';
import config from './manager/config';

var request = require('sync-request');

const browserCommand = new DiskFileManager();

const browser = new function () {

  this.browse = function (req, res) {
    var command = req.query.Command;
    var currentFolder = req.query.CurrentFolder;
    var originFolder = req.query.CurrentFolder;
    var newPath = req.query.NewPath;
    var oldPath = req.query.OldPath;
    var acceptFile = req.query.Accept;
    if (!command || !currentFolder) {
      return;
    }

    var token = req.headers.authorization;
    if (!token) {
      return res.response('Not access').code(403);
    }
    
    if (token !== undefined && token !== 'undefined' && token !== null && token !== 'null' && token !== '') {
      var response = request('GET', config.url_check_token, {
        headers: {
          'authorization': 'bearer ' + token,
          'Content-Type': 'application/json'
        },
      });
      if (response.statusCode === 200 && response.body !== null && response.body !== '') {
        var body = JSON.parse(response.body);
        var userName = body.data.userName;
        newPath = userName + newPath;
        oldPath = userName + oldPath;
        var foldersAndFiles = {};
          switch (command) {
            case 'FileUpload':
              // check accept format upload
              if (acceptFile === undefined || acceptFile === null || acceptFile === 'undefined' || acceptFile === 'null' || acceptFile === '') {
                acceptFile = 'files';
              }
              if (config.hasOwnProperty(acceptFile)) {
                var formatFile = config[acceptFile];  
                const data = req.payload;
                if (data.NewFile.length) {
                  var check = false;
                  data.NewFile.forEach(element => {
                    var tail = element.hapi.headers['content-type'];
                    if(!formatFile.includes(tail)) {
                      check = true;
                    }
                  });
                  if(check) {
                    return res.response(xml.renderResult(1, 'format not valid.')).code(403);
                  }
                } else {
                  var tail = data.NewFile.hapi.headers['content-type'];
                  if(!formatFile.includes(tail)) {
                    return res.response(xml.renderResult(1, 'format not valid.')).code(403);
                  }
                }
              }
              else {
                return res.response(xml.renderResult(1, 'format not valid.')).code(403);
              }

              currentFolder = userName + currentFolder;
              if (browserCommand.fileUpload(req, currentFolder)) {
                return res.response(xml.renderResult(1, 'Upload successfully.')).code(200);
              } else {
                return res.response(xml.renderResult(1, 'Upload successfully.')).code(501);
              }
            case 'CreateFolder':
              currentFolder = userName + currentFolder;
              var newFolderName = req.query.NewFolderName;
              if (browserCommand.createFolder(currentFolder, newFolderName)) {
                return res.response('Create folder successfull').code(200);
              } else {
                return res.response('Create folder failed').code(400);
              }
              
            case 'GetFolders':
              currentFolder = userName + currentFolder;
              foldersAndFiles = browserCommand.getFolders(currentFolder);
              break;
            case 'RenameFile':
              if (!browserCommand.RenameFile(currentFolder, oldPath, newPath, userName)) {
                return res.response('Rename not valid !').code(400);
              }
              return res.response('Rename successfully.').code(200);
            case 'RenameFolder':
              currentFolder = userName + currentFolder;
              if (!browserCommand.RenameFolder(currentFolder, oldPath, newPath, userName)) {
                return res.response('Rename not valid !').code(400);
              }
              else {
                return res.response('Rename successfully.').code(200);
              }
            case 'DeleteFile':
              foldersAndFiles = browserCommand.DeleteFile(currentFolder);
              break;
            case 'DeleteFolder':
              currentFolder = userName + currentFolder;
              foldersAndFiles = browserCommand.DeleteFolder(currentFolder);
              break;
            case 'GetFoldersAndFiles':
              currentFolder = userName + currentFolder;
              foldersAndFiles = browserCommand.getFoldersAndFiles(currentFolder, res);
              if (foldersAndFiles === undefined || foldersAndFiles === null)
                return res.response(xml.renderError(1, 'Not have folder')).code(401);
              break;
            }
            return res.response(xml.renderXml(command, originFolder, foldersAndFiles, userName, config.url_public)).code(200);
          } else {
            return res.response('Not access').code(403);
          }
      } else {
        return res.response('Not access').code(403);
      }
  };
};

export default browser;

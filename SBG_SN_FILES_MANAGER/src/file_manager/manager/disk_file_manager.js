import xml from './xml_preparing';
import util from './utils';
import fs from 'fs-extra';
import FileManager from './base_interface';

class DiskFileManager {
  /* eslint-disable */
  constructor(options) { }

  DeleteFile($currentFolder) {
    // Map the virtual path to the local server path.
    var serverDir = util.serverMapFolder($currentFolder);
    var result = util.DeleteFile(serverDir);
    var xmlFoldersObj = xml.deleteObject(result);
    return [xmlFoldersObj];
  }

  DeleteFolder($currentFolder) {
    // Map the virtual path to the local server path.
    var serverDir = util.serverMapFolder($currentFolder);
    var result = util.DeleteFolder(serverDir);
    var xmlFoldersObj = xml.deleteObject(result);
    return [xmlFoldersObj];
  }

  RenameFolder($currentFolder, $folderOld, $folderNew, $userName) {
    // Map the virtual path to the local server path.
    var checkNameValid = this.CheckEditNameFolder($folderNew, $currentFolder, $userName);
    if (checkNameValid) {
      var serverDirOld = util.serverMapFolder($folderOld);
      var serverDirNew = util.serverMapFolder($folderNew);
      var result = util.RenameFolder(serverDirOld, serverDirNew);
      return result;
    }
    return false;
  }

  CheckEditNameFolder(nameFolder, currentFolder, userName) {
    try {
      var serverDir = util.serverMapFolder(currentFolder);
      var foldersAndFiles = util.getFilesAndFolders(serverDir);
      if (foldersAndFiles.folders) {
        for (let i = 0; i < foldersAndFiles.folders.length; i++) {
          if ((userName + '/' + foldersAndFiles.folders[i].name + '/').localeCompare(nameFolder) === 0) {
            return false;
          }
        }
      }
      return true;
    }
    catch (exception) {
      return false;
    }
  }

  CheckEditNameFile(nameFile, currentFolder, userName) {
    try {
      var serverDir = util.serverMapFolder(userName + currentFolder + '/');
      var foldersAndFiles = util.getFilesAndFolders(serverDir);
      if (foldersAndFiles.files) {
        for (let i = 0; i < foldersAndFiles.files.length; i++) {
          if ((userName + '/' + foldersAndFiles.files[i].name.localeCompare(nameFile)) === 0) {
            return false;
          }
        }
      }
      return true;
    }
    catch (exception) {
      return false;
    }
  }

  RenameFile($currentFolder, $fileOld, $fileNew, $userName) {
    // Map the virtual path to the local server path.
    var checkValidName = this.CheckEditNameFile($fileNew, $currentFolder, $userName);
    if (checkValidName) {
      var serverDirOld = util.serverMapFolder($fileOld);
      var serverDirNew = util.serverMapFolder($fileNew);
      var result = util.RenameFile(serverDirOld, serverDirNew);
      return result;
    }
    return false;
  }

  getFolders($currentFolder) {
    // Map the virtual path to the local server path.
    var serverDir = util.serverMapFolder($currentFolder);
    var foldersAndFiles = util.getFilesAndFolders(serverDir);
    var xmlFoldersObj = xml.prepareFoldersObject(foldersAndFiles.folders);
    return [xmlFoldersObj];
  }

  getFoldersAndFiles(currentFolder) {
    // Map the virtual path to the local server path.
    try {
      var serverDir = util.serverMapFolder(currentFolder);
      var foldersAndFiles = util.getFilesAndFolders(serverDir);
      var xmlFoldersObj = xml.prepareFoldersObject(foldersAndFiles.folders);
      var xmlFilesObj = xml.prepareFilesObject(foldersAndFiles.files);
      return [xmlFoldersObj, xmlFilesObj];
    }
    catch (err) {
      return null;
    }
  }

  createFolder(currentFolder, newFolderName) {
    var errorNumber = '0';
    var errorMsg = '';
    var result = false;
   
      try {
         if (newFolderName) {
          newFolderName = util.sanitizeFolderName(newFolderName);
          var serverDir = util.serverMapFolder(currentFolder);
          newFolderName = this.checkNameFolderValid(newFolderName, serverDir);
          result = util.createDirIfNotExists('', serverDir + newFolderName);
          return result;
         }
         return false;
      }
      catch (err) {
        return false;
      }
  }

  checkNameFileValid(nameFile, serverDir) {
    var newName = nameFile;
    try {
      var foldersAndFiles = util.getFilesAndFolders(serverDir);
      if (foldersAndFiles.files) {
        var number = 0;
        for (let i = 0; i < foldersAndFiles.files.length; i++) {
          if (foldersAndFiles.files[i].name.localeCompare(newName) === 0) {
            number++;
            newName = '(' + number + ') ' + nameFile;
          }
          for (let j = 0; j < foldersAndFiles.files.length; j++) {
            if (foldersAndFiles.files[j].name.localeCompare(newName) === 0) {
              number++;
              newName = '(' + number + ') ' + nameFile;
            }
          }
        }
      }
      return newName;
    }
    catch (exception) {
      return newName;
    }
  }

  checkNameFolderValid(nameFolder, serverDir) {
    var newName = nameFolder;
    try {
      var foldersAndFiles = util.getFilesAndFolders(serverDir);
      if (foldersAndFiles.folders) {
        var number = 0;
        for (let i = 0; i < foldersAndFiles.folders.length; i++) {
          if (foldersAndFiles.folders[i].name.localeCompare(newName) === 0) {
            number++;
            newName = nameFolder + ' (' + number + ')';
          }
          for (let j = 0; j < foldersAndFiles.folders.length; j++) {
            if (foldersAndFiles.folders[j].name.localeCompare(newName) === 0) {
              number++;
              newName = nameFolder + ' (' + number + ')';
            }
          }
        }
      }
      return newName;
    }
    catch (exception) {
      return newName;
    }
  }

  fileUpload(req, currentFolder) {
    
    var serverDir = util.serverMapFolder(currentFolder);
    const data = req.payload;
    try {
      if (data.NewFile.length) {
        data.NewFile.forEach(element => {
          var name = element.hapi.filename;
          name = this.checkNameFileValid(name, serverDir);
          const path = serverDir + name;
          const file = fs.createWriteStream(path);
          file.on('error', (err) => console.error(err));
          element.pipe(file);
          element.on('end', (err) => {
            const ret = {
              filename: element.hapi.filename,
              headers: element.hapi.headers
            };
            return JSON.stringify(ret);
          });
        });
        return true;
      }else {
        var name = data.NewFile.hapi.filename;
        name = this.checkNameFileValid(name, serverDir);
        const path = serverDir + name;
        const file = fs.createWriteStream(path);
        file.on('error', (err) => console.error(err));
        data.NewFile.pipe(file);
        data.NewFile.on('end', (err) => {
          const ret = {
            filename: data.NewFile.hapi.filename,
            headers: data.NewFile.hapi.headers
          };
          return JSON.stringify(ret);
        });
        return true;
      }
    }
    catch (exception) {
      return false;
    }
  }
}

DiskFileManager.prototipe = new FileManager({});

export default DiskFileManager;

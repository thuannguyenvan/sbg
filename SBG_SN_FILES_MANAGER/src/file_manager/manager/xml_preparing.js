import escape from 'escape-html';
import xml from 'xml';
import util from './utils';

const xmlPreparing = new function () {

  this.prepareFoldersObject = function (folders) {
    const xmlFolders = [];
    folders.map(function (folder) {
      xmlFolders.push({ Folder: [{ _attr: { name: escape(folder.name), subfolder: escape(folder.subfolder) } }] });
    });
    return { Folders: xmlFolders };
  };

  this.deleteObject = function (status) {
    return { result: status };
  };

  this.renameFile = function (status) {
    return { result: status };
  };

  this.ErrorAuthor = function (status) {
    return { result: status };
  };

  this.prepareFilesObject = function (files) {
    const xmlFiles = [];
    files.map(function (file) {
      xmlFiles.push({ File: [{ _attr: { name: escape(file.name), size: file.size } }] });
    });
    return { Files: xmlFiles };
  };

  this.renderError = function (number, text) {
    const errorObj = createErrorNode(number, text);
    return xml({ Connector: errorObj });
  };

  this.renderResult = function (number, text) {
    const Obj = createResultNode(number, text);
    return xml({ Connector: Obj });
  };

  this.renderResultXml = function (res) {
    const _attr = { result: escape(res) };
    const result = { Result: [{ _attr: _attr }] };
    return xml({ Connector: result });
  };

  this.renderXml = function (command, currentFolder, xmlObjs, userName, url_public) {
    const currentFolderURL = util.getUrlFromPath('/' + userName + currentFolder, command);
    xmlObjs.unshift({ CurrentFolder: [{ _attr: { path: escape(currentFolder), url: escape(currentFolderURL), CurrentFolder: escape(currentFolder), domainImage: escape(url_public), } }] });
    xmlObjs.unshift({ _attr: { command: command } });
    return xml({ Connector: xmlObjs }, { declaration: true });
  };

  function createErrorNode(number, text) {
    const _attr = text ? { number: number, text: escape(text) } : { number: number };
    return { Error: [{ _attr: _attr }] };
  }

  function createResultNode(number, text) {
    const _attr = text ? { number: number, text: escape(text) } : { number: number };
    return { Result: [{ _attr: _attr }] };
  }
};

export default xmlPreparing;

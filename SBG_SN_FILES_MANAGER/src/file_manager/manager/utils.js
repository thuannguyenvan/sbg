import config from './config';
import path from 'path';
import fs from 'fs-extra';

const BASE_FILES_PATH = path.resolve(config.path_root);

const browserUtil = new function () {
  const self = this;

  this.isAllowedCommand = function (command) {
    return config['ConfigAllowedCommands'].join(',').indexOf(command) >= 0;
  };

  this.setXmlHeaders = function (res) {
    res.setHeader('Expires', 'Mon, 26 Jul 1997 05:00:00 GMT');
    res.setHeader('Last-Modified', new Date().toUTCString());
    res.setHeader('Cache-Control', 'no-store, no-cache, must-revalidate');
    res.setHeader('Cache-Control', 'post-check=0, pre-check=0');
    res.setHeader('Pragma', 'no-cache');
    res.setHeader('Content-Type', 'text/xml; charset=utf-8');
  };

  this.isDirExists = function (dir) {
    try {
      return fs.statSync(dir).isDirectory();
    } catch (err) {
      return false;
    }
  };

  this.isFileExists = function (filepath) {
    try {
      return fs.statSync(filepath).isFile();
    } catch (err) {
      return false;
    }
  };

  this.prepareDirs = function (root, dirsArray) {
    dirsArray.map(function (dir) {
      if (!self.isDirExists(root + dir)) {
        fs.mkdirSync(root + dir);
      }
    });
  };

  this.getBaseFilesPath = function () {
    return BASE_FILES_PATH;
  };

  this.DeleteFile = function (dir) {
    try {
      fs.unlinkSync(dir);
      return true;
    } catch (err) {
      return false;
    }
  };

  this.DeleteFolder = function (path) {
    try {
      fs.removeSync(path);
      return true;
    } catch (err) {
      return false;
    }
  };

  this.RenameFolder = function (dir, newdir) {
    try {
      fs.renameSync(dir, newdir, function (err) {
        if (err) return false;
      });
      return true;
    } catch (err) {
      return false;
    }
  };

  this.RenameFile = function (dir, newName) {
    try {
      fs.renameSync(dir, newName, function (err) {
        if (err) return false;
      });
      return true;
    } catch (err) {
      return false;
    }
  };

  this.getFilesAndFolders = function (dir) {
    const folders = [];
    const files = [];

    const readFiles = fs.readdirSync(dir);
    for (let i = 0; i < readFiles.length; i++) {
      const fileStat = fs.lstatSync(dir + readFiles[i]);
      if (fileStat.isDirectory()) {
        // Get subfolder
        const readFolders = fs.readdirSync(dir + readFiles[i] + '/');

        const subs = readFolders.filter(function (fol) {
          const fileStat = fs.statSync(dir + readFiles[i] + '/' + fol);
          if (fileStat.isDirectory()) {
            return fol;
          }
        });

        const folder = {
          name: readFiles[i],
          size: fileStat.size,
          subfolder: (subs !== undefined && subs !== null && subs.length > 0)
        };

        folders.push(folder);

      } else if (fileStat.isFile()) {
        // var reloadFile1 = fs.readFile(dir + readFiles[i]);
        //var reloadFile = fs.lstatSync(dir + readFiles[i]);
        var size = this.bytesToSize(fileStat.size);
        files.push({ name: readFiles[i], size: size });
      }
    }
    return { folders: folders, files: files };
  };

  this.bytesToSize = function(bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return '0 Byte';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
 };

  this.server_MapPath = function (filepath) {
    return BASE_FILES_PATH + '/' + filepath;
  };

  this.serverMapFolder = function (folderPath) {
    folderPath = self.server_MapPath(folderPath);
    return folderPath;
  };

  this.combinePaths = function (basePath, folder) {
    return removeFromEnd(basePath, '/') + '/' + removeFromStart(folder, '/');
  };

  this.getUrlFromPath = function (folderPath) {
    return removeFromStart(folderPath, '/');
  };

  this.sanitizeFolderName = function (newFolderName) {
    newFolderName = stripslashes(newFolderName);
    newFolderName = newFolderName.replace('/\\.|\\\\|\\/|\\||\\:|\\?|\\*|"|<|>|[[:cntrl:]]/', '_');

    return newFolderName;
  };

  this.createDirIfNotExists = function (root, dir) {
    try {
      if (!self.isDirExists(root + dir)) {
        fs.mkdirSync(root + dir);
      }
      return true;
    } catch (err) {
      return false;
    }
  };

  this.isDirExists = function (dir) {
    try {
      return fs.statSync(dir).isDirectory();
    } catch (err) {
      return false;
    }
  };

  function stripslashes(str) {
    /* eslint-disable */
    return str.replace('/\0/g', '0').replace('/\(.)/g', '$1');
  };

  function removeFromStart(sourceString, charToRemove) {
    const pattern = new RegExp('^' + charToRemove + '+');
    return sourceString.replace(pattern, '');
  };

  function removeFromEnd(sourceString, charToRemove) {
    const pattern = new RegExp(charToRemove + '+$');
    return sourceString.replace(pattern, '');
  };
};

export default browserUtil;

import path from 'path';
/* eslint-disable */
const rootPath = path.resolve(process.cwd());
let config = {};
try {
  config = require(`${rootPath}/src/config.json`);
} catch (err) {
  /* eslint-disable */
  console.log('Not file config');
}

var mainConfig = Object.assign(config);

export default Object.assign({}, mainConfig);
var padDigits = function (number, digits) {
    return Array(Math.max(digits - String(number).length + 1, 0)).join(0) + number;
}

var insertNodeAfter = function(self, newNode) {
	self.parentNode.insertBefore(newNode, self.nextSibling);
}

var oConnector	= new Object() ;
var oIcons		= new Object() ;
var oListManager = new Object() ;
oConnector.CurrentFolder	= '/' ;
oConnector.NewPath	= '/' ;
oConnector.OldPath	= '/' ;
oConnector.FolderId = '';
oConnector.FileInFolder = null;

(function() {
	var d = document.domain;
	
	while ( true )
	{
		// Test if we can access a parent property.
		try
		{
			var test = window.opener.document.domain;
			break;
		}
		catch( e )
		{}

		// Remove a domain part: www.mytest.example.com => mytest.example.com => example.com ...
		d = d.replace( /.*?(?:\.|$)/, '' ) ;

		if ( d.length == 0 )
			break;		// It was not able to detect the domain.

		try
		{
			document.domain = d ;
		}
		catch (e)
		{
			break ;
		}
	}
})();

// var getCookie = function(name) {
// 	var value = "; " + document.cookie;
// 	var parts = value.split("; " + name + "=");
// 	if (parts.length == 2) return parts.pop().split(";").shift();
//   }

var GetUrlParam = function ( paramName )
{
	var oRegex = new RegExp( '[\?&]' + paramName + '=([^&]+)', 'i' ) ;
	var oMatch = oRegex.exec( window.top.location.search ) ;

	if ( oMatch && oMatch.length > 1 )
		return decodeURIComponent( oMatch[1] ) ;
	else
		return '' ;
}

var sConnUrl = GetUrlParam( 'Connector' ) ;

if ( sConnUrl.substr(0,1) != '/' && sConnUrl.indexOf( '://' ) < 0 )
	sConnUrl = window.location.href.replace( /browser.html.*$/, '' ) + sConnUrl ;

oConnector.ConnectorUrl = sConnUrl + ( sConnUrl.indexOf('?') != -1 ? '&' : '?' ) ;

var sServerPath = GetUrlParam( 'ServerPath' ) ;
if ( sServerPath.length > 0 )
	oConnector.ConnectorUrl += 'ServerPath=' + encodeURIComponent( sServerPath ) + '&' ;

oConnector.SendCommand = function( command, params, callBackFunction )
{
	var sUrl = this.ConnectorUrl + 'Command=' + command ;
	sUrl += '&CurrentFolder=' + encodeURIComponent( this.CurrentFolder ) ;
	sUrl += '&NewPath=' + encodeURIComponent( this.NewPath ) ;
	sUrl += '&OldPath=' + encodeURIComponent( this.OldPath ) ;

	if ( params ) sUrl += '&' + params ;

	// Add a random salt to avoid getting a cached version of the command execution
	sUrl += '&uuid=' + new Date().getTime() ;

	var fileMan = new FileMan() ;

	if ( callBackFunction )
	fileMan.LoadUrl( sUrl, callBackFunction ) ;	// Asynchronous load.
	else
		return fileMan.LoadUrl( sUrl ) ;

	return null ;
}

oConnector.CheckError = function( responseXml )
{
	var iErrorNumber = 0 ;
	var oErrorNode = responseXml.SelectSingleNode( 'Connector/Error' ) ;

	if ( oErrorNode )
	{
		iErrorNumber = parseInt( oErrorNode.attributes.getNamedItem('number').value, 10 ) ;

		switch ( iErrorNumber )
		{
			case 0 :
				break ;
			case 1 :	// Custom error. Message placed in the "text" attribute.
				alert( oErrorNode.attributes.getNamedItem('text').value ) ;
				break ;
			case 101 :
				alert( 'Folder already exists' ) ;
				break ;
			case 102 :
				alert( 'Invalid folder name' ) ;
				break ;
			case 103 :
				alert( 'You have no permissions to create the folder' ) ;
				break ;
			case 110 :
				alert( 'Unknown error creating folder' ) ;
				break ;
			default :
				alert( 'Error on your request. Error number: ' + iErrorNumber ) ;
				break ;
		}
	}
	return iErrorNumber ;
}

var FileMan = function() {}


FileMan.prototype.SelectSingleNode = function( xpath )
{
	if ( navigator.userAgent.indexOf('MSIE') >= 0 )		// IE
		return this.DOMDocument.selectSingleNode( xpath ) ;
	else					// Gecko
	{
		var xPathResult = this.DOMDocument.evaluate( xpath, this.DOMDocument,
				this.DOMDocument.createNSResolver(this.DOMDocument.documentElement), 9, null);

		if ( xPathResult && xPathResult.singleNodeValue )
			return xPathResult.singleNodeValue ;
		else
			return null ;
	}
}


FileMan.prototype.SelectNodes = function( xpath )
{
	if ( navigator.userAgent.indexOf('MSIE') >= 0 )		// IE
		return this.DOMDocument.selectNodes( xpath ) ;
	else					// Gecko
	{
		var aNodeArray = new Array();

		var xPathResult = this.DOMDocument.evaluate( xpath, this.DOMDocument,
				this.DOMDocument.createNSResolver(this.DOMDocument.documentElement), XPathResult.ORDERED_NODE_ITERATOR_TYPE, null) ;
		if ( xPathResult )
		{
			var oNode = xPathResult.iterateNext() ;
 			while( oNode )
 			{
 				aNodeArray[aNodeArray.length] = oNode ;
 				oNode = xPathResult.iterateNext();
 			}
		}
		return aNodeArray ;
	}
}


FileMan.prototype.GetHttpRequest = function()
{
	// Gecko / IE7
	try { return new XMLHttpRequest(); }
	catch(e) {}

	// IE6
	try { return new ActiveXObject( 'Msxml2.XMLHTTP' ) ; }
	catch(e) {}

	// IE5
	try { return new ActiveXObject( 'Microsoft.XMLHTTP' ) ; }
	catch(e) {}

	return null ;
}

FileMan.prototype.UploadFile = function ( urlToCall, asyncFunctionPointer )
{
	var oFCKXml = this ;
	var form = document.getElementById('frmUpload');
	var token = getParaFromUrl(config.token_key);
	var formData = new FormData(form);
	var bAsync = ( typeof(asyncFunctionPointer) == 'function' ) ;
	var oXmlHttp = this.GetHttpRequest() ;
	oXmlHttp.open( "POST", urlToCall, bAsync ) ;
	oXmlHttp.setRequestHeader('Authorization', token);
	oXmlHttp.onreadystatechange = function()
		{
			if ( oXmlHttp.readyState == 4 )
			{
				asyncFunctionPointer(oXmlHttp.status) ;
			}
		}
	oXmlHttp.send(formData);
}

FileMan.prototype.LoadUrl = function ( urlToCall, asyncFunctionPointer )
{
	var oFCKXml = this ;
	var bAsync = ( typeof(asyncFunctionPointer) == 'function' ) ;
	var token = getParaFromUrl(config.token_key);
	var oXmlHttp = this.GetHttpRequest() ;
	oXmlHttp.open( "GET", urlToCall, bAsync ) ;
    oXmlHttp.setRequestHeader('Authorization', token);
	if ( bAsync )
	{
		oXmlHttp.onreadystatechange = function()
		{
			if ( oXmlHttp.readyState == 4 )
			{
				var oXml ;

				if (oXmlHttp.status === 404 ) { 
					document.getElementById('check-author').style.display = 'block';
					window.resizeTo(window.screen.availWidth / 2, window.screen.availHeight / 2);
					document.getElementById('element').style.display = 'none';
					document.getElementById('check-login').style.display = 'none';
					return ;
				}

				if (oXmlHttp.status === 401 ) {
					document.getElementById('check-author').style.display = 'block';
					document.getElementById('check-login').style.display = 'none';
					// window.resizeTo(window.screen.availWidth / 2, window.screen.availHeight / 2);
					document.getElementById('element').style.display = 'none';
					return ;
				}

				if (oXmlHttp.status === 403 ) {
					document.getElementById('check-login').style.display = 'block';
					document.getElementById('check-author').style.display = 'none';
					// window.resizeTo(window.screen.availWidth / 2, window.screen.availHeight / 2);
					document.getElementById('element').style.display = 'none';

					return ;
				}
				else {
					document.getElementById('check-author').style.display = 'none';
					document.getElementById('element').style.display = 'block';
				}
				try
				{
					oXmlHttp.responseXML.firstChild ;
					oXml = oXmlHttp.responseXML ;
				}
				catch ( e )
				{
					try
					{
						oXml = (new DOMParser()).parseFromString( oXmlHttp.responseText, 'text/xml' ) ;
					}
					catch ( e ) {}
				}

				if ( !oXml || !oXml.firstChild || oXml.firstChild.nodeName == 'parsererror' )
				{
					alert( 'The server didn\'t send back a proper XML response. Please contact your system administrator.\n\n' +
							'XML request error: ' + oXmlHttp.statusText + ' (' + oXmlHttp.status + ')\n\n' +
							'Requested URL:\n' + urlToCall + '\n\n' +
							'Response text:\n' + oXmlHttp.responseText ) ;
					return ;
				}

				oFCKXml.DOMDocument = oXml ;
				asyncFunctionPointer( oFCKXml, oXmlHttp.status) ;
			}
		}
	}

	oXmlHttp.send( null ) ;

	if ( ! bAsync )
	{
		if ( oXmlHttp.status == 200 || oXmlHttp.status == 304)
			this.DOMDocument = oXmlHttp.responseXML ;
		else
		{
			alert( 'XML request error: ' + oXmlHttp.statusText + ' (' + oXmlHttp.status + ')' ) ;
		}
	}
}

FileMan.prototype.GetFolders = function (folderPath) {
	oConnector.CurrentFolder = folderPath;
    oConnector.SendCommand( 'GetFoldersAndFiles', null, GetFoldersCallBack ) ;
}

FileMan.prototype.DeleteFile = function (fileUrl,folderPath) {
	
	oConnector.CurrentFolder = fileUrl;
	oConnector.SendCommand( 'DeleteFile', null, GetDeleteFileCallBack ) ;
	GetFileByFolder(folderPath);
}

FileMan.prototype.DeleteFolder = function (folderPath) {
	
	oConnector.CurrentFolder = folderPath;
	oConnector.SendCommand( 'DeleteFolder', null, GetDeleteFolderCallBack ) ;
	this.LoadFolders('/');
}

FileMan.prototype.RenameFolder = function (folderPath, folderOld, folderNew) {
	
	oConnector.CurrentFolder = folderPath;
	oConnector.NewPath = folderNew;
	oConnector.OldPath = folderOld;
	oConnector.SendCommand( 'RenameFolder', null, GetRenameFolderCallBack ) ;
	this.LoadFolders('/');
}

FileMan.prototype.RenameFile = function (folderPath, fileUrlOld, fileUrlNew) {
	
	oConnector.CurrentFolder = folderPath;
	oConnector.NewPath = fileUrlNew;
	oConnector.OldPath = fileUrlOld;
	oConnector.SendCommand( 'RenameFile', null, GetRenameFileCallBack ) ;
	GetFileByFolder(folderPath);
}

FileMan.prototype.GetFileByFolder = function (folderPath) {
	oConnector.CurrentFolder = folderPath;
    oConnector.SendCommand( 'GetFoldersAndFiles', null, GetFilesCallBack ) ;
}


FileMan.prototype.LoadFolders = function (folderPath) {
	oConnector.CurrentFolder = folderPath;
	oConnector.SendCommand( 'GetFoldersAndFiles', null, GetFoldersAndFilesCallBack ) ;
}

var getParaFromUrl = function(name) {
	var para = document.URL.match(new RegExp(name + '=([^&]*)'));
	if (para !== undefined && para !== null && para !== 'undefined' && para !== '') {
		return para[1].replace('#', '');
	}
	return '';
}

var loadJSON = function(callback) {   

    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open('GET', './config.json', true); 
    xobj.onreadystatechange = function () {
          if (xobj.readyState == 4 && xobj.status == "200") {
          	return  callback(xobj.responseText);
          }
    };
    xobj.send(null);  
 }

var langCode = getParaFromUrl(config.lang_key);
	if (langCode !== undefined && langCode !== null && langCode !== 'undefined' && langCode !== '') {
		langCode = langCode.replace('#', '');
	} else {
		langCode = 'en';
	}

var acceptFile = getParaFromUrl(config.accept_upload_key);
	if (acceptFile !== undefined && acceptFile !== null && acceptFile !== 'undefined' && acceptFile !== '') {
		acceptFile = acceptFile[1].replace('#', '');
	} else {
		acceptFile = 'files';
	}
		
	if (config.hasOwnProperty(acceptFile)) {
		var inputFile = document.getElementById("NewFile"); 
		inputFile.setAttribute("accept", config[acceptFile]);
	} else {
		var inputFile = document.getElementById("NewFile"); 
		inputFile.setAttribute("accept", "image/*");
	}
	
var loadFile = function(path, suc, err) {
	const script = document.createElement('script');
	script.type = 'text/javascript';
	script.async = true;
	script.src = path;
	document.getElementsByTagName("head")[0].appendChild(script);
	script.addEventListener('load', () => {
		suc();
	});
	script.addEventListener('error', () => {
		err();
	});
}

var fileMan = new FileMan();
var oIcons = new Object();
window.onload = function () {
	SpinnerStart();
	loadFile("./lang/" + langCode + ".js", function () {
		document.title = lang.title;
		document.getElementById("notAuthor_lang").innerHTML = lang.notAuthor;
		document.getElementById("accessDenine_lang").innerHTML = lang.accessDenine;
		document.getElementById("createFolder_lang").innerHTML = lang.newFolder;
		document.getElementById("uploadFile_lang").innerHTML = lang.upLoadFile;
		document.getElementById("search_lang").placeholder = lang.searchFile + ' ..';
		document.getElementById("emptyFolder_lang").innerHTML = lang.emptyFolder;
		
		fileMan.LoadFolders('/');
	}, function () {
		loadFile("./lang/vi.js", function () {
			fileMan.LoadFolders('/');
		});
	});
}

oIcons.AvailableIcons = new Object();

oIcons.GetIcon = function (fileName) {
	if (fileName !== undefined && fileName !== null && fileName != '') {
		var format = fileName.split('.').pop();
		if (format === 'png' || format === 'jpeg' || format === 'jpg') {
			return 'default';
		}

		if (formatBackground !== undefined && formatBackground !== null) {
			if (formatBackground.hasOwnProperty(format)) {
				return formatBackground[format];
			} else {
				return './images/formatfile/default.png';
			}
		}
	}
}

var ProtectPath = function (path) {
	path = path.replace(/\\/g, '\\\\');
	path = path.replace(/'/g, '\\\'');
	return path;
}

var rootLoad = function (folderPath, id) {
	GetFileByFolder(folderPath, id);
}

var GetFolderRowHtml = function (folderName, folderPath, hasSubfolder, id, isRoot) {
	var arrow = hasSubfolder ? '<span class="arrow-container" onclick="OpenFolder(\'' + ProtectPath(folderPath) + '\', \'' + id + '\');return false;">'
		+ '<span class="arrow fas"></span>' + '</span>' : '';
	var arrowCollsapse = hasSubfolder ? '<h1 style="display:none" class="arrow-container" onclick="OpenFolderCollapse(\'' + ProtectPath(folderPath) + '\', \'' + id + '\');return false;">'
	+ '</h1></li>' : '';
	if (isRoot) {
		return '<li data-target="#sub-' + id + '" id="' + id + '" style="position: relative;" class="collapsed active ' + folderPath + '" >' +
			'<a onclick="GetFileByFolder(\'' + ProtectPath(folderPath) + '\', \'' + id + '\');return false;"> <i class="fas fa-lg fa-home"></i>  ' +
			folderName + '</a>' + arrow + arrowCollsapse;
	}

	return '<li data-target="#sub-' + id + '" id="' + id + '" style="position: relative;" class="collapsed ' + folderPath + '" >' +
		'<a  title="' + folderName + '" class="nameFolder" oncontextmenu="contextmenufolder(event,\'' + ProtectPath(folderPath) + '\')"  onclick="GetFileByFolder(\'' + ProtectPath(folderPath) + '\', \'' + id + '\');return false;"> <i class="fas fa-lg item-folder"></i>  ' +
		folderName + '</a>' + arrow + arrowCollsapse;
}

// var GetFolderRightRowHtml = function (folderName, folderPath, id) {
// 	var sLink = '<div oncontextmenu="contextmenufolder(event,\'' + ProtectPath(folderPath) + '\')" onclick="GetFileByFolder(\'' + ProtectPath(folderPath) + '\', \'' + id + '\');return false;" class="itemFolder" ><a oncontextmenu="return false" href="#" >';
// 	return sLink + '<i class="fas fa-lg fa-home"></i></a><div oncontextmenu="return false" class="nameFile nameFolderLeft" title="' + folderName + '">' + folderName + '</div>'
// 			+ '</div>';
// }

var GetFolderRightRowHtml = function (folderName, folderPath, id) {
	var sLink = '<div oncontextmenu="contextmenufolder(event,\'' + ProtectPath(folderPath) + '\')" onclick="GetFileByFolder(\'' + ProtectPath(folderPath) + '\', \'' + id + '\');return false;" class="item" ><a oncontextmenu="return false" href="#" >';
	var src = formatBackground["folder"];
	return sLink + '<img  alt="" src="' + src + '" border="0" ></a><div oncontextmenu="return false" class="nameFile nameFolderLeft" title="' + folderName + '">' + folderName + '</div>'
			+ '</div>';
}

var GetFileRowHtml = function (fileName, fileUrl, fileSize, CurrentFolderUrl, domainImage) {
	var sLink = '<div onclick="OpenFile(\'' + ProtectPath(fileUrl) + '\');return false;" class="item" oncontextmenu="contextmenu(event, \'' + fileUrl + '\',\'' + CurrentFolderUrl + '\',\'' + fileName + '\');return false;"><div class="item_img_content"><a href="#" >';
	var sIcon = oIcons.GetIcon(fileName);
	var src = sIcon == 'default' ? (domainImage + ProtectPath(fileUrl)) : sIcon;
	return sLink + '<img alt="" src="' + src + '" border="0" ></div></a><div class="nameFile" title="' + fileName + '">' + fileName + '</div>'
			+ '<div class="sizeFile"  title="' + fileSize + '">' + fileSize + '</div>'
			+ '</div>';
}

var GetFileByFolder = function (folderPath, id) {
	SpinnerStart();
	fileMan.GetFileByFolder(folderPath);
	OpenFolder(folderPath, id);
	oConnector.sCurrentFolderPath = folderPath;
	var sUrl = oConnector.ConnectorUrl + 'Connector=/browse&' + 'Command=FileUpload';
	sUrl += '&CurrentFolder=' + encodeURIComponent(folderPath);
	document.getElementById('frmUpload').action = sUrl;

	var menuContent = document.getElementById("menu-content");
	var lst = menuContent.getElementsByTagName("li");
	for (var i = 0; i < lst.length; i++) {
		lst[i].classList.remove("active");
	}
	document.getElementsByClassName(folderPath)[0].classList.add("active");
	
	var liCurrent = document.getElementsByClassName(folderPath)[0];
	// var a = liCurrent.getElementsByTagName("h1")[0] ;
	if (liCurrent.getElementsByTagName("h1")[0] !== undefined && liCurrent.getElementsByTagName("h1")[0] !== null) {
		liCurrent.getElementsByTagName("h1")[0].click();
	}
}

var getUrlParam = function (paramName) {
	var reParam = new RegExp('(?:[\?&]|&)' + paramName + '=([^&]+)', 'i');
	var match = window.top.location.search.match(reParam);

	return (match && match.length > 1) ? match[1] : null;
}

var OpenFile = function (fileUrl) {
	var funcNum = getUrlParam('CKEditorFuncNum');
	window.top.opener.CKEDITOR.tools.callFunction(funcNum, fileUrl);
	window.top.close();
	window.top.opener.focus();
}

var CreateFolder = function () {
	swal({
		title: lang.titleCreateFolder,
		input: 'text',
		width: 350,
		showCancelButton: true,
		confirmButtonText: lang.buttonCreate,
		cancelButtonText: lang.buttonCancel,
		showLoaderOnConfirm: true,
		preConfirm: function (text) {
			return new Promise(function (resolve) {
				setTimeout(function () {
					if (text.length > 50) {
						swal.showValidationError(
							lang.validLenghtFolder
							//'Tên thư mục không được dài quá 25 ký tự.'
						)
					}
					resolve()
				}, 100)
			})
		},
		allowOutsideClick: false
	}).then(function (result) {
		if (result.value) {
			SpinnerHide();
			oConnector.SendCommand('CreateFolder', 'NewFolderName=' + encodeURIComponent(result.value), CreateFolderCallBack);

		}
	});
}

var toastMessage = function (message, status) {
	const toast = Swal.mixin({
		toast: true,
		position: 'top-end',
		showConfirmButton: false,
		timer: 2000
	});
	toast({
		type: status,
		title: message
	});
}

var CreateFolderCallBack = function (fckXml, statusCode) {
	if (statusCode === 400) {
		toastMessage(lang.messageError, 'error');
	}
	if (statusCode === 200) {
		toastMessage(lang.messageCreateSuccess, 'success');
	}
	if (oConnector.CheckError(fckXml) == 0) fileMan.LoadFolders('/');
	SpinnerHide();
}

var OpenFolder = function (folderPath, id) {
	// check xem da load subfolder hay chua
	var folder = document.getElementById(id);
	var subFolder = document.getElementById('sub-' + id);

	// document.getElementById("element-id").outerHTML = "";
	if (subFolder === undefined || subFolder === null || subFolder === '') {
		oConnector.FolderId = id;
		SpinnerStart();
		fileMan.GetFolders(folderPath);
	} else {
		SpinnerStart();
		subFolder.outerHTML = "";
		fileMan.GetFolders(folderPath);
		if (subFolder.classList.contains("show")) {
			subFolder.classList.remove("show");
			folder.classList.add("collapsed");
		} else {
			subFolder.classList.add("show");
			folder.classList.remove("collapsed");
		}
	}
}

var OpenFolderCollapse = function (folderPath, id) {
	// check xem da load subfolder hay chua
	var folder = document.getElementById(id);
	var subFolder = document.getElementById('sub-' + id);

	//folder.classList.add("loading");
	if (subFolder === undefined || subFolder === null) {
		oConnector.FolderId = id;
		SpinnerStart();
		fileMan.GetFolders(folderPath);
	} else if (!subFolder.classList.contains("show")) {
		subFolder.classList.remove("show");
		folder.classList.add("collapsed");
	}
}

var GetRenameFolderCallBack = function (fckXml, statusCode) {
	if (statusCode === 400) {
		toastMessage(lang.messageRenameFolderNotValid, 'warning');
	}
	if (statusCode === 200) {
		toastMessage(lang.messageEditFolderSuccess, 'success');
	}
	SpinnerHide();
}

var GetRenameFileCallBack = function (fckXml, statusCode) {
	if (statusCode === 400) {
		toastMessage(lang.messageError, 'error');
	}
	if (statusCode === 200) {
		toastMessage(lang.messageEditFileSuccess, 'success');
	}
	SpinnerHide();
}


var GetDeleteFolderCallBack = function (fckXml, statusCode) {
	if (statusCode === 400) {
		toastMessage(lang.messageError, 'error');
	}
	if (statusCode === 200) {
		toastMessage(lang.messageDeleteFolderSuccess, 'success');
	}
	SpinnerHide();
}

var GetDeleteFileCallBack = function (fckXml, statusCode) {
	if (statusCode === 400) {
		toastMessage(lang.messageError, 'error');
	}
	if (statusCode === 200) {
		toastMessage(lang.messageDeleteFileSuccess, 'success');
	}
	SpinnerHide();
}

var GetFoldersCallBack = function (fckXml) {
	if (oConnector.CheckError(fckXml) != 0)
		return;

	// Get the current folder path.
	var oFolderNode = fckXml.SelectSingleNode('Connector/CurrentFolder');
	if (oFolderNode == null) {
		toastMessage(lang.messageError, 'error');
		return;
	}
	var sCurrentFolderPath = oFolderNode.attributes.getNamedItem('path').value;
	var sCurrentFolderUrl = oFolderNode.attributes.getNamedItem('url').value;
	var oHtmlFolder = new StringBuilder();
	var oHtmlFile = new StringBuilder();

	// Add the Folders.
	var oNodes;
	oNodes = fckXml.SelectNodes('Connector/Folders/Folder');
	for (var i = 0; i < oNodes.length; i++) {
		var sFolderName = oNodes[i].attributes.getNamedItem('name').value;
		var sFolderHasSubfolder = oNodes[i].attributes.getNamedItem('subfolder').value === "true";

		var id = new Date().getTime();
		oHtmlFolder.Append(GetFolderRowHtml(sFolderName, sCurrentFolderPath + sFolderName + "/", sFolderHasSubfolder, id + padDigits(i, 5)));
	}

	var folder = document.getElementById(oConnector.FolderId);
	if (folder !== undefined && folder !== null) {
		var ulNode = document.createElement("ul");
		var level = sCurrentFolderPath.split('/').length - 1;
		var levelClass = 'sub-level-' + level;
		ulNode.id = 'sub-' + oConnector.FolderId;
		ulNode.classList.add(levelClass);
		ulNode.classList.add("collapse");
		insertNodeAfter(folder, ulNode);

		oConnector.FolderId = '';
		ulNode.innerHTML = oHtmlFolder.ToString();

		ulNode.classList.add("show");
		folder.classList.remove("collapsed");
	}
	
	SpinnerHide();
}

var GetFilesCallBack = function (fckXml) {
	if (oConnector.CheckError(fckXml) != 0)
		return;

	// Get the current folder path.
	var oFolderNode = fckXml.SelectSingleNode('Connector/CurrentFolder');
	if (oFolderNode == null) {
		toastMessage(lang.messageError, 'error');
		return;
	}
	var sCurrentFolderPath = oFolderNode.attributes.getNamedItem('path').value;
	var sCurrentFolderUrl = oFolderNode.attributes.getNamedItem('url').value;
	var domainImage = oFolderNode.attributes.getNamedItem('domainImage').value;
	var CurrentFolderUrl = oFolderNode.attributes.getNamedItem('CurrentFolder').value;

	// Add the Files.
	var oHtmlFile = new StringBuilder();
	var oNodes;
	oNodes = fckXml.SelectNodes('Connector/Files/File');
	oConnector.FileInFolder = oNodes;
	
	oConnector.DomainImage = domainImage;
	oConnector.sCurrentFolderUrl = sCurrentFolderUrl;
	oConnector.CurrentFolder = CurrentFolderUrl;
	oConnector.sCurrentFolderPath = sCurrentFolderPath;
	var oNodesFolder;
	oNodesFolder = fckXml.SelectNodes('Connector/Folders/Folder');
	oConnector.FolderRight = oNodesFolder;
	//oHtmlFile.Append('<div class="lableHeader">Thư mục</div><br>');

	var htmlFolder = '';
	for (var i = 0; i < oNodesFolder.length; i++) {
		var sFolderName = oNodesFolder[i].attributes.getNamedItem('name').value;
		var sFolderHasSubfolder = oNodesFolder[i].attributes.getNamedItem('subfolder').value === "true";

		var id = new Date().getTime() + padDigits(i, 5);
		htmlFolder += (GetFolderRightRowHtml(sFolderName, sCurrentFolderPath + sFolderName + "/",  id));
	}
	if (htmlFolder !== null && htmlFolder !== '') {
		oHtmlFile.Append('<div class="lableHeader">' + lang.folder + '</div>');
		oHtmlFile.Append(htmlFolder.toString());
		oHtmlFile.Append('<br><br>');
	}

	// oHtmlFile.Append('<div class="lableHeader">Tệp</div><br>');
	var htmlFIle = '';
	for (var j = 0; j < oNodes.length; j++) {
		var oNode = oNodes[j];
		var sFileName = oNode.attributes.getNamedItem('name').value;
		var sFileSize = oNode.attributes.getNamedItem('size').value;
		var oFileUrlAtt = oNodes[j].attributes.getNamedItem('url');
		var sFileUrl = oFileUrlAtt != null ? oFileUrlAtt.value : (sCurrentFolderUrl + sFileName);
		htmlFIle += (GetFileRowHtml(sFileName, sFileUrl, sFileSize, CurrentFolderUrl, domainImage));
	}
	if (htmlFIle !== null && htmlFIle !== '') {
		oHtmlFile.Append('<div class="lableHeader">' + lang.file + '</div>');
		oHtmlFile.Append(htmlFIle.toString());
	}

	document.getElementById("files").innerHTML = oHtmlFile.ToString();

	if (oHtmlFile == null || oHtmlFile.ToString() == '') {
		$("#folderEmpty").css("display", "inline-flex");
	} else {
		$("#folderEmpty").css("display", "none");
	}
	SpinnerHide();
}

var GetFoldersAndFilesCallBack = function (fckXml) {
	if (oConnector.CheckError(fckXml) != 0)
		return;

	// Get the current folder path.
	var oFolderNode = fckXml.SelectSingleNode('Connector/CurrentFolder');
	if (oFolderNode == null) {
		toastMessage(lang.messageError, 'error');
		return;
	}

	// GetFoldersCallBack(fckXml);
	GetFilesCallBack(fckXml);
	var sCurrentFolderPath = oFolderNode.attributes.getNamedItem('path').value;
	var sCurrentFolderUrl = oFolderNode.attributes.getNamedItem('url').value;

	var oHtmlFolder = new StringBuilder();
	var id =  padDigits(new Date().getTime(), 6)
	oHtmlFolder.Append(GetFolderRowHtml(lang.sourceFolder, '/', true, id, true));
	
	var sUrl = oConnector.ConnectorUrl + 'Connector=/browse&' + 'Command=FileUpload';
	sUrl += '&CurrentFolder=' + encodeURIComponent('/');
	document.getElementById('frmUpload').action = sUrl;
	document.getElementById("menu-content").innerHTML = oHtmlFolder.ToString();
	OpenFolder('/',id.toString());
	SpinnerHide();
}

var uploadfile = function () {
	SpinnerStart();
	fileMan.UploadFile(document.getElementById('frmUpload').action + '&Accept=' + acceptFile, successUpload);
	$('#NewFile').val(null);
}

var successUpload = function (statusCode) {
	switch (statusCode) {
		case 413:
			toastMessage(lang.messageUploadTooLarge + ' ' + config.maxSize, 'warning');
			break;
		case 403:
		toastMessage(lang.messageUploadFormatError, 'warning');
			break;
		case 200:
			toastMessage(lang.uploadFileSuccess, 'success');
			break;
	}
	var folderPath = oConnector.sCurrentFolderPath;
	fileMan.GetFileByFolder(folderPath);
}

var Spinner = function () {
	var target = $('#target');
	if (target.hasClass('loading')) {
		$('body').css('pointer-events', 'all');
		target.loadingOverlay('remove');
	} else {
		$('body').css('pointer-events', 'none');
		target.loadingOverlay();
	};
}

var SpinnerHide = function () {
	var target = $('#target');
	if (target.hasClass('loading')) {
		$('body').css('pointer-events', 'all');
		target.loadingOverlay('remove');
	}
}

var SpinnerStart = function () {
	var target = $('#target');
	if (!target.hasClass('loading')) {
		$('body').css('pointer-events', 'none');
		target.loadingOverlay();
	}
}

var DeleteFolder = function (folderPath) {
	Swal({
		title: lang.titleDeleteFolder,
		width: 350,
		text: lang.answerDeleteFolder,
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		customClass: 'answer-delete',
		cancelButtonColor: '#d33',
		confirmButtonText: lang.buttonAccept,
		cancelButtonText: lang.buttonCancel,
	}).then((result) => {
		if (result.value) {
			SpinnerStart();
			fileMan.DeleteFolder(folderPath);
		}
	});
}

var RenameFolder = function (folderPath) {
	var arrayFolder = folderPath.split('/');
	var infoValue = arrayFolder[arrayFolder.length - 2];
	swal({
		title: lang.titleEditFolder,
		input: 'text',
		width: 350,
		inputValue: infoValue,
		showCancelButton: true,
		confirmButtonText: lang.buttonEdit,
		cancelButtonText: lang.buttonCancel,
		showLoaderOnConfirm: true,
		preConfirm: function (text) {
			return new Promise(function (resolve) {
				setTimeout(function () {
					if (text.length > 50) {
						swal.showValidationError(
							lang.validLenghtFolder
						)
					}
					resolve()
				}, 100)
			})
		},
		allowOutsideClick: false
	}).then(function (result) {
		if (result.value) {
			var path = folderPath.substr(0, folderPath.lastIndexOf(infoValue));
			SpinnerStart();
			fileMan.RenameFolder(path, folderPath, path + result.value + '/');
		}
	});
}

var DeleteFile = function (pathFile, folderPath) {
	Swal({
		title: lang.titleDeleteFile,
		text: lang.answerDeleteFile,
		type: 'warning',
		showCancelButton: true,
		width: 350,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: lang.buttonAccept,
		cancelButtonText: lang.buttonCancel,
	})
		.then((result) => {
			if (result.value) {
				SpinnerStart();
				fileMan.DeleteFile(pathFile, folderPath);
			}
		});
}

var onSearch = function () {
	var keysearch = document.getElementById('search_lang').value;
	if (oConnector.sCurrentFolderUrl !== undefined && oConnector.sCurrentFolderUrl != null && oConnector.sCurrentFolderUrl != ''
		&& oConnector.FileInFolder !== undefined && oConnector.FileInFolder != null && oConnector.FileInFolder != '') {
		var oHtmlFile = new StringBuilder();
		var oNodes = oConnector.FileInFolder;
		var domainImage = oConnector.DomainImage;
		var oNodesFolder;
		oNodesFolder = oConnector.FolderRight;
		sCurrentFolderPath = oConnector.sCurrentFolderPath;
		for (var i = 0; i < oNodesFolder.length; i++) {
			var sFolderName = oNodesFolder[i].attributes.getNamedItem('name').value;
			var id = new Date().getTime() + padDigits(i, 5);
			if (keysearch !== undefined && keysearch !== null && keysearch !== '') {
				if (sFolderName.includes(keysearch)) {
					oHtmlFile.Append(GetFolderRightRowHtml(sFolderName, sCurrentFolderPath + sFolderName + "/",  id));
				}
			} else {
				oHtmlFile.Append(GetFolderRightRowHtml(sFolderName, sCurrentFolderPath + sFolderName + "/",  id));
			}
		}
		for (var j = 0; j < oNodes.length; j++) {

			var oNode = oNodes[j];
			var sFileName = oNode.attributes.getNamedItem('name').value;
			var sFileSize = oNode.attributes.getNamedItem('size').value;
			var oFileUrlAtt = oNodes[j].attributes.getNamedItem('url');

			var sFileUrl = oFileUrlAtt != null ? oFileUrlAtt.value : (oConnector.sCurrentFolderUrl + sFileName);
			if (keysearch !== undefined && keysearch !== null && keysearch !== '') {
				if (sFileName.includes(keysearch)) {
					oHtmlFile.Append(GetFileRowHtml(sFileName, sFileUrl, sFileSize, oConnector.CurrentFolder, domainImage));
				}
			} else {
				oHtmlFile.Append(GetFileRowHtml(sFileName, sFileUrl, sFileSize, oConnector.CurrentFolder, domainImage));
			}
		}

		document.getElementById("files").innerHTML = oHtmlFile.ToString();
	}
}

var baseName = function (str) {
	var base = new String(str).substring(str.lastIndexOf('/') + 1);
	if (base.lastIndexOf(".") != -1)
		base = base.substring(0, base.lastIndexOf("."));
	return base;
}

var RenameFile = function (pathFile, folderPath, fileName) {
	var tail = fileName.split('.').pop();
	var nameFile = baseName(fileName);
	swal({
		title: lang.titleEditFile,
		input: 'text',
		inputValue: nameFile,
		width: 350,
		showCancelButton: true,
		confirmButtonText: lang.buttonEdit,
		cancelButtonText: lang.buttonCancel,
		showLoaderOnConfirm: true,
		preConfirm: function (text) {
			return new Promise(function (resolve) {
				setTimeout(function () {
					if (text.length > 50) {
						swal.showValidationError(
							lang.validLenghtFile
						)
					}
					resolve()
				}, 100)
			})
		},
		allowOutsideClick: false
	}).then(function (result) {
		if (result.value) {
			SpinnerStart();
			fileMan.RenameFile(folderPath, folderPath + fileName, folderPath + result.value + '.' + tail);
		}
	});
}

var addClassActive = function(e) {
	$('#files .item').removeClass('active');
	var parent = $(e.target).parent();
	if (parent !== undefined && parent !== null) {
		if ($(parent).hasClass('item')) {
			$(parent).addClass('active');

		} else {
			parent = $(parent).parent().parent();
			if (parent !== undefined && parent !== null) {
				$(parent).addClass('active');
			}
		}
	}
}

var contextmenu = function (e, fileurl, folderPath, fileName) {
	addClassActive(e);
	var myMenuFile = [{
		icon: 'fa fa-trash',
		label: lang.labelDelete,
		action: function (option, contextMenuIndex, optionIndex) {
			DeleteFile(option.data, option.folderPath);
		},
		submenu: null,
		disabled: false,
		data: fileurl,
		folderPath: folderPath
	},
	{
		icon: 'fa fa-edit',
		label: lang.labelEdit,
		action: function (option, contextMenuIndex, optionIndex) {
			RenameFile(option.data, option.folderPath, option.fileName);
		},
		submenu: null,
		disabled: false,
		data: fileurl,
		folderPath: folderPath,
		fileName: fileName
	}];

	superCm.createMenu(myMenuFile, e);
}

var contextmenufolder = function (e, folderPath) {
	addClassActive(e);
	var myMenuFolder = [{
		icon: 'fa fa-trash',
		label: lang.labelDelete,
		action: function (option, contextMenuIndex, optionIndex) {
			DeleteFolder(option.folderPath);
		},
		submenu: null,
		disabled: false,
		folderPath: folderPath
	},
	{
		icon: 'fa fa-edit',
		label: lang.labelEdit,
		action: function (option, contextMenuIndex, optionIndex) {
			RenameFolder(option.folderPath);
		},
		submenu: null,
		disabled: false,
		folderPath: folderPath
	}];

	superCm.createMenu(myMenuFolder, e);
}
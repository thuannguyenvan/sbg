formatBackground = {
    txt: "./images/formatfile/txt.png",
    rar: "./images/formatfile/rar.png",
    exe: "./images/formatfile/exe.png",
    css: "./images/formatfile/css.png",
    html: "./images/formatfile/chrome.png",
    dll: "./images/formatfile/default.png",
    docx: "./images/formatfile/doc.png",
    zip: "./images/formatfile/zip.png",
    xlsx: "./images/formatfile/excel.png",
    xls: "./images/formatfile/excel.png",
    ppt: "./images/formatfile/default.png",
    pdf: "./images/formatfile/pdf.png",
    js: "./images/formatfile/js.png",
    folder: "./images/formatfile/folder.png"
  }
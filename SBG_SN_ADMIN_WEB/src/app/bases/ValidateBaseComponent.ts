import { ValidateHelper } from '../helpers';
import { FormGroup } from '@angular/forms';

export abstract class ValidateBaseComponent {
    public error: any = {};
    public validators: any = {};
    public formGroup: FormGroup;

    constructor(private helper: ValidateHelper) {
    }

    public resetValidate() {
        this.helper.resetValidate(this.formGroup);
    }

    public resetValidateForm(formGroup: FormGroup) {
        this.helper.resetValidate(formGroup);
    }

    public clearValidate() {
        this.error = {};
        this.resetValidate();
    }

    public invalid(): boolean {
        this.helper.validateAllFormFields(this.formGroup);
        return this.formGroup.invalid;
    }

    public invalidForm(formGroup: FormGroup): boolean {
        this.helper.validateAllFormFields(formGroup);
        return formGroup.invalid;
    }

    public removeValidateServer(fieldName: string) {
        const control = this.formGroup.get(fieldName);
        control.setErrors({});
        control.markAsPristine();
        control.markAsTouched({ onlySelf: true });
        control.updateValueAndValidity();
    }
}

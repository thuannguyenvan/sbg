import { FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';
import {
  OnInit,
  OnDestroy
} from '@angular/core';

export interface IBaseComponent extends OnInit, OnDestroy {
  subscriptions: Subscription[];
  onSubscribeSuccess(): any;
  onSubscribeError(): any;
  destroyAction(): any;
  initValidate(fb: FormBuilder): any;
}

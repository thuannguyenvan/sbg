﻿export enum ErrorType {
    ERROR = 0,
    WARNING = 1,
    FORBIDDEN = 2
}

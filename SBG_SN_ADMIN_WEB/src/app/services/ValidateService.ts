import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHelper } from '../helpers';
import { EnvService } from './EnvService';
import { UrlApiConstants } from '../constants';

@Injectable()
export class ValidateService {

    constructor(private httpHelper: HttpHelper,
        private env: EnvService) {
    }

    checkValid(model: any): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.Validate;
        return this.httpHelper.post(url, model);
    }
}

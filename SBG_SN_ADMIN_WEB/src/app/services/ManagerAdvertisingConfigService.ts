import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHelper } from '../helpers';
import { EnvService } from './EnvService';
import { UrlApiConstants } from '../constants';
import { String } from 'typescript-string-operations';

@Injectable()
export class ManagerAdvertisingConfigService {

    constructor(private httpHelper: HttpHelper, private env: EnvService) {
    }

    searchAdvertisingsConfig(model: any): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.ManagerAdvertisingConfig.SEARCH_ADVERTISING_CONFIG;
        return this.httpHelper.post(url, model);
    }

    searchAdvertisingsPopUp(model: any): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.ManagerAdvertisingConfig.SEARCH_ADVERTISING_POPUP;
        return this.httpHelper.post(url, model);
    }

    getSearchAdvertisingInfoPopUp(): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.ManagerAdvertisingConfig.SEARCH_INFO_ADVERTISING_POPUP;
        return this.httpHelper.get(url);
    }

    createAdvertisingConfig(model: any): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.ManagerAdvertisingConfig.CREATE_ADVERTISING_CONFIG;
        return this.httpHelper.post(url, model);
    }

    deleteAdvertisingConfig(id: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.ManagerAdvertisingConfig.DELETE_ADVERTISING_CONFIG, id);
        return this.httpHelper.delete(url);
    }

    updateAdvertisingConfig(model: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.ManagerAdvertisingConfig.UPDATE_ADVERTISING_CONFIG, model.id);
        return this.httpHelper.put(url, model.data);
    }

    getUpdateAdvertisingConfigInfo(id: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.ManagerAdvertisingConfig.UPDATE_INFO_ADVERTISING_CONFIG, id);
        return this.httpHelper.get(url);
    }

    activateAdvertisingConfig(id: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.ManagerAdvertisingConfig.ACTIVE_ADVERTISING_CONFIG, id);
        return this.httpHelper.post(url, null);
    }

    deActivateAdvertisingConfig(id: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.ManagerAdvertisingConfig.DEACTIVE_ADVERTISING_CONFIG, id);
        return this.httpHelper.post(url, null);
    }
}

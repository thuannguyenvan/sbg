import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHelper } from '../helpers';
import { EnvService } from './EnvService';
import { UrlApiConstants } from '../constants';
import { String } from 'typescript-string-operations';

@Injectable()
export class ManagerLivestreamService {

    constructor(private httpHelper: HttpHelper, private env: EnvService) {
    }

    getCompetition(): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.ManagerCompetitions.GET_COMPETITION_INFO;
        return this.httpHelper.get(url);
    }

    getSearchInfo(): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.ManagerLivestream.GET_SEARCH_INFO;
        return this.httpHelper.get(url);
    }

    getRound(id: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.ManagerLivestream.GET_ROUND, id);
        return this.httpHelper.get(url);
    }

    getMatch(id: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.ManagerLivestream.GET_MATCH, id);
        return this.httpHelper.get(url);
    }

    deleteLivestream(id: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.ManagerLivestream.DELETE_LIVESTREAM, id);
        return this.httpHelper.delete(url);
    }

    createLivestream(model: any): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.ManagerLivestream.CREATE_LIVESTREAM;
        return this.httpHelper.post(url, model);
    }

    getCreateInfo(matchId: any): Observable<any> {
        let url = this.env.API_ENDPOINT + UrlApiConstants.ManagerLivestream.CREATE_INFO_LIVESTREAM;
        if (matchId !== undefined && matchId !== null) {
            url = url + '?soccerMatchId=' + matchId;
        }
        return this.httpHelper.get(url);
    }

    updateLivestream(model: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.ManagerLivestream.UPDATE_LIVESTREAM, model.id);
        return this.httpHelper.put(url, model.data);
    }

    getInfoUpdate(id: any): Observable<any> {
        const url =  String.Format(this.env.API_ENDPOINT + UrlApiConstants.ManagerLivestream.UPDATE_INFO_LIVESTREAM, id);
        return this.httpHelper.get(url);
    }

    seachLivestream(model: any): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.ManagerLivestream.SEARCH;
        return this.httpHelper.post(url, model);
    }

}

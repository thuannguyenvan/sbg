import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHelper } from '../helpers';
import { EnvService } from './EnvService';
import { UrlApiConstants } from '../constants';
import { String } from 'typescript-string-operations';

@Injectable()
export class ManagerPartnerService {

    constructor(private httpHelper: HttpHelper, private env: EnvService) {
    }

    createPartner(model: any): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.Partner.CREATE_PARTNER;
        return this.httpHelper.post(url, model);
    }

    updatePartner(model: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.Partner.UPDATE_PARTNER, model.id);
        return this.httpHelper.put(url, model.data);
    }

    updateInfoPartner(id: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.Partner.UPDATE_INFO_PARTNER, id);
        return this.httpHelper.get(url);
    }

    deletePartner(id: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.Partner.DELETE_PARTNER, id);
        return this.httpHelper.delete(url);
    }

    activePartner(model: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.Partner.ACTIVE_PARTNER, model);
        return this.httpHelper.post(url, null);
    }

    deActivePartner(model: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.Partner.DEACTIVE_PARTNER, model);
        return this.httpHelper.post(url, null);
    }

    getPartner(model: any): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.Partner.SEARCH;
        return this.httpHelper.post(url, model);
    }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie';
import { KeyConstants } from '../constants';

@Injectable()
export class TranslateService {

  public data: any = {};
  private lang: any;

  constructor(private http: HttpClient,
    private cookieService: CookieService) {
    this.lang = this.cookieService.get(KeyConstants.LANGUAGE);
  }

  public translate(key: string) {
    if (this.data === null || this.data === undefined) {
      return key;
    }
    return this.data[key] || key;
  }

  public setlang() {
    this.lang = this.cookieService.get(KeyConstants.LANGUAGE);

    return new Promise<{}>((resolve) => {
      const langPath = `assets/i18n/${this.lang || 'vi'}.json`;

      this.http.get<{}>(langPath).subscribe(
        translation => {
          this.data = Object.assign({}, translation || {});
          resolve(this.data);
        },
        () => {
          this.data = {};
          resolve(this.data);
        }
      );
    });
  }

  public use(lang: string): Promise<any> {
    return new Promise<{}>((resolve) => {
      const langPath = `assets/i18n/${lang || 'vi'}.json`;

      this.http.get<{}>(langPath).subscribe(
        translation => {
          this.data = Object.assign({}, translation || {});
          resolve(this.data);
        },
        () => {
          this.data = {};
          resolve(this.data);
        }
      );
    });
  }
}

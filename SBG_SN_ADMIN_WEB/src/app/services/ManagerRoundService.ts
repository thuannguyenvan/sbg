import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHelper } from '../helpers';
import { EnvService } from './EnvService';
import { UrlApiConstants } from '../constants';
import { String } from 'typescript-string-operations';


@Injectable()
export class ManagerRoundService {

    constructor(private httpHelper: HttpHelper, private env: EnvService) {
    }

    getCompetitionInfo(): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.ManagerCompetitions.GET_COMPETITION_INFO;
        return this.httpHelper.get(url);
    }

    getStageByCp(id: string): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.ManagerCompetitions.GET_LIST_STAGE, id);
        return this.httpHelper.get(url);
    }

    createRound(model: any): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.ManagerCompetitions.ADD_ROUND;
        return this.httpHelper.post(url, model);
    }

    updateRound(model: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.ManagerCompetitions.UPDATE_ROUND, model.id);
        return this.httpHelper.put(url, model.data);
    }

    getInfoRound(id: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.ManagerCompetitions.EDIT_ROUND_INFO, id);
        return this.httpHelper.get(url);
    }

    getRounds(model: any): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.ManagerCompetitions.SOCCER_MATCH.GET_ROUNDS;
        return this.httpHelper.post(url, model);
    }

    deleteRound(id: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.ManagerCompetitions.DELETE_ROUND, id);
        return this.httpHelper.delete(url);
    }
}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHelper } from '../helpers';
import { EnvService } from './EnvService';
import { UrlApiConstants } from '../constants';

@Injectable()
export class AuthService {

    constructor(private httpHelper: HttpHelper,
        private env: EnvService) {
    }

    login(model: any): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.Auths.LOGIN;
        return this.httpHelper.post(url, model);
    }

    getLoginInfo(): Observable<any> {
        return this.httpHelper.get(this.env.API_ENDPOINT + UrlApiConstants.Auths.GET_LOGIN_INFO);
    }
}

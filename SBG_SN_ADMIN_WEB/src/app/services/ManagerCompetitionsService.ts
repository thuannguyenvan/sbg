import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHelper } from '../helpers';
import { EnvService } from './EnvService';
import { UrlApiConstants } from '../constants';
import { String } from 'typescript-string-operations';

@Injectable()
export class ManagerCompetitionsService {

    constructor(private httpHelper: HttpHelper, private env: EnvService) {
    }

    getCompetitionInfo(): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.ManagerCompetitions.GET_COMPETITION_INFO;
        return this.httpHelper.get(url);
    }

    getListStage(id: string): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.ManagerCompetitions.GET_LIST_STAGE, id);
        return this.httpHelper.get(url);
    }

    // Start Giải đấu
    getCompetitions(model: any): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.ManagerCompetitions.GET_LIST_COMPETITION;
        return this.httpHelper.post(url, model);
    }

    getTeamByCompetitions(id: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.ManagerCompetitions.GET_TEAM_COMPETITION_ID, id);
        return this.httpHelper.get(url);
    }

    getCompetitionsCreateInfo(): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.ManagerCompetitions.GET_COMPETITION_CREATE_EDIT_INFO;
        return this.httpHelper.get(url);
    }

    getCompetitionsEditInfo(id: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.ManagerCompetitions.GET_COMPETITION_EDIT_INFO, id);
        return this.httpHelper.get(url);
    }

    createCompetition(model: any): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.ManagerCompetitions.CREATE_COMPETITION;
        return this.httpHelper.post(url, model);
    }

    editCompetition(model: any): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.ManagerCompetitions.EDIT_COMPETITION + model.id;
        return this.httpHelper.put(url, model);
    }

    deleteCompetition(id: any): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.ManagerCompetitions.DELETE_COMPETITION + id;
        return this.httpHelper.delete(url);
    }

    getSoccerTeam(model: any): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.ManagerCompetitions.GET_SOCCER_TEAM;
        return this.httpHelper.post(url, model);
    }

    getCompetitionSoccerTeam(id: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.ManagerCompetitions.GET_COMPETITION_SOCCER_TEAM, id);
        return this.httpHelper.get(url);
    }

    updateCompetitionSoccerTeam(model: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.ManagerCompetitions.UPDATE_CONFIG_TEAM, model.id);
        return this.httpHelper.post(url, {soccerCompetitionTeams: model.data});
    }
    // End Giải đấu
}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHelper } from '../helpers';
import { EnvService } from './EnvService';
import { UrlApiConstants } from '../constants';
import { String } from 'typescript-string-operations';

@Injectable()
export class UserService {

    constructor(private httpHelper: HttpHelper, private env: EnvService) {
    }

    getUsersInfo(model: any): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.Users.GET_LIST_USER;
        return this.httpHelper.post(url, model);
    }

    updateRoleUser(model: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.Users.ROLE_USER, model.userId);
        return this.httpHelper.post(url, { roles: model.userRoles });
    }

    getRolesUser(id: string): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.Users.ROLE_USER, id);
        return this.httpHelper.get(url);
    }

    activateUser(model: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.Users.ACTIVE_USER, model.id);
        return this.httpHelper.post(url, model);
    }

    deactivateUser(model: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.Users.DEACTIVE_USER, model.id);
        return this.httpHelper.post(url, model);
    }
}

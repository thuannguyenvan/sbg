import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHelper } from '../helpers';
import { EnvService } from './EnvService';
import { UrlApiConstants } from '../constants';
import { String } from 'typescript-string-operations';

@Injectable()
export class ManagerStandingService {

    constructor(private httpHelper: HttpHelper, private env: EnvService) {
    }

    getCompetitionInfo(): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.ManagerCompetitions.GET_COMPETITION_INFO;
        return this.httpHelper.get(url);
    }

    getListStages(id: string): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.ManagerCompetitions.GET_LIST_STAGE_STANDING, id);
        return this.httpHelper.get(url);
    }

    getTeamInfoByStageId(id: string): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.Standing.MANAGER_STANDING, id);
        return this.httpHelper.get(url);
    }

    deleteStanding(id: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.Standing.DELETE_STANDING, id);
        return this.httpHelper.delete(url);
    }

    createStanding(id: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.Standing.CREATE_STANDING, id);
        return this.httpHelper.post(url, null);
    }

    updateStandingStage(model: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.Standing.UPDATE_MANAGER_STANDING, model.id);
        return this.httpHelper.post(url, {soccerCompetitionStageTeams: model.soccerCompetitionStageTeams});
    }

    updateStanding(model: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.Standing.UPDATE_STANDING, model.id);
        return this.httpHelper.put(url, model.data);
    }

    getInfoStanding(id: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.Standing.GET_UPDATE_STANDING_INFO, id);
        return this.httpHelper.get(url);
    }
}

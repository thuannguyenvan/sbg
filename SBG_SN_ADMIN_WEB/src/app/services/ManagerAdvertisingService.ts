import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHelper } from '../helpers';
import { EnvService } from './EnvService';
import { UrlApiConstants } from '../constants';
import { String } from 'typescript-string-operations';

@Injectable()
export class ManagerAdvertisingService {

    constructor(private httpHelper: HttpHelper, private env: EnvService) {
    }

    getSearchAdvertisingInfo(): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.ManagerAdvertising.SEARCH_INFO_ADVERTISING;
        return this.httpHelper.get(url);
    }

    searchAdvertisings(model: any): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.ManagerAdvertising.SEARCH_ADVERTISING;
        return this.httpHelper.post(url, model);
    }

    getCreateAdvertisingInfo(): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.ManagerAdvertising.CREATE_INFO_ADVERTISING;
        return this.httpHelper.get(url);
    }

    createAdvertising(model: any): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.ManagerAdvertising.CREATE_ADVERTISING;
        return this.httpHelper.post(url, model);
    }

    deleteAdvertising(id: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.ManagerAdvertising.DELETE_ADVERTISING, id);
        return this.httpHelper.delete(url);
    }

    updateAdvertising(model: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.ManagerAdvertising.UPDATE_ADVERTISING, model.id);
        return this.httpHelper.put(url, model.data);
    }

    getUpdateAdvertisingInfo(id: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.ManagerAdvertising.UPDATE_INFO_ADVERTISING, id);
        return this.httpHelper.get(url);
    }

    activateAdvertising(id: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.ManagerAdvertising.ACTIVE_ADVERTISING, id);
        return this.httpHelper.post(url, null);
    }

    deActivateAdvertising(id: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.ManagerAdvertising.DEACTIVE_ADVERTISING, id);
        return this.httpHelper.post(url, null);
    }
}

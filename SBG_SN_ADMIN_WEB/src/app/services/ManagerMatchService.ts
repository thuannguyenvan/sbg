import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHelper } from '../helpers';
import { EnvService } from './EnvService';
import { UrlApiConstants } from '../constants';
import { String } from 'typescript-string-operations';

@Injectable()
export class ManagerMatchService {

    constructor(private httpHelper: HttpHelper, private env: EnvService) {
    }

    // lấy thông tin info tạo trận đấu
    getAddMatchInfo(): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.ManagerMatch.GET_ADD_INFO;
        return this.httpHelper.get(url);
    }

    // lấy danh sách round đấu theo giải đấu id
    getRoundByCompetition(id: string): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.ManagerMatch.GET_ROUND, id);
        return this.httpHelper.get(url);
    }

    // lấy danh sách đội them round id
    getTeamByRound(id: string): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.ManagerMatch.GET_TEAM, id);
        return this.httpHelper.get(url);
    }

    // tạo trận đấu
    createMatch(model: any): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.ManagerMatch.ADD_MATCH;
        return this.httpHelper.post(url, model);
    }

    // lấy thông tin info edit trận đấu
    getEditMatchInfo(id: string): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.ManagerMatch.GET_EDIT_MATCH, id);
        return this.httpHelper.get(url);
    }

    // cập nhập trận đấu
    updateMatch(model: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.ManagerMatch.UPDATE_MATCH, model.id);
        return this.httpHelper.put(url, model.data);
    }

    // search danh sách trận đấu
    getMatchInfoList(model: any): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.Math.GET_MATCH_LIST;
        return this.httpHelper.post(url, model);
    }

    // lấy danh sách round từ giải đấu
    getRoundByCompetitionInfoList(id: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.Math.GET_ROUND_BY_COMPETITION, id);
        return this.httpHelper.get(url);
    }

    // lấy thông tin để search trận đấu
    getSearchInfoMatchList(): Observable<any> {
        const url = this.env.API_ENDPOINT + UrlApiConstants.Math.GET_MATCH_SEARCH_INFO;
        return this.httpHelper.get(url);
    }

    // delete trận đấu
    deleteMatch(id: any): Observable<any> {
        const url = String.Format(this.env.API_ENDPOINT + UrlApiConstants.Math.DELETE_MATCH, id);
        return this.httpHelper.delete(url);
    }
}

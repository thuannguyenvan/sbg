import { Injectable } from '@angular/core';
import {
    HttpEvent,
    HttpRequest,
    HttpHandler,
    HttpInterceptor,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie';
import {
    KeyConstants,
} from '../constants';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private cookieService: CookieService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const accessToken = this.cookieService.get(KeyConstants.TOKEN);
        const headers = req.headers.set('Authorization', 'Bearer ' + accessToken);
        const authReq = req.clone({ headers: headers });
        return next.handle(authReq);
    }
}

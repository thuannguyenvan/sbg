﻿
export interface AppState {
    shareds: {
        sessionUserInfo: any,
        activeRouterOutlet: any,
        error: any,
    };
    validate: {
        validateModel: any,
        error: any,
    };
    auths: {
        login: any,
        error: any,
        getLoginInfo: any,
    };
    users: {
        getUsersInfo: any,
        getRoleUser: any,
        updateRoleUser: any,
        error: any,
        activateUser: any,
        deactivateUser: any,
    };
    standing: {
        getCompetitionsInfo: any,
        getListStage: any,
        updateStandingStage: any,
        deleteStanding: any,
        updateStanding: any,
        getInfoStanding: any,
        createStanding: any,
        error: any,
        getTeamInfoByStageId: any,
    };
    competitions: {
        getCompetitionsCreateInfo: any,
        getCompetitionsEditInfo: any,
        getCompetitionsInfo: any,
        getListCompetitionsInfo: any,
        getCompetitionsTeamInfo: any,
        createCompetition: any,
        editCompetition: any,
        deleteCompetition: any,
        getSoccerTeam: any,
        getCompetitionsSoccerTeam: any,
        updateConfigTeam: any,
        error: any,
    };
    managerRound: {
        getCompetitionsInfo: any,
        GetStageByCp: any,
        createRound: any,
        updateRound: any,
        getRoundInfo: any,
        deleteRound: any,
        getSoccerMatchs: any,
        getRounds: any,
        error: any,
    };
    soccerMatch: {
        createMatch: any,
        getCreateMatchinfo: any,
        getRoundByCp: any,
        getTeamByRound: any,
        getEditInfo: any,
        updateMatch: any,
        getMatchInfoList: any,
        getRoundByCompetitionInfoList: any,
        getSearchInfoMatchList: any,
        deleteMatch: any,
        error: any,
    };
    managerLivestream: {
        getCompetitionsInfo: any,
        getRound: any,
        getMatch: any,
        Search: any,
        getSearchInfo: any,
        getInfo: any,
        update: any,
        create: any,
        delete: any,
        getCreateInfo: any,
        getEditInfo: any,
        error: any,
    };
    managerAdvertisings: {
        search: any,
        getSearchInfo: any,
        getEditInfo: any,
        getCreateInfo: any,
        update: any,
        create: any,
        delete: any,
        activate: any,
        deactivate: any,
        error: any,
    };
    managerAdvertisingsConfig: {
        search: any,
        getSearchInfo: any,
        searchPopUp: any,
        getSearchPopUpInfo: any,
        getEditInfo: any,
        getCreateInfo: any,
        update: any,
        create: any,
        delete: any,
        activate: any,
        deactivate: any,
        error: any,
    };
    managerPartner: {
        Search: any,
        getEditInfo: any,
        update: any,
        create: any,
        delete: any,
        active: any,
        deActive: any,
        error: any,
    };
}

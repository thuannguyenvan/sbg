﻿import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
    SharedModule,
} from '../../modules';

import {
    DashboardComponent,
} from './components';
import { CaseInsensitiveMatcher } from '../../helpers';

const routes: Routes = [
    { matcher: CaseInsensitiveMatcher('dashboard'), component: DashboardComponent },
];

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        FormsModule,
        BrowserModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
    ],
    declarations: [
        DashboardComponent,
    ]
})
export class DashboardModule { }

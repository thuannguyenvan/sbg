﻿import {
    Component,
    OnInit,
    OnDestroy,
    Inject,
} from '@angular/core';
import { Store } from '@ngrx/store';
import {
    WebStorageService,
    SESSION_STORAGE
} from 'angular-webstorage-service';
import { Subscription } from 'rxjs';
import { AppState } from '../../../../AppState';
import { ToastrHelper } from '../../../../helpers';
import {
    InfoResult,
    SessionUserInfo
} from '../../../../models';
import { KeyConstants } from '../../../../constants';

@Component({
    selector: 'dashboard',
    templateUrl: './index.html',
    styleUrls: ['./index.scss'],
})

export class DashboardComponent implements OnInit, OnDestroy {
    private subscriptions: Subscription[] = [];
    public userInfo: SessionUserInfo = new SessionUserInfo();
    constructor(
        private store: Store<AppState>,
        public toastr: ToastrHelper,
        @Inject(SESSION_STORAGE) private storage: WebStorageService,
    ) {
    }

    ngOnInit() {
        this.onSubscriptionsSuccess();
        this.onSubscribeError();
        this.getLoginInfo();
    }

    onSubscriptionsSuccess() {
        this.subscriptions.push(this.store.select<InfoResult<SessionUserInfo>>(state => state.auths.getLoginInfo)
            .subscribe((result) => {
                if (result !== undefined) {
                    if (result.data !== undefined && result.data !== null) {
                        this.userInfo = result.data;
                    }
                }
            }));
    }

    onSubscribeError() {
    }

    getLoginInfo() {
        const sessionUserInfoString = this.storage.get(KeyConstants.USER_INFO);

        if (sessionUserInfoString !== null && sessionUserInfoString !== undefined && sessionUserInfoString !== '') {
            const sessionUserInfo = JSON.parse(sessionUserInfoString);
            if (sessionUserInfo !== null && sessionUserInfo !== undefined) {
                if (sessionUserInfo === null || sessionUserInfo === undefined || sessionUserInfo === null) {
                    this.userInfo = new SessionUserInfo();
                } else {
                    this.userInfo = new SessionUserInfo(sessionUserInfo);
                }
            }
        }
    }

    onDestroy() {
    }

    ngOnDestroy() {
        this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
        this.onDestroy();
    }
}

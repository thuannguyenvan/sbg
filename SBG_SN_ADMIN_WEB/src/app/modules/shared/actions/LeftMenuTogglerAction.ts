﻿import { Action } from '@ngrx/store';

export class LeftMenuTogglerAction implements Action {
    public static TYPE = 'LEFT_MENU_TOGGLER_ACTION';
    readonly type: string = LeftMenuTogglerAction.TYPE;
    constructor(public payload: any = null) { }
}

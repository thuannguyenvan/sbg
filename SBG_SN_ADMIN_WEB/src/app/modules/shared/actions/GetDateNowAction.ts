﻿import { Action } from '@ngrx/store';

export class GetDateNowAction implements Action {
    public static TYPE = 'GET_DATE_NOW_ACTION';
    readonly type: string = GetDateNowAction.TYPE;
    constructor(public payload: any = null) { }
}

export class SuccessGetDateNowAction implements Action {
    public static TYPE = 'SUCCESS_GET_DATE_NOW_ACTION';
    readonly type: string = SuccessGetDateNowAction.TYPE;
    constructor(public payload: any) { }
}

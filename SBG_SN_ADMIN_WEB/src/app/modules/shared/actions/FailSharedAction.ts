﻿import { Action } from '@ngrx/store';

export class FailSharedAction implements Action {
    public static TYPE = 'FAIL_SHARED_ACTION';
    readonly type: string = FailSharedAction.TYPE;
    constructor(public payload: any) { }
}

import { Action } from '@ngrx/store';

export class SuccessActiveRouterOutletAction implements Action {
    public static TYPE = 'SUCCESS_ACTIVE_ROUTER_OUTLET_ACTION';
    readonly type: string = SuccessActiveRouterOutletAction.TYPE;
    constructor(public payload: any) { }
}

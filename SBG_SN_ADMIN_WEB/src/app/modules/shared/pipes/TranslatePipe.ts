import {
  Pipe,
  PipeTransform
} from '@angular/core';
import { TranslateService } from '../../../services';

@Pipe({
  name: 'translate',
  pure: false
})
export class TranslatePipe implements PipeTransform {

  constructor(private translate: TranslateService) { }

  transform(key: any, ...args: string[]): any {

    if (this.translate.data === null || this.translate.data === undefined) {
      return key;
    } else {
      const tran = this.translate.data[key] || key;
      if (args.length > 0) {
        const message = tran.replace(/{([^}]+)}/g, (_match: any, index: number) => {
          return (args[index] === undefined || args[index] === null) ? _match : args[index];
        });
        return message;
      } else {
        return tran;
      }
    }
  }
}

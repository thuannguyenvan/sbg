﻿import {
    InfoResult,
} from '../../../models';

import {
    SuccessActiveRouterOutletAction,
} from '../actions';

export function ShareReducer(state: any = {}, action) {
    switch (action.type) {
        case SuccessActiveRouterOutletAction.TYPE:
            state.activeRouterOutlet = action.payload === undefined || action.payload === null ?
                action.payload
                : new InfoResult<boolean>(action.payload);
            return state;

        default: {
            return state;
        }
    }
}

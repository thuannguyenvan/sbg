import {
    NgModule,
} from '@angular/core';
import {
    CommonModule,
} from '@angular/common';
import {
    RouterModule,
} from '@angular/router';
import {
    BrowserModule,
} from '@angular/platform-browser';
import {
    BrowserAnimationsModule,
} from '@angular/platform-browser/animations';
import {
    StoreModule,
} from '@ngrx/store';
import {
    EffectsModule,
} from '@ngrx/effects';
import {
    FlexLayoutModule,
} from '@angular/flex-layout';
import {
    ToastrModule,
} from 'ngx-toastr';
import {
    FormsModule,
    ReactiveFormsModule,
} from '@angular/forms';
import {
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    MAT_DATE_LOCALE,
    MatFormFieldModule,
} from '@angular/material';
import {
    ShareReducer,
} from './reducers';
import {
    SharedEffects
} from './effects';
import {
    HeaderComponent,
    FooterComponent,
    MasterPageComponent,
    ShowErrorsComponent,
    PaginationComponent,
    AutocompleteComponent,
    DateTimePickerModule,
} from './components';
import {
    DisabledControlDirective,
    NumberOnlyDirective,
    MinDirective,
    MaxDirective,
} from './directives';
import {
    TranslatePipe,
} from './pipes';
import {
    DateTimeHelper,
} from '../../helpers/DateTimeHelper';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';

const MODULE_SHARED_COMPONENTS = [
    HeaderComponent,
    FooterComponent,
    MasterPageComponent,
    DisabledControlDirective,
    NumberOnlyDirective,
    MinDirective,
    ShowErrorsComponent,
    MaxDirective,
    TranslatePipe,
    PaginationComponent,
    AutocompleteComponent
];

@NgModule({
    imports: [
        DateTimeHelper,
        FormsModule,
        CommonModule,
        RouterModule,
        BrowserModule,
        MatFormFieldModule,
        DateTimeHelper,
        MatAutocompleteModule,
        MatBadgeModule,
        MatBottomSheetModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatCardModule,
        MatCheckboxModule,
        MatChipsModule,
        MatDatepickerModule,
        MatDialogModule,
        MatDividerModule,
        MatExpansionModule,
        MatGridListModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        MatMenuModule,
        MatNativeDateModule,
        MatPaginatorModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        MatRadioModule,
        MatRippleModule,
        MatSelectModule,
        MatSidenavModule,
        MatSliderModule,
        MatSlideToggleModule,
        MatSnackBarModule,
        MatSortModule,
        MatStepperModule,
        MatTableModule,
        MatTabsModule,
        MatToolbarModule,
        MatTooltipModule,
        MatTreeModule,
        MatFormFieldModule,
        FlexLayoutModule,
        EffectsModule.forFeature([
            SharedEffects,
        ]),
        StoreModule.forFeature('shareds', ShareReducer),
        ReactiveFormsModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot({
            closeButton: true,
            progressBar: true,
            timeOut: 2000,
            positionClass: 'toast-top-right',
            preventDuplicates: true,
            enableHtml: true,
        }),
        NgxMaterialTimepickerModule.forRoot(),
        DateTimePickerModule,
    ],
    exports: [
        MatAutocompleteModule,
        MatBadgeModule,
        MatBottomSheetModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatCardModule,
        MatCheckboxModule,
        MatChipsModule,
        MatDatepickerModule,
        MatDialogModule,
        MatDividerModule,
        MatExpansionModule,
        MatGridListModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        MatMenuModule,
        MatNativeDateModule,
        MatPaginatorModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        MatRadioModule,
        MatRippleModule,
        MatSelectModule,
        MatSidenavModule,
        MatSliderModule,
        MatSlideToggleModule,
        MatSnackBarModule,
        MatSortModule,
        MatStepperModule,
        MatTableModule,
        MatTabsModule,
        MatToolbarModule,
        MatTooltipModule,
        MatFormFieldModule,
        MatTreeModule,
        DateTimeHelper,
        MatAutocompleteModule,
        MatBadgeModule,
        MatBottomSheetModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatCardModule,
        MatCheckboxModule,
        MatChipsModule,
        MatDatepickerModule,
        MatDialogModule,
        MatDividerModule,
        MatExpansionModule,
        MatGridListModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        MatMenuModule,
        MatNativeDateModule,
        MatPaginatorModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        MatRadioModule,
        MatRippleModule,
        MatSelectModule,
        MatSidenavModule,
        MatSliderModule,
        MatSlideToggleModule,
        MatSnackBarModule,
        MatSortModule,
        MatStepperModule,
        MatTableModule,
        MatTabsModule,
        MatToolbarModule,
        MatTooltipModule,
        MatTreeModule,
        MatFormFieldModule,
        FlexLayoutModule,
        NgxMaterialTimepickerModule,
        DateTimePickerModule,
        MODULE_SHARED_COMPONENTS,
    ],
    declarations: [
        MODULE_SHARED_COMPONENTS,
    ],
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'en-GB' },
    ]
})
export class SharedModule { }

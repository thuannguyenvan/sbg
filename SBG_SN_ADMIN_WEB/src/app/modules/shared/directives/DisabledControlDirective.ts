﻿import { NgControl } from '@angular/forms';
import { Directive, Input } from '@angular/core';
@Directive({
    selector: '[appDisabledControl]'
})
export class DisabledControlDirective {

    @Input() set appDisabledControl(condition: boolean) {
        const action = condition ? 'disable' : 'enable';
        this.ngControl.control[action]();
    }

    constructor(private ngControl: NgControl) {
    }
}

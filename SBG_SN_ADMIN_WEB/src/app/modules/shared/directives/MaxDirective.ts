﻿import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { Validator, AbstractControl, ValidatorFn, FormGroup, ValidationErrors, NG_VALIDATORS } from '@angular/forms';

@Directive({
    selector: '[appMax]',
    providers: [{
        provide: NG_VALIDATORS, useExisting: MaxDirective,
        multi: true
    }]
})
export class MaxDirective implements Validator {

    @Input('appMax') appMax: Number;

    validate(control: AbstractControl): { [key: string]: any } | null {
        return this.appMax ? this.maxValidator(control)
            : null;
    }

     maxValidator(control: AbstractControl): ValidationErrors {
        const appMax = this.appMax;
         return appMax && control && Number(appMax) < Number(control.value) ? {
             appMax: {
                 valid: false,
                 max: this.appMax
             }
         } : null;
    }
}


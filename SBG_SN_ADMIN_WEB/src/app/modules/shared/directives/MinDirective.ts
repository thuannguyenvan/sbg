﻿import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { Validator, AbstractControl, ValidatorFn, FormGroup, ValidationErrors, NG_VALIDATORS } from '@angular/forms';

@Directive({
    selector: '[appMin]',
    providers: [{
        provide: NG_VALIDATORS, useExisting: MinDirective,
        multi: true
    }]
})
export class MinDirective implements Validator {

    @Input('appMin') appMin: Number;

    validate(control: AbstractControl): { [key: string]: any } | null {
        return this.appMin ? this.minValidator(control)
            : null;
    }

    minValidator(control: AbstractControl): ValidationErrors {
        const appMin = this.appMin;
        if (control.value === '' || control.value === undefined || control.value === null) {
            return null;
        }
        return appMin && control && Number(appMin) > Number(control.value) ? {
            appMin: {
                valid: false,
                min: this.appMin
            }
        } : null;
    }
}

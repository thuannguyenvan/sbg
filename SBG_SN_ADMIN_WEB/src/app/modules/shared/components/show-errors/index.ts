﻿import { Component, Input } from '@angular/core';
import { AbstractControlDirective, AbstractControl } from '@angular/forms';
import {
    TranslateService
} from '../../../../services';

@Component({
    selector: 'app-show-errors',
    templateUrl: './errors.html',
    styleUrls: ['./errors.scss'],
})
export class ShowErrorsComponent {

    private _displayName = '';
    private _patternDisplay = '';
    private _compareDisplay = '';
    private _errorMessage = '';

    @Input()
    private control: AbstractControlDirective | AbstractControl;
    constructor(
        private translate: TranslateService,
    ) {

    }

    getMessageValidate(format: string, params: any) {
        if (this.translate.translate(format) === undefined ||
            this.translate.translate(format) === null ||
            this.translate.translate(format) === '') {
            return this.translate.translate(format);
        }
        const message = this.translate.translate(format).replace(/{(!?)([^}]+)}/g, (_$0: any, _isSecure: any, name: string | number) => {
            return params[name];
        });
        return message;
    }

    @Input()
    set displayName(displayName: string) {
        this._displayName = (displayName && displayName.trim()) || '';
    }
    get displayName(): string { return this._displayName; }

    @Input()
    set patternDisplay(patternDisplay: string) {
        this._patternDisplay = (patternDisplay && patternDisplay.trim()) || '';
    }
    get patternDisplay(): string { return this._patternDisplay; }

    @Input()
    set errorMessage(errorMessage: string) {
        this._errorMessage = (errorMessage && errorMessage.trim()) || '';
    }
    get errorMessage(): string { return this._errorMessage; }

    @Input()
    set compareDisplay(compareDisplay: string) {
        this._compareDisplay = (compareDisplay && compareDisplay.trim()) || '';
    }
    get compareDisplay(): string { return this._compareDisplay; }

    shouldShowErrors(): boolean {
        return this.control &&
            this.control.errors &&
            (this.control.dirty || this.control.touched);
    }

    listOfErrors(): string[] {
        return Object.keys(this.control.errors)
            .map(field => this.getMessage(field, this.control.errors[field]));
    }

    private getMessage(type: string, params: any) {
        if (type === 'pattern' && this.patternDisplay !== '') {
            return this.patternDisplay;
        }
        if (type === 'compare') {
            return this.compareDisplay;
        }
        if (type === 'validateServer') {
            return this.errorMessage;
        }
        if (typeof params !== 'object') {
            params = {};
        }
        params.displayName = this.displayName;
        return this.getMessageValidate('VALIDATION_' + type.toUpperCase(), params);
    }

}

import {
    Component,
    ChangeDetectionStrategy,
    Input,
    ChangeDetectorRef,
    Output,
    EventEmitter,
    forwardRef,
    Inject,
} from '@angular/core';
import {
    ControlValueAccessor,
    NG_VALUE_ACCESSOR,
    FormControl,
} from '@angular/forms';
import {
    MatAutocompleteModule,
    MatInputModule,
} from '@angular/material';
import { noop } from 'rxjs';
import { DOCUMENT } from '@angular/common';

export const AUTO_COMPLETE_CONTROL_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    /* tslint:disable */
    useExisting: forwardRef(() => AutocompleteComponent),
    multi: true
};

@Component({
    selector: 'app-autocomplete[ngModel]',
    templateUrl: './index.html',
    styleUrls: ['./index.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        MatAutocompleteModule,
        MatInputModule,
        AUTO_COMPLETE_CONTROL_VALUE_ACCESSOR
    ],
})
export class AutocompleteComponent implements ControlValueAccessor {

    @Input() placeholder: string;
    @Input() dataSource: Array<any>;
    @Input() displayField: string;
    @Input() selectField: string;
    @Input() required: boolean;
    @Input() disabled = false;
    @Input() formControl: FormControl;
    @Output() selectionChange = new EventEmitter();
    @Output() focus = new EventEmitter();
    public data: Array<any>;
    public model: any;

    constructor(
        private changeDetectorRef: ChangeDetectorRef,
        @Inject(DOCUMENT) private document: Document
    ) {
    }

    convertData(data: Array<any>) {
        let result: Array<any> = [];

        if (data === undefined || data === null || data.length === 0) {
            result = [];
        } else {
            data.forEach(item => {
                result.push(
                    {
                        title: item[this.displayField],
                        value: item[this.selectField],
                    });
            });
        }
        return result;
    }

    display(cp?: any): string {
        return cp ? cp.title : '';
    }

    writeValue(value: any): void {
        const data = this.convertData(this.dataSource);
        if (typeof value !== 'object') {
            this.model = data.find(x => x.value === value);
        }
        this.changeDetectorRef.detectChanges();
    }

    // tslint:disable-next-line:member-ordering
    onTouchedCallback: () => void = noop;
    // tslint:disable-next-line:member-ordering
    onChangeCallback: (_: any) => void = noop;

    // From ControlValueAccessor interface
    registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    // From ControlValueAccessor interface
    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }

    // Set touched on blur
    onBlur() {
        this.onTouchedCallback();
    }

    onSelectionChange(event: any, model: any) {
        if (event.isUserInput) {
            this.model = model;
            this.onChangeCallback(model.value);
            const result = this.dataSource.find(x => x[this.selectField] === this.model.value);
            this.selectionChange.emit(result);
        }
    }

    onFocus(event: any) {
        this.focus.emit(event);
    }

    onClick() {
        this.data = this.convertData(this.dataSource);
        setTimeout(() => {
            const matAutocompletepanel = this.document.getElementsByClassName('mat-autocomplete-panel');
            if (matAutocompletepanel === undefined 
                || matAutocompletepanel === null 
                || matAutocompletepanel.length === 0) {
                
                return;
            }
            (matAutocompletepanel[0] as HTMLElement).classList.add('show');
            this.changeDetectorRef.detectChanges();
        }, 1);
    }

    onChangeText() {
        setTimeout(() => {
            this.data = this.filter(this.model);
            this.changeDetectorRef.detectChanges();
        }, 1);
    }

    filter(key: string): Array<any> {
        const data = this.convertData(this.dataSource);
        if (key === undefined || key === null || key === '' || (typeof key) != 'string') {
            return data;
        }
        const filterValue = this.removeUnicode(key);
        return data.filter(option => this.removeUnicode(option.title).includes(filterValue));
    }

    removeUnicode(str: string) {
        if (str === null || str === undefined) {
            return '';
        }
        str = str.trim().toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
        str = str.replace(/đ/g, 'd');
        return str;
    }

    setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }
}

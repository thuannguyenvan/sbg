﻿import {
    Component,
    OnInit,
    Inject,
    OnDestroy,
    ChangeDetectionStrategy
} from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../AppState';

@Component({
    selector: 'app-footer',
    templateUrl: './index.html',
    styleUrls: ['./index.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterComponent implements OnInit, OnDestroy {
    private subscriptions: Subscription[] = [];

    constructor(private store: Store<AppState>) {
    }

    ngOnInit() {
    }

    ngOnDestroy() {
        this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
    }
}

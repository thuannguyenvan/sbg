import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild,
  Inject,
} from '@angular/core';
import { Router } from '@angular/router';
import {
  ObservableMedia,
  MediaChange
} from '@angular/flex-layout';
import { MatSidenav } from '@angular/material';
import { Subscription } from 'rxjs';
import {
  SessionUserInfo,
  InfoResult,
  MenuInfo,
  LangModel
} from '../../../../models';
import {
  SESSION_STORAGE,
  WebStorageService
} from 'angular-webstorage-service';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../AppState';
import {
  KeyConstants,
  TranslateKeyConstants,
  RouterConstants
} from '../../../../constants';
import { CookieService } from 'ngx-cookie';

@Component({
  selector: 'app-master-page',
  templateUrl: './index.html',
  styleUrls: ['./index.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class MasterPageComponent implements OnInit {

  public subscriptions: Subscription[] = [];
  public expandHeight = '42px';
  public collapseHeight = '42px';
  public displayMode = 'flat';
  public watcher: Subscription;
  @ViewChild('sidenav') private sidenav: MatSidenav;
  public model: SessionUserInfo = new SessionUserInfo();
  public translateKey: any;
  public lang: LangModel = new LangModel();

  constructor(
    private media: ObservableMedia,
    private store: Store<AppState>,
    private router: Router,
    private cookieService: CookieService,
    @Inject(SESSION_STORAGE) private storage: WebStorageService,
  ) {
    this.translateKey = TranslateKeyConstants;
  }

  ngOnInit() {
    let langActive = this.cookieService.get(KeyConstants.LANGUAGE);
    if (langActive === null || langActive === undefined || langActive === '') {
      langActive = 'vi';
    }

    this.watcher = this.media.subscribe((change: MediaChange) => {

      if (change.mqAlias === 'sm' || change.mqAlias === 'xs') {

        this.sidenav.opened = false;
        this.sidenav.mode = 'over';
        this.sidenav.close();

      } else {

        this.sidenav.open();
        this.sidenav.mode = 'side';
      }

    });

    this.subscriptions.push(this.store.select<InfoResult<SessionUserInfo>>(state => state.shareds.sessionUserInfo)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result === null || result.data === undefined || result.data === null) {
            this.model = new SessionUserInfo();
          } else {
            this.model = result.data;
          }

          this.lang = new LangModel({
            active: langActive,
            langs: this.model.supportedLanguage
          });
        }
      }));

    const sessionUserInfoString = this.storage.get(KeyConstants.USER_INFO);

    if (sessionUserInfoString !== null && sessionUserInfoString !== undefined && sessionUserInfoString !== '') {
      const sessionUserInfo = JSON.parse(sessionUserInfoString);
      if (sessionUserInfo !== null && sessionUserInfo !== undefined) {
        if (sessionUserInfo === null || sessionUserInfo === undefined || sessionUserInfo === null) {
          this.model = new SessionUserInfo();
        } else {
          this.model = new SessionUserInfo(sessionUserInfo);
        }
        this.onActiveItem();
        this.lang = new LangModel({
          active: langActive,
          langs: this.model.supportedLanguage
        });
      }
    }
  }

  onGoLink(item: MenuInfo) {
    this.storage.set(KeyConstants.ACTIVE_MENU, item);
    if (item.url !== null && item.url !== undefined && item.url !== '#') {
      this.router.navigate([item.url]);
      this.onActiveItem();
    }
    return false;
  }

  onLogout() {
    this.cookieService.remove(KeyConstants.TOKEN);
    this.storage.remove(KeyConstants.USER_INFO);
    setTimeout(() => {
      this.router.navigate([RouterConstants.auths.login]);
    }, 1000);
    return false;
  }

  onSelectLang(lang: any) {
    this.cookieService.put(KeyConstants.LANGUAGE, lang.value);
    window.location.reload();
    return false;
  }

  onActiveItem() {
    const activeMenu = this.storage.get(KeyConstants.ACTIVE_MENU);
    if (activeMenu !== null && activeMenu !== undefined &&
      this.model.cmsLeftMenus !== null && this.model.cmsLeftMenus !== undefined) {
      const menu = this.model.cmsLeftMenus.filter(x => x.id === activeMenu.parentId);
      if (menu !== null && menu !== undefined && menu.length > 0) {
        menu[0].expanded = true;
        const subMenu = (menu[0] as any).menus.filter(x => x.id === activeMenu.id);
        if (subMenu !== null && subMenu !== undefined && subMenu.length > 0) {
          subMenu[0].expanded = true;
        }
      }
    }
  }
}

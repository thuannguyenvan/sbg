import {
    Component,
    Input,
    Output,
    EventEmitter,
    OnInit,
    ChangeDetectionStrategy
} from '@angular/core';
import {
    NgModel,
    ControlValueAccessor
} from '@angular/forms';
import {
    ThemePalette,
    PageEvent
} from '@angular/material';
import { String } from 'typescript-string-operations';
import { TranslateService } from '../../../../services';
import { TranslateKeyConstants } from '../../../../constants';

@Component({
    selector: 'app-pagination[ngModel]',
    templateUrl: './index.html',
    styleUrls: ['./index.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaginationComponent implements ControlValueAccessor, OnInit {

    @Input() color: ThemePalette;
    @Input() disabled: boolean;
    @Input() hidePageSize: boolean;
    @Input() length: number;
    @Input() pageIndex: number;
    @Input() pageSize: number;
    @Input() pageSizeOptions: number[];
    @Input() showFirstLastButtons: boolean;
    @Output() pageChanged = new EventEmitter();
    cPage: number;
    public onChange: Function;
    public onTouched: Function;
    public seletedPage: number;
    public nextItem: number;
    public previousItem: number;
    public nextItemValid: boolean;
    public previousItemValid: boolean;
    public totalPage: number;
    public _intl: any;
    public _displayedPageSizeOptions: number[];
    public pageEvent: PageEvent;

    constructor(
        private pageChangedNgModel: NgModel,
        tran: TranslateService,
    ) {
        this.pageChangedNgModel.valueAccessor = this;
        this._intl = {
            itemsPerPageLabel: tran.translate(TranslateKeyConstants.PAGING.ITEMS_PER_PAGE),
            firstPageLabel: tran.translate(TranslateKeyConstants.PAGING.FIRST_PAGE),
            lastPageLabel: tran.translate(TranslateKeyConstants.PAGING.LAST_PAGE),
            nextPageLabel: tran.translate(TranslateKeyConstants.PAGING.NEXT_PAGE),
            previousPageLabel: tran.translate(TranslateKeyConstants.PAGING.PREVIOUS_PAGE),
            getRangeLabel: (page: number, pageSize: number, length: number) => {
                if (length === 0 || pageSize === 0) {
                    return tran.translate(TranslateKeyConstants.PAGING.EMPTY);
                }
                length = Math.max(length, 0);
                const startIndex = (page - 1) * pageSize;
                const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
                return String.Format(tran.translate(TranslateKeyConstants.PAGING.LABEL_INFO), startIndex + 1, endIndex, length);
            }
        };
    }

    ngOnInit() {
        this._displayedPageSizeOptions = this.pageSizeOptions;
        this.pageSize = this._displayedPageSizeOptions[0];
        this.showFirstLastButtons = true;
    }

    _nextButtonsDisabled() {
        const startIndex = (this.pageIndex - 1) * this.pageSize;
        const endIndex = startIndex < length ? Math.min(startIndex + this.pageSize, length) : startIndex + this.pageSize;
        if (endIndex >= this.length) {
            return true;
        }
        return false;
    }

    _previousButtonsDisabled() {
        if (this.pageIndex === 1) {
            return true;
        }
        return false;
    }

    nextPage() {
        this.cPage = this.pageIndex + 1;
        this.pageChangedNgModel.viewToModelUpdate(this.cPage);
        this.pageChageListner();
    }

    previousPage() {
        this.cPage = this.pageIndex - 1;
        this.pageChangedNgModel.viewToModelUpdate(this.cPage);
        this.pageChageListner();
    }

    firstPage() {
        this.cPage = 1;
        this.pageChangedNgModel.viewToModelUpdate(this.cPage);
        this.pageChageListner();
    }

    lastPage() {
        this.cPage = Math.ceil(this.length / this.pageSize);
        this.pageChangedNgModel.viewToModelUpdate(this.cPage);
        this.pageChageListner();
    }

    writeValue(value: string): void {
    }

    registerOnChange(fn: (_: any) => {}): void {
    }

    registerOnTouched(fn: (_: any) => {}): void {
    }

    pageChageListner() {
        this.pageChanged.emit({
            pageIndex: this.cPage,
            pageSize: this.pageSize,
        });
    }

    _changePageSize($event: number) {
        this.pageSize = $event;
        this.cPage = 1;
        this.pageChageListner();
    }
}

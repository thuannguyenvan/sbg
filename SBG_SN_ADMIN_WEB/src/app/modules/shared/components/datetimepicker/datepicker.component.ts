﻿import {
    Component,
    OnInit,
    EventEmitter,
    Input,
    Output,
    ChangeDetectionStrategy,
    forwardRef,
} from '@angular/core';
import { DateRange } from './model';
import {
    ControlValueAccessor,
    NG_VALUE_ACCESSOR,
    FormControl,
    NG_VALIDATORS,
    Validator,
} from '@angular/forms';
import { Settings } from './interface';
import * as moment from 'moment';
import { noop } from 'rxjs';
/* tslint:disable */
export const DATEPICKER_CONTROL_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => DatePickerComponent),
    multi: true
};

export const DATEPICKER_CONTROL_VALIDATORS: any = {
    provide: NG_VALIDATORS,
    useExisting: forwardRef(() => DatePickerComponent),
    multi: true,
}
/* tslint:enable */
@Component({
    selector: 'app-date-picker[ngModel]',
    templateUrl: './datepicker.component.html',
    styleUrls: [
        './datepicker.component.scss',
        './rangepicker.scss'
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        DATEPICKER_CONTROL_VALUE_ACCESSOR,
        DATEPICKER_CONTROL_VALIDATORS
    ]
})

export class DatePickerComponent implements ControlValueAccessor, OnInit, Validator {
    @Input()
    settings: Settings;
    @Input()
    appDisabledControl: boolean;
    @Input()
    enableWeekdays: Array<number>;
    @Input()
    placeholder: string;
    @Input()
    required: boolean;
    @Input() formControl: FormControl;
    @Output()
    onDateSelect: EventEmitter<string> = new EventEmitter<string>();
    @Output()
    focus: EventEmitter<any> = new EventEmitter<any>();
    selectedDate: String;
    date: Date;
    val: string;
    dateRange: DateRange = new DateRange();
    popover: Boolean = false;
    cal_days_in_month: Array<any> = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    timeViewDate: Date = new Date(this.date);
    hourValue: number;
    toHourValue: number;
    minValue: number;
    toMinValue: number;
    timeViewMeridian: string;
    toTimeViewMeridian: string;
    timeView: boolean;
    yearView: Boolean;
    yearsList: Array<any> = [];
    monthDays: Array<any> = [];
    toMonthDays: Array<any> = [];
    monthsView: boolean;
    today: Date = new Date();
    leftDate: Date = new Date();
    rightDate: Date = new Date();
    rangeSelected: number;
    defaultSettings: Settings = {
        defaultOpen: false,
        bigBanner: true,
        timePicker: false,
        format: 'DD/MM/YYYY HH:mm',
        formatValidate: 'D/M/YYYY HH:mm',
        cal_days_labels: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
        cal_full_days_lables: ['Chủ nhật', 'Thứ 2', 'Thứ 3', 'Thứ 4', 'Thứ 5', 'Thứ 6', 'Thứ 7'],
        cal_months_labels: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4',
            'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9',
            'Tháng 10', 'Tháng 11', 'Tháng 12'],
        cal_months_labels_short: ['T1', 'T2', 'T3', 'T4',
            'T5', 'T6', 'T7', 'T8', 'T9',
            'T10', 'T11', 'T12'],
        closeOnSelect: true,
        rangepicker: false
    };
    private parseError: boolean;
    private showError: boolean;
    constructor() {
    }
    ngOnInit() {
        this.hourValue = 0;
        this.toHourValue = 0;
        this.minValue = 0;
        this.toMinValue = 0;
        this.timeViewMeridian = '';
        this.toTimeViewMeridian = '';
        this.timeView = false;
        this.yearView = false;
        this.monthsView = false;
        this.rangeSelected = 0;
        this.settings = Object.assign(this.defaultSettings, this.settings);
        if (this.settings.defaultOpen) {
            this.popover = true;
        }
    }
    onClickDate() {
        this.writeValue(this.val);
        this.popover = !this.popover;
    }
    getCurrentLabelDayOfWeek() {
        if (this.date === null || this.date === undefined) {
            return null;
        }
        return this.settings.cal_full_days_lables[this.date.getDay()];
    }
    getCurrentLabelMonth() {
        if (this.date === null || this.date === undefined) {
            return null;
        }
        return this.settings.cal_months_labels[this.date.getMonth()];
    }
    public validate(c: FormControl) {
        return (!this.parseError) ? null : {
            DATETIMEVALID: {
                valid: false,
            },
        };
    }
    /* tslint:enable */
    writeValue(value: any) {
        if (value !== undefined && value !== null && value !== '') {
            if (!this.settings.rangepicker) {
                this.val = value;
                this.initDate(value);
                this.monthDays = this.generateDays(this.date);
            } else {
                this.initDateRange(value);
                if (this.dateRange.startDate.getMonth() === this.dateRange.endDate.getMonth() &&
                    this.dateRange.startDate.getFullYear() === this.dateRange.endDate.getFullYear()) {
                    this.leftDate = new Date(this.dateRange.startDate);
                    const tempDate = new Date(this.dateRange.startDate);
                    tempDate.setMonth(tempDate.getMonth() + 1);
                    tempDate.setDate(1);
                    this.rightDate = new Date(tempDate);
                    this.monthDays = this.generateDays(this.leftDate);
                    this.toMonthDays = this.generateDays(this.rightDate);
                } else {
                    this.leftDate = new Date(this.dateRange.startDate);
                    this.rightDate = new Date(this.dateRange.endDate);
                    this.monthDays = this.generateDays(this.leftDate);
                    this.toMonthDays = this.generateDays(this.rightDate);
                }
            }

        } else {
            this.val = null;
            this.date = new Date();
            this.monthDays = this.generateDays(this.date);
            this.hourValue = this.date.getHours();
            this.minValue = this.date.getMinutes();
        }
    }
    // tslint:disable-next-line:member-ordering
    onTouchedCallback: () => void = noop;
    // tslint:disable-next-line:member-ordering
    onChangeCallback: (_: any) => void = noop;
    // From ControlValueAccessor interface
    registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }
    // From ControlValueAccessor interface
    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }
    initDate(val: string) {
        this.date = moment(val, this.settings.format).toDate();
        this.hourValue = this.date.getHours();
        this.minValue = this.date.getMinutes();
    }
    initDateRange(val: DateRange) {
        this.dateRange.startDate = new Date(val.startDate);
        this.dateRange.endDate = new Date(val.endDate);
        if (this.dateRange.startDate.getHours() <= 11) {
            this.hourValue = this.dateRange.startDate.getHours();
            this.timeViewMeridian = 'AM';
        } else {
            this.hourValue = this.dateRange.startDate.getHours() - 12;
            this.timeViewMeridian = 'PM';
        }
        if (this.dateRange.startDate.getHours() === 0 || this.dateRange.startDate.getHours() === 12) {
            this.hourValue = 12;
        }
        this.minValue = this.dateRange.startDate.getMinutes();

        if (this.dateRange.endDate.getHours() <= 11) {
            this.toHourValue = this.dateRange.endDate.getHours();
            this.toTimeViewMeridian = 'AM';
        } else {
            this.toHourValue = this.dateRange.endDate.getHours() - 12;
            this.toTimeViewMeridian = 'PM';
        }
        if (this.dateRange.endDate.getHours() === 0 || this.dateRange.endDate.getHours() === 12) {
            this.toHourValue = 12;
        }
        this.toMinValue = this.dateRange.endDate.getMinutes();

    }
    generateDays(date: Date) {
        const year = date.getFullYear(),
            month = date.getMonth(),
            current_day = date.getDate(),
            today = new Date();
        const firstDay = new Date(year, month, 1);
        const startingDay = firstDay.getDay();
        const monthLength = this.getMonthLength(month, year);
        let day = 1;
        const dateArr = [];
        let dateRow = [];
        // this loop is for is weeks (rows)
        for (let i = 0; i < 9; i++) {
            // this loop is for weekdays (cells)
            dateRow = [];
            for (let j = 0; j <= 6; j++) {
                let dateCell = null;
                if (day <= monthLength && (i > 0 || j >= startingDay)) {
                    dateCell = day;
                    if (day === current_day) {
                        // dateCell.classList.add('selected-day');
                    }
                    if (day === today.getDate() && date.getMonth() === today.getMonth() && date.getFullYear() === today.getFullYear()) {
                        // dateCell.classList.add('today');
                    }
                    day++;
                }
                let disabled = false;
                const dateNew = new Date(date.getFullYear(), month, dateCell);
                if (this.enableWeekdays === null || this.enableWeekdays === undefined || this.enableWeekdays.length === 0) {
                    disabled = false;
                } else {
                    let weekdays = dateNew.getDay();
                    if (weekdays === 0) {
                        weekdays = 8;
                    } else {
                        weekdays = weekdays + 1;
                    }

                    if (this.enableWeekdays.findIndex(x => x === weekdays) >= 0) {
                        disabled = false;
                    } else {
                        disabled = true;
                    }
                }

                dateRow.push({ disabled: disabled, day: dateCell, date: dateNew });
            }
            // stop making rows if we've run out of days
            if (day > monthLength) {
                dateArr.push(dateRow);
                break;
            } else {
                dateArr.push(dateRow);
            }
        }
        return dateArr;
    }
    generateYearList(param: string) {
        let startYear = null;
        let currentYear = null;
        if (param === 'next') {
            startYear = this.yearsList[8] + 1;
            currentYear = this.date.getFullYear();
        } else if (param === 'prev') {
            startYear = this.yearsList[0] - 9;
            currentYear = this.date.getFullYear();
        } else {
            currentYear = this.date.getFullYear();
            startYear = currentYear - 4;
            this.yearView = !this.yearView;
            this.monthsView = false;
        }
        for (let k = 0; k < 9; k++) {
            this.yearsList[k] = startYear + k;
        }
    }
    getMonthLength(month: number, year: number) {
        let monthLength = this.cal_days_in_month[month];
        // compensate for leap year
        if (month === 1) { // February only!
            if ((year % 4 === 0 && year % 100 !== 0) || year % 400 === 0) {
                monthLength = 29;
            }
        }
        return monthLength;
    }
    toggleMonthView() {
        this.yearView = false;
        this.monthsView = !this.monthsView;
    }
    toggleMeridian(val: string) {
        this.timeViewMeridian = val;
    }
    setTimeView() {
        this.date.setHours(this.hourValue);
        this.date.setMinutes(this.minValue);
        this.date = new Date(this.date);
        this.timeView = !this.timeView;
        this.done();
    }
    setDay(evt: any, type: string) {
        if (evt.target.parentElement.classList.contains('disabled-day') ||
            evt.target.getAttribute('data-label') === null ||
            evt.target.getAttribute('data-label') === undefined) {
            return false;
        }
        if (evt.target.innerHTML) {
            const selectedDay = moment(evt.target.getAttribute('data-label'), this.settings.format).toDate();
            if (type === 'range') {
                if (this.rangeSelected === 0) {
                    this.setStartDate(selectedDay);
                } else if (this.rangeSelected === 1) {
                    this.setEndDate(selectedDay);
                }
            } else {
                this.date = new Date(selectedDay);
                this.date.setHours(this.hourValue);
                this.date.setMinutes(this.minValue);
                this.parseError = false;
                this.onChangeCallback(moment(this.date).format(this.settings.format));
            }
            if (this.settings.closeOnSelect) {
                this.popover = false;
                this.onDateSelect.emit(moment(this.date).format(this.settings.format));
            }

            this.val = moment(this.date).format(this.settings.format);
        }
    }
    setStartDate(selectedDate: Date) {
        if (selectedDate < this.dateRange.endDate) {
            this.dateRange.startDate = new Date(selectedDate);
        } else if (selectedDate > this.dateRange.endDate) {
            this.dateRange.startDate = new Date(selectedDate);
            this.dateRange.endDate = new Date(selectedDate);
        }
        this.rangeSelected = 1;
    }
    setEndDate(selectedDate: Date) {
        if (selectedDate > this.dateRange.startDate && (this.dateRange.startDate !== this.dateRange.endDate)) {
            this.dateRange.endDate = new Date(selectedDate);
        } else if (selectedDate > this.dateRange.startDate && (this.dateRange.startDate === this.dateRange.endDate)) {
            this.dateRange.endDate = new Date(selectedDate);
        } else if (selectedDate < this.dateRange.startDate && (this.dateRange.startDate !== this.dateRange.endDate)) {
            this.dateRange.startDate = new Date(selectedDate);
            this.dateRange.endDate = new Date(selectedDate);
        } else if (selectedDate < this.dateRange.startDate && (this.dateRange.startDate === this.dateRange.endDate)) {
            this.dateRange.startDate = new Date(selectedDate);
            this.dateRange.endDate = new Date(selectedDate);
        } else if (selectedDate.getTime() === this.dateRange.startDate.getTime()) {
            this.dateRange.startDate = new Date(selectedDate);
            this.dateRange.endDate = new Date(selectedDate);
        }
        this.rangeSelected = 0;
    }
    highlightRange(date: Date) {
        return (date > this.dateRange.startDate && date < this.dateRange.endDate);
    }
    setYear(evt: any) {
        const selectedYear = parseInt(evt.target.getAttribute('id'), 0);
        this.date = new Date(this.date.setFullYear(selectedYear));
        this.yearView = !this.yearView;
        this.monthDays = this.generateDays(this.date);
        evt.stopPropagation();
    }
    setMonth(evt: any) {
        if (evt.target.getAttribute('id')) {
            const selectedMonth = this.settings.cal_months_labels_short.indexOf(evt.target.getAttribute('id'));
            this.date = new Date(this.date.setMonth(selectedMonth));
            this.monthsView = !this.monthsView;
            this.monthDays = this.generateDays(this.date);
        }
    }
    prevMonth(e: any) {
        e.stopPropagation();
        const self = this;
        if (this.date.getMonth() === 0) {
            this.date.setMonth(11);
            this.date.setFullYear(this.date.getFullYear() - 1);
        } else {
            const prevmonthLength = this.getMonthLength(this.date.getMonth() - 1, this.date.getFullYear());
            const currentDate = this.date.getDate();
            if (currentDate > prevmonthLength) {
                this.date.setDate(prevmonthLength);
            }
            this.date.setMonth(this.date.getMonth() - 1);
        }
        this.date = new Date(this.date);
        this.monthDays = this.generateDays(this.date);
    }
    nextMonth(e: any) {
        e.stopPropagation();
        const self = this;
        if (this.date.getMonth() === 11) {
            this.date.setMonth(0);
            this.date.setFullYear(this.date.getFullYear() + 1);
        } else {
            const nextmonthLength = this.getMonthLength(this.date.getMonth() + 1, this.date.getFullYear());
            const currentDate = this.date.getDate();
            if (currentDate > nextmonthLength) {
                this.date.setDate(nextmonthLength);
            }
            this.date.setMonth(this.date.getMonth() + 1);

        }
        this.date = new Date(this.date);
        this.monthDays = this.generateDays(this.date);
    }
    incHour() {
        if (this.hourValue < 24) {
            this.hourValue += 1;
        }
    }
    decHour() {
        if (this.hourValue > 1) {
            this.hourValue -= 1;
        }
    }
    incMinutes() {
        if (this.minValue < 59) {
            this.minValue += 1;
        }
    }
    decMinutes() {
        if (this.minValue > 0) {
            this.minValue -= 1;
        }
    }
    done() {
        this.val = moment(this.date).format(this.settings.format);
        this.parseError = false;
        this.onChangeCallback(this.val);
        this.popover = false;
        this.onDateSelect.emit(this.val);
    }
    clear() {
        this.date = new Date();
        this.val = null;
        this.parseError = false;
        this.onChangeCallback(undefined);
        this.popover = false;
        this.onDateSelect.emit(undefined);
    }
    togglePopover() {
        if (this.popover) {
            this.closepopover();
        } else {
            this.popover = true;
        }
    }
    closepopover() {
        this.rangeSelected = 0;
        this.popover = false;
    }
    composeDate(date: Date) {
        return moment(date).format(this.settings.format);
    }
    getCurrentWeek() {
        const curr_date = new Date();
        const day = curr_date.getDay();
        const diff = curr_date.getDate() - day + (day === 0 ? -6 : 1); // 0 for sunday
        const week_start_tstmp = curr_date.setDate(diff);
        const week_start = new Date(week_start_tstmp);
        let week_end = new Date(week_start_tstmp);
        week_end = new Date(week_end.setDate(week_end.getDate() + 6));
        const date = week_start + ' to ' + week_end;
        if (week_start.getMonth() === week_end.getMonth()) {
            this.monthDays = this.generateDays(week_start);
            const tempDate = new Date(week_end);
            tempDate.setMonth(tempDate.getMonth() + 1);
            tempDate.setDate(1);
            this.toMonthDays = this.generateDays(tempDate);
        } else {
            this.monthDays = this.generateDays(week_start);
            this.toMonthDays = this.generateDays(week_end);
        }

        this.setStartDate(week_start);
        this.setEndDate(week_end);
    }
    onChange(event: any) {
        const newValue = event.target.value;
        if (moment(newValue, this.settings.formatValidate, true).isValid() ||
            newValue === undefined || newValue === null || newValue === '') {
            this.parseError = false;
            if (this.required && (newValue === undefined || newValue === null || newValue === '')) {
                this.showError = true;
            } else {
                this.showError = false;
            }

            if (newValue === undefined || newValue === null || newValue === '') {
                this.onChangeCallback(undefined);
            } else {
                const date = moment(newValue, this.settings.format).toDate();
                const val = moment(date).format(this.settings.format);
                this.onChangeCallback(val);
            }
        } else {
            this.parseError = true;
            this.showError = true;
            this.onChangeCallback(newValue);
        }
    }
    onFocus() {
        this.focus.emit();
    }
    shouldShowErrors(): boolean {
        return this.formControl &&
            this.formControl.errors &&
            (this.formControl.dirty || this.formControl.touched);
    }
}

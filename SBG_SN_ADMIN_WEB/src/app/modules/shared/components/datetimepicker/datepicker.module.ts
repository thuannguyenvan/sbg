import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DatePickerComponent } from './datepicker.component';
import { FormsModule } from '@angular/forms';
import { ClickOutsideDirective } from './clickOutside';
import { MatInputModule, MatFormFieldModule } from '@angular/material';
@NgModule({
    imports: [CommonModule,
        FormsModule,
        MatInputModule,
        MatFormFieldModule,
    ],
    declarations: [
        DatePickerComponent,
        ClickOutsideDirective],
    exports: [
        DatePickerComponent,
        FormsModule,
        ClickOutsideDirective]
})
export class DateTimePickerModule {

}

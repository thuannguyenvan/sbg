﻿import {
    Component,
    OnInit,
    Inject,
    OnDestroy,
    ChangeDetectionStrategy,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../AppState';
import {
    SESSION_STORAGE,
    WebStorageService
} from 'angular-webstorage-service';

@Component({
    selector: 'app-header',
    templateUrl: './index.html',
    styleUrls: ['./index.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent implements OnInit, OnDestroy {
    private subscriptions: Subscription[] = [];
    constructor(private store: Store<AppState>,
        @Inject(SESSION_STORAGE) private storage: WebStorageService,
    ) {

    }

    ngOnInit() {
    }

    ngOnDestroy() {
        this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
    }
}

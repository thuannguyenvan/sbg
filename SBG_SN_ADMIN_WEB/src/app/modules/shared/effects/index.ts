﻿import { Injectable } from '@angular/core';
import { of, Observable } from 'rxjs';
import {
    Actions,
    Effect
} from '@ngrx/effects';
import { Action } from '@ngrx/store';
import {
    FailSharedAction,
    GetDateNowAction,
    SuccessGetDateNowAction,
} from '../actions';

@Injectable()
export class SharedEffects {
    constructor(private actions$: Actions
       ) { }
}

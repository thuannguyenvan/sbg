import { Injectable } from '@angular/core';
import {
    Actions,
    Effect,
    ofType,
} from '@ngrx/effects';
import { ValidateService } from '../../../services';
import {
    Observable,
    of
} from 'rxjs';
import { Action } from '@ngrx/store';
import {
    FailValidateAction,
    SuccessValidateAction,
    ValidateAction,
} from '../actions';
import { switchMap, map, catchError } from 'rxjs/operators';

@Injectable()
export class ValidateEffect {
    constructor(private actions$: Actions,
        private validateService: ValidateService) {
    }

    @Effect() checkValid$: Observable<Action> = this.actions$.pipe(
        ofType<ValidateAction>(ValidateAction.TYPE),
        switchMap(action => {
            return this.validateService.checkValid(action.payload).pipe(
                map((data) => new SuccessValidateAction(data)),
                catchError((error) => of(new FailValidateAction(
                    {
                        key: ValidateAction.TYPE,
                        info: error
                    }))));
        }));
}

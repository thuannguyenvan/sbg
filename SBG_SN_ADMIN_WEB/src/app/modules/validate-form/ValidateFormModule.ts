import { NgModule } from '@angular/core';
import {
    Routes,
    RouterModule
} from '@angular/router';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import {
    FormsModule,
    ReactiveFormsModule
} from '@angular/forms';

import {
    SharedModule,
} from '../../modules';

import {
    ValidateClientComponent,
    ValidateServerComponent,
    ValidateFromComponent,
} from './components';
import { CaseInsensitiveMatcher } from '../../helpers';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { ValidateEffect } from './effects';
import { ValidateReducer } from './reducers';

const routes: Routes = [
    { path: 'validate-form', component: ValidateFromComponent },
    { matcher: CaseInsensitiveMatcher('validate-form/client'), component: ValidateClientComponent },
    { matcher: CaseInsensitiveMatcher('validate-form/server'), component: ValidateServerComponent },
];

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        BrowserModule,
        FormsModule,
        BrowserModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        RouterModule.forChild(routes),
        EffectsModule.forFeature([
            ValidateEffect,
        ]),
        StoreModule.forFeature('validate', ValidateReducer),
    ],
    declarations: [
        ValidateFromComponent,
        ValidateClientComponent,
        ValidateServerComponent,
    ]
})
export class ValidateFormModule { }

import {
    OnInit,
    OnDestroy,
    Component,
    ChangeDetectionStrategy
} from '@angular/core';
import { Subscription } from 'rxjs';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
@Component({
    selector: 'app-validate-from',
    templateUrl: './index.html',
    styleUrls: ['./index.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})


export class ValidateFromComponent implements OnInit, OnDestroy {
    private subscriptions: Subscription[] = [];
    constructor(
    ) {
    }

    ngOnInit() {
    }

    onSubscriptionsSuccess() {
    }

    onSubscriptionsError() {
    }

    destroyAction() {
    }

    ngOnDestroy() {
        this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
        this.destroyAction();
    }
}

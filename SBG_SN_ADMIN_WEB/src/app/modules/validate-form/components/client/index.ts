import {
    Component,
    ChangeDetectionStrategy,
    Inject,
} from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import {
    FormControl,
    Validators,
    FormBuilder,
    FormGroup,
} from '@angular/forms';
import {
    ValidateHelper,
} from '../../../../helpers';
import { TranslateService } from '../../../../services';
import { TranslateKeyConstants } from '../../../../constants';
import {
    ValidateBaseComponent,
    IBaseComponent
} from '../../../../bases';

@Component({
    selector: 'app-validate-client',
    templateUrl: './index.html',
    styleUrls: ['./index.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})

export class ValidateClientComponent extends ValidateBaseComponent implements IBaseComponent {
    public subscriptions: Subscription[] = [];
    public formGroup: FormGroup;
    public model: any = {};
    public translateKey: any;

    constructor(@Inject(FormBuilder) fb: FormBuilder,
        private validateHelper: ValidateHelper,
        title: Title,
        private translate: TranslateService,
    ) {
        super(validateHelper);
        this.translateKey = TranslateKeyConstants;
        title.setTitle(this.translate.translate(this.translateKey.VALIDATE_CLIENT.PAGE_TITLE));
        this.initValidate(fb);
    }

    ngOnInit() {
        this.onSubscribeError();
        this.onSubscribeSuccess();
    }

    initValidate(fb: FormBuilder) {

        this.validators = {
            userName: [],
            fullName: [Validators.maxLength(5)],
            age: [this.validateHelper.numberValidator],
            number: [this.validateHelper.numberValidator],
            numberPhone: [this.validateHelper.phoneValidator],
            dateTime: [this.validateHelper.dateTimeValidator],
            password: [Validators.maxLength(5)],
            email: [Validators.email],
            maxLenght: [Validators.maxLength(5)]
        };

        this.formGroup = fb.group({
            email: new FormControl(this.model.email, this.validators.email),
            userName: new FormControl(this.model.userName, this.validators.userName),
            fullName: new FormControl(this.model.fullName, this.validators.fullName),
            age: new FormControl(this.model.age, this.validators.number),
            phone: new FormControl(this.model.phone, this.validators.numberPhone),
            birthDay: new FormControl(this.model.birthDay, this.validators.dateTime),
            maxLenght: new FormControl(this.model.maxLenght, this.validators.maxLenght)
        });
    }

    onSubscribeSuccess() {
    }

    onSubscribeError() {
    }

    onSubmit() {
        this.validateHelper.validateAllFormFields(this.formGroup);
    }

    onClear() {
        this.validateHelper.resetValidate(this.formGroup);
    }

    destroyAction() {
    }

    ngOnDestroy() {
        this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
        this.destroyAction();
    }
}

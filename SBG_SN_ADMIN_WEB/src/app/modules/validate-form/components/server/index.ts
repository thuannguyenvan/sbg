import {
    Component,
    OnDestroy,
    ChangeDetectionStrategy,
    Inject,
    OnInit,
} from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Subscription } from 'rxjs';

import {
    FormControl,
    Validators,
    FormBuilder,
} from '@angular/forms';

import {
    ToastrHelper,
    ValidateHelper,
    SpinnerHelper,
    AlertHelper,
} from '../../../../helpers';
import {
    ErrorResult,
    InfoResult,
    ValidateModel
} from '../../../../models';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../AppState';
import {
    ValidateAction,
    SuccessValidateAction,
} from '../../actions';
import { TranslateService } from '../../../../services';
import {
    ValidateBaseComponent,
    IBaseComponent
} from '../../../../bases';
import { TranslateKeyConstants } from '../../../../constants';

@Component({
    selector: 'app-validate-server',
    templateUrl: './index.html',
    styleUrls: ['./index.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ValidateServerComponent extends ValidateBaseComponent implements IBaseComponent {
    public subscriptions: Subscription[] = [];
    public model: any = {};
    public translateKey: any;

    constructor(@Inject(FormBuilder) fb: FormBuilder,
        private validateHelper: ValidateHelper,
        title: Title,
        private toastHelper: ToastrHelper,
        private store: Store<AppState>,
        private tran: TranslateService,
        private spinner: SpinnerHelper,
        private alert: AlertHelper,
    ) {
        super(validateHelper);
        this.translateKey = TranslateKeyConstants;
        title.setTitle(tran.translate(TranslateKeyConstants.VALIDATE_SERVER.PAGE_TITLE));
        this.initValidate(fb);
    }

    ngOnInit() {
        this.onSubscribeError();
        this.onSubscribeSuccess();
    }

    initValidate(fb: FormBuilder) {
        this.validators = {
            userName: [Validators.required],
            fullName: [Validators.maxLength(5)],
            age: [this.validateHelper.numberValidator],
            password: [Validators.maxLength(5)]
        };

        this.formGroup = fb.group({
            userName: new FormControl(this.model.userName, this.validators.userName),
            fullName: new FormControl(this.model.fullName, this.validators.fullName),
            dateStart: new FormControl(this.model.dateStart),
            dateEnd: new FormControl(this.model.dateEnd),
            age: new FormControl(this.model.age, this.validators.age),
            password: new FormControl(this.model.password, this.validators.password)
        });
    }

    onSubscribeSuccess() {
        this.subscriptions.push(this.store.select<InfoResult<ValidateModel>>(state => state.validate.validateModel)
            .subscribe((result) => {
                if (result !== undefined) {
                    this.spinner.hide();
                    if (result.data !== undefined && result.data !== null) {
                        this.toastHelper.success(result.data.userName);
                    }
                }
            }));
    }

    onSubscribeError() {
        this.subscriptions.push(this.store.select<ErrorResult>(state => state.validate.error)
            .subscribe((result) => {
                if (result !== undefined) {
                    this.spinner.hide();
                    this.toastHelper.showError(result);
                    this.error = this.validateHelper.validateByServer(this.formGroup, result);
                }
            }));
    }

    onSubmit() {
        this.clearValidate();
        if (!this.invalid()) {
            this.spinner.show();
            this.store.dispatch(new ValidateAction(new ValidateModel({
                userName: this.model.userName,
                password: this.model.password,
                dateStart: this.model.dateStart,
                dateEnd: this.model.dateEnd,
                age: this.model.age,
                format: ['string']
            })));
        }
    }

    onClear() {
        this.alert.confirmWarning('Bạn có muốn clear validate không?', (r: any) => {
            if (r.value) {
                this.clearValidate();
            }
        });
    }

    destroyAction() {
        this.store.dispatch({ type: SuccessValidateAction.TYPE });
    }

    ngOnDestroy() {
        this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
        this.destroyAction();
    }
}

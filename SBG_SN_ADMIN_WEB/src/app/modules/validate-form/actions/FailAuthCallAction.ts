import { Action } from '@ngrx/store';

export class FailValidateAction implements Action {
    public static TYPE = 'FAIL_VALIDATE_ACTION';
    readonly type: string = FailValidateAction.TYPE;
    constructor(public payload: any) { }
}

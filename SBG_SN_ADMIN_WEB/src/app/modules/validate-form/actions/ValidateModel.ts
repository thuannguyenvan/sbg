import { Action } from '@ngrx/store';

export class ValidateAction implements Action {
    public static TYPE = 'VALIDATE_ACTION';
    readonly type: string = ValidateAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessValidateAction implements Action {
    public static TYPE = 'SUCCESS_VALIDATE_ACTION';
    readonly type: string = SuccessValidateAction.TYPE;
    constructor(public payload: any) { }
}


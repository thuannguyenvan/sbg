import {
    SessionUserInfo,
    ErrorResult,
    InfoResult,
    ValidateModel,
} from '../../../models';

import {
    SuccessValidateAction,
    FailValidateAction
} from '../actions';

export function ValidateReducer(state: any = {}, action: any) {
    switch (action.type) {
        case SuccessValidateAction.TYPE:
            state.validateModel = action.payload === undefined || action.payload === null ? action.payload
                : new InfoResult<ValidateModel>(action.payload);
            return state;
        case FailValidateAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.error = action.payload;
                return state;
            }
            if (action.payload.info === undefined) {
                state.error = undefined;
                return state;
            }
            const errorResult = new ErrorResult(action.payload.info);
            errorResult.key = action.payload.key;
            state.error = errorResult;
            return state;
        default: {
            return state;
        }
    }
}

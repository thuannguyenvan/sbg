import {
    NgModule,
} from '@angular/core';
import {
    RouterModule,
} from '@angular/router';
import {
    BrowserAnimationsModule,
} from '@angular/platform-browser/animations';
import {
    CommonModule,
} from '@angular/common';
import {
    BrowserModule,
} from '@angular/platform-browser';
import {
    FormsModule,
    ReactiveFormsModule,
} from '@angular/forms';
import {
    EffectsModule,
} from '@ngrx/effects';
import {
    StoreModule,
} from '@ngrx/store';

import {
    SharedModule,
} from '../../modules';
import {
    UserEffect,
} from './effects';
import {
    EnvService
} from '../../services';
import {
    UserReducer,
} from './reducers/UserReducer';
import {
    UserListComponent,
} from './components';
import {
    PopupEditRoleUserComponent,
} from './components/popupEditRoleUser';

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        FormsModule,
        BrowserModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        RouterModule.forChild([
            { path: 'users', component: UserListComponent },
        ]),
        EffectsModule.forFeature([
            UserEffect,
        ]),
        StoreModule.forFeature('users', UserReducer),
    ],
    declarations: [
        UserListComponent,
        PopupEditRoleUserComponent,
    ],
    entryComponents: [
        PopupEditRoleUserComponent,
    ],
    providers: [
        EnvService,
    ]
})
export class UserModule { }

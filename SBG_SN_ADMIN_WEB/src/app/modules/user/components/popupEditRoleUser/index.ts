import {
  Component,
  Inject,
} from '@angular/core';
import {
  Store,
} from '@ngrx/store';
import {
  Subscription,
} from 'rxjs';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material';
import { AppState } from '../../../../AppState';
import {
  SpinnerHelper,
  ToastrHelper,
} from '../../../../helpers';
import {
  GetRolesAction,
  SuccessGetRolesAction,
  UpdateRoleUserAction,
  SuccessUpdateRoleUserAction,
  FailUserCallAction,
} from '../../actions';
import {
  InfoResult,
  UserRoleModel,
  ErrorResult,
} from '../../../../models';
import {
  IBaseComponent,
} from '../../../../bases';
import {
  TranslateKeyConstants,
} from '../../../../constants';
import {
  TranslateService,
} from '../../../../services';

@Component({
  selector: 'app-popup-editRoleUser',
  templateUrl: './index.html',
  styleUrls: ['./index.scss'],
})
export class PopupEditRoleUserComponent implements IBaseComponent {
  subscriptions: Subscription[] = [];
  public userRoles: UserRoleModel[] = [];
  public translateKey: any;
  public selectedOptions: string[] = [];
  public hidePoup: boolean;

  constructor(
    private store: Store<AppState>,
    private spinner: SpinnerHelper,
    private toastr: ToastrHelper,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<PopupEditRoleUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private tran: TranslateService,
  ) {
    this.translateKey = TranslateKeyConstants;
  }

  ngOnInit() {
    this.hidePoup = true;
    this.onSubscribeError();
    this.onSubscribeSuccess();
    if (this.data.id !== null) {
      setTimeout(() => {
        this.spinner.show();
        this.store.dispatch(new GetRolesAction(this.data.id));
      }, 100);
    }
  }

  initValidate() {
  }

  onSubscribeSuccess() {
    this.subscriptions.push(this.store.select<InfoResult<Array<UserRoleModel>>>(state => state.users.getRoleUser)
      .subscribe((result) => {
        if (result !== undefined) {
          this.hidePoup = false;
          if (result.data !== undefined &&
            result.data !== null
          ) {
            this.userRoles = result.data;
            this.selectedOptions = [];
            this.userRoles.forEach(role => {
              if (role.checked) {
                this.selectedOptions.push(role.id);
              }
            });
          }
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<Array<UserRoleModel>>>(state => state.users.updateRoleUser)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.data !== undefined &&
            result.data !== null
          ) {
            this.toastr.success(result.message);
            this.dialogRef.close(result.data);
            this.spinner.hide();
          }
        }
      }));
  }

  onSubscribeError() {
    this.subscriptions.push(this.store.select<ErrorResult>(state => state.users.error)
      .subscribe((result) => {
        if (result !== undefined) {
          switch (result.key) {
            case GetRolesAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              this.onClose();
              break;
            case UpdateRoleUserAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
          }
        }
      }));

  }

  onClose() {
    this.dialogRef.close();
  }

  onSave() {
    if (this.selectedOptions !== undefined && this.selectedOptions !== null && this.selectedOptions.length > 0) {
      this.userRoles.forEach(role => {
        if (this.selectedOptions.findIndex(x => x === role.id) > -1) {
          role.checked = true;
        } else {
          role.checked = false;
        }
      });
      const model = {
        userId: this.data.id,
        userRoles: this.userRoles,
      };
      this.spinner.show();
      this.store.dispatch(new UpdateRoleUserAction(model));
    } else {
      this.toastr.warning(this.tran.translate(TranslateKeyConstants.POPUP_EDIT_ROLE_USER.VALIDATE_YOU_MUST_SELECT_ROLES));
    }
  }

  destroyAction() {
    this.store.dispatch({ type: FailUserCallAction.TYPE });
    this.store.dispatch({ type: SuccessGetRolesAction.TYPE });
    this.store.dispatch({ type: SuccessUpdateRoleUserAction.TYPE });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
    this.destroyAction();
  }
}

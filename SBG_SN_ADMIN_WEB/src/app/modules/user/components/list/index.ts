import {
  Component,
  Inject,
} from '@angular/core';
import {
  Title,
} from '@angular/platform-browser';
import {
  Store,
} from '@ngrx/store';
import {
  Subscription,
} from 'rxjs';
import {
  MatDialog,
} from '@angular/material';
import {
  AppState,
} from '../../../../AppState';
import {
  GetUsersAction,
  FailUserCallAction,
  SuccessGetUsersAction,
  ActivateUserAction,
  DeactivateUserAction,
  SuccessActivateUserAction,
  SuccessDeactivateUserAction,
} from '../../actions';
import {
  SpinnerHelper,
  ToastrHelper,
  AlertHelper,
  PermissionActionHelper,
  ValidateHelper,
} from '../../../../helpers';
import {
  UserSearchModel,
  InfoResult,
  UserInfo,
  ErrorResult,
  Pagination,
  UserRoleModel,
} from '../../../../models';
import {
  IBaseComponent,
  ValidateBaseComponent,
} from '../../../../bases';
import {
  TranslateService,
} from '../../../../services';
import {
  TranslateKeyConstants,
} from '../../../../constants';
import {
  PopupEditRoleUserComponent,
} from '../popupEditRoleUser';
import {
  ActionType,
} from '../../../../enums';
import {
  FormBuilder,
  Validators,
  FormControl,
  FormGroup,
} from '@angular/forms';
import {
  String,
} from 'typescript-string-operations';

@Component({
  selector: 'app-modules-index',
  templateUrl: './index.html',
  styleUrls: ['./index.scss'],
})
export class UserListComponent extends ValidateBaseComponent implements IBaseComponent {
  public modelUserSearch: UserSearchModel = new UserSearchModel();
  public modelSearchUserSearch: UserSearchModel = new UserSearchModel();
  public usersInfo: UserInfo[] = [];
  public subscriptions: Subscription[] = [];
  public sortDefault = {
    active: 'UserName',
    direction: 'asc'
  };
  public pagination: Pagination = new Pagination();
  public roles: UserRoleModel[] = [];
  public translateKey: any;
  public permissionAction: any = {
    USER_ACTIVATE: false,
    USER_DEACTIVATE: false,
    USER_VIEW_LIST: false,
    USER_UPDATE_ROLES: false,
  };
  public formGroup: FormGroup;

  constructor(
    @Inject(FormBuilder) fb: FormBuilder,
    private validateHelper: ValidateHelper,
    private store: Store<AppState>,
    private spinner: SpinnerHelper,
    private toastr: ToastrHelper,
    title: Title,
    public dialog: MatDialog,
    private tran: TranslateService,
    private alert: AlertHelper,
    public permissionActionHelper: PermissionActionHelper,
  ) {
    super(validateHelper);
    this.translateKey = TranslateKeyConstants;
    title.setTitle(this.tran.translate(this.translateKey.USER_LIST.PAGE_TITLE));
    this.initValidate(fb);
  }

  ngOnInit() {
    this.onSubscribeError();
    this.onSubscribeSuccess();
    this.modelUserSearch = new UserSearchModel({
      orderBy: this.sortDefault.active,
      order: this.sortDefault.direction,
    });
    this.spinner.show();
    this.onSearch();
  }

  initValidate(fb: FormBuilder) {
    this.validators = {
      keywords: [Validators.maxLength(255)],
    };
    this.formGroup = fb.group({
      keywords: new FormControl(this.modelUserSearch.keywords, this.validators.keywords),
    });
  }

  onSubscribeSuccess() {
    this.subscriptions.push(this.store.select<InfoResult<Array<UserInfo>>>(state => state.users.getUsersInfo)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.data !== undefined &&
            result.data !== null
          ) {
            this.usersInfo = result.data;
            if (result.data.length === 0) {
              this.pagination.length = 0;
            }
            this.usersInfo.forEach(x => {
              let roles = [];
              this.pagination.length = x.total;
              x.roles.forEach(y => {
                roles = roles.concat(y.name);
              });
              x.rolesStr = roles.join(', ');
            });
            this.onCheckPermissionAction();
          }
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<any>>(state => state.users.activateUser)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.message !== undefined &&
            result.message !== null
          ) {
            this.toastr.success(result.message);
            this.onSearch();
            this.onCheckPermissionAction();
          }
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<any>>(state => state.users.deactivateUser)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.message !== undefined &&
            result.message !== null
          ) {
            this.toastr.success(result.message);
            this.onSearch();
            this.onCheckPermissionAction();
          }
          this.spinner.hide();
        }
      }));
  }

  onSubscribeError() {
    this.subscriptions.push(this.store.select<ErrorResult>(state => state.users.error)
      .subscribe((result) => {
        if (result !== undefined) {
          switch (result.key) {
            case GetUsersAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
            case ActivateUserAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
            case DeactivateUserAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
          }
        }
      }));
  }

  onCheckPermissionAction() {
    this.permissionAction.USER_ACTIVATE =
      this.permissionActionHelper.isValid(ActionType.USER_ACTIVATE);
    this.permissionAction.USER_DEACTIVATE =
      this.permissionActionHelper.isValid(ActionType.USER_DEACTIVATE);
    this.permissionAction.USER_VIEW_LIST =
      this.permissionActionHelper.isValid(ActionType.USER_VIEW_LIST);
    this.permissionAction.USER_UPDATE_ROLES =
      this.permissionActionHelper.isValid(ActionType.USER_UPDATE_ROLES);
  }

  public onShowActtionItem(item: UserInfo, key: string) {
    let checkShow = false;
    switch (key) {
      case 'activate':
        checkShow = item.actions.findIndex(x => x === ActionType.USER_ACTIVATE) > -1 &&
          this.permissionAction.USER_ACTIVATE;
        break;
      case 'deactivate':
        checkShow = item.actions.findIndex(x => x === ActionType.USER_DEACTIVATE) > -1 &&
          this.permissionAction.USER_DEACTIVATE;
        break;
      case 'editRole':
        checkShow = item.actions.findIndex(x => x === ActionType.USER_UPDATE_ROLES) > -1 &&
          this.permissionAction.USER_UPDATE_ROLES;
        break;
    }
    return checkShow;
  }

  onSearch() {
    if (!this.invalid()) {
      this.pagination.pageIndex = 1;
      this.modelSearchUserSearch = new UserSearchModel({
        page: this.pagination.pageIndex,
        size: this.pagination.pageSize,
        orderBy: this.modelUserSearch.orderBy,
        order: this.modelUserSearch.order,
        keywords: this.modelUserSearch.keywords,
      });
      this.searchUserList();
    }
  }

  searchUserList() {
    this.spinner.show();
    this.store.dispatch(new GetUsersAction(this.modelSearchUserSearch));
  }

  onPageChanged(page: any) {
    this.modelSearchUserSearch.page = this.pagination.pageIndex = page.pageIndex;
    this.modelSearchUserSearch.size = this.pagination.pageSize = page.pageSize;
    this.searchUserList();
  }

  sortData(sort: any) {
    this.modelUserSearch.orderBy = sort.active;
    this.modelUserSearch.order = sort.direction;
    this.modelUserSearch.page = 1;
    this.pagination.pageIndex = 1;
    this.onSearch();
  }

  onUpdateListUser(userId: number, result: any) {
    this.roles = [];
    const index = this.usersInfo.findIndex(x => x.id === userId);
    if (index > -1) {
      let roles = [];
      result.forEach((role: UserRoleModel) => {
        if (role.checked) {
          this.roles.push(role);
        }
      });
      this.roles.forEach(y => {
        roles = roles.concat(y.name);
      });
      this.usersInfo[index].rolesStr = roles.join(', ');
      this.usersInfo[index].roles = this.roles;
    }
  }

  openDialog(userId: number): void {
    if (userId != null && userId !== undefined) {

      const dialogRef = this.dialog.open(
        PopupEditRoleUserComponent,
        {
          width: '500px',
          data: { id: userId },
        });

      dialogRef.afterClosed().subscribe(data => {
        if (data !== undefined && data != null) {
          this.onSearch();
        }
      });
    }
  }

  onActivate(user: UserInfo) {
    const msg = String.Format(this.tran.translate(this.translateKey.USER_LIST.MESSAGE_CONFIRM_ACTIVATE_USER), user.userName);
    this.alert.confirmWarning(msg,
      (r: any) => {
        if (r.value) {
          const model = {
            id: user.id
          };
          this.spinner.show();
          this.store.dispatch(new ActivateUserAction(model));
        }
      });
  }

  onDeactivate(user: UserInfo) {
    const msg = String.Format(this.tran.translate(this.translateKey.USER_LIST.MESSAGE_CONFIRM_DEACTIVATE_USER), user.userName);
    this.alert.confirmWarning(msg,
      (r: any) => {
        if (r.value) {
          const model = {
            id: user.id
          };
          this.spinner.show();
          this.store.dispatch(new DeactivateUserAction(model));
        }
      });

  }

  destroyAction() {
    this.store.dispatch({ type: FailUserCallAction.TYPE });
    this.store.dispatch({ type: SuccessGetUsersAction.TYPE });
    this.store.dispatch({ type: SuccessActivateUserAction.TYPE });
    this.store.dispatch({ type: SuccessDeactivateUserAction.TYPE });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
    this.destroyAction();
  }
}

import {
    Injectable,
} from '@angular/core';
import {
    Actions,
    Effect,
    ofType,
} from '@ngrx/effects';
import {
    Observable,
    of,
} from 'rxjs';
import {
    Action,
} from '@ngrx/store';
import {
    switchMap,
    map,
    catchError,
} from 'rxjs/operators';

import {
    GetUsersAction,
    SuccessGetUsersAction,
    FailUserCallAction,
    GetRolesAction,
    SuccessGetRolesAction,
    UpdateRoleUserAction,
    SuccessUpdateRoleUserAction,
    ActivateUserAction,
    SuccessActivateUserAction,
    DeactivateUserAction,
    SuccessDeactivateUserAction,
} from '../actions';
import { UserService } from '../../../services';

@Injectable()
export class UserEffect {
    constructor(private actions$: Actions, private userService: UserService) { }

    @Effect() getUsersInfo$: Observable<Action> = this.actions$.pipe(
        ofType<GetUsersAction>(GetUsersAction.TYPE),
        switchMap(action => {
            return this.userService.getUsersInfo(action.payload).pipe(
                map((data) => new SuccessGetUsersAction(data)),
                catchError((error) => of(new FailUserCallAction(
                    {
                        key: GetUsersAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() getRoleUser$: Observable<Action> = this.actions$.pipe(
        ofType<GetRolesAction>(GetRolesAction.TYPE),
        switchMap(action => {
            return this.userService.getRolesUser(action.payload).pipe(
                map((data) => new SuccessGetRolesAction(data)),
                catchError((error) => of(new FailUserCallAction(
                    {
                        key: GetRolesAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() updateRoleUser$: Observable<Action> = this.actions$.pipe(
        ofType<UpdateRoleUserAction>(UpdateRoleUserAction.TYPE),
        switchMap(action => {
            return this.userService.updateRoleUser(action.payload).pipe(
                map((data) => new SuccessUpdateRoleUserAction(data)),
                catchError((error) => of(new FailUserCallAction(
                    {
                        key: UpdateRoleUserAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() activateUser$: Observable<Action> = this.actions$.pipe(
        ofType<ActivateUserAction>(ActivateUserAction.TYPE),
        switchMap(action => {
            return this.userService.activateUser(action.payload).pipe(
                map((data) => new SuccessActivateUserAction(data)),
                catchError((error) => of(new FailUserCallAction(
                    {
                        key: ActivateUserAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() deactivateUser$: Observable<Action> = this.actions$.pipe(
        ofType<DeactivateUserAction>(DeactivateUserAction.TYPE),
        switchMap(action => {
            return this.userService.deactivateUser(action.payload).pipe(
                map((data) => new SuccessDeactivateUserAction(data)),
                catchError((error) => of(new FailUserCallAction(
                    {
                        key: DeactivateUserAction.TYPE,
                        info: error
                    }))));
        }));
}


import { Action } from '@ngrx/store';

export class ActivateUserAction implements Action {
    public static TYPE = 'ACTIVATE_USER_ACTION';
    readonly type: string = ActivateUserAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessActivateUserAction implements Action {
    public static TYPE = 'SUCCESS_ACTIVATE_USER_ACTION';
    readonly type: string = SuccessActivateUserAction.TYPE;
    constructor(public payload: any) { }
}

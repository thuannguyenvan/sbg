import { Action } from '@ngrx/store';

export class GetUsersAction implements Action {
    public static TYPE = 'GET_USERS_ACTION';
    readonly type: string = GetUsersAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessGetUsersAction implements Action {
    public static TYPE = 'SUCCESS_GET_USERS_ACTION';
    readonly type: string = SuccessGetUsersAction.TYPE;
    constructor(public payload: any) { }
}

import { Action } from '@ngrx/store';

export class FailUserCallAction implements Action {
    public static TYPE = 'FAIL_USER_CALL_ACTION';
    readonly type: string = FailUserCallAction.TYPE;
    constructor(public payload: any) { }
}

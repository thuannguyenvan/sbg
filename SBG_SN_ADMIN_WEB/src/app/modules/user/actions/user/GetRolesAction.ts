import { Action } from '@ngrx/store';

export class GetRolesAction implements Action {
    public static TYPE = 'GET_ROLES_ACTION';
    readonly type: string = GetRolesAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessGetRolesAction implements Action {
    public static TYPE = 'SUCCESS_GET_ROLES_ACTION';
    readonly type: string = SuccessGetRolesAction.TYPE;
    constructor(public payload: any) { }
}

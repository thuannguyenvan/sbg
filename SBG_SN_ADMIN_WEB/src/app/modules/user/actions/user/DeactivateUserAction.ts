
import { Action } from '@ngrx/store';

export class DeactivateUserAction implements Action {
    public static TYPE = 'DEACTIVATE_USER_ACTION';
    readonly type: string = DeactivateUserAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessDeactivateUserAction implements Action {
    public static TYPE = 'SUCCESS_DEACTIVATE_USER_ACTION';
    readonly type: string = SuccessDeactivateUserAction.TYPE;
    constructor(public payload: any) { }
}

import { Action } from '@ngrx/store';

export class UpdateRoleUserAction implements Action {
    public static TYPE = 'UPDATE_ROLE_USER_ACTION';
    readonly type: string = UpdateRoleUserAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessUpdateRoleUserAction implements Action {
    public static TYPE = 'SUCCESS_UPDATE_ROLE_USER_ACTION';
    readonly type: string = SuccessUpdateRoleUserAction.TYPE;
    constructor(public payload: any) { }
}

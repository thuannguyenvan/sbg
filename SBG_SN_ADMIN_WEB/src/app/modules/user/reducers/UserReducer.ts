import {
    ErrorResult,
    InfoResult,
    UserInfo,
    UserRoleModel,
} from '../../../models';

import {
    FailUserCallAction,
    SuccessGetUsersAction,
    SuccessGetRolesAction,
    SuccessUpdateRoleUserAction,
    SuccessActivateUserAction,
    SuccessDeactivateUserAction,
} from '../actions';

export function UserReducer(state: any = {}, action: any) {
    switch (action.type) {
        case SuccessGetUsersAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.getUsersInfo = action.payload;
            } else {
                let data = null;
                if (action.payload.data === undefined || action.payload.data === null) {
                    data = action.payload;
                } else {
                    action.payload.data = action.payload.data.map((d: any) => new UserInfo(d));
                    data = action.payload;
                }
                state.getUsersInfo = new InfoResult<Array<UserInfo>>(data);
            }
            return state;
        case SuccessGetRolesAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.getRoleUser = action.payload;
            } else {
                let data = null;
                if (action.payload.data === undefined || action.payload.data === null) {
                    data = action.payload;
                } else {
                    action.payload.data = action.payload.data.map((d: any) => new UserRoleModel(d));
                    data = action.payload;
                }
                state.getRoleUser = new InfoResult<Array<UserRoleModel>>(data);
            }
            return state;
        case SuccessUpdateRoleUserAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.updateRoleUser = action.payload;
            } else {
                let data = null;
                if (action.payload.data === undefined || action.payload.data === null) {
                    data = action.payload;
                } else {
                    action.payload.data = action.payload.data.map((d: any) => new UserRoleModel(d));
                    data = action.payload;
                }
                state.updateRoleUser = new InfoResult<Array<UserRoleModel>>(data);
            }
            return state;
        case SuccessActivateUserAction.TYPE:
            state.activateUser
                = action.payload === undefined || action.payload === null ? action.payload
                    : new InfoResult<any>(action.payload);
            return state;
        case SuccessDeactivateUserAction.TYPE:
            state.deactivateUser
                = action.payload === undefined || action.payload === null ? action.payload
                    : new InfoResult<any>(action.payload);
            return state;
        case FailUserCallAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.error = action.payload;
                return state;
            }
            if (action.payload.info === undefined) {
                state.error = undefined;
                return state;
            }
            const errorResult = new ErrorResult(action.payload.info);
            errorResult.key = action.payload.key;
            state.error = errorResult;
            return state;
        default: {
            return state;
        }
    }
}

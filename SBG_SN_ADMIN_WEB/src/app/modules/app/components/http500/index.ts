import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { RouterConstants } from '../../../../constants';

@Component({
    selector: 'app-500',
    templateUrl: './500.html',
})

export class Http500Component {
    constructor(private router: Router) {

    }

    onGoLoginPage() {
        this.router.navigate([RouterConstants.auths.login]);
    }

    onGoHomePage() {
        this.router.navigate([RouterConstants.dashboard]);
    }
}

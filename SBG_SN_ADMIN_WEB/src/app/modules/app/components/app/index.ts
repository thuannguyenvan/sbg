import {
    OnInit,
    OnDestroy,
    Component,
    Inject
} from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../AppState';
import { Subscription } from 'rxjs';
import {
    SESSION_STORAGE,
    WebStorageService
} from 'angular-webstorage-service';
import {
    PermissionActionHelper,
    SpinnerHelper
} from '../../../../helpers';
import {
    SessionUserInfo,
    InfoResult
} from '../../../../models';
import {
    KeyConstants,
    RouterConstants
} from '../../../../constants';
import { GetLoginInfo } from '../../../auth/actions';

@Component({
    selector: 'app-root',
    template: `<router-outlet *ngIf="isActive"></router-outlet>
    <ngx-spinner bdOpacity="0.6" bdColor="rgba(51,51,51,0.8)" size="medium" color="#fff" ></ngx-spinner>`,
})
export class AppComponent implements OnInit, OnDestroy {
    private subscriptions: Subscription[] = [];
    private isActive = false;

    constructor(private store: Store<AppState>,
        @Inject(SESSION_STORAGE) private storage: WebStorageService,
        private permissionActionHelper: PermissionActionHelper,
        private spinner: SpinnerHelper,
    ) {
    }

    ngOnInit() {
        this.permissionActionHelper.initPermission();
        const url = window.location.pathname;
        if (!url.toLowerCase().startsWith(RouterConstants.ssoCallback.toLowerCase())) {
            this.spinner.show();
            this.store.dispatch(new GetLoginInfo());
        }

        if (url.toLowerCase().startsWith(RouterConstants.auths.login.toLowerCase()) ||
            url.toLowerCase().startsWith(RouterConstants.ssoCallback.toLowerCase())) {
            this.isActive = true;
        }

        this.subscriptions.push(this.store.select<InfoResult<SessionUserInfo>>(state => state.auths.getLoginInfo)
            .subscribe((result) => {
                if (result !== undefined) {
                    this.spinner.hide();
                    this.isActive = true;
                    if (result.data !== null && result.data !== undefined) {
                        const userInfoJson = JSON.stringify(result.data);
                        this.storage.set(KeyConstants.USER_INFO, userInfoJson);
                    } else {
                        this.storage.remove(KeyConstants.USER_INFO);
                    }
                }
            }));

        this.subscriptions.push(this.store.select<InfoResult<boolean>>(state => state.shareds.activeRouterOutlet)
            .subscribe((result) => {
                if (result !== undefined) {
                    this.isActive = result.data;
                }
            }));
    }
    ngOnDestroy() {
        this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
    }
}

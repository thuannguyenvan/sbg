import {
    OnInit,
    OnDestroy,
    Component,
} from '@angular/core';
import {
    Params,
    ActivatedRoute,
} from '@angular/router';
import { Subscription } from 'rxjs';

@Component({ selector: '<app-auth></app-auth>', templateUrl: './app-auth.html'})
export class AuthComponent implements OnInit, OnDestroy {

    private dispose: Subscription;
    public state: any;

    constructor(private activatedRoute: ActivatedRoute) {
        this.state = {
            redirectUri: '/'
        };
    }

    ngOnInit(): void {
        this.dispose = this.activatedRoute.queryParams.subscribe((params: Params) => {
            this.state.redirectUri = params['redirect_uri'];
        });
    }

    ngOnDestroy(): void {
        this.dispose.unsubscribe();
    }
}

import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { RouterConstants } from '../../../../constants';

@Component({
    selector: 'app-404',
    templateUrl: './404.html',
})

export class Http404Component {
    constructor(private router: Router) {

    }

    onGoLoginPage() {
        this.router.navigate([RouterConstants.auths.login]);
    }

    onGoHomePage() {
        this.router.navigate([RouterConstants.dashboard]);
    }
}

import {
    OnInit,
    OnDestroy,
    Component
} from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../AppState';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';

@Component({ templateUrl: './index.html' })
export class HomePageComponent implements OnInit, OnDestroy {
    private subscriptions: Subscription[] = [];
    constructor(private store: Store<AppState>,
        private router: Router, ) {
    }

    ngOnInit(): void {
    }

    ngOnDestroy(): void {

    }
}

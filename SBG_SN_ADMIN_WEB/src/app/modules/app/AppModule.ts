import {
    NgModule,
    APP_INITIALIZER,
} from '@angular/core';
import {
    CommonModule,
} from '@angular/common';
import {
    EffectsModule,
} from '@ngrx/effects';
import {
    BrowserModule,
} from '@angular/platform-browser';
import {
    FormsModule,
    ReactiveFormsModule,
} from '@angular/forms';
import {
    RouterModule,
    Routes,
} from '@angular/router';
import {
    HTTP_INTERCEPTORS,
    HttpClientModule,
    HttpClientXsrfModule,
} from '@angular/common/http';
import {
    HttpModule,
} from '@angular/http';
import {
    StoreModule,
} from '@ngrx/store';
import {
    StoreDevtoolsModule,
} from '@ngrx/store-devtools';
import {
    StorageServiceModule,
} from 'angular-webstorage-service';
import {
    routerReducer,
    StoreRouterConnectingModule,
} from '@ngrx/router-store';
import {
    CookieModule,
} from 'ngx-cookie';
import {
    BrowserAnimationsModule
} from '@angular/platform-browser/animations';
import {
    NgxSpinnerModule,
} from 'ngx-spinner';

import {
    SharedModule,
    DashboardModule,
    ValidateFormModule,
    LivestreamModule,
    AdvertisingModule,
} from '../../modules';
import {
    AppComponent,
    AuthComponent,
    HomePageComponent,
    Http401Component,
    Http403Component,
    Http404Component,
    Http500Component,
} from './components';
import {
    AuthInterceptor,
    HttpErrorInterceptor,
} from '../../interceptors';
import {
    environment,
} from '../../../environments/environment';
import {
    DashboardComponent,
} from '../dashboard/components';
import {
    HttpHelper,
    SpinnerHelper,
    ToastrHelper,
    CaseInsensitiveMatcher,
    ValidateHelper,
    EnvServiceProvider,
    AlertHelper,
    PermissionActionHelper,
} from '../../helpers';
import {
    AuthModule,
} from '../../modules';
import {
    AuthService,
    ValidateService,
    TranslateService,
    EnvService,
    UserService,
    ManagerCompetitionsService,
    ManagerLivestreamService,
    ManagerRoundService,
    ManagerStandingService,
    ManagerMatchService,
    ManagerPartnerService,
    ManagerAdvertisingService,
    ManagerAdvertisingConfigService,
} from '../../services';
import { UserModule } from '../user/UserModule';

export function setupTranslateFactory(
    service: TranslateService): Function {
    return () => service.setlang();
}

export const EnvServiceFactory = () => {
    const env = new EnvService();
    const browserWindow = window || {};
    const browserWindowEnv = browserWindow['__env'] || {};
    for (const key in browserWindowEnv) {
        if (browserWindowEnv.hasOwnProperty(key)) {
            env[key] = window['__env'][key];
        }
    }

    return env;
};

const appRoutes: Routes = [
    { matcher: CaseInsensitiveMatcher('401'), component: Http401Component },
    { matcher: CaseInsensitiveMatcher('403'), component: Http403Component },
    { matcher: CaseInsensitiveMatcher('404'), component: Http404Component },
    { matcher: CaseInsensitiveMatcher('500'), component: Http500Component },
    { matcher: CaseInsensitiveMatcher('auth'), component: AuthComponent },
    { path: '', component: DashboardComponent },
    { path: '**', component: Http404Component }
];

@NgModule({
    declarations: [
        AppComponent,
        AuthComponent,
        HomePageComponent,
        Http401Component,
        Http403Component,
        Http404Component,
        Http500Component,
    ],
    imports: [
        FormsModule,
        CommonModule,
        BrowserModule,
        HttpClientModule,
        HttpModule,
        ReactiveFormsModule,
        StorageServiceModule,
        BrowserAnimationsModule,
        NgxSpinnerModule,
        SharedModule,
        DashboardModule,
        AuthModule,
        ValidateFormModule,
        LivestreamModule,
        AdvertisingModule,
        UserModule,
        StoreModule.forRoot({
            routerReducer: routerReducer,
        }),
        !environment.production ? StoreDevtoolsModule.instrument({ maxAge: 5 }) : [],
        RouterModule.forRoot(appRoutes, { enableTracing: !environment.production, useHash: false }),
        HttpClientXsrfModule.withOptions({
            cookieName: 'XSRF-TOKEN',
            headerName: 'X-XSRF-TOKEN',
        }),
        EffectsModule.forRoot([]),
        StoreRouterConnectingModule,
        CookieModule.forRoot(),
        ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' })
    ],
    providers: [
        HttpHelper,
        AuthService,
        ValidateService,
        UserService,
        EnvServiceProvider,
        SpinnerHelper,
        ToastrHelper,
        AlertHelper,
        ValidateHelper,
        TranslateService,
        PermissionActionHelper,
        ManagerCompetitionsService,
        ManagerLivestreamService,
        ManagerRoundService,
        ManagerStandingService,
        ManagerMatchService,
        ManagerPartnerService,
        ManagerAdvertisingService,
        ManagerAdvertisingConfigService,
        {
            provide: APP_INITIALIZER,
            useFactory: setupTranslateFactory,
            deps: [TranslateService],
            multi: true
        },
        {
            provide: EnvService,
            useFactory: EnvServiceFactory,
            deps: [],
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpErrorInterceptor,
            multi: true,
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true,
        },
    ],
    bootstrap: [AppComponent],
})
export class AppModule { }

import {
    Injectable,
} from '@angular/core';
import {
    Actions,
    Effect,
    ofType,
} from '@ngrx/effects';

import {
    Observable,
    of,
} from 'rxjs';
import {
    Action,
} from '@ngrx/store';
import {
    switchMap,
    map,
    catchError,
} from 'rxjs/operators';

import {
    CreateRoundAction,
    SuccessCreateRoundAction,
    SuccessUpdateRoundAction,
    GetListCompetitionsRoundAction,
    SuccessGetListCompetitionsRoundAction,
    UpdateRoundAction,
    GetRoundInfoAction,
    SuccessGetRoundInfoAction,
    FailManagerRoundCallAction,
    GetStageByCpAction,
    SuccessGetStageByCpAction,
    GetRoundsAction,
    DeleteRoundAction,
    SuccessGetRoundsAction,
    SuccessDeleteRoundAction,
} from '../actions';
import {
    ManagerRoundService,
} from '../../../services';

@Injectable()
export class ManagerRoundEffect {
    constructor(private actions$: Actions,
        private managerRoundService: ManagerRoundService) { }

    @Effect() getCompetitions$: Observable<Action> = this.actions$.pipe(
        ofType<GetListCompetitionsRoundAction>(GetListCompetitionsRoundAction.TYPE),
        switchMap(action => {
            return this.managerRoundService.getCompetitionInfo().pipe(
                map((data) => new SuccessGetListCompetitionsRoundAction(data)),
                catchError((error) => of(new FailManagerRoundCallAction(
                    {
                        key: GetListCompetitionsRoundAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() getStageByCp$: Observable<Action> = this.actions$.pipe(
        ofType<GetStageByCpAction>(GetStageByCpAction.TYPE),
        switchMap(action => {
            return this.managerRoundService.getStageByCp(action.payload).pipe(
                map((data) => new SuccessGetStageByCpAction(data)),
                catchError((error) => of(new FailManagerRoundCallAction(
                    {
                        key: GetStageByCpAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() createRound$: Observable<Action> = this.actions$.pipe(
        ofType<CreateRoundAction>(CreateRoundAction.TYPE),
        switchMap(action => {
            return this.managerRoundService.createRound(action.payload).pipe(
                map((data) => new SuccessCreateRoundAction(data)),
                catchError((error) => of(new FailManagerRoundCallAction(
                    {
                        key: CreateRoundAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() updateRound$: Observable<Action> = this.actions$.pipe(
        ofType<UpdateRoundAction>(UpdateRoundAction.TYPE),
        switchMap(action => {
            return this.managerRoundService.updateRound(action.payload).pipe(
                map((data) => new SuccessUpdateRoundAction(data)),
                catchError((error) => of(new FailManagerRoundCallAction(
                    {
                        key: UpdateRoundAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() getInfoRound$: Observable<Action> = this.actions$.pipe(
        ofType<GetRoundInfoAction>(GetRoundInfoAction.TYPE),
        switchMap(action => {
            return this.managerRoundService.getInfoRound(action.payload).pipe(
                map((data) => new SuccessGetRoundInfoAction(data)),
                catchError((error) => of(new FailManagerRoundCallAction(
                    {
                        key: GetRoundInfoAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() getRounds$: Observable<Action> = this.actions$.pipe(
        ofType<GetRoundsAction>(GetRoundsAction.TYPE),
        switchMap(action => {
            return this.managerRoundService.getRounds(action.payload).pipe(
                map((data) => new SuccessGetRoundsAction(data)),
                catchError((error) => of(new FailManagerRoundCallAction(
                    {
                        key: GetRoundsAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() deleteRound$: Observable<Action> = this.actions$.pipe(
        ofType<DeleteRoundAction>(DeleteRoundAction.TYPE),
        switchMap(action => {
            return this.managerRoundService.deleteRound(action.payload).pipe(
                map((data) => new SuccessDeleteRoundAction(data)),
                catchError((error) => of(new FailManagerRoundCallAction(
                    {
                        key: DeleteRoundAction.TYPE,
                        info: error
                    }))));
        }));
}

import {
    Injectable,
} from '@angular/core';
import {
    Actions,
    Effect,
    ofType,
} from '@ngrx/effects';

import {
    Observable,
    of,
} from 'rxjs';
import { Action } from '@ngrx/store';
import {
    switchMap,
    map,
    catchError,
} from 'rxjs/operators';

import {
    GetCompetitionsInfoAction,
    FailManagerStageCallAction,
    SuccessGetCompetitionsInfoAction,
    GetListStageAction,
    SuccessGetListStageAction,
    CreateStandingAction,
    SuccessCreateStandingAction,
    SuccessDeleteStandingAction,
    UpdateStageStandingAction,
    SuccessUpdateStageStandingAction,
    DeleteStandingAction,
    SuccessUpdateStandingAction,
    GetStandingInfoAction,
    SuccessGetStandingInfoAction,
    UpdateStandingAction,
    GetTeamInfoByStageIdAction,
    SuccessGetTeamInfoByStageIdAction,
} from '../actions';
import {
    ManagerStandingService
} from '../../../services';

@Injectable()
export class ManagerStageEffect {
    constructor(private actions$: Actions,
        private managerStandingService: ManagerStandingService) { }

    @Effect() getCompetitionInfo$: Observable<Action> = this.actions$.pipe(
        ofType<GetCompetitionsInfoAction>(GetCompetitionsInfoAction.TYPE),
        switchMap(action => {
            return this.managerStandingService.getCompetitionInfo().pipe(
                map((data) => new SuccessGetCompetitionsInfoAction(data)),
                catchError((error) => of(new FailManagerStageCallAction(
                    {
                        key: GetCompetitionsInfoAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() getListStages$: Observable<Action> = this.actions$.pipe(
        ofType<GetListStageAction>(GetListStageAction.TYPE),
        switchMap(action => {
            return this.managerStandingService.getListStages(action.payload).pipe(
                map((data) => new SuccessGetListStageAction(data)),
                catchError((error) => of(new FailManagerStageCallAction(
                    {
                        key: GetListStageAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() getTeamInfoByStageId$: Observable<Action> = this.actions$.pipe(
        ofType<GetTeamInfoByStageIdAction>(GetTeamInfoByStageIdAction.TYPE),
        switchMap(action => {
            return this.managerStandingService.getTeamInfoByStageId(action.payload).pipe(
                map((data) => new SuccessGetTeamInfoByStageIdAction(data)),
                catchError((error) => of(new FailManagerStageCallAction(
                    {
                        key: GetTeamInfoByStageIdAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() createStanding$: Observable<Action> = this.actions$.pipe(
        ofType<CreateStandingAction>(CreateStandingAction.TYPE),
        switchMap(action => {
            return this.managerStandingService.createStanding(action.payload).pipe(
                map((data) => new SuccessCreateStandingAction(data)),
                catchError((error) => of(new FailManagerStageCallAction(
                    {
                        key: CreateStandingAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() deleteStanding$: Observable<Action> = this.actions$.pipe(
        ofType<DeleteStandingAction>(DeleteStandingAction.TYPE),
        switchMap(action => {
            return this.managerStandingService.deleteStanding(action.payload).pipe(
                map((data) => new SuccessDeleteStandingAction(data)),
                catchError((error) => of(new FailManagerStageCallAction(
                    {
                        key: DeleteStandingAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() updateStandingStage$: Observable<Action> = this.actions$.pipe(
        ofType<UpdateStageStandingAction>(UpdateStageStandingAction.TYPE),
        switchMap(action => {
            return this.managerStandingService.updateStandingStage(action.payload).pipe(
                map((data) => new SuccessUpdateStageStandingAction(data)),
                catchError((error) => of(new FailManagerStageCallAction(
                    {
                        key: UpdateStageStandingAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() updateStanding$: Observable<Action> = this.actions$.pipe(
        ofType<UpdateStandingAction>(UpdateStandingAction.TYPE),
        switchMap(action => {
            return this.managerStandingService.updateStanding(action.payload).pipe(
                map((data) => new SuccessUpdateStandingAction(data)),
                catchError((error) => of(new FailManagerStageCallAction(
                    {
                        key: UpdateStandingAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() getInfoStanding$: Observable<Action> = this.actions$.pipe(
        ofType<GetStandingInfoAction>(GetStandingInfoAction.TYPE),
        switchMap(action => {
            return this.managerStandingService.getInfoStanding(action.payload).pipe(
                map((data) => new SuccessGetStandingInfoAction(data)),
                catchError((error) => of(new FailManagerStageCallAction(
                    {
                        key: GetStandingInfoAction.TYPE,
                        info: error
                    }))));
        }));
}

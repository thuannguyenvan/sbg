import {
    Injectable,
} from '@angular/core';
import {
    Actions,
    Effect,
    ofType,
} from '@ngrx/effects';
import {
    Observable,
    of,
} from 'rxjs';
import {
    Action,
} from '@ngrx/store';
import {
    switchMap,
    map,
    catchError,
} from 'rxjs/operators';

import {
    GetCompetitionsAction,
    SuccessGetCompetitionsAction,
    FailCompetitionCallAction,
    CreateCompetitionsAction,
    SuccessCreateCompetitionsAction,
    DeleteCompetitionsAction,
    SuccessDeleteCompetitionsAction,
    GetCompetitionsCreateInfoAction,
    SuccessGetCompetitionsCreateInfoAction,
    SuccessGetCompetitionsEditInfoAction,
    GetCompetitionsEditInfoAction,
    GetSoccerTeamAction,
    SuccessGetSoccerTeamAction,
    GetCompetitionSoccerTeamAction,
    SuccessGetCompetitionSoccerTeamAction,
    SuccessGetListCompetitionsAction,
    GetListCompetitionsAction,
    GetCompetitionsConfigInfoAction,
    SuccessGetCompetitionsConfigInfoAction,
    EditCompetitionsAction,
    SuccessEditCompetitionsAction,
    UpdateSoccerTeamCompetition,
    SuccessUpdateSoccerTeamCompetition,
} from '../actions';
import {
    ManagerCompetitionsService,
} from '../../../services';

@Injectable()
export class CompetitionEffect {
    constructor(private actions$: Actions,
        private competitionService: ManagerCompetitionsService) { }

    @Effect() getCompetitionsInfo$: Observable<Action> = this.actions$.pipe(
        ofType<GetCompetitionsAction>(GetCompetitionsAction.TYPE),
        switchMap(action => {
            return this.competitionService.getCompetitions(action.payload).pipe(
                map((data) => new SuccessGetCompetitionsAction(data)),
                catchError((error) => of(new FailCompetitionCallAction(
                    {
                        key: GetCompetitionsAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() updateCompetitionSoccerTeam$: Observable<Action> = this.actions$.pipe(
        ofType<UpdateSoccerTeamCompetition>(UpdateSoccerTeamCompetition.TYPE),
        switchMap(action => {
            return this.competitionService.updateCompetitionSoccerTeam(action.payload).pipe(
                map((data) => new SuccessUpdateSoccerTeamCompetition(data)),
                catchError((error) => of(new FailCompetitionCallAction(
                    {
                        key: UpdateSoccerTeamCompetition.TYPE,
                        info: error
                    }))));
        }));

    @Effect() getTeamByCompetitions$: Observable<Action> = this.actions$.pipe(
        ofType<GetCompetitionsConfigInfoAction>(GetCompetitionsConfigInfoAction.TYPE),
        switchMap(action => {
            return this.competitionService.getTeamByCompetitions(action.payload).pipe(
                map((data) => new SuccessGetCompetitionsConfigInfoAction(data)),
                catchError((error) => of(new FailCompetitionCallAction(
                    {
                        key: GetCompetitionsConfigInfoAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() getCompetitionInfo$: Observable<Action> = this.actions$.pipe(
        ofType<GetListCompetitionsAction>(GetListCompetitionsAction.TYPE),
        switchMap(action => {
            return this.competitionService.getCompetitionInfo().pipe(
                map((data) => new SuccessGetListCompetitionsAction(data)),
                catchError((error) => of(new FailCompetitionCallAction(
                    {
                        key: GetListCompetitionsAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() getCompetitionsCreateInfo$: Observable<Action> = this.actions$.pipe(
        ofType<GetCompetitionsCreateInfoAction>(GetCompetitionsCreateInfoAction.TYPE),
        switchMap(action => {
            return this.competitionService.getCompetitionsCreateInfo().pipe(
                map((data) => new SuccessGetCompetitionsCreateInfoAction(data)),
                catchError((error) => of(new FailCompetitionCallAction(
                    {
                        key: GetCompetitionsCreateInfoAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() getCompetitionsEditInfo$: Observable<Action> = this.actions$.pipe(
        ofType<GetCompetitionsEditInfoAction>(GetCompetitionsEditInfoAction.TYPE),
        switchMap(action => {
            return this.competitionService.getCompetitionsEditInfo(action.payload).pipe(
                map((data) => new SuccessGetCompetitionsEditInfoAction(data)),
                catchError((error) => of(new FailCompetitionCallAction(
                    {
                        key: GetCompetitionsEditInfoAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() createCompetitions$: Observable<Action> = this.actions$.pipe(
        ofType<CreateCompetitionsAction>(CreateCompetitionsAction.TYPE),
        switchMap(action => {
            return this.competitionService.createCompetition(action.payload).pipe(
                map((data) => new SuccessCreateCompetitionsAction(data)),
                catchError((error) => of(new FailCompetitionCallAction(
                    {
                        key: CreateCompetitionsAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() editCompetitions$: Observable<Action> = this.actions$.pipe(
        ofType<EditCompetitionsAction>(EditCompetitionsAction.TYPE),
        switchMap(action => {
            return this.competitionService.editCompetition(action.payload).pipe(
                map((data) => new SuccessEditCompetitionsAction(data)),
                catchError((error) => of(new FailCompetitionCallAction(
                    {
                        key: EditCompetitionsAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() deleteCompetition$: Observable<Action> = this.actions$.pipe(
        ofType<DeleteCompetitionsAction>(DeleteCompetitionsAction.TYPE),
        switchMap(action => {
            return this.competitionService.deleteCompetition(action.payload).pipe(
                map((data) => new SuccessDeleteCompetitionsAction(data)),
                catchError((error) => of(new FailCompetitionCallAction(
                    {
                        key: DeleteCompetitionsAction.TYPE,
                        info: error
                    }))));
        }));

    // tìm kiếm đội bóng
    @Effect() getSoccerTeam$: Observable<Action> = this.actions$.pipe(
        ofType<GetSoccerTeamAction>(GetSoccerTeamAction.TYPE),
        switchMap(action => {
            return this.competitionService.getSoccerTeam(action.payload).pipe(
                map((data) => new SuccessGetSoccerTeamAction(data)),
                catchError((error) => of(new FailCompetitionCallAction(
                    {
                        key: GetSoccerTeamAction.TYPE,
                        info: error
                    }))));
        }));

    // get đội bóng thuộc giải đấu
    @Effect() getCompetitionSoccerTeam$: Observable<Action> = this.actions$.pipe(
        ofType<GetCompetitionSoccerTeamAction>(GetCompetitionSoccerTeamAction.TYPE),
        switchMap(action => {
            return this.competitionService.getCompetitionSoccerTeam(action.payload).pipe(
                map((data) => new SuccessGetCompetitionSoccerTeamAction(data)),
                catchError((error) => of(new FailCompetitionCallAction(
                    {
                        key: GetCompetitionSoccerTeamAction.TYPE,
                        info: error
                    }))));
        }));
}

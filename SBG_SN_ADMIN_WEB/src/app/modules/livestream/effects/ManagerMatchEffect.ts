import { Injectable } from '@angular/core';
import {
    Actions,
    Effect,
    ofType,
} from '@ngrx/effects';

import {
    Observable,
    of
} from 'rxjs';
import { Action } from '@ngrx/store';
import {
    switchMap,
    map,
    catchError
} from 'rxjs/operators';

import {
    GetCreateMatchInfoAction,
    SuccessGetCreateMatchInfoAction,
    GetRoundByCpCreateMatchAction,
    SuccessGetRoundByCpCreateMatchAction,
    GetTeamByRoundCreateMatchAction,
    SuccessGetTeamByRoundCreateMatchAction,
    CreateMatchAction,
    SuccessCreateMatchAction,
    GetEditMatchInfoAction,
    SuccessGetEditMatchInfoAction,
    EditMatchAction,
    SuccessEditMatchAction,
    GetMatchInfoListAction,
    SuccessGetMatchInfoListAction,
    FailSoccerMatchMatchCallAction,
    GetRoundByCompetitionInfoListAction,
    SuccessGetRoundByCompetitionInfoListAction,
    GetSearchInfoMatchListAction,
    SuccessGetSearchInfoMatchListAction,
    SuccessDeleteSoccerMatchAction,
    DeleteSoccerMatchAction
} from '../actions';
import {
    ManagerMatchService,
} from '../../../services';

@Injectable()
export class ManagerMatchEffect {
    constructor(private actions$: Actions,
        private managerMatchService: ManagerMatchService) { }

    @Effect() getAddMatchInfo$: Observable<Action> = this.actions$.pipe(
        ofType<GetCreateMatchInfoAction>(GetCreateMatchInfoAction.TYPE),
        switchMap(action => {
            return this.managerMatchService.getAddMatchInfo().pipe(
                map((data) => new SuccessGetCreateMatchInfoAction(data)),
                catchError((error) => of(new FailSoccerMatchMatchCallAction(
                    {
                        key: GetCreateMatchInfoAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() getRoundByCompetition$: Observable<Action> = this.actions$.pipe(
        ofType<GetRoundByCpCreateMatchAction>(GetRoundByCpCreateMatchAction.TYPE),
        switchMap(action => {
            return this.managerMatchService.getRoundByCompetition(action.payload).pipe(
                map((data) => new SuccessGetRoundByCpCreateMatchAction(data)),
                catchError((error) => of(new FailSoccerMatchMatchCallAction(
                    {
                        key: GetRoundByCpCreateMatchAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() getTeamByRound$: Observable<Action> = this.actions$.pipe(
        ofType<GetTeamByRoundCreateMatchAction>(GetTeamByRoundCreateMatchAction.TYPE),
        switchMap(action => {
            return this.managerMatchService.getTeamByRound(action.payload).pipe(
                map((data) => new SuccessGetTeamByRoundCreateMatchAction(data)),
                catchError((error) => of(new FailSoccerMatchMatchCallAction(
                    {
                        key: GetTeamByRoundCreateMatchAction.TYPE,
                        info: error
                    }))));
        }));

    // search danh sách trận đấu
    @Effect() getMatchInfoList$: Observable<Action> = this.actions$.pipe(
        ofType<GetMatchInfoListAction>(GetMatchInfoListAction.TYPE),
        switchMap(action => {
            return this.managerMatchService.getMatchInfoList(action.payload).pipe(
                map((data) => new SuccessGetMatchInfoListAction(data)),
                catchError((error) => of(new FailSoccerMatchMatchCallAction(
                    {
                        key: GetMatchInfoListAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() createMatch$: Observable<Action> = this.actions$.pipe(
        ofType<CreateMatchAction>(CreateMatchAction.TYPE),
        switchMap(action => {
            return this.managerMatchService.createMatch(action.payload).pipe(
                map((data) => new SuccessCreateMatchAction(data)),
                catchError((error) => of(new FailSoccerMatchMatchCallAction(
                    {
                        key: CreateMatchAction.TYPE,
                        info: error
                    }))));
        }));

    // lấy danh sách round từ giải đấu
    @Effect() getRoundByCompetitionInfoList$: Observable<Action> = this.actions$.pipe(
        ofType<GetRoundByCompetitionInfoListAction>(GetRoundByCompetitionInfoListAction.TYPE),
        switchMap(action => {
            return this.managerMatchService.getRoundByCompetitionInfoList(action.payload).pipe(
                map((data) => new SuccessGetRoundByCompetitionInfoListAction(data)),
                catchError((error) => of(new FailSoccerMatchMatchCallAction(
                    {
                        key: GetRoundByCompetitionInfoListAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() getEditMatchInfo$: Observable<Action> = this.actions$.pipe(
        ofType<GetEditMatchInfoAction>(GetEditMatchInfoAction.TYPE),
        switchMap(action => {
            return this.managerMatchService.getEditMatchInfo(action.payload).pipe(
                map((data) => new SuccessGetEditMatchInfoAction(data)),
                catchError((error) => of(new FailSoccerMatchMatchCallAction(
                    {
                        key: GetEditMatchInfoAction.TYPE,
                        info: error
                    }))));
        }));

    // lấy thông tin để search trận đấu
    @Effect() getSearchInfoMatchList$: Observable<Action> = this.actions$.pipe(
        ofType<GetSearchInfoMatchListAction>(GetSearchInfoMatchListAction.TYPE),
        switchMap(action => {
            return this.managerMatchService.getSearchInfoMatchList().pipe(
                map((data) => new SuccessGetSearchInfoMatchListAction(data)),
                catchError((error) => of(new FailSoccerMatchMatchCallAction(
                    {
                        key: GetSearchInfoMatchListAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() updateMatch$: Observable<Action> = this.actions$.pipe(
        ofType<EditMatchAction>(EditMatchAction.TYPE),
        switchMap(action => {
            return this.managerMatchService.updateMatch(action.payload).pipe(
                map((data) => new SuccessEditMatchAction(data)),
                catchError((error) => of(new FailSoccerMatchMatchCallAction(
                    {
                        key: EditMatchAction.TYPE,
                        info: error
                    }))));
        }));

    // delete
    @Effect() deleteMatch$: Observable<Action> = this.actions$.pipe(
        ofType<DeleteSoccerMatchAction>(DeleteSoccerMatchAction.TYPE),
        switchMap(action => {
            return this.managerMatchService.deleteMatch(action.payload).pipe(
                map((data) => new SuccessDeleteSoccerMatchAction(data)),
                catchError((error) => of(new FailSoccerMatchMatchCallAction(
                    {
                        key: DeleteSoccerMatchAction.TYPE,
                        info: error
                    }))));
        }));
}

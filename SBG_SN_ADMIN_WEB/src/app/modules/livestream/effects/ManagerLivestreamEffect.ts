import {
    Injectable,
} from '@angular/core';
import {
    Actions,
    Effect,
    ofType,
} from '@ngrx/effects';

import {
    Observable,
    of,
} from 'rxjs';
import {
    Action,
} from '@ngrx/store';
import {
    GetRoundAction,
    SuccessGetRoundAction,
    FailManagerLivestreamCallAction,
    GetMatchAction,
    SuccessGetMatchAction,
    SearchAction,
    SuccessSearchAction,
    GetInfoEditlivestreamAction,
    SuccessGetInfoEditlivestreamAction,
    CreateLivestreamAction,
    SuccessCreateLivestreamAction,
    DeleteLivestreamAction,
    SuccessDeleteLivestreamAction,
    UpdateLivestreamAction,
    SuccessUpdateLivestreamAction,
    GetSearchLTInfoAction,
    SuccessGetSearchLTInfoAction,
    SuccessGetInfoCreateLivestreamAction,
    GetInfoCreateLivestreamAction,
} from '../actions';
import {
    switchMap,
    map,
    catchError,
} from 'rxjs/operators';
import {
    ManagerLivestreamService,
} from '../../../services';

@Injectable()
export class ManagerLivestreamEffect {
    constructor(private actions$: Actions,
        private managerLivestreamService: ManagerLivestreamService) { }

    @Effect() getCompetitions$: Observable<Action> = this.actions$.pipe(
        ofType<GetSearchLTInfoAction>(GetSearchLTInfoAction.TYPE),
        switchMap(action => {
            return this.managerLivestreamService.getSearchInfo().pipe(
                map((data) => new SuccessGetSearchLTInfoAction(data)),
                catchError((error) => of(new FailManagerLivestreamCallAction(
                    {
                        key: GetSearchLTInfoAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() getRound$: Observable<Action> = this.actions$.pipe(
        ofType<GetRoundAction>(GetRoundAction.TYPE),
        switchMap(action => {
            return this.managerLivestreamService.getRound(action.payload).pipe(
                map((data) => new SuccessGetRoundAction(data)),
                catchError((error) => of(new FailManagerLivestreamCallAction(
                    {
                        key: GetRoundAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() getMatch$: Observable<Action> = this.actions$.pipe(
        ofType<GetMatchAction>(GetMatchAction.TYPE),
        switchMap(action => {
            return this.managerLivestreamService.getMatch(action.payload).pipe(
                map((data) => new SuccessGetMatchAction(data)),
                catchError((error) => of(new FailManagerLivestreamCallAction(
                    {
                        key: GetMatchAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() seachLivestream$: Observable<Action> = this.actions$.pipe(
        ofType<SearchAction>(SearchAction.TYPE),
        switchMap(action => {
            return this.managerLivestreamService.seachLivestream(action.payload).pipe(
                map((data) => new SuccessSearchAction(data)),
                catchError((error) => of(new FailManagerLivestreamCallAction(
                    {
                        key: SearchAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() getInfoUpdate$: Observable<Action> = this.actions$.pipe(
        ofType<GetInfoEditlivestreamAction>(GetInfoEditlivestreamAction.TYPE),
        switchMap(action => {
            return this.managerLivestreamService.getInfoUpdate(action.payload).pipe(
                map((data) => new SuccessGetInfoEditlivestreamAction(data)),
                catchError((error) => of(new FailManagerLivestreamCallAction(
                    {
                        key: GetInfoEditlivestreamAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() getCreateInfo$: Observable<Action> = this.actions$.pipe(
            ofType<GetInfoCreateLivestreamAction>(GetInfoCreateLivestreamAction.TYPE),
            switchMap(action => {
                return this.managerLivestreamService.getCreateInfo(action.payload).pipe(
                    map((data) => new SuccessGetInfoCreateLivestreamAction(data)),
                    catchError((error) => of(new FailManagerLivestreamCallAction(
                        {
                            key: GetInfoCreateLivestreamAction.TYPE,
                            info: error
                        }))));
            }));

    @Effect() createLivestream$: Observable<Action> = this.actions$.pipe(
        ofType<CreateLivestreamAction>(CreateLivestreamAction.TYPE),
        switchMap(action => {
            return this.managerLivestreamService.createLivestream(action.payload).pipe(
                map((data) => new SuccessCreateLivestreamAction(data)),
                catchError((error) => of(new FailManagerLivestreamCallAction(
                    {
                        key: CreateLivestreamAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() deleteLivestream$: Observable<Action> = this.actions$.pipe(
        ofType<DeleteLivestreamAction>(DeleteLivestreamAction.TYPE),
        switchMap(action => {
            return this.managerLivestreamService.deleteLivestream(action.payload).pipe(
                map((data) => new SuccessDeleteLivestreamAction(data)),
                catchError((error) => of(new FailManagerLivestreamCallAction(
                    {
                        key: DeleteLivestreamAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() updateLivestream$: Observable<Action> = this.actions$.pipe(
        ofType<UpdateLivestreamAction>(UpdateLivestreamAction.TYPE),
        switchMap(action => {
            return this.managerLivestreamService.updateLivestream(action.payload).pipe(
                map((data) => new SuccessUpdateLivestreamAction(data)),
                catchError((error) => of(new FailManagerLivestreamCallAction(
                    {
                        key: UpdateLivestreamAction.TYPE,
                        info: error
                    }))));
        }));
}

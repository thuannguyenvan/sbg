import {
    NgModule,
} from '@angular/core';
import {
    RouterModule,
} from '@angular/router';
import {
    BrowserAnimationsModule,
} from '@angular/platform-browser/animations';
import {
    CommonModule,
} from '@angular/common';
import {
    BrowserModule,
} from '@angular/platform-browser';
import {
    EffectsModule,
} from '@ngrx/effects';
import {
    StoreModule,
} from '@ngrx/store';
import {
    FormsModule,
    ReactiveFormsModule,
} from '@angular/forms';
import {
    SharedModule,
} from '../../modules';
import {
    ManagerLivestreamEffect,
    ManagerRoundEffect,
    ManagerStageEffect,
    ManagerMatchEffect,
    CompetitionEffect,
} from './effects';
import {
    EnvService,
} from '../../services';

import {
    ManagerLiveStreamComponent,
    CreateRoundComponent,
    EditRoundComponent,
    RoundListComponent,
    CreateEditMathcComponent,
    ManagerStageComponent,
    CreateEditCompetitionComponent,
    CompetitionListComponent,
    CompetitionConfigComponent,
    PopupManagerGroupComponent,
    PopupUpdateStandingComponent,
    MatchListComponent,
    PopupAddSoccerTeamComponent,
    CreateEditLivestreamComponent,
} from './components';
import {
    ManagerLivestreamReducer,
    ManagerRoundReducer,
    ManagerStageReducer,
    ManagerMatchReducer,
    CompetitionReducer,
} from './reducers';

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        FormsModule,
        BrowserModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        RouterModule.forChild([
            {
                path: 'livestream',
                children: [
                    { path: 'manager-stage', component: ManagerStageComponent },
                    { path: 'manager-livestream', component: ManagerLiveStreamComponent },
                    {
                        path: 'soccer-match',
                        children: [
                            { path: 'add-round', component: CreateRoundComponent },
                            { path: 'edit-round/:roundId/:competitionId', component: EditRoundComponent },
                            { path: 'edit-round/:id', component: EditRoundComponent },
                            { path: 'rounds', component: RoundListComponent },
                            { path: 'rounds/:competitionId', component: RoundListComponent },
                            { path: 'create-match', component: CreateEditMathcComponent },
                            { path: 'edit-match/:id', component: CreateEditMathcComponent },
                            { path: 'create-match/:roundId/:competitionId', component: CreateEditMathcComponent },
                            { path: 'list-livestream', component: ManagerLiveStreamComponent },
                            { path: 'create-livestream', component: CreateEditLivestreamComponent },
                            { path: 'create-livestream/:matchId', component: CreateEditLivestreamComponent },
                            { path: 'edit-livestream/:id', component: CreateEditLivestreamComponent },
                            { path: '', component: MatchListComponent }
                        ]
                    },
                    {
                        path: 'soccer-competitions',
                        children: [
                            { path: 'create', component: CreateEditCompetitionComponent },
                            { path: '', component: CompetitionListComponent },
                            { path: 'edit/:id', component: CreateEditCompetitionComponent },
                            { path: 'config', component: CompetitionConfigComponent },
                            { path: 'config/:id', component: CompetitionConfigComponent },
                            { path: 'manager-stage', component: ManagerStageComponent },
                            { path: 'manager-stage/:id', component: ManagerStageComponent },
                        ]
                    },
                ]
            }
        ]),
        EffectsModule.forFeature([
            ManagerRoundEffect,
            ManagerStageEffect,
            ManagerLivestreamEffect,
            ManagerMatchEffect,
            CompetitionEffect,
        ]),
        StoreModule.forFeature('managerStage', ManagerStageReducer),
        StoreModule.forFeature('managerLivestream', ManagerLivestreamReducer),
        StoreModule.forFeature('standing', ManagerStageReducer),
        StoreModule.forFeature('soccerMatch', ManagerMatchReducer),
        StoreModule.forFeature('managerRound', ManagerRoundReducer),
        StoreModule.forFeature('competitions', CompetitionReducer),
    ],
    declarations: [
        ManagerStageComponent,
        ManagerLiveStreamComponent,
        PopupManagerGroupComponent,
        PopupUpdateStandingComponent,
        CreateRoundComponent,
        EditRoundComponent,
        RoundListComponent,
        CreateEditMathcComponent,
        MatchListComponent,
        CompetitionListComponent,
        CreateEditCompetitionComponent,
        PopupAddSoccerTeamComponent,
        CompetitionConfigComponent,
        ManagerStageComponent,
        CreateEditLivestreamComponent,
    ],
    entryComponents: [
        PopupAddSoccerTeamComponent,
        PopupManagerGroupComponent,
        PopupUpdateStandingComponent,
    ],
    providers: [
        EnvService,
    ]
})
export class LivestreamModule { }

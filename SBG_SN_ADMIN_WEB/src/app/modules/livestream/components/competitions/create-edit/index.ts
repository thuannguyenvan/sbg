import {
  Component,
  Inject,
} from '@angular/core';
import {
  Title,
} from '@angular/platform-browser';
import {
  Store,
} from '@ngrx/store';
import {
  Subscription,
} from 'rxjs';
import {
  MatDialog,
} from '@angular/material';
import {
  Router,
  ActivatedRoute,
} from '@angular/router';
import {
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import {
  IBaseComponent,
  ValidateBaseComponent,
} from '../../../../../bases';
import {
  ToastrHelper,
  SpinnerHelper,
  ValidateHelper,
  PermissionActionHelper,
} from '../../../../../helpers';
import {
  AppState,
} from '../../../../../AppState';
import {
  TranslateService,
} from '../../../../../services';
import {
  TranslateKeyConstants,
  RouterConstants,
  LenghtValidateConstants,
} from '../../../../../constants';
import {
  Pagination,
  InfoResult,
  ErrorResult,
  CompetitionCreateEditInfo,
  CompetitionCreateEditModel,
  CompetitionStageModel,
  CompetitionBaseModel,
} from '../../../../../models';
import {
  FailCompetitionCallAction,
  CreateCompetitionsAction,
  EditCompetitionsAction,
  SuccessCreateCompetitionsAction,
  SuccessEditCompetitionsAction,
  GetCompetitionsCreateInfoAction,
  GetCompetitionsEditInfoAction,
  SuccessGetCompetitionsCreateInfoAction,
  SuccessGetCompetitionsEditInfoAction,
} from '../../../actions';
import {
  ActionType,
} from '../../../../../enums';
import { DateTimeHelper } from '../../../../../helpers/DateTimeHelper';

@Component({
  selector: 'app-competitions-create-edit',
  templateUrl: './index.html',
  styleUrls: ['./index.scss'],
})
export class CreateEditCompetitionComponent extends ValidateBaseComponent implements IBaseComponent {
  subscriptions: Subscription[] = [];
  public translateKey: any;
  public competitionCreateEditModel: CompetitionCreateEditModel = new CompetitionCreateEditModel();
  public pagination: Pagination = new Pagination();
  selectedOptions: string[] = [];
  public stages: CompetitionStageModel[] = [];
  public competitionBases: CompetitionBaseModel[] = [];
  public showBtn: any = {
    showBtnCreate: false,
  };
  public permissionAction: any = {
    COMPETITION_ADD_COMPETITION: false,
    COMPETITION_EDIT_COMPETITION: false,
  };

  public optionDateOnly: any = DateTimeHelper.getOptionDateOnly();

  constructor(
    @Inject(FormBuilder) fb: FormBuilder,
    private validateHelper: ValidateHelper,
    private store: Store<AppState>,
    private spinner: SpinnerHelper,
    private toastr: ToastrHelper,
    public title: Title,
    public dialog: MatDialog,
    private translate: TranslateService,
    private router: Router,
    private route: ActivatedRoute,
    public permissionActionHelper: PermissionActionHelper,
  ) {
    super(validateHelper);
    this.translateKey = TranslateKeyConstants;
    this.initValidate(fb);
  }

  ngOnInit() {
    this.onSubscribeError();
    this.onSubscribeSuccess();
    this.onCheckPermissionAction();
    // get info
    if (this.router.url.indexOf(RouterConstants.competitions.create) > -1) {
      this.spinner.show();
      this.store.dispatch(new GetCompetitionsCreateInfoAction());
      this.showBtn.showBtnCreate = true;
      this.title.setTitle(this.translate.translate(this.translateKey.COMPETITION_ADD_EDIT.PAGE_TITLE_ADD));
    } else {
      /* Form Edit */
      const paramId = this.route.snapshot.params['id'];
      this.store.dispatch(new GetCompetitionsEditInfoAction(paramId));
      this.showBtn.showBtnCreate = false;
      this.title.setTitle(this.translate.translate(this.translateKey.COMPETITION_ADD_EDIT.PAGE_TITLE_EDIT));

    }
  }

  initValidate(fb: FormBuilder) {
    this.validators = {
      season: [Validators.required, Validators.maxLength(LenghtValidateConstants.STRING_MEDIUM)],
      title: [Validators.required, Validators.maxLength(LenghtValidateConstants.STRING_MEDIUM)],
      place: [Validators.maxLength(LenghtValidateConstants.STRING_MEDIUM)],
      description: [Validators.maxLength(LenghtValidateConstants.STRING_MAX)],
      startAt: [],
      endAt: [],
      competitionBase: [Validators.required],
    };

    this.formGroup = fb.group({
      season: new FormControl(this.competitionCreateEditModel.season, this.validators.season),
      title: new FormControl(this.competitionCreateEditModel.title, this.validators.title),
      place: new FormControl(this.competitionCreateEditModel.place, this.validators.place),
      description: new FormControl(this.competitionCreateEditModel.description, this.validators.description),
      startAt: new FormControl(this.competitionCreateEditModel.startAt, this.validators.startAt),
      endAt: new FormControl(this.competitionCreateEditModel.endAt, this.validators.endAt),
      competitionBase: new FormControl(this.competitionCreateEditModel.soccerCompetitionBaseId, this.validators.competitionBase),
    });
  }

  onSubscribeSuccess() {
    this.subscriptions.push(this.store.select<InfoResult<CompetitionCreateEditInfo>>(state =>
      state.competitions.getCompetitionsCreateInfo)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.data !== undefined && result.data !== null) {
            this.competitionCreateEditModel.soccerCompetitionBaseId = result.data.competitionBases[0].id;
            this.competitionBases = result.data.competitionBases;
            this.stages = result.data.stages;
          }
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<CompetitionCreateEditInfo>>(state =>
      state.competitions.getCompetitionsEditInfo)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.data !== undefined && result.data !== null) {
            if (result.data.competition !== undefined && result.data.competition !== null) {
              this.competitionBases = result.data.competitionBases;
              this.competitionCreateEditModel = new CompetitionCreateEditModel(result.data.competition);
              this.competitionCreateEditModel.soccerCompetitionBaseId = result.data.competition.soccerCompetitionBaseId;
              this.stages = result.data.stages;
              if (result.data.competition.stages !== undefined
                && result.data.competition.stages !== null
                && result.data.competition.stages.length > 0
              ) {
                this.selectedOptions = result.data.competition.stages.map(({ soccerStageId }) => soccerStageId);
              }
            }
          }
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<any>>(state => state.competitions.createCompetition)
      .subscribe((result) => {
        if (result !== undefined) {
          this.toastr.success(result.message);
          this.router.navigate([RouterConstants.competitions.list]);
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<any>>(state => state.competitions.editCompetition)
      .subscribe((result) => {
        if (result !== undefined) {
          this.toastr.success(result.message);
          this.router.navigate([RouterConstants.competitions.list]);
          this.spinner.hide();
        }
      }));
  }

  onSubscribeError() {
    this.subscriptions.push(this.store.select<ErrorResult>(state => state.competitions.error)
      .subscribe((result) => {
        if (result !== undefined) {
          switch (result.key) {
            case GetCompetitionsCreateInfoAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
            case GetCompetitionsEditInfoAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
            case CreateCompetitionsAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
            case EditCompetitionsAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
          }
        }
      }));
  }

  onCheckPermissionAction() {
    this.permissionAction.COMPETITION_SEARCH =
      this.permissionActionHelper.isValid(ActionType.COMPETITION_SEARCH);
    this.permissionAction.COMPETITION_ADD_COMPETITION =
      this.permissionActionHelper.isValid(ActionType.COMPETITION_ADD_COMPETITION);
    this.permissionAction.COMPETITION_EDIT_COMPETITION =
      this.permissionActionHelper.isValid(ActionType.COMPETITION_EDIT_COMPETITION);
  }

  onCreateCompetition() {
    this.router.navigate([RouterConstants.competitions.create]);
  }

  onCreate() {
    if (!this.invalid()) {
      this.onMapStage();
      this.spinner.show();
      this.store.dispatch(new CreateCompetitionsAction(this.competitionCreateEditModel));
    } else {
      this.toastr.warning(this.translate.translate(this.translateKey.SHARE.MESSAGE_DATA_INVALID_WARNING));
    }
  }

  onEdit() {
    if (!this.invalid()) {
      this.onMapStage();
      this.spinner.show();
      this.store.dispatch(new EditCompetitionsAction(this.competitionCreateEditModel));
    } else {
      this.toastr.warning(this.translate.translate(this.translateKey.SHARE.MESSAGE_DATA_INVALID_WARNING));
    }
  }

  onBack() {
    this.router.navigate([RouterConstants.competitions.list]);
  }

  onChange(event: any) {
    const index = this.stages.findIndex(x => x.id === event.option.value
      && x.standalone && event.option._selected);
    if (index > -1) {
      this.selectedOptions = [];
      this.selectedOptions.push(event.option.value);
    } else {
      const indexRemove = this.stages.findIndex(x => x.id === event.option.value
        && !x.standalone && event.option._selected);
      if (indexRemove > -1) {
        let selects = [];
        selects = this.selectedOptions;
        this.stages.forEach(st => {
          if (st.standalone) {
            this.selectedOptions.forEach((sl, i) => {
              if (st.id === sl) {
                selects.splice(i, 1);
              }
            }
            );
          }
        });
        this.selectedOptions = [];
        selects.forEach(st => {
          this.selectedOptions.push(st);
        });

      }
    }
  }

  onMapStage() {
    this.competitionCreateEditModel.stages = this.selectedOptions;
  }

  destroyAction() {
    this.store.dispatch({ type: FailCompetitionCallAction.TYPE });
    this.store.dispatch({ type: SuccessGetCompetitionsCreateInfoAction.TYPE });
    this.store.dispatch({ type: SuccessGetCompetitionsEditInfoAction.TYPE });
    this.store.dispatch({ type: SuccessCreateCompetitionsAction.TYPE });
    this.store.dispatch({ type: SuccessEditCompetitionsAction.TYPE });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
    this.destroyAction();
  }
}

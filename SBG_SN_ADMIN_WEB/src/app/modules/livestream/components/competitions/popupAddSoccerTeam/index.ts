import {
  Component,
  Inject,
} from '@angular/core';
import {
  Store,
} from '@ngrx/store';
import {
  Subscription,
} from 'rxjs';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material';
import { AppState } from '../../../../../AppState';
import { String } from 'typescript-string-operations';
import {
  SpinnerHelper,
  ToastrHelper,
  ValidateHelper,
} from '../../../../../helpers';
import {
  InfoResult,
  UserRoleModel,
  ErrorResult,
  SoccerTeamModel,
  Pagination,
  SoccerCompetitionTeams,
  CompetitionTeamModel,
} from '../../../../../models';
import {
  IBaseComponent, ValidateBaseComponent,
} from '../../../../../bases';
import {
  TranslateKeyConstants,
} from '../../../../../constants';
import {
  TranslateService,
} from '../../../../../services';
import {
  GetSoccerTeamAction,
  FailCompetitionCallAction,
  SuccessGetSoccerTeamAction,
} from '../../../actions';
import { SelectionModel } from '@angular/cdk/collections';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-popup-add-soccer-team',
  templateUrl: './index.html',
  styleUrls: ['./index.scss'],
})
export class PopupAddSoccerTeamComponent extends ValidateBaseComponent implements IBaseComponent {
  subscriptions: Subscription[] = [];
  public userRoles: UserRoleModel[] = [];
  public translateKey: any;
  public hidePoup: boolean;
  public soccerTeams: CompetitionTeamModel[] = [];
  public soccerTeamAlls: CompetitionTeamModel[] = [];
  public modelSearch: SoccerTeamModel = new SoccerTeamModel();
  public pagination: Pagination = new Pagination();
  private sortDefault = 'NAME';
  selection = new SelectionModel<string>(true, []);
  public soccerCompetitionTeams: SoccerCompetitionTeams[] = [];
  public selectSoccerTeams: string[] = [];
  public selectSoccerTeamsFirst: string[] = [];
  public showSoccerTeam = '';
  public formGroup: FormGroup;
  constructor(
    @Inject(FormBuilder) fb: FormBuilder,
    private validateHelper: ValidateHelper,
    private store: Store<AppState>,
    private spinner: SpinnerHelper,
    private toastr: ToastrHelper,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<PopupAddSoccerTeamComponent>,

    @Inject(MAT_DIALOG_DATA) public data: any,
    private tran: TranslateService,
  ) {
    super(validateHelper);
    this.translateKey = TranslateKeyConstants;
    this.initValidate(fb);
  }

  ngOnInit() {
    this.hidePoup = true;
    this.onSubscribeError();
    this.onSubscribeSuccess();

    this.modelSearch = new SoccerTeamModel({
      page: this.pagination.pageIndex,
      size: this.pagination.pageSizeOptions[0],
      orderBy: this.sortDefault,
      id: this.data.id
    });
    this.selectSoccerTeams = [];
    this.store.dispatch(new GetSoccerTeamAction(this.modelSearch));
    this.selectSoccerTeams = this.data.soccerTeams.map(row => row.soccerTeamId);
    this.selectSoccerTeamsFirst = this.data.soccerTeams.map(row => row.soccerTeamId);
    this.soccerTeamAlls = this.data.soccerTeams;
  }

  initValidate(fb: FormBuilder) {
    this.validators = {
      name: [Validators.maxLength(255)],
    };
    this.formGroup = fb.group({
      name: new FormControl(this.modelSearch.name, this.validators.name),
    });
  }

  onSubscribeSuccess() {
    this.subscriptions.push(this.store.select<InfoResult<Array<CompetitionTeamModel>>>
      (state => state.competitions.getSoccerTeam)
      .subscribe((result) => {
        if (result !== undefined) {
          this.hidePoup = false;
          if (result.data !== undefined &&
            result.data !== null
          ) {
            this.hidePoup = false;
            this.soccerTeams = result.data;
            this.soccerTeams.forEach(x => {
              this.pagination.length = x.total;
              const indexAdd = this.soccerTeamAlls.findIndex(y => y.soccerTeamId === x.id);
              if (indexAdd < 0) {
                this.soccerTeamAlls.push(new CompetitionTeamModel({
                  id: x.id,
                  name: x.name,
                  soccerTeamId: x.id,
                  teamCode: x.teamCode,
                }));
              }
            });
            this.onShowNameSoccerTeam();
            if (this.selectSoccerTeams !== null && this.selectSoccerTeams.length > 0) {
              this.selectSoccerTeams.forEach(row => {
                const index = this.soccerTeams.findIndex(x => x.id === row);
                if (index > -1) {
                  this.selection.select(row);
                }
              });
            }

          }
          this.spinner.hide();
        }
      }));
  }
  onSubscribeError() {
    this.subscriptions.push(this.store.select<ErrorResult>(state => state.users.error)
      .subscribe((result) => {
        if (result !== undefined) {
          switch (result.key) {
            case GetSoccerTeamAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              this.onClose();
              break;
          }
        }
      }));
  }

  onSearch() {
    if (!this.invalid()) {
      this.modelSearch.page = 1;
      this.pagination.pageIndex = 1;
      this.selection.clear();
      this.spinner.show();
      setTimeout(() => {
        this.spinner.show();
        this.store.dispatch(new GetSoccerTeamAction(this.modelSearch));
      }, 100);
    }
  }

  onClose() {
    const selection = this.selectSoccerTeamsFirst;
    this.dialogRef.close(this.soccerTeamAlls.filter(function (s) {
      return selection.indexOf(s.soccerTeamId) >= 0;
    }));
  }

  onSave() {
    const selection = this.selectSoccerTeams;
    this.dialogRef.close(this.soccerTeamAlls.filter(function (s) {
      return selection.indexOf(s.soccerTeamId) >= 0;
    }));
  }

  onPageChanged(page: any) {
    if (page.pageIndex !== undefined && page.pageIndex !== null) {
      this.modelSearch.page = this.pagination.pageIndex = page.pageIndex;
    }
    if (page.pageSize !== undefined && page.pageSize !== null) {
      this.modelSearch.size = this.pagination.pageSize = page.pageSize;
    }
    this.selection.clear();
    this.spinner.show();
    setTimeout(() => {
      this.spinner.show();
      this.store.dispatch(new GetSoccerTeamAction(this.modelSearch));
    }, 100);
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.soccerTeams.length;
    return numSelected === numRows;
  }

  masterToggle() {
    if (this.isAllSelected()) {
      const selection = this.selection.selected.map(s => s);
      this.selection.clear();
      this.selectSoccerTeams = this.selectSoccerTeams.filter(function (s) {
        return selection.indexOf(s) < 0;
      });
    } else {
      for (const row of this.soccerTeams) {
        this.selection.select(row.id);
      }
      const selection = this.selectSoccerTeams.map(s => s);
      for (const s of this.selection.selected) {
        if (selection.indexOf(s) < 0) {
          this.selectSoccerTeams.push(s);
        }
      }
    }
    this.onShowNameSoccerTeam();
  }

  onSelection(id: string) {
    this.selection.toggle(id);
    const checked = this.selection.selected.indexOf(id) >= 0;
    if (checked) {
      const removeIndex = this.selectSoccerTeams.indexOf(id);
      if (removeIndex < 0) {
        this.selectSoccerTeams.push(id);
      }
    } else {
      const removeIndex = this.selectSoccerTeams.indexOf(id);
      if (removeIndex >= 0) {
        this.selectSoccerTeams.splice(removeIndex, 1);
      }
    }

    this.onShowNameSoccerTeam();
  }

  onShowNameSoccerTeam() {
    const _this = this;
    const countSoccerTeam = this.soccerTeamAlls.filter(function (team) {
      return _this.selectSoccerTeams.indexOf(team.soccerTeamId) >= 0;
    }).length;
    this.showSoccerTeam = String.Format(
      this.tran.translate(this.translateKey.COMPETITION_LIST.LABEL_SOCCER_CHOOSE), countSoccerTeam);
  }

  destroyAction() {
    this.store.dispatch({ type: FailCompetitionCallAction.TYPE });
    this.store.dispatch({ type: SuccessGetSoccerTeamAction.TYPE });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
    this.destroyAction();
  }
}

import {
  Component,
} from '@angular/core';
import {
  Title,
} from '@angular/platform-browser';
import {
  Store,
} from '@ngrx/store';
import {
  Subscription,
} from 'rxjs';
import {
  MatDialog,
  MatTableDataSource,
} from '@angular/material';
import {
  ActivatedRoute,
  Router,
} from '@angular/router';
import {
  IBaseComponent,
} from '../../../../../bases';
import {
  ToastrHelper,
  SpinnerHelper,
  PermissionActionHelper,
  AlertHelper,
} from '../../../../../helpers';
import {
  AppState,
} from '../../../../../AppState';
import {
  TranslateService,
} from '../../../../../services';
import {
  TranslateKeyConstants,
  RouterConstants,
} from '../../../../../constants';
import {
  CompetitionSearch,
  Pagination,
  InfoResult,
  CompetitionsInfo,
  ErrorResult,
  CompetitionTeamModel,
  CompetitionTeamInfoModel,
} from '../../../../../models';
import {
  FailCompetitionCallAction,
  GetListCompetitionsAction,
  SuccessGetListCompetitionsAction,
  GetCompetitionsConfigInfoAction,
  SuccessGetCompetitionsConfigInfoAction,
  UpdateSoccerTeamCompetition,
  SuccessUpdateSoccerTeamCompetition,
} from '../../../actions';
import {
  ActionType,
} from '../../../../../enums';
import {
  PopupAddSoccerTeamComponent,
} from '..';
import {
  FormSizeConstants,
} from '../../../../../constants';
import {
  Location,
} from '@angular/common';

@Component({
  selector: 'app-competitions-config',
  templateUrl: './index.html',
  styleUrls: ['./index.scss'],
})
export class CompetitionConfigComponent implements IBaseComponent {
  subscriptions: Subscription[] = [];
  public translateKey: any;
  public teamlist: CompetitionTeamModel[] = [];
  public modelSearch: CompetitionSearch = new CompetitionSearch();
  public pagination: Pagination = new Pagination();
  public competitions: CompetitionsInfo[] = [];
  public permissionAction: any = {
    COMPETITION_CONFIG_TEAM: false,
  };
  public dataSource: MatTableDataSource<any>;
  public length: any;
  public selectedCompetitionId: string;
  public lengthTeam: Number = 0;
  public location: Location;

  constructor(
    private store: Store<AppState>,
    private spinner: SpinnerHelper,
    private toastr: ToastrHelper,
    private router: Router,
    title: Title,
    public dialog: MatDialog,
    private translate: TranslateService,
    private route: ActivatedRoute,
    public permissionActionHelper: PermissionActionHelper,
    private alert: AlertHelper,
    location: Location,
  ) {
    this.location = location;
    this.translateKey = TranslateKeyConstants;
    title.setTitle(this.translate.translate(this.translateKey.COMPETITION_CONFIG.PAGE_TITLE));
  }

  ngOnInit() {
    this.onSubscribeError();
    this.onSubscribeSuccess();
    this.spinner.show();
    this.store.dispatch(new GetListCompetitionsAction());
  }

  initValidate() {
  }

  onSubscribeSuccess() {
    this.subscriptions.push(this.store.select<InfoResult<Array<CompetitionsInfo>>>(state => state.competitions.getListCompetitionsInfo)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.data !== undefined && result.data !== null) {
            let id: any;
            this.competitions = result.data;
            const paramId = this.route.snapshot.params['id'];
            if (paramId !== undefined && paramId !== null && paramId !== '') {
              id = paramId;
            } else if (this.competitions !== undefined && this.competitions !== null
              && this.competitions.length > 0) {
              id = this.competitions[0].id;
              this.location.replaceState(RouterConstants.competitions.config + '/' + id);
            }
            const selectedCompetition = this.competitions.find(x => x.id === id);
            this.selectedCompetitionId = selectedCompetition === null || selectedCompetition === undefined ? undefined
              : selectedCompetition.id;
            this.onSearch();
          }
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<CompetitionTeamInfoModel>>
      (state => state.competitions.getCompetitionsTeamInfo)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.data !== undefined && result.data !== null && result.data.soccerCompetitionTeams !== undefined
            && result.data.soccerCompetitionTeams !== null) {
            this.teamlist = result.data.soccerCompetitionTeams;
            this.lengthTeam = this.teamlist.length;
            this.dataSource = new MatTableDataSource(this.teamlist);
          }
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<boolean>>
      (state => state.competitions.updateConfigTeam)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.data !== undefined && result.data !== null && result.data) {
          }
          this.toastr.success(result.message);
          this.onBack();
          this.spinner.hide();
        }
      }));
  }

  display(cp?: CompetitionsInfo): string {
    return cp ? cp.title : '';
  }

  onSubmit() {
    if (this.competitions !== undefined && this.selectedCompetitionId !== null
      && this.selectedCompetitionId !== undefined) {

      this.onChangeCompetition();
    } else {
      this.toastr.warning(this.translate.translate(this.translateKey.ROUND_MANAGER.MESSAGE_WARNING_CHOOESE_COMPETITIONS));
    }
  }

  onDelete(item: any) {
    const indexRemove = this.teamlist.indexOf(item);
    this.teamlist.splice(indexRemove, 1);
    this.lengthTeam = this.teamlist.length;
    this.dataSource = new MatTableDataSource(this.teamlist);
  }

  onSave() {
    this.alert.confirmWarning(
      this.translate.translate(this.translateKey.COMPETITION_CONFIG.MESSAGE_CONFIRM_SAVE_SOCCER_TEAM),
      (r: any) => {
        if (r.value) {
          const model = {
            id: this.selectedCompetitionId,
            data: this.teamlist
          };
          this.store.dispatch(new UpdateSoccerTeamCompetition(model));
        }
      });
  }

  onSubscribeError() {
    this.subscriptions.push(this.store.select<ErrorResult>(state => state.competitions.error)
      .subscribe((result) => {
        if (result !== undefined) {
          switch (result.key) {
            case GetListCompetitionsAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
            case GetCompetitionsConfigInfoAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
            case UpdateSoccerTeamCompetition.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
          }
        }
      }));
  }

  onChangeCompetition() {
    this.location.replaceState(RouterConstants.competitions.config + '/' + this.selectedCompetitionId);
    this.onSearch();
  }

  onCheckPermissionAction() {
    this.permissionAction.COMPETITION_CONFIG_TEAM =
      this.permissionActionHelper.isValid(ActionType.COMPETITION_ADD_SOCCER_TEAM);
  }

  onSearch() {
    this.spinner.show();
    this.store.dispatch(new GetCompetitionsConfigInfoAction(this.selectedCompetitionId));
  }

  onAddSoccerTeam(id: string) {
    const dialogRef = this.dialog.open(
      PopupAddSoccerTeamComponent,
      {
        width: FormSizeConstants.WIDTH_MIN,
        data: {
          title: this.translate.translate(this.translateKey.POPUP_ADD_SOCCER_COMPETITION.BUTTON_ADD_COMPETITION_HEADER),
          id: this.selectedCompetitionId,
          soccerTeams: this.teamlist
        },
      });

    dialogRef.afterClosed().subscribe(data => {
      if (data !== undefined && data != null) {
        this.dataSource = new MatTableDataSource(data);
        this.teamlist = data;
        this.lengthTeam = this.teamlist.length;
      }
    });
  }

  onBack() {
    this.router.navigate([RouterConstants.competitions.list]);
  }

  destroyAction() {
    this.store.dispatch({ type: FailCompetitionCallAction.TYPE });
    this.store.dispatch({ type: SuccessGetListCompetitionsAction.TYPE });
    this.store.dispatch({ type: SuccessGetCompetitionsConfigInfoAction.TYPE });
    this.store.dispatch({ type: SuccessUpdateSoccerTeamCompetition.TYPE });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
    this.destroyAction();
  }
}

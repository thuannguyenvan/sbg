import {
  Component,
} from '@angular/core';
import {
  Title,
} from '@angular/platform-browser';
import {
  Store,
} from '@ngrx/store';
import {
  Subscription,
} from 'rxjs';
import {
  MatDialog,
} from '@angular/material';
import {
  Router,
} from '@angular/router';
import {
  IBaseComponent,
} from '../../../../../bases';
import { String } from 'typescript-string-operations';
import {
  ToastrHelper,
  SpinnerHelper,
  AlertHelper,
  PermissionActionHelper,
} from '../../../../../helpers';
import {
  AppState,
} from '../../../../../AppState';
import {
  TranslateService,
} from '../../../../../services';
import {
  TranslateKeyConstants,
  RouterConstants,
} from '../../../../../constants';
import {
  CompetitionSearch,
  Pagination,
  InfoResult,
  CompetitionsInfo,
  ErrorResult,
} from '../../../../../models';
import {
  GetCompetitionsAction,
  FailCompetitionCallAction,
  DeleteCompetitionsAction,
  SuccessGetCompetitionsAction,
  SuccessDeleteCompetitionsAction,
} from '../../../actions';
import {
  ActionType,
} from '../../../../../enums';

@Component({
  selector: 'app-competitions-list',
  templateUrl: './index.html',
  styleUrls: ['./index.scss'],
})
export class CompetitionListComponent implements IBaseComponent {
  subscriptions: Subscription[] = [];
  public translateKey: any;
  public modelSearch: CompetitionSearch = new CompetitionSearch();
  public model: CompetitionSearch = new CompetitionSearch();
  public pagination: Pagination = new Pagination();
  public competitions: CompetitionsInfo[] = [];
  public permissionAction: any = {
    COMPETITION_SEARCH: false,
    COMPETITION_ADD_COMPETITION: false,
    COMPETITION_ADD_SOCCER_TEAM: false,
    COMPETITION_EDIT_COMPETITION: false,
    COMPETITION_MANAGER_STAGE: false,
    COMPETITION_DELETE: false,
  };
  public sortDefault = {
    active: 'Title',
    direction: 'asc'
  };

  constructor(
    private store: Store<AppState>,
    private spinner: SpinnerHelper,
    private toastr: ToastrHelper,
    title: Title,
    public dialog: MatDialog,
    private translate: TranslateService,
    private router: Router,
    private alert: AlertHelper,
    public permissionActionHelper: PermissionActionHelper,
  ) {
    this.translateKey = TranslateKeyConstants;
    title.setTitle(this.translate.translate(this.translateKey.COMPETITION_LIST.PAGE_TITLE));
  }

  ngOnInit() {
    this.onSubscribeError();
    this.onSubscribeSuccess();
    this.modelSearch.orderBy = this.sortDefault.active;
    this.modelSearch.order = this.sortDefault.direction;
    this.modelSearch.page = this.pagination.pageIndex = 1;
    this.modelSearch.size = this.pagination.pageSize;
    this.searchList();
  }

  initValidate() {
  }

  onSubscribeSuccess() {
    this.subscriptions.push(this.store.select<InfoResult<Array<CompetitionsInfo>>>(state => state.competitions.getCompetitionsInfo)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.data !== undefined &&
            result.data !== null
          ) {
            this.competitions = result.data;
            if (this.competitions !== undefined && this.competitions !== null && this.competitions.length > 0) {
              this.competitions.forEach(x => {
                this.pagination.length = x.total;
                if (x.startAt !== undefined && x.endAt !== undefined) {
                  x.timeAt = x.startAt.concat(' - '.concat(x.endAt));
                }
              });
            } else {
              this.pagination.length = 0;
            }
          }
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<any>>(state => state.competitions.deleteCompetition)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.message !== undefined &&
            result.message !== null
          ) {
            this.toastr.success(result.message);
            this.onSearch();
          }
          this.spinner.hide();
        }
      }));
  }

  onSubscribeError() {
    this.subscriptions.push(this.store.select<ErrorResult>(state => state.competitions.error)
      .subscribe((result) => {
        if (result !== undefined) {
          switch (result.key) {
            case GetCompetitionsAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
            case DeleteCompetitionsAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
          }
        }
      }));
  }

  onCheckPermissionAction() {
    this.permissionAction.COMPETITION_SEARCH =
      this.permissionActionHelper.isValid(ActionType.COMPETITION_SEARCH);
    this.permissionAction.COMPETITION_ADD_COMPETITION =
      this.permissionActionHelper.isValid(ActionType.COMPETITION_ADD_COMPETITION);
    this.permissionAction.COMPETITION_ADD_SOCCER_TEAM =
      this.permissionActionHelper.isValid(ActionType.COMPETITION_ADD_SOCCER_TEAM);
    this.permissionAction.COMPETITION_EDIT_COMPETITION =
      this.permissionActionHelper.isValid(ActionType.COMPETITION_EDIT_COMPETITION);
    this.permissionAction.COMPETITION_MANAGER_STAGE =
      this.permissionActionHelper.isValid(ActionType.COMPETITION_MANAGER_STAGE);
    this.permissionAction.COMPETITION_DELETE =
      this.permissionActionHelper.isValid(ActionType.COMPETITION_DELETE);
  }

  public onShowActtionItem(item: CompetitionsInfo, key: string) {
    let checkShow = false;
    switch (key) {
      case 'addTeam':
        checkShow = item.actions.findIndex(x => x === ActionType.COMPETITION_ADD_SOCCER_TEAM) > -1 &&
          this.permissionAction.COMPETITION_ADD_SOCCER_TEAM;
        break;
      case 'editCompetition':
        checkShow = item.actions.findIndex(x => x === ActionType.COMPETITION_EDIT_COMPETITION) > -1 &&
          this.permissionAction.COMPETITION_EDIT_COMPETITION;
        break;
      case 'configStage':
        checkShow = item.actions.findIndex(x => x === ActionType.COMPETITION_MANAGER_STAGE) > -1 &&
          this.permissionAction.COMPETITION_MANAGER_STAGE;
        break;
      case 'deleteCompetition':
        checkShow = item.actions.findIndex(x => x === ActionType.COMPETITION_DELETE) > -1 &&
          this.permissionAction.COMPETITION_DELETE;
        break;
    }
    return checkShow;
  }

  onSearch() {
    this.model.order = this.modelSearch.order;
    this.model.orderBy = this.modelSearch.orderBy;
    this.modelSearch = new CompetitionSearch(this.model);
    this.modelSearch.page = this.pagination.pageIndex = 1;
    this.modelSearch.size = this.pagination.pageSize;
    this.searchList();
  }

  searchList() {
    this.spinner.show();
    this.store.dispatch(new GetCompetitionsAction(this.modelSearch));
  }

  sortData(sort: any) {
    this.modelSearch.orderBy = sort.active;
    this.modelSearch.order = sort.direction;
    this.modelSearch.page = 1;
    this.pagination.pageIndex = 1;
    this.searchList();
  }

  onPageChanged(page: any) {
    this.modelSearch.page = this.pagination.pageIndex = page.pageIndex;
    this.modelSearch.size = this.pagination.pageSize = page.pageSize;
    this.searchList();
  }

  onCreateCompetition() {
    this.router.navigate([RouterConstants.competitions.create]);
  }

  onDelete(competition: CompetitionsInfo) {
    const msg = String.Format(this.translate.translate(
      this.translateKey.COMPETITION_LIST.MESSAGE_CONFIRM_DELETE_COMPETITION), competition.title);
    this.alert.confirmWarning(msg, (r: any) => {
      if (r.value) {
        this.spinner.show();
        this.store.dispatch(new DeleteCompetitionsAction(competition.id));
      }
    });
  }

  onManagerStage(id: string) {
    this.router.navigate([RouterConstants.competitions.stage + '/' + id]);
  }

  onEditCompetition(id: string) {
    this.router.navigate([RouterConstants.competitions.edit + id]);
  }

  onAddSoccerTeam(id: string) {
    this.router.navigate([RouterConstants.competitions.config + id]);
  }

  destroyAction() {
    this.store.dispatch({ type: FailCompetitionCallAction.TYPE });
    this.store.dispatch({ type: SuccessGetCompetitionsAction.TYPE });
    this.store.dispatch({ type: SuccessDeleteCompetitionsAction.TYPE });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
    this.destroyAction();
  }
}

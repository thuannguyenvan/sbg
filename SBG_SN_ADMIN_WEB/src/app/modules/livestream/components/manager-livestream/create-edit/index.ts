import {
  Component,
  Inject,
} from '@angular/core';
import {
  Router,
  ActivatedRoute,
} from '@angular/router';
import {
  Title,
} from '@angular/platform-browser';
import {
  Store,
} from '@ngrx/store';
import {
  Subscription,
} from 'rxjs';
import {
  AppState,
} from '../../../../../AppState';
import {
  FormBuilder,
  Validators,
  FormControl,
  FormGroup,
} from '@angular/forms';
import {
  COMMA,
  ENTER,
} from '@angular/cdk/keycodes';

import {
  GetCreateMatchInfoAction,
  GetInfoCreateLivestreamAction,
  GetInfoEditlivestreamAction,
  GetRoundAction,
  GetMatchAction,
  FailManagerLivestreamCallAction,
  SuccessGetInfoCreateLivestreamAction,
  SuccessGetInfoEditlivestreamAction,
  SuccessGetRoundAction,
  SuccessGetMatchAction,
  SuccessUpdateLivestreamAction,
  SuccessCreateLivestreamAction,
  CreateLivestreamAction,
  UpdateLivestreamAction,
} from '../../../actions';
import {
  SpinnerHelper,
  ToastrHelper,
  ValidateHelper,
  PermissionActionHelper,
} from '../../../../../helpers';
import {
  InfoResult,
  ErrorResult,
  CreateLivestreamInfoModel,
  RoundModel,
  MatchModel,
  LivestreamUrlsModel,
  EditLivestreamInfoModel,
  LivestreamCreateEditModel,
} from '../../../../../models';
import {
  IBaseComponent,
  ValidateBaseComponent,
} from '../../../../../bases';
import {
  TranslateService,
} from '../../../../../services';
import {
  TranslateKeyConstants,
  RouterConstants,
  LenghtValidateConstants,
} from '../../../../../constants';
import {
  ActionType,
} from '../../../../../enums';
import {
  DateTimeHelper,
} from '../../../../../helpers/DateTimeHelper';
import {
  MatTableDataSource,
  MatChipInputEvent,
} from '@angular/material';

@Component({
  selector: 'app-livestream-create-edit',
  templateUrl: './index.html',
  styleUrls: ['./index.scss'],
})
export class CreateEditLivestreamComponent extends ValidateBaseComponent implements IBaseComponent {
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  subscriptions: Subscription[] = [];
  public translateKey: any;
  public modelInfo: any = {};
  public model: LivestreamCreateEditModel = new LivestreamCreateEditModel();
  public permissionAction: any = {
    CREATE_MATCH: false,
    UPDATE_MATCH: false,
  };
  public linkModel: LivestreamUrlsModel = new LivestreamUrlsModel();
  public linkModelOld: LivestreamUrlsModel = new LivestreamUrlsModel();
  public linkList: LivestreamUrlsModel[] = [];
  public show: any = {
    Create: false,
    showBtnUpdate: false,
    createLink: true,
    delete: false,
  };
  public id: string;
  public validatorsLink: any = {};
  public formGroupLink: FormGroup;
  public dataSource: MatTableDataSource<any>;
  public selectedRowIndex: Number = -1;
  public optionDateFull: any = DateTimeHelper.getOptionDateFull();

  constructor(@Inject(FormBuilder) fb: FormBuilder,
    private validateHelper: ValidateHelper,
    private store: Store<AppState>,
    private spinner: SpinnerHelper,
    private router: Router,
    private permissionActionHelper: PermissionActionHelper,
    private route: ActivatedRoute,
    private toastr: ToastrHelper,
    private title: Title,
    private translate: TranslateService,
  ) {
    super(validateHelper);
    this.initValidate(fb);
    this.translateKey = TranslateKeyConstants;
    this.resetValidate();
  }

  ngOnInit() {
    this.onSubscribeError();
    this.onSubscribeSuccess();
    this.spinner.show();
    this.show.createLink = true;
    if (this.router.url.toLowerCase().indexOf(RouterConstants.MANAGER_LIVESTREAM.CREATE_LIVESTREAM.toLowerCase()) > -1) {
      this.show.Create = true;
      this.spinner.show();
      const matchId = this.route.snapshot.queryParams['matchId'];
      this.store.dispatch(new GetInfoCreateLivestreamAction(matchId));
      this.title.setTitle(this.translate.translate(this.translateKey.LIVESTREAM_MANAGER.PAGE_CREATE_TITLE));
    } else {
      /* Form Edit */
      this.show.Create = false;
      this.id = this.route.snapshot.params['id'];
      if (this.id !== undefined && this.id !== null && this.id !== '') {
        this.store.dispatch(new GetInfoEditlivestreamAction(this.id));
      }
      this.title.setTitle(this.translate.translate(this.translateKey.LIVESTREAM_MANAGER.PAGE_UPDATE_TITLE));
    }
  }

  initValidate(fb: FormBuilder) {
    this.validators = {
      title: [Validators.required, Validators.maxLength(LenghtValidateConstants.STRING_MEDIUM)],
      tags: [Validators.maxLength(LenghtValidateConstants.STRING_MEDIUM)],
      description: [Validators.maxLength(LenghtValidateConstants.STRING_MAX)],
      chatUrl: [Validators.maxLength(LenghtValidateConstants.STRING_MEDIUM), this.validateHelper.invalidUrl],
      urlSlug: [Validators.required, Validators.maxLength(LenghtValidateConstants.STRING_MEDIUM), this.validateHelper.invalidUrl],
      startAt: [Validators.required],
      soccerMatchId: [Validators.required],
      status: [Validators.required],
      link: [Validators.maxLength(LenghtValidateConstants.STRING_MEDIUM), Validators.required, this.validateHelper.invalidUrl],
      label: [Validators.maxLength(LenghtValidateConstants.STRING_MEDIUM), Validators.required],
      quanlity: [Validators.required],
    };

    this.formGroupLink = fb.group({
      link: new FormControl(this.linkModel.link, this.validators.link),
      label: new FormControl(this.linkModel.label, this.validators.label),
      quanlity: new FormControl(this.linkModel.quanlity, this.validators.quanlity),
    });

    this.formGroup = fb.group({
      title: new FormControl(this.model.title, this.validators.title),
      soccerMatchId: new FormControl(this.model.soccerMatchId, this.validators.soccerMatchId),
      description: new FormControl(this.model.description, this.validators.description),
      chatUrl: new FormControl(this.model.chatUrl, this.validators.chatUrl),
      urlSlug: new FormControl(this.model.urlSlug, this.validators.urlSlug),
      startAt: new FormControl(this.model.startAt, this.validators.startAt),
      status: new FormControl(this.model.status, this.validators.status),
      tags: new FormControl(this.model.tags, this.validators.tags),
    });
  }

  onSubscribeSuccess() {
    this.subscriptions.push(this.store.select<InfoResult<CreateLivestreamInfoModel>>(state => state.managerLivestream.getCreateInfo)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.data !== undefined && result.data !== null) {
            this.modelInfo.listStatus = result.data.livestreamStatuses;
            this.modelInfo.listCompetiton = result.data.soccerCompetitions;
            this.modelInfo.livestreamVideoUnits = result.data.livestreamVideoUnits;
            this.modelInfo.listConfigAds = result.data.adsConfigurations;
            this.modelInfo.roundList = result.data.soccerRounds;
            this.modelInfo.matchList = result.data.soccerMatchs;
            if (result.data.soccerMatchStream !== undefined && result.data.soccerMatchStream !== null) {
              this.model.soccerCompetitionId = result.data.soccerMatchStream.soccerCompetitionId;
              this.model.soccerMatchId = result.data.soccerMatchStream.soccerMatchId;
              this.model.soccerRoundId = result.data.soccerMatchStream.soccerRoundId;
            }
          }
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<Array<RoundModel>>>(state => state.managerLivestream.getRound)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.data !== undefined && result.data !== null) {
            this.modelInfo.roundList = result.data;
          }
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<Array<MatchModel>>>(state => state.managerLivestream.getMatch)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.data !== undefined && result.data !== null) {
            this.modelInfo.matchList = result.data;
          }
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<boolean>>(state => state.managerLivestream.create)
      .subscribe((result) => {
        if (result !== undefined) {
          this.toastr.success(result.message);
          this.onBack();
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<boolean>>(state => state.managerLivestream.update)
      .subscribe((result) => {
        if (result !== undefined) {
          this.toastr.success(result.message);
          this.onBack();
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<EditLivestreamInfoModel>>(state => state.managerLivestream.getEditInfo)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.data !== undefined && result.data !== null) {
            this.modelInfo.listStatus = result.data.livestreamStatuses;
            this.modelInfo.listCompetiton = result.data.soccerCompetitions;
            this.modelInfo.livestreamVideoUnits = result.data.livestreamVideoUnits;
            this.modelInfo.listConfigAds = result.data.adsConfigurations;
            this.modelInfo.roundList = result.data.soccerRounds;
            this.modelInfo.matchList = result.data.soccerMatchs;
            this.model = result.data.soccerMatchStream;
            if (this.model.livestreamUrls !== undefined && this.model.livestreamUrls !== null) {
              this.linkList = this.model.livestreamUrls.map((d: any) => new LivestreamUrlsModel({
                label: d.label,
                link: d.link,
                quanlity: d.quanlity,
                quanlityTitle: this.onMapQuality(d.quanlity)
              }));
              this.dataSource = new MatTableDataSource(this.linkList);
            }
          }
          this.spinner.hide();
        }
      }));
  }

  onMapQuality(qualityValue: any) {
    if (this.modelInfo.livestreamVideoUnits !== undefined && this.modelInfo.livestreamVideoUnits !== null) {
      const a = this.modelInfo.livestreamVideoUnits.filter(x => x.value === qualityValue);
      return a === undefined || a === null ? null : a[0].title;
    }
  }

  onCreate() {
    if (!this.invalid()) {
      this.spinner.show();
      this.model.livestreamUrls = this.linkList;
      this.store.dispatch(new CreateLivestreamAction(this.model));
    } else {
      this.toastr.warning(this.translate.translate(this.translateKey.SHARE.MESSAGE_INPUT_DATA_INVALID));
    }
  }

  onCancel() {
    this.selectedRowIndex = -1;
    this.show.createLink = true;
    this.linkModel = new LivestreamUrlsModel();
    this.linkModelOld = new LivestreamUrlsModel();
    this.resetValidateForm(this.formGroupLink);
  }

  onSearchRound() {
    if (this.model.soccerCompetitionId !== undefined && this.model.soccerCompetitionId !== null
      && this.model.soccerCompetitionId !== '') {
      this.spinner.show();
      this.model.soccerRoundId = undefined;
      this.model.soccerMatchId = undefined;
      this.modelInfo.roundList = [];
      this.modelInfo.matchList = [];
      this.store.dispatch(new GetRoundAction(this.model.soccerCompetitionId));
    }
  }

  onCreateLink() {
    if (!this.invalidForm(this.formGroupLink)) {
      this.linkList.push(new LivestreamUrlsModel({
        label: this.linkModel.label,
        link: this.linkModel.link,
        quanlity: this.linkModel.quanlity,
        quanlityTitle: this.onMapQuality(this.linkModel.quanlity)
      }));
      this.dataSource = new MatTableDataSource(this.linkList);
      this.linkModel = new LivestreamUrlsModel();
      this.linkModelOld = new LivestreamUrlsModel();
      this.resetValidateForm(this.formGroupLink);
    } else {
      this.toastr.warning(this.translate.translate(this.translateKey.LIVESTREAM_MANAGER.MESSAGE_INPUT_DATA_LINK_INVALID));
    }
  }

  onDeleteLink(data: LivestreamUrlsModel) {
    const index = this.linkList.indexOf(data);
    if (index > -1) {
      this.linkList.splice(index, 1);
    }
    this.dataSource = new MatTableDataSource(this.linkList);
  }

  onEditLink(data: LivestreamUrlsModel, index: any) {
    this.show.delete = true;
    this.selectedRowIndex = index;
    this.show.createLink = false;
    this.linkModel = new LivestreamUrlsModel(data);
    this.linkModelOld = data;
  }

  onSubmitEditLink() {
    if (!this.invalidForm(this.formGroupLink)) {
      this.linkList.push(new LivestreamUrlsModel({
        label: this.linkModel.label,
        link: this.linkModel.link,
        quanlity: this.linkModel.quanlity,
        quanlityTitle: this.onMapQuality(this.linkModel.quanlity)
      }));
      this.onDeleteLink(this.linkModelOld);
      this.linkModelOld = new LivestreamUrlsModel();
      this.dataSource = new MatTableDataSource(this.linkList);
      this.show.createLink = true;
      this.selectedRowIndex = -1;
      this.show.delete = false;
      this.linkModel = new LivestreamUrlsModel();
      this.resetValidateForm(this.formGroupLink);
    } else {
      this.toastr.warning(this.translate.translate(this.translateKey.LIVESTREAM_MANAGER.MESSAGE_INPUT_DATA_LINK_INVALID));
    }
  }

  onSearchMatch() {
    if (this.model.soccerRoundId !== undefined && this.model.soccerRoundId !== null
      && this.model.soccerRoundId !== '') {
      this.spinner.show();
      this.model.soccerMatchId = undefined;
      this.modelInfo.matchList = [];
      this.store.dispatch(new GetMatchAction(this.model.soccerRoundId));
    }
  }

  // distinct array
  onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }

  onSave() {
    if (!this.invalid()) {
      this.spinner.show();
      this.model.livestreamUrls = this.linkList;
      const model = {
        id: this.id,
        data: this.model
      };
      this.store.dispatch(new UpdateLivestreamAction(model));
    } else {
      this.toastr.warning(this.translate.translate(this.translateKey.SHARE.MESSAGE_INPUT_DATA_INVALID));
    }
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    const a = new LivestreamCreateEditModel();
    if ((value || '').trim()) {
      // Add our fruit
      if (value.length <= LenghtValidateConstants.STRING_MEDIUM) {
        this.model.tags.push(value);
        // Reset the input value
        if (input) {
          input.value = '';
        }
      }
    }
  }

  remove(tag: string): void {
    const index = this.model.tags.indexOf(tag);

    if (index >= 0) {
      this.model.tags.splice(index, 1);
    }
  }

  onBack() {
    this.router.navigate([RouterConstants.MANAGER_LIVESTREAM.LIST_STREAM]);
  }

  onCheckPermissionAction() {
    this.permissionAction.CREATE_MATCH = this.permissionActionHelper.isValid(ActionType.MATCH_ADD);
    this.permissionAction.UPDATE_MATCH = this.permissionActionHelper.isValid(ActionType.MATCH_UPDATE);
  }

  onSubscribeError() {
    this.subscriptions.push(this.store.select<ErrorResult>(state => state.managerLivestream.error)
      .subscribe((result) => {
        if (result !== undefined) {
          switch (result.key) {
            case GetCreateMatchInfoAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
            case GetInfoEditlivestreamAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
            case GetRoundAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
            case GetMatchAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
            case CreateLivestreamAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.onShowErrorByKey(result, 'livestreamUrls');
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
            case UpdateLivestreamAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.onShowErrorByKey(result, 'livestreamUrls');
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
          }
        }
      }));
  }

  onShowErrorByKey(errorResult: ErrorResult, keyError: any) {
    if (errorResult && errorResult.errors && errorResult.errors.length > 0) {
      errorResult.errors.forEach(e => {
        if (e.key === keyError) {
          this.toastr.error(e.message);
        }
      });
    }
  }

  destroyAction() {
    this.store.dispatch({ type: FailManagerLivestreamCallAction.TYPE });
    this.store.dispatch({ type: SuccessGetInfoCreateLivestreamAction.TYPE });
    this.store.dispatch({ type: SuccessGetInfoEditlivestreamAction.TYPE });
    this.store.dispatch({ type: SuccessGetRoundAction.TYPE });
    this.store.dispatch({ type: SuccessGetMatchAction.TYPE });
    this.store.dispatch({ type: SuccessCreateLivestreamAction.TYPE });
    this.store.dispatch({ type: SuccessUpdateLivestreamAction.TYPE });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
    this.destroyAction();
  }
}

import {
  Component,
  Inject,
} from '@angular/core';
import {
  Router,
} from '@angular/router';
import {
  DOCUMENT,
} from '@angular/common';
import {
  Title,
} from '@angular/platform-browser';
import {
  Store,
} from '@ngrx/store';
import {
  Subscription,
} from 'rxjs';
import {
  MatTableDataSource,
} from '@angular/material';
import {
  AppState,
} from '../../../../../AppState';
import {
  SuccessGetCompetitionsForLTAction,
  GetCompetitionsForLTAction,
  GetRoundAction,
  GetMatchAction,
  SuccessGetRoundAction,
  SuccessGetMatchAction,
  SuccessSearchAction,
  SearchAction,
  GetSearchLTInfoAction,
  DeleteLivestreamAction,
  FailManagerLivestreamCallAction,
  SuccessDeleteLivestreamAction,
  SuccessGetSearchLTInfoAction,
} from '../../../actions';
import {
  SpinnerHelper,
  ToastrHelper,
  ValidateHelper,
  PermissionActionHelper,
  AlertHelper,
} from '../../../../../helpers';
import {
  InfoResult,
  ErrorResult,
  CompetitionsInfo,
  RoundModel,
  MatchModel,
  Pagination,
  LivestreamModel,
  SearchInfoLTModel,
  SoccerMatchStatuses,
  SearchLiveStreamModel,
} from '../../../../../models';
import {
  IBaseComponent,
  ValidateBaseComponent,
} from '../../../../../bases';
import {
  TranslateService,
} from '../../../../../services';
import {
  TranslateKeyConstants,
  LenghtValidateConstants,
  RouterConstants,
} from '../../../../../constants';
import {
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import {
  ActionType,
} from '../../../../../enums';
import {
  String,
} from 'typescript-string-operations';

import {
  DateTimeHelper,
} from '../../../../../helpers/DateTimeHelper';

@Component({
  selector: 'app-livestream-list',
  templateUrl: './index.html',
  styleUrls: ['./index.scss'],
})
export class ManagerLiveStreamComponent extends ValidateBaseComponent implements IBaseComponent {
  subscriptions: Subscription[] = [];
  public translateKey: any;
  public competitionsInfo: CompetitionsInfo[] = [];
  public competitionList: CompetitionsInfo[] = [];
  public roundList: RoundModel[] = [];
  public matchList: MatchModel[] = [];
  public statusList: SoccerMatchStatuses[] = [];
  public listLivestream: LivestreamModel[] = [];
  public model: any = {};
  public modelSearch: any = {};
  public pagination: Pagination = new Pagination();
  public dataSource: MatTableDataSource<any>;
  public permissionAction: any = {
    LIVESTREAM_ADD: false,
    LIVESTREAM_SEARCH: false,
    LIVESTREAM_UPDATE: false,
    LIVESTREAM_DELETE: false,
  };
  public isInitialize: Boolean = true;
  public optionDateOnly: any = DateTimeHelper.getOptionDateOnly();
  public sortDefault = {
    active: 'Title',
    direction: 'asc'
  };

  constructor(
    @Inject(FormBuilder) fb: FormBuilder,
    private router: Router,
    @Inject(DOCUMENT) private document: any,
    private store: Store<AppState>,
    private spinner: SpinnerHelper,
    private toastr: ToastrHelper,
    public permissionActionHelper: PermissionActionHelper,
    title: Title,
    private translate: TranslateService,
    private validateHelper: ValidateHelper,
    private alert: AlertHelper,
  ) {
    super(validateHelper);
    this.initValidate(fb);
    this.clearValidate();
    this.translateKey = TranslateKeyConstants;
    title.setTitle(this.translate.translate(this.translateKey.LIVESTREAM_MANAGER.PAGE_LIST_TITLE));
  }

  ngOnInit() {
    this.onSubscribeError();
    this.onSubscribeSuccess();
    this.isInitialize = true;
    this.spinner.show();
    this.store.dispatch(new GetSearchLTInfoAction());
  }

  onSubscribeSuccess() {
    this.subscriptions.push(this.store.select<InfoResult<SearchInfoLTModel>>(state => state.managerLivestream.getSearchInfo)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.data !== undefined && result.data !== null) {
            const selectCompetitionDefault = new CompetitionsInfo(
              {
                id: '',
                title: this.translate.translate(this.translateKey.LIVESTREAM_MANAGER.SELECT_ALL)
              });
            const selectStatusDefault = new SoccerMatchStatuses(
              {
                value: '',
                title: this.translate.translate(this.translateKey.LIVESTREAM_MANAGER.SELECT_ALL)
              });
            this.competitionList = [selectCompetitionDefault];
            this.statusList = [selectStatusDefault];
            this.statusList = this.statusList.concat(result.data.livestreamStatuses);
            this.competitionList = this.competitionList.concat(result.data.soccerCompetitions);
            this.model.soccerCompetitionId = '';
            this.model.soccerMatchStatus = '';
            this.model.livestreamStatus = '';
            this.model.orderBy = this.modelSearch.orderBy = this.sortDefault.active;
            this.model.order = this.modelSearch.order = this.sortDefault.direction;
            this.onSubmit();
          }
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<Array<RoundModel>>>(state => state.managerLivestream.getRound)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.data !== undefined && result.data !== null) {
            const selectRoundDefault = new RoundModel(
              {
                id: '',
                title: this.translate.translate(this.translateKey.LIVESTREAM_MANAGER.SELECT_ALL)
              });
            this.model.soccerRoundId = '';
            this.roundList = [selectRoundDefault];
            this.roundList = this.roundList.concat(result.data);
          }
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<Array<MatchModel>>>(state => state.managerLivestream.getMatch)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.data !== undefined && result.data !== null) {
            const selectMatchDefault = new MatchModel(
              {
                soccerMatchId: '',
                title: this.translate.translate(this.translateKey.LIVESTREAM_MANAGER.SELECT_ALL)
              });
            this.matchList = [selectMatchDefault];
            this.matchList = this.matchList.concat(result.data);
          }
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<Array<LivestreamModel>>>(state => state.managerLivestream.Search)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.data !== undefined && result.data !== null) {
            if (result.data.length > 0) {
              this.pagination.length = result.data[0].total;
            } else {
              this.pagination.length = 0;
            }
            this.listLivestream = result.data;
            this.dataSource = new MatTableDataSource(this.listLivestream);
          }
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<any>>
      (state => state.managerLivestream.delete)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.message !== undefined &&
            result.message !== null
          ) {
            this.toastr.success(result.message);
            this.onCheckPermissionAction();
            this.onSearch();
          }
          this.spinner.hide();
        }
      }));
  }

  initValidate(fb: FormBuilder) {
    this.validators = {
      title: [Validators.maxLength(LenghtValidateConstants.STRING_MEDIUM)],
      competions: [Validators.required],
      dateStartAt: [this.validateHelper.dateTimeValidator]
    };

    this.formGroup = fb.group({
      competions: new FormControl(this.validators.competions),
      title: new FormControl(this.model.title, this.validators.title),
      dateStartAt: new FormControl(this.validators.dateStartAt),
    });
  }

  onCheckPermissionAction() {
    this.permissionAction.LIVESTREAM_ADD =
      this.permissionActionHelper.isValid(ActionType.LIVESTREAM_ADD);
    this.permissionAction.LIVESTREAM_UPDATE =
      this.permissionActionHelper.isValid(ActionType.LIVESTREAM_UPDATE);
    this.permissionAction.LIVESTREAM_DELETE =
      this.permissionActionHelper.isValid(ActionType.LIVESTREAM_DELETE);
    this.permissionAction.LIVESTREAM_SEARCH =
      this.permissionActionHelper.isValid(ActionType.LIVESTREAM_SEARCH);
  }

  onShowActtionItem(item: LivestreamModel, key: string) {
    let checkShow = false;
    switch (key) {
      case 'updateLivestream':
        checkShow = item.actions.findIndex(x => x === ActionType.LIVESTREAM_UPDATE) > -1 &&
          this.permissionAction.LIVESTREAM_UPDATE;
        break;
      case 'deleteLivestream':
        checkShow = item.actions.findIndex(x => x === ActionType.LIVESTREAM_DELETE) > -1 &&
          this.permissionAction.LIVESTREAM_DELETE;
        break;
    }
    return checkShow;
  }

  onSearch() {
    this.spinner.show();
    this.store.dispatch(new SearchAction(this.modelSearch));
  }

  onSubmit() {
    this.model.orderBy = this.modelSearch.orderBy;
    this.model.order = this.modelSearch.order;
    this.modelSearch = new SearchLiveStreamModel(this.model);
    this.modelSearch.page = this.pagination.pageIndex = 1;
    this.modelSearch.size = this.pagination.pageSize;
    this.onSearch();
  }

  sortData(sort: any) {
    this.modelSearch.orderBy = sort.active;
    this.modelSearch.order = sort.direction;
    this.modelSearch.page = 1;
    this.pagination.pageIndex = 1;
    this.onSearch();
  }

  onSearchRound() {
    if (this.model.soccerCompetitionId !== undefined && this.model.soccerCompetitionId !== null
      && this.model.soccerCompetitionId !== '') {
      this.spinner.show();
      this.store.dispatch(new GetRoundAction(this.model.soccerCompetitionId));
    } else {
      this.model.soccerRoundId = undefined;
      this.model.soccerMatchId = undefined;
      this.roundList = [];
      this.matchList = [];
    }
  }

  onSearchMatch() {
    if (this.model.soccerRoundId !== undefined && this.model.soccerRoundId !== null
      && this.model.soccerRoundId !== '') {
      this.spinner.show();
      this.store.dispatch(new GetMatchAction(this.model.soccerRoundId));
    } else {
      this.model.soccerMatchId = undefined;
      this.matchList = [];
    }
  }

  onEdit(item: LivestreamModel) {
    this.router.navigate([RouterConstants.MANAGER_LIVESTREAM.EDIT_LIVESTREAM + '/' + item.id]);
  }

  onCreate() {
    this.router.navigate([RouterConstants.MANAGER_LIVESTREAM.CREATE_LIVESTREAM]);
  }

  onPageChanged(page: any) {
    this.modelSearch.page = this.pagination.pageIndex = page.pageIndex;
    this.modelSearch.size = this.pagination.pageSize = page.pageSize;
    this.onSearch();
  }

  onDelete(item: LivestreamModel) {
    const msg = String.Format(this.translate.translate(
      this.translateKey.LIVESTREAM_MANAGER.QUESTION_DELETE_LIVESTREAM), item.title);
    this.alert.confirmWarning(msg, (r: any) => {
      if (r.value) {
        this.store.dispatch(new DeleteLivestreamAction(item.id));
      }
    });
  }

  onSubscribeError() {
    this.subscriptions.push(this.store.select<ErrorResult>(state => state.managerLivestream.error)
      .subscribe((result) => {
        if (result !== undefined) {
          switch (result.key) {
            case GetCompetitionsForLTAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
            case GetRoundAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
            case GetMatchAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
            case SearchAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
            case DeleteLivestreamAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
            case GetSearchLTInfoAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
          }
        }
      }));
  }

  destroyAction() {
    this.store.dispatch({ type: FailManagerLivestreamCallAction.TYPE });
    this.store.dispatch({ type: SuccessGetSearchLTInfoAction.TYPE });
    this.store.dispatch({ type: SuccessGetCompetitionsForLTAction.TYPE });
    this.store.dispatch({ type: SuccessGetRoundAction.TYPE });
    this.store.dispatch({ type: SuccessGetMatchAction.TYPE });
    this.store.dispatch({ type: SuccessSearchAction.TYPE });
    this.store.dispatch({ type: SuccessDeleteLivestreamAction.TYPE });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
    this.destroyAction();
  }
}

import {
  Component,
} from '@angular/core';
import {
  ActivatedRoute,
} from '@angular/router';
import {
  Title,
} from '@angular/platform-browser';
import {
  Store,
} from '@ngrx/store';
import {
  Subscription,
} from 'rxjs';
import {
  MatDialog,
  MatTableDataSource,
} from '@angular/material';
import {
  AppState,
} from '../../../../../AppState';
import {
  FailManagerStageCallAction,
  GetCompetitionsInfoAction,
  SuccessGetCompetitionsInfoAction,
  GetListStageAction,
  CreateStandingAction,
  DeleteStandingAction,
  SuccessCreateStandingAction,
  SuccessDeleteStandingAction,
  SuccessGetListStageAction,
} from '../../../actions';
import {
  SpinnerHelper,
  ToastrHelper,
  AlertHelper,
  PermissionActionHelper,
} from '../../../../../helpers';
import {
  InfoResult,
  ErrorResult,
  CompetitionsInfo,
  CompetitionStageStandingModel,
} from '../../../../../models';
import {
  IBaseComponent,
} from '../../../../../bases';
import {
  TranslateService,
} from '../../../../../services';
import {
  TranslateKeyConstants,
  FormSizeConstants,
  RouterConstants,
} from '../../../../../constants';
import {
  PopupManagerGroupComponent,
  PopupUpdateStandingComponent,
} from '..';
import {
  ActionType,
} from '../../../../../enums';
import {
  Location,
} from '@angular/common';

@Component({
  selector: 'app-stage-list',
  templateUrl: './index.html',
  styleUrls: ['./index.scss'],
})
export class ManagerStageComponent implements IBaseComponent {
  subscriptions: Subscription[] = [];
  public translateKey: any;
  public dataSource: MatTableDataSource<any>;
  public competitionsInfo: CompetitionsInfo[] = [];
  public CompetitionStageStandingModel: CompetitionStageStandingModel[] = [];
  public competitionsId: string;
  public permissionAction: any = {
    MANAGE_STANDING_VIEW: false,
    UPDATE_STANDING_VIEW: false,
    CREATE_STANDING_VIEW: false,
    DELETE_STANDING_VIEW: false,
  };
  public initPage: Boolean = false;
  public location: Location;

  constructor(
    private store: Store<AppState>,
    private spinner: SpinnerHelper,
    private alert: AlertHelper,
    private route: ActivatedRoute,
    private toastr: ToastrHelper,
    public dialog: MatDialog,
    private permissionActionHelper: PermissionActionHelper,
    title: Title,
    private translate: TranslateService,
    location: Location,
  ) {
    this.location = location;
    this.translateKey = TranslateKeyConstants;
    title.setTitle(this.translate.translate(this.translateKey.STAGE_MANAGER.PAGE_TITLE));
  }

  ngOnInit() {
    this.onSubscribeError();
    this.onSubscribeSuccess();
    this.spinner.show();
    this.initPage = true;
    this.store.dispatch(new GetCompetitionsInfoAction());
  }

  initValidate() {
  }

  onSubscribeSuccess() {
    this.subscriptions.push(this.store.select<InfoResult<Array<CompetitionsInfo>>>(state => state.standing.getCompetitionsInfo)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.data !== undefined && result.data !== null) {
            this.competitionsInfo = result.data;
            let id = this.route.snapshot.params.id;
            if (id === undefined || id === null) {
              id = this.competitionsInfo.length > 0 ? this.competitionsInfo[0].id : id;
            }
            this.competitionsId = id;
            if (this.competitionsId !== undefined && this.competitionsId !== null) {
              this.location.replaceState(RouterConstants.competitions.stage + '/' + this.competitionsId);
              this.onSearch();
            }
          }
        }
        this.spinner.hide();
      }));

    this.subscriptions.push(this.store.select<InfoResult<Array<CompetitionStageStandingModel>>>(state => state.standing.getListStage)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.data !== undefined && result.data !== null) {
            this.CompetitionStageStandingModel = result.data;
            this.dataSource = new MatTableDataSource(this.CompetitionStageStandingModel);
          }
        }
        this.spinner.hide();
      }));

    this.subscriptions.push(this.store.select<InfoResult<Boolean>>(state => state.standing.createStanding)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.data !== undefined && result.data !== null) {
            if (result.data) {
              this.toastr.success(result.message);
              this.onSearch();
            }
            this.toastr.error(result.message);
          }
        }
        this.spinner.hide();
      }));

    this.subscriptions.push(this.store.select<InfoResult<Boolean>>(state => state.standing.deleteStanding)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.data !== undefined && result.data !== null) {
            if (result.data) {
              this.toastr.success(result.message);
              this.onSearch();
            }
            this.toastr.error(result.message);
          }
        }
        this.spinner.hide();
      }));
  }

  onShow() {
    this.onShowMagagerStanding(new CompetitionStageStandingModel());
  }

  onShowMagagerStanding(stage: CompetitionStageStandingModel): void {
    if (stage != null) {
      const dialogRef = this.dialog.open(PopupManagerGroupComponent, {
        width: FormSizeConstants.WIDTH_MEDIUM,
        data: {
          dataObject: stage
        },
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result !== undefined && result != null) {
          this.onSearch();
        }
      });
    }
  }

  onCompetitionSelectionChange() {
    this.location.replaceState(RouterConstants.competitions.stage + '/' + this.competitionsId);
    this.onSearch();
  }

  onSearch() {
    this.spinner.show();
    this.store.dispatch(new GetListStageAction(this.competitionsId));
  }

  onUpdateStanding(stage: CompetitionStageStandingModel) {
    if (stage != null) {
      const dialogRef = this.dialog.open(PopupUpdateStandingComponent, {
        width: FormSizeConstants.WIDTH_MEDIUM,
        data: {
          dataObject: stage,
          competitionsId: this.competitionsId
        },
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result !== undefined && result != null) {
          this.onSearch();
        }
      });
    }
  }

  onShowActtionItem(item: CompetitionStageStandingModel, key: string) {
    let checkShow = false;
    switch (key) {
      case 'manage':
        checkShow = item.actions.findIndex(x => x === ActionType.MANAGE_STANDING_VIEW) > -1 &&
          this.permissionAction.MANAGE_STANDING_VIEW;
        break;
      case 'edit':
        checkShow = item.actions.findIndex(x => x === ActionType.UPDATE_STANDING_VIEW) > -1 &&
          this.permissionAction.UPDATE_STANDING_VIEW;
        break;
      case 'create':
        checkShow = item.actions.findIndex(x => x === ActionType.CREATE_STANDING_VIEW) > -1 &&
          this.permissionAction.CREATE_STANDING_VIEW;
        break;
      case 'delete':
        checkShow = item.actions.findIndex(x => x === ActionType.DELETE_STANDING_VIEW) > -1 &&
          this.permissionAction.DELETE_STANDING_VIEW;
        break;
    }
    return checkShow;
  }

  onCheckPermissionAction() {
    this.permissionAction.MANAGE_STANDING_VIEW =
      this.permissionActionHelper.isValid(ActionType.MANAGE_STANDING_VIEW);
    this.permissionAction.CREATE_STANDING_VIEW =
      this.permissionActionHelper.isValid(ActionType.CREATE_STANDING_VIEW);
    this.permissionAction.DELETE_STANDING_VIEW =
      this.permissionActionHelper.isValid(ActionType.DELETE_STANDING_VIEW);
    this.permissionAction.UPDATE_STANDING_VIEW =
      this.permissionActionHelper.isValid(ActionType.UPDATE_STANDING_VIEW);
  }

  onCreateStanding(id: string) {
    this.alert.confirmWarning(this.translate.translate(this.translateKey.STAGE_MANAGER.QUESTION_CREATE_GROUP), (r: any) => {
      if (r.value) {
        this.spinner.show();
        this.store.dispatch(new CreateStandingAction(id));
      }
    });
  }

  onDeleteStanding(id: string) {
    this.alert.confirmWarning(this.translate.translate(this.translateKey.STAGE_MANAGER.QUESTION_DELETE_GROUP), (r: any) => {
      if (r.value) {
        this.spinner.show();
        this.store.dispatch(new DeleteStandingAction(id));
      }
    });
  }

  onSubscribeError() {
    this.subscriptions.push(this.store.select<ErrorResult>(state => state.standing.error)
      .subscribe((result) => {
        if (result !== undefined) {
          switch (result.key) {
            case GetCompetitionsInfoAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
            case GetListStageAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
            case CreateStandingAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
            case DeleteStandingAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
          }
        }
      }));
  }

  destroyAction() {
    this.store.dispatch({ type: FailManagerStageCallAction.TYPE });
    this.store.dispatch({ type: SuccessGetCompetitionsInfoAction.TYPE });
    this.store.dispatch({ type: SuccessGetListStageAction.TYPE });
    this.store.dispatch({ type: SuccessCreateStandingAction.TYPE });
    this.store.dispatch({ type: SuccessDeleteStandingAction.TYPE });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
    this.destroyAction();
  }
}

import {
  Component,
  Inject,
} from '@angular/core';
import {
  Store,
} from '@ngrx/store';
import {
  Subscription,
} from 'rxjs';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatTableDataSource,
} from '@angular/material';
import {
  AppState,
} from '../../../../../AppState';
import {
  SpinnerHelper,
  ToastrHelper,
} from '../../../../../helpers';
import {
  UpdateStageStandingAction,
  SuccessUpdateStageStandingAction,
  FailManagerStageCallAction,
  GetTeamInfoByStageIdAction,
  SuccessGetTeamInfoByStageIdAction,
} from '../../../actions';
import {
  InfoResult,
  ErrorResult,
  CompetitionTeamModel,
  CompetitionStageTeamModel,
  TeamStageInfoModel,
} from '../../../../../models';
import {
  IBaseComponent,
} from '../../../../../bases';
import {
  TranslateKeyConstants,
} from '../../../../../constants';


@Component({
  selector: 'app-popup-manager-stage',
  templateUrl: './index.html',
  styleUrls: ['./index.scss'],
})
export class PopupManagerGroupComponent implements IBaseComponent {
  subscriptions: Subscription[] = [];
  public translateKey: any;
  public headerPopup: string;
  public teamInfo: TeamStageInfoModel;
  public competitionTeamList: CompetitionTeamModel[] = [];
  public competitionTeamSelect: CompetitionTeamModel;
  public dataSource: MatTableDataSource<any>;
  public competitionStageTeamList: Array<CompetitionStageTeamModel> = [];
  public hidePoup: boolean;
  public btnDisable: boolean;
  public btnAddDisable: boolean;
  public btnAddAllDisable: boolean;
  public selectedTeam: CompetitionTeamModel;

  constructor(
    private store: Store<AppState>,
    private spinner: SpinnerHelper,
    private toastr: ToastrHelper,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<PopupManagerGroupComponent>,

    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.translateKey = TranslateKeyConstants;
  }

  ngOnInit() {
    this.hidePoup = true;
    this.onSubscribeError();
    this.onSubscribeSuccess();
    if (this.data !== null) {
      setTimeout(() => {
        this.spinner.show();
        this.headerPopup = this.data.dataObject.title;
        this.store.dispatch(new GetTeamInfoByStageIdAction(this.data.dataObject.id));
      }, 100);
    }
  }

  initValidate() {
  }

  onSubscribeSuccess() {
    this.subscriptions.push(this.store.select<InfoResult<TeamStageInfoModel>>
      (state => state.standing.getTeamInfoByStageId)
      .subscribe((result) => {
        if (result !== undefined) {
          this.hidePoup = false;
          if (result.data !== undefined && result.data !== null) {
            this.teamInfo = result.data;
            if ((result.data.competitionStageTeams === undefined || result.data.competitionStageTeams === null
                  || result.data.competitionStageTeams.length < 0)
                     && (result.data.competitionTeams === null || result.data.competitionTeams === null
                        || result.data.competitionStageTeams.length < 0)) {
                             this.btnDisable = true;
            } else {
              this.btnDisable = false;
            }
            this.competitionTeamList = this.teamInfo.competitionTeams;
            this.competitionStageTeamList = this.teamInfo.competitionStageTeams;
            // Bỏ đội bóng trùng của giải đấu và đội trong vòng đấu
            if (this.competitionTeamList !== undefined && this.competitionTeamList !== null
              && this.competitionStageTeamList.length > 0 && this.competitionTeamList.length > 0) {
              for (let i = 0; i < this.competitionStageTeamList.length; i++) {
                for (let j = 0; j < this.competitionTeamList.length; j++) {
                  if (this.competitionStageTeamList[i].soccerTeamId === this.competitionTeamList[j].soccerTeamId) {
                    this.competitionTeamList.splice(j, 1);
                  }
                }
              }
            }
            this.dataSource = new MatTableDataSource(this.competitionStageTeamList);
            this.onCheckShow();
          }
        }
        this.spinner.hide();
      }));

    this.subscriptions.push(this.store.select<InfoResult<Boolean>>
      (state => state.standing.updateStandingStage)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.data !== undefined && result.data !== null) {
            if (result.data) {
              this.toastr.success(result.message);
              this.dialogRef.close(result.data);
              this.spinner.hide();
            }
            this.toastr.error(result.message);
          }
        }
        this.spinner.hide();
      }));
  }

  onSubscribeError() {
    this.subscriptions.push(this.store.select<ErrorResult>(state => state.standing.error)
      .subscribe((result) => {
        if (result !== undefined) {
          switch (result.key) {
            case GetTeamInfoByStageIdAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              this.dialogRef.close();
              break;
            case UpdateStageStandingAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
          }
        }
      }));
  }

  onSelected(value: any) {
    this.competitionTeamSelect = value;
    this.onCheckShow();
  }

  onClose() {
    this.dialogRef.close();
  }

  getOptionGroup() {
    const result = new Array<String>();
     // Tạo danh sách từ A-Z
    for (let i = 65; i <= 90; i++) {
      result.push(String.fromCharCode(i));
    }
    return result;
  }

  onAdd() {
    if (this.competitionTeamSelect !== undefined && this.competitionTeamSelect !== null) {
      const index = this.competitionTeamList.indexOf(this.competitionTeamSelect);
      this.competitionTeamList.splice(index, 1);
      const object = {
        soccerCompetitionStageId: this.data.dataObject.soccerCompetitionStageId,
        soccerCompetitionTeamId: this.competitionTeamSelect.id,
        soccerCompetitionId: this.competitionTeamSelect.soccerCompetitionId,
        soccerTeamId: this.competitionTeamSelect.soccerTeamId,
        group: undefined, // Chọn group rỗng
        name: this.competitionTeamSelect.name,
        logo: this.competitionTeamSelect.logo,
        teamCode: this.competitionTeamSelect.teamCode,
        id: this.competitionTeamSelect.id,
      };
      this.competitionStageTeamList.push(new CompetitionStageTeamModel(object));
    }
    this.selectedTeam = undefined;
    this.competitionTeamSelect = null;
    this.dataSource = new MatTableDataSource(this.competitionStageTeamList);
    this.onCheckShow();
  }

  onCheckShow() {
    if (this.competitionTeamList !== undefined && this.competitionTeamList !== null
      && this.competitionTeamList.length > 0) {
        this.btnAddAllDisable = false;
         if (this.competitionTeamSelect !== undefined && this.competitionTeamSelect !== null) {
          this.btnAddDisable = false;
        } else {
          this.btnAddDisable = true;
        }
      } else {
        this.btnAddDisable = true;
        this.btnAddAllDisable = true;
      }
  }

  onAddAll() {
    if (this.competitionTeamList !== undefined && this.competitionTeamList !== null
      && this.competitionTeamList.length > 0) {
      for (let i = 0; i < this.competitionTeamList.length; i++) {
        const object = {
          soccerCompetitionStageId: this.data.dataObject.soccerCompetitionStageId,
          soccerCompetitionTeamId: this.competitionTeamList[i].id,
          soccerCompetitionId: this.competitionTeamList[i].soccerCompetitionId,
          soccerTeamId: this.competitionTeamList[i].soccerTeamId,
          group: undefined,
          name: this.competitionTeamList[i].name,
          logo: this.competitionTeamList[i].logo,
          teamCode: this.competitionTeamList[i].teamCode,
          id: this.competitionTeamList[i].id,
        };
        this.competitionStageTeamList.push(new CompetitionStageTeamModel(object));
      }
      this.selectedTeam = undefined;
      this.competitionTeamList = [];
      this.competitionTeamSelect = null;
    }
    this.dataSource = new MatTableDataSource(this.competitionStageTeamList);
    this.onCheckShow();
  }

  onOutStage(data: CompetitionStageTeamModel) {
    const index = this.competitionStageTeamList.indexOf(data);
    if (index > -1) {
      this.competitionStageTeamList.splice(index, 1);
    }
    this.competitionTeamList.push(new CompetitionTeamModel({
      id: data.soccerCompetitionTeamId,
      soccerTeamId: data.soccerTeamId,
      soccerCompetitionId: data.soccerCompetitionId,
      name: data.name,
      logo: data.logo,
      teamCode: data.teamCode
    }));
    this.onCheckShow();
    this.dataSource = new MatTableDataSource(this.competitionStageTeamList);
  }

  onSave() {
    const model = {
      soccerCompetitionStageTeams: this.competitionStageTeamList,
      id: this.data.dataObject.id
    };
    this.store.dispatch(new UpdateStageStandingAction(model));
  }

  destroyAction() {
    this.store.dispatch({ type: FailManagerStageCallAction.TYPE });
    this.store.dispatch({ type: SuccessGetTeamInfoByStageIdAction.TYPE });
    this.store.dispatch({ type: SuccessUpdateStageStandingAction.TYPE });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
    this.destroyAction();
  }
}

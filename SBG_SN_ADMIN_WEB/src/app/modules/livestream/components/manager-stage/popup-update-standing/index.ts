import {
  Component,
  Inject
} from '@angular/core';
import {
  Store,
} from '@ngrx/store';
import {
  Subscription,
  from,
} from 'rxjs';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material';
import {
  FormBuilder,
  FormControl,
} from '@angular/forms';
import {
  groupBy,
  mergeMap,
  toArray,
} from 'rxjs/operators';
import {
  AppState,
} from '../../../../../AppState';
import {
  SpinnerHelper,
  ToastrHelper,
  ValidateHelper,
} from '../../../../../helpers';
import {
  UpdateStandingAction,
  SuccessUpdateStandingAction,
  SuccessGetStandingInfoAction,
  FailManagerStageCallAction,
  GetStandingInfoAction,
} from '../../../actions';
import {
  InfoResult,
  ErrorResult,
  StandingModel,
  GroupStandingModel,
  StandingObjectModel,
} from '../../../../../models';
import {
  IBaseComponent,
  ValidateBaseComponent,
} from '../../../../../bases';
import {
  TranslateKeyConstants,
} from '../../../../../constants';
import {
  TranslateService,
} from '../../../../../services';

@Component({
  selector: 'app-popup-update-standing',
  templateUrl: './index.html',
  styleUrls: ['./index.scss'],
})
export class PopupUpdateStandingComponent extends ValidateBaseComponent implements IBaseComponent {
  subscriptions: Subscription[] = [];
  public translateKey: any;
  public standingInfo: StandingModel = new StandingModel();
  public hidePoup: boolean;
  public model: any = {};
  public listGroup: GroupStandingModel[] = [];
  public tables: StandingObjectModel[] = [];
  public headerPopup: string;

  constructor(
    private store: Store<AppState>,
    private spinner: SpinnerHelper,
    private toastr: ToastrHelper,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<PopupUpdateStandingComponent>,
    @Inject(FormBuilder) fb: FormBuilder,
    private validateHelper: ValidateHelper,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private translate: TranslateService,
  ) {
    super(validateHelper);
    this.translateKey = TranslateKeyConstants;
    this.initValidate(fb);
  }

  ngOnInit() {
    this.hidePoup = true;
    this.onSubscribeError();
    this.onSubscribeSuccess();
    if (this.data !== null) {
      this.headerPopup = this.data.dataObject.title;
      setTimeout(() => {
        this.spinner.show();
        this.store.dispatch(new GetStandingInfoAction(this.data.dataObject.id));
      }, 100);
    }
  }

  initValidate(fb: FormBuilder) {

    this.validators = {
      number: [this.validateHelper.numberValidator],
    };

    this.formGroup = fb.group({
      played: new FormControl(this.model.number, this.validators.number),
      won: new FormControl(this.model.number, this.validators.number),
      number: new FormControl(this.model.number, this.validators.number),
      draw: new FormControl(this.model.number, this.validators.number),
      lost: new FormControl(this.model.number, this.validators.number),
      goalFor: new FormControl(this.model.number, this.validators.number),
      goalsAgainst: new FormControl(this.model.number, this.validators.number),
      goalDifference: new FormControl(this.model.number, this.validators.number),
      point: new FormControl(this.model.number, this.validators.number),
    });
  }

  onSubscribeSuccess() {
    this.subscriptions.push(this.store.select<InfoResult<StandingModel>>
      (state => state.standing.getInfoStanding)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.data !== undefined && result.data !== null && result.data.tables !== undefined
                && result.data.tables !== null && result.data.tables.length > 0) {
            this.hidePoup = false;
            this.standingInfo = result.data;
            this.setGroup(this.standingInfo.tables);
          } else {
            this.toastr.warning(this.translate.translate(this.translateKey.STAGE_MANAGER.MESSAGE_UPDATE_NONE_ELEMENT));
            this.spinner.hide();
            this.dialogRef.close();
          }
        }
        this.spinner.hide();
      }));

    this.subscriptions.push(this.store.select<InfoResult<Boolean>>
      (state => state.standing.updateStandingStage)
      .subscribe((result) => {
        if (result !== undefined) {
          this.toastr.success(result.message);
          this.spinner.hide();
          this.dialogRef.close(result.data);
        }
      }));
  }

  onSubscribeError() {
    this.subscriptions.push(this.store.select<ErrorResult>(state => state.standing.error)
      .subscribe((result) => {
        if (result !== undefined) {
          switch (result.key) {
            case UpdateStandingAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              this.dialogRef.close();
              break;
            case GetStandingInfoAction.TYPE:
              this.toastr.warning(result.message);
              this.spinner.hide();
              this.dialogRef.close();
              break;
          }
        }
      }));
  }


  getOptionGroup() {
    const result = new Array<String>();
    // Tạo danh sách từ A-Z
    for (let i = 65; i <= 90; i++) {
      result.push(String.fromCharCode(i));
    }
    return result;
  }

  setGroup(data: Array<StandingObjectModel>) {
    const sourceData = from(data);
    const groups = sourceData.pipe(
      groupBy(groupName => groupName.group),
      mergeMap(groupList => groupList.pipe(toArray()))
    );

    groups.subscribe(val => {
      const group = new GroupStandingModel({
        group: val[0].group,
        tables: val
      });
      this.listGroup.push(group);
    });
  }

  onClose() {
    this.dialogRef.close();
  }

  onSave() {
    if (this.listGroup !== undefined && this.listGroup !== null && this.listGroup.length > 0) {
      for (let i = 0; i < this.listGroup.length; i++) {
        this.tables.push.apply(this.tables, this.listGroup[i].tables);
      }
      this.standingInfo.tables = this.tables;
      const model = {
        id: this.data.dataObject.id,
        data: this.standingInfo
      };
      this.store.dispatch(new UpdateStandingAction(model));
    } else {
      this.toastr.warning(this.translate.translate(this.translateKey.STAGE_MANAGER.MESSAGE_UPDATE_NONE_ELEMENT));
      this.dialogRef.close();
    }
  }

  destroyAction() {
    this.store.dispatch({ type: FailManagerStageCallAction.TYPE });
    this.store.dispatch({ type: SuccessUpdateStandingAction.TYPE });
    this.store.dispatch({ type: SuccessGetStandingInfoAction.TYPE });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
    this.destroyAction();
  }
}

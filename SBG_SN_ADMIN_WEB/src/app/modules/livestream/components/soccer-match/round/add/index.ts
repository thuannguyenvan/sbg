import {
  Component,
  Inject,
} from '@angular/core';
import {
  Router,
} from '@angular/router';
import {
  Title,
} from '@angular/platform-browser';
import {
  Store,
} from '@ngrx/store';
import {
  Subscription,
} from 'rxjs';
import {
  AppState,
} from '../../../../../../AppState';
import {
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import {
  GetListCompetitionsRoundAction,
  SuccessGetListCompetitionsRoundAction,
  FailManagerRoundCallAction,
  SuccessGetStageByCpAction,
  SuccessCreateRoundAction,
  CreateRoundAction,
  GetStageByCpAction,
} from '../../../../actions';
import {
  SpinnerHelper,
  ToastrHelper,
  ValidateHelper,
  PermissionActionHelper,
} from '../../../../../../helpers';
import {
  InfoResult,
  ErrorResult,
  CompetitionsInfo,
  StageModel,
  RoundModel,
} from '../../../../../../models';
import {
  IBaseComponent,
  ValidateBaseComponent,
} from '../../../../../../bases';
import {
  TranslateService
} from '../../../../../../services';
import {
  TranslateKeyConstants,
  RouterConstants,
  LenghtValidateConstants,
} from '../../../../../../constants';
import {
  ActionType,
} from '../../../../../../enums';
import {
  DateTimeHelper,
} from '../../../../../../helpers/DateTimeHelper';

@Component({
  selector: 'app-round-create',
  templateUrl: './index.html',
  styleUrls: ['./index.scss'],
})
export class CreateRoundComponent extends ValidateBaseComponent implements IBaseComponent {
  subscriptions: Subscription[] = [];
  public translateKey: any;
  public competitionsInfo: CompetitionsInfo[] = [];
  public stageList: StageModel[] = [];
  public model: RoundModel = new RoundModel();
  public permissionAction: any = {
    CREATE_ROUND: false,
  };
  public selectedCompetitionId: string;
  public optionDateOnly: any = DateTimeHelper.getOptionDateOnly();

  constructor(@Inject(FormBuilder) fb: FormBuilder,
    private store: Store<AppState>,
    private spinner: SpinnerHelper,
    private router: Router,
    private permissionActionHelper: PermissionActionHelper,
    private toastr: ToastrHelper,
    title: Title,
    private translate: TranslateService,
    private validateHelper: ValidateHelper,
  ) {
    super(validateHelper);
    this.translateKey = TranslateKeyConstants;
    this.initValidate(fb);
    this.resetValidate();
    this.translateKey = TranslateKeyConstants;
    title.setTitle(this.translate.translate(this.translateKey.ROUND_MANAGER.PAGE_CREATE_TITLE));
  }

  ngOnInit() {
    this.onSubscribeError();
    this.onSubscribeSuccess();
    this.spinner.show();
    this.store.dispatch(new GetListCompetitionsRoundAction());
  }

  initValidate(fb: FormBuilder) {
    this.validators = {
      selectedCompetitionId: [Validators.required],
      soccerCompetitionStageId: [Validators.required],
      title: [Validators.required, Validators.maxLength(LenghtValidateConstants.STRING_MEDIUM)],
      description: [Validators.maxLength(LenghtValidateConstants.STRING_MAX)],
      startAt: [],
      endAt: [],
    };

    this.formGroup = fb.group({
      selectedCompetitionId: new FormControl(this.selectedCompetitionId, this.validators.selectedCompetitionId),
      soccerCompetitionStageId: new FormControl(this.model.soccerCompetitionStageId, this.validators.soccerCompetitionStageId),
      title: new FormControl(this.model.title, this.validators.title),
      description: new FormControl(this.model.description, this.validators.description),
      startAt: new FormControl(this.model.startAt, this.validators.startAt),
      endAt: new FormControl(this.model.endAt, this.validators.endAt),
    });
  }

  onSubscribeSuccess() {
    this.subscriptions.push(this.store.select<InfoResult<Array<CompetitionsInfo>>>(state => state.managerRound.getCompetitionsInfo)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.data !== undefined && result.data !== null) {
            this.competitionsInfo = result.data;
          }
          this.spinner.hide();
          this.onCheckPermissionAction();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<Array<StageModel>>>(state => state.managerRound.GetStageByCp)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.data !== undefined && result.data !== null) {
            this.stageList = result.data;
          }
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<boolean>>(state => state.managerRound.createRound)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.data !== undefined && result.data !== null) {
            if (result.data) {
              this.toastr.success(result.message);
              this.onBack();
            }
          }
          this.spinner.hide();
        }
      }));
  }

  onSearch(id: string) {
    this.store.dispatch(new GetStageByCpAction(id));
  }

  selected(value: any) {
    this.model.soccerCompetitionStageId = undefined;
    this.onSearch(this.selectedCompetitionId);
  }

  onSave() {
    if (!this.invalid()) {
      this.spinner.show();
      this.model.soccerCompetitionId = this.selectedCompetitionId;
      this.store.dispatch(new CreateRoundAction(this.model));
    } else {
      this.toastr.warning(this.translate.translate(this.translateKey.SHARE.MESSAGE_INPUT_DATA_INVALID));
    }
  }

  onBack() {
    this.router.navigate([RouterConstants.MANAGER_ROUND.LIST]);
  }

  onCheckPermissionAction() {
    this.permissionAction.CREATE_ROUND = this.permissionActionHelper.isValid(ActionType.CREATE_ROUND);
  }

  onSubscribeError() {
    this.subscriptions.push(this.store.select<ErrorResult>(state => state.managerRound.error)
      .subscribe((result) => {
        if (result !== undefined) {
          switch (result.key) {
            case GetListCompetitionsRoundAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
            case GetStageByCpAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
            case CreateRoundAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
          }
        }
      }));
  }

  destroyAction() {
    this.store.dispatch({ type: FailManagerRoundCallAction.TYPE });
    this.store.dispatch({ type: SuccessGetStageByCpAction.TYPE });
    this.store.dispatch({ type: SuccessCreateRoundAction.TYPE });
    this.store.dispatch({ type: SuccessGetListCompetitionsRoundAction.TYPE });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
    this.destroyAction();
  }
}

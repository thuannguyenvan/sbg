import {
  Component,
  Inject,
} from '@angular/core';
import {
  Title,
} from '@angular/platform-browser';
import {
  Store,
} from '@ngrx/store';
import {
  Subscription,
} from 'rxjs';
import {
  MatDialog,
  MatTableDataSource,
} from '@angular/material';
import {
  Router, ActivatedRoute,
} from '@angular/router';
import {
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import {
  IBaseComponent,
  ValidateBaseComponent,
} from '../../../../../../bases';
import {
  ToastrHelper,
  SpinnerHelper,
  AlertHelper,
  PermissionActionHelper,
  ValidateHelper,
} from '../../../../../../helpers';
import {
  AppState,
} from '../../../../../../AppState';
import {
  TranslateService,
} from '../../../../../../services';
import {
  TranslateKeyConstants,
  RouterConstants,
  LenghtValidateConstants,
} from '../../../../../../constants';
import {
  Pagination,
  InfoResult,
  CompetitionsInfo,
  ErrorResult,
  SoccerMatchSearchModel,
  RoundInfo,
  StageModel,
} from '../../../../../../models';
import {
  GetRoundsAction,
  SuccessGetRoundsAction,
  DeleteRoundAction,
  SuccessDeleteRoundAction,
  GetListCompetitionsRoundAction,
  GetStageByCpAction,
  FailManagerRoundCallAction,
  SuccessGetStageByCpAction,
  SuccessGetListCompetitionsRoundAction,
} from '../../../../actions';
import {
  ActionType,
} from '../../../../../../enums';
import {
  String
} from 'typescript-string-operations';


@Component({
  selector: 'app-round-list',
  templateUrl: './index.html',
  styleUrls: ['./index.scss'],
})
export class RoundListComponent extends ValidateBaseComponent implements IBaseComponent {
  subscriptions: Subscription[] = [];
  public translateKey: any;
  public modelSearch: SoccerMatchSearchModel = new SoccerMatchSearchModel();
  public model: SoccerMatchSearchModel = new SoccerMatchSearchModel();
  public pagination: Pagination = new Pagination();
  public competitions: CompetitionsInfo[] = [];
  public stages: StageModel[] = [];
  public rounds: RoundInfo[] = [];
  public dataSource: MatTableDataSource<any>;
  public permissionAction: any = {
    ROUND_ADD_SOCCER_TEAM: false,
    ROUND_EDIT_ROUND: false,
    ROUND_DELETE_ROUND: false,
    ROUND_SEARCH: false,
    ROUND_ADD_ROUND: false,
  };
  private isInitialize: Boolean = true;
  public sortDefault = {
    active: 'Title',
    direction: 'asc'
  };

  constructor(
    @Inject(FormBuilder) fb: FormBuilder,
    public permissionActionHelper: PermissionActionHelper,
    private store: Store<AppState>,
    private spinner: SpinnerHelper,
    private route: ActivatedRoute,
    private toastr: ToastrHelper,
    title: Title,
    public dialog: MatDialog,
    private translate: TranslateService,
    private router: Router,
    private alert: AlertHelper,
    validateHelper: ValidateHelper,
  ) {
    super(validateHelper);
    this.initValidate(fb);
    this.resetValidate();
    this.translateKey = TranslateKeyConstants;
    title.setTitle(this.translate.translate(this.translateKey.ROUND.PAGE_TITLE));
  }

  ngOnInit() {
    this.isInitialize = true;
    this.onSubscribeError();
    this.onSubscribeSuccess();
    this.spinner.show();
    this.model.orderBy = this.modelSearch.orderBy = this.sortDefault.active;
    this.model.order = this.modelSearch.order = this.sortDefault.direction;
    this.store.dispatch(new GetListCompetitionsRoundAction());
  }

  initValidate(fb: FormBuilder) {
    this.validators = {
      title: [Validators.maxLength(LenghtValidateConstants.STRING_MEDIUM)],
      competions: [Validators.required],
    };

    this.formGroup = fb.group({
      competions: new FormControl(this.validators.competions),
      title: new FormControl(this.model.title, this.validators.title),
    });
  }

  onSubscribeSuccess() {
    // get Giải đấu
    this.subscriptions.push(this.store.select<InfoResult<Array<CompetitionsInfo>>>(state => state.managerRound.getCompetitionsInfo)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.data !== undefined && result.data !== null) {
            this.competitions = result.data;
            if (this.competitions.length > 0) {
              const idCp = this.route.snapshot.params['competitionId'];
              if (idCp !== undefined && idCp !== null && idCp !== '') {
                this.model.soccerCompetitionId = idCp;
              } else {
                this.model.soccerCompetitionId = this.competitions[0].id;
              }
              this.store.dispatch(new GetStageByCpAction(this.model.soccerCompetitionId));
            }
          }
          this.spinner.hide();
        }
      }));

    // get Statge theo Giải đấu
    this.subscriptions.push(this.store.select<InfoResult<Array<StageModel>>>
      (state => state.managerRound.GetStageByCp)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.data !== undefined && result.data !== null) {
            this.model.soccerCompetitionStageId = '';
            this.stages = result.data;
            if (this.isInitialize) {

              this.onSubmit();
              this.isInitialize = false;
            }
          }
          this.spinner.hide();
        }
      }));

    // get Danh sách Round đấu
    this.subscriptions.push(this.store.select<InfoResult<Array<RoundInfo>>>
      (state => state.managerRound.getRounds)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.data !== undefined &&
            result.data !== null
          ) {
            this.rounds = result.data;
            this.dataSource = new MatTableDataSource(this.rounds);
            this.pagination.length = result.data.length > 0 ? result.data[0].total : 0;
            this.onCheckPermissionAction();
          }
          this.spinner.hide();
        }
      }));

    // delete Round
    this.subscriptions.push(this.store.select<InfoResult<any>>
      (state => state.managerRound.deleteRound)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.message !== undefined &&
            result.message !== null
          ) {
            this.toastr.success(result.message);
            this.onSubmit();
          }
          this.spinner.hide();
        }
      }));
  }

  onSubscribeError() {
    this.subscriptions.push(this.store.select<ErrorResult>(state => state.managerRound.error)
      .subscribe((result) => {
        if (result !== undefined) {
          switch (result.key) {
            case GetListCompetitionsRoundAction.TYPE:
              this.toastr.showError(result);
              this.pagination.length = 0;
              this.spinner.hide();
              break;
            case GetStageByCpAction.TYPE:
              this.toastr.showError(result);
              this.pagination.length = 0;
              this.spinner.hide();
              break;
            case DeleteRoundAction.TYPE:
              this.toastr.showError(result);
              this.pagination.length = 0;
              this.spinner.hide();
              break;
            case GetRoundsAction.TYPE:
              this.toastr.showError(result);
              this.pagination.length = 0;
              this.spinner.hide();
              break;
          }
        }
      }));
  }

  onCheckPermissionAction() {
    this.permissionAction.ROUND_ADD_SOCCER_TEAM =
      this.permissionActionHelper.isValid(ActionType.ROUND_ADD_SOCCER_TEAM);
    this.permissionAction.ROUND_DELETE_ROUND =
      this.permissionActionHelper.isValid(ActionType.ROUND_DELETE_ROUND);
    this.permissionAction.ROUND_EDIT_ROUND =
      this.permissionActionHelper.isValid(ActionType.ROUND_EDIT_ROUND);
    this.permissionAction.ROUND_SEARCH =
      this.permissionActionHelper.isValid(ActionType.ROUND_SEARCH);
    this.permissionAction.ROUND_ADD_ROUND =
      this.permissionActionHelper.isValid(ActionType.ROUND_ADD_ROUND);
  }

  public onShowActtionItem(item: CompetitionsInfo, key: string) {
    let checkShow = false;
    switch (key) {
      case 'addSoccer':
        checkShow = item.actions.findIndex(x => x === ActionType.ROUND_ADD_SOCCER_TEAM) > -1 &&
          this.permissionAction.ROUND_ADD_SOCCER_TEAM;
        break;
      case 'deleteRound':
        checkShow = item.actions.findIndex(x => x === ActionType.ROUND_DELETE_ROUND) > -1 &&
          this.permissionAction.ROUND_DELETE_ROUND;
        break;
      case 'editRound':
        checkShow = item.actions.findIndex(x => x === ActionType.ROUND_EDIT_ROUND) > -1 &&
          this.permissionAction.ROUND_EDIT_ROUND;
        break;
    }
    return checkShow;
  }

  sortData(sort: any) {
    this.modelSearch.orderBy = sort.active;
    this.modelSearch.order = sort.direction;
    this.modelSearch.page = 1;
    this.pagination.pageIndex = 1;
    this.searchMatchList();
  }

  onChangeCompetition() {
    if (this.model.soccerCompetitionId !== undefined && this.model.soccerCompetitionId !== null) {
      this.spinner.show();
      this.store.dispatch(new GetStageByCpAction(this.model.soccerCompetitionId));
    } else {
      this.toastr.warning(this.translate.translate(this.translateKey.ROUND_MANAGER.MESSAGE_WARNING_CHOOESE_COMPETITIONS));
    }
  }

  onSubmit() {
    if (this.model.soccerCompetitionId !== undefined && this.model.soccerCompetitionId !== null) {
      this.model.orderBy = this.modelSearch.orderBy;
      this.model.order = this.modelSearch.order;
      this.modelSearch = new SoccerMatchSearchModel(this.model);
      this.onSearch();
    } else {
      this.toastr.warning(this.translate.translate(this.translateKey.ROUND_MANAGER.MESSAGE_WARNING_CHOOESE_COMPETITIONS));
    }
  }

  onSearch() {
    this.pagination.pageIndex = 1;
    this.modelSearch.soccerCompetitionStageId =
      this.modelSearch.soccerCompetitionStageId === null
        || this.modelSearch.soccerCompetitionStageId === ''
        ? undefined
        : this.modelSearch.soccerCompetitionStageId;
    this.modelSearch.page = this.pagination.pageIndex;
    this.modelSearch.size = this.pagination.pageSize;
    this.searchMatchList();
  }

  searchMatchList() {
    this.spinner.show();
    this.store.dispatch(new GetRoundsAction(this.modelSearch));
  }

  onPageChanged(page: any) {
    this.modelSearch.page = this.pagination.pageIndex = page.pageIndex;
    this.modelSearch.size = this.pagination.pageSize = page.pageSize;
    this.searchMatchList();
  }

  onDelete(competition: CompetitionsInfo) {
    const msg = String.Format(this.translate.translate(this.translateKey.ROUND_MANAGER.QUESTION_DELETE_ROUND), competition.title);
    this.alert.confirmWarning(msg, (r: any) => {
      if (r.value) {
        this.store.dispatch(new DeleteRoundAction(competition.id));
      }
    });
  }

  onEditRound(id: string) {
    this.router.navigate([RouterConstants.MANAGER_ROUND.EDIT_ROUND + '/' + id]);
  }

  onCreateRound() {
    this.router.navigate([RouterConstants.MANAGER_ROUND.ADD_ROUND]);
  }

  onAddSoccerMatch(round: RoundInfo) {
    this.router.navigate([RouterConstants.MANAGER_MATCH.CREATE_MATCH],
      { queryParams: { roundId: round.id, competitionId: round.soccerCompetitionId } });
  }

  destroyAction() {
    this.store.dispatch({ type: FailManagerRoundCallAction.TYPE });
    this.store.dispatch({ type: SuccessDeleteRoundAction.TYPE });
    this.store.dispatch({ type: SuccessGetRoundsAction.TYPE });
    this.store.dispatch({ type: SuccessGetStageByCpAction.TYPE });
    this.store.dispatch({ type: SuccessGetListCompetitionsRoundAction.TYPE });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
    this.destroyAction();
  }
}

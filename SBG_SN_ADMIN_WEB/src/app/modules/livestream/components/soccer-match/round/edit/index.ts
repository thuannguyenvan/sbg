import {
  Component,
  Inject,
} from '@angular/core';
import {
  Router,
  ActivatedRoute,
} from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import {
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { AppState } from '../../../../../../AppState';
import {
  FailManagerRoundCallAction,
  GetRoundInfoAction,
  UpdateRoundAction,
  SuccessUpdateRoundAction,
  GetStageByCpAction,
  SuccessGetRoundInfoAction,
} from '../../../../actions';
import {
  SpinnerHelper,
  ToastrHelper,
  ValidateHelper,
  PermissionActionHelper,
} from '../../../../../../helpers';
import {
  InfoResult,
  ErrorResult,
  RoundInfoModel,
} from '../../../../../../models';
import {
  IBaseComponent,
  ValidateBaseComponent,
} from '../../../../../../bases';
import {
  TranslateService,
} from '../../../../../../services';
import {
  TranslateKeyConstants,
  RouterConstants,
  LenghtValidateConstants,
} from '../../../../../../constants';
import {
  ActionType,
} from '../../../../../../enums';
import {
  DateTimeHelper,
} from '../../../../../../helpers/DateTimeHelper';

@Component({
  selector: 'app-round-edit',
  templateUrl: './index.html',
  styleUrls: ['./index.scss'],
})
export class EditRoundComponent extends ValidateBaseComponent implements IBaseComponent {
  subscriptions: Subscription[] = [];
  public translateKey: any;
  public model: RoundInfoModel = new RoundInfoModel();
  public id: any;
  public permissionAction: any = {
    EDIT_ROUND: false,
  };
  public routerConstants: any;
  public optionDateOnly: any = DateTimeHelper.getOptionDateOnly();

  constructor(@Inject(FormBuilder) fb: FormBuilder,
    private store: Store<AppState>,
    private spinner: SpinnerHelper,
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrHelper,
    private permissionActionHelper: PermissionActionHelper,
    title: Title,
    private translate: TranslateService,
    private validateHelper: ValidateHelper,
  ) {
    super(validateHelper);
    this.initValidate(fb);
    this.resetValidate();
    this.routerConstants = RouterConstants;
    this.translateKey = TranslateKeyConstants;
    title.setTitle(this.translate.translate(this.translateKey.ROUND_MANAGER.PAGE_EDIT_TITLE));
  }

  ngOnInit() {
    this.onSubscribeError();
    this.onSubscribeSuccess();
    this.spinner.show();
    this.route.params.subscribe(para => {
      this.id = para['id'];
      if (this.id !== undefined || this.id !== null || this.id !== '') {
        this.store.dispatch(new GetRoundInfoAction(this.id));
      }
    });
  }

  initValidate(fb: FormBuilder) {
    this.validators = {
      title: [Validators.required, Validators.maxLength(LenghtValidateConstants.STRING_MEDIUM)],
      description: [Validators.maxLength(LenghtValidateConstants.STRING_MAX)],
      startAt: [],
      endAt: [],
    };

    this.formGroup = fb.group({
      title: new FormControl(this.model.title, this.validators.title),
      description: new FormControl(this.model.description, this.validators.description),
      startAt: new FormControl(this.model.startAt, this.validators.startAt),
      endAt: new FormControl(this.model.endAt, this.validators.endAt),
    });
  }

  onSubscribeSuccess() {
    this.subscriptions.push(this.store.select<InfoResult<boolean>>(state => state.managerRound.updateRound)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.data !== undefined && result.data !== null) {
            if (result.data) {
              this.toastr.success(result.message);
              const idCp = this.route.snapshot.params['competitionId'];
              if (idCp !== undefined && idCp !== null && idCp !== '') {
                this.router.navigate([RouterConstants.MANAGER_ROUND.LIST + '/' + idCp]);
              } else {
                this.onBack();
              }
            }
          }
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<RoundInfoModel>>(state => state.managerRound.getRoundInfo)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.data !== undefined && result.data !== null) {
            this.model = result.data;
          }
          this.spinner.hide();
          this.onCheckPermissionAction();
        }
      }));
  }

  onSearch(id: string) {
    this.store.dispatch(new GetStageByCpAction(id));
  }

  onCheckPermissionAction() {
    this.permissionAction.EDIT_ROUND = this.permissionActionHelper.isValid(ActionType.EDIT_ROUND);
  }

  onSave() {
    if (!this.invalid()) {
      const model = {
        id: this.id,
        data: this.model
      };
      this.spinner.show();
      this.store.dispatch(new UpdateRoundAction(model));
    } else {
      this.toastr.warning(this.translate.translate(this.translateKey.SHARE.MESSAGE_INPUT_DATA_INVALID));
    }
  }

  onBack() {
    this.router.navigate([RouterConstants.MANAGER_ROUND.LIST]);
  }

  onSubscribeError() {
    this.subscriptions.push(this.store.select<ErrorResult>(state => state.managerRound.error)
      .subscribe((result) => {
        if (result !== undefined) {
          switch (result.key) {
            case GetRoundInfoAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
            case UpdateRoundAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
          }
        }
      }));
  }

  destroyAction() {
    this.store.dispatch({ type: FailManagerRoundCallAction.TYPE });
    this.store.dispatch({ type: SuccessGetRoundInfoAction.TYPE });
    this.store.dispatch({ type: SuccessUpdateRoundAction.TYPE });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
    this.destroyAction();
  }
}

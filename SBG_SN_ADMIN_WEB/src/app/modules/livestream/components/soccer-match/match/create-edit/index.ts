import {
  Component,
  Inject,
} from '@angular/core';
import {
  Router, ActivatedRoute,
} from '@angular/router';
import {
  Title,
} from '@angular/platform-browser';
import {
  Store,
} from '@ngrx/store';
import {
  Subscription,
} from 'rxjs';
import {
  AppState,
} from '../../../../../../AppState';
import {
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import {
  FailSoccerMatchMatchCallAction,
  SuccessGetTeamByRoundCreateMatchAction,
  SuccessGetRoundByCpCreateMatchAction,
  SuccessCreateMatchAction,
  GetRoundByCpCreateMatchAction,
  GetTeamByRoundCreateMatchAction,
  CreateMatchAction,
  SuccessGetCreateMatchInfoAction,
  GetCreateMatchInfoAction,
  SuccessEditMatchAction,
  SuccessGetEditMatchInfoAction,
  EditMatchAction,
  GetEditMatchInfoAction,
} from '../../../../actions';
import {
  SpinnerHelper,
  ToastrHelper,
  ValidateHelper,
  PermissionActionHelper,
} from '../../../../../../helpers';
import {
  InfoResult,
  ErrorResult,
  SoccerMatchInfo,
  TeamsModel,
  RoundInfoModel,
  SoccerMatchModel,
  CompetitionsInfo,
  WinnerTeamTypes,
  SoccerMatchStatuses,
  SoccerMatchUpdateInfoModel,
} from '../../../../../../models';
import {
  IBaseComponent,
  ValidateBaseComponent,
} from '../../../../../../bases';
import {
  TranslateService
} from '../../../../../../services';
import {
  TranslateKeyConstants,
  RouterConstants,
  LenghtValidateConstants,
} from '../../../../../../constants';
import {
  ActionType,
} from '../../../../../../enums';
import {
  DateTimeHelper,
} from '../../../../../../helpers/DateTimeHelper';
import {
  String,
} from 'typescript-string-operations';

@Component({
  selector: 'app-match-create-edit',
  templateUrl: './index.html',
  styleUrls: ['./index.scss'],
})
export class CreateEditMathcComponent extends ValidateBaseComponent implements IBaseComponent {
  subscriptions: Subscription[] = [];
  public translateKey: any;
  public listCompetiton: Array<CompetitionsInfo>;
  public listStatus: Array<SoccerMatchStatuses>;
  public listWinnerTeam: Array<WinnerTeamTypes>;
  public listRound: RoundInfoModel[] = [];
  public listTeamA: TeamsModel[] = [];
  public listTeamB: TeamsModel[] = [];
  public listTeam: TeamsModel[] = [];
  public model: SoccerMatchModel = new SoccerMatchModel();
  public permissionAction: any = {
    CREATE_MATCH: false,
    UPDATE_MATCH: false,
  };
  public show: any = {
    Create: false,
    showBtnUpdate: false,
  };
  public para: any = {
    id: 'id',
    roundId: 'roundId',
    competitionId: 'competitionId'
  };
  public id: string;
  public roundId: string;
  public competitionId: string;
  public soccerTeamIdTeamA: string;
  public soccerTeamIdTeamB: string;
  public optionDateFull: any = DateTimeHelper.getOptionDateFull();

  constructor(@Inject(FormBuilder) fb: FormBuilder,
    private store: Store<AppState>,
    private spinner: SpinnerHelper,
    private router: Router,
    private permissionActionHelper: PermissionActionHelper,
    private route: ActivatedRoute,
    private toastr: ToastrHelper,
    private title: Title,
    private translate: TranslateService,
    private validateHelper: ValidateHelper,
  ) {
    super(validateHelper);
    this.translateKey = TranslateKeyConstants;
    this.initValidate(fb);
    this.resetValidate();
    this.translateKey = TranslateKeyConstants;
  }

  ngOnInit() {
    this.onSubscribeError();
    this.onSubscribeSuccess();
    this.spinner.show();
    if (this.router.url.toLowerCase().indexOf(RouterConstants.MANAGER_MATCH.CREATE_MATCH.toLowerCase()) > -1) {
      this.show.Create = true;
      this.spinner.show();
      this.store.dispatch(new GetCreateMatchInfoAction());
      this.model = new SoccerMatchModel();
      this.roundId = this.route.snapshot.queryParams[this.para.roundId];
      this.competitionId = this.route.snapshot.queryParams[this.para.competitionId];
      this.title.setTitle(this.translate.translate(this.translateKey.MATCH_MANAGER.PAGE_CREATE_TITLE));
    } else {
      /* Form Edit */
      this.show.Create = false;
      this.id = this.route.snapshot.params[this.para.id];
      if (this.id !== undefined && this.id !== null && this.id !== '') {
        this.store.dispatch(new GetEditMatchInfoAction(this.id));
      }
      this.title.setTitle(this.translate.translate(this.translateKey.MATCH_MANAGER.PAGE_UPDATE_TITLE));
    }
  }

  initValidate(fb: FormBuilder) {
    this.validators = {
      soccerCompetitionId: [Validators.required],
      rounds: [Validators.required],
      teamA: [Validators.required],
      teamB: [Validators.required],
      status: [Validators.required],
      title: [Validators.required, Validators.maxLength(LenghtValidateConstants.STRING_MEDIUM)],
      place: [Validators.maxLength(LenghtValidateConstants.STRING_MEDIUM)],
      startAt: [Validators.required],
      finishedAt: [],
      score_fullTime_teamA: [Validators.maxLength(LenghtValidateConstants.NUMBER_3)],
      score_fullTime_teamB: [Validators.maxLength(LenghtValidateConstants.NUMBER_3)],
      score_halftime_teamA: [Validators.maxLength(LenghtValidateConstants.NUMBER_3)],
      score_halftime_teamB: [Validators.maxLength(LenghtValidateConstants.NUMBER_3)],
      score_extraTime_teamA: [Validators.maxLength(LenghtValidateConstants.NUMBER_3)],
      score_extraTime_teamB: [Validators.maxLength(LenghtValidateConstants.NUMBER_3)],
      score_penalty_teamA: [Validators.maxLength(LenghtValidateConstants.NUMBER_3)],
      score_penalty_teamB: [Validators.maxLength(LenghtValidateConstants.NUMBER_3)],
      score_aggregate_teamA: [Validators.maxLength(LenghtValidateConstants.NUMBER_3)],
      score_aggregate_teamB: [Validators.maxLength(LenghtValidateConstants.NUMBER_3)],
    };

    this.formGroup = fb.group({
      soccerCompetitionId: new FormControl(this.model.soccerCompetitionId, this.validators.soccerCompetitionId),
      rounds: new FormControl(this.model.soccerRoundId, this.validators.rounds),
      teamA: new FormControl(this.model.teamA, this.validators.teamA),
      teamB: new FormControl(this.model.teamB, this.validators.teamB),
      status: new FormControl(this.model.status, this.validators.status),
      title: new FormControl(this.model.title, this.validators.title),
      startAt: new FormControl(this.model.startAt, this.validators.startAt),
      finishedAt: new FormControl(this.model.finishedAt, this.validators.finishedAt),
      place: new FormControl(this.model.place, this.validators.place),
      score_fullTime_teamA: new FormControl(this.model.score.fullTime.teamA, this.validators.score_fullTime_teamA),
      score_fullTime_teamB: new FormControl(this.model.score.fullTime.teamB, this.validators.score_fullTime_teamB),
      score_halftime_teamA: new FormControl(this.model.score.halfTime.teamA, this.validators.score_halftime_teamA),
      score_halftime_teamB: new FormControl(this.model.score.halfTime.teamB, this.validators.score_halftime_teamB),
      score_extraTime_teamA: new FormControl(this.model.score.extraTime.teamA, this.validators.score_extraTime_teamA),
      score_extraTime_teamB: new FormControl(this.model.score.extraTime.teamB, this.validators.score_extraTime_teamB),
      score_penalty_teamA: new FormControl(this.model.score.penalty.teamA, this.validators.score_penalty_teamA),
      score_penalty_teamB: new FormControl(this.model.score.penalty.teamB, this.validators.score_penalty_teamB),
      score_aggregate_teamA: new FormControl(this.model.score.aggregate.teamA, this.validators.score_aggregate_teamA),
      score_aggregate_teamB: new FormControl(this.model.score.aggregate.teamB, this.validators.score_aggregate_teamB)
    });
  }

  onSubscribeSuccess() {
    this.subscriptions.push(this.store.select<InfoResult<SoccerMatchInfo>>(state => state.soccerMatch.getCreateMatchinfo)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.data !== undefined && result.data !== null) {
            this.listStatus = result.data.soccerMatchStatuses;
            this.listCompetiton = result.data.soccerCompetitions;
            this.listWinnerTeam = result.data.winnerTeamTypes;
            if (this.competitionId !== undefined && this.competitionId !== null && this.competitionId !== '') {
              this.model.soccerCompetitionId = this.competitionId;
              this.store.dispatch(new GetRoundByCpCreateMatchAction(this.competitionId));
            }
            if (this.roundId !== undefined && this.roundId !== null && this.roundId !== '') {
              this.model.soccerRoundId = this.roundId;
              this.store.dispatch(new GetTeamByRoundCreateMatchAction(this.roundId));
            }
          }
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<Array<RoundInfoModel>>>(state => state.soccerMatch.getRoundByCp)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.data !== undefined && result.data !== null) {
            this.listRound = result.data;
          }
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<Array<TeamsModel>>>(state => state.soccerMatch.getTeamByRound)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.data !== undefined && result.data !== null) {
            this.listTeamA = result.data.map((d: any) => new TeamsModel(d));
            this.listTeamB = result.data.map((d: any) => new TeamsModel(d));
            this.listTeam = result.data.map((d: any) => new TeamsModel(d));
            this.onSelectTeamDeault();
          } else {
            this.listTeamA = [];
            this.listTeamB = [];
            this.listTeam = [];
          }
          this.soccerTeamIdTeamA = undefined;
          this.soccerTeamIdTeamB = undefined;
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<boolean>>(state => state.soccerMatch.createMatch)
      .subscribe((result) => {
        if (result !== undefined) {
          this.toastr.success(result.message);
          this.onBack();
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<boolean>>(state => state.soccerMatch.updateMatch)
      .subscribe((result) => {
        if (result !== undefined) {
          this.toastr.success(result.message);
          this.onBack();
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<SoccerMatchUpdateInfoModel>>(state => state.soccerMatch.getEditInfo)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.data !== undefined && result.data !== null) {
            this.listStatus = result.data.soccerMatchStatuses;
            this.listWinnerTeam = result.data.winnerTeamTypes;
            this.model = result.data.soccerMatch;
            this.listTeamA = result.data.team.map((d: any) => new TeamsModel(d));
            this.listTeamB = result.data.team.map((d: any) => new TeamsModel(d));
            this.listTeam = result.data.team.map((d: any) => new TeamsModel(d));
            this.soccerTeamIdTeamA = this.model.teamA.soccerTeamId;
            this.soccerTeamIdTeamB = this.model.teamB.soccerTeamId;
            this.onSelectTeamDeault();
          } else {
            this.listTeamA = [];
            this.listTeamB = [];
            this.listTeam = [];
          }
          this.spinner.hide();
        }
      }));
  }

  onSelectTeamDeault() {
    if (this.model.teamA !== undefined && this.model.teamA !== null &&
      this.model.teamA.soccerTeamId !== undefined && this.model.teamA.soccerTeamId !== null) {
      this.soccerTeamIdTeamA = this.model.teamA.soccerTeamId;
    }
    if (this.model.teamB !== undefined && this.model.teamB !== null &&
      this.model.teamB.soccerTeamId !== undefined && this.model.teamB.soccerTeamId !== null) {
      this.soccerTeamIdTeamB = this.model.teamB.soccerTeamId;
    }
    this.onSelectTeam(true);
    this.onSelectTeam(false);
  }

  onSearchRound(value: CompetitionsInfo) {
    this.listTeamA = [];
    this.listTeamB = [];
    this.listTeam = [];
    this.soccerTeamIdTeamA = undefined;
    this.model.soccerRoundId = undefined;
    this.soccerTeamIdTeamB = undefined;
    this.spinner.show();
    this.store.dispatch(new GetRoundByCpCreateMatchAction(value.id));
  }

  onSearchTeam(id: string) {
    this.spinner.show();
    this.store.dispatch(new GetTeamByRoundCreateMatchAction(id));
  }

  onConvertTeam() {
    const teamA = this.listTeam.filter(x => x.soccerTeamId === this.soccerTeamIdTeamA);
    this.model.teamA = teamA === undefined || teamA === null ? undefined : teamA[0];
    const teamB = this.listTeam.filter(x => x.soccerTeamId === this.soccerTeamIdTeamB);
    this.model.teamB = teamB === undefined || teamB === null ? undefined : teamB[0];
  }

  onCreate() {
    if (!this.invalid()) {
        this.onConvertTeam();
        const model = new SoccerMatchModel(this.model);
        this.spinner.show();
        this.store.dispatch(new CreateMatchAction(model));
    } else {
      this.toastr.warning(this.translate.translate(this.translateKey.SHARE.MESSAGE_INPUT_DATA_INVALID));
    }
  }

  onSave() {
    if (!this.invalid()) {
        this.onConvertTeam();
        this.spinner.show();
        const modelData = new SoccerMatchModel(this.model);
        const model = {
          id: this.id,
          data: modelData
        };
        this.store.dispatch(new EditMatchAction(model));
    } else {
      this.toastr.warning(this.translate.translate(this.translateKey.SHARE.MESSAGE_INPUT_DATA_INVALID));
    }
  }

  onBack() {
    this.router.navigate([RouterConstants.MANAGER_MATCH.LIST_MATCH]);
  }

  onSelectTeam(checkTeamA: boolean) {
    if (checkTeamA) {
      if (this.soccerTeamIdTeamA !== null && this.soccerTeamIdTeamA !== undefined) {
        this.listTeamB = this.listTeam.filter(x => x.soccerTeamId !== this.soccerTeamIdTeamA);
      }
    } else {
      if (this.soccerTeamIdTeamB !== null && this.soccerTeamIdTeamB !== undefined) {
        this.listTeamA = this.listTeam.filter(x => x.soccerTeamId !== this.soccerTeamIdTeamB);
      }
    }
  }

  onCheckPermissionAction() {
    this.permissionAction.CREATE_MATCH = this.permissionActionHelper.isValid(ActionType.MATCH_ADD);
    this.permissionAction.UPDATE_MATCH = this.permissionActionHelper.isValid(ActionType.MATCH_UPDATE);
  }

  onSubscribeError() {
    this.subscriptions.push(this.store.select<ErrorResult>(state => state.soccerMatch.error)
      .subscribe((result) => {
        if (result !== undefined) {
          switch (result.key) {
            case GetCreateMatchInfoAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
            case GetRoundByCpCreateMatchAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
            case GetTeamByRoundCreateMatchAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
            case CreateMatchAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
            case EditMatchAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
            case GetEditMatchInfoAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
          }
        }
      }));
  }

  destroyAction() {
    this.store.dispatch({ type: FailSoccerMatchMatchCallAction.TYPE });
    this.store.dispatch({ type: SuccessGetCreateMatchInfoAction.TYPE });
    this.store.dispatch({ type: SuccessGetRoundByCpCreateMatchAction.TYPE });
    this.store.dispatch({ type: SuccessGetTeamByRoundCreateMatchAction.TYPE });
    this.store.dispatch({ type: SuccessCreateMatchAction.TYPE });
    this.store.dispatch({ type: SuccessGetEditMatchInfoAction.TYPE });
    this.store.dispatch({ type: SuccessEditMatchAction.TYPE });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
    this.destroyAction();
  }
}

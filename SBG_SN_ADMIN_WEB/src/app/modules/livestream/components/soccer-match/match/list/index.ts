import {
  Component,
  Inject,
} from '@angular/core';
import {
  Title,
} from '@angular/platform-browser';
import {
  Store,
} from '@ngrx/store';
import {
  Subscription,
} from 'rxjs';
import {
  MatDialog,
  MatTableDataSource,
} from '@angular/material';
import {
  Router,
} from '@angular/router';
import {
  String
} from 'typescript-string-operations';
import {
  IBaseComponent,
  ValidateBaseComponent,
} from '../../../../../../bases';
import {
  ToastrHelper,
  SpinnerHelper,
  AlertHelper,
  PermissionActionHelper,
  ValidateHelper,
} from '../../../../../../helpers';
import {
  AppState,
} from '../../../../../../AppState';
import {
  TranslateService,
} from '../../../../../../services';
import {
  TranslateKeyConstants,
  RouterConstants,
  LenghtValidateConstants,
} from '../../../../../../constants';
import {
  Pagination,
  InfoResult,
  ErrorResult,
  StageModel,
  MatchSearchModel,
  MatchSearchInfo,
  MatchRoundModel,
  MatchListInfo,
  MatchSoccerCompetition,
  ValueModel,
  MatchScoreModel,
  MatchSocerDetailModel,
} from '../../../../../../models';
import {
  GetSearchInfoMatchListAction,
  GetRoundByCompetitionInfoListAction,
  GetMatchInfoListAction,
  FailSoccerMatchMatchCallAction,
  SuccessGetSearchInfoMatchListAction,
  SuccessGetMatchInfoListAction,
  SuccessGetRoundByCompetitionInfoListAction,
  DeleteSoccerMatchAction,
  SuccessDeleteSoccerMatchAction,
} from '../../../../actions';
import {
  ActionType,
} from '../../../../../../enums';
import {
  DateTimeHelper,
} from '../../../../../../helpers/DateTimeHelper';
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators
} from '@angular/forms';

@Component({
  selector: 'app-match-list-index',
  templateUrl: './index.html',
  styleUrls: ['./index.scss'],
})
export class MatchListComponent extends ValidateBaseComponent implements IBaseComponent {
  subscriptions: Subscription[] = [];
  public translateKey: any;
  public modelSearch: MatchSearchModel = new MatchSearchModel();
  public modelSearchMatch: MatchSearchModel = new MatchSearchModel();
  public matchSearchInfo: MatchSearchInfo = new MatchSearchInfo();
  public matchs: MatchListInfo[] = [];
  public pagination: Pagination = new Pagination();
  public competitions: MatchSoccerCompetition[] = [];
  public status: ValueModel[] = [];
  public stages: StageModel[] = [];
  public rounds: MatchRoundModel[] = [];
  public dataSource: MatTableDataSource<any>;
  public permissionAction: any = {
    MATCH_VIEW_LIST: false,
    MATCH_ADD: false,
    MATCH_UPDATE: false,
    MATCH_DELETE: false,
    MATCH_STREAM_ADD: false,
    MATCH_STREAM_UPDATE: false,
  };
  public formGroup: FormGroup;
  public scoreDetail: MatchSocerDetailModel = new MatchSocerDetailModel();
  public isInitialize: Boolean = true;
  public optionDateOnly: any = DateTimeHelper.getOptionDateOnly();
  public sortDefault = {
    active: 'Title',
    direction: 'asc'
  };

  constructor(
    @Inject(FormBuilder) fb: FormBuilder,
    private validateHelper: ValidateHelper,
    public permissionActionHelper: PermissionActionHelper,
    private store: Store<AppState>,
    private spinner: SpinnerHelper,
    private toastr: ToastrHelper,
    title: Title,
    public dialog: MatDialog,
    private translate: TranslateService,
    private router: Router,
    private alert: AlertHelper,
  ) {
    super(validateHelper);
    this.translateKey = TranslateKeyConstants;
    title.setTitle(this.translate.translate(this.translateKey.MATCH_LIST.PAGE_TITLE));
    this.initValidate(fb);
  }

  ngOnInit() {
    this.onSubscribeError();
    this.onSubscribeSuccess();
    this.spinner.show();
    this.store.dispatch(new GetSearchInfoMatchListAction());
  }

  initValidate(fb: FormBuilder) {
    this.validators = {
      dateStartAt: [this.validateHelper.dateTimeValidator],
      title: [Validators.maxLength(LenghtValidateConstants.STRING_MEDIUM)],
    };
    this.formGroup = fb.group({
      dateStartAt: new FormControl(this.modelSearch.dateStartAt, this.validators.dateStartAt),
      title: new FormControl(this.modelSearch.title, this.validators.title),
    });
  }

  onSubscribeSuccess() {
    // get Giải đấu info
    this.subscriptions.push(this.store.select<InfoResult<MatchSearchInfo>>
      (state => state.soccerMatch.getSearchInfoMatchList)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.data !== undefined &&
            result.data !== null
          ) {
            this.onCheckPermissionAction();
            this.matchSearchInfo = result.data;
            const selectEnumDefault = new MatchSoccerCompetition(
              {
                id: '',
                title: this.translate.translate(this.translateKey.ROUND.SELECT_ALL)
              });
            this.competitions = [selectEnumDefault];
            this.competitions = this.competitions.concat(this.matchSearchInfo.soccerCompetitions);
            this.status = this.matchSearchInfo.soccerMatchStatuses;
            this.modelSearch.soccerCompetitionId = '';
            this.modelSearch.soccerMatchStatus = '';
            this.modelSearch.soccerRoundId = '';
            this.modelSearch = new MatchSearchModel({
              orderBy: this.sortDefault.active,
              order: this.sortDefault.direction,
            });
            if (this.isInitialize) {
              this.isInitialize = false;
              this.onSearch();
            }
          }
          this.spinner.hide();
        }
      }));

    // get Danh sách trận đấu
    this.subscriptions.push(this.store.select<InfoResult<Array<MatchListInfo>>>
      (state => state.soccerMatch.getMatchInfoList)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.data !== undefined &&
            result.data !== null
          ) {
            if (result.data.length === 0) {
              this.matchs = [];
              this.pagination.length = 0;
            } else {
              this.matchs = result.data;
              this.matchs.forEach(x => {
                this.pagination.length = x.total;
                this.onShowScore(x.score);
                if (this.scoreDetail.showAG) {
                  x.scoreAGTeamA = this.scoreDetail.scoreAGTeamA;
                  x.scoreAGTeamB = this.scoreDetail.scoreAGTeamB;
                  x.showAG = this.scoreDetail.showAG;
                }
                if (this.scoreDetail.showPen) {
                  x.penTeamA = this.scoreDetail.penTeamA;
                  x.penTeamB = this.scoreDetail.penTeamB;
                  x.showPen = this.scoreDetail.showPen;
                }
                if (this.scoreDetail.showScore) {
                  x.scoreTeamA = this.scoreDetail.scoreTeamA;
                  x.scoreTeamB = this.scoreDetail.scoreTeamB;
                  x.showScore = this.scoreDetail.showScore;
                }
                if (this.scoreDetail.showEmpty) {
                  x.showEmpty = this.scoreDetail.showEmpty;
                }
              });
            }
            this.dataSource = new MatTableDataSource(this.matchs);
            this.onCheckPermissionAction();
          }
          this.spinner.hide();
        }
      }));

    // get Danh sách Round đấu
    this.subscriptions.push(this.store.select<InfoResult<Array<MatchRoundModel>>>
      (state => state.soccerMatch.getRoundByCompetitionInfoList)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.data !== undefined && result.data !== null
          ) {
            this.modelSearch.soccerRoundId = '';
            if (result.data.length > 0) {
              const selectEnumDefault = new MatchRoundModel({
                id: '',
                title: this.translate.translate(this.translateKey.SHARE.SELECT_ALL)
              });
              this.rounds = [selectEnumDefault];
              this.rounds = this.rounds.concat(result.data);
            }
          }
          this.spinner.hide();
        }
      }));

    // delete Round
    this.subscriptions.push(this.store.select<InfoResult<any>>
      (state => state.soccerMatch.deleteMatch)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.message !== undefined &&
            result.message !== null
          ) {
            this.toastr.success(result.message);
            this.onCheckPermissionAction();
            this.onSearch();
          }
          this.spinner.hide();
        }
      }));
  }

  onSubscribeError() {
    this.subscriptions.push(this.store.select<ErrorResult>(state => state.soccerMatch.error)
      .subscribe((result) => {
        if (result !== undefined) {
          switch (result.key) {
            case GetSearchInfoMatchListAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
            case GetMatchInfoListAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
            case DeleteSoccerMatchAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
            case GetRoundByCompetitionInfoListAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
          }
        }
      }));
  }

  onCheckPermissionAction() {
    this.permissionAction.MATCH_VIEW_LIST =
      this.permissionActionHelper.isValid(ActionType.MATCH_VIEW_LIST);
    this.permissionAction.MATCH_ADD =
      this.permissionActionHelper.isValid(ActionType.MATCH_ADD);
    this.permissionAction.MATCH_UPDATE =
      this.permissionActionHelper.isValid(ActionType.MATCH_UPDATE);
    this.permissionAction.MATCH_DELETE =
      this.permissionActionHelper.isValid(ActionType.MATCH_DELETE);
    this.permissionAction.MATCH_STREAM_ADD =
      this.permissionActionHelper.isValid(ActionType.MATCH_STREAM_ADD);
    this.permissionAction.MATCH_STREAM_UPDATE =
      this.permissionActionHelper.isValid(ActionType.MATCH_STREAM_UPDATE);
  }

  onShowActtionItem(item: MatchListInfo, key: string) {
    let checkShow = false;
    switch (key) {
      case 'editMatch':
        checkShow = item.actions.findIndex(x => x === ActionType.MATCH_UPDATE) > -1 &&
          this.permissionAction.MATCH_UPDATE;
        break;
      case 'addStream':
        checkShow = item.actions.findIndex(x => x === ActionType.MATCH_STREAM_ADD) > -1 &&
          this.permissionAction.MATCH_STREAM_ADD;
        break;
      case 'updateStream':
        checkShow = item.actions.findIndex(x => x === ActionType.MATCH_STREAM_UPDATE) > -1 &&
          this.permissionAction.MATCH_STREAM_UPDATE;
        break;
      case 'deleteMatch':
        checkShow = item.actions.findIndex(x => x === ActionType.MATCH_DELETE) > -1 &&
          this.permissionAction.MATCH_DELETE;
        break;
    }
    return checkShow;
  }

  sortData(sort: any) {
    this.modelSearch.orderBy = sort.active;
    this.modelSearch.order = sort.direction;
    this.modelSearch.page = 1;
    this.pagination.pageIndex = 1;
    this.onSearch();
  }

  onShowScore(score: MatchScoreModel) {
    this.scoreDetail = new MatchSocerDetailModel();
    if (score !== undefined && score !== null) {
      if (score.penalty !== undefined && score.penalty !== null) {
        this.scoreDetail.penTeamA = score.penalty.teamA === undefined || score.penalty.teamA === null ? '' :
          score.penalty.teamA.toString();
        this.scoreDetail.penTeamB = score.penalty.teamB === undefined || score.penalty.teamB === null ? '' :
          score.penalty.teamB.toString();
        this.scoreDetail.showPen = true;
      } else {
        this.scoreDetail.showPen = false;

      }
      if (score.fullTime !== undefined && score.fullTime !== null) {
        this.scoreDetail.scoreTeamA = score.fullTime.teamA === undefined || score.fullTime.teamA === null ? ''
          : score.fullTime.teamA.toString();
        this.scoreDetail.scoreTeamB = score.fullTime.teamB === undefined || score.fullTime.teamB === null ? ''
          : score.fullTime.teamB.toString();
        this.scoreDetail.showScore = true;
      } else {
        if (score.extraTime !== undefined && score.extraTime !== null) {
          this.scoreDetail.scoreTeamA = score.extraTime.teamA === undefined || score.extraTime.teamA === null ? ''
            : score.extraTime.teamA.toString();
          this.scoreDetail.scoreTeamB = score.extraTime.teamB === undefined || score.extraTime.teamB === null ? ''
            : score.extraTime.teamB.toString();
          this.scoreDetail.showScore = true;
        } else {
          this.scoreDetail.showScore = false;
        }
      }

      if (score.aggregate !== undefined && score.aggregate !== null) {
        this.scoreDetail.scoreAGTeamA = score.aggregate.teamA === undefined || score.aggregate.teamA === null ? ''
          : score.aggregate.teamA.toString();
        this.scoreDetail.scoreAGTeamB = score.aggregate.teamB === undefined || score.aggregate.teamB === null ? ''
          : score.aggregate.teamB.toString();
        this.scoreDetail.showAG = true;
      } else {
        this.scoreDetail.showAG = false;
      }

      if (!this.scoreDetail.showScore && !this.scoreDetail.showAG && !this.scoreDetail.showPen) {
        this.scoreDetail.showEmpty = true;
      }
    }
  }

  onSearch() {
    this.pagination.pageIndex = 1;
    this.modelSearchMatch = new MatchSearchModel({
      soccerCompetitionId: this.modelSearch.soccerCompetitionId || undefined,
      soccerRoundId: this.modelSearch.soccerRoundId || undefined,
      soccerMatchStatus: this.modelSearch.soccerMatchStatus || undefined,
      title: this.modelSearch.title,
      page: this.pagination.pageIndex,
      orderBy: this.modelSearch.orderBy,
      order: this.modelSearch.order,
      size: this.pagination.pageSize,
      dateStartAt: this.modelSearch.dateStartAt
    });
    this.searchMatchList();
  }

  searchMatchList() {
    this.spinner.show();
    this.store.dispatch(new GetMatchInfoListAction(this.modelSearchMatch));
  }

  onPageChanged(page: any) {
    this.modelSearchMatch.page = this.pagination.pageIndex = page.pageIndex;
    this.modelSearchMatch.size = this.pagination.pageSize = page.pageSize;
    this.searchMatchList();
  }

  onChangeCompetition(id: string) {
    if (id !== undefined && id !== null && id !== '') {
      this.spinner.show();
      this.store.dispatch(new GetRoundByCompetitionInfoListAction(id));
    } else {
      this.rounds = [];
      this.modelSearch.soccerRoundId = '';
    }
  }

  onDeleteMatch(iteam: any) {

    const msg = String.Format(this.translate.translate(this.translateKey.MATCH_LIST.LABLE_DELETE_CONFIRM), iteam.title);
    this.alert.confirmWarning(msg, (r: any) => {
      if (r.value) {
        this.store.dispatch(new DeleteSoccerMatchAction(iteam.id));
      }
    });
  }

  onEditMatch(id: string) {
    this.router.navigate([RouterConstants.MANAGER_MATCH.EDIT_MATCH + id]);
  }

  onCreateMatch() {
    this.router.navigate([RouterConstants.MANAGER_MATCH.CREATE_MATCH]);
  }

  onAddStream(matchId: string) {
    this.router.navigate([RouterConstants.MANAGER_LIVESTREAM.CREATE_LIVESTREAM], { queryParams: { matchId: matchId } });
  }

  onUpdateStream(id: string) {
    this.router.navigate([RouterConstants.MANAGER_LIVESTREAM.EDIT_LIVESTREAM + '/' + id]);
  }

  destroyAction() {
    this.store.dispatch({ type: FailSoccerMatchMatchCallAction.TYPE });
    this.store.dispatch({ type: SuccessGetSearchInfoMatchListAction.TYPE });
    this.store.dispatch({ type: SuccessGetMatchInfoListAction.TYPE });
    this.store.dispatch({ type: SuccessGetRoundByCompetitionInfoListAction.TYPE });
    this.store.dispatch({ type: SuccessDeleteSoccerMatchAction.TYPE });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
    this.destroyAction();
  }
}

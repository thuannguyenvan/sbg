import { Action } from '@ngrx/store';

export class FailManagerRoundCallAction implements Action {
    public static TYPE = 'FAIL_MANAGER_ROUND_CALL_ACTION';
    readonly type: string = FailManagerRoundCallAction.TYPE;
    constructor(public payload: any) { }
}

import { Action } from '@ngrx/store';

export class CreateRoundAction implements Action {
    public static TYPE = 'CREATE_ROUND_ACTION';
    readonly type: string = CreateRoundAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessCreateRoundAction implements Action {
    public static TYPE = 'SUCCESS_CREATE_ROUND_ACTION';
    readonly type: string = SuccessCreateRoundAction.TYPE;
    constructor(public payload: any) { }
}


import { Action } from '@ngrx/store';

export class GetRoundsAction implements Action {
    public static TYPE = 'GET_ROUNDS_ACTION';
    readonly type: string = GetRoundsAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessGetRoundsAction implements Action {
    public static TYPE = 'SUCCESS_GET_ROUNDS_ACTION';
    readonly type: string = SuccessGetRoundsAction.TYPE;
    constructor(public payload: any) { }
}

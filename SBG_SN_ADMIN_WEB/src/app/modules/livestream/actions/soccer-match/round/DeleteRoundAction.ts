import { Action } from '@ngrx/store';

export class DeleteRoundAction implements Action {
    public static TYPE = 'DELETE_ROUND_ACTION';
    readonly type: string = DeleteRoundAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessDeleteRoundAction implements Action {
    public static TYPE = 'SUCCESS_DELETE_ROUND_ACTION';
    readonly type: string = SuccessDeleteRoundAction.TYPE;
    constructor(public payload: any) { }
}

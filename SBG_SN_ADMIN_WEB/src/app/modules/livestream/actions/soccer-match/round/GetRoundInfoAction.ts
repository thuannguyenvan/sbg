import { Action } from '@ngrx/store';

export class GetRoundInfoAction implements Action {
    public static TYPE = 'GET_ROUND_INFO_ACTION';
    readonly type: string = GetRoundInfoAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessGetRoundInfoAction implements Action {
    public static TYPE = 'SUCCESS_GET_ROUND_INFO_ACTION';
    readonly type: string = SuccessGetRoundInfoAction.TYPE;
    constructor(public payload: any) { }
}

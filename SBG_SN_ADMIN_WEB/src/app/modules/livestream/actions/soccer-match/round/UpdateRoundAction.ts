import { Action } from '@ngrx/store';

export class UpdateRoundAction implements Action {
    public static TYPE = 'UPDATE_ROUND_ACTION';
    readonly type: string = UpdateRoundAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessUpdateRoundAction implements Action {
    public static TYPE = 'SUCCESS_UPDATE_ROUND_ACTION';
    readonly type: string = SuccessUpdateRoundAction.TYPE;
    constructor(public payload: any) { }
}

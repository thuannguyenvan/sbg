import { Action } from '@ngrx/store';

export class GetListCompetitionsRoundAction implements Action {
    public static TYPE = 'GET_LIST_COMPETITIONS_ROUND_ACTION';
    readonly type: string = GetListCompetitionsRoundAction.TYPE;
    constructor() { }
}

export class SuccessGetListCompetitionsRoundAction implements Action {
    public static TYPE = 'SUCCESS_GET_LIST_COMPETITIONS_ROUND_ACTION';
    readonly type: string = SuccessGetListCompetitionsRoundAction.TYPE;
    constructor(public payload: any) { }
}

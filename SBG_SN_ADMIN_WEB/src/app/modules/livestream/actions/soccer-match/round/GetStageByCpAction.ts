import { Action } from '@ngrx/store';

export class GetStageByCpAction implements Action {
    public static TYPE = 'GET_STAGE_BY_CP_ACTION';
    readonly type: string = GetStageByCpAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessGetStageByCpAction implements Action {
    public static TYPE = 'SUCCESS_GET_STAGE_BY_CP_ACTION';
    readonly type: string = SuccessGetStageByCpAction.TYPE;
    constructor(public payload: any) { }
}

import { Action } from '@ngrx/store';

export class GetEditMatchInfoAction implements Action {
    public static TYPE = 'GET_EDIT_MATCH_INFO_ACTION';
    readonly type: string = GetEditMatchInfoAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessGetEditMatchInfoAction implements Action {
    public static TYPE = 'SUCCESS_GET_EDIT_MATCH_INFO_ACTION';
    readonly type: string = SuccessGetEditMatchInfoAction.TYPE;
    constructor(public payload: any) { }
}

import { Action } from '@ngrx/store';

export class CreateMatchAction implements Action {
    public static TYPE = 'ADD_MATCH_ACTION';
    readonly type: string = CreateMatchAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessCreateMatchAction implements Action {
    public static TYPE = 'SUCCESS_ADD_MATCH_ACTION';
    readonly type: string = SuccessCreateMatchAction.TYPE;
    constructor(public payload: any) { }
}

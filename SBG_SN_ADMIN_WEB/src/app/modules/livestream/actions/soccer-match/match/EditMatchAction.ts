import { Action } from '@ngrx/store';

export class EditMatchAction implements Action {
    public static TYPE = 'EDIT_MATCH_ACTION';
    readonly type: string = EditMatchAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessEditMatchAction implements Action {
    public static TYPE = 'SUCCESS_EDIT_MATCH_ACTION';
    readonly type: string = SuccessEditMatchAction.TYPE;
    constructor(public payload: any) { }
}

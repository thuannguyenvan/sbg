import { Action } from '@ngrx/store';

export class GetCreateMatchInfoAction implements Action {
    public static TYPE = 'GET_CREATE_MATCH_INFO_ACTION';
    readonly type: string = GetCreateMatchInfoAction.TYPE;
    constructor() { }
}

export class SuccessGetCreateMatchInfoAction implements Action {
    public static TYPE = 'SUCCESS_GET_CREATE_MATCH_INFO_ACTION';
    readonly type: string = SuccessGetCreateMatchInfoAction.TYPE;
    constructor(public payload: any) { }
}

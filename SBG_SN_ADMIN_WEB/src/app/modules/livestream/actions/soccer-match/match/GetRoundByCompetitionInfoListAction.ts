import { Action } from '@ngrx/store';

export class GetRoundByCompetitionInfoListAction implements Action {
    public static TYPE = 'GET_ROUND_BY_COMPETITION_INFO_LIST_ACTION';
    readonly type: string = GetRoundByCompetitionInfoListAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessGetRoundByCompetitionInfoListAction implements Action {
    public static TYPE = 'SUCCESS_GET_ROUND_BY_COMPETITION_INFO_LIST_ACTION';
    readonly type: string = SuccessGetRoundByCompetitionInfoListAction.TYPE;
    constructor(public payload: any) { }
}

import { Action } from '@ngrx/store';

export class GetRoundByCpCreateMatchAction implements Action {
    public static TYPE = 'GET_ROUND_BY_CP_ADD_MATCH_ACTION';
    readonly type: string = GetRoundByCpCreateMatchAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessGetRoundByCpCreateMatchAction implements Action {
    public static TYPE = 'SUCCESS_GET_ROUND_BY_CP_ADD_MATCH_ACTION';
    readonly type: string = SuccessGetRoundByCpCreateMatchAction.TYPE;
    constructor(public payload: any) { }
}

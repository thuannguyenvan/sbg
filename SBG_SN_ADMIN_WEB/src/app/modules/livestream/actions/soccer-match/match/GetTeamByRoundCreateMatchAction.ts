import { Action } from '@ngrx/store';

export class GetTeamByRoundCreateMatchAction implements Action {
    public static TYPE = 'GET_TEAM_BY_ROUND_ADD_MATCH_ACTION';
    readonly type: string = GetTeamByRoundCreateMatchAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessGetTeamByRoundCreateMatchAction implements Action {
    public static TYPE = 'SUCCESS_GET_TEAM_BY_ROUND_ADD_MATCH_ACTION';
    readonly type: string = SuccessGetTeamByRoundCreateMatchAction.TYPE;
    constructor(public payload: any) { }
}

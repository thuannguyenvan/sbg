import { Action } from '@ngrx/store';

export class FailSoccerMatchMatchCallAction implements Action {
    public static TYPE = 'FAIL_SOCCER_MATCH_MATCH_CALL_ACTION';
    readonly type: string = FailSoccerMatchMatchCallAction.TYPE;
    constructor(public payload: any) { }
}

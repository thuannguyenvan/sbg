import { Action } from '@ngrx/store';

export class DeleteSoccerMatchAction implements Action {
    public static TYPE = 'DELETE_SOCCER_MATCH_ACTION';
    readonly type: string = DeleteSoccerMatchAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessDeleteSoccerMatchAction implements Action {
    public static TYPE = 'SUCCESS_DELETE_SOCCER_MATCH_ACTION';
    readonly type: string = SuccessDeleteSoccerMatchAction.TYPE;
    constructor(public payload: any) { }
}

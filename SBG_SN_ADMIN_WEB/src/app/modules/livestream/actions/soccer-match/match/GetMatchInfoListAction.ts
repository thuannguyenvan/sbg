import { Action } from '@ngrx/store';

export class GetMatchInfoListAction implements Action {
    public static TYPE = 'GET_MATCH_INFO_LIST_ACTION';
    readonly type: string = GetMatchInfoListAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessGetMatchInfoListAction implements Action {
    public static TYPE = 'SUCCESS_GET_MATCH_INFO_LIST_ACTION';
    readonly type: string = SuccessGetMatchInfoListAction.TYPE;
    constructor(public payload: any) { }
}

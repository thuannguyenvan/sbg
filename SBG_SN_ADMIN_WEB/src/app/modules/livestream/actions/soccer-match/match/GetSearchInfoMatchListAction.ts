import { Action } from '@ngrx/store';

export class GetSearchInfoMatchListAction implements Action {
    public static TYPE = 'GET_SEARCH_INFO_MATCH_LIST_ACTION';
    readonly type: string = GetSearchInfoMatchListAction.TYPE;
}

export class SuccessGetSearchInfoMatchListAction implements Action {
    public static TYPE = 'SUCCESS_GET_SEARCH_INFO_MATCH_LIST_ACTION';
    readonly type: string = SuccessGetSearchInfoMatchListAction.TYPE;
    constructor(public payload: any) { }
}

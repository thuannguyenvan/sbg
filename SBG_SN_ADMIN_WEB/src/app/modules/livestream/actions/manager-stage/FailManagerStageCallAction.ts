import { Action } from '@ngrx/store';

export class FailManagerStageCallAction implements Action {
    public static TYPE = 'FAIL_MANAGERSTAGE_CALL_ACTION';
    readonly type: string = FailManagerStageCallAction.TYPE;
    constructor(public payload: any) { }
}

import { Action } from '@ngrx/store';

export class UpdateStageStandingAction implements Action {
    public static TYPE = 'GET_UPDATE_STANDING_STAGE_ACTION';
    readonly type: string = UpdateStageStandingAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessUpdateStageStandingAction implements Action {
    public static TYPE = 'SUCCESS_GET_UPDATE_STANDING_STAGE_ACTION';
    readonly type: string = SuccessUpdateStageStandingAction.TYPE;
    constructor(public payload: any) { }
}

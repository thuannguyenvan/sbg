import { Action } from '@ngrx/store';

export class GetTeamInfoByStageIdAction implements Action {
    public static TYPE = 'GET_TEAM_INFO_BY_STAGE_ID_ACTION';
    readonly type: string = GetTeamInfoByStageIdAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessGetTeamInfoByStageIdAction implements Action {
    public static TYPE = 'SUCCESS_GET_TEAM_INFO_BY_STAGE_ID_ACTION';
    readonly type: string = SuccessGetTeamInfoByStageIdAction.TYPE;
    constructor(public payload: any) { }
}

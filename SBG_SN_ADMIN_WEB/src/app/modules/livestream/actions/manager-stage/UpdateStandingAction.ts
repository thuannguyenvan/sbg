import { Action } from '@ngrx/store';

export class UpdateStandingAction implements Action {
    public static TYPE = 'GET_UPDATE_STANDING_ACTION';
    readonly type: string = UpdateStandingAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessUpdateStandingAction implements Action {
    public static TYPE = 'SUCCESS_GET_UPDATE_STANDING_ACTION';
    readonly type: string = SuccessUpdateStandingAction.TYPE;
    constructor(public payload: any) { }
}

import { Action } from '@ngrx/store';

export class CreateStandingAction implements Action {
    public static TYPE = 'CREATE_STANDING_ACTION';
    readonly type: string = CreateStandingAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessCreateStandingAction implements Action {
    public static TYPE = 'SUCCESS_CREATE_STANDING_ACTION';
    readonly type: string = SuccessCreateStandingAction.TYPE;
    constructor(public payload: any) { }
}

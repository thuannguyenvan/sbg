import { Action } from '@ngrx/store';

export class GetStandingInfoAction implements Action {
    public static TYPE = 'GET_STANDING_INFO_ACTION';
    readonly type: string = GetStandingInfoAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessGetStandingInfoAction implements Action {
    public static TYPE = 'SUCCESS_GET_STANDING_INFO_ACTION';
    readonly type: string = SuccessGetStandingInfoAction.TYPE;
    constructor(public payload: any) { }
}

import { Action } from '@ngrx/store';

export class DeleteStandingAction implements Action {
    public static TYPE = 'DELETE_STANDING_ACTION';
    readonly type: string = DeleteStandingAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessDeleteStandingAction implements Action {
    public static TYPE = 'SUCCESS_DELETE_STANDING_ACTION';
    readonly type: string = SuccessDeleteStandingAction.TYPE;
    constructor(public payload: any) { }
}

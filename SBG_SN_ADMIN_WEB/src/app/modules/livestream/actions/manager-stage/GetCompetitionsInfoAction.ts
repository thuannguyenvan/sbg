import { Action } from '@ngrx/store';

export class GetCompetitionsInfoAction implements Action {
    public static TYPE = 'GET_COMPETITIONS_INFO_ACTION';
    readonly type: string = GetCompetitionsInfoAction.TYPE;
    constructor() { }
}

export class SuccessGetCompetitionsInfoAction implements Action {
    public static TYPE = 'SUCCESS_GET_COMPETITIONS_INFO_ACTION';
    readonly type: string = SuccessGetCompetitionsInfoAction.TYPE;
    constructor(public payload: any) { }
}

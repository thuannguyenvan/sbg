import { Action } from '@ngrx/store';

export class GetListStageAction implements Action {
    public static TYPE = 'GET_LIST_STAGE_ACTION';
    readonly type: string = GetListStageAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessGetListStageAction implements Action {
    public static TYPE = 'SUCCESS_GET_LIST_STAGE_ACTION';
    readonly type: string = SuccessGetListStageAction.TYPE;
    constructor(public payload: any) { }
}

import { Action } from '@ngrx/store';

export class GetCompetitionsEditInfoAction implements Action {
    public static TYPE = 'GET_COMPETITIONS_EDIT_INFO_ACTION';
    readonly type: string = GetCompetitionsEditInfoAction.TYPE;
    constructor(public payload: any = null) { }
}

export class SuccessGetCompetitionsEditInfoAction implements Action {
    public static TYPE = 'SUCCESS_GET_COMPETITIONS_EDIT_INFO_ACTION';
    readonly type: string = SuccessGetCompetitionsEditInfoAction.TYPE;
    constructor(public payload: any) { }
}

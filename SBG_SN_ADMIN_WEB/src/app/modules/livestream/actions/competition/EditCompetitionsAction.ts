import { Action } from '@ngrx/store';

export class EditCompetitionsAction implements Action {
    public static TYPE = 'EDIT_COMPETITIONS_ACTION';
    readonly type: string = EditCompetitionsAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessEditCompetitionsAction implements Action {
    public static TYPE = 'SUCCESS_EDIT_COMPETITIONS_ACTION';
    readonly type: string = SuccessEditCompetitionsAction.TYPE;
    constructor(public payload: any) { }
}

import { Action } from '@ngrx/store';

export class GetCompetitionsCreateInfoAction implements Action {
    public static TYPE = 'GET_COMPETITIONS_CREATE_INFO_ACTION';
    readonly type: string = GetCompetitionsCreateInfoAction.TYPE;
    constructor(public payload: any = null) { }
}

export class SuccessGetCompetitionsCreateInfoAction implements Action {
    public static TYPE = 'SUCCESS_GET_COMPETITIONS_CREATE_INFO_ACTION';
    readonly type: string = SuccessGetCompetitionsCreateInfoAction.TYPE;
    constructor(public payload: any) { }
}

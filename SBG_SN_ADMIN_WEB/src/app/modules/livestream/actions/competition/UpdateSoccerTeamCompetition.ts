import { Action } from '@ngrx/store';

export class UpdateSoccerTeamCompetition implements Action {
    public static TYPE = 'UPDATE_SOCCER_TEAM_COMPETITION';
    readonly type: string = UpdateSoccerTeamCompetition.TYPE;
    constructor(public payload: any) { }
}

export class SuccessUpdateSoccerTeamCompetition implements Action {
    public static TYPE = 'SUCCESS_UPDATE_SOCCER_TEAM_COMPETITION';
    readonly type: string = SuccessUpdateSoccerTeamCompetition.TYPE;
    constructor(public payload: any) { }
}

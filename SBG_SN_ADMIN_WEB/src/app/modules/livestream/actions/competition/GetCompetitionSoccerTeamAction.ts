import { Action } from '@ngrx/store';

export class GetCompetitionSoccerTeamAction implements Action {
    public static TYPE = 'GET_COMPETITION_SOCCER_TEAM_ACTION';
    readonly type: string = GetCompetitionSoccerTeamAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessGetCompetitionSoccerTeamAction implements Action {
    public static TYPE = 'SUCCESS_GET_COMPETITION_SOCCER_TEAM_ACTION';
    readonly type: string = SuccessGetCompetitionSoccerTeamAction.TYPE;
    constructor(public payload: any) { }
}

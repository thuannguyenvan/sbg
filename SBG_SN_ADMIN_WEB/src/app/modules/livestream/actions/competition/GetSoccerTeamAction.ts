import { Action } from '@ngrx/store';

export class GetSoccerTeamAction implements Action {
    public static TYPE = 'GET_SOCCER_TEAM_ACTION';
    readonly type: string = GetSoccerTeamAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessGetSoccerTeamAction implements Action {
    public static TYPE = 'SUCCESS_GET_SOCCER_TEAM_ACTION';
    readonly type: string = SuccessGetSoccerTeamAction.TYPE;
    constructor(public payload: any) { }
}

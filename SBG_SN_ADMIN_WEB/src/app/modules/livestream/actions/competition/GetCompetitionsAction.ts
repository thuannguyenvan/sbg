import { Action } from '@ngrx/store';

export class GetCompetitionsAction implements Action {
    public static TYPE = 'GET_COMPETITIONS_ACTION';
    readonly type: string = GetCompetitionsAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessGetCompetitionsAction implements Action {
    public static TYPE = 'SUCCESS_GET_COMPETITIONS_ACTION';
    readonly type: string = SuccessGetCompetitionsAction.TYPE;
    constructor(public payload: any) { }
}

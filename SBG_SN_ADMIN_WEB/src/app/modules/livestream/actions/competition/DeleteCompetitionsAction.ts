
import { Action } from '@ngrx/store';

export class DeleteCompetitionsAction implements Action {
    public static TYPE = 'DELETE_COMPETITIONS_ACTION';
    readonly type: string = DeleteCompetitionsAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessDeleteCompetitionsAction implements Action {
    public static TYPE = 'SUCCESS_DELETE_COMPETITIONS_ACTION';
    readonly type: string = SuccessDeleteCompetitionsAction.TYPE;
    constructor(public payload: any) { }
}

import { Action } from '@ngrx/store';

export class GetCompetitionsConfigInfoAction implements Action {
    public static TYPE = 'GET_COMPETITIONS_CONFIG_INFO_ACTION';
    readonly type: string = GetCompetitionsConfigInfoAction.TYPE;
    constructor(public payload: any = null) { }
}

export class SuccessGetCompetitionsConfigInfoAction implements Action {
    public static TYPE = 'SUCCESS_GET_COMPETITIONS_CONFIG_INFO_ACTION';
    readonly type: string = SuccessGetCompetitionsConfigInfoAction.TYPE;
    constructor(public payload: any) { }
}

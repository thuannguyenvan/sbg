import { Action } from '@ngrx/store';

export class GetListCompetitionsAction implements Action {
    public static TYPE = 'GET_LIST_COMPETITIONS_ACTION';
    readonly type: string = GetListCompetitionsAction.TYPE;
    constructor() { }
}

export class SuccessGetListCompetitionsAction implements Action {
    public static TYPE = 'SUCCESS_GET_LIST_COMPETITIONS_ACTION';
    readonly type: string = SuccessGetListCompetitionsAction.TYPE;
    constructor(public payload: any) { }
}

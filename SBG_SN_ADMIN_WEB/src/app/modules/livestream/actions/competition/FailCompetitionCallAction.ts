import { Action } from '@ngrx/store';

export class FailCompetitionCallAction implements Action {
    public static TYPE = 'FAIL_COMPETITION_CALL_ACTION';
    readonly type: string = FailCompetitionCallAction.TYPE;
    constructor(public payload: any) { }
}

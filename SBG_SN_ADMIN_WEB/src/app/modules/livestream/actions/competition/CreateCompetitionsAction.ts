import { Action } from '@ngrx/store';

export class CreateCompetitionsAction implements Action {
    public static TYPE = 'CREATE_COMPETITIONS_ACTION';
    readonly type: string = CreateCompetitionsAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessCreateCompetitionsAction implements Action {
    public static TYPE = 'SUCCESS_CREATE_COMPETITIONS_ACTION';
    readonly type: string = SuccessCreateCompetitionsAction.TYPE;
    constructor(public payload: any) { }
}

import { Action } from '@ngrx/store';

export class GetMatchAction implements Action {
    public static TYPE = 'GET_MATCH_ACTION';
    readonly type: string = GetMatchAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessGetMatchAction implements Action {
    public static TYPE = 'SUCCESS_GET_MATCH_ACTION';
    readonly type: string = SuccessGetMatchAction.TYPE;
    constructor(public payload: any) { }
}

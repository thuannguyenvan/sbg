import { Action } from '@ngrx/store';

export class GetSearchLTInfoAction implements Action {
    public static TYPE = 'GET_SEARCH_LT_INFO_ACTION';
    readonly type: string = GetSearchLTInfoAction.TYPE;
    constructor() { }
}

export class SuccessGetSearchLTInfoAction implements Action {
    public static TYPE = 'SUCCESS_GET_SEARCH_LT_INFO_ACTION';
    readonly type: string = SuccessGetSearchLTInfoAction.TYPE;
    constructor(public payload: any) { }
}

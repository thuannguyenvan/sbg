import { Action } from '@ngrx/store';

export class GetInfoEditlivestreamAction implements Action {
    public static TYPE = 'GET_INFO_EDIT_LIVESTREAM_ACTION';
    readonly type: string = GetInfoEditlivestreamAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessGetInfoEditlivestreamAction implements Action {
    public static TYPE = 'SUCCESS_GET_INFO_EDIT_LIVESTREAM_ACTION';
    readonly type: string = SuccessGetInfoEditlivestreamAction.TYPE;
    constructor(public payload: any) { }
}

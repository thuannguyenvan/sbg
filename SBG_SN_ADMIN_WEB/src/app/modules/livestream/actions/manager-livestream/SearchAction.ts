import { Action } from '@ngrx/store';

export class SearchAction implements Action {
    public static TYPE = 'SEARCH_ACTION';
    readonly type: string = SearchAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessSearchAction implements Action {
    public static TYPE = 'SUCCESS_SEARCH_ACTION';
    readonly type: string = SuccessSearchAction.TYPE;
    constructor(public payload: any) { }
}

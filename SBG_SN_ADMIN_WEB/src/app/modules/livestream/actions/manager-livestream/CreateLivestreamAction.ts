import { Action } from '@ngrx/store';

export class CreateLivestreamAction implements Action {
    public static TYPE = 'CREATE_LIVESTREAM_ACTION';
    readonly type: string = CreateLivestreamAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessCreateLivestreamAction implements Action {
    public static TYPE = 'SUCCESS_CREATE_LIVESTREAM_ACTION';
    readonly type: string = SuccessCreateLivestreamAction.TYPE;
    constructor(public payload: any) { }
}

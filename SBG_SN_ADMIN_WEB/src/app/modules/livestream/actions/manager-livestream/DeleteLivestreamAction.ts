import { Action } from '@ngrx/store';

export class DeleteLivestreamAction implements Action {
    public static TYPE = 'DELETE_LIVESTREAM_ACTION';
    readonly type: string = DeleteLivestreamAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessDeleteLivestreamAction implements Action {
    public static TYPE = 'SUCCESS_DELETE_LIVESTREAM_ACTION';
    readonly type: string = SuccessDeleteLivestreamAction.TYPE;
    constructor(public payload: any) { }
}

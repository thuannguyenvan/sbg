import { Action } from '@ngrx/store';

export class GetInfoCreateLivestreamAction implements Action {
    public static TYPE = 'GET_INFO_CREATE_LIVESTREAM_ACTION';
    readonly type: string = GetInfoCreateLivestreamAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessGetInfoCreateLivestreamAction implements Action {
    public static TYPE = 'SUCCESS_GET_INFO_CREATE_LIVESTREAM_ACTION';
    readonly type: string = SuccessGetInfoCreateLivestreamAction.TYPE;
    constructor(public payload: any) { }
}

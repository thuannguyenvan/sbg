import { Action } from '@ngrx/store';

export class FailManagerLivestreamCallAction implements Action {
    public static TYPE = 'FAIL_MANAGER_LIVESTREAM_CALL_ACTION';
    readonly type: string = FailManagerLivestreamCallAction.TYPE;
    constructor(public payload: any) { }
}

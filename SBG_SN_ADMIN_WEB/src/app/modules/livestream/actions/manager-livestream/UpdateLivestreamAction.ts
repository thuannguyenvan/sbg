import { Action } from '@ngrx/store';

export class UpdateLivestreamAction implements Action {
    public static TYPE = 'UPDATE_LIVESTREAM_ACTION';
    readonly type: string = UpdateLivestreamAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessUpdateLivestreamAction implements Action {
    public static TYPE = 'SUCCESS_UPDATE_LIVESTREAM_ACTION';
    readonly type: string = SuccessUpdateLivestreamAction.TYPE;
    constructor(public payload: any) { }
}

import { Action } from '@ngrx/store';

export class GetCompetitionsForLTAction implements Action {
    public static TYPE = 'GET_COMPETITIONS_FOR_LT_ACTION';
    readonly type: string = GetCompetitionsForLTAction.TYPE;
    constructor() { }
}

export class SuccessGetCompetitionsForLTAction implements Action {
    public static TYPE = 'SUCCESS_GET_COMPETITIONS_FOR_LT_ACTION';
    readonly type: string = SuccessGetCompetitionsForLTAction.TYPE;
    constructor(public payload: any) { }
}

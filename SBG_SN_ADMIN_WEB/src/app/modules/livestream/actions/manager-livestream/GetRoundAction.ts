import { Action } from '@ngrx/store';

export class GetRoundAction implements Action {
    public static TYPE = 'GET_ROUND_ACTION';
    readonly type: string = GetRoundAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessGetRoundAction implements Action {
    public static TYPE = 'SUCCESS_GET_ROUND_ACTION';
    readonly type: string = SuccessGetRoundAction.TYPE;
    constructor(public payload: any) { }
}


import {
    SuccessGetListCompetitionsRoundAction,
    SuccessGetStageByCpAction,
    SuccessCreateRoundAction,
    SuccessUpdateRoundAction,
    SuccessGetRoundInfoAction,
    FailManagerRoundCallAction,
    SuccessGetRoundsAction,
    SuccessDeleteRoundAction,
} from '../actions';
import {
    CompetitionsInfo,
    ErrorResult,
    InfoResult,
    StageModel,
    RoundInfoModel,
    RoundInfo,
} from '../../../models';

export function ManagerRoundReducer(state: any = {}, action: any) {
    switch (action.type) {
        case SuccessGetListCompetitionsRoundAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.getCompetitionsInfo = action.payload;
            } else {
                if (action.payload.data === undefined || action.payload.data === null) {
                    state.getCompetitionsInfo = action.payload;
                } else {
                    action.payload.data = action.payload.data.map((d: any) => new CompetitionsInfo(d));
                    state.getCompetitionsInfo = new InfoResult<Array<CompetitionsInfo>>(action.payload);
                }
            }
            return state;

        case SuccessGetStageByCpAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.GetStageByCp = action.payload;
            } else {
                if (action.payload.data === undefined || action.payload.data === null) {
                    state.GetStageByCp = action.payload;
                } else {
                    action.payload.data = action.payload.data.map((d: any) => new StageModel(d));
                    state.GetStageByCp = new InfoResult<Array<StageModel>>(action.payload);
                }
            }
            return state;

        case SuccessCreateRoundAction.TYPE:
            state.createRound
                = action.payload === undefined || action.payload === null ? action.payload
                    : new InfoResult<boolean>(action.payload);
            return state;

        case SuccessUpdateRoundAction.TYPE:
            state.updateRound
                = action.payload === undefined || action.payload === null ? action.payload
                    : new InfoResult<boolean>(action.payload);
            return state;

        case SuccessGetRoundInfoAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.getRoundInfo = action.payload;
                return state;
            }
            if (action.payload.data === null || action.payload.data === undefined) {
                state.getRoundInfo = new InfoResult<RoundInfoModel>(action.payload);
                return state;
            }
            const roundInfo = action.payload;
            roundInfo.data = new RoundInfoModel(action.payload.data);
            state.getRoundInfo = new InfoResult<RoundInfoModel>(roundInfo);
            return state;

        case SuccessGetRoundsAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
              state.getRounds = action.payload;
            } else {
              if (action.payload.data === undefined || action.payload.data === null) {
                state.getRounds = action.payload;
              } else {
                action.payload.data = action.payload.data.map((d: any) => new RoundInfo(d));
                state.getRounds = new InfoResult<Array<RoundInfo>>(action.payload);
              }
            }
            return state;

        case SuccessDeleteRoundAction.TYPE:
            state.deleteRound
                = action.payload === undefined || action.payload === null ? action.payload
                    : new InfoResult<boolean>(action.payload);
            return state;

        case FailManagerRoundCallAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.error = action.payload;
                return state;
            }
            if (action.payload.info === undefined) {
                state.error = undefined;
                return state;
            }
            const errorResult = new ErrorResult(action.payload.info);
            errorResult.key = action.payload.key;
            state.error = errorResult;
            return state;

        default: {
            return state;
        }
    }
}

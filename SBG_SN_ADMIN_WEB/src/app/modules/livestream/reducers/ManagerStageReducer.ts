
import {
    SuccessGetCompetitionsInfoAction,
    FailManagerStageCallAction,
    SuccessGetListStageAction,
    SuccessDeleteStandingAction,
    SuccessCreateStandingAction,
    SuccessUpdateStageStandingAction,
    SuccessGetStandingInfoAction,
    SuccessUpdateStandingAction,
    SuccessGetTeamInfoByStageIdAction,
} from '../actions';
import {
    CompetitionsInfo,
    ErrorResult,
    InfoResult,
    StandingModel,
    TeamStageInfoModel,
    CompetitionStageStandingModel,
} from '../../../models';

export function ManagerStageReducer(state: any = {}, action: any) {
    switch (action.type) {
        case SuccessGetCompetitionsInfoAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.getCompetitionsInfo = action.payload;
            } else {
                let data = null;
                if (action.payload.data === undefined || action.payload.data === null) {
                    data = action.payload;
                } else {
                    action.payload.data = action.payload.data.map((d: any) => new CompetitionsInfo(d));
                    data = action.payload;
                }
                state.getCompetitionsInfo = new InfoResult<Array<CompetitionsInfo>>(data);
            }
            return state;

        case SuccessGetListStageAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.getListStage = action.payload;
            } else {
                let data = null;
                if (action.payload.data === undefined || action.payload.data === null) {
                    data = action.payload;
                } else {
                    action.payload.data = action.payload.data.map((d: any) => new CompetitionStageStandingModel(d));
                    data = action.payload;
                }
                state.getListStage = new InfoResult<Array<CompetitionStageStandingModel>>(data);
            }
            return state;

        case SuccessGetTeamInfoByStageIdAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.getTeamInfoByStageId = action.payload;
                return state;
            }
            if (action.payload.data === null || action.payload.data === undefined) {
                state.getTeamInfoByStageId = new InfoResult<TeamStageInfoModel>(action.payload);
                return state;
            }
            const teamInfoByStageIdInfo = action.payload;
            teamInfoByStageIdInfo.data = new TeamStageInfoModel(action.payload.data);
            state.getTeamInfoByStageId = new InfoResult<TeamStageInfoModel>(teamInfoByStageIdInfo);
            return state;

        case SuccessDeleteStandingAction.TYPE:
            state.deleteStanding
                = action.payload === undefined || action.payload === null ? action.payload
                    : new InfoResult<Boolean>(action.payload);
            return state;

        case SuccessCreateStandingAction.TYPE:
            state.createStanding
                = action.payload === undefined || action.payload === null ? action.payload
                    : new InfoResult<Boolean>(action.payload);
            return state;

        case SuccessUpdateStageStandingAction.TYPE:
            state.updateStandingStage
                = action.payload === undefined || action.payload === null ? action.payload
                    : new InfoResult<Boolean>(action.payload);
            return state;

        case SuccessUpdateStandingAction.TYPE:
            state.updateStandingStage
                = action.payload === undefined || action.payload === null ? action.payload
                    : new InfoResult<Boolean>(action.payload);
            return state;

        case SuccessGetStandingInfoAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.getInfoStanding = action.payload;
                return state;
            }
            if (action.payload.data === null || action.payload.data === undefined) {
                state.getTeamInfoByStageId = new InfoResult<StandingModel>(action.payload);
                return state;
            }
            const Standing = action.payload;
            Standing.data = new StandingModel(action.payload.data);
            state.getInfoStanding = new InfoResult<StandingModel>(Standing);
            return state;

        case FailManagerStageCallAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.error = action.payload;
                return state;
            }
            if (action.payload.info === undefined) {
                state.error = undefined;
                return state;
            }
            const errorResult = new ErrorResult(action.payload.info);
            errorResult.key = action.payload.key;
            state.error = errorResult;
            return state;

        default: {
            return state;
        }
    }
}

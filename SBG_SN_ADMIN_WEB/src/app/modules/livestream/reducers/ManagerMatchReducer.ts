
import {
    SuccessGetRoundByCpCreateMatchAction,
    SuccessGetTeamByRoundCreateMatchAction,
    SuccessCreateMatchAction,
    SuccessEditMatchAction,
    SuccessGetEditMatchInfoAction,
    SuccessGetCreateMatchInfoAction,
    SuccessGetSearchInfoMatchListAction,
    SuccessGetRoundByCompetitionInfoListAction,
    SuccessGetMatchInfoListAction,
    FailSoccerMatchMatchCallAction,
    SuccessDeleteSoccerMatchAction,
} from '../actions';
import {
    ErrorResult,
    InfoResult,
    RoundInfoModel,
    SoccerMatchInfo,
    TeamsModel,
    SoccerMatchUpdateInfoModel,
    MatchListInfo,
    MatchRoundModel,
    MatchSearchInfo
} from '../../../models';

export function ManagerMatchReducer(state: any = {}, action: any) {
    switch (action.type) {
        case SuccessGetCreateMatchInfoAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.getCreateMatchinfo = action.payload;
                return state;
            }
            if (action.payload.data === null || action.payload.data === undefined) {
                state.getCreateMatchinfo = new InfoResult<SoccerMatchInfo>(action.payload);
                return state;
            }
            const addMatchInfo = action.payload;
            addMatchInfo.data = new SoccerMatchInfo(action.payload.data);
            state.getCreateMatchinfo = new InfoResult<SoccerMatchInfo>(addMatchInfo);
            return state;

        case SuccessGetRoundByCpCreateMatchAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.getRoundByCp = action.payload;
            } else {
                if (action.payload.data === undefined || action.payload.data === null) {
                    state.getRoundByCp = action.payload;
                } else {
                    action.payload.data = action.payload.data.map((d: any) => new RoundInfoModel(d));
                    state.getRoundByCp = new InfoResult<Array<RoundInfoModel>>(action.payload);
                }
            }
            return state;

        case SuccessGetTeamByRoundCreateMatchAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.getTeamByRound = action.payload;
            } else {
                if (action.payload.data === undefined || action.payload.data === null) {
                    state.getTeamByRound = action.payload;
                } else {
                    action.payload.data = action.payload.data.map((d: any) => new TeamsModel(d));
                    state.getTeamByRound = new InfoResult<Array<TeamsModel>>(action.payload);
                }
            }
            return state;

        case SuccessCreateMatchAction.TYPE:
            state.createMatch
                = action.payload === undefined || action.payload === null ? action.payload
                    : new InfoResult<boolean>(action.payload);
            return state;

        case SuccessEditMatchAction.TYPE:
            state.updateMatch
                = action.payload === undefined || action.payload === null ? action.payload
                    : new InfoResult<boolean>(action.payload);
            return state;

        case SuccessGetEditMatchInfoAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.getEditInfo = action.payload;
                return state;
            }
            if (action.payload.data === null || action.payload.data === undefined) {
                state.getEditInfo = new InfoResult<SoccerMatchUpdateInfoModel>(action.payload);
                return state;
            }
            const editMatchInfo = action.payload;
            editMatchInfo.data = new SoccerMatchUpdateInfoModel(action.payload.data);
            state.getEditInfo = new InfoResult<SoccerMatchUpdateInfoModel>(editMatchInfo);
            return state;

        // search danh sách trận đấu
        case SuccessGetMatchInfoListAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.getMatchInfoList = action.payload;
            } else {
                if (action.payload.data === undefined || action.payload.data === null) {
                    state.getMatchInfoList = action.payload;
                } else {
                    action.payload.data = action.payload.data.map((d: any) => new MatchListInfo(d));
                    state.getMatchInfoList = new InfoResult<Array<MatchListInfo>>(action.payload);
                }
            }
            return state;

        // lấy danh sách round từ giải đấu
        case SuccessGetRoundByCompetitionInfoListAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.getRoundByCompetitionInfoList = action.payload;
            } else {
                if (action.payload.data === undefined || action.payload.data === null) {
                    state.getRoundByCompetitionInfoList = action.payload;
                } else {
                    action.payload.data = action.payload.data.map((d: any) => new MatchRoundModel(d));
                    state.getRoundByCompetitionInfoList = new InfoResult<Array<MatchRoundModel>>(action.payload);
                }
            }
            return state;

        // lấy thông tin để search trận đấu
        case SuccessGetSearchInfoMatchListAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.getSearchInfoMatchList = action.payload;
                return state;
            }
            if (action.payload.data === null || action.payload.data === undefined) {
                state.getSearchInfoMatchList = new InfoResult<MatchSearchInfo>(action.payload);
                return state;
            }
            const roundInfo = action.payload;
            roundInfo.data = new MatchSearchInfo(action.payload.data);
            state.getSearchInfoMatchList = new InfoResult<MatchSearchInfo>(roundInfo);
            return state;

        // delete
        case SuccessDeleteSoccerMatchAction.TYPE:
            state.deleteMatch
                = action.payload === undefined || action.payload === null ? action.payload
                    : new InfoResult<boolean>(action.payload);
            return state;

        case FailSoccerMatchMatchCallAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.error = action.payload;
                return state;
            }
            if (action.payload.info === undefined) {
                state.error = undefined;
                return state;
            }
            const errorResult = new ErrorResult(action.payload.info);
            errorResult.key = action.payload.key;
            state.error = errorResult;
            return state;

        default: {
            return state;
        }
    }
}


import {
    SuccessGetRoundAction,
    FailManagerLivestreamCallAction,
    SuccessGetMatchAction,
    SuccessSearchAction,
    SuccessGetInfoEditlivestreamAction,
    SuccessCreateLivestreamAction,
    SuccessDeleteLivestreamAction,
    SuccessUpdateLivestreamAction,
    SuccessGetSearchLTInfoAction,
    SuccessGetInfoCreateLivestreamAction,
} from '../actions';
import {
    ErrorResult,
    InfoResult,
    LivestreamModel,
    RoundModel,
    MatchModel,
    SearchInfoLTModel,
    CreateLivestreamInfoModel,
    EditLivestreamInfoModel,
} from '../../../models';

export function ManagerLivestreamReducer(state: any = {}, action: any) {
    switch (action.type) {
        case SuccessGetSearchLTInfoAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.getSearchInfo = action.payload;
                return state;
            }
            if (action.payload.data === null || action.payload.data === undefined) {
                state.getSearchInfo = new InfoResult<SearchInfoLTModel>(action.payload);
                return state;
            }
            const infolivestream = action.payload;
            infolivestream.data = new SearchInfoLTModel(action.payload.data);
            state.getSearchInfo = new InfoResult<SearchInfoLTModel>(infolivestream);
            return state;

        case SuccessGetRoundAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.getRound = action.payload;
            } else {
                let data = null;
                if (action.payload.data === undefined || action.payload.data === null) {
                    data = action.payload;
                } else {
                    action.payload.data = action.payload.data.map((d) => new RoundModel(d));
                    data = action.payload;
                }
                state.getRound = new InfoResult<Array<RoundModel>>(data);
            }
            return state;

        case SuccessGetMatchAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.getMatch = action.payload;
            } else {
                let data = null;
                if (action.payload.data === undefined || action.payload.data === null) {
                    data = action.payload;
                } else {
                    action.payload.data = action.payload.data.map((d) => new MatchModel(d));
                    data = action.payload;
                }
                state.getMatch = new InfoResult<Array<MatchModel>>(data);
            }
            return state;

        case SuccessSearchAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.Search = action.payload;
            } else {
                let data = null;
                if (action.payload.data === undefined || action.payload.data === null) {
                    data = action.payload;
                } else {
                    action.payload.data = action.payload.data.map((d) => new LivestreamModel(d));
                    data = action.payload;
                }
                state.Search = new InfoResult<Array<LivestreamModel>>(data);
            }
            return state;

        case SuccessGetInfoEditlivestreamAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.getEditInfo = action.payload;
                return state;
            }
            if (action.payload.data === null || action.payload.data === undefined) {
                state.getEditInfo = new InfoResult<EditLivestreamInfoModel>(action.payload);
                return state;
            }
            const InfoEditlivestream = action.payload;
            InfoEditlivestream.data = new EditLivestreamInfoModel(action.payload.data);
            state.getEditInfo = new InfoResult<EditLivestreamInfoModel>(InfoEditlivestream);
            return state;

        case SuccessGetInfoCreateLivestreamAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.getCreateInfo = action.payload;
                return state;
            }
            if (action.payload.data === null || action.payload.data === undefined) {
                state.getCreateInfo = new InfoResult<CreateLivestreamInfoModel>(action.payload);
                return state;
            }
            const InfoCreatelivestream = action.payload;
            InfoCreatelivestream.data = new CreateLivestreamInfoModel(action.payload.data);
            state.getCreateInfo = new InfoResult<CreateLivestreamInfoModel>(InfoCreatelivestream);
            return state;

        case SuccessCreateLivestreamAction.TYPE:
            state.create
                = action.payload === undefined || action.payload === null ? action.payload
                    : new InfoResult<boolean>(action.payload);
            return state;

        case SuccessDeleteLivestreamAction.TYPE:
            state.delete
                = action.payload === undefined || action.payload === null ? action.payload
                    : new InfoResult<boolean>(action.payload);
            return state;

        case SuccessUpdateLivestreamAction.TYPE:
            state.update
                = action.payload === undefined || action.payload === null ? action.payload
                    : new InfoResult<boolean>(action.payload);
            return state;

        case FailManagerLivestreamCallAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.error = action.payload;
                return state;
            }
            if (action.payload.info === undefined) {
                state.error = undefined;
                return state;
            }
            const errorResult = new ErrorResult(action.payload.info);
            errorResult.key = action.payload.key;
            state.error = errorResult;
            return state;

        default: {
            return state;
        }
    }
}

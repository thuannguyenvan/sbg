import {
  ErrorResult,
  InfoResult,
  CompetitionsInfo,
  CompetitionCreateEditInfo,
  CompetitionSoccerTeamInfo,
  CompetitionTeamInfoModel,
  CompetitionTeamModel,
} from '../../../models';

import {
  FailCompetitionCallAction,
  SuccessGetCompetitionsAction,
  SuccessCreateCompetitionsAction,
  SuccessDeleteCompetitionsAction,
  SuccessGetCompetitionsCreateInfoAction,
  SuccessGetCompetitionsEditInfoAction,
  SuccessGetSoccerTeamAction,
  SuccessGetCompetitionSoccerTeamAction,
  SuccessGetListCompetitionsAction,
  SuccessGetCompetitionsConfigInfoAction,
  SuccessEditCompetitionsAction,
  SuccessUpdateSoccerTeamCompetition,
} from '../actions';

export function CompetitionReducer(state: any = {}, action: any) {
  switch (action.type) {
    case SuccessGetCompetitionsAction.TYPE:
      if (action.payload === undefined || action.payload === null) {
        state.getCompetitionsInfo = action.payload;
      } else {
        if (action.payload.data === undefined || action.payload.data === null) {
          state.getCompetitionsInfo = action.payload;
        } else {
          action.payload.data = action.payload.data.map((d: any) => new CompetitionsInfo(d));
          state.getCompetitionsInfo = new InfoResult<Array<CompetitionsInfo>>(action.payload);
        }
      }
      return state;

    case SuccessGetListCompetitionsAction.TYPE:
      if (action.payload === undefined || action.payload === null) {
        state.getListCompetitionsInfo = action.payload;
      } else {
        if (action.payload.data === undefined || action.payload.data === null) {
          state.getListCompetitionsInfo = action.payload;
        } else {
          action.payload.data = action.payload.data.map((d: any) => new CompetitionsInfo(d));
          state.getListCompetitionsInfo = new InfoResult<Array<CompetitionsInfo>>(action.payload);
        }
      }
      return state;

    case SuccessGetCompetitionsConfigInfoAction.TYPE:
      if (action.payload === undefined || action.payload === null) {
        state.getCompetitionsTeamInfo = action.payload;
        return state;
      }
      if (action.payload.data === null || action.payload.data === undefined) {
        state.getCompetitionsTeamInfo = new InfoResult<CompetitionTeamInfoModel>(action.payload);
        return state;
      }
      const teamInfo = action.payload;
      teamInfo.data = new CompetitionTeamInfoModel(action.payload.data);
      state.getCompetitionsTeamInfo = new InfoResult<CompetitionTeamInfoModel>(teamInfo);
      return state;

    case SuccessGetCompetitionsCreateInfoAction.TYPE:
      if (action.payload === undefined || action.payload === null) {
        state.getCompetitionsCreateInfo = action.payload;
        return state;
      }
      if (action.payload.data === null || action.payload.data === undefined) {
        state.getCompetitionsCreateInfo = new InfoResult<CompetitionCreateEditInfo>(action.payload);
        return state;
      }
      const info = action.payload;
      info.data = new CompetitionCreateEditInfo(action.payload.data);
      state.getCompetitionsCreateInfo = new InfoResult<CompetitionCreateEditInfo>(info);
      return state;

    case SuccessGetCompetitionsEditInfoAction.TYPE:
      if (action.payload === undefined || action.payload === null) {
        state.getCompetitionsEditInfo = action.payload;
        return state;
      }
      if (action.payload.data === null || action.payload.data === undefined) {
        state.getCompetitionsEditInfo = new InfoResult<CompetitionCreateEditInfo>(action.payload);
        return state;
      }
      const infoEdit = action.payload;
      infoEdit.data = new CompetitionCreateEditInfo(action.payload.data);
      state.getCompetitionsEditInfo = new InfoResult<CompetitionCreateEditInfo>(infoEdit);
      return state;

    case SuccessCreateCompetitionsAction.TYPE:
      state.createCompetition
        = action.payload === undefined || action.payload === null ? action.payload
          : new InfoResult<any>(action.payload);
      return state;

    case SuccessUpdateSoccerTeamCompetition.TYPE:
      state.updateConfigTeam
        = action.payload === undefined || action.payload === null ? action.payload
          : new InfoResult<any>(action.payload);
      return state;

    case SuccessEditCompetitionsAction.TYPE:
      state.editCompetition
        = action.payload === undefined || action.payload === null ? action.payload
          : new InfoResult<any>(action.payload);
      return state;

    case SuccessDeleteCompetitionsAction.TYPE:
      state.deleteCompetition
        = action.payload === undefined || action.payload === null ? action.payload
          : new InfoResult<any>(action.payload);
      return state;

    case SuccessDeleteCompetitionsAction.TYPE:
      state.deleteCompetition
        = action.payload === undefined || action.payload === null ? action.payload
          : new InfoResult<any>(action.payload);
      return state;

    // get soccer Team
    case SuccessGetSoccerTeamAction.TYPE:
      if (action.payload === undefined || action.payload === null) {
        state.getSoccerTeam = action.payload;
      } else {
        if (action.payload.data === undefined || action.payload.data === null) {
          state.getSoccerTeam = action.payload;
        } else {
          action.payload.data = action.payload.data.map((d: any) => new CompetitionTeamModel(d));
          state.getSoccerTeam = new InfoResult<Array<CompetitionTeamModel>>(action.payload);
        }
      }
      return state;

    // get Competition soccer team
    case SuccessGetCompetitionSoccerTeamAction.TYPE:
      if (action.payload === undefined || action.payload === null) {
        state.getCompetitionsSoccerTeam = action.payload;
        return state;
      }
      if (action.payload.data === null || action.payload.data === undefined) {
        state.getCompetitionsSoccerTeam = new InfoResult<CompetitionSoccerTeamInfo>(action.payload);
        return state;
      }
      const infoCompetitionSoccerTeam = action.payload;
      infoCompetitionSoccerTeam.data = new CompetitionSoccerTeamInfo(action.payload.data);
      state.getCompetitionsSoccerTeam =
        new InfoResult<CompetitionSoccerTeamInfo>(infoCompetitionSoccerTeam);
      return state;

    case FailCompetitionCallAction.TYPE:
      if (action.payload === undefined || action.payload === null) {
        state.error = action.payload;
        return state;
      }
      if (action.payload.info === undefined) {
        state.error = undefined;
        return state;
      }
      const errorResult = new ErrorResult(action.payload.info);
      errorResult.key = action.payload.key;
      state.error = errorResult;
      return state;

    default: {
      return state;
    }
  }
}

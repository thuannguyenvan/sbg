import {
    NgModule,
} from '@angular/core';
import {
    RouterModule,
} from '@angular/router';
import {
    BrowserAnimationsModule,
} from '@angular/platform-browser/animations';
import {
    CommonModule,
} from '@angular/common';
import {
    BrowserModule,
} from '@angular/platform-browser';
import {
    EffectsModule,
} from '@ngrx/effects';
import {
    StoreModule,
} from '@ngrx/store';
import {
    FormsModule,
    ReactiveFormsModule,
} from '@angular/forms';
import {
    SharedModule,
} from '../../modules';
import {
    ManagerPartnerEffect,
    ManagerAdvertisingsEffect,
    ManagerAdvertisingsConfigEffect,
} from './effects';
import {
    EnvService,
} from '../../services';

import {
    ManagerPartnerComponent,
    CreateEditPartnerComponent,
    CreateEditManagerAdvertisingsComponent,
    ManagerAdvertisingsComponent,
    ManagerAdvertisingsConfigComponent,
    CreateEditManagerAdvertisingsConfigComponent,
    PopupAddAdvertisingComponent,
} from './components';
import {
    ManagerPartnerReducer,
    ManagerAdvertisingReducer,
    ManagerAdvertisingConfigReducer,
} from './reducers';

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        FormsModule,
        BrowserModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        RouterModule.forChild([
            {
                path: 'ads',
                children: [
                    {
                        path: 'partners',
                        children: [
                            { path: 'create', component: CreateEditPartnerComponent },
                            { path: 'edit/:id', component: CreateEditPartnerComponent },
                            { path: 'list', component: ManagerPartnerComponent },
                        ]
                    },
                    {
                        path: 'advertisings',
                        children: [
                            { path: 'create', component: CreateEditManagerAdvertisingsComponent },
                            { path: 'create/:partnerId', component: CreateEditManagerAdvertisingsComponent },
                            { path: 'edit/:id', component: CreateEditManagerAdvertisingsComponent },
                            { path: 'list', component: ManagerAdvertisingsComponent },
                        ]
                    },
                    {
                        path: 'advertisingsConfig',
                        children: [
                            { path: 'create', component: CreateEditManagerAdvertisingsConfigComponent },
                            { path: 'edit/:id', component: CreateEditManagerAdvertisingsConfigComponent },
                            { path: 'list', component: ManagerAdvertisingsConfigComponent },
                        ]
                    },
                ]
            }
        ]),
        EffectsModule.forFeature([
            ManagerPartnerEffect,
            ManagerAdvertisingsEffect,
            ManagerAdvertisingsConfigEffect,
        ]),
        StoreModule.forFeature('managerPartner', ManagerPartnerReducer),
        StoreModule.forFeature('managerAdvertisings', ManagerAdvertisingReducer),
        StoreModule.forFeature('managerAdvertisingsConfig', ManagerAdvertisingConfigReducer),
    ],
    declarations: [
        ManagerPartnerComponent,
        CreateEditPartnerComponent,
        CreateEditManagerAdvertisingsComponent,
        ManagerAdvertisingsComponent,
        ManagerAdvertisingsConfigComponent,
        CreateEditManagerAdvertisingsConfigComponent,
        PopupAddAdvertisingComponent,
    ],
    entryComponents: [
        PopupAddAdvertisingComponent,
    ],
    providers: [
        EnvService,
    ]
})
export class AdvertisingModule { }

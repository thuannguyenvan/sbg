
import {
    SuccessSearchAdvertisingConfigAction,
    SuccessGetEditAdvertisingConfigInfoAction,
    SuccessCreateAdvertisingConfigAction,
    SuccessEditAdvertisingConfigAction,
    SuccessDeActiveAdvertisingConfigAction,
    SuccessActiveAdvertisingConfigAction,
    SuccessDeleteAdvertisingConfigAction,
    FailAdvertisingConfigCallAction,
    SuccessGetAdvertisingSearchInfoPopUpAction,
    SuccessAdvertisingSearchPopUpAction
} from '../actions';
import {
    ErrorResult,
    InfoResult,
    AdvertisingConfigModel,
    AdvertisingModel,
    AdvertisingConfigSearchModel,
    SearchInfoPopUpAdvertisingModel,
} from '../../../models';

export function ManagerAdvertisingConfigReducer(state: any = {}, action: any) {
    switch (action.type) {
        case SuccessAdvertisingSearchPopUpAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.searchPopUp = action.payload;
            } else {
                let data = null;
                if (action.payload.data === undefined || action.payload.data === null) {
                    data = action.payload;
                } else {
                    action.payload.data = action.payload.data.map((d) => new AdvertisingModel(d));
                    data = action.payload;
                }
                state.searchPopUp = new InfoResult<Array<AdvertisingModel>>(data);
            }
            return state;

        case SuccessGetAdvertisingSearchInfoPopUpAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.getSearchPopUpInfo = action.payload;
                return state;
            }
            if (action.payload.data === null || action.payload.data === undefined) {
                state.getSearchPopUpInfo = new InfoResult<SearchInfoPopUpAdvertisingModel>(action.payload);
                return state;
            }
            const infoCreateAdvertising = action.payload;
            infoCreateAdvertising.data = new SearchInfoPopUpAdvertisingModel(action.payload.data);
            state.getSearchPopUpInfo = new InfoResult<SearchInfoPopUpAdvertisingModel>(infoCreateAdvertising);
            return state;

        case SuccessSearchAdvertisingConfigAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.search = action.payload;
            } else {
                let data = null;
                if (action.payload.data === undefined || action.payload.data === null) {
                    data = action.payload;
                } else {
                    action.payload.data = action.payload.data.map((d) => new AdvertisingConfigSearchModel(d));
                    data = action.payload;
                }
                state.search = new InfoResult<Array<AdvertisingConfigSearchModel>>(data);
            }
            return state;

        case SuccessGetEditAdvertisingConfigInfoAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.getEditInfo = action.payload;
                return state;
            }
            if (action.payload.data === null || action.payload.data === undefined) {
                state.getEditInfo = new InfoResult<AdvertisingConfigModel>(action.payload);
                return state;
            }
            const infoEditAdvertisingConfig = action.payload;
            infoEditAdvertisingConfig.data = new AdvertisingConfigModel(action.payload.data);
            state.getEditInfo = new InfoResult<AdvertisingConfigModel>(infoEditAdvertisingConfig);
            return state;

        case SuccessCreateAdvertisingConfigAction.TYPE:
            state.create
                = action.payload === undefined || action.payload === null ? action.payload
                    : new InfoResult<boolean>(action.payload);
            return state;

        case SuccessDeleteAdvertisingConfigAction.TYPE:
            state.delete
                = action.payload === undefined || action.payload === null ? action.payload
                    : new InfoResult<boolean>(action.payload);
            return state;

        case SuccessActiveAdvertisingConfigAction.TYPE:
            state.activate
                = action.payload === undefined || action.payload === null ? action.payload
                    : new InfoResult<boolean>(action.payload);
            return state;

        case SuccessDeActiveAdvertisingConfigAction.TYPE:
            state.deactivate
                = action.payload === undefined || action.payload === null ? action.payload
                    : new InfoResult<boolean>(action.payload);
            return state;

        case SuccessEditAdvertisingConfigAction.TYPE:
            state.update
                = action.payload === undefined || action.payload === null ? action.payload
                    : new InfoResult<boolean>(action.payload);
            return state;

        case FailAdvertisingConfigCallAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.error = action.payload;
                return state;
            }
            if (action.payload.info === undefined) {
                state.error = undefined;
                return state;
            }
            const errorResult = new ErrorResult(action.payload.info);
            errorResult.key = action.payload.key;
            state.error = errorResult;
            return state;

        default: {
            return state;
        }
    }
}

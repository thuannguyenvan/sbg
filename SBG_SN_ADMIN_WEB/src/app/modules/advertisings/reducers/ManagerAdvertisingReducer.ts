
import {
    SuccessGetSearchAdvertisingInfoAction,
    FailAdvertisingCallAction,
    SuccessSearchAdvertisingAction,
    SuccessGetEditAdvertisingInfoAction,
    SuccessEditAdvertisingAction,
    SuccessDeleteAdvertisingAction,
    SuccessDeActiveAdvertisingAction,
    SuccessCreateAdvertisingAction,
    SuccessActiveAdvertisingAction,
    SuccessGetCreateInfoAdvertisingAction
} from '../actions';
import {
    ErrorResult,
    InfoResult,
    SearchInfoAdvertisingModel,
    AdvertisingModel,
    EditInfoAdvertisingModel,
} from '../../../models';

export function ManagerAdvertisingReducer(state: any = {}, action: any) {
    switch (action.type) {
        case SuccessGetSearchAdvertisingInfoAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.getSearchInfo = action.payload;
                return state;
            }
            if (action.payload.data === null || action.payload.data === undefined) {
                state.getSearchInfo = new InfoResult<SearchInfoAdvertisingModel>(action.payload);
                return state;
            }
            const infoAdvertising = action.payload;
            infoAdvertising.data = new SearchInfoAdvertisingModel(action.payload.data);
            state.getSearchInfo = new InfoResult<SearchInfoAdvertisingModel>(infoAdvertising);
            return state;

        case SuccessGetCreateInfoAdvertisingAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.getCreateInfo = action.payload;
                return state;
            }
            if (action.payload.data === null || action.payload.data === undefined) {
                state.getCreateInfo = new InfoResult<SearchInfoAdvertisingModel>(action.payload);
                return state;
            }
            const infoCreateAdvertising = action.payload;
            infoCreateAdvertising.data = new SearchInfoAdvertisingModel(action.payload.data);
            state.getCreateInfo = new InfoResult<SearchInfoAdvertisingModel>(infoCreateAdvertising);
            return state;

        case SuccessSearchAdvertisingAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.search = action.payload;
            } else {
                let data = null;
                if (action.payload.data === undefined || action.payload.data === null) {
                    data = action.payload;
                } else {
                    action.payload.data = action.payload.data.map((d) => new AdvertisingModel(d));
                    data = action.payload;
                }
                state.search = new InfoResult<Array<AdvertisingModel>>(data);
            }
            return state;

        case SuccessGetEditAdvertisingInfoAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.getEditInfo = action.payload;
                return state;
            }
            if (action.payload.data === null || action.payload.data === undefined) {
                state.getEditInfo = new InfoResult<EditInfoAdvertisingModel>(action.payload);
                return state;
            }
            const infoEditAdvertising = action.payload;
            infoEditAdvertising.data = new EditInfoAdvertisingModel(action.payload.data);
            state.getEditInfo = new InfoResult<EditInfoAdvertisingModel>(infoEditAdvertising);
            return state;

        case SuccessCreateAdvertisingAction.TYPE:
            state.create
                = action.payload === undefined || action.payload === null ? action.payload
                    : new InfoResult<boolean>(action.payload);
            return state;

        case SuccessDeleteAdvertisingAction.TYPE:
            state.delete
                = action.payload === undefined || action.payload === null ? action.payload
                    : new InfoResult<boolean>(action.payload);
            return state;

        case SuccessActiveAdvertisingAction.TYPE:
            state.activate
                = action.payload === undefined || action.payload === null ? action.payload
                    : new InfoResult<boolean>(action.payload);
            return state;

        case SuccessDeActiveAdvertisingAction.TYPE:
            state.deactivate
                = action.payload === undefined || action.payload === null ? action.payload
                    : new InfoResult<boolean>(action.payload);
            return state;

        case SuccessEditAdvertisingAction.TYPE:
            state.update
                = action.payload === undefined || action.payload === null ? action.payload
                    : new InfoResult<boolean>(action.payload);
            return state;

        case FailAdvertisingCallAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.error = action.payload;
                return state;
            }
            if (action.payload.info === undefined) {
                state.error = undefined;
                return state;
            }
            const errorResult = new ErrorResult(action.payload.info);
            errorResult.key = action.payload.key;
            state.error = errorResult;
            return state;

        default: {
            return state;
        }
    }
}

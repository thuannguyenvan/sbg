
import {
    SuccessSearchPartnerAction,
    FailPartnerCallAction,
    SuccessEditPartnerAction,
    SuccessDeletePartnerAction,
    SuccessCreatePartnerAction,
    SuccessActivePartnerAction,
    SuccessDeActivePartnerAction,
    SuccessGetEditPartnerInfoAction,
} from '../actions';
import {
    ErrorResult,
    InfoResult,
    PartnerModel,
} from '../../../models';

export function ManagerPartnerReducer(state: any = {}, action: any) {
    switch (action.type) {
        case SuccessSearchPartnerAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.Search = action.payload;
            } else {
                let data = null;
                if (action.payload.data === undefined || action.payload.data === null) {
                    data = action.payload;
                } else {
                    action.payload.data = action.payload.data.map((d) => new PartnerModel(d));
                    data = action.payload;
                }
                state.Search = new InfoResult<Array<PartnerModel>>(data);
            }
            return state;

        case SuccessCreatePartnerAction.TYPE:
            state.create
                = action.payload === undefined || action.payload === null ? action.payload
                    : new InfoResult<boolean>(action.payload);
            return state;

        case SuccessDeletePartnerAction.TYPE:
            state.delete
                = action.payload === undefined || action.payload === null ? action.payload
                    : new InfoResult<boolean>(action.payload);
            return state;

        case SuccessActivePartnerAction.TYPE:
            state.active
                = action.payload === undefined || action.payload === null ? action.payload
                    : new InfoResult<boolean>(action.payload);
            return state;

        case SuccessDeActivePartnerAction.TYPE:
            state.deActive
                = action.payload === undefined || action.payload === null ? action.payload
                    : new InfoResult<boolean>(action.payload);
            return state;

        case SuccessEditPartnerAction.TYPE:
            state.update
                = action.payload === undefined || action.payload === null ? action.payload
                    : new InfoResult<boolean>(action.payload);
            return state;

        case SuccessGetEditPartnerInfoAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.getEditInfo = action.payload;
                return state;
            }
            if (action.payload.data === null || action.payload.data === undefined) {
                state.getEditInfo = new InfoResult<PartnerModel>(action.payload);
                return state;
            }
            const InfoEditPartner = action.payload;
            InfoEditPartner.data = new PartnerModel(action.payload.data);
            state.getEditInfo = new InfoResult<PartnerModel>(InfoEditPartner);
            return state;

        case FailPartnerCallAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.error = action.payload;
                return state;
            }
            if (action.payload.info === undefined) {
                state.error = undefined;
                return state;
            }
            const errorResult = new ErrorResult(action.payload.info);
            errorResult.key = action.payload.key;
            state.error = errorResult;
            return state;

        default: {
            return state;
        }
    }
}

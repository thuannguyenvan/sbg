import {
  Component,
  Inject
} from '@angular/core';
import {
  Store,
} from '@ngrx/store';
import {
  Subscription,
} from 'rxjs';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatTableDataSource,
} from '@angular/material';
import {
  FormBuilder,
  FormControl,
  Validators,
} from '@angular/forms';
import {
  AppState,
} from '../../../../../AppState';
import {
  SpinnerHelper,
  ToastrHelper,
  ValidateHelper,
} from '../../../../../helpers';
import {
  GetAdvertisingSearchInfoPopUpAction,
  AdvertisingSearchPopUpAction,
  FailAdvertisingConfigCallAction,
  SuccessGetAdvertisingSearchInfoPopUpAction,
  SuccessAdvertisingSearchPopUpAction,
} from '../../../actions';
import {
  InfoResult,
  ErrorResult,
  PartnerModel,
  AdvertisingModel,
  Pagination,
  SearchInfoPopUpAdvertisingModel,
} from '../../../../../models';
import {
  IBaseComponent,
  ValidateBaseComponent,
} from '../../../../../bases';
import {
  TranslateKeyConstants, LenghtValidateConstants,
} from '../../../../../constants';
import {
  TranslateService,
} from '../../../../../services';

@Component({
  selector: 'app-popup-add-advertising',
  templateUrl: './index.html',
  styleUrls: ['./index.scss'],
})
export class PopupAddAdvertisingComponent extends ValidateBaseComponent implements IBaseComponent {
  subscriptions: Subscription[] = [];
  public translateKey: any;
  public hidePoup: boolean;
  public model: any = {};
  public modelSearch: any = {};
  public headerPopup: string;
  public listAdsPartners: Array<PartnerModel> = new Array<PartnerModel>();
  public pagination: Pagination = new Pagination();
  public dataSource: MatTableDataSource<any>;
  public ShowAccept: Boolean = false;
  public objectResult: AdvertisingModel;
  public selectedRowIndex: Number = -1;

  constructor(
    private store: Store<AppState>,
    private spinner: SpinnerHelper,
    private toastr: ToastrHelper,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<PopupAddAdvertisingComponent>,
    @Inject(FormBuilder) fb: FormBuilder,
    private validateHelper: ValidateHelper,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private translate: TranslateService,
  ) {
    super(validateHelper);
    this.translateKey = TranslateKeyConstants;
    this.initValidate(fb);
  }

  ngOnInit() {
    this.hidePoup = true;
    this.onSubscribeError();
    this.onSubscribeSuccess();
    setTimeout(() => {
      this.spinner.show();
      this.store.dispatch(new GetAdvertisingSearchInfoPopUpAction());
    }, 100);
  }

  initValidate(fb: FormBuilder) {
    this.validators = {
      title: [Validators.maxLength(LenghtValidateConstants.STRING_MEDIUM)],
    };

    this.formGroup = fb.group({
      title: new FormControl(this.model.title, this.validators.title),
    });
  }

  onSubscribeSuccess() {
    this.subscriptions.push(this.store.select<InfoResult<SearchInfoPopUpAdvertisingModel>>
      (state => state.managerAdvertisingsConfig.getSearchPopUpInfo)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.data !== undefined && result.data !== null) {
            this.hidePoup = false;
            this.spinner.hide();
            const selectEnumDefaultPartner = new PartnerModel(
              {
                id: '',
                name: this.translate.translate(this.translateKey.ADVERTISING_CONFIG_MANAGER.SELECT_ALL)
              });
            this.listAdsPartners = [selectEnumDefaultPartner];
            this.listAdsPartners = this.listAdsPartners.concat(result.data.adsPartners);
            this.model.adsPartnerId = '';
            this.onSubmit();
          }
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<Array<AdvertisingModel>>>
      (state => state.managerAdvertisingsConfig.searchPopUp)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.data !== undefined && result.data !== null) {
            this.hidePoup = false;
            this.spinner.hide();
            if (result.data.length > 0) {
              this.pagination.length = result.data[0].total;
            } else {
              this.pagination.length = 0;
            }
            if (this.data !== undefined && this.data !== null) {
            }
            this.dataSource = new MatTableDataSource(result.data);
          } else {
            this.toastr.warning(this.translate.translate(this.translateKey.ADVERTISING_CONFIG_MANAGER.MESSAGE_NONE_SELECT_ADDVERTISING));
            this.spinner.hide();
            this.dialogRef.close();
          }
        }
      }));
  }
  onSearch() {
    this.spinner.show();
    this.modelSearch.page = this.pagination.pageIndex;
    this.modelSearch.size = this.pagination.pageSize;
    this.store.dispatch(new AdvertisingSearchPopUpAction(this.modelSearch));
  }

  onPageChanged(page: any) {
    this.pagination.pageIndex = 1;
    this.pagination.pageSize = page.pageSize;
    this.onSearch();
  }

  onSubmit() {
    this.modelSearch = {
      title: this.model.title,
      adsPartnerId: this.model.adsPartnerId === '' ? undefined : this.model.adsPartnerId
    };
    this.onSearch();
  }

  onClose() {
    this.dialogRef.close();
  }

  onAccept(ads: AdvertisingModel) {
    if (ads !== undefined || ads !== null) {
      this.dialogRef.close(ads);
    } else {
      return;
    }
  }

  onSubscribeError() {
    this.subscriptions.push(this.store.select<ErrorResult>(state => state.managerAdvertisingsConfig.error)
      .subscribe((result) => {
        if (result !== undefined) {
          switch (result.key) {
            case GetAdvertisingSearchInfoPopUpAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              this.dialogRef.close();
              break;
            case AdvertisingSearchPopUpAction.TYPE:
              this.toastr.warning(result.message);
              this.spinner.hide();
              this.dialogRef.close();
              break;
          }
        }
      }));
  }

  destroyAction() {
    this.store.dispatch({ type: FailAdvertisingConfigCallAction.TYPE });
    this.store.dispatch({ type: SuccessGetAdvertisingSearchInfoPopUpAction.TYPE });
    this.store.dispatch({ type: SuccessAdvertisingSearchPopUpAction.TYPE });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
    this.destroyAction();
  }
}

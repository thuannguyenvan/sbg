import {
  Component,
  Inject,
} from '@angular/core';
import {
  Router,
  ActivatedRoute,
} from '@angular/router';
import {
  Title,
} from '@angular/platform-browser';
import {
  Store,
} from '@ngrx/store';
import {
  Subscription,
} from 'rxjs';
import {
  AppState,
} from '../../../../../AppState';
import {
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';

import {
  GetEditAdvertisingConfigInfoAction,
  CreateAdvertisingConfigAction,
  EditAdvertisingConfigAction,
  SuccessCreateAdvertisingConfigAction,
  SuccessGetEditAdvertisingConfigInfoAction,
  FailAdvertisingConfigCallAction,
  SuccessEditAdvertisingConfigAction,
} from '../../../actions';
import {
  SpinnerHelper,
  ToastrHelper,
  ValidateHelper,
  PermissionActionHelper,
} from '../../../../../helpers';
import {
  InfoResult,
  ErrorResult,
  AdvertisingConfigModel,
  RollsModel,
  MidRollsModel,
  ConfigModel,
} from '../../../../../models';
import {
  IBaseComponent,
  ValidateBaseComponent,
} from '../../../../../bases';
import {
  TranslateService,
} from '../../../../../services';
import {
  TranslateKeyConstants,
  RouterConstants,
  LenghtValidateConstants,
  FormSizeConstants,
} from '../../../../../constants';
import {
  ActionType,
} from '../../../../../enums';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { PopupAddAdvertisingComponent } from '..';

@Component({
  selector: 'app-config-ads-create-edit',
  templateUrl: './index.html',
  styleUrls: ['./index.scss'],
})
export class CreateEditManagerAdvertisingsConfigComponent extends ValidateBaseComponent implements IBaseComponent {
  public subscriptions: Subscription[] = [];
  public translateKey: any;
  public modelInfo: any = {};
  public model: AdvertisingConfigModel = new AdvertisingConfigModel();
  public permissionAction: any = {
    CREATE_ADVERTISING_CONFIG: false,
    UPDATE_ADVERTISING_CONFIG: false,
  };
  public show: any = {
    Create: false,
    showBtnUpdate: false,
  };
  public id: string;
  public listPreRolls: Array<RollsModel> = new Array<RollsModel>();
  public listMidRolls: Array<MidRollsModel> = new Array<MidRollsModel>();
  public listPostRolls: Array<RollsModel> = new Array<RollsModel>();
  public dataSourcePreRolls: MatTableDataSource<any>;
  public dataSourcePostRolls: MatTableDataSource<any>;
  public dataSourceMidRolls: MatTableDataSource<any>;

  constructor(@Inject(FormBuilder) fb: FormBuilder,
    private validateHelper: ValidateHelper,
    private store: Store<AppState>,
    public dialog: MatDialog,
    private spinner: SpinnerHelper,
    private router: Router,
    private permissionActionHelper: PermissionActionHelper,
    private route: ActivatedRoute,
    private toastr: ToastrHelper,
    private title: Title,
    private translate: TranslateService,
  ) {
    super(validateHelper);
    this.initValidate(fb);
    this.translateKey = TranslateKeyConstants;
    this.resetValidate();
  }

  ngOnInit() {
    this.onSubscribeError();
    this.onSubscribeSuccess();
    this.onCheckPermissionAction();
    this.model = new AdvertisingConfigModel();
    if (this.router.url.toLowerCase().indexOf(RouterConstants.MANAGER_CONFIG_ADVERTISING.CREATE_ADVERTISING_CONFIG.toLowerCase()) > -1) {
      this.show.Create = true;
      this.title.setTitle(this.translate.translate(this.translateKey.ADVERTISING_CONFIG_MANAGER.PAGE_CREATE_TITLE));
    } else {
      /* Form Edit */
      this.spinner.show();
      this.show.Create = false;
      this.id = this.route.snapshot.params['id'];
      if (this.id !== undefined && this.id !== null && this.id !== '') {
        this.store.dispatch(new GetEditAdvertisingConfigInfoAction(this.id));
      }
      this.title.setTitle(this.translate.translate(this.translateKey.ADVERTISING_CONFIG_MANAGER.PAGE_UPDATE_TITLE));
    }
  }

  initValidate(fb: FormBuilder) {
    this.validators = {
      title: [Validators.required, Validators.maxLength(LenghtValidateConstants.STRING_MEDIUM)],
      skipOffset: [],
      description: [Validators.maxLength(LenghtValidateConstants.STRING_MAX)],
    };

    this.formGroup = fb.group({
      title: new FormControl(this.model.title, this.validators.title),
      skipOffset: new FormControl(this.validators.skipOffset),
      description: new FormControl(this.model.description, this.validators.description),
    });
  }

  onSubscribeSuccess() {
    this.subscriptions.push(this.store.select<InfoResult<boolean>>(state => state.managerAdvertisingsConfig.create)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          this.toastr.success(result.message);
          this.onBack();
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<boolean>>(state => state.managerAdvertisingsConfig.update)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          this.toastr.success(result.message);
          this.onBack();
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<AdvertisingConfigModel>>(state => state.managerAdvertisingsConfig.getEditInfo)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.data !== undefined && result.data !== null) {
            this.model = result.data;
            this.listMidRolls = this.model.config.midRolls;
            this.listPostRolls = this.model.config.postRolls;
            this.listPreRolls = this.model.config.preRolls;
          }
          this.dataSourceMidRolls = new MatTableDataSource(this.listMidRolls);
          this.dataSourcePostRolls = new MatTableDataSource(this.listPostRolls);
          this.dataSourcePreRolls = new MatTableDataSource(this.listPreRolls);
          this.spinner.hide();
        }
      }));
  }

  onCreate() {
    if (!this.invalid()) {
      this.spinner.show();
      this.model.config = new ConfigModel({
        midRolls: this.listMidRolls,
        postRolls: this.listPostRolls,
        preRolls: this.listPreRolls
      });
      const model = new AdvertisingConfigModel(this.model);
      this.store.dispatch(new CreateAdvertisingConfigAction(model));
    } else {
      this.toastr.warning(this.translate.translate(this.translateKey.SHARE.MESSAGE_INPUT_DATA_INVALID));
    }
  }

  onAddAdsPre() {
    const dialogRef = this.dialog.open(PopupAddAdvertisingComponent, {
      width: FormSizeConstants.WIDTH_MEDIUM,
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined && result != null) {
        const preRoll = new RollsModel({
          advertisingTitle: result.title,
          advertisingId: result.id,
        });
        this.listPreRolls.push(preRoll);
        this.dataSourcePreRolls = new MatTableDataSource(this.listPreRolls);
      }
    });
  }

  onAddAdsPost() {
    const dialogRef = this.dialog.open(PopupAddAdvertisingComponent, {
      width: FormSizeConstants.WIDTH_MEDIUM,
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined && result != null) {
        const postRoll = new RollsModel({
          advertisingTitle: result.title,
          advertisingId: result.id,
        });
        this.listPostRolls.push(postRoll);
        this.dataSourcePostRolls = new MatTableDataSource(this.listPostRolls);
      }
    });
  }

  onAddAdsMid() {
    const dialogRef = this.dialog.open(PopupAddAdvertisingComponent, {
      width: FormSizeConstants.WIDTH_MEDIUM,
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined && result != null) {
        const midRoll = new MidRollsModel({
          advertisingTitle: result.title,
          advertisingId: result.id,
        });
        this.listMidRolls.push(midRoll);
        this.dataSourceMidRolls = new MatTableDataSource(this.listMidRolls);
      }
    });
  }

  onEditAdsMid(object: MidRollsModel) {
    if (object.advertisingId !== undefined && object.advertisingId !== null) {
      const dialogRef = this.dialog.open(PopupAddAdvertisingComponent, {
        width: FormSizeConstants.WIDTH_MEDIUM,
        data: {
          id: object.advertisingId
        }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result !== undefined && result != null) {
          const index = this.listMidRolls.indexOf(object);
          const midRollNew = new MidRollsModel({
            advertisingTitle: result.title,
            advertisingId: result.id,
          });
          if (index !== -1) {
            this.listMidRolls[index] = midRollNew;
          }
          this.dataSourceMidRolls = new MatTableDataSource(this.listMidRolls);
        }
      });
    }
  }

  onEditAdsPost(object: RollsModel) {
    if (object.advertisingId !== undefined && object.advertisingId !== null) {
      const dialogRef = this.dialog.open(PopupAddAdvertisingComponent, {
        width: FormSizeConstants.WIDTH_MEDIUM,
        data: {
          id: object.advertisingId
        }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result !== undefined && result != null) {
          const index = this.listPostRolls.indexOf(object);
          const postRollNew = new RollsModel({
            advertisingTitle: result.title,
            advertisingId: result.id,
          });
          if (index !== -1) {
            this.listPostRolls[index] = postRollNew;
          }
          this.dataSourcePostRolls = new MatTableDataSource(this.listPostRolls);
        }
      });
    }
  }

  onEditAdsPre(object: RollsModel) {
    if (object.advertisingId !== undefined && object.advertisingId !== null) {
      const dialogRef = this.dialog.open(PopupAddAdvertisingComponent, {
        width: FormSizeConstants.WIDTH_MEDIUM,
        data: {
          id: object.advertisingId
        }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result !== undefined && result != null) {
          const index = this.listPreRolls.indexOf(object);
          const preRollNew = new RollsModel({
            advertisingTitle: result.title,
            advertisingId: result.id,
          });
          if (index !== -1) {
            this.listPreRolls[index] = preRollNew;
          }
          this.dataSourcePreRolls = new MatTableDataSource(this.listPreRolls);
        }
      });
    }
  }

  onDeletePreRoll(preRolls: RollsModel) {
    const index = this.listPreRolls.indexOf(preRolls);
    if (index > -1) {
      this.listPreRolls.splice(index, 1);
    }
    this.dataSourcePreRolls = new MatTableDataSource(this.listPreRolls);
  }

  onDeletePostRoll(postRolls: RollsModel) {
    const index = this.listPostRolls.indexOf(postRolls);
    if (index > -1) {
      this.listPostRolls.splice(index, 1);
    }
    this.dataSourcePostRolls = new MatTableDataSource(this.listPostRolls);
  }

  onDeleteMidRoll(midRolls: MidRollsModel) {
    const index = this.listMidRolls.indexOf(midRolls);
    if (index > -1) {
      this.listMidRolls.splice(index, 1);
    }
    this.dataSourceMidRolls = new MatTableDataSource(this.listMidRolls);
  }

  onSave() {
    if (!this.invalid()) {
      this.spinner.show();
      this.model.config = new ConfigModel({
        midRolls: this.listMidRolls,
        postRolls: this.listPostRolls,
        preRolls: this.listPreRolls
      });
      const model = {
        id: this.id,
        data: new AdvertisingConfigModel(this.model)
      };
      this.store.dispatch(new EditAdvertisingConfigAction(model));
    } else {
      this.toastr.warning(this.translate.translate(this.translateKey.SHARE.MESSAGE_INPUT_DATA_INVALID));
    }
  }

  onBack() {
    this.router.navigate([RouterConstants.MANAGER_CONFIG_ADVERTISING.LIST_ADVERTISING_CONFIG]);
  }

  onCheckPermissionAction() {
    this.permissionAction.CREATE_ADVERTISING_CONFIG = this.permissionActionHelper.isValid(ActionType.ADVERTISING_CONFIG_ADD);
    this.permissionAction.UPDATE_ADVERTISING_CONFIG = this.permissionActionHelper.isValid(ActionType.ADVERTISING_CONFIG_UPDATE);
  }

  onSubscribeError() {
    this.subscriptions.push(this.store.select<ErrorResult>(state => state.managerAdvertisingsConfig.error)
      .subscribe((result) => {
        if (result !== undefined) {
          switch (result.key) {
            case CreateAdvertisingConfigAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
            case EditAdvertisingConfigAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
            case GetEditAdvertisingConfigInfoAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
          }
        }
      }));
  }

  destroyAction() {
    this.store.dispatch({ type: FailAdvertisingConfigCallAction.TYPE });
    this.store.dispatch({ type: SuccessCreateAdvertisingConfigAction.TYPE });
    this.store.dispatch({ type: SuccessGetEditAdvertisingConfigInfoAction.TYPE });
    this.store.dispatch({ type: SuccessEditAdvertisingConfigAction.TYPE });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
    this.destroyAction();
  }
}

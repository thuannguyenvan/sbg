import {
  Component,
  Inject,
} from '@angular/core';
import {
  Router,
} from '@angular/router';
import {
  DOCUMENT,
} from '@angular/common';
import {
  Title,
} from '@angular/platform-browser';
import {
  Store,
} from '@ngrx/store';
import {
  Subscription,
} from 'rxjs';
import {
  MatTableDataSource,
} from '@angular/material';
import {
  AppState,
} from '../../../../../AppState';
import {
  SearchAdvertisingConfigAction,
  DeleteAdvertisingConfigAction,
  ActiveAdvertisingConfigAction,
  DeActiveAdvertisingConfigAction,
  SuccessDeleteAdvertisingConfigAction,
  SuccessActiveAdvertisingConfigAction,
  SuccessDeActiveAdvertisingConfigAction,
  SuccessSearchAdvertisingConfigAction,
  FailAdvertisingConfigCallAction,
} from '../../../actions';
import {
  SpinnerHelper,
  ToastrHelper,
  ValidateHelper,
  PermissionActionHelper,
  AlertHelper,
} from '../../../../../helpers';
import {
  InfoResult,
  ErrorResult,
  Pagination,
  AdvertisingModel,
  SearchInfoAdvertisingModel,
  AdvertisingConfigSearchModel,
} from '../../../../../models';
import {
  IBaseComponent,
  ValidateBaseComponent,
} from '../../../../../bases';
import {
  TranslateService,
} from '../../../../../services';
import {
  TranslateKeyConstants,
  LenghtValidateConstants,
  RouterConstants,
} from '../../../../../constants';
import {
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import {
  ActionType,
} from '../../../../../enums';
import {
  String,
} from 'typescript-string-operations';

@Component({
  selector: 'app-config=ads-list',
  templateUrl: './index.html',
  styleUrls: ['./index.scss'],
})
export class ManagerAdvertisingsConfigComponent extends ValidateBaseComponent implements IBaseComponent {
  public subscriptions: Subscription[] = [];
  public translateKey: any;
  public model: any = {};
  public modelSearch: any = {};
  public pagination: Pagination = new Pagination();
  public dataSource: MatTableDataSource<any>;
  public permissionAction: any = {
    ADVERTISING_CONFIG_SEARCH: false,
    ADVERTISING_CONFIG_UPDATE: false,
    ADVERTISING_CONFIG_DELETE: false,
    ADVERTISING_CONFIG_DEACTIVE: false,
    ADVERTISING_CONFIG_ACTIVE: false,
    ADVERTISING_CONFIG_ADD: false,
  };
  public sortDefault = {
    active: 'Title',
    direction: 'asc'
  };
  public modelSearchInfo: SearchInfoAdvertisingModel = new SearchInfoAdvertisingModel();

  constructor(
    @Inject(FormBuilder) fb: FormBuilder,
    private router: Router,
    @Inject(DOCUMENT) private document: any,
    private store: Store<AppState>,
    private spinner: SpinnerHelper,
    private toastr: ToastrHelper,
    public permissionActionHelper: PermissionActionHelper,
    title: Title,
    private translate: TranslateService,
    validateHelper: ValidateHelper,
    private alert: AlertHelper,
  ) {
    super(validateHelper);
    this.initValidate(fb);
    this.clearValidate();
    this.translateKey = TranslateKeyConstants;
    title.setTitle(this.translate.translate(this.translateKey.ADVERTISING_CONFIG_MANAGER.PAGE_LIST_TITLE));
  }

  ngOnInit() {
    this.onSubscribeError();
    this.onSubscribeSuccess();
    this.spinner.show();
    this.modelSearch.order = this.model.order = this.sortDefault.direction;
    this.modelSearch.orderBy = this.model.orderBy = this.sortDefault.active;
    this.onSubmit();
    this.model.isActive = '';
  }

  onSubscribeSuccess() {
    this.subscriptions.push(this.store.select<InfoResult<Array<AdvertisingConfigSearchModel>>>
      (state => state.managerAdvertisingsConfig.search)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.data !== undefined && result.data !== null) {
            if (result.data.length > 0) {
              this.pagination.length = result.data[0].total;
            } else {
              this.pagination.length = 0;
            }
            this.dataSource = new MatTableDataSource(result.data);
            if (this.model.isActive === null || this.model.isActive === undefined) {
              this.model.isActive = '';
            }
          }
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<any>>
      (state => state.managerAdvertisingsConfig.delete)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.message !== undefined && result.message !== null) {
            this.toastr.success(result.message);
            this.onCheckPermissionAction();
            this.onSearch();
          }
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<any>>
      (state => state.managerAdvertisingsConfig.activate)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.message !== undefined && result.message !== null) {
            this.toastr.success(result.message);
            this.onCheckPermissionAction();
            this.onSearch();
          }
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<any>>
      (state => state.managerAdvertisingsConfig.deactivate)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.message !== undefined && result.message !== null) {
            this.toastr.success(result.message);
            this.onCheckPermissionAction();
            this.onSearch();
          }
          this.spinner.hide();
        }
      }));
  }

  initValidate(fb: FormBuilder) {
    this.validators = {
      title: [Validators.maxLength(LenghtValidateConstants.STRING_MEDIUM)],
    };

    this.formGroup = fb.group({
      title: new FormControl(this.model.title, this.validators.title),
    });
  }

  onCheckPermissionAction() {
    this.permissionAction.ADVERTISING_CONFIG_ADD =
      this.permissionActionHelper.isValid(ActionType.ADVERTISING_CONFIG_ADD);
    this.permissionAction.ADVERTISING_CONFIG_UPDATE =
      this.permissionActionHelper.isValid(ActionType.ADVERTISING_CONFIG_UPDATE);
    this.permissionAction.ADVERTISING_CONFIG_DELETE =
      this.permissionActionHelper.isValid(ActionType.ADVERTISING_CONFIG_DELETE);
    this.permissionAction.ADVERTISING_CONFIG_ACTIVE =
      this.permissionActionHelper.isValid(ActionType.ADVERTISING_CONFIG_ACTIVE);
    this.permissionAction.ADVERTISING_CONFIG_DEACTIVE =
      this.permissionActionHelper.isValid(ActionType.ADVERTISING_CONFIG_DEACTIVE);
    this.permissionAction.ADVERTISING_CONFIG_SEARCH =
      this.permissionActionHelper.isValid(ActionType.ADVERTISING_CONFIG_SEARCH);
  }

  onShowActtionItem(item: AdvertisingModel, key: string) {
    let checkShow = false;
    switch (key) {
      case 'updateAdvertisingConfig':
        checkShow = item.actions.findIndex(x => x === ActionType.ADVERTISING_CONFIG_UPDATE) > -1 &&
          this.permissionAction.ADVERTISING_CONFIG_UPDATE;
        break;
      case 'deleteAdvertisingConfig':
        checkShow = item.actions.findIndex(x => x === ActionType.ADVERTISING_CONFIG_DELETE) > -1 &&
          this.permissionAction.ADVERTISING_CONFIG_DELETE;
        break;
      case 'activeAdvertisingConfig':
        checkShow = item.actions.findIndex(x => x === ActionType.ADVERTISING_CONFIG_ACTIVE) > -1 &&
          this.permissionAction.ADVERTISING_CONFIG_ACTIVE;
        break;
      case 'deActiveAdvertisingConfig':
        checkShow = item.actions.findIndex(x => x === ActionType.ADVERTISING_CONFIG_DEACTIVE) > -1 &&
          this.permissionAction.ADVERTISING_CONFIG_DEACTIVE;
        break;
      case 'addAdvertisingConfig':
        checkShow = item.actions.findIndex(x => x === ActionType.ADVERTISING_CONFIG_ADD) > -1 &&
          this.permissionAction.ADVERTISING_CONFIG_ADD;
        break;
    }
    return checkShow;
  }

  sortData(sort: any) {
    this.modelSearch.orderBy = this.model.orderBy = sort.active;
    this.modelSearch.order = this.model.order = sort.direction;
    this.onSearch();
  }

  onSearch() {
    this.spinner.show();
    this.store.dispatch(new SearchAdvertisingConfigAction(this.modelSearch));
  }

  onSubmit() {
    this.modelSearch = {
      title: this.model.title,
      orderBy: this.model.orderBy,
      order: this.model.order,
      page: this.pagination.pageIndex,
      size: this.pagination.pageSize,
      isActive: this.model.isActive === '' ? undefined : this.model.isActive
    };
    this.onSearch();
  }

  onEdit(item: AdvertisingConfigSearchModel) {
    this.router.navigate([RouterConstants.MANAGER_CONFIG_ADVERTISING.EDIT_ADVERTISING_CONFIG + '/' + item.id]);
  }

  onCreate() {
    this.router.navigate([RouterConstants.MANAGER_CONFIG_ADVERTISING.CREATE_ADVERTISING_CONFIG]);
  }

  onPageChanged(page: any) {
    this.modelSearch.page = this.pagination.pageIndex = page.pageIndex;
    this.modelSearch.size = this.pagination.pageSize = page.pageSize;
    this.onSearch();
  }

  onDelete(item: AdvertisingConfigSearchModel) {
    const msg = String.Format(this.translate.translate(
      this.translateKey.ADVERTISING_CONFIG_MANAGER.MESSAGE_CONFIRM_DELETE), item.title);
    this.alert.confirmWarning(msg, (r: any) => {
      if (r.value) {
        this.spinner.show();
        this.store.dispatch(new DeleteAdvertisingConfigAction(item.id));
      }
    });
  }

  onActive(item: AdvertisingConfigSearchModel) {
    const msg = String.Format(this.translate.translate(
      this.translateKey.ADVERTISING_CONFIG_MANAGER.MESSAGE_CONFIRM_ACTIVE), item.title);
    this.alert.confirmWarning(msg, (r: any) => {
      if (r.value) {
        this.spinner.show();
        this.store.dispatch(new ActiveAdvertisingConfigAction(item.id));
      }
    });
  }

  onDeActive(item: AdvertisingConfigSearchModel) {
    const msg = String.Format(this.translate.translate(
      this.translateKey.ADVERTISING_CONFIG_MANAGER.MESSAGE_CONFIRM_DEACTIVE), item.title);
    this.alert.confirmWarning(msg, (r: any) => {
      if (r.value) {
        this.spinner.show();
        this.store.dispatch(new DeActiveAdvertisingConfigAction(item.id));
      }
    });
  }

  onSubscribeError() {
    this.subscriptions.push(this.store.select<ErrorResult>(state => state.managerAdvertisingsConfig.error)
      .subscribe((result) => {
        if (result !== undefined) {
          switch (result.key) {
            case SearchAdvertisingConfigAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
            case DeActiveAdvertisingConfigAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
            case ActiveAdvertisingConfigAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
            case DeleteAdvertisingConfigAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
          }
        }
      }));
  }

  destroyAction() {
    this.store.dispatch({ type: FailAdvertisingConfigCallAction.TYPE });
    this.store.dispatch({ type: SuccessSearchAdvertisingConfigAction.TYPE });
    this.store.dispatch({ type: SuccessDeActiveAdvertisingConfigAction.TYPE });
    this.store.dispatch({ type: SuccessActiveAdvertisingConfigAction.TYPE });
    this.store.dispatch({ type: SuccessDeleteAdvertisingConfigAction.TYPE });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
    this.destroyAction();
  }
}

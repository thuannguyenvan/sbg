import {
  Component,
  Inject,
} from '@angular/core';
import {
  Router,
} from '@angular/router';
import {
  DOCUMENT,
} from '@angular/common';
import {
  Title,
} from '@angular/platform-browser';
import {
  Store,
} from '@ngrx/store';
import {
  Subscription,
} from 'rxjs';
import {
  MatTableDataSource,
} from '@angular/material';
import {
  AppState,
} from '../../../../../AppState';
import {
  GetSearchAdvertisingInfoAction,
  SearchAdvertisingAction,
  DeleteAdvertisingAction,
  ActiveAdvertisingAction,
  DeActiveAdvertisingAction,
  FailAdvertisingCallAction,
  SuccessSearchAdvertisingAction,
  SuccessGetSearchAdvertisingInfoAction,
  SuccessDeActiveAdvertisingAction,
  SuccessActiveAdvertisingAction,
  SuccessDeleteAdvertisingAction,
} from '../../../actions';
import {
  SpinnerHelper,
  ToastrHelper,
  ValidateHelper,
  PermissionActionHelper,
  AlertHelper,
} from '../../../../../helpers';
import {
  InfoResult,
  ErrorResult,
  Pagination,
  AdvertisingModel,
  SearchInfoAdvertisingModel,
  PartnerModel,
  AdvertisingFomartsModel,
} from '../../../../../models';
import {
  IBaseComponent,
  ValidateBaseComponent,
} from '../../../../../bases';
import {
  TranslateService,
} from '../../../../../services';
import {
  TranslateKeyConstants,
  LenghtValidateConstants,
  RouterConstants,
} from '../../../../../constants';
import {
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import {
  ActionType,
} from '../../../../../enums';
import {
  String,
} from 'typescript-string-operations';

@Component({
  selector: 'app-ads-list',
  templateUrl: './index.html',
  styleUrls: ['./index.scss'],
})
export class ManagerAdvertisingsComponent extends ValidateBaseComponent implements IBaseComponent {
  public subscriptions: Subscription[] = [];
  public translateKey: any;
  public model: any = {};
  public modelSearch: any = {};
  public pagination: Pagination = new Pagination();
  public dataSource: MatTableDataSource<any>;
  public permissionAction: any = {
    ADVERTISING_SEARCH: false,
    ADVERTISING_UPDATE: false,
    ADVERTISING_DELETE: false,
    ADVERTISING_DEACTIVE: false,
    ADVERTISING_ACTIVE: false,
    ADVERTISING_ADD: false,
  };
  public modelSearchInfo: SearchInfoAdvertisingModel = new SearchInfoAdvertisingModel();
  public sortDefault = {
    active: 'Title',
    direction: 'asc'
  };

  constructor(
    @Inject(FormBuilder) fb: FormBuilder,
    private router: Router,
    @Inject(DOCUMENT) private document: any,
    private store: Store<AppState>,
    private spinner: SpinnerHelper,
    private toastr: ToastrHelper,
    public permissionActionHelper: PermissionActionHelper,
    title: Title,
    private translate: TranslateService,
    private validateHelper: ValidateHelper,
    private alert: AlertHelper,
  ) {
    super(validateHelper);
    this.initValidate(fb);
    this.clearValidate();
    this.translateKey = TranslateKeyConstants;
    title.setTitle(this.translate.translate(this.translateKey.ADVERTISING_MANAGER.PAGE_LIST_TITLE));
  }

  ngOnInit() {
    this.onSubscribeError();
    this.onSubscribeSuccess();
    this.spinner.show();
    this.model.isActive = '';
    this.modelSearch.order = this.model.order = this.sortDefault.direction;
    this.modelSearch.orderBy = this.model.orderBy = this.sortDefault.active;
    this.store.dispatch(new GetSearchAdvertisingInfoAction());
  }

  onSubscribeSuccess() {
    this.subscriptions.push(this.store.select<InfoResult<Array<AdvertisingModel>>>(state => state.managerAdvertisings.search)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.data !== undefined && result.data !== null) {
            if (result.data.length > 0) {
              this.pagination.length = result.data[0].total;
            } else {
              this.pagination.length = 0;
            }
            this.dataSource = new MatTableDataSource(result.data);
            if (this.model.isActive === null || this.model.isActive === undefined) {
              this.model.isActive = '';
            }
          }
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<SearchInfoAdvertisingModel>>
      (state => state.managerAdvertisings.getSearchInfo)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.data !== undefined && result.data !== null) {
            const selectEnumDefaultPartner = new PartnerModel(
              {
                id: '',
                name: this.translate.translate(this.translateKey.ADVERTISING_MANAGER.SELECT_ALL)
              });
            this.modelSearchInfo.adsPartners = [selectEnumDefaultPartner];
            this.modelSearchInfo.adsPartners = this.modelSearchInfo.adsPartners.concat(result.data.adsPartners);
            const selectEnumDefaultType = new AdvertisingFomartsModel(
              {
                value: '',
                title: this.translate.translate(this.translateKey.ADVERTISING_MANAGER.SELECT_ALL)
              });
            this.modelSearchInfo.advertisingFormats = [selectEnumDefaultType];
            this.modelSearchInfo.advertisingFormats = this.modelSearchInfo.advertisingFormats.concat(result.data.advertisingFormats);
            this.model.adsPartnerId = '';
            this.model.type = '';
            this.model.isActive = '';
            this.onSubmit();
          }
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<any>>
      (state => state.managerAdvertisings.delete)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.message !== undefined && result.message !== null) {
            this.toastr.success(result.message);
            this.onCheckPermissionAction();
            this.onSearch();
          }
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<any>>
      (state => state.managerAdvertisings.activate)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.message !== undefined && result.message !== null) {
            this.toastr.success(result.message);
            this.onCheckPermissionAction();
            this.onSearch();
          }
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<any>>
      (state => state.managerAdvertisings.deactivate)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.message !== undefined && result.message !== null) {
            this.toastr.success(result.message);
            this.onCheckPermissionAction();
            this.onSearch();
          }
          this.spinner.hide();
        }
      }));
  }

  initValidate(fb: FormBuilder) {
    this.validators = {
      title: [Validators.maxLength(LenghtValidateConstants.STRING_MEDIUM)],
    };

    this.formGroup = fb.group({
      title: new FormControl(this.model.title, this.validators.title),
    });
  }

  onCheckPermissionAction() {
    this.permissionAction.ADVERTISING_ADD =
      this.permissionActionHelper.isValid(ActionType.ADVERTISING_ADD);
    this.permissionAction.ADVERTISING_UPDATE =
      this.permissionActionHelper.isValid(ActionType.ADVERTISING_UPDATE);
    this.permissionAction.ADVERTISING_DELETE =
      this.permissionActionHelper.isValid(ActionType.ADVERTISING_DELETE);
    this.permissionAction.ADVERTISING_ACTIVE =
      this.permissionActionHelper.isValid(ActionType.ADVERTISING_ACTIVE);
    this.permissionAction.ADVERTISING_DEACTIVE =
      this.permissionActionHelper.isValid(ActionType.ADVERTISING_DEACTIVE);
    this.permissionAction.ADVERTISING_SEARCH =
      this.permissionActionHelper.isValid(ActionType.ADVERTISING_SEARCH);
  }

  onShowActtionItem(item: AdvertisingModel, key: string) {
    let checkShow = false;
    switch (key) {
      case 'updateAdvertising':
        checkShow = item.actions.findIndex(x => x === ActionType.ADVERTISING_UPDATE) > -1 &&
          this.permissionAction.ADVERTISING_UPDATE;
        break;
      case 'deleteAdvertising':
        checkShow = item.actions.findIndex(x => x === ActionType.ADVERTISING_DELETE) > -1 &&
          this.permissionAction.ADVERTISING_DELETE;
        break;
      case 'activeAdvertising':
        checkShow = item.actions.findIndex(x => x === ActionType.ADVERTISING_ACTIVE) > -1 &&
          this.permissionAction.ADVERTISING_ACTIVE;
        break;
      case 'deActiveAdvertising':
        checkShow = item.actions.findIndex(x => x === ActionType.ADVERTISING_DEACTIVE) > -1 &&
          this.permissionAction.ADVERTISING_DEACTIVE;
        break;
      case 'addAdvertising':
        checkShow = item.actions.findIndex(x => x === ActionType.ADVERTISING_ADD) > -1 &&
          this.permissionAction.ADVERTISING_ADD;
        break;
    }
    return checkShow;
  }

  onSearch() {
    this.spinner.show();
    this.store.dispatch(new SearchAdvertisingAction(this.modelSearch));
  }

  sortData(sort: any) {
    this.modelSearch.orderBy = sort.active;
    this.modelSearch.order = sort.direction;
    this.onSearch();
  }

  onSubmit() {
    this.modelSearch = {
      adsPartnerId: this.model.adsPartnerId || undefined,
      title: this.model.title,
      page: this.pagination.pageIndex,
      size: this.pagination.pageSize,
      orderBy: this.model.orderBy,
      order: this.model.order,
      adsFormat: this.model.type || undefined,
      isActive: this.model.isActive === '' ? undefined : this.model.isActive
    };
    this.onSearch();
  }

  onEdit(item: AdvertisingModel) {
    this.router.navigate([RouterConstants.MANAGER_ADVERTISING.EDIT_ADVERTISING + '/' + item.id]);
  }

  onCreate() {
    this.router.navigate([RouterConstants.MANAGER_ADVERTISING.CREATE_ADVERTISING]);
  }

  onPageChanged(page: any) {
    this.modelSearch.page = this.pagination.pageIndex = page.pageIndex;
    this.modelSearch.size = this.pagination.pageSize = page.pageSize;
    this.onSearch();
  }

  onDelete(item: AdvertisingModel) {
    const msg = String.Format(this.translate.translate(
      this.translateKey.ADVERTISING_MANAGER.MESSAGE_CONFIRM_DELETE), item.title);
    this.alert.confirmWarning(msg, (r: any) => {
      if (r.value) {
        this.spinner.show();
        this.store.dispatch(new DeleteAdvertisingAction(item.id));
      }
    });
  }

  onActive(item: AdvertisingModel) {
    const msg = String.Format(this.translate.translate(
      this.translateKey.ADVERTISING_MANAGER.MESSAGE_CONFIRM_ACTIVE), item.title);
    this.alert.confirmWarning(msg, (r: any) => {
      if (r.value) {
        this.spinner.show();
        this.store.dispatch(new ActiveAdvertisingAction(item.id));
      }
    });
  }

  onDeActive(item: AdvertisingModel) {
    const msg = String.Format(this.translate.translate(
      this.translateKey.ADVERTISING_MANAGER.MESSAGE_CONFIRM_DEACTIVE), item.title);
    this.alert.confirmWarning(msg, (r: any) => {
      if (r.value) {
        this.spinner.show();
        this.store.dispatch(new DeActiveAdvertisingAction(item.id));
      }
    });
  }

  onSubscribeError() {
    this.subscriptions.push(this.store.select<ErrorResult>(state => state.managerAdvertisings.error)
      .subscribe((result) => {
        if (result !== undefined) {
          switch (result.key) {
            case SearchAdvertisingAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
            case GetSearchAdvertisingInfoAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
            case DeActiveAdvertisingAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
            case ActiveAdvertisingAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
            case DeleteAdvertisingAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
          }
        }
      }));
  }

  destroyAction() {
    this.store.dispatch({ type: FailAdvertisingCallAction.TYPE });
    this.store.dispatch({ type: SuccessSearchAdvertisingAction.TYPE });
    this.store.dispatch({ type: SuccessGetSearchAdvertisingInfoAction.TYPE });
    this.store.dispatch({ type: SuccessDeActiveAdvertisingAction.TYPE });
    this.store.dispatch({ type: SuccessActiveAdvertisingAction.TYPE });
    this.store.dispatch({ type: SuccessDeleteAdvertisingAction.TYPE });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
    this.destroyAction();
  }
}

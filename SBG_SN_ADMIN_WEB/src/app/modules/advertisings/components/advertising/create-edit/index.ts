import {
  Component,
  Inject,
} from '@angular/core';
import {
  Router,
  ActivatedRoute,
} from '@angular/router';
import {
  Title,
} from '@angular/platform-browser';
import {
  Store,
} from '@ngrx/store';
import {
  Subscription,
} from 'rxjs';
import {
  AppState,
} from '../../../../../AppState';
import {
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';

import {
  GetEditAdvertisingInfoAction,
  CreateAdvertisingAction,
  EditAdvertisingAction,
  GetCreateInfoAdvertisingAction,
  FailAdvertisingCallAction,
  SuccessCreateAdvertisingAction,
  SuccessEditAdvertisingAction,
  SuccessGetCreateInfoAdvertisingAction,
  SuccessGetEditAdvertisingInfoAction,
} from '../../../actions';
import {
  SpinnerHelper,
  ToastrHelper,
  ValidateHelper,
  PermissionActionHelper,
} from '../../../../../helpers';
import {
  InfoResult,
  ErrorResult,
  AdvertisingModel,
  SearchInfoAdvertisingModel,
  EditInfoAdvertisingModel,
} from '../../../../../models';
import {
  IBaseComponent,
  ValidateBaseComponent,
} from '../../../../../bases';
import {
  TranslateService,
} from '../../../../../services';
import {
  TranslateKeyConstants,
  RouterConstants,
  LenghtValidateConstants,
} from '../../../../../constants';
import {
  ActionType,
} from '../../../../../enums';

@Component({
  selector: 'app-ads-create-edit',
  templateUrl: './index.html',
  styleUrls: ['./index.scss'],
})
export class CreateEditManagerAdvertisingsComponent extends ValidateBaseComponent implements IBaseComponent {
  public subscriptions: Subscription[] = [];
  public translateKey: any;
  public modelInfo: any = {};
  public model: AdvertisingModel = new AdvertisingModel();
  public permissionAction: any = {
    CREATE_ADVERTISING: false,
    UPDATE_ADVERTISING: false,
  };
  public show: any = {
    Create: false,
    showBtnUpdate: false,
  };
  public id: string;
  public createEditInfoModel: any = {};

  constructor(@Inject(FormBuilder) fb: FormBuilder,
    private validateHelper: ValidateHelper,
    private store: Store<AppState>,
    private spinner: SpinnerHelper,
    private router: Router,
    private permissionActionHelper: PermissionActionHelper,
    private route: ActivatedRoute,
    private toastr: ToastrHelper,
    private title: Title,
    private translate: TranslateService,
  ) {
    super(validateHelper);
    this.initValidate(fb);
    this.translateKey = TranslateKeyConstants;
    this.resetValidate();
  }

  ngOnInit() {
    this.onSubscribeError();
    this.onSubscribeSuccess();
    this.model = new AdvertisingModel();
    if (this.router.url.toLowerCase().indexOf(RouterConstants.MANAGER_ADVERTISING.CREATE_ADVERTISING.toLowerCase()) > -1) {
      this.show.Create = true;
      const adsPartnerId = this.route.snapshot.params['partnerId'];
      if (adsPartnerId !== undefined && adsPartnerId !== null && adsPartnerId !== '') {
        this.model.adsPartnerId = adsPartnerId;
      }
      this.title.setTitle(this.translate.translate(this.translateKey.ADVERTISING_MANAGER.PAGE_CREATE_TITLE));
      this.store.dispatch(new GetCreateInfoAdvertisingAction());
    } else {
      /* Form Edit */
      this.spinner.show();
      this.show.Create = false;
      this.id = this.route.snapshot.params['id'];
      if (this.id !== undefined && this.id !== null && this.id !== '') {
        this.store.dispatch(new GetEditAdvertisingInfoAction(this.id));
      }
      this.title.setTitle(this.translate.translate(this.translateKey.ADVERTISING_MANAGER.PAGE_UPDATE_TITLE));
    }
  }

  initValidate(fb: FormBuilder) {
    this.validators = {
      title: [Validators.required, Validators.maxLength(LenghtValidateConstants.STRING_MEDIUM)],
      partner: [Validators.required],
      clickThrough: [this.validateHelper.invalidUrl, Validators.maxLength(LenghtValidateConstants.STRING_MEDIUM)],
      clickTracking: [this.validateHelper.invalidUrl, Validators.maxLength(LenghtValidateConstants.STRING_MEDIUM)],
      content: [Validators.required, Validators.maxLength(LenghtValidateConstants.STRING_MEDIUM)],
      adsFormat: [Validators.required],
      description: [Validators.maxLength(LenghtValidateConstants.STRING_MAX)],
    };

    this.formGroup = fb.group({
      title: new FormControl(this.model.title, this.validators.title),
      adsPartnerId: new FormControl(this.model.adsPartnerId, this.validators.partner),
      clickThrough: new FormControl(this.model.clickThrough, this.validators.clickThrough),
      clickTracking: new FormControl(this.model.clickTracking, this.validators.clickTracking),
      content: new FormControl(this.model.content, this.validators.content),
      adsFormat: new FormControl(this.model.adsFormat, this.validators.adsFormat),
      description: new FormControl(this.model.description, this.validators.description),
    });
  }

  onSubscribeSuccess() {
    this.subscriptions.push(this.store.select<InfoResult<boolean>>(state => state.managerAdvertisings.create)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          this.toastr.success(result.message);
          this.onBack();
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<boolean>>(state => state.managerAdvertisings.update)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          this.toastr.success(result.message);
          this.onBack();
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<EditInfoAdvertisingModel>>(state => state.managerAdvertisings.getEditInfo)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.data !== undefined && result.data !== null) {
            this.model = result.data.advertising;
            this.createEditInfoModel.adsPartners = result.data.adsPartners;
            this.createEditInfoModel.advertisingFormats = result.data.advertisingFormats;
          }
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<SearchInfoAdvertisingModel>>
      (state => state.managerAdvertisings.getCreateInfo)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.data !== undefined && result.data !== null) {
            this.createEditInfoModel.adsPartners = result.data.adsPartners;
            this.createEditInfoModel.advertisingFormats = result.data.advertisingFormats;
          }
          this.spinner.hide();
        }
      }));
  }

  onCreate() {
    if (!this.invalid()) {
      this.spinner.show();
      this.store.dispatch(new CreateAdvertisingAction(this.model));
    } else {
      this.toastr.warning(this.translate.translate(this.translateKey.SHARE.MESSAGE_INPUT_DATA_INVALID));
    }
  }

  onSave() {
    if (!this.invalid()) {
      this.spinner.show();
      const model = {
        id: this.id,
        data: this.model
      };
      this.store.dispatch(new EditAdvertisingAction(model));
    } else {
      this.toastr.warning(this.translate.translate(this.translateKey.SHARE.MESSAGE_INPUT_DATA_INVALID));
    }
  }

  onBack() {
    this.router.navigate([RouterConstants.MANAGER_ADVERTISING.LIST_ADVERTISING]);
  }

  onCheckPermissionAction() {
    this.permissionAction.CREATE_ADVERTISING = this.permissionActionHelper.isValid(ActionType.ADVERTISING_ADD);
    this.permissionAction.UPDATE_ADVERTISING = this.permissionActionHelper.isValid(ActionType.ADVERTISING_UPDATE);
  }

  onSubscribeError() {
    this.subscriptions.push(this.store.select<ErrorResult>(state => state.managerAdvertisings.error)
      .subscribe((result) => {
        if (result !== undefined) {
          switch (result.key) {
            case CreateAdvertisingAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
            case EditAdvertisingAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
            case GetEditAdvertisingInfoAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
            case GetCreateInfoAdvertisingAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
          }
        }
      }));
  }

  destroyAction() {
    this.store.dispatch({ type: FailAdvertisingCallAction.TYPE });
    this.store.dispatch({ type: SuccessCreateAdvertisingAction.TYPE });
    this.store.dispatch({ type: SuccessGetCreateInfoAdvertisingAction.TYPE });
    this.store.dispatch({ type: SuccessEditAdvertisingAction.TYPE });
    this.store.dispatch({ type: SuccessGetEditAdvertisingInfoAction.TYPE });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
    this.destroyAction();
  }
}

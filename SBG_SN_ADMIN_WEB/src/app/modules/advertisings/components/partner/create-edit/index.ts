import {
  Component,
  Inject,
} from '@angular/core';
import {
  Router,
  ActivatedRoute,
} from '@angular/router';
import {
  Title,
} from '@angular/platform-browser';
import {
  Store,
} from '@ngrx/store';
import {
  Subscription,
} from 'rxjs';
import {
  AppState,
} from '../../../../../AppState';
import {
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';

import {
  GetEditPartnerInfoAction,
  CreatePartnerAction,
  EditPartnerAction,
  FailPartnerCallAction,
  SuccessGetEditPartnerInfoAction,
  SuccessEditPartnerAction,
  SuccessCreatePartnerAction,
} from '../../../actions';
import {
  SpinnerHelper,
  ToastrHelper,
  ValidateHelper,
  PermissionActionHelper,
} from '../../../../../helpers';
import {
  InfoResult,
  ErrorResult,
  PartnerModel,
} from '../../../../../models';
import {
  IBaseComponent,
  ValidateBaseComponent,
} from '../../../../../bases';
import {
  TranslateService,
} from '../../../../../services';
import {
  TranslateKeyConstants,
  RouterConstants,
  LenghtValidateConstants,
} from '../../../../../constants';
import {
  ActionType,
} from '../../../../../enums';

@Component({
  selector: 'app-partner-create-edit',
  templateUrl: './index.html',
  styleUrls: ['./index.scss'],
})
export class CreateEditPartnerComponent extends ValidateBaseComponent implements IBaseComponent {
  public subscriptions: Subscription[] = [];
  public translateKey: any;
  public modelInfo: any = {};
  public model: PartnerModel = new PartnerModel();
  public permissionAction: any = {
    CREATE_PARTNER: false,
    UPDATE_PARTNER: false,
  };
  public show: any = {
    Create: false,
    showBtnUpdate: false,
  };
  public id: string;

  constructor(@Inject(FormBuilder) fb: FormBuilder,
    private validateHelper: ValidateHelper,
    private store: Store<AppState>,
    private spinner: SpinnerHelper,
    private router: Router,
    private permissionActionHelper: PermissionActionHelper,
    private route: ActivatedRoute,
    private toastr: ToastrHelper,
    private title: Title,
    private translate: TranslateService,
  ) {
    super(validateHelper);
    this.initValidate(fb);
    this.translateKey = TranslateKeyConstants;
    this.resetValidate();
  }

  ngOnInit() {
    this.onSubscribeError();
    this.onSubscribeSuccess();
    this.onCheckPermissionAction();
    this.show.createLink = true;
    if (this.router.url.toLowerCase().indexOf(RouterConstants.MANAGER_PARTNER.CREATE_PARTNER.toLowerCase()) > -1) {
      this.show.Create = true;
      this.title.setTitle(this.translate.translate(this.translateKey.PARTNER_MANAGER.PAGE_CREATE_TITLE));
    } else {
      /* Form Edit */
      this.spinner.show();
      this.show.Create = false;
      this.id = this.route.snapshot.params['id'];
      if (this.id !== undefined && this.id !== null && this.id !== '') {
        this.store.dispatch(new GetEditPartnerInfoAction(this.id));
      }
      this.title.setTitle(this.translate.translate(this.translateKey.PARTNER_MANAGER.PAGE_UPDATE_TITLE));
    }
  }

  initValidate(fb: FormBuilder) {
    this.validators = {
      name: [Validators.required, Validators.maxLength(LenghtValidateConstants.STRING_MEDIUM)],
      description: [Validators.maxLength(LenghtValidateConstants.STRING_MAX)],
      website: [Validators.maxLength(LenghtValidateConstants.STRING_MEDIUM), this.validateHelper.invalidUrl],
      logo: [Validators.maxLength(LenghtValidateConstants.STRING_MEDIUM)],
    };

    this.formGroup = fb.group({
      name: new FormControl(this.model.name, this.validators.name),
      description: new FormControl(this.model.description, this.validators.description),
      website: new FormControl(this.model.website, this.validators.website),
      logo: new FormControl(this.model.logo, this.validators.logo),
    });
  }

  onSubscribeSuccess() {
    this.subscriptions.push(this.store.select<InfoResult<boolean>>(state => state.managerPartner.create)
      .subscribe((result) => {
        if (result !== undefined) {
          this.toastr.success(result.message);
          this.onBack();
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<boolean>>(state => state.managerPartner.update)
      .subscribe((result) => {
        if (result !== undefined) {
          this.toastr.success(result.message);
          this.onBack();
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<PartnerModel>>(state => state.managerPartner.getEditInfo)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.data !== undefined && result.data !== null) {
            this.model = result.data;
          }
          this.spinner.hide();
        }
      }));
  }

  onCreate() {
    if (!this.invalid()) {
      this.spinner.show();
      this.store.dispatch(new CreatePartnerAction(this.model));
    } else {
      this.toastr.warning(this.translate.translate(this.translateKey.SHARE.MESSAGE_INPUT_DATA_INVALID));
    }
  }

  onSave() {
    if (!this.invalid()) {
      this.spinner.show();
      const model = {
        id: this.id,
        data: this.model
      };
      this.store.dispatch(new EditPartnerAction(model));
    } else {
      this.toastr.warning(this.translate.translate(this.translateKey.SHARE.MESSAGE_INPUT_DATA_INVALID));
    }
  }

  onBack() {
    this.router.navigate([RouterConstants.MANAGER_PARTNER.LIST_PARTNER]);
  }

  onCheckPermissionAction() {
    this.permissionAction.CREATE_PARTNER = this.permissionActionHelper.isValid(ActionType.PARTNER_ADD);
    this.permissionAction.UPDATE_PARTNER = this.permissionActionHelper.isValid(ActionType.PARTNER_UPDATE);
  }

  onSubscribeError() {
    this.subscriptions.push(this.store.select<ErrorResult>(state => state.managerPartner.error)
      .subscribe((result) => {
        if (result !== undefined) {
          switch (result.key) {
            case CreatePartnerAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
            case EditPartnerAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
            case GetEditPartnerInfoAction.TYPE:
              this.spinner.hide();
              this.toastr.showError(result);
              this.error = this.validateHelper.validateByServer(this.formGroup, result);
              break;
          }
        }
      }));
  }

  destroyAction() {
    this.store.dispatch({ type: FailPartnerCallAction.TYPE });
    this.store.dispatch({ type: SuccessCreatePartnerAction.TYPE });
    this.store.dispatch({ type: SuccessEditPartnerAction.TYPE });
    this.store.dispatch({ type: SuccessGetEditPartnerInfoAction.TYPE });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
    this.destroyAction();
  }
}

import {
  Component,
  Inject,
} from '@angular/core';
import {
  Router,
} from '@angular/router';
import {
  DOCUMENT,
} from '@angular/common';
import {
  Title,
} from '@angular/platform-browser';
import {
  Store,
} from '@ngrx/store';
import {
  Subscription,
} from 'rxjs';
import {
  MatTableDataSource,
} from '@angular/material';
import {
  AppState,
} from '../../../../../AppState';
import {
  DeletePartnerAction,
  ActivePartnerAction,
  DeActivePartnerAction,
  SearchPartnerAction,
  FailPartnerCallAction,
  SuccessDeletePartnerAction,
  SuccessActivePartnerAction,
  SuccessDeActivePartnerAction,
  SuccessSearchPartnerAction,
} from '../../../actions';
import {
  SpinnerHelper,
  ToastrHelper,
  ValidateHelper,
  PermissionActionHelper,
  AlertHelper,
} from '../../../../../helpers';
import {
  InfoResult,
  ErrorResult,
  Pagination,
  PartnerModel,
} from '../../../../../models';
import {
  IBaseComponent,
  ValidateBaseComponent,
} from '../../../../../bases';
import {
  TranslateService,
} from '../../../../../services';
import {
  TranslateKeyConstants,
  LenghtValidateConstants,
  RouterConstants,
} from '../../../../../constants';
import {
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import {
  ActionType,
} from '../../../../../enums';
import {
  String,
} from 'typescript-string-operations';

@Component({
  selector: 'app-partner-list',
  templateUrl: './index.html',
  styleUrls: ['./index.scss'],
})
export class ManagerPartnerComponent extends ValidateBaseComponent implements IBaseComponent {
  public subscriptions: Subscription[] = [];
  public translateKey: any;
  public model: any = {};
  public modelSearch: any = {};
  public pagination: Pagination = new Pagination();
  public dataSource: MatTableDataSource<any>;
  public permissionAction: any = {
    PARTNER_ADD: false,
    PARTNER_SEARCH: false,
    PARTNER_UPDATE: false,
    PARTNER_DELETE: false,
    PARTNER_DEACTIVE: false,
    PARTNER_ACTIVE: false,
    ADVERTISING_ADD: false,
  };
  public sortDefault = {
    active: 'Name',
    direction: 'asc'
  };

  constructor(
    @Inject(FormBuilder) fb: FormBuilder,
    private router: Router,
    @Inject(DOCUMENT) private document: any,
    private store: Store<AppState>,
    private spinner: SpinnerHelper,
    private toastr: ToastrHelper,
    public permissionActionHelper: PermissionActionHelper,
    title: Title,
    private translate: TranslateService,
    private validateHelper: ValidateHelper,
    private alert: AlertHelper,
  ) {
    super(validateHelper);
    this.initValidate(fb);
    this.clearValidate();
    this.translateKey = TranslateKeyConstants;
    title.setTitle(this.translate.translate(this.translateKey.PARTNER_MANAGER.PAGE_LIST_TITLE));
  }

  ngOnInit() {
    this.onSubscribeError();
    this.onSubscribeSuccess();
    this.spinner.show();
    this.model.isActive = '';
    this.modelSearch.order = this.model.order = this.sortDefault.direction;
    this.modelSearch.orderBy =  this.model.orderBy = this.sortDefault.active;
    this.onSubmit();
  }

  onSubscribeSuccess() {
    this.subscriptions.push(this.store.select<InfoResult<Array<PartnerModel>>>(state => state.managerPartner.Search)
      .subscribe((result) => {
        if (result !== undefined) {
          this.onCheckPermissionAction();
          if (result.data !== undefined && result.data !== null) {
            if (result.data.length > 0) {
              this.pagination.length = result.data[0].total;
            } else {
              this.pagination.length = 0;
            }
            this.dataSource = new MatTableDataSource(result.data);
          }
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<any>>
      (state => state.managerPartner.delete)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.message !== undefined && result.message !== null) {
            this.toastr.success(result.message);
            this.onCheckPermissionAction();
            this.onSearch();
          }
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<any>>
      (state => state.managerPartner.active)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.message !== undefined && result.message !== null) {
            this.toastr.success(result.message);
            this.onCheckPermissionAction();
            this.onSearch();
          }
          this.spinner.hide();
        }
      }));

    this.subscriptions.push(this.store.select<InfoResult<any>>
      (state => state.managerPartner.deActive)
      .subscribe((result) => {
        if (result !== undefined) {
          if (result.message !== undefined && result.message !== null) {
            this.toastr.success(result.message);
            this.onCheckPermissionAction();
            this.onSearch();
          }
          this.spinner.hide();
        }
      }));
  }

  initValidate(fb: FormBuilder) {
    this.validators = {
      namePartner: [Validators.maxLength(LenghtValidateConstants.STRING_MEDIUM)],
    };

    this.formGroup = fb.group({
      name: new FormControl(this.model.name, this.validators.namePartner),
    });
  }

  sortData(sort: any) {
    this.modelSearch.orderBy = sort.active;
    this.modelSearch.order = sort.direction;
    this.pagination.pageIndex = 1;
    this.onSearch();
  }

  onCheckPermissionAction() {
    this.permissionAction.PARTNER_ADD =
      this.permissionActionHelper.isValid(ActionType.PARTNER_ADD);
    this.permissionAction.PARTNER_UPDATE =
      this.permissionActionHelper.isValid(ActionType.PARTNER_UPDATE);
    this.permissionAction.PARTNER_DELETE =
      this.permissionActionHelper.isValid(ActionType.PARTNER_DELETE);
    this.permissionAction.PARTNER_ACTIVE =
      this.permissionActionHelper.isValid(ActionType.PARTNER_ACTIVE);
    this.permissionAction.PARTNER_DEACTIVE =
      this.permissionActionHelper.isValid(ActionType.PARTNER_DEACTIVE);
    this.permissionAction.PARTNER_SEARCH =
      this.permissionActionHelper.isValid(ActionType.PARTNER_SEARCH);
    this.permissionAction.ADVERTISING_ADD =
      this.permissionActionHelper.isValid(ActionType.ADVERTISING_ADD);
  }

  onShowActtionItem(item: PartnerModel, key: string) {
    let checkShow = false;
    switch (key) {
      case 'updatePartner':
        checkShow = item.actions.findIndex(x => x === ActionType.PARTNER_UPDATE) > -1 &&
          this.permissionAction.PARTNER_UPDATE;
        break;
      case 'deletePartner':
        checkShow = item.actions.findIndex(x => x === ActionType.PARTNER_DELETE) > -1 &&
          this.permissionAction.PARTNER_DELETE;
        break;
      case 'activePartner':
        checkShow = item.actions.findIndex(x => x === ActionType.PARTNER_ACTIVE) > -1 &&
          this.permissionAction.PARTNER_ACTIVE;
        break;
      case 'deActivePartner':
        checkShow = item.actions.findIndex(x => x === ActionType.PARTNER_DEACTIVE) > -1 &&
          this.permissionAction.PARTNER_DEACTIVE;
        break;
      case 'addAdvertising':
        checkShow = item.actions.findIndex(x => x === ActionType.ADVERTISING_ADD) > -1 &&
          this.permissionAction.ADVERTISING_ADD;
        break;
    }
    return checkShow;
  }

  onaddAdvertising(id: any) {
    this.router.navigate([RouterConstants.MANAGER_ADVERTISING.CREATE_ADVERTISING, { partnerId: id }]);
  }

  onSearch() {
    this.spinner.show();
    this.store.dispatch(new SearchPartnerAction(this.modelSearch));
  }

  onSubmit() {
    this.pagination.pageIndex = 1;
    this.modelSearch = {
      name: this.model.name,
      orderBy: this.model.orderBy,
      order: this.model.order,
      page: this.pagination.pageIndex,
      size: this.pagination.pageSize,
      isActive: this.model.isActive === '' || this.model.isActive === null ? undefined : this.model.isActive
    };
    this.onSearch();
  }

  onEdit(item: PartnerModel) {
    this.router.navigate([RouterConstants.MANAGER_PARTNER.EDIT_PARTNER + '/' + item.id]);
  }

  onCreate() {
    this.router.navigate([RouterConstants.MANAGER_PARTNER.CREATE_PARTNER]);
  }

  onPageChanged(page: any) {
    this.modelSearch.page = this.pagination.pageIndex = page.pageIndex;
    this.modelSearch.size = this.pagination.pageSize = page.pageSize;

    this.onSearch();
  }

  onDelete(item: PartnerModel) {
    const msg = String.Format(this.translate.translate(
      this.translateKey.PARTNER_MANAGER.MESSAGE_CONFIRM_DELETE), item.name);
    this.alert.confirmWarning(msg, (r: any) => {
      if (r.value) {
        this.spinner.show();
        this.store.dispatch(new DeletePartnerAction(item.id));
      }
    });
  }

  onActive(item: PartnerModel) {
    const msg = String.Format(this.translate.translate(
      this.translateKey.PARTNER_MANAGER.MESSAGE_CONFIRM_ACTIVE), item.name);
    this.alert.confirmWarning(msg, (r: any) => {
      if (r.value) {
        this.spinner.show();
        this.store.dispatch(new ActivePartnerAction(item.id));
      }
    });
  }

  onDeActive(item: PartnerModel) {
    const msg = String.Format(this.translate.translate(
      this.translateKey.PARTNER_MANAGER.MESSAGE_CONFIRM_DEACTIVE), item.name);
    this.alert.confirmWarning(msg, (r: any) => {
      if (r.value) {
        this.spinner.show();
        this.store.dispatch(new DeActivePartnerAction(item.id));
      }
    });
  }

  onSubscribeError() {
    this.subscriptions.push(this.store.select<ErrorResult>(state => state.managerPartner.error)
      .subscribe((result) => {
        if (result !== undefined) {
          switch (result.key) {
            case SearchPartnerAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
            case DeActivePartnerAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
            case ActivePartnerAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
            case DeletePartnerAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
          }
        }
      }));
  }

  destroyAction() {
    this.store.dispatch({ type: FailPartnerCallAction.TYPE });
    this.store.dispatch({ type: SuccessSearchPartnerAction.TYPE });
    this.store.dispatch({ type: SuccessDeActivePartnerAction.TYPE });
    this.store.dispatch({ type: SuccessActivePartnerAction.TYPE });
    this.store.dispatch({ type: SuccessDeletePartnerAction.TYPE });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
    this.destroyAction();
  }
}

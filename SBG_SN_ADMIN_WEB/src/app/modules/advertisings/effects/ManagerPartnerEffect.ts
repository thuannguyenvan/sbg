import {
    Injectable,
} from '@angular/core';
import {
    Actions,
    Effect,
    ofType,
} from '@ngrx/effects';

import {
    Observable,
    of,
} from 'rxjs';
import { Action } from '@ngrx/store';
import {
    switchMap,
    map,
    catchError,
} from 'rxjs/operators';

import {
    FailPartnerCallAction,
    CreatePartnerAction,
    SuccessCreatePartnerAction,
    DeletePartnerAction,
    SuccessDeletePartnerAction,
    EditPartnerAction,
    SuccessEditPartnerAction,
    SearchPartnerAction,
    SuccessSearchPartnerAction,
    ActivePartnerAction,
    SuccessActivePartnerAction,
    DeActivePartnerAction,
    SuccessDeActivePartnerAction,
    GetEditPartnerInfoAction,
    SuccessGetEditPartnerInfoAction,
} from '../actions';
import {
    ManagerPartnerService
} from '../../../services';

@Injectable()
export class ManagerPartnerEffect {
    constructor(private actions$: Actions,
        private managerPartnerService: ManagerPartnerService) { }

    @Effect() createPartner$: Observable<Action> = this.actions$.pipe(
        ofType<CreatePartnerAction>(CreatePartnerAction.TYPE),
        switchMap(action => {
            return this.managerPartnerService.createPartner(action.payload).pipe(
                map((data) => new SuccessCreatePartnerAction(data)),
                catchError((error) => of(new FailPartnerCallAction(
                    {
                        key: CreatePartnerAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() deletePartner$: Observable<Action> = this.actions$.pipe(
        ofType<DeletePartnerAction>(DeletePartnerAction.TYPE),
        switchMap(action => {
            return this.managerPartnerService.deletePartner(action.payload).pipe(
                map((data) => new SuccessDeletePartnerAction(data)),
                catchError((error) => of(new FailPartnerCallAction(
                    {
                        key: DeletePartnerAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() activePartner$: Observable<Action> = this.actions$.pipe(
        ofType<ActivePartnerAction>(ActivePartnerAction.TYPE),
        switchMap(action => {
            return this.managerPartnerService.activePartner(action.payload).pipe(
                map((data) => new SuccessActivePartnerAction(data)),
                catchError((error) => of(new FailPartnerCallAction(
                    {
                        key: ActivePartnerAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() deActivePartner$: Observable<Action> = this.actions$.pipe(
        ofType<DeActivePartnerAction>(DeActivePartnerAction.TYPE),
        switchMap(action => {
            return this.managerPartnerService.deActivePartner(action.payload).pipe(
                map((data) => new SuccessDeActivePartnerAction(data)),
                catchError((error) => of(new FailPartnerCallAction(
                    {
                        key: DeActivePartnerAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() updatePartner$: Observable<Action> = this.actions$.pipe(
        ofType<EditPartnerAction>(EditPartnerAction.TYPE),
        switchMap(action => {
            return this.managerPartnerService.updatePartner(action.payload).pipe(
                map((data) => new SuccessEditPartnerAction(data)),
                catchError((error) => of(new FailPartnerCallAction(
                    {
                        key: EditPartnerAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() updateInfoPartner$: Observable<Action> = this.actions$.pipe(
        ofType<GetEditPartnerInfoAction>(GetEditPartnerInfoAction.TYPE),
        switchMap(action => {
            return this.managerPartnerService.updateInfoPartner(action.payload).pipe(
                map((data) => new SuccessGetEditPartnerInfoAction(data)),
                catchError((error) => of(new FailPartnerCallAction(
                    {
                        key: GetEditPartnerInfoAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() getPartner$: Observable<Action> = this.actions$.pipe(
        ofType<SearchPartnerAction>(SearchPartnerAction.TYPE),
        switchMap(action => {
            return this.managerPartnerService.getPartner(action.payload).pipe(
                map((data) => new SuccessSearchPartnerAction(data)),
                catchError((error) => of(new FailPartnerCallAction(
                    {
                        key: SearchPartnerAction.TYPE,
                        info: error
                    }))));
        }));
}

import {
    Injectable,
} from '@angular/core';
import {
    Actions,
    Effect,
    ofType,
} from '@ngrx/effects';

import {
    Observable,
    of,
} from 'rxjs';
import {
    Action,
} from '@ngrx/store';
import {
    SuccessGetSearchAdvertisingInfoAction,
    GetSearchAdvertisingInfoAction,
    FailAdvertisingCallAction,
    SuccessSearchAdvertisingAction,
    SearchAdvertisingAction,
    GetEditAdvertisingInfoAction,
    SuccessGetEditAdvertisingInfoAction,
    EditAdvertisingAction,
    SuccessEditAdvertisingAction,
    DeleteAdvertisingAction,
    SuccessDeleteAdvertisingAction,
    DeActiveAdvertisingAction,
    SuccessDeActiveAdvertisingAction,
    CreateAdvertisingAction,
    SuccessCreateAdvertisingAction,
    ActiveAdvertisingAction,
    SuccessActiveAdvertisingAction,
    GetCreateInfoAdvertisingAction,
    SuccessGetCreateInfoAdvertisingAction
} from '../actions';
import {
    switchMap,
    map,
    catchError,
} from 'rxjs/operators';
import {
    ManagerAdvertisingService,
} from '../../../services';

@Injectable()
export class ManagerAdvertisingsEffect {
    constructor(private actions$: Actions,
        private managerAdvertisingService: ManagerAdvertisingService) { }

    @Effect() getSearchAdvertisingInfo$: Observable<Action> = this.actions$.pipe(
        ofType<GetSearchAdvertisingInfoAction>(GetSearchAdvertisingInfoAction.TYPE),
        switchMap(action => {
            return this.managerAdvertisingService.getSearchAdvertisingInfo().pipe(
                map((data) => new SuccessGetSearchAdvertisingInfoAction(data)),
                catchError((error) => of(new FailAdvertisingCallAction(
                    {
                        key: GetSearchAdvertisingInfoAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() searchAdvertisings$: Observable<Action> = this.actions$.pipe(
        ofType<SearchAdvertisingAction>(SearchAdvertisingAction.TYPE),
        switchMap(action => {
            return this.managerAdvertisingService.searchAdvertisings(action.payload).pipe(
                map((data) => new SuccessSearchAdvertisingAction(data)),
                catchError((error) => of(new FailAdvertisingCallAction(
                    {
                        key: SearchAdvertisingAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() getUpdateAdvertisingInfo$: Observable<Action> = this.actions$.pipe(
        ofType<GetEditAdvertisingInfoAction>(GetEditAdvertisingInfoAction.TYPE),
        switchMap(action => {
            return this.managerAdvertisingService.getUpdateAdvertisingInfo(action.payload).pipe(
                map((data) => new SuccessGetEditAdvertisingInfoAction(data)),
                catchError((error) => of(new FailAdvertisingCallAction(
                    {
                        key: GetEditAdvertisingInfoAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() updateAdvertising$: Observable<Action> = this.actions$.pipe(
        ofType<EditAdvertisingAction>(EditAdvertisingAction.TYPE),
        switchMap(action => {
            return this.managerAdvertisingService.updateAdvertising(action.payload).pipe(
                map((data) => new SuccessEditAdvertisingAction(data)),
                catchError((error) => of(new FailAdvertisingCallAction(
                    {
                        key: EditAdvertisingAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() getCreateAdvertisingInfo$: Observable<Action> = this.actions$.pipe(
        ofType<GetCreateInfoAdvertisingAction>(GetCreateInfoAdvertisingAction.TYPE),
        switchMap(action => {
            return this.managerAdvertisingService.getCreateAdvertisingInfo().pipe(
                map((data) => new SuccessGetCreateInfoAdvertisingAction(data)),
                catchError((error) => of(new FailAdvertisingCallAction(
                    {
                        key: GetCreateInfoAdvertisingAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() createAdvertising$: Observable<Action> = this.actions$.pipe(
        ofType<CreateAdvertisingAction>(CreateAdvertisingAction.TYPE),
        switchMap(action => {
            return this.managerAdvertisingService.createAdvertising(action.payload).pipe(
                map((data) => new SuccessCreateAdvertisingAction(data)),
                catchError((error) => of(new FailAdvertisingCallAction(
                    {
                        key: CreateAdvertisingAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() deleteAdvertising$: Observable<Action> = this.actions$.pipe(
        ofType<DeleteAdvertisingAction>(DeleteAdvertisingAction.TYPE),
        switchMap(action => {
            return this.managerAdvertisingService.deleteAdvertising(action.payload).pipe(
                map((data) => new SuccessDeleteAdvertisingAction(data)),
                catchError((error) => of(new FailAdvertisingCallAction(
                    {
                        key: DeleteAdvertisingAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() deActivateAdvertising$: Observable<Action> = this.actions$.pipe(
        ofType<DeActiveAdvertisingAction>(DeActiveAdvertisingAction.TYPE),
        switchMap(action => {
            return this.managerAdvertisingService.deActivateAdvertising(action.payload).pipe(
                map((data) => new SuccessDeActiveAdvertisingAction(data)),
                catchError((error) => of(new FailAdvertisingCallAction(
                    {
                        key: DeActiveAdvertisingAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() activateAdvertising$: Observable<Action> = this.actions$.pipe(
        ofType<ActiveAdvertisingAction>(ActiveAdvertisingAction.TYPE),
        switchMap(action => {
            return this.managerAdvertisingService.activateAdvertising(action.payload).pipe(
                map((data) => new SuccessActiveAdvertisingAction(data)),
                catchError((error) => of(new FailAdvertisingCallAction(
                    {
                        key: ActiveAdvertisingAction.TYPE,
                        info: error
                    }))));
        }));
}

import {
    Injectable,
} from '@angular/core';
import {
    Actions,
    Effect,
    ofType,
} from '@ngrx/effects';

import {
    Observable,
    of,
} from 'rxjs';
import {
    Action,
} from '@ngrx/store';
import {
    FailAdvertisingConfigCallAction,
    SearchAdvertisingConfigAction,
    SuccessSearchAdvertisingConfigAction,
    GetEditAdvertisingConfigInfoAction,
    SuccessGetEditAdvertisingConfigInfoAction,
    SuccessEditAdvertisingConfigAction,
    EditAdvertisingConfigAction,
    CreateAdvertisingConfigAction,
    SuccessCreateAdvertisingConfigAction,
    DeleteAdvertisingConfigAction,
    SuccessDeleteAdvertisingConfigAction,
    DeActiveAdvertisingConfigAction,
    SuccessDeActiveAdvertisingConfigAction,
    ActiveAdvertisingConfigAction,
    SuccessActiveAdvertisingConfigAction,
    AdvertisingSearchPopUpAction,
    SuccessAdvertisingSearchPopUpAction,
    GetAdvertisingSearchInfoPopUpAction,
    SuccessGetAdvertisingSearchInfoPopUpAction,
} from '../actions';
import {
    switchMap,
    map,
    catchError,
} from 'rxjs/operators';
import {
    ManagerAdvertisingConfigService,
} from '../../../services';

@Injectable()
export class ManagerAdvertisingsConfigEffect {
    constructor(private actions$: Actions,
        private managerAdvertisingConfigService: ManagerAdvertisingConfigService) { }

    @Effect() searchAdvertisings$: Observable<Action> = this.actions$.pipe(
        ofType<SearchAdvertisingConfigAction>(SearchAdvertisingConfigAction.TYPE),
        switchMap(action => {
            return this.managerAdvertisingConfigService.searchAdvertisingsConfig(action.payload).pipe(
                map((data) => new SuccessSearchAdvertisingConfigAction(data)),
                catchError((error) => of(new FailAdvertisingConfigCallAction(
                    {
                        key: SearchAdvertisingConfigAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() getSearchAdvertisingInfoPopUp$: Observable<Action> = this.actions$.pipe(
        ofType<GetAdvertisingSearchInfoPopUpAction>(GetAdvertisingSearchInfoPopUpAction.TYPE),
        switchMap(action => {
            return this.managerAdvertisingConfigService.getSearchAdvertisingInfoPopUp().pipe(
                map((data) => new SuccessGetAdvertisingSearchInfoPopUpAction(data)),
                catchError((error) => of(new FailAdvertisingConfigCallAction(
                    {
                        key: GetAdvertisingSearchInfoPopUpAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() searchAdvertisingsPopUp$: Observable<Action> = this.actions$.pipe(
        ofType<AdvertisingSearchPopUpAction>(AdvertisingSearchPopUpAction.TYPE),
        switchMap(action => {
            return this.managerAdvertisingConfigService.searchAdvertisingsPopUp(action.payload).pipe(
                map((data) => new SuccessAdvertisingSearchPopUpAction(data)),
                catchError((error) => of(new FailAdvertisingConfigCallAction(
                    {
                        key: AdvertisingSearchPopUpAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() getUpdateAdvertisingConfigInfo$: Observable<Action> = this.actions$.pipe(
        ofType<GetEditAdvertisingConfigInfoAction>(GetEditAdvertisingConfigInfoAction.TYPE),
        switchMap(action => {
            return this.managerAdvertisingConfigService.getUpdateAdvertisingConfigInfo(action.payload).pipe(
                map((data) => new SuccessGetEditAdvertisingConfigInfoAction(data)),
                catchError((error) => of(new FailAdvertisingConfigCallAction(
                    {
                        key: GetEditAdvertisingConfigInfoAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() updateAdvertisingConfig$: Observable<Action> = this.actions$.pipe(
        ofType<EditAdvertisingConfigAction>(EditAdvertisingConfigAction.TYPE),
        switchMap(action => {
            return this.managerAdvertisingConfigService.updateAdvertisingConfig(action.payload).pipe(
                map((data) => new SuccessEditAdvertisingConfigAction(data)),
                catchError((error) => of(new FailAdvertisingConfigCallAction(
                    {
                        key: EditAdvertisingConfigAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() createAdvertisingConfig$: Observable<Action> = this.actions$.pipe(
        ofType<CreateAdvertisingConfigAction>(CreateAdvertisingConfigAction.TYPE),
        switchMap(action => {
            return this.managerAdvertisingConfigService.createAdvertisingConfig(action.payload).pipe(
                map((data) => new SuccessCreateAdvertisingConfigAction(data)),
                catchError((error) => of(new FailAdvertisingConfigCallAction(
                    {
                        key: CreateAdvertisingConfigAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() deleteAdvertisingConfig$: Observable<Action> = this.actions$.pipe(
        ofType<DeleteAdvertisingConfigAction>(DeleteAdvertisingConfigAction.TYPE),
        switchMap(action => {
            return this.managerAdvertisingConfigService.deleteAdvertisingConfig(action.payload).pipe(
                map((data) => new SuccessDeleteAdvertisingConfigAction(data)),
                catchError((error) => of(new FailAdvertisingConfigCallAction(
                    {
                        key: DeleteAdvertisingConfigAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() deActivateAdvertisingConfig$: Observable<Action> = this.actions$.pipe(
        ofType<DeActiveAdvertisingConfigAction>(DeActiveAdvertisingConfigAction.TYPE),
        switchMap(action => {
            return this.managerAdvertisingConfigService.deActivateAdvertisingConfig(action.payload).pipe(
                map((data) => new SuccessDeActiveAdvertisingConfigAction(data)),
                catchError((error) => of(new FailAdvertisingConfigCallAction(
                    {
                        key: DeActiveAdvertisingConfigAction.TYPE,
                        info: error
                    }))));
        }));

    @Effect() activateAdvertisingConfig$: Observable<Action> = this.actions$.pipe(
        ofType<ActiveAdvertisingConfigAction>(ActiveAdvertisingConfigAction.TYPE),
        switchMap(action => {
            return this.managerAdvertisingConfigService.activateAdvertisingConfig(action.payload).pipe(
                map((data) => new SuccessActiveAdvertisingConfigAction(data)),
                catchError((error) => of(new FailAdvertisingConfigCallAction(
                    {
                        key: ActiveAdvertisingConfigAction.TYPE,
                        info: error
                    }))));
        }));
}

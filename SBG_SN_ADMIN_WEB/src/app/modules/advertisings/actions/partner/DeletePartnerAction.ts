import { Action } from '@ngrx/store';

export class DeletePartnerAction implements Action {
    public static TYPE = 'DELETE_PARTNER_ACTION';
    readonly type: string = DeletePartnerAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessDeletePartnerAction implements Action {
    public static TYPE = 'SUCCESS_DELETE_PARTNER_ACTION';
    readonly type: string = SuccessDeletePartnerAction.TYPE;
    constructor(public payload: any) { }
}

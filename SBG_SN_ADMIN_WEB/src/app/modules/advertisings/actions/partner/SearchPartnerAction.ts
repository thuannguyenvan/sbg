import { Action } from '@ngrx/store';

export class SearchPartnerAction implements Action {
    public static TYPE = 'SEARCH_PARTNER_ACTION';
    readonly type: string = SearchPartnerAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessSearchPartnerAction implements Action {
    public static TYPE = 'SUCCESS_SEARCH_PARTNER_ACTION';
    readonly type: string = SuccessSearchPartnerAction.TYPE;
    constructor(public payload: any) { }
}

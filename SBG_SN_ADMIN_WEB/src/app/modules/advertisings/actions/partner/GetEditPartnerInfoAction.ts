import { Action } from '@ngrx/store';

export class GetEditPartnerInfoAction implements Action {
    public static TYPE = 'GET_EDIT_PARTNER_INFO_ACTION';
    readonly type: string = GetEditPartnerInfoAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessGetEditPartnerInfoAction implements Action {
    public static TYPE = 'SUCCESS_GET_EDIT_PARTNER_INFO_ACTION';
    readonly type: string = SuccessGetEditPartnerInfoAction.TYPE;
    constructor(public payload: any) { }
}

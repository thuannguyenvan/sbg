import { Action } from '@ngrx/store';

export class FailPartnerCallAction implements Action {
    public static TYPE = 'FAIL_PARTNER_CALL_ACTION';
    readonly type: string = FailPartnerCallAction.TYPE;
    constructor(public payload: any) { }
}

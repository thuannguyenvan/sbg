import { Action } from '@ngrx/store';

export class CreatePartnerAction implements Action {
    public static TYPE = 'CREATE_PARTNER_ACTION';
    readonly type: string = CreatePartnerAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessCreatePartnerAction implements Action {
    public static TYPE = 'SUCCESS_CREATE_PARTNER_ACTION';
    readonly type: string = SuccessCreatePartnerAction.TYPE;
    constructor(public payload: any) { }
}

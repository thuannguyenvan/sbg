import { Action } from '@ngrx/store';

export class EditPartnerAction implements Action {
    public static TYPE = 'EDIT_PARTNER_ACTION';
    readonly type: string = EditPartnerAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessEditPartnerAction implements Action {
    public static TYPE = 'SUCCESS_EDIT_PARTNER_ACTION';
    readonly type: string = SuccessEditPartnerAction.TYPE;
    constructor(public payload: any) { }
}

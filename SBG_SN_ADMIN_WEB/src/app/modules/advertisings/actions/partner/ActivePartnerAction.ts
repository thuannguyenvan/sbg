import { Action } from '@ngrx/store';

export class ActivePartnerAction implements Action {
    public static TYPE = 'ACTIVE_PARTNER_ACTION';
    readonly type: string = ActivePartnerAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessActivePartnerAction implements Action {
    public static TYPE = 'SUCCESS_ACTIVE_PARTNER_ACTION';
    readonly type: string = SuccessActivePartnerAction.TYPE;
    constructor(public payload: any) { }
}

import { Action } from '@ngrx/store';

export class DeActivePartnerAction implements Action {
    public static TYPE = 'DEACTIVE_PARTNER_ACTION';
    readonly type: string = DeActivePartnerAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessDeActivePartnerAction implements Action {
    public static TYPE = 'SUCCESS_DEACTIVE_PARTNER_ACTION';
    readonly type: string = SuccessDeActivePartnerAction.TYPE;
    constructor(public payload: any) { }
}

import { Action } from '@ngrx/store';

export class SearchAdvertisingConfigAction implements Action {
    public static TYPE = 'SEARCH_ADVERTISING_CONFIG_ACTION';
    readonly type: string = SearchAdvertisingConfigAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessSearchAdvertisingConfigAction implements Action {
    public static TYPE = 'SUCCESS_SEARCH_ADVERTISING_CONFIG_ACTION';
    readonly type: string = SuccessSearchAdvertisingConfigAction.TYPE;
    constructor(public payload: any) { }
}

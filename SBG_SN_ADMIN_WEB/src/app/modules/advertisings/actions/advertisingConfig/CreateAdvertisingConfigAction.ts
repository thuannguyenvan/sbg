import { Action } from '@ngrx/store';

export class CreateAdvertisingConfigAction implements Action {
    public static TYPE = 'CREATE_ADVERTISING_CONFIG_ACTION';
    readonly type: string = CreateAdvertisingConfigAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessCreateAdvertisingConfigAction implements Action {
    public static TYPE = 'SUCCESS_CREATE_ADVERTISING_CONFIG_ACTION';
    readonly type: string = SuccessCreateAdvertisingConfigAction.TYPE;
    constructor(public payload: any) { }
}

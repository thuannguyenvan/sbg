import { Action } from '@ngrx/store';

export class DeActiveAdvertisingConfigAction implements Action {
    public static TYPE = 'DEACTIVE_ADVERTISING_CONFIG_ACTION';
    readonly type: string = DeActiveAdvertisingConfigAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessDeActiveAdvertisingConfigAction implements Action {
    public static TYPE = 'SUCCESS_DEACTIVE_ADVERTISING_CONFIG_ACTION';
    readonly type: string = SuccessDeActiveAdvertisingConfigAction.TYPE;
    constructor(public payload: any) { }
}

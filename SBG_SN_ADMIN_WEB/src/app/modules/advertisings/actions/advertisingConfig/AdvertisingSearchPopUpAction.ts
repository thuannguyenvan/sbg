import { Action } from '@ngrx/store';

export class AdvertisingSearchPopUpAction implements Action {
    public static TYPE = 'ADVERTISING_SEARCH_POPUP_ACTION';
    readonly type: string = AdvertisingSearchPopUpAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessAdvertisingSearchPopUpAction implements Action {
    public static TYPE = 'SUCCESS_ADVERTISING_SEARCH_POPUP_ACTION';
    readonly type: string = SuccessAdvertisingSearchPopUpAction.TYPE;
    constructor(public payload: any) { }
}

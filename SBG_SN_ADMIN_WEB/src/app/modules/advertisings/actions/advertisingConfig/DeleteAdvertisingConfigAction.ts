import { Action } from '@ngrx/store';

export class DeleteAdvertisingConfigAction implements Action {
    public static TYPE = 'DELETE_ADVERTISING_CONFIG_ACTION';
    readonly type: string = DeleteAdvertisingConfigAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessDeleteAdvertisingConfigAction implements Action {
    public static TYPE = 'SUCCESS_DELETE_ADVERTISING_CONFIG_ACTION';
    readonly type: string = SuccessDeleteAdvertisingConfigAction.TYPE;
    constructor(public payload: any) { }
}

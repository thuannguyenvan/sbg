import { Action } from '@ngrx/store';

export class GetAdvertisingSearchInfoPopUpAction implements Action {
    public static TYPE = 'GET_ADVERTISING_SEARCH_INFO_POPUP_ACTION';
    readonly type: string = GetAdvertisingSearchInfoPopUpAction.TYPE;
    constructor() { }
}

export class SuccessGetAdvertisingSearchInfoPopUpAction implements Action {
    public static TYPE = 'SUCCESS_GET_ADVERTISING_SEARCH_INFO_POPUP_ACTION';
    readonly type: string = SuccessGetAdvertisingSearchInfoPopUpAction.TYPE;
    constructor(public payload: any) { }
}

import { Action } from '@ngrx/store';

export class EditAdvertisingConfigAction implements Action {
    public static TYPE = 'EDIT_ADVERTISING_CONFIG_ACTION';
    readonly type: string = EditAdvertisingConfigAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessEditAdvertisingConfigAction implements Action {
    public static TYPE = 'SUCCESS_EDIT_ADVERTISING_CONFIG_ACTION';
    readonly type: string = SuccessEditAdvertisingConfigAction.TYPE;
    constructor(public payload: any) { }
}

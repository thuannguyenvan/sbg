import { Action } from '@ngrx/store';

export class FailAdvertisingConfigCallAction implements Action {
    public static TYPE = 'FAIL_ADVERTISING_CONFIG_CALL_ACTION';
    readonly type: string = FailAdvertisingConfigCallAction.TYPE;
    constructor(public payload: any) { }
}

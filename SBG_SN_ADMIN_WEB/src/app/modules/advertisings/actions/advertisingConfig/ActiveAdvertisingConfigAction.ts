import { Action } from '@ngrx/store';

export class ActiveAdvertisingConfigAction implements Action {
    public static TYPE = 'ACTIVE_ADVERTISING_CONFIG_ACTION';
    readonly type: string = ActiveAdvertisingConfigAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessActiveAdvertisingConfigAction implements Action {
    public static TYPE = 'SUCCESS_ACTIVE_ADVERTISING_CONFIG_ACTION';
    readonly type: string = SuccessActiveAdvertisingConfigAction.TYPE;
    constructor(public payload: any) { }
}

import { Action } from '@ngrx/store';

export class GetEditAdvertisingConfigInfoAction implements Action {
    public static TYPE = 'GET_EDIT_ADVERTISING_CONFIG_INFO_ACTION';
    readonly type: string = GetEditAdvertisingConfigInfoAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessGetEditAdvertisingConfigInfoAction implements Action {
    public static TYPE = 'SUCCESS_GET_EDIT_ADVERTISING_CONFIG_INFO_ACTION';
    readonly type: string = SuccessGetEditAdvertisingConfigInfoAction.TYPE;
    constructor(public payload: any) { }
}

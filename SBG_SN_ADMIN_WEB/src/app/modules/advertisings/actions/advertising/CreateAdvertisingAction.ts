import { Action } from '@ngrx/store';

export class CreateAdvertisingAction implements Action {
    public static TYPE = 'CREATE_ADVERTISING_ACTION';
    readonly type: string = CreateAdvertisingAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessCreateAdvertisingAction implements Action {
    public static TYPE = 'SUCCESS_CREATE_ADVERTISING_ACTION';
    readonly type: string = SuccessCreateAdvertisingAction.TYPE;
    constructor(public payload: any) { }
}

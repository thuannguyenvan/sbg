import { Action } from '@ngrx/store';

export class GetEditAdvertisingInfoAction implements Action {
    public static TYPE = 'GET_EDIT_ADVERTISING_INFO_ACTION';
    readonly type: string = GetEditAdvertisingInfoAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessGetEditAdvertisingInfoAction implements Action {
    public static TYPE = 'SUCCESS_GET_EDIT_ADVERTISING_INFO_ACTION';
    readonly type: string = SuccessGetEditAdvertisingInfoAction.TYPE;
    constructor(public payload: any) { }
}

import { Action } from '@ngrx/store';

export class DeActiveAdvertisingAction implements Action {
    public static TYPE = 'DEACTIVE_ADVERTISING_ACTION';
    readonly type: string = DeActiveAdvertisingAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessDeActiveAdvertisingAction implements Action {
    public static TYPE = 'SUCCESS_DEACTIVE_ADVERTISING_ACTION';
    readonly type: string = SuccessDeActiveAdvertisingAction.TYPE;
    constructor(public payload: any) { }
}

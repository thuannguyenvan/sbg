import { Action } from '@ngrx/store';

export class ActiveAdvertisingAction implements Action {
    public static TYPE = 'ACTIVE_ADVERTISING_ACTION';
    readonly type: string = ActiveAdvertisingAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessActiveAdvertisingAction implements Action {
    public static TYPE = 'SUCCESS_ACTIVE_ADVERTISING_ACTION';
    readonly type: string = SuccessActiveAdvertisingAction.TYPE;
    constructor(public payload: any) { }
}

import { Action } from '@ngrx/store';

export class DeleteAdvertisingAction implements Action {
    public static TYPE = 'DELETE_ADVERTISING_ACTION';
    readonly type: string = DeleteAdvertisingAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessDeleteAdvertisingAction implements Action {
    public static TYPE = 'SUCCESS_DELETE_ADVERTISING_ACTION';
    readonly type: string = SuccessDeleteAdvertisingAction.TYPE;
    constructor(public payload: any) { }
}

import { Action } from '@ngrx/store';

export class EditAdvertisingAction implements Action {
    public static TYPE = 'EDIT_ADVERTISING_ACTION';
    readonly type: string = EditAdvertisingAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessEditAdvertisingAction implements Action {
    public static TYPE = 'SUCCESS_EDIT_ADVERTISING_ACTION';
    readonly type: string = SuccessEditAdvertisingAction.TYPE;
    constructor(public payload: any) { }
}

import { Action } from '@ngrx/store';

export class FailAdvertisingCallAction implements Action {
    public static TYPE = 'FAIL_ADVERTISING_CALL_ACTION';
    readonly type: string = FailAdvertisingCallAction.TYPE;
    constructor(public payload: any) { }
}

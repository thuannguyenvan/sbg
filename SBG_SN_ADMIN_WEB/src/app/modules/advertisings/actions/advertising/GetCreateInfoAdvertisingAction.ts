import { Action } from '@ngrx/store';

export class GetCreateInfoAdvertisingAction implements Action {
    public static TYPE = 'GET_CREATE_INFO_ADVERTISING_ACTION';
    readonly type: string = GetCreateInfoAdvertisingAction.TYPE;
    constructor() { }
}

export class SuccessGetCreateInfoAdvertisingAction implements Action {
    public static TYPE = 'SUCCESS_GET_CREATE_INFO_ADVERTISING_ACTION';
    readonly type: string = SuccessGetCreateInfoAdvertisingAction.TYPE;
    constructor(public payload: any) { }
}

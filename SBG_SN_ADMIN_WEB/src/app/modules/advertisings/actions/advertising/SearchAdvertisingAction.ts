import { Action } from '@ngrx/store';

export class SearchAdvertisingAction implements Action {
    public static TYPE = 'SEARCH_ADVERTISING_ACTION';
    readonly type: string = SearchAdvertisingAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessSearchAdvertisingAction implements Action {
    public static TYPE = 'SUCCESS_SEARCH_ADVERTISING_ACTION';
    readonly type: string = SuccessSearchAdvertisingAction.TYPE;
    constructor(public payload: any) { }
}

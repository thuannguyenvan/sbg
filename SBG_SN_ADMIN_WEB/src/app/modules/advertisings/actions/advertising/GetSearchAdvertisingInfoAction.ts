import { Action } from '@ngrx/store';

export class GetSearchAdvertisingInfoAction implements Action {
    public static TYPE = 'GET_SEARCH_ADVERTISING_INFO_ACTION';
    readonly type: string = GetSearchAdvertisingInfoAction.TYPE;
    constructor() { }
}

export class SuccessGetSearchAdvertisingInfoAction implements Action {
    public static TYPE = 'SUCCESS_GET_SEARCH_ADVERTISING_INFO_ACTION';
    readonly type: string = SuccessGetSearchAdvertisingInfoAction.TYPE;
    constructor(public payload: any) { }
}

import { Action } from '@ngrx/store';

export class LoginAction implements Action {
    public static TYPE = 'LOGIN_ACTION';
    readonly type: string = LoginAction.TYPE;
    constructor(public payload: any) { }
}

export class SuccessLoginAction implements Action {
    public static TYPE = 'SUCCESS_LOGIN_ACTION';
    readonly type: string = SuccessLoginAction.TYPE;
    constructor(public payload: any) { }
}

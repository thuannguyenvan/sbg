import { Action } from '@ngrx/store';

export class GetLoginInfo implements Action {
    public static TYPE = 'GET_LOGIN_INFO';
    readonly type: string = GetLoginInfo.TYPE;
}

export class SuccessGetLoginInfo implements Action {
    public static TYPE = 'SUCCESS_GET_LOGIN_INFO ';
    readonly type: string = SuccessGetLoginInfo.TYPE;
    constructor(public payload: any) { }
}


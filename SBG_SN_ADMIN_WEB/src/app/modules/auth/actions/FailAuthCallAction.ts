import { Action } from '@ngrx/store';

export class FailAuthCallAction implements Action {
    public static TYPE = 'FAIL_AUTH_CALL_ACTION';
    readonly type: string = FailAuthCallAction.TYPE;
    constructor(public payload: any) { }
}

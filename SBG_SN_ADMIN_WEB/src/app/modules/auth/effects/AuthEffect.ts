import { Injectable } from '@angular/core';
import {
    Actions,
    Effect,
    ofType,
} from '@ngrx/effects';
import { AuthService } from '../../../services';
import { Observable, of } from 'rxjs';
import { Action } from '@ngrx/store';
import {
    LoginAction,
    SuccessLoginAction,
    FailAuthCallAction,
    GetLoginInfo,
    SuccessGetLoginInfo,
} from '../actions';
import { switchMap, map, catchError } from 'rxjs/operators';

@Injectable()
export class AuthEffect {
    constructor(private actions$: Actions, private authenService: AuthService) { }

    @Effect() login$: Observable<Action> = this.actions$.pipe(
        ofType<LoginAction>(LoginAction.TYPE),
        switchMap(action => {
            return this.authenService.login(action.payload).pipe(
                map((data) => new SuccessLoginAction(data)),
                catchError((error) => of(new FailAuthCallAction(
                    {
                        key: LoginAction.TYPE,
                        info: error
                    }))));
        }));
    @Effect() getLoginInfo$: Observable<Action> = this.actions$.pipe(
        ofType<GetLoginInfo>(GetLoginInfo.TYPE),
        switchMap(action => {
            return this.authenService.getLoginInfo().pipe(
                map((data) => new SuccessGetLoginInfo(data)),
                catchError((error) => of(new FailAuthCallAction({
                    key: GetLoginInfo.TYPE,
                    info: error
                }))));
        }));
}

import {
    SessionUserInfo,
    ErrorResult,
    InfoResult,
} from '../../../models';

import {
    SuccessLoginAction,
    FailAuthCallAction,
    SuccessGetLoginInfo,
} from '../actions';

export function AuthReducer(state: any = {}, action: any) {
    switch (action.type) {
        case SuccessLoginAction.TYPE:
            state.login = action.payload === undefined || action.payload === null ? action.payload
                : new InfoResult<SessionUserInfo>(action.payload);
            return state;
        case SuccessGetLoginInfo.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.getLoginInfo = action.payload;
                return state;
            }
            if (action.payload.data === null || action.payload.data === undefined) {
                state.getLoginInfo = new InfoResult<SessionUserInfo>(action.payload);
                return state;
            }
            const loginInfo = action.payload;
            loginInfo.data = new SessionUserInfo(action.payload.data);
            state.getLoginInfo = new InfoResult<SessionUserInfo>(loginInfo);
            return state;
        case FailAuthCallAction.TYPE:
            if (action.payload === undefined || action.payload === null) {
                state.error = action.payload;
                return state;
            }
            if (action.payload.info === undefined) {
                state.error = undefined;
                return state;
            }
            const errorResult = new ErrorResult(action.payload.info);
            errorResult.key = action.payload.key;
            state.error = errorResult;
            return state;
        default: {
            return state;
        }
    }
}

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';

import {
    SharedModule,
} from '../../modules';

import {
    AuthEffect,
} from './effects';
import {
    EnvService
} from '../../services';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { AuthReducer } from './reducers';
import {
    LoginComponent,
    CallbackComponent
} from './components';

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        FormsModule,
        BrowserModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        RouterModule.forChild([
            { path: 'login', component: LoginComponent },
            { path: 'sso-callback', component: CallbackComponent },
        ]),
        EffectsModule.forFeature([
            AuthEffect,
        ]),
        StoreModule.forFeature('auths', AuthReducer),
    ],
    declarations: [
        LoginComponent,
        CallbackComponent
    ],
    providers: [
        CookieService,
        EnvService,
    ]
})
export class AuthModule { }

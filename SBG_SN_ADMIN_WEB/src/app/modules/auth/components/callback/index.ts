import {
  Component,
  OnInit,
  OnDestroy,
  Inject
} from '@angular/core';
import {
  ActivatedRoute,
  Router
} from '@angular/router';
import {
  LoginModel,
  InfoResult,
  SessionUserInfo,
  ErrorResult
} from '../../../../models';
import { CookieService } from 'ngx-cookie';
import {
  KeyConstants,
  RouterConstants
} from '../../../../constants';
import { AppState } from '../../../../AppState';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import {
  FailAuthCallAction,
  LoginAction,
  SuccessLoginAction,
  SuccessGetLoginInfo
} from '../../actions';
import {
  SpinnerHelper,
  ToastrHelper,
} from '../../../../helpers';
import {
  SESSION_STORAGE,
  WebStorageService
} from 'angular-webstorage-service';
@Component({
  selector: 'app-callback',
  templateUrl: './index.html',
})
export class CallbackComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  public loginModel: LoginModel;

  constructor(
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private cookieService: CookieService,
    private router: Router,
    private spinner: SpinnerHelper,
    private toastr: ToastrHelper,
    @Inject(SESSION_STORAGE) private storage: WebStorageService,
  ) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.spinner.show();
      this.store.dispatch(new LoginAction(new LoginModel({
        token: params['access_token'],
      })));
    });
  }

  ngOnInit() {
    this.onSubscriptionsError();
    this.onSubscriptionsSuccess();
  }

  onSubscriptionsSuccess() {
    this.subscriptions.push(this.store.select<InfoResult<SessionUserInfo>>(state => state.auths.login)
      .subscribe((result) => {
        if (result !== undefined) {
          this.spinner.hide();
          if (result.data.token !== undefined &&
            result.data.token !== null &&
            result.data.token !== ''
          ) {
            this.toastr.success(result.message);
            this.cookieService.put(KeyConstants.TOKEN, result.data.token);
            const userInfoJson = JSON.stringify(result.data);
            this.storage.set(KeyConstants.USER_INFO, userInfoJson);
            this.store.dispatch(new SuccessGetLoginInfo(result));

            setTimeout(() => {
              this.router.navigate(['']);
            }, 1000);

          } else {
            this.storage.remove(KeyConstants.USER_INFO);
            this.router.navigate([RouterConstants.auths.login]);
          }
        }
      }));
  }

  onSubscriptionsError() {
    this.subscriptions.push(this.store.select<ErrorResult>(state => state.auths.error)
      .subscribe((result) => {
        if (result !== undefined) {
          switch (result.key) {
            case LoginAction.TYPE:
              this.toastr.showError(result);
              this.spinner.hide();
              break;
          }
        }
      }));
  }

  destroyAction() {
    this.store.dispatch({ type: FailAuthCallAction.TYPE });
    this.store.dispatch({ type: SuccessLoginAction.TYPE });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscriptsion) => subscriptsion.unsubscribe());
    this.destroyAction();
  }
}

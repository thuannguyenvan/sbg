import { Component, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { EnvService } from '../../../../services';

@Component({
  selector: 'app-login',
  templateUrl: './index.html',
  styleUrls: ['./index.scss'],
})
export class LoginComponent {
  constructor(
    @Inject(DOCUMENT) private document: any,
    private env: EnvService,
  ) {
  }

  onLogin() {
    const encodeUrl = encodeURIComponent('http://' + document.location.host + '/sso-callback');
    const url = this.env.SSO_SIGNIN + encodeUrl;
    this.document.location.href = url;
  }
}

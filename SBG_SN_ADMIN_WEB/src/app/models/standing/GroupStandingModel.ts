import { StandingObjectModel } from '.';

export class GroupStandingModel {
    constructor(private p: any = null) {
        if (p !== null && p !== undefined) {
            this.group = p.group;
            this.tables = this.p.tables === null || this.p.tables === undefined ? []
            : this.p.tables.map((obj: any) => new StandingObjectModel(obj));
        }
    }
    group: string;
    tables: Array<StandingObjectModel>;
}

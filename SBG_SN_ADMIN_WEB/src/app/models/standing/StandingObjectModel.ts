export class StandingObjectModel {
    constructor(private p: any = null) {
        if (p !== null && p !== undefined) {
            this.name = p.name;
            this.id = p.id;
            this.group = p.group;
            this.played = p.played === undefined || p.played == null ? undefined : p.played;
            this.position = p.position === undefined || p.position == null ? undefined : p.position;
            this.won = p.won === undefined || p.won == null ? undefined : p.won;
            this.drawn = p.drawn === undefined || p.drawn == null ? undefined : p.drawn;
            this.lost = p.lost === undefined || p.lost == null ? undefined : p.lost;
            this.goalsFor = p.goalsFor === undefined || p.goalsFor == null ? undefined : p.goalsFor;
            this.goalsAgainst = p.goalsAgainst === undefined || p.goalsAgainst == null ? undefined : p.goalsAgainst;
            this.goalDifference = p.goalDifference === undefined || p.goalDifference == null ? undefined : p.goalDifference;
            this.points = p.points === undefined || p.points == null ? undefined : p.points;
        }
    }
    id: string;
    group: string;
    name: string;
    position: number;
    played: number;
    won: number;
    drawn: number;
    lost: number;
    goalsFor: number;
    goalsAgainst: number;
    goalDifference: number;
    points: number;
}

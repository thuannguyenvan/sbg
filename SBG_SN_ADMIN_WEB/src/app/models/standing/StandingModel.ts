import { StandingObjectModel } from '..';

export class StandingModel {
    constructor(private p: any = null) {
        if (p !== null && p !== undefined) {
            this.id = p.id;
            this.isShowInHomepage = p.isShowInHomepage;
            this.soccerCompetitionId = p.soccerCompetitionId;
            this.soccerCompetitionStageId = p.soccerCompetitionStageId;
            this.tables = this.p.tables === null || this.p.tables === undefined ? []
            : this.p.tables.map((obj: any) => new StandingObjectModel(obj));
        }
    }
    id: string;
    soccerCompetitionId: string;
    soccerCompetitionStageId: string;
    isShowInHomepage: boolean;
    tables: Array<StandingObjectModel>;
}

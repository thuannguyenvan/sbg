export class SearchTeamModel {
    constructor(private p: any = null) {
        if (p !== null && p !== undefined) {
            this.soccerCompetitionsId = p.soccerCompetitionsId;
            this.soccerStageId = p.soccerStageId;
        }
    }
    soccerStageId: string;
    soccerCompetitionsId: string;
}

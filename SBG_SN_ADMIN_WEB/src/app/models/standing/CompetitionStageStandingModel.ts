export class CompetitionStageStandingModel {
    constructor(private p: any = null) {
        if (p !== null && p !== undefined) {
            this.title = p.title;
            this.id = p.id;
            this.soccerStageId = p.soccerStageId;
            this.soccerCompetitionId = p.soccerCompetitionId;
            this.groupInfos = p.groupInfos;
            this.groupInfoDisplays = p.groupInfos === undefined || p.groupInfos === null
                                    || p.groupInfos.length <= 0 ? '' :  p.groupInfos.join();
            this.actions = p.actions;
            this.standAlone = p.standAlone;
        }
    }
    title: string;
    id: string;
    soccerStageId: string;
    groupInfos: Array<string>;
    groupInfoDisplays: string;
    actions: Array<string>;
    soccerCompetitionId: string;
    standAlone: boolean;
}

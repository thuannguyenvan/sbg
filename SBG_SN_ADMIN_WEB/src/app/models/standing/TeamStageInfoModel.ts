import {
    CompetitionTeamModel,
    CompetitionStageTeamModel
} from '..';

export class TeamStageInfoModel {
    constructor(p: any = null) {
        if (p !== null && p !== undefined) {
            this.competitionTeams = p.competitionTeams === null || p.competitionTeams === undefined ? []
                : p.competitionTeams.map((d: CompetitionTeamModel) => new CompetitionTeamModel(d));
            this.competitionStageTeams = p.competitionStageTeams === null || p.competitionStageTeams === undefined ? []
                : p.competitionStageTeams.map((d: CompetitionStageTeamModel) => new CompetitionStageTeamModel(d));
        }
    }
    competitionTeams: Array<CompetitionTeamModel>;
    competitionStageTeams: Array<CompetitionStageTeamModel>;
}

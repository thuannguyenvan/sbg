export class CompetitionStageTeamModel {
    constructor(private p: any = null) {
        if (p !== null && p !== undefined) {
            this.id = p.id;
            this.soccerCompetitionStageId = p.soccerCompetitionStageId;
            this.soccerCompetitionTeamId = p.soccerCompetitionTeamId;
            this.soccerCompetitionId = p.soccerCompetitionId;
            this.soccerTeamId = p.soccerTeamId;
            this.group = p.group === null || p.group === '' ? undefined : p.group;
            this.name = p.name;
            this.logo = p.logo;
            this.teamCode = p.teamCode;
        }
    }
    id: string;
    soccerCompetitionStageId: string;
    soccerCompetitionTeamId: string;
    soccerCompetitionId: string;
    soccerTeamId: string;
    group: string;
    name: string;
    logo: string;
    teamCode: string;
}

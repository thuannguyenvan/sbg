﻿export class InfoResult<T extends any> {
    constructor(private p: any = null) {
        this.code = p === null || p === undefined ? p
            : p.code;
        this.message = p === null || p === undefined ? p
            : p.message;
        this.time = p === null || p === undefined ? p
            : p.time;
        this.data = p === null || p === undefined ? p
            : p.data;
    }
    code: number;
    message: string;
    time: number;
    data: T;
}

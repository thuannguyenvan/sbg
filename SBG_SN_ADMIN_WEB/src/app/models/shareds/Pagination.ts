﻿export class Pagination {
    constructor(private props: any = null) {
        if (this.props === null || this.props === undefined) {
            this.pageSize = 1;
            this.pageIndex = 1;
            this.pageSizeOptions = [1, 2, 50, 100];
            this.length = 0;
        } else {
            this.pageSize = this.props.pageSize === undefined || this.props.pageSize === undefined ? 10 : this.props.pageSize;
            this.pageIndex = this.props.pageIndex === undefined || this.props.pageIndex === undefined ? 1 : this.props.pageIndex;
            this.pageSort = this.props.pageSort;
            this.pageSizeOptions = this.props.pageSizeOptions === undefined ||
                this.props.pageSizeOptions === undefined ? [10, 20, 50, 100] : this.props.pageSizeOptions;
            this.length = this.props.length === undefined || this.props.length === undefined ? 100 : this.props.length;

        }
    }
    pageSize: number;
    pageSort: string;
    length: number;
    pageSizeOptions: number[];
    pageIndex: number;
}

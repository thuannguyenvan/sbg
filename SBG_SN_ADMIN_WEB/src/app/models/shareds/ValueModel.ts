export class ValueModel {
    constructor(private p: any = null) {
        if (p !== null && p !== undefined) {
            this.value = p.value;
            this.title = p.title;
        }
    }
    value: string;
    title: string;
}

export class SearchModel {
    constructor(p: any = null) {
        if (p === undefined || p === null) {
            this.page = 1;
            this.size = 10;
        } else {
            this.page = p.page === null || p.page === undefined ? 1
                : p.page;
            this.size = p.size === null || p.size === undefined ? 10
                : p.size;
        }
    }
    page: number;
    size: number;
}

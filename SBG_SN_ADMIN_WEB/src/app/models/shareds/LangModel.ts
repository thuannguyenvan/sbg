import { String } from 'typescript-string-operations';
export class LangModel {
    constructor(private p: any = null) {
        if (this.p === null || this.p === undefined) {
            this.active = 'vi';
            this.langs = [{
                avatar: String.Format(this.urlAvatarBase, this.active),
                value: this.active,
                title: 'Tiếng việt'
            }];
        } else {
            this.active = p.active;
            this.langs = p.langs === null || p.langs === undefined ? [] : p.langs.map(e => {
                return {
                    avatar: String.Format(this.urlAvatarBase, e.value),
                    value: e.value,
                    title: e.title
                };
            });
        }
        this.activeAvatar = String.Format(this.urlAvatarBase, this.active);
    }
    urlAvatarBase = '/assets/images/lang/{0}.png';
    active: string;
    activeAvatar: string;
    langs: Array<any>;
}

﻿import { ErrorItem } from '../../models';
import { ErrorType } from '../../enums';

export class ErrorResult {
    constructor(private p: any = null) {
        this.message = p === null || p === undefined ? p
            : p.message;
        this.type = p === null || p === undefined ? p
            : p.type;
        this.time = p === null || p === undefined ? p
            : p.time;
        this.errors = p === null || p === undefined ? p
            : p.errors === undefined || p.errors === null ? p.errors
                : p.errors.map((data) => new ErrorItem(data));
        this.messages = p === null || p === undefined ? p
            : p.messages === undefined || p.errors === null ? p.messages
                : p.messages.map((data) => data);
        this.key = p === null || p === undefined ? p
            : p.key;
    }
    key: string;
    type: ErrorType;
    message: string;
    messages: Array<string>;
    errors: Array<ErrorItem>;
    time: number;
}

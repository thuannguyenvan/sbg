﻿export class FileModel {
    constructor(private p: any = null) {
        if (p === null || p === undefined) {
            this.id = '';
            this.file = null;
        } else {
            this.id = p.id;
            this.file = p.file;
        }
    }
    id: string;
    file: File;
}

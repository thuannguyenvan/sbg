﻿export class ErrorItem {
    constructor(private p: any) {
        this.key = p.key;
        this.message = p.message;
    }
    key: string;
    message: string;
}

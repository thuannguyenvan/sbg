﻿import { MenuInfo } from '.';
import { LocationMenu } from '../../enums';

export class SessionUserInfo {
    constructor(p: any = null) {
        if (p !== null && p !== undefined) {
            this.id = p.id;
            this.token = p.token;
            this.avatar = p.avatar;
            this.userName = p.userName;
            this.email = p.email;
            this.fullName = p.fullName;
            this.phoneNumber = p.phoneNumber;
            this.roles = p.roles === null && p.roles === undefined ? []
                : p.roles;
            this.actions = p.actions === null && p.actions === undefined ? []
                : p.actions;
            this.isSupperAdmin = p.isSupperAdmin;
            this.status = p.status;
            this.isActive = p.isActive;
            this.cmsLeftMenus = [];
            this.cmsTopMenus = [];
            const menus = (p.menus === null || p.menus === undefined || p.menus.length === 0) ? [] : p.menus.map(e => new MenuInfo(e));
            this.menus = menus;
            const cmsLeftMenuRoot = menus.length > 0 ? menus.filter(e => e.level === 0 && e.location === LocationMenu.CMS_LEFT_MENU) : [];
            if (cmsLeftMenuRoot.length > 0) {
                cmsLeftMenuRoot.forEach(e => {
                    const menu = e;
                    menu.menus = menus.filter(x => x.parentId === e.id && e.location === LocationMenu.CMS_LEFT_MENU);
                    this.cmsLeftMenus.push(menu);
                });
            }

            const cmsTopMenuRoot = menus.length > 0 ? menus.filter(e => e.level === 0 && e.location === LocationMenu.CMS_TOP_MENU) : [];
            if (cmsTopMenuRoot.length > 0) {
                cmsTopMenuRoot.forEach(e => {
                    const menu = e;
                    menu.menus = menus.filter(x => x.parentId === e.id && e.location === LocationMenu.CMS_TOP_MENU);
                    this.cmsTopMenus.push(menu);
                });
            }

            this.supportedLanguage = p.supportedLanguage === null || p.supportedLanguage === undefined ? [] : p.supportedLanguage;
        }
    }
    id: string;
    token: string;
    userName: string;
    email: string;
    fullName: string;
    phoneNumber: string;
    avatar: string;
    roles: Array<string>;
    actions: Array<string>;
    isSupperAdmin: boolean;
    status: number;
    isActive: boolean;
    menus: Array<MenuInfo>;
    cmsLeftMenus: Array<MenuInfo>;
    cmsTopMenus: Array<MenuInfo>;
    supportedLanguage: Array<any>;
}

﻿export class SelectEnum {
    constructor(private p: any) {
        this.id = p.id;
        this.name = p.name;
        this.description = p.description;
    }
    id: number;
    name: string;
    description: string;
}

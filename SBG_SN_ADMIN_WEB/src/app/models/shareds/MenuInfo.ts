import { LocationMenu } from '../../enums';

export class MenuInfo {
    constructor(private p: any = null) {
        if (p !== null && p !== undefined) {
            this.id = p.id;
            this.parentId = p.parentId;
            this.level = p.level;
            this.title = p.title;
            this.url = p.url;
            this.icon = p.icon;
            this.location = p.location;
            this.expanded = p.expanded;
        }
    }
    id: string;
    parentId: string;
    title: string;
    level: number;
    icon: string;
    url: string;
    location: LocationMenu;
    expanded: boolean;
}

export class ValidateModel {
    constructor(private props: any) {
        this.userName = this.props.userName;
        this.password = this.props.password;
        this.age = (this.props.age === undefined) ? null : this.props.age;
        this.dateStart = this.props.dateStart;
        this.dateEnd = this.props.dateEnd;
        this.format = this.props.format;
    }
    userName: string;
    password: string;
    age: number;
    dateStart: string;
    dateEnd: string;
    format: Array <string>;
}

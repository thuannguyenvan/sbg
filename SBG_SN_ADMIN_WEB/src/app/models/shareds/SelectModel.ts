﻿export class SelectModel {
    constructor(private p: any) {
        if (p === undefined || p === null) {
            this.id = p;
            this.name = p;
            this.partnerId = p;
            this.status = p;
            this.value = p;
            this.display = p;
        } else {
            this.id = p.id;
            this.name = p.name;
            this.partnerId = p.partnerId;
            this.status = p.status;
            this.value = p.value;
            this.display = p.display;
        }
    }
    id: number;
    name: string;
    partnerId: number;
    status: number;
    value: string;
    display: string;
}

export class UserRoleModel {
    constructor( p: any = null) {
        if (p !== null && p !== undefined) {
            this.id = p.id;
            this.checked = p.checked;
            this.name = p.name;
            this.userId = p.userId;
        }
    }
    id: string;
    checked: boolean;
    name: string;
    userId: string;
}


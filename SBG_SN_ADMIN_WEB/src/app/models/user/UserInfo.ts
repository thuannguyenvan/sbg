import { UserRoleModel } from './UserRoleModel';

export class UserInfo {
    constructor(p: any = null) {
        if (p !== null && p !== undefined) {
            this.id = p.id;
            this.userName = p.userName;
            this.fullName = p.fullName;
            this.email = p.email;
            this.roles = p.roles;
            this.rolesStr = p.rolesStr;
            this.total = p.total;
            this.actions = p.actions;
        }
    }
    id: number;
    userName: string;
    fullName: string;
    email: string;
    roles: Array<UserRoleModel>;
    rolesStr: string;
    total: number;
    actions: Array<string>;
}

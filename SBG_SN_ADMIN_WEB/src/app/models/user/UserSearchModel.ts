import { SearchModel } from '../shareds';

export class UserSearchModel extends SearchModel {
    constructor(p: any = null) {
        super(p);
        if (p !== null && p !== undefined) {
            this.orderBy = p.orderBy;
            this.order = p.order;
            this.keywords = p.keywords;
        }
    }
    orderBy: string;
    order: string;
    keywords: string;
}


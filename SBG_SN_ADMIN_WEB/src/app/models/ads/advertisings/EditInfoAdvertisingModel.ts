

import {
    AdvertisingFomartsModel,
    PartnerModel,
    AdvertisingModel
} from '..';

export class EditInfoAdvertisingModel {
    constructor(p: any = null) {
        if (p !== null && p !== undefined) {
            this.advertisingFormats = p.advertisingFormats === null || p.advertisingFormats === undefined ? []
            : p.advertisingFormats.map((obj: any) => new AdvertisingFomartsModel(obj));
            this.adsPartners = p.adsPartners === null || p.adsPartners === undefined ? []
            : p.adsPartners.map((obj: any) => new PartnerModel(obj));
            this.advertising = p.advertising;
        }
    }
    adsPartners: Array<PartnerModel>;
    advertising: AdvertisingModel;
    advertisingFormats: AdvertisingFomartsModel;
}

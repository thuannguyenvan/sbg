
export class AdvertisingModel {
    constructor(p: any = null) {
        if (p !== null && p !== undefined) {
            this.id = p.id;
            this.adsPartnerId = p.adsPartnerId;
            this.title = p.title;
            this.content = p.content;
            this.description = p.description;
            this.clickThrough = p.clickThrough;
            this.clickTracking = p.clickTracking;
            this.total = p.total;
            this.actions = p.actions;
            this.adsFormat = p.adsFormat;
            this.type = p.type;
            this.isActive = p.isActive;
            this.adsPartnerName = p.adsPartnerName;
            this.adsFormatTitle = p.adsFormatTitle;
            this.adsPartnerWebsite = p.adsPartnerWebsite;
        }
    }
    adsPartnerId: string;
    id: string;
    title: string;
    content: string;
    clickThrough: string;
    clickTracking: string;
    description: string[];
    type: string;
    adsFormat: string;
    adsPartnerName: string;
    isActive: Boolean;
    total: number;
    actions: Array<string>;
    adsFormatTitle: string;
    adsPartnerWebsite: string;
}

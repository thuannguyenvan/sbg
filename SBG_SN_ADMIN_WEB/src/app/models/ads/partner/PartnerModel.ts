
export class PartnerModel {
    constructor(p: any = null) {
        if (p !== null && p !== undefined) {
            this.id = p.id;
            this.name = p.name;
            this.logo = p.logo;
            this.website = p.website;
            this.websiteUrl = p.website;
            if (p.website !== undefined && p.website !== null) {
                if (p.website.toLowerCase().indexOf('http') !== 0 && p.website.toLowerCase().indexOf('https') !== 0
                && p.website.toLowerCase().indexOf('/') !== 0 && p.website.toLowerCase().indexOf('\\') !== 0) {
                    this.websiteUrl = 'http://' + p.website;
                }
            }
            this.description = p.description;
            this.actions = p.actions;
            this.isActive = p.isActive;
            this.total = p.total;
        }
    }
    name: string;
    id: string;
    logo: string;
    website: string;
    websiteUrl: string;
    description: string;
    isActive: boolean;
    actions: string[];
    total: number;
}

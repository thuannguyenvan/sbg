
export class RollsModel {
    constructor(p: any = null) {
        if (p !== null && p !== undefined) {
            this.advertisingId = p.advertisingId;
            this.duration = p.duration;
            this.skipOffset = p.skipOffset === null ? undefined : p.skipOffset;
            this.advertisingTitle = p.advertisingTitle;
        }
    }
    advertisingTitle: string;
    advertisingId: string;
    duration: string;
    skipOffset: Number;
}

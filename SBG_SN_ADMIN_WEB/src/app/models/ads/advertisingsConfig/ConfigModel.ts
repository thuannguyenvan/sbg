import {
    MidRollsModel,
    RollsModel,
} from '.';

export class ConfigModel {
    constructor(p: any = null) {
        if (p !== null && p !== undefined) {
            this.midRolls = p.midRolls === null || p.midRolls === undefined ? []
                : p.midRolls.map((obj: any) => new MidRollsModel(obj));
            this.postRolls = p.postRolls === null || p.postRolls === undefined ? []
                : p.postRolls.map((obj: any) => new RollsModel(obj));
            this.preRolls = p.preRolls === null || p.preRolls === undefined ? []
                : p.preRolls.map((obj: any) => new RollsModel(obj));
        }
    }
    midRolls: Array<MidRollsModel>;
    postRolls: Array<RollsModel>;
    preRolls: Array<RollsModel>;
}

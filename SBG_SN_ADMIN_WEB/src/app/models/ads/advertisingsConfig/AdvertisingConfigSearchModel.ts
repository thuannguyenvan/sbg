
export class AdvertisingConfigSearchModel {
    constructor(p: any = null) {
        if (p !== null && p !== undefined) {
            this.id = p.id;
            this.title = p.title;
            this.total = p.total;
            this.actions = p.actions;
            this.isActive = p.isActive;
        }
    }
    id: string;
    title: string;
    isActive: Boolean;
    total: number;
    actions: Array<string>;
}

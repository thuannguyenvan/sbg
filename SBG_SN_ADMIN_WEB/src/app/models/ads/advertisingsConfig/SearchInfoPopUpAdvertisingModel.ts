
import {
    PartnerModel,
} from '..';

export class SearchInfoPopUpAdvertisingModel {
    constructor(p: any = null) {
        if (p !== null && p !== undefined) {
            this.adsPartners = p.adsPartners === null || p.adsPartners === undefined ? []
            : p.adsPartners.map((obj: any) => new PartnerModel(obj));
        }
    }
    adsPartners: Array<PartnerModel>;
}


import {
    ConfigModel,
} from '.';

export class AdvertisingConfigModel {
    constructor(p: any = null) {
        if (p !== null && p !== undefined) {
            this.id = p.id;
            this.title = p.title;
            this.description = p.description;
            this.config = p.config === null ? undefined
            : new ConfigModel(p.config);
        }
    }
    id: string;
    title: string;
    description: string;
    config: ConfigModel;
}

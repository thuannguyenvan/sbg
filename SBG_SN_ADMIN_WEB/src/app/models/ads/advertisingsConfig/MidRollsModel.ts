
export class MidRollsModel {
    constructor(p: any = null) {
        if (p !== null && p !== undefined) {
            this.advertisingId = p.advertisingId;
            this.duration = p.duration;
            this.skipOffset = p.skipOffset;
            this.timeOffset = p.timeOffset;
            this.overlay = p.overlay === undefined || p.overlay === null ? false : p.overlay;
            this.advertisingTitle = p.advertisingTitle;
        }
    }
    advertisingId: string;
    duration: string;
    skipOffset: string;
    timeOffset: string;
    overlay: string;
    advertisingTitle: string;
}

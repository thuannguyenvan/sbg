export class MatchTeamModel {
    constructor(p: any = null) {
        if (p !== null && p !== undefined) {
            this.soccerCompetitionStageTeamId = p.soccerCompetitionStageTeamId;
            this.soccerCompetitionTeamId = p.soccerCompetitionTeamId;
            this.soccerTeamId = p.soccerTeamId;
            this.name = p.name;
            this.teamCode = p.teamCode;
            this.logo = p.logo;
        }
    }
    soccerCompetitionStageTeamId: string;
    soccerCompetitionTeamId: string;
    soccerTeamId: string;
    name: string;
    teamCode: string;
    logo: string;
}


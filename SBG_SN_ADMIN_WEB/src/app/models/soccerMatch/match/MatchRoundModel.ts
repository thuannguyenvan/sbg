import { DateTimeHelper } from '../../../helpers/DateTimeHelper';
import { FormatDateTimeConstants } from '../../../constants';

export class MatchRoundModel {
    constructor(p: any = null) {
        if (p !== null && p !== undefined) {
            this.id = p.id;
            this.title = p.title;
            this.soccerCompetitionId = p.soccerCompetitionId;
            this.soccerCompetitionName = p.soccerCompetitionName;
            this.soccerCompetitionStageId = p.soccerCompetitionStageId;
            this.soccerCompetitionStageId = p.soccerCompetitionStageId;
            this.soccerCompetitionStageName = p.soccerCompetitionStageName;
            this.description = p.description;
            if (p.startAt === null || p.startAt === undefined) {
                this.startAt = p.startAt;
            } else {
                if (!DateTimeHelper.isValidDateTime(p.dateStartAt)) {
                    this.startAt = DateTimeHelper.formatDateTime(new Date(p.startAt), FormatDateTimeConstants.DD_MM_YYYY);
                } else {
                    this.startAt = p.startAt;
                }
            }
            if (p.endAt === null || p.endAt === undefined) {
                this.endAt = p.endAt;
            } else {
                if (!DateTimeHelper.isValidDateTime(p.endAt)) {
                    this.endAt = DateTimeHelper.formatDateTime(new Date(p.endAt), FormatDateTimeConstants.DD_MM_YYYY);
                } else {
                    this.endAt = p.endAt;
                }
            }
        }
    }
    id: string;
    title: string;
    soccerCompetitionId: string;
    soccerCompetitionName: string;
    soccerCompetitionStageId: string;
    soccerCompetitionStageName: string;
    startAt: string;
    endAt: string;
    description: string;
}

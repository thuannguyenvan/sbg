import { MatchSoccerCompetition } from '.';
import { ValueModel } from '../..';

export class MatchSearchInfo {
    constructor(p: any = null) {
        if (p !== null && p !== undefined) {
            this.soccerCompetitions = p.soccerCompetitions;
            this.soccerMatchStatuses = p.soccerMatchStatuses;
        }
    }
    soccerCompetitions: Array<MatchSoccerCompetition>;
    soccerMatchStatuses: Array<ValueModel>;
}


import { DateTimeHelper } from '../../../helpers/DateTimeHelper';
import { FormatDateTimeConstants } from '../../../constants';
import {
    MatchTeamModel,
    MatchScoreModel
} from '.';

export class MatchListInfo {
    constructor(p: any = null) {
        if (p !== null && p !== undefined) {
            this.title = p.title;
            this.id = p.id;
            this.soccerCompetitionId = p.soccerCompetitionId;
            this.soccerCompetitionTitle = p.soccerCompetitionTitle;
            this.soccerRoundId = p.soccerRoundId;
            this.soccerRoundTitle = p.soccerRoundTitle;
            if (p.startAt === null || p.startAt === undefined) {
                this.startAt = p.startAt;
            } else {
                if (!DateTimeHelper.isValidDateTime(p.startAt)) {
                    this.startAt = DateTimeHelper.formatDateTime(new Date(p.startAt), FormatDateTimeConstants.DD_MM_YYYY_HH_MM);
                } else {
                    this.startAt = p.startAt;
                }
            }
            this.status = p.status;
            this.teamA = p.teamA;
            this.teamB = p.teamB;
            this.winner = p.winner;
            this.score = p.score;
            this.total = p.total;
            this.actions = p.actions;
            this.statusTitle = p.statusTitle;
            this.scoreAGTeamA = p.scoreAGTeamA;
            this.scoreAGTeamB = p.scoreAGTeamB;
            this.penTeamA = p.penTeamA;
            this.penTeamB = p.penTeamB;
            this.scoreTeamA = p.scoreTeamA;
            this.scoreTeamB = p.scoreTeamB;
            this.showPen = p.showPen;
            this.showAG = p.showAG;
            this.showScore = p.showScore;
            this.showEmpty = p.showEmpty;
            this.soccerMatchStreamId = p.soccerMatchStreamId;
        }
    }
    id: string;
    title: string;
    soccerCompetitionId: string;
    soccerCompetitionTitle: string;
    soccerRoundId: string;
    soccerRoundTitle: string;
    startAt: string;
    status: string;
    teamA: MatchTeamModel;
    teamB: MatchTeamModel;
    winner: string;
    score: MatchScoreModel;
    total: number;
    actions: string[];
    statusTitle: string;
    // score
    scoreTeamA: string;
    scoreTeamB: string;
    scoreAGTeamA: string;
    scoreAGTeamB: string;
    penTeamA: string;
    penTeamB: string;
    showPen: boolean;
    showAG: boolean;
    showScore: boolean;
    showEmpty: boolean;
    soccerMatchStreamId: string;
}


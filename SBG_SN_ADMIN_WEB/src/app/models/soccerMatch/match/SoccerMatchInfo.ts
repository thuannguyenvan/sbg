import {
    SoccerMatchStatuses,
    WinnerTeamTypes,
} from '.';
import { CompetitionsInfo } from '../..';

export class SoccerMatchInfo {
    constructor( p: any = null) {
        if (p !== null && p !== undefined) {
            this.soccerCompetitions = p.soccerCompetitions === null || p.soccerCompetitions === undefined ? []
            : p.soccerCompetitions.map((obj: any) => new CompetitionsInfo(obj));
            this.soccerMatchStatuses = p.soccerMatchStatuses === null || p.soccerMatchStatuses === undefined ? []
            : p.soccerMatchStatuses.map((obj: any) => new SoccerMatchStatuses(obj));
            this.winnerTeamTypes = p.winnerTeamTypes === null || p.winnerTeamTypes === undefined ? []
            : p.winnerTeamTypes.map((obj: any) => new WinnerTeamTypes(obj));
        }
    }
    soccerCompetitions: Array<CompetitionsInfo>;
    soccerMatchStatuses: Array<SoccerMatchStatuses>;
    winnerTeamTypes: Array<WinnerTeamTypes>;
}

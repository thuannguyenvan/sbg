import { MatchScoreTypeModel } from '.';

export class MatchScoreModel {
    constructor(p: any = null) {
        if (p !== null && p !== undefined) {
            this.fullTime = p.fullTime;
            this.halfTime = p.halfTime;
            this.extraTime = p.extraTime;
            this.penalty = p.penalty;
            this.aggregate = p.aggregate;
        }
    }
    fullTime: MatchScoreTypeModel;
    halfTime: MatchScoreTypeModel;
    extraTime: MatchScoreTypeModel;
    penalty: MatchScoreTypeModel;
    aggregate: MatchScoreTypeModel;
}


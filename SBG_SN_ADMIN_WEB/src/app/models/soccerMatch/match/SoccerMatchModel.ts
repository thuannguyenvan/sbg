import {
    TeamsModel,
    ScoreMatch,
} from '.';
import { DateTimeHelper } from '../../../helpers/DateTimeHelper';
import { FormatDateTimeConstants } from '../../../constants';

export class SoccerMatchModel {
    constructor( p: any = null) {
        if (p !== null && p !== undefined) {
            this.soccerRoundTitle = p.soccerRoundTitle;
            this.soccerCompetitionTitle = p.soccerCompetitionTitle;
            this.soccerRoundId = p.soccerRoundId;
            this.status = p.status;
            this.startAt = p.startAt;
            this.finishedAt = p.finishedAt;
            this.title = p.title;
            this.place = p.place === null ? undefined : p.place;
            this.teamA = new TeamsModel(p.teamA);
            this.teamB = new TeamsModel(p.teamB);
            this.score = p.score === undefined || p.score === undefined ? new ScoreMatch() : new ScoreMatch(p.score);
            this.winner = p.winner === null ? undefined : p.winner;
            this.soccerCompetitionId = p.soccerCompetitionId;
            if (p.startAt === null || p.startAt === undefined) {
                this.startAt = p.startAt;
            } else {
                if (!DateTimeHelper.isValidDateTime(p.startAt)) {
                    if (DateTimeHelper.isValidDateTime(p.startAt, FormatDateTimeConstants.DD_MM_YYYY_HH_MM)) {
                        this.startAt = p.startAt;
                    } else {
                        this.startAt = DateTimeHelper.formatDateTime(new Date(p.startAt), FormatDateTimeConstants.DD_MM_YYYY_HH_MM);
                    }
                } else {
                    this.startAt = p.startAt;
                }
            }
            if (p.finishedAt === null || p.finishedAt === undefined) {
                this.finishedAt = p.finishedAt;
            } else {
                if (!DateTimeHelper.isValidDateTime(p.finishedAt)) {
                    if (DateTimeHelper.isValidDateTime(p.finishedAt, FormatDateTimeConstants.DD_MM_YYYY_HH_MM)) {
                        this.finishedAt = p.finishedAt;
                    } else {
                        this.finishedAt = DateTimeHelper.formatDateTime(new Date(p.finishedAt), FormatDateTimeConstants.DD_MM_YYYY_HH_MM);
                    }
                } else {
                    this.finishedAt = p.finishedAt;
                }
            }
        } else {
            this.soccerRoundId = null;
            this.status = null;
            this.title = null;
            this.place = null;
            this.teamA = new TeamsModel();
            this.teamB = new TeamsModel();
            this.score = new ScoreMatch();
            this.winner = null;
            this.soccerCompetitionId = null;
        }
    }
    soccerRoundTitle: string;
    soccerRoundId: string;
    status: string;
    startAt: string;
    finishedAt: string;
    title: string;
    place: string;
    teamA: TeamsModel;
    teamB: TeamsModel;
    winner: string;
    score: ScoreMatch;
    soccerCompetitionId: string;
    soccerCompetitionTitle: string;
}

export class MatchScoreTypeModel {
    constructor( p: any = null) {
        if (p !== null && p !== undefined) {
            this.teamA = p.teamA;
            this.teamB = p.teamB;
        }
    }
    teamA: number;
    teamB: number;
}


import {
  FullTime,
  HalfTime,
  ExtraTime,
  Penalty,
  Aggregate,
} from './';

export class ScoreMatch {
    constructor( p: any = null) {
        if (p !== null && p !== undefined) {
            this.fullTime = p.fullTime === undefined || p.fullTime === null ? new FullTime() : new FullTime(p.fullTime);
            this.halfTime = p.halfTime === undefined || p.halfTime === null ? new HalfTime() : new HalfTime(p.halfTime);
            this.extraTime =  p.extraTime === undefined || p.extraTime === null ? new ExtraTime() : new ExtraTime(p.extraTime);
            this.penalty =  p.penalty === undefined || p.penalty === null ? new Penalty() : new Penalty(p.penalty);
            this.aggregate =  p.aggregate === undefined || p.aggregate === null ? new Aggregate() : new Aggregate(p.aggregate);
        } else {
            this.fullTime = new FullTime();
            this.halfTime = new HalfTime();
            this.extraTime = new ExtraTime();
            this.penalty = new Penalty();
            this.aggregate =  new Aggregate();
        }
    }
        fullTime: FullTime;
        halfTime: HalfTime;
        extraTime: ExtraTime;
        penalty: Penalty;
        aggregate: Aggregate;
}

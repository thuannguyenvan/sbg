export class ExtraTime {
    constructor( p: any = null) {
        if (p !== null && p !== undefined) {
            this.teamA = p.teamA === null || p.teamA === '' ? undefined : p.teamA;
            this.teamB = p.teamB === null || p.teamB === '' ? undefined : p.teamB;
        } else {
            this.teamA = undefined;
            this.teamB = undefined;
        }
    }
    teamA: number;
    teamB: number;
}

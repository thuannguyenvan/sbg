export class WinnerTeamTypes {
    constructor( p: any = null) {
        if (p !== null && p !== undefined) {
            this.title = p.title;
            this.value = p.value;
        }
    }
    title: string;
    value: string;
}

export class MatchSocerDetailModel {
    constructor(p: any = null) {
        if (p !== null && p !== undefined) {
            this.scoreAGTeamA = p.scoreAGTeamA;
            this.scoreAGTeamB = p.scoreAGTeamB;
            this.penTeamA = p.penTeamA;
            this.penTeamB = p.penTeamB;
            this.scoreTeamA = p.scoreTeamA;
            this.scoreTeamB = p.scoreTeamB;
            this.showPen = p.showPen;
            this.showAG = p.showAG;
            this.showScore = p.showScore;
            this.showEmpty = p.showEmpty;
        }
    }
    scoreTeamA: string;
    scoreTeamB: string;
    scoreAGTeamA: string;
    scoreAGTeamB: string;
    penTeamA: string;
    penTeamB: string;
    showPen: boolean;
    showAG: boolean;
    showScore: boolean;
    showEmpty: boolean;
}

import {
    WinnerTeamTypes,
    SoccerMatchModel,
    SoccerMatchStatuses,
    TeamsModel,
} from '.';

export class SoccerMatchUpdateInfoModel {
    constructor( p: any = null) {
        if (p !== null && p !== undefined) {
            this.soccerMatch = new SoccerMatchModel(p.soccerMatch);
        this.soccerMatchStatuses = p.soccerMatchStatuses === null || p.soccerMatchStatuses === undefined ? []
        : p.soccerMatchStatuses.map((obj: any) => new SoccerMatchStatuses(obj));
        this.winnerTeamTypes = p.winnerTeamTypes === null || p.winnerTeamTypes === undefined ? []
        : p.winnerTeamTypes.map((obj: any) => new WinnerTeamTypes(obj));
        this.team = p.soccerCompetitionStageTeams === null || p.soccerCompetitionStageTeams === undefined ? []
        : p.soccerCompetitionStageTeams.map((obj: any) => new TeamsModel(obj));
        }
    }
    soccerMatchStatuses: Array<SoccerMatchStatuses>;
    soccerMatch: SoccerMatchModel;
    winnerTeamTypes: Array<WinnerTeamTypes>;
    team: Array<TeamsModel>;
}

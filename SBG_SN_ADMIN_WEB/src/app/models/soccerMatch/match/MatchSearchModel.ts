import { DateTimeHelper } from '../../../helpers/DateTimeHelper';
import { FormatDateTimeConstants } from '../../../constants';
import { SearchModel } from '../../shareds';

export class MatchSearchModel extends SearchModel {
    constructor(p: any = null) {
        super(p);
        if (p !== null && p !== undefined) {
            this.soccerCompetitionId = p.soccerCompetitionId;
            this.soccerRoundId = p.soccerRoundId;
            this.soccerMatchStatus = p.soccerMatchStatus;
            this.title = p.title;
            this.orderBy = p.orderBy;
            this.order = p.order;
            if (p.dateStartAt === null || p.dateStartAt === undefined) {
                this.dateStartAt = p.dateStartAt;
            } else {
                if (!DateTimeHelper.isValidDateTime(p.dateStartAt)) {
                    this.dateStartAt = DateTimeHelper.formatDateTime(new Date(p.dateStartAt), FormatDateTimeConstants.DD_MM_YYYY);
                } else {
                    this.dateStartAt = p.dateStartAt;
                }
            }
        }
    }
    soccerCompetitionId: string;
    soccerRoundId: string;
    soccerMatchStatus: string;
    dateStartAt: string;
    title: string;
    orderBy: string;
    order: string;
}

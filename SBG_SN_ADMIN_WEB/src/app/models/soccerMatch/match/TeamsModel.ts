export class TeamsModel {
    constructor( p: any = null) {
        if (p !== null && p !== undefined) {
            this.soccerCompetitionStageTeamId = p.soccerCompetitionStageTeamId;
            this.soccerCompetitionTeamId = p.soccerCompetitionTeamId;
            this.soccerTeamId = p.soccerTeamId;
            this.soccerCompetitionId = p.soccerCompetitionId;
            this.soccerCompetitionStageId = p.soccerCompetitionStageId;
            this.soccerRoundId = p.soccerRoundId;
            this.group = p.group;
            this.name = p.name;
            this.teamCode = p.teamCode;
            this.logo = p.logo;
        }
    }
    soccerCompetitionStageTeamId: string;
    soccerCompetitionTeamId: string;
    soccerTeamId: string;
    soccerCompetitionId: string;
    soccerCompetitionStageId: string;
    soccerRoundId: string;
    group: string;
    name: string;
    teamCode: string;
    logo: string;
}

export class RoundInfo {
    constructor( p: any = null) {
        if (p !== null && p !== undefined) {
            this.title = p.title;
            this.id = p.id;
            this.soccerCompetitionId = p.soccerCompetitionId;
            this.soccerCompetitionStageId = p.soccerCompetitionStageId;
            this.soccerCompetitionStageName = p.soccerCompetitionStageName;
            this.soccerCompetitionName = p.soccerCompetitionName;
            this.total = p.total;
            this.actions = p.actions;
        }
    }
    id: string;
    title: string;
    soccerCompetitionId: string;
    soccerCompetitionStageId: string;
    soccerCompetitionStageName: string;
    soccerCompetitionName: string;
    startAt: string;
    endAt: string;
    description: string;
    total: number;
    actions: string[];
}


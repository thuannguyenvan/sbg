export class RoundModel {
    constructor( p: any = null) {
        if (p !== null && p !== undefined) {
            this.id = p.id;
            this.title = p.title;
            this.soccerCompetitionStageId = p.soccerCompetitionStageId;
            this.soccerCompetitionId = p.soccerCompetitionId;
            this.startAt = p.startAt;
            this.endAt = p.endAt;
            this.description = p.decription;
        }
    }
    id: string;
    title: string;
    soccerCompetitionStageId: string;
    soccerCompetitionId: string;
    startAt: string;
    endAt: string;
    description: string;
}

import { SearchModel } from '../../shareds';

export class SoccerMatchSearchModel extends SearchModel {
    constructor(p: any = null) {
        super(p);
        if (p !== null && p !== undefined) {
            this.title = p.title;
            this.soccerCompetitionId = p.soccerCompetitionId;
            this.soccerCompetitionStageId = p.soccerCompetitionStageId;
            this.order = p.order;
            this.orderBy = p.orderBy;
        }
    }
    soccerCompetitionId: string;
    soccerCompetitionStageId: string;
    title: string;
    order: string;
    orderBy: string;
}

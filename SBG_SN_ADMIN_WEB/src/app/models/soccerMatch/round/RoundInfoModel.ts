import { DateTimeHelper } from '../../../helpers/DateTimeHelper';
import { FormatDateTimeConstants } from '../../../constants';

export class RoundInfoModel {
    constructor(p: any = null) {
        if (p !== null && p !== undefined) {
            this.id = p.id;
            this.title = p.title;
            this.soccerCompetitionStageId = p.soccerCompetitionStageId;
            this.soccerCompetitionStageName = p.soccerCompetitionStageName;
            this.soccerCompetitionName = p.soccerCompetitionName;
            this.soccerCompetitionId = p.soccerCompetitionId;
            if (p.startAt === null || p.startAt === undefined) {
                this.startAt = p.startAt;
            } else {
                if (!DateTimeHelper.isValidDateTime(p.startAt)) {
                    this.startAt = DateTimeHelper.formatDateTime(new Date(p.startAt), FormatDateTimeConstants.DD_MM_YYYY);
                } else {
                    this.startAt = p.startAt;
                }
            }
            if (p.endAt === null || p.endAt === undefined) {
                this.endAt = p.endAt;
            } else {
                if (!DateTimeHelper.isValidDateTime(p.endAt)) {
                    this.endAt = DateTimeHelper.formatDateTime(new Date(p.endAt), FormatDateTimeConstants.DD_MM_YYYY);
                } else {
                    this.endAt = p.endAt;
                }
            }
            this.description = p.description;
        }
    }
    id: string;
    title: string;
    soccerCompetitionStageId: string;
    soccerCompetitionStageName: string;
    soccerCompetitionId: string;
    soccerCompetitionName: string;
    startAt: string;
    endAt: string;
    description: string;
}

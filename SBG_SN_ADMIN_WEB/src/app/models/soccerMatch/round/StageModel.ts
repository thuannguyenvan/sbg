export class StageModel {
    constructor( p: any = null) {
        if (p !== null && p !== undefined) {
            this.title = p.title;
            this.soccerStageId = p.soccerStageId;
            this.teamInfos = p.teamInfos;
            this.standAlone = p.standAlone;
            this.id = p.id;
        }
    }
    id: string;
    title: string;
    soccerStageId: string;
    teamInfos: string;
    standAlone: boolean;
}

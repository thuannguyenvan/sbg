import {
    CompetitionsInfo,
    SoccerMatchStatuses,
    LinklivestreamQualityModel,
    ConfigAdsModel,
    SoccerMatchStreamInfoModel,
    MatchModel,
    RoundModel,
} from '..';

export class CreateLivestreamInfoModel {
    constructor(p: any = null) {
        if (p !== null && p !== undefined) {
            this.soccerCompetitions = p.soccerCompetitions === undefined  || p.soccerCompetitions === null
                ? [] : p.soccerCompetitions.map((d: any) => new CompetitionsInfo(d));
            this.livestreamStatuses = p.livestreamStatuses === undefined  || p.livestreamStatuses === null
                ? [] : p.livestreamStatuses.map((d: any) => new SoccerMatchStatuses(d));
            this.livestreamVideoUnits = p.livestreamVideoUnits === undefined || p.livestreamVideoUnits === null
                ? [] : p.livestreamVideoUnits.map((d: any) => new LinklivestreamQualityModel(d));
            this.soccerMatchs = p.soccerMatchs === undefined || p.soccerMatchs === null
                ? [] : p.soccerMatchs.map((d: any) => new MatchModel(d));
            this.soccerRounds = p.soccerRounds === undefined || p.soccerRounds === null
                ? [] : p.soccerRounds.map((d: any) => new RoundModel(d));
            this.soccerMatchStream = p.soccerMatchStream === undefined || p.soccerMatchStream === null
                ? undefined : new SoccerMatchStreamInfoModel(p.soccerMatchStream);
            this.adsConfigurations = p.adsConfigurations === undefined  && p.adsConfigurations === null
                ? [] : p.adsConfigurations.map((d: any) => new ConfigAdsModel(d));
        }
    }
    soccerCompetitions: Array<CompetitionsInfo>;
    livestreamStatuses: Array<SoccerMatchStatuses>;
    livestreamVideoUnits: Array<LinklivestreamQualityModel>;
    adsConfigurations: Array<ConfigAdsModel>;
    soccerMatchs: Array<MatchModel>;
    soccerRounds: Array<RoundModel>;
    soccerMatchStream: SoccerMatchStreamInfoModel;
}

import {
    CompetitionsInfo,
    SoccerMatchStatuses,
} from '..';

export class SearchInfoLTModel {
    constructor(private props: any = null) {
        if (props !== null && props !== undefined) {
            this.soccerCompetitions = props.soccerCompetitions === undefined  || props.soccerCompetitions === null
                ? [] : props.soccerCompetitions.map((d: any) => new CompetitionsInfo(d));
            this.livestreamStatuses = props.livestreamStatuses === undefined  || props.livestreamStatuses === null
                ? [] : props.livestreamStatuses.map((d: any) => new SoccerMatchStatuses(d));
        }
    }
    soccerCompetitions: Array<CompetitionsInfo>;
    livestreamStatuses: Array<SoccerMatchStatuses>;
}

import { DateTimeHelper } from '../../helpers/DateTimeHelper';
import { FormatDateTimeConstants } from '../../constants';
import {
    LivestreamUrlsModel,
} from '..';

export class LivestreamCreateEditModel {
    constructor(private p: any = null) {
        if (p !== null && p !== undefined) {
            this.id = p.id;
            this.roundName = p.roundName;
            this.chatUrl = p.chatUrl;
            this.status = p.status;
            this.tags =  p.tags === undefined || p.tags === null || p.tags === '' ? new Array<string>() : p.tags;
            this.urlSlug = p.urlSlug;
            this.description = p.description;
            this.matchName = p.matchName;
            this.statusName = p.statusName;
            this.title = p.title;
            this.soccerCompetitionId = p.soccerCompetitionId;
            this.soccerRoundId = p.soccerRoundId;
            this.soccerMatchId = p.soccerMatchId;
            this.actions = p.actions;
            this.soccerMatchTitle = p.soccerMatchTitle;
            this.startAt = p.startAt;
            this.statusTitle = p.statusTitle;
            this.adsConfigurationId = p.adsConfigurationId;
            this.soccerCompetitionTitle = p.soccerCompetitionTitle;
            this.livestreamUrls = p.livestreamUrls === undefined && p.livestreamUrls === null
            ? [] : p.livestreamUrls.map((d: any) => new LivestreamUrlsModel(d));
            if (p.startAt === null || p.startAt === undefined) {
                this.startAt = p.startAt;
            } else {
                if (!DateTimeHelper.isValidDateTime(p.startAt)) {
                    this.startAt = DateTimeHelper.formatDateTime(new Date(p.startAt), FormatDateTimeConstants.DD_MM_YYYY_HH_MM);
                } else {
                    this.startAt = p.startAt;
                }
            }
        } else {
            this.tags = [];
        }
    }
    id: string;
    roundName: string;
    adsConfigurationId: string;
    matchName: string;
    statusName: string;
    title: string;
    urlSlug: string;
    soccerCompetitionId: string;
    soccerRoundId: string;
    soccerMatchId: string;
    tags: Array<string>;
    actions: string[];
    soccerMatchTitle: string;
    startAt: string;
    statusTitle: string;
    status: string;
    soccerCompetitionTitle: string;
    chatUrl: string;
    description: string;
    livestreamUrls: Array<LivestreamUrlsModel>;
}

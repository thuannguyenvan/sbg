export class SoccerMatchStreamInfoModel {
    constructor(p: any = null) {
        if (p !== null && p !== undefined) {
            this.soccerCompetitionId = p.soccerCompetitionId;
            this.soccerCompetitionTitle = p.soccerCompetitionTitle;
            this.soccerMatchId = p.soccerMatchId;
            this.soccerMatchTitle = p.soccerMatchTitle;
            this.soccerRoundId = p.soccerRoundId;
            this.soccerRoundTitle = p.soccerRoundTitle;
        }
    }
    soccerCompetitionId: string;
    soccerCompetitionTitle: string;
    soccerMatchId: string;
    soccerMatchTitle: string;
    soccerRoundId: string;
    soccerRoundTitle: string;
}

export class MatchModel {
    constructor(private props: any = null) {
        if (props !== null && props !== undefined) {
            this.soccerMatchId = props.soccerMatchId;
            this.title = props.title;
        }
    }
    soccerMatchId: string;
    title: string;
}

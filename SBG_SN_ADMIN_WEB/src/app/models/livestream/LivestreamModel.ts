import { DateTimeHelper } from '../../helpers/DateTimeHelper';
import { FormatDateTimeConstants } from '../../constants';

export class LivestreamModel {
    constructor(private p: any = null) {
        if (p !== null && p !== undefined) {
            this.id = p.id;
            this.roundName = p.roundName;
            this.matchName = p.matchName;
            this.statusName = p.statusName;
            this.dateStart = p.dateStart;
            this.title = p.title;
            this.soccerCompetitionId = p.soccerCompetitionId;
            this.soccerRoundId = p.soccerRoundId;
            this.soccerMatchId = p.soccerMatchId;
            this.actions = p.actions;
            this.soccerMatchTitle = p.soccerMatchTitle;
            this.startAt = p.startAt;
            this.total = p.total;
            this.adsConfigurationId = p.adsConfigurationId;
            this.statusTitle = p.statusTitle;
            this.soccerCompetitionTitle = p.soccerCompetitionTitle;
            if (p.startAt === null || p.startAt === undefined) {
                this.startAt = p.startAt;
            } else {
                if (!DateTimeHelper.isValidDateTime(p.startAt)) {
                    this.startAt = DateTimeHelper.formatDateTime(new Date(p.startAt), FormatDateTimeConstants.DD_MM_YYYY_HH_MM);
                } else {
                    this.startAt = p.startAt;
                }
            }
        }
    }
    id: string;
    roundName: string;
    total: number;
    matchName: string;
    statusName: string;
    dateStart: Date;
    title: string;
    soccerCompetitionId: string;
    soccerRoundId: string;
    soccerMatchId: string;
    actions: string[];
    soccerMatchTitle: string;
    startAt: string;
    statusTitle: string;
    soccerCompetitionTitle: string;
    adsConfigurationId: string;
}

import {
    LinklivestreamQualityModel,
} from '..';

export class LivestreamUrlsModel {
    constructor(private p: any = null) {
        if (p !== null && p !== undefined) {
            this.label = p.label;
            this.link = p.link;
            this.quanlity = p.quanlity;
            this.quanlityTitle = p.quanlityTitle;
        }
    }
    label: string;
    link: string;
    quanlity: string;
    quanlityTitle: string;
}

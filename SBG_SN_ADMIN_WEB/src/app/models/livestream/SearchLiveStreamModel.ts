import { DateTimeHelper } from '../../helpers/DateTimeHelper';
import { FormatDateTimeConstants } from '../../constants';

export class SearchLiveStreamModel {
    constructor(private p: any = null) {
        if (p !== null && p !== undefined) {
            this.soccerCompetitionId = p.soccerCompetitionId === null || p.soccerCompetitionId === ''
                ? undefined : p.soccerCompetitionId;
            this.soccerRoundId = p.soccerRoundId === null || p.soccerRoundId === ''
                ? undefined : p.soccerRoundId;
            this.soccerMatchId = p.soccerMatchId === null || p.soccerMatchId === ''
                ? undefined : p.soccerMatchId;
            this.livestreamStatus = p.livestreamStatus === null || p.livestreamStatus === ''
                ? undefined : p.livestreamStatus;
            this.page = p.page;
            this.title = p.title;
            this.size = p.size;
            this.orderBy = p.orderBy;
            this.order = p.order;
            if (p.dateStartAt === null || p.dateStartAt === undefined) {
                this.dateStartAt = p.startAt;
            } else {
                if (!DateTimeHelper.isValidDateTime(p.dateStartAt)) {
                    this.dateStartAt = DateTimeHelper.formatDateTime(new Date(p.dateStartAt), FormatDateTimeConstants.DD_MM_YYYY_HH_MM);
                } else {
                    this.dateStartAt = p.dateStartAt;
                }
            }
        }
    }
    soccerCompetitionId: string;
    soccerRoundId: string;
    soccerMatchId: string;
    livestreamStatus: string;
    dateStartAt: string;
    title: string;
    page: number;
    size: number;
    orderBy: string;
    order: string;
}

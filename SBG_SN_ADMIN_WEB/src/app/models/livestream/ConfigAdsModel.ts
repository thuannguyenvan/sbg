export class ConfigAdsModel {
    constructor(p: any = null) {
        if (p !== null && p !== undefined) {
            this.id = p.id;
            this.title = p.title;
            this.description = p.description;
            this.adsData = p.adsData;
            this.isActive = p.isActive;
        }
    }
    id: string;
    title: string;
    description: string;
    isActive: string;
    adsData: string;
}


import { FormatDateTimeConstants } from '../../constants';
import { DateTimeHelper } from '../../helpers/DateTimeHelper';

export class CompetitionCreateEditModel {
    constructor(p: any = null) {
        if (p !== null && p !== undefined) {
            this.title = p.title;
            this.description = p.description;
            if (p.startAt === null || p.startAt === undefined) {
                this.startAt = p.startAt;
            } else {
                if (!DateTimeHelper.isValidDateTime(p.startAt)) {
                    this.startAt = DateTimeHelper.formatDateTime(new Date(p.startAt), FormatDateTimeConstants.DD_MM_YYYY);
                } else {
                    this.startAt = p.startAt;
                }
            }
            if (p.endAt === null || p.endAt === undefined) {
                this.endAt = p.endAt;
            } else {
                if (!DateTimeHelper.isValidDateTime(p.endAt)) {
                    this.endAt = DateTimeHelper.formatDateTime(new Date(p.endAt), FormatDateTimeConstants.DD_MM_YYYY);
                } else {
                    this.endAt = p.endAt;
                }
            }
            this.soccerCompetitionBaseId = p.soccerCompetitionBaseId;
            this.season = p.season;
            this.place = p.place;
            this.stages = p.stages;
            this.id = p.id;
        }
    }
    id: string;
    stages: Array<string>;
    soccerCompetitionBaseId: string;
    season: string;
    title: string;
    description: string;
    startAt: string;
    endAt: string;
    place: string;
}

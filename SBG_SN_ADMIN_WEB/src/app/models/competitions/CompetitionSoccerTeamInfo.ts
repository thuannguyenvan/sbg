import { SoccerCompetitionTeams } from './SoccerCompetitionTeams';

export class CompetitionSoccerTeamInfo {
    constructor(private p: any = null) {
        if (p !== null && p !== undefined) {
            this.id = p.id;
            this.soccerCompetitionId = p.soccerCompetitionId;
            this.soccerCompetitionTitle = p.soccerCompetitionTitle;
            this.soccerCompetitionTeams =
                this.p.soccerCompetitionTeams === null || this.p.soccerCompetitionTeams === undefined ? []
                    : this.p.soccerCompetitionTeams.map((obj: any) => new SoccerCompetitionTeams(obj));
        }
    }
    id: string;
    soccerCompetitionId: string;
    soccerCompetitionTitle: string;
    soccerCompetitionTeams: Array<SoccerCompetitionTeams>;
}

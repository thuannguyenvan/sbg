export class CompetitionBaseModel {
    constructor(private props: any = null) {
        if (props !== null && props !== undefined) {
            this.title = props.title;
            this.des = props.des;
            this.key = props.key;
            this.id = props.id;
        }
    }
    id: string;
    title: number;
    des: string;
    key: string;
}

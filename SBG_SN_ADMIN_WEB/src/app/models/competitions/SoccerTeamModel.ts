import { SearchModel } from '../shareds';

export class SoccerTeamModel extends SearchModel {
    constructor(private p: any = null) {
        super(p);
        if (p !== null && p !== undefined) {
            this.id = p.id;
            this.name = p.name;
            this.teamCode = p.teamCode;
            this.logo = p.logo;
            this.isClub = p.isClub;
            this.total = p.total;
            this.soccerTeamId = p.soccerTeamId;
        }
    }
    id: string;
    name: string;
    teamCode: boolean;
    soccerTeamId: boolean;
    logo: boolean;
    isClub: boolean;
    total: number;
}

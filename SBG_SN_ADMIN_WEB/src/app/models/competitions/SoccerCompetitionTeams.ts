export class SoccerCompetitionTeams {
    constructor(private p: any = null) {
        if (p !== null && p !== undefined) {
            this.id = p.id;
            this.soccerCompetitionId = p.soccerCompetitionId;
            this.soccerTeamId = p.soccerTeamId;
            this.name = p.name;
            this.teamCode = p.teamCode;
            this.logo = p.logo;
        }
    }
    id: string;
    soccerCompetitionId: string;
    soccerTeamId: string;
    name: boolean;
    teamCode: boolean;
    logo: number;
}

import { SearchModel } from '..';

export class CompetitionSearch extends SearchModel {
    constructor(p: any = null) {
        super(p);
        if (p !== null && p !== undefined) {
            this.title = p.title;
            this.orderBy = p.orderBy;
            this.order = p.order;
        }
    }
    title: string;
    orderBy: string;
    order: string;
}

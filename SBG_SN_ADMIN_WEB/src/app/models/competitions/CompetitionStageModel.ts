export class CompetitionStageModel {
    constructor(private p: any = null) {
        if (p !== null && p !== undefined) {
            this.id = p.id;
            this.title = p.title;
            this.checked = p.checked;
            this.standalone = p.standalone;
            this.soccerStageId = p.soccerStageId;
        }
    }
    id: string;
    title: string;
    checked: boolean;
    standalone: boolean;
    soccerStageId: string;
}

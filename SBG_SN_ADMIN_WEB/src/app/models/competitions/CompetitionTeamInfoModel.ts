import { CompetitionTeamModel } from '.';

export class CompetitionTeamInfoModel {
    constructor(p: any = null) {
        if (p !== null && p !== undefined) {
            this.soccerCompetitionId = p.soccerCompetitionId;
            this.soccerCompetitionTitle = p.soccerCompetitionTitle;

            this.soccerCompetitionTeams = p === null || p === undefined ||
            p.soccerCompetitionTeams === null || p.soccerCompetitionTeams === undefined ? []
            : p.soccerCompetitionTeams.map((obj: any) => new CompetitionTeamModel(obj));
        }
    }
    soccerCompetitionId: string;
    soccerCompetitionTitle: string;
    soccerCompetitionTeams: Array<CompetitionTeamModel>;
}

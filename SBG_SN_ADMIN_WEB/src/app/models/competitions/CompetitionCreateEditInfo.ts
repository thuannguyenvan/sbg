import { CompetitionBaseModel } from './CompetitionBaseModel';
import {
    CompetitionStageModel,
    CompetitionsInfo
} from '.';

export class CompetitionCreateEditInfo {
    constructor(private props: any = null) {
        if (props !== null && props !== undefined) {
            this.title = props.title;
            this.des = props.des;
            this.startAt = props.startAt;
            this.endAt = props.endAt;

            this.competitionBases = props === null || props === undefined ||
                props.competitionBases === null || props.competitionBases === undefined ? []
                : props.competitionBases.map((obj: any) => new CompetitionBaseModel(obj));

            this.stages = props === null || props === undefined ||
                props.stages === null || props.stages === undefined ? []
                : props.stages.map((obj: any) => new CompetitionStageModel(obj));

            this.competition = props.competition;
        }
    }
    competitionBases: Array<CompetitionBaseModel>;
    stages: Array<CompetitionStageModel>;
    competition: CompetitionsInfo;
    title: string;
    des: string;
    startAt: string;
    endAt: string;
}

export class CompetitionTeamModel {
    constructor(p: any = null) {
        if (p !== null && p !== undefined) {
            this.id = p.id;
            this.name = p.name;
            this.soccerTeamId = p.soccerTeamId;
            this.teamCode = p.teamCode;
            this.soccerCompetitionId = p.soccerCompetitionId;
            this.logo = p.logo;
            this.total = p.total;
        }
    }
    id: string;
    name: string;
    soccerTeamId: string;
    teamCode: string;
    soccerCompetitionId: string;
    logo: string;
    total: number;
}

import { DateTimeHelper } from '../../helpers/DateTimeHelper';
import { FormatDateTimeConstants } from '../../constants';
import { CompetitionStageModel } from '.';

export class CompetitionsInfo {
    constructor(private p: any = null) {
        if (p !== null && p !== undefined) {
            this.id = p.id;
            this.title = p.title;
            this.logo = p.logo;
            this.season = p.season;
            this.place = p.place;
            this.total = p.total;
            this.actions = p.actions;
            this.timeAt = p.timeAt;
            this.description = p.description;
            if (p.startAt === null || p.startAt === undefined) {
                this.startAt = p.startAt;
            } else {
                if (!DateTimeHelper.isValidDateTime(p.startAt)) {
                    this.startAt = DateTimeHelper.formatDateTime(new Date(p.startAt), FormatDateTimeConstants.DD_MM_YYYY);
                } else {
                    this.startAt = p.startAt;
                }
            }
            if (p.endAt === null || p.endAt === undefined) {
                this.endAt = p.endAt;
            } else {
                if (!DateTimeHelper.isValidDateTime(p.endAt)) {
                    this.endAt = DateTimeHelper.formatDateTime(new Date(p.endAt), FormatDateTimeConstants.DD_MM_YYYY);
                } else {
                    this.endAt = p.endAt;
                }
            }
            this.soccerCompetitionBaseId = p.soccerCompetitionBaseId;
            this.stages = this.p.stages === null || this.p.stages === undefined ? []
                : this.p.stages.map((obj: any) => new CompetitionStageModel(obj));
        }
    }
    id: string;
    title: string;
    startAt: string;
    endAt: string;
    timeAt: string;
    logo: string;
    season: string;
    place: string;
    total: number;
    actions: string[];
    soccerCompetitionBaseId: string;
    description: string;
    stages: Array<CompetitionStageModel>;
}

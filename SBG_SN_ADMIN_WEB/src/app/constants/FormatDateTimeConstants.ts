﻿export const FormatDateTimeConstants = {
    DD_MM_YYYY_HH_MM: 'DD/MM/YYYY HH:mm',
    DD_MM_YYYY: 'DD/MM/YYYY',
    MM_DD_YYYY: 'MM/DD/YYYY',
    HH_MM: 'HH:mm',
};

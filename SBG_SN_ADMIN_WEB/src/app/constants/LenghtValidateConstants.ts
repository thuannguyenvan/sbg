﻿export const LenghtValidateConstants = {
    STRING_MEDIUM: 255,
    STRING_MIN: 80,
    STRING_MAX: 1000,
    NUMBER_3: 3,
};

﻿export const OptionConstants = {
    dateFullOptions: {
        bigBanner: true,
        timePicker: true,
        format: 'DD/MM/YYYY HH:mm',
        defaultOpen: false,
        closeOnSelect: true
    },
    onlyDateOptions: {
        bigBanner: true,
        timePicker: false,
        format: 'DD/MM/YYYY',
        defaultOpen: false,
        closeOnSelect: true,
        showButtonClear: true
    }
};

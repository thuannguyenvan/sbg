﻿export const RouterConstants = {
    dashboard: '/dashboard',
    ssoCallback: '/sso-callback',
    auths: {
        login: '/login',
        redirect_url: '/login?redirect=',
    },
    competitions: {
        create: 'livestream/soccer-competitions/create',
        edit: 'livestream/soccer-competitions/edit/',
        config: 'livestream/soccer-competitions/config/',
        list: 'livestream/soccer-competitions',
        stage: 'livestream/soccer-competitions/manager-stage',
    },
    MANAGER_ROUND: {
        LIST: 'livestream/soccer-match/rounds',
        ADD_ROUND: '/livestream/soccer-match/add-round',
        EDIT_ROUND: '/livestream/soccer-match/edit-round'
    },
    MANAGER_MATCH: {
        CREATE_MATCH: '/livestream/soccer-match/create-match',
        Edit_MATCH: '/livestream/soccer-match/edit-match',
        LIST_MATCH: '/livestream/soccer-match',
        ADD_MATCH: '/livestream/soccer-match/add-match',
        EDIT_MATCH: '/livestream/soccer-match/edit-match/',
        ADD_LIVESTREAM: '/livestream/soccer-match/create-livestream',
        EDIT_LIVESTREAM: '/livestream/soccer-match/edit-livestream',
    },
    MANAGER_LIVESTREAM: {
        CREATE_LIVESTREAM: '/livestream/soccer-match/create-livestream',
        EDIT_LIVESTREAM: '/livestream/soccer-match/edit-livestream/',
        LIST_STREAM: '/livestream/soccer-match/list-livestream',
    },
    MANAGER_PARTNER: {
        CREATE_PARTNER: 'ads/partners/create',
        EDIT_PARTNER: 'ads/partners/edit',
        LIST_PARTNER: 'ads/partners/list',
    },
    MANAGER_ADVERTISING: {
        CREATE_ADVERTISING: 'ads/advertisings/create',
        EDIT_ADVERTISING: 'ads/advertisings/edit',
        LIST_ADVERTISING: 'ads/advertisings/list',
    },
    MANAGER_CONFIG_ADVERTISING: {
        CREATE_ADVERTISING_CONFIG: 'ads/advertisingsConfig/create',
        EDIT_ADVERTISING_CONFIG: 'ads/advertisingsConfig/edit',
        LIST_ADVERTISING_CONFIG: 'ads/advertisingsConfig/list',
    }
};

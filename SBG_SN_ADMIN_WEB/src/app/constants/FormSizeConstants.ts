﻿export const FormSizeConstants = {
    WIDTH_MIN: '800px',
    WIDTH_MEDIUM: '1000px',
    WIDTH_MAX: '1200px',
};

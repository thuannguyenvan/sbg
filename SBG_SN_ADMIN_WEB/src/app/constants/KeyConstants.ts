﻿export const KeyConstants = {
    TOKEN: 'AccessToken',
    LANGUAGE: 'lang',
    REDIRECT: 'redirect',
    USER_INFO: 'UserInfo',
    ACTIVE_MENU: 'ActiveMenu'
};

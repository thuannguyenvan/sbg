import { NgModule } from '@angular/core';
import * as moment from 'moment';
@NgModule({
})
export class DateTimeHelper {
    public static formatDate(dateTime: string, formatInPutStr: string = '', formatOutPutStr: string = 'DD/MM/YYYY'): string {
        let date: moment.MomentInput;
        if (formatInPutStr === '' || formatInPutStr === undefined || formatInPutStr === null) {
            date = new Date(dateTime);
        } else {
            date = moment(dateTime, formatInPutStr);
        }
        if (moment(date).isValid()) {
            return moment(date).format(formatOutPutStr);
        } else {
            return dateTime;
        }
    }

    public static formatDateTime(dateTime: Date, formatOutPutStr: string = 'DD/MM/YYYY'): string {
        return moment(dateTime).format(formatOutPutStr);
    }

    public static onConvertTimeFormat(time: any) {
        let hours = Number(time.match(/^(\d+)/)[1]);
        const minutes = Number(time.match(/:(\d+)/)[1]);
        const AMPM = time.match(/\s(.*)$/)[1];
        if (AMPM === 'pm' && hours < 12) {
            hours = hours + 12;
        }
        if (AMPM === 'am' && hours === 12) {
            hours = hours - 12;
        }
        let sHours = hours.toString();
        let sMinutes = minutes.toString();
        if (hours < 10) {
            sHours = '0' + sHours;
        }
        if (minutes < 10) {
            sMinutes = '0' + sMinutes;
        }
        return sHours + ':' + sMinutes;
    }

    public static isValidDateTime(dateStr: string, formatString: string = 'DD/MM/YYYY'): boolean {
        return moment(dateStr, formatString, true).isValid();
    }

    public static createDate(dateTime: string, formatString: string = 'DD/MM/YYYY'): Date {
        const dateStr = this.formatDate(dateTime, formatString);
        return moment(dateStr, formatString).toDate();
    }

    public static compareDate(dateFrom: Date, dateTo: Date): number {
        if (moment(dateFrom).isSame(dateTo)) {
            return 0;
        } else if (moment(dateFrom).isBefore(dateTo)) {
            return 1;
        } else {
            return -1;
        }
    }

    public static addDays(dateFrom: Date, duration: number): Date {
        return moment(dateFrom).add(duration, 'd').toDate();
    }

    public static getOptionDateFull() {
        return {
            bigBanner: true,
            timePicker: true,
            format: 'DD/MM/YYYY HH:mm',
            formatValidate: 'D/M/YYYY HH:mm',
            defaultOpen: false,
            closeOnSelect: true
        };
    }

    public static getOptionDateOnly() {
        return {
            bigBanner: true,
            timePicker: false,
            format: 'DD/MM/YYYY',
            formatValidate: 'D/M/YYYY',
            defaultOpen: false,
            closeOnSelect: true,
            showButtonClear: true
        };
    }
}

import { Injectable, forwardRef, Inject } from '@angular/core';
import Swal from 'sweetalert2';
import { TranslateService } from '../services';
import { TranslateKeyConstants } from '../constants';

@Injectable()
export class AlertHelper {
    constructor(
        @Inject(forwardRef(() => TranslateService)) public tran: TranslateService
    ) {
    }

    confirmWarning(text: string, callback: any) {
        Swal.fire({
            text: text,
            type: 'warning',
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonText: this.tran.translate(TranslateKeyConstants.SHARE.BTN_CONFIRM),
            cancelButtonText: this.tran.translate(TranslateKeyConstants.SHARE.BTN_CANCEL)
        }).then((r) => {
            callback(r);
        });
    }
}

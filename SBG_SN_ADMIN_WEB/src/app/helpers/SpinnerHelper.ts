import { Injectable } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
declare var $: any;

@Injectable()
export class SpinnerHelper {

    constructor(private spinner: NgxSpinnerService,
    ) { }

    show() {
        this.spinner.show();
    }

    hide() {
        setTimeout(() => {
            this.spinner.hide();
            const body = document.body;
            if (body !== null && body !== undefined) {
                body.click();
            }
        }, 500);
    }
}

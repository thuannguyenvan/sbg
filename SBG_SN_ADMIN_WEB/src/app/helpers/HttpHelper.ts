﻿import {
    Injectable,
    Inject,
    forwardRef
} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { throwError } from 'rxjs';
import {
    ErrorItem,
    ErrorResult
} from '../models';
import { ErrorType } from '../enums';
import {
    map,
    catchError
} from 'rxjs/operators';
import { Router } from '@angular/router';
import {
    RouterConstants,
    KeyConstants,
    TranslateKeyConstants
} from '../constants';
import { CookieService } from 'ngx-cookie';
import { TranslateService } from '../services';
import { ToastrHelper, SpinnerHelper } from '.';
import { Store } from '@ngrx/store';
import { AppState } from '../AppState';
import { SuccessActiveRouterOutletAction } from '../modules/shared/actions';

@Injectable()
export class HttpHelper {

    private lang = '';
    constructor(
        private store: Store<AppState>,
        private http: HttpClient,
        private router: Router,
        private cookieService: CookieService,
        @Inject(forwardRef(() => ToastrHelper)) public toastr: ToastrHelper,
        @Inject(forwardRef(() => SpinnerHelper)) public spinner: SpinnerHelper,
        @Inject(forwardRef(() => TranslateService)) public tran: TranslateService
    ) {
        this.lang = this.cookieService.get(KeyConstants.LANGUAGE);
        if (this.lang === undefined || this.lang === null || this.lang === '') {
            this.lang = 'vi';
        }
    }

    public extractData(res: Response | any) {
        if (res instanceof Response) {
            return res.json() || null;
        }
        return res;
    }

    public handleError(res: Response | any) {
        let errorResult = new ErrorResult();
        try {
            if (res.status === 404) {
                errorResult.type = 0;
                errorResult.message = this.tran.translate(TranslateKeyConstants.SHARE.MESSAGE_ERROR);
            } else {
                if (res.status === 401) {
                    errorResult = undefined;
                    this.spinner.hide();
                    this.toastr.showError(res.error);
                    this.store.dispatch(new SuccessActiveRouterOutletAction({ data: true }));
                    this.router.navigate([RouterConstants.auths.login]);
                } else {
                    const error = res.error;
                    if (error.success !== undefined && error.success !== null) {

                        const messages = (error.messages === null || error.messages === undefined ? null
                            : error.messages.map((data: any) => data));
                        const errors = error.errors === null || error.errors === undefined ? null
                            : error.errors.map((d: any) => new ErrorItem({
                                key: d.key,
                                message: d.message
                            }));

                        errorResult = new ErrorResult({
                            type: error.type,
                            message: error.message,
                            messages: messages,
                            errors: errors,
                            time: new Date().getTime()
                        });

                    } else {
                        errorResult = new ErrorResult({
                            message: this.tran.translate(TranslateKeyConstants.SHARE.MESSAGE_ERROR),
                            type: ErrorType.ERROR,
                            time: new Date().getTime()
                        });
                    }
                }
            }
        } catch (e) {
            if (res.status === 401) {
                errorResult = undefined;
                this.toastr.showError(res.error);
                this.store.dispatch(new SuccessActiveRouterOutletAction({ data: true }));
                this.router.navigate([RouterConstants.auths.login]);
            } else {
                errorResult = new ErrorResult({
                    message: res.error,
                    type: ErrorType.ERROR,
                    time: new Date().getTime()
                });
            }
        }

        return throwError(errorResult);
    }

    public get(url: string) {
        url = this.getUrl(url);
        return this.http.get<any>(url).pipe(
            map(res => {
                return this.extractData(res);
            }), catchError(error => {
                return this.handleError(error);
            }));
    }

    public post(url: string, model: any, options: any = null) {
        url = this.getUrl(url);
        if (options === null || options === undefined) {
            return this.http.post<any>(url, model).pipe(
                map(res => {
                    return this.extractData(res);
                }),
                catchError(error => {
                    return this.handleError(error);
                }));
        } else {
            return this.http.post<any>(url, model, options).pipe(
                map(res => {
                    return this.extractData(res);
                }), catchError(error => {
                    return this.handleError(error);
                }));
        }
    }

    public put(url: string, model: any, options: any = null) {
        url = this.getUrl(url);
        if (options === null || options === undefined) {
            return this.http.put<any>(url, model).pipe(
                map(res => {
                    return this.extractData(res);
                }), catchError(error => {
                    return this.handleError(error);
                }));
        } else {
            return this.http.put<any>(url, model, options).pipe(
                map(res => {
                    return this.extractData(res);
                }), catchError(error => {
                    return this.handleError(error);
                }));
        }
    }

    public delete(url: string) {
        url = this.getUrl(url);
        return this.http.delete<any>(url).pipe(
            map(res => {
                return this.extractData(res);
            }), catchError(error => {
                return this.handleError(error);
            }));
    }

    private getUrl(url: string) {

        if (url.indexOf('?') > -1) {
            url = url + '&lang=' + this.lang;
        } else {
            url = url + '?lang=' + this.lang;
        }
        return url;
    }
}

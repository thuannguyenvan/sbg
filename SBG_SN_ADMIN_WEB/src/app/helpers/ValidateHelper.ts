﻿import { Injectable } from '@angular/core';
import {
    FormGroup,
    FormControl,
} from '@angular/forms';
import {
    PatternConstants,
} from '../constants';
import { ErrorResult } from '../models';

@Injectable()
export class ValidateHelper {
    constructor(
    ) {
    }

    validateByServer(formGroup: FormGroup, errorResult: ErrorResult): any {
        let error = {};
        if (errorResult.errors !== undefined && errorResult.errors !== null && errorResult.errors.length > 0) {
            let strJson = '';
            errorResult.errors.forEach(e => {
                let control = formGroup.get(e.key);
                let isDate = false;
                if (control === undefined || control === null) {
                    // Trường hợp date time
                    control = formGroup.get(e.key + 'Date');
                    isDate = true;
                }

                if (control !== undefined && control !== null) {
                    control.setErrors({
                        validateServer: true
                    });
                    control.markAsTouched();
                    strJson += '"' + (isDate === false ? e.key : e.key + 'Date') + '":' + '"' + e.message.replace(/"/g, '\\"') + '",';
                }
            });

            strJson = '{' + strJson.substr(0, strJson.length - 1) + '}';
            const objectJson = JSON.parse(strJson);
            error = objectJson;
        }
        return error;
    }

    validateAllFormFields(formGroup: FormGroup) {
        this.resetValidateBase(formGroup);
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsPristine();
                control.markAsTouched({ onlySelf: true });
                control.updateValueAndValidity();
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }

    private resetValidateBase(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsPristine();
                control.markAsUntouched();
                control.updateValueAndValidity();
            } else if (control instanceof FormGroup) {
                this.resetValidateBase(control);
            }
        });
    }

    resetValidate(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsPristine();
                control.markAsUntouched();
                control.updateValueAndValidity();
            } else if (control instanceof FormGroup) {
                this.resetValidate(control);
            }
        });
    }

    // Validates numbers
    numberValidator(number: any): any {
        if (number.pristine) {
            return null;
        }
        const regex = new RegExp(PatternConstants.NUMBER);
        number.markAsTouched();
        if (regex.test(number.value)) {
            return null;
        }
        return {
            invalidNumber: true
        };
    }

    // Validates phone numbers
    phoneValidator(numberPhone: any): any {
        if (numberPhone.pristine) {
            return null;
        }
        const regex = new RegExp(PatternConstants.PHONE);
        numberPhone.markAsTouched();
        if (regex.test(numberPhone.value)) {
            return null;
        }
        return {
            numberPhone: true
        };
    }

    // Validates date time
    dateTimeValidator(dateTime: any): any {
        return null;
    }

    // Validate url
    invalidUrl(urlInput: any) {
        if (urlInput.value === undefined || urlInput.value === null || urlInput.value === '') {
            return null;
        }
        const regex = new RegExp(PatternConstants.URL);
        if (regex.test(urlInput.value)) {
            return null;
        }
        // return {
        //     invalidUrl: true
        // };
        return null;
    }
}

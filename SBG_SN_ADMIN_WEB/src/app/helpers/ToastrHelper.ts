import { Injectable } from '@angular/core';
import { ErrorType } from '../enums';
import { ErrorResult } from '../models';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '../services';
import { TranslateKeyConstants } from '../constants';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie';

@Injectable()
export class ToastrHelper {
    private translate: TranslateService;
    constructor(
        private toastr: ToastrService,
        http: HttpClient,
        cookieService: CookieService
    ) {
        this.translate = new TranslateService(http, cookieService);
        this.translate.setlang();
    }

    success(message: string, title: string = null) {
        this.toastr.success(message,
            title === null || title === undefined ?
                this.translate.translate(TranslateKeyConstants.SHARE.MESSAGE_LABLE_SUCCESS) :
                title
        );
    }

    warning(message: string, title: string = null) {
        this.toastr.warning(message,
            title === null || title === undefined ?
                this.translate.translate(TranslateKeyConstants.SHARE.MESSAGE_LABLE_WARNING) :
                title);
    }

    error(message: string, title: string = null) {
        this.toastr.error(message,
            title === null || title === undefined ?
                this.translate.translate(TranslateKeyConstants.SHARE.MESSAGE_LABLE_ERROR) :
                title);
    }

    info(message: string, title: string = null) {
        this.toastr.info(message,
            title === null || title === undefined ?
                this.translate.translate(TranslateKeyConstants.SHARE.MESSAGE_LABLE_INFO) :
                title);
    }

    showError(error: ErrorResult) {
        switch (error.type) {
            case ErrorType.ERROR:
                this.error(error.message);
                break;
            case ErrorType.WARNING:
                this.warning(error.message);
                break;
            case ErrorType.FORBIDDEN:
                this.error(error.message);
                break;
            default:
                this.error(error.message);
                break;
        }
    }

}

﻿import {
    Injectable,
    Inject
} from '@angular/core';
import { ActionType } from '../enums';
import { AppState } from '../AppState';
import { Store } from '@ngrx/store';
import {
    InfoResult,
    SessionUserInfo
} from '../models';
import { Subscription } from 'rxjs';
import {
    WebStorageService,
    SESSION_STORAGE
} from 'angular-webstorage-service';
import { KeyConstants } from '../constants';

@Injectable()
export class PermissionActionHelper {
    private subscriptions: Subscription[] = [];
    private actions: Array<string> = [];

    constructor(private store: Store<AppState>,
        @Inject(SESSION_STORAGE) private storage: WebStorageService,
    ) {
    }

    initPermission() {
        this.subscriptions.push(this.store.select<InfoResult<SessionUserInfo>>(state => state.auths.getLoginInfo)
            .subscribe((result) => {
                if (result !== undefined) {
                    this.actions = result.data === undefined || result.data === null ? [] : result.data.actions;
                }
            }));
        const sessionUserInfoString = this.storage.get(KeyConstants.USER_INFO);

        if (sessionUserInfoString !== null && sessionUserInfoString !== undefined && sessionUserInfoString !== '') {
            const sessionUserInfo = JSON.parse(sessionUserInfoString);
            if (sessionUserInfo !== null && sessionUserInfo !== undefined) {
                if (sessionUserInfo === null || sessionUserInfo === undefined) {
                    this.actions = sessionUserInfo.actionIds;
                } else {
                    this.actions = [];
                }
            }
        }
    }

    isValid(actionType: ActionType): boolean {
        if (this.actions === null || this.actions === undefined || this.actions.length === 0) {
            return false;
        } else {
            return this.actions.indexOf(actionType) >= 0;
        }
    }
}

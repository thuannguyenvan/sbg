﻿(function (window) {
    window.__env = window.__env || {};
    window.__env.API_ENDPOINT = 'http://192.168.1.9:3333/api/v1/';
    window.__env.SSO_SIGNIN = 'http://192.168.1.9:8800/signin?app_id=147852&continue_url=';
    window.__env.SSO_SIGNOUT = 'http://192.168.1.9:8800/signout?app_id=147852';
    window.__env.ENABLE_DEBUG = true;
}(this));
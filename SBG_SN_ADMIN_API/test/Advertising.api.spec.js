const chaiHttp = require('chai-http');
const config = require('./config.json');

import chai from 'chai';

import {
  describe,
  it,
} from 'mocha';
import {
  AdsPartner,
  Advertising,
  AdsConfiguration,
  AdsConfigurationDetail,
} from '../src/data/models';
import {
  Routers
} from '../src/data/constants';
import {
  LanguageKeys,
} from '../src/data/constants';
import { _translateTemplateTest as translate } from '../src/utils/translate';
const ROUTES = Routers.Advertising.ROUTES;

const headers = {
  authorization: config.authorization
};

const Messages = LanguageKeys.messages;

chai.use(chaiHttp);
describe.only('Quản lý quảng cáo (API)', () => {

  let objPartner;
  let objAdsTestPagging;
  let objAdsActive;
  let objPartnerInActive;
  let objAds;
  let advertisingInConfig;
  let modelAddAdsValid;
  let adsConfig;
  let adsConfigDetail;
  let modelAddAdsSamePartnerAndTitle;
  let modelAdsInActive;

  const modelPartner = {
    Name: 'unitest tran van test---',
    Logo: 'unitest logo',
    Website: 'unitest google.com',
    Description: 'unitest doi tac quan trong'
  };

  let modelAddAdsNotExitPartner = {
    adsPartnerId: '0c6619052ba68131301f9119',
    title: 'VALID_ADVERTISING' + '_VALID_' + Date.now(),
    clickThrough: 'model.clickThrough',
    clickTracking: 'model.clickTracking',
    content: 'model.content',
    adsFormat: 'ADS_FORMAT.IMAGE',
    description: 'model.description'
  };

  beforeEach(async () => {
    //Tạo dữ liệu cho test case
    objPartner = new AdsPartner(modelPartner);
    await objPartner.save();

    const modelAds = new Advertising({
      AdsPartnerId: objPartner.id,
      AdsPartnerName: objPartner.Name,
      Title: 'unit test model.title',
      ClickThrough: 'unit test model.clickThrough',
      ClickTracking: 'unit test model.clickTracking',
      Content: 'unit test model.content',
      AdsFormat: 'ADS_FORMAT.IMAGE',
      Description: 'unit test model.description'
    });

    modelAddAdsValid = {
      adsPartnerId: objPartner.id,
      title: 'VALID_ADVERTISING' + '_VALID_' + Date.now(),
      clickThrough: 'model.clickThrough',
      clickTracking: 'model.clickTracking',
      content: 'model.content',
      adsFormat: 'ADS_FORMAT.IMAGE',
      description: 'model.description'
    };

    for (let i = 0; i < 5; i++) {
      const adsModel = new Advertising({
        AdsPartnerId: objPartner.id,
        AdsPartnerName: objPartner.Name,
        Title: 'unit test model.title pagging',
        ClickThrough: 'unit test model.clickThrough pagging',
        ClickTracking: 'unit test model.clickTracking pagging',
        Content: 'unit test model.content pagging',
        AdsFormat: 'ADS_FORMAT.IMAGE',
        Description: 'unit test model.description pagging'
      });
      const dataAds = new Advertising(adsModel);
      await dataAds.save();
    }

    objAds = new Advertising(modelAds);
    await objAds.save();

    modelAddAdsSamePartnerAndTitle = {
      adsPartnerId: objPartner.id,
      adsPartnerName: 'unit test adsPartnerName',
      adsPartnerLogo: 'unit test adsPartnerLogo',
      adsPartnerWebsite: 'unit test adsPartnerWebsite',
      title: 'unit test UPDATE_ADS_UNIT_TEST _IN_CONFIG',
      clickThrough: 'unit test model.clickThrough',
      clickTracking: 'unit test model.clickTracking',
      content: 'unit test model.content',
      adsFormat: 'ADS_FORMAT.IMAGE',
      description: 'unit test model.description'
    };

    modelAdsInActive = new Advertising({
      AdsPartnerId: objPartner.id,
      AdsPartnerName: objPartner.Name,
      Title: 'unit test model.title',
      ClickThrough: 'unit test model.clickThrough',
      ClickTracking: 'unit test model.clickTracking',
      Content: 'unit test model.content',
      AdsFormat: 'ADS_FORMAT.IMAGE',
      Description: 'unit test model.description',
      IsActive: false
    });

    advertisingInConfig = new Advertising({
      AdsPartnerId: objPartner.id,
      AdsPartnerName: 'unit test adsPartnerName',
      AdsPartnerLogo: 'unit test adsPartnerLogo',
      AdsPartnerWebsite: 'unit test adsPartnerWebsite',
      Title: 'unit test UPDATE_ADS_UNIT_TEST _IN_CONFIG',
      ClickThrough: 'unit test model.clickThrough',
      ClickTracking: 'unit test model.clickTracking',
      Content: 'unit test model.content',
      AdsFormat: 'ADS_FORMAT.IMAGE',
      Description: 'unit test model.description'
    });
    await advertisingInConfig.save();

    adsConfig = new AdsConfiguration({
      Title: 'CONFIG_ADS_' + Date.now(),
      Description: 'DESC',
      UpdatingStatus: 'abc'
    });
    await adsConfig.save();

    adsConfigDetail = new AdsConfigurationDetail({
      AdsConfigurationId: adsConfig.id,
      AdsPartnerId: advertisingInConfig.toJsonObject().AdsPartnerId,
      AdvertisingId: advertisingInConfig.id,
      AdsFormat: 'ADS_FORMAT.IMAGE'
    });
    await adsConfigDetail.save();
  });

  afterEach(async () => {
    // Xóa dữ liệu test


    await AdsPartner.deleteOne({ _id: objPartner.id });
    await Advertising.deleteMany({ Title: modelAddAdsSamePartnerAndTitle.title });
    await Advertising.deleteMany({ Title: modelAddAdsNotExitPartner.title });
    await Advertising.deleteMany({ Title: modelAdsInActive.Title });
    await Advertising.deleteOne({ _id: objAds.id });
    await Advertising.deleteOne({ _id: advertisingInConfig.id });
    await AdsConfigurationDetail.deleteOne({ _id: adsConfigDetail.id });
    await AdsConfiguration.deleteOne({ _id: adsConfig.id });
    const dataAddAdsValid = await Advertising.find({ Title: modelAddAdsValid.title });
    dataAddAdsValid.forEach(item => {
      item.remove();
    });
    await Advertising.deleteOne({ _id: modelAddAdsValid.id });
    const dataAds = await Advertising.find({ Title: 'unit test model.title pagging' });
    dataAds.forEach(item => {
      item.remove();
    });
    // await AdsPartner.remove({});
    // await Advertising.remove({});
    // await AdsConfigurationDetail.remove({});
    // await AdsConfiguration.remove({});
  });

  /*
  * Test case: Khóa và mở khóa quảng cáo
  */
  describe('activatedeactivateAdvertising', () => {
    it('it: khóa quảng cáo với adsId không đúng hoặc không tồn tại', (done) => {
      chai.request(config.server)
        .post(ROUTES.DEACTIVATE.URL.replace('{id}', '5c6619052ba68131301f0119'))
        .set(headers)
        .send()
        .end((err, res) => {
          //Kiểm tra kết quả
          res.status.should.be.eql(400);
          res.body.message.should.be.eql(translate(Messages.advertising.ADVERTISING_DOES_NOT_EXIST));
          done();
        });
    });

    it('it: Khóa quảng cáo với adsId đúng và đã bị khóa', (done) => {
      let adsDeactive = new Advertising(modelAdsInActive);
      adsDeactive.save();
      chai.request(config.server)
        .post(ROUTES.DEACTIVATE.URL.replace('{id}', adsDeactive.id))
        .set(headers)
        .send()
        .end((err, res) => {
          res.status.should.be.eql(400);
          res.body.message.should.be.eql(translate(Messages.advertising.ADVERTISING_ALREADY_INACTIVE));
          done();
        });
    });

    it('it: Khóa quảng cáo với adsId đúng và chưa bị khóa', (done) => {
      chai.request(config.server)
        .post(ROUTES.DEACTIVATE.URL.replace('{id}', objAds.id))
        .set(headers)
        .send()
        .end((err, res) => {
          res.status.should.be.eql(200);
          res.body.message.should.be.eql(translate(Messages.advertising.DEACTIVATE_ADVERTISING_SUCCESS));
          done();
        });
    });

    it('it: Mở khóa quảng cáo với adsId không đúng hoặc không tồn tại', (done) => {
      chai.request(config.server)
        .post(ROUTES.ACTIVATE.URL.replace('{id}', '5c6619052ba68131301f0119'))
        .set(headers)
        .send()
        .end((err, res) => {
          //Kiểm tra kết quả
          res.status.should.be.eql(400);
          res.body.message.should.be.eql(translate(Messages.advertising.ADVERTISING_DOES_NOT_EXIST));
          done();
        });
    });

    it('it: Mở khóa quảng cáo với adsId đúng và đã mở khóa', (done) => {
      chai.request(config.server)
        .post(ROUTES.ACTIVATE.URL.replace('{id}', objAds.id))
        .set(headers)
        .send()
        .end((err, res) => {
          //Kiểm tra kết quả
          res.status.should.be.eql(400);
          res.body.message.should.be.eql(translate(Messages.advertising.ADVERTISING_ALREADY_ACTIVE));
          done();
        });
    });

    it('it: Mở khóa quảng cáo với AdsId đúng và trạng thái quảng cáo đang bị khóa', (done) => {
      let adsDeactive = new Advertising(modelAdsInActive);
      adsDeactive.save();
      chai.request(config.server)
        .post(ROUTES.ACTIVATE.URL.replace('{id}', adsDeactive.id))
        .set(headers)
        .send()
        .end((err, res) => {
          //Kiểm tra kết quả
          res.status.should.be.eql(200);
          res.body.message.should.be.eql(translate(Messages.advertising.ACTIVATE_ADVERTISING_SUCCESS));
          done();
        });
    });
  });

  /*
  * Test case: Cập nhập thông tin quảng cáo
  */
  describe('updateAdvertising', () => {
    it('it: Cập nhập thông tin quảng cáo với quảng cáo không tồn tại', (done) => {
      const modelAds = {
        adsPartnerId: '0c6619052ba68131301f9119',
        title: advertisingInConfig.Title,
        clickThrough: 'unittest model.clickThrough',
        clickTracking: 'unittest model.clickTracking',
        content: 'unittest model.content',
        adsFormat: 'ADS_FORMAT.IMAGE',
        description: 'unittest model.description'
      };
      chai.request(config.server)
        .put(ROUTES.UPDATE.URL.replace('{id}', '0c6619052ba68131301f0119'))
        .set(headers)
        .send(modelAds)
        .end((err, res) => {
          res.status.should.be.eql(400);
          res.body.message.should.be.eql(translate(Messages.advertising.ADVERTISING_DOES_NOT_EXIST));
          done();
        });
    });

    it('it: Cập nhập thông tin quảng cáo với partner đã tồn tại và không tồn tại partner hoặc partner không được active', (done) => {
      const modelAds = {
        adsPartnerId: '0c6619052ba68131301f9119',
        title: advertisingInConfig.Title,
        clickThrough: 'unittest model.clickThrough',
        clickTracking: 'unittest model.clickTracking',
        content: 'unittest model.content',
        adsFormat: 'ADS_FORMAT.IMAGE',
        description: 'unittest model.description'
      };
      chai.request(config.server)
        .put(ROUTES.UPDATE.URL.replace('{id}', objAds.id))
        .set(headers)
        .send(modelAds)
        .end((err, res) => {
          res.status.should.be.eql(400);
          res.body.message.should.be.eql(translate(Messages.advertising.ADS_PARTNER_DOES_NOT_EXIST_OR_INACTIVE));
          done();
        });
    });

    it('it: Cập nhập thông tin quảng cáo với partner đã tồn tại và cùng đối tác và cùng tiêu đề', (done) => {
      const modelAds = {
        adsPartnerId: objPartner.id,
        title: advertisingInConfig.Title,
        clickThrough: 'unittest model.clickThrough',
        clickTracking: 'unittest model.clickTracking',
        content: 'unittest model.content',
        adsFormat: 'ADS_FORMAT.IMAGE',
        description: 'unittest model.description'
      };

      chai.request(config.server)
        .put(ROUTES.UPDATE.URL.replace('{id}', objAds.id))
        .set(headers)
        .send(modelAds)
        .end((err, res) => {
          res.status.should.be.eql(400);
          res.body.message.should.be.eql(translate(Messages.advertising.ADVERTISING_WITH_SAME_TITLE_AND_PARTNER_ALREADY_EXIST));
          done();
        });
    });

    it('it: Cập nhập thông tin quảng cáo hợp lệ', (done) => {
      const modelAds = {
        adsPartnerId: objPartner.id,
        title: 'VALID_ADVERTISING' + '_VALID_' + Date.now(),
        clickThrough: 'model.clickThrough',
        clickTracking: 'model.clickTracking',
        content: 'model.content',
        adsFormat: 'ADS_FORMAT.IMAGE',
        description: 'model.description'
      };

      chai.request(config.server)
        .put(ROUTES.UPDATE.URL.replace('{id}', objAds.id))
        .set(headers)
        .send(modelAds)
        .end((err, res) => {
          res.status.should.be.eql(200);
          res.body.success.should.be.true;
          res.body.message.should.be.eql(translate(Messages.advertising.UPDATE_ADVERTISING_SUCCESS));
          done();
        });
    });
  });

  /*
  * Test case: Thêm mới quảng cáo
  */
  describe('addAdvertising', () => {
    it('it: Thêm mới Quảng cáo có Đối tác không tồn tại hoặc không active', (done) => {
      chai.request(config.server)
        .post(ROUTES.ADD.URL)
        .set(headers)
        .send(modelAddAdsNotExitPartner)
        .end((err, res) => {
          res.status.should.be.eql(400);
          res.body.message.should.be.eql(translate(Messages.advertising.ADS_PARTNER_DOES_NOT_EXIST_OR_INACTIVE));
          done();
        });
    });

    it('it: Thêm mới Quảng cáo có trùng Đối tác và Tiêu đề', (done) => {
      chai.request(config.server)
        .post(ROUTES.ADD.URL)
        .set(headers)
        .send(modelAddAdsSamePartnerAndTitle)
        .end((err, res) => {
          res.status.should.be.eql(400);
          res.body.message.should.be.eql(translate(Messages.advertising.ADVERTISING_WITH_SAME_TITLE_AND_PARTNER_ALREADY_EXIST));
          done();
        });
    });

    it('it: Thêm mới Quảng cáo có Đối tác đã tồn tại và không trùng Tiêu đề', (done) => {
      chai.request(config.server)
        .post(ROUTES.ADD.URL)
        .set(headers)
        .send(modelAddAdsValid)
        .end((err, res) => {
          res.status.should.be.eql(200);
          res.body.data.should.be.not.null;
          done();
        });
    });
  });

  /*
    * Test case: Xóa quảng cáo
    */
  describe('deleteAdvertising', () => {
    it('it: Xóa quảng cáo khi id không đúng hoặc không tồn tại', (done) => {
      chai.request(config.server)
        .delete(ROUTES.DELETE.URL.replace('{id}', '0c6619052ba68131301f0119'))
        .set(headers)
        .send()
        .end((err, res) => {
          res.status.should.be.eql(400);
          res.body.message.should.be.eql(translate(Messages.advertising.ADVERTISING_DOES_NOT_EXIST));
          done();
        });
    });

    it('it: Xóa quảng cáo khi id đúng và chưa có config quảng cáo', (done) => {
      chai.request(config.server)
        .delete(ROUTES.DELETE.URL.replace('{id}', objAds.id))
        .set(headers)
        .send()
        .end((err, res) => {
          res.status.should.be.eql(200);
          res.body.success.should.be.true;
          done();
        });
    });

    it('it: Xóa quảng cáo khi id đúng và đã có config quảng cáo', (done) => {
      chai.request(config.server)
        .delete(ROUTES.DELETE.URL.replace('{id}', advertisingInConfig.id))
        .set(headers)
        .send()
        .end((err, res) => {
          res.status.should.be.eql(400);
          res.body.message.should.be.eql(translate(Messages.advertising.CANNOT_DELETE_ADVERTISING_CAUSE_USED_IN_CONFIG));
          done();
        });
    });
  });

  /*
    * Test case: danh sách quảng cáo
    */
  describe('searchAdvertising', () => {
    it('it: Tìm kiếm quảng cáo', (done) => {
      const modelSearch = {
        page: 0,
        size: 10,
        title: objAds.Title
      };
      chai.request(config.server)
        .post(ROUTES.SEARCH.URL)
        .set(headers)
        .send(modelSearch)
        .end((err, res) => {
          res.status.should.be.eql(200);
          res.body.data.should.be.a('array');
          res.body.data.length.should.be.eql(6);
          done();
        });
    });

    it('it: Tìm kiếm tất cả quảng cáo', (done) => {
      const modelSearch = {
        page: 0,
        size: 10
      };
      chai.request(config.server)
        .post(ROUTES.SEARCH.URL)
        .set(headers)
        .send(modelSearch)
        .end((err, res) => {
          res.status.should.be.eql(200);
          res.body.data.should.be.a('array');
          res.body.data.length.should.be.above(0);
          done();
        });
    });

    it('it: Phân trang danh sách quảng cáo', (done) => {
      const modelSearch = {
        page: 0,
        size: 1,
        title: objAds.Title
      };
      chai.request(config.server)
        .post(ROUTES.SEARCH.URL)
        .set(headers)
        .send(modelSearch)
        .end((err, res) => {
          res.status.should.be.eql(200);
          res.body.data.should.be.a('array');
          res.body.data.length.should.be.eql(1);
          res.body.data[0].total.should.be.eql(6);
          done();
        });
    });
  });

  /*
  * Test case: Lấy thông tin quảng cáo
  */
  describe('getEditInfo', () => {
    it('it: Lấy thông tin quảng cáo với ads không tồn tại hoặc sai IdAds', (done) => {
      chai.request(config.server)
        .get(ROUTES.GET_EDIT_INFO.URL.replace('{id}', '0c6619052ba68131301f0119'))
        .set(headers)
        .send()
        .end((err, res) => {
          res.status.should.be.eql(400);
          res.body.message.should.be.eql(translate(Messages.advertising.ADVERTISING_DOES_NOT_EXIST));
          done();
        });
    });

    it('it: Lấy thông tin quảng cáo với ads đã tồn tại', (done) => {
      chai.request(config.server)
        .get(ROUTES.GET_EDIT_INFO.URL.replace('{id}', objAds.id))
        .set(headers)
        .send()
        .end((err, res) => {
          res.status.should.be.eql(200);
          res.body.message.should.be.eql(translate(Messages.advertising.GET_EDIT_INFO_SUCCESS));
          done();
        });
    });
  });

});
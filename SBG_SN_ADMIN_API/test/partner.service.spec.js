import {
  expect,
  assert,
} from 'chai';
import {
  AdsPartner,
  Advertising,
} from '../src/data/models';
import {
  describe,
  beforeEach,
  afterEach,
  it,
} from 'mocha';
import {
  LanguageKeys,
} from '../src/data/constants';
const Messages = LanguageKeys.messages;

import {
  searchAdsPartner,
  addAdsPartner,
  getEditInfo,
  updateAdsPartner,
  deleteAdsPartner,
  activateDeactivateAdsPartner,
} from '../src/services/AdsPartnerService';

describe('Quản lý đối tác (Service)', () => {

  let objPartner;
  let objPartnerPagging;
  let objPartnerSecond;
  let objPartnerActive;
  let objPartnerInActive;
  let objAds;
  const modelSearch = {
    'page': 1,
    'size': 10,
    'isActive': true,
    'name': 'tran van test',
    'userActions': []
  };
  const modelPartner = {
    Name: 'tran van test',
    Logo: 'logo',
    Website: 'google.com',
    Description: 'doi tac quan trong'
  };

  const modelPartnerPagging = {
    Name: 'tran van test pagging',
    Logo: 'logo',
    Website: 'google.com',
    Description: 'doi tac quan trong'
  };

  const modelPartnerTestAddExit = {
    name: 'tran van test',
    logo: 'logo',
    website: 'google.com',
    description: 'doi tac quan trong'
  };

  const modelPartnerTestAddNotExit = {
    name: 'tran van test123',
    logo: 'logo',
    website: 'google.com',
    description: 'doi tac quan trong'
  };

  const modelPartnerSecond = {
    Name: 'tran van ztest',
    Logo: 'logo',
    Website: 'google.com',
    Description: 'doi tac quan trong'
  };

  const modelPartnerActive = {
    Name: 'tran van ztest',
    Logo: 'logo',
    Website: 'google.com',
    Description: 'doi tac quan trong',
    IsActive: true
  };

  const modelPartnerInActive = {
    Name: 'tran van ztest',
    Logo: 'logo',
    Website: 'google.com',
    Description: 'doi tac quan trong',
    IsActive: false
  };

  beforeEach(async () => {
    //Tạo dữ liệu cho test case
    for (let i = 0; i < 5; i++) {
      objPartnerPagging = new AdsPartner(modelPartnerPagging);
      await objPartnerPagging.save();
    }
    objPartner = new AdsPartner(modelPartner);
    await objPartner.save();
    objPartnerSecond = new AdsPartner(modelPartnerSecond);
    await objPartnerSecond.save();
    const modelAds = new Advertising({
      AdsPartnerId: objPartner.id,
      AdsPartnerName: 'tran van a',
      AdsPartnerLogo: 'logo',
      AdsPartnerWebsite: 'ưebsite',
      Title: 'model.title',
      ClickThrough: 'model.clickThrough',
      ClickTracking: 'model.clickTracking',
      Content: 'model.content',
      AdsFormat: 'model.adsFormat',
      Description: 'model.description'
    });
    objAds = new Advertising(modelAds);
    await objAds.save();
    objPartnerActive = new AdsPartner(modelPartnerActive);
    await objPartnerActive.save();
    objPartnerInActive = new AdsPartner(modelPartnerInActive);
    await objPartnerInActive.save();
  });

  describe('updateAdsPartner', () => {
    it('it: Cập nhập thông tin đối tác với partner không tồn tại hoặc sai Idpartner', async () => {
      objPartner.id = '5c6619052ba68131301f0119';
      await updateAdsPartner(objPartner).catch((error) => {
        //Kiểm tra kết quả
        error.output.statusCode.should.be.eql(400);
        error.output.payload.message.should.be.eql(Messages.adsPartner.ADS_PARTNER_DOES_NOT_EXIST);
      });
    });

    it('it: Cập nhập thông tin đối tác với partner đã tồn tại và trùng tên', async () => {
      const obj = {
        adsPartnerId: objPartner.id,
        name: 'tran van test123',
        logo: 'logo',
        website: 'google.com',
        description: 'doi tac quan trong'
      };
      await updateAdsPartner(obj).catch((error) => {
        //Kiểm tra kết quả
        error.output.statusCode.should.be.eql(400);
        error.output.payload.message.should.be.eql(Messages.adsPartner.ADS_PARTNER_WITH_THE_SAME_NAME_ALREADY_EXIST);
      });
    });

    it('it: Cập nhập thông tin đối tác với partner đã tồn tại và khác tên', async () => {
      const obj = {
        adsPartnerId: objPartner.id,
        name: 'tran van test12334',
        logo: 'logo',
        website: 'google.com',
        description: 'doi tac quan trong'
      };
      const result = await updateAdsPartner(obj);
      //Kiểm tra kết quả
      expect(result).to.be.true;
    });
  });

  describe('getEditInfo', () => {
    it('it: Lấy thông tin đối tác với partner không tồn tại hoặc sai Idpartner', async () => {
      await getEditInfo({ adsPartnerId: '5c6619052ba68131301f0119' }).catch((error) => {
        //Kiểm tra kết quả
        error.output.statusCode.should.be.eql(400);
        error.output.payload.message.should.be.eql(Messages.adsPartner.ADS_PARTNER_DOES_NOT_EXIST);
      });
    });

    it('it: Lấy thông tin đối tác với partner đã tồn tại', async () => {
      const result = await getEditInfo({ adsPartnerId: objPartner.id });
      //Kiểm tra kết quả
      expect(result).to.be.not.null;
      assert.equal(result.name, objPartner.Name);
    });
  });

  describe('searchAdsPartner', () => {
    it('it: Tìm kiếm partner với data có dữ liệu search', async () => {
      const result = await searchAdsPartner(modelSearch);
      //Kiểm tra kết quả
      expect(result).to.be.an('array');
      expect(result).to.have.length(6);
    });

    it('it: Tìm kiếm partner với data không có dữ liệu search', async () => {
      modelSearch.name = '-1undefined';
      const result = await searchAdsPartner(modelSearch);
      //Kiểm tra kết quả
      expect(result).to.be.length(0);
    });

    it('it: Phân trang danh sách đối tác', async () => {
      const modelSearch = {
        page: 0,
        size: 20,
        name: modelPartnerPagging.Name,
        userActions: []
      };

      const result = await searchAdsPartner(modelSearch);
      expect(result).to.be.a('array');
      expect(result).to.be.length(5);
      expect(result[0].total).to.be.eql(5);
    });
  });

  describe('deleteAdsPartner', () => {
    it('it: Xóa đối tác với PartnerId không đúng hoặc không tồn tại', async () => {
      await deleteAdsPartner({ adsPartnerId: '5c6619052ba68131301f0119' }).catch((error) => {
        //Kiểm tra kết quả
        error.output.statusCode.should.be.eql(400);
        error.output.payload.message.should.be.eql(Messages.adsPartner.ADS_PARTNER_DOES_NOT_EXIST);
      });
    });

    it('it: Xóa đối tác với PartnerId đúng và đã tồn tại quảng cáo', async () => {
      await deleteAdsPartner({ adsPartnerId: objPartner.id }).catch((error) => {
        //Kiểm tra kết quả
        error.output.statusCode.should.be.eql(400);
        error.output.payload.message.should.be.eql(Messages.adsPartner.CANNOT_DELETE_ADS_PARTNER_CAUSE_HAVE_ADVERTISING);
      });
    });

    it('it: Xóa đối tác với PartnerId đúng và chưa tồn tại quảng cáo', async () => {
      const result = await deleteAdsPartner({ adsPartnerId: objPartnerSecond.id });
      //Kiểm tra kết quả
      expect(result).to.be.true;
    });
  });

  describe('addAdsPartner', () => {
    it('it: Thêm mới đối tác với tên partner đã tồn tại', async () => {
      await addAdsPartner(modelPartnerTestAddExit).catch((error) => {
        //Kiểm tra kết quả
        error.output.statusCode.should.be.eql(400);
        error.output.payload.message.should.be.eql(Messages.adsPartner.ADS_PARTNER_WITH_THE_SAME_NAME_ALREADY_EXIST);
      });
    });

    it('it: Thêm mới đối tác với tên partner chưa tồn tại', async () => {
      const result = await addAdsPartner(modelPartnerTestAddNotExit);
      //Kiểm tra kết quả
      expect(result).to.be.not.null;
    });
  });

  describe('activateDeactivateAdsPartner', () => {
    it('it: khóa đối tác với PartnerId không đúng hoặc không tồn tại', (done) => {
      activateDeactivateAdsPartner({ adsPartnerId: '5c6619052ba68131301f0119' }).catch((error) => {
        //Kiểm tra kết quả
        error.output.statusCode.should.be.eql(400);
        error.output.payload.message.should.be.eql(Messages.adsPartner.ADS_PARTNER_DOES_NOT_EXIST);
      });
      done();
    });

    it('it: Khóa đối tác với PartnerId đúng và đã bị khóa', (done) => {
      activateDeactivateAdsPartner({ adsPartnerId: objPartnerInActive.id }).catch((error) => {
        //Kiểm tra kết quả
        error.output.statusCode.should.be.eql(400);
        error.output.payload.message.should.be.eql(Messages.adsPartner.ADS_PARTNER_ALREADY_INACTIVE);
      });
      done();
    });

    it('it: Khóa đối tác với PartnerId đúng và chưa bị khóa', async () => {
      const result = await activateDeactivateAdsPartner({ adsPartnerId: objPartnerActive.id });
      //Kiểm tra kết quả
      expect(result).to.be.true;
    });

    it('it: Mở khóa đối tác với PartnerId không đúng hoặc không tồn tại', (done) => {
      activateDeactivateAdsPartner({ adsPartnerId: '5c6619052ba68131301f0119', isActivate: true }).catch((error) => {
        //Kiểm tra kết quả
        error.output.statusCode.should.be.eql(400);
        error.output.payload.message.should.be.eql(Messages.adsPartner.ADS_PARTNER_DOES_NOT_EXIST);
      });
      done();
    });

    it('it: Mở khóa đối tác với PartnerId đúng và đã mở khóa', (done) => {
      activateDeactivateAdsPartner({ adsPartnerId: objPartnerActive.id, isActivate: true }).catch((error) => {
        //Kiểm tra kết quả
        error.output.statusCode.should.be.eql(400);
        error.output.payload.message.should.be.eql(Messages.adsPartner.ADS_PARTNER_ALREADY_ACTIVE);
      });
      done();
    });

    it('it: Mở khóa đối tác với PartnerId đúng và chưa mở khóa', async () => {
      const result = await activateDeactivateAdsPartner({ adsPartnerId: objPartnerInActive.id, isActivate: true });
      //Kiểm tra kết quả
      expect(result).to.be.true;
    });
  });

  afterEach(async () => {
    // Xóa dữ liệu test
    const data = await AdsPartner.find({ name: modelPartnerPagging.name });
    data.forEach(item => {
      item.remove();
    });
    await AdsPartner.deleteOne({ _id: objPartner.id });
    await AdsPartner.deleteOne({ Name: modelPartnerTestAddExit.name });
    await AdsPartner.deleteOne({ Name: modelPartnerTestAddNotExit.name });
    await AdsPartner.deleteOne({ _id: objPartnerSecond.id });
    await AdsPartner.deleteOne({ _id: objPartnerActive.id });
    await AdsPartner.deleteOne({ _id: objPartnerInActive.id });
    await Advertising.deleteOne({ _id: objAds.id });
  });
});

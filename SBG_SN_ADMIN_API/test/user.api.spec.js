const chai = require('chai');
const chaiHttp = require('chai-http');
const config = require('./config.json');
const server = require('../src/index');
const should = chai.should();

import {
  User
} from '../src/data/models';
import {
  Routers
} from '../src/data/constants';
const ROUTES = Routers.User.ROUTES;

const headers = {
  authorization: config.authorization
};

chai.use(chaiHttp);

describe('Quản lý người dùng (API)', () => {

  const model = {
    UserName: 'sinhpd',
    Email: 'sinhpd@heliosys.com.vn',
    FullName: 'Phạm Duy Sinh',
    PhoneNumber: '0911991',
    Status: 1
  };

  beforeEach((done) => {
    //Tạo dữ liệu cho test case
    for (let i = 0; i < 10; i++) {
      User.create(model);
    }
    done();
  });
  /*
   * Test case: danh sách người dùng
   */
  describe('/GET Danh sách người dùng', () => {

    it('it: Tìm kiếm người dùng', (done) => {

      const modelSearch = {
        page: 0,
        size: 10,
        keywords: model.UserName
      }

      chai.request(config.server)
        .post(ROUTES.SEARCH.URL)
        .set(headers)
        .send(modelSearch)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.data.should.be.a('array');
          res.body.data.length.should.be.eql(10);
          done();
        });
    });

    it('it: Tìm kiếm tất cả người dùng người dùng', (done) => {

      const modelSearch = {
        page: 0,
        size: 10
      }

      chai.request(config.server)
        .post(ROUTES.SEARCH.URL)
        .set(headers)
        .send(modelSearch)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.data.should.be.a('array');
          res.body.data.length.should.be.above(0);
          done();
        });
    });

    it('it: Phân trang danh sách người dùng', (done) => {

      const modelSearch = {
        page: 0,
        size: 5,
        keywords: model.UserName
      }

      chai.request(config.server)
        .post(ROUTES.SEARCH.URL)
        .set(headers)
        .send(modelSearch)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.data.should.be.a('array');
          res.body.data.length.should.be.eql(5);
          res.body.data[0].total.should.be.eql(10);
          done();
        });
    });
  });

  afterEach(async () => {
    // Xóa dữ liệu test
    const data = await User.find({ UserName: model.UserName });
    data.forEach(item => {
      item.remove();
    });
  });

});
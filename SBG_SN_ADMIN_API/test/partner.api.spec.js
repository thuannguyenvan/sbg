const chaiHttp = require('chai-http');
const config = require('./config.json');

import chai from 'chai';

import {
  describe,
  it,
} from 'mocha';
import {
  AdsPartner,
  Advertising,
} from '../src/data/models';
import {
  Routers
} from '../src/data/constants';
import {
  LanguageKeys,
} from '../src/data/constants';
import { _translateTemplateTest as translate } from '../src/utils/translate';
const ROUTES = Routers.AdsPartner.ROUTES;

const headers = {
  authorization: config.authorization
};

const Messages = LanguageKeys.messages;

chai.use(chaiHttp);
describe('Quản lý đối tác (API)', () => {

  let objPartner;
  let objPartnerSecond;
  let objPartnerActive;
  let objPartnerInActive;
  let objAds;
  const modelPartner = {
    Name: 'tran van test---',
    Logo: 'logo',
    Website: 'google.com',
    Description: 'doi tac quan trong'
  };

  const modelPartnerTestAddExit = {
    name: 'tran van test---',
    logo: 'logo',
    website: 'google.com',
    description: 'doi tac quan trong'
  };

  const modelPartnerTestAddNotExit = {
    name: 'tran van test123212',
    logo: 'logo',
    website: 'google.com',
    description: 'doi tac quan trong'
  };

  const modelPartnerSecond = {
    Name: 'tran van z1test',
    Logo: 'logo',
    Website: 'google.com',
    Description: 'doi tac quan trong'
  };

  const modelPartnerActive = {
    Name: 'tran van ztest',
    Logo: 'logo',
    Website: 'google.com',
    Description: 'doi tac quan trong',
    IsActive: true
  };

  const modelPartnerInActive = {
    Name: 'tran van ztest',
    Logo: 'logo',
    Website: 'google.com',
    Description: 'doi tac quan trong',
    IsActive: false
  };

  beforeEach(async () => {
    //Tạo dữ liệu cho test case
    objPartner = new AdsPartner(modelPartner);
    await objPartner.save();

    const modelAds = new Advertising({
      AdsPartnerId: objPartner.id,
      AdsPartnerName: 'tran van a',
      AdsPartnerLogo: 'logo',
      AdsPartnerWebsite: 'ưebsite',
      Title: 'model.title',
      ClickThrough: 'model.clickThrough',
      ClickTracking: 'model.clickTracking',
      Content: 'model.content',
      AdsFormat: 'model.adsFormat',
      Description: 'model.description'
    });
    objAds = new Advertising(modelAds);
    await objAds.save();
    objPartnerActive = new AdsPartner(modelPartnerActive);
    await objPartnerActive.save();
    objPartnerInActive = new AdsPartner(modelPartnerInActive);
    await objPartnerInActive.save();
    objPartnerSecond = new AdsPartner(modelPartnerSecond);
    await objPartnerSecond.save();
  });

  afterEach(async () => {
    // Xóa dữ liệu test
    await AdsPartner.deleteOne({ _id: objPartner.id });
    await AdsPartner.deleteOne({ Name: modelPartnerTestAddExit.name });
    await AdsPartner.deleteOne({ Name: modelPartnerTestAddNotExit.name });
    await AdsPartner.deleteOne({ _id: objPartnerSecond.id });
    await AdsPartner.deleteOne({ _id: objPartnerActive.id });
    await AdsPartner.deleteOne({ _id: objPartnerInActive.id });
    await Advertising.deleteOne({ _id: objAds.id });
  });

  /*
  * Test case: Khóa và mở khóa đối tác
  */
  describe('activateDeactivateAdsPartner', () => {
    it('it: khóa đối tác với PartnerId không đúng hoặc không tồn tại', (done) => {
      chai.request(config.server)
        .post(ROUTES.DEACTIVATE.URL.replace('{id}', '5c6619052ba68131301f0119'))
        .set(headers)
        .send()
        .end((err, res) => {
          //Kiểm tra kết quả
          res.status.should.be.eql(400);
          res.body.message.should.be.eql(translate(Messages.adsPartner.ADS_PARTNER_DOES_NOT_EXIST));
          done();
        });
    });

    it('it: Khóa đối tác với PartnerId đúng và đã bị khóa', (done) => {
      let partnerDeactive = new AdsPartner(modelPartnerInActive);
      partnerDeactive.save();
      chai.request(config.server)
        .post(ROUTES.DEACTIVATE.URL.replace('{id}', partnerDeactive.id))
        .set(headers)
        .send()
        .end((err, res) => {
          res.status.should.be.eql(400);
          res.body.message.should.be.eql(translate(Messages.adsPartner.ADS_PARTNER_ALREADY_INACTIVE));
          done();
        });
    });

    it('it: Khóa đối tác với PartnerId đúng và chưa bị khóa', (done) => {
      chai.request(config.server)
        .post(ROUTES.DEACTIVATE.URL.replace('{id}', objPartner.id))
        .set(headers)
        .send()
        .end((err, res) => {
          res.status.should.be.eql(200);
          res.body.message.should.be.eql(translate(Messages.adsPartner.DEACTIVATE_ADS_PARTNER_SUCCESS));
          done();
        });
    });

    it('it: Mở khóa đối tác với PartnerId không đúng hoặc không tồn tại', (done) => {
      chai.request(config.server)
        .post(ROUTES.ACTIVATE.URL.replace('{id}', '5c6619052ba68131301f0119'))
        .set(headers)
        .send()
        .end((err, res) => {
          //Kiểm tra kết quả
          res.status.should.be.eql(400);
          res.body.message.should.be.eql(translate(Messages.adsPartner.ADS_PARTNER_DOES_NOT_EXIST));
          done();
        });
    });

    it('it: Mở khóa đối tác với PartnerId đúng và đã mở khóa', (done) => {
      chai.request(config.server)
        .post(ROUTES.ACTIVATE.URL.replace('{id}', objPartner.id))
        .set(headers)
        .send()
        .end((err, res) => {
          //Kiểm tra kết quả
          res.status.should.be.eql(400);
          res.body.message.should.be.eql(translate(Messages.adsPartner.ADS_PARTNER_ALREADY_ACTIVE));
          done();
        });
    });

    it('it: Mở khóa đối tác với PartnerId đúng và chưa mở khóa', (done) => {
      let partnerDeactive = new AdsPartner(modelPartnerInActive);
      partnerDeactive.save();
      chai.request(config.server)
        .post(ROUTES.ACTIVATE.URL.replace('{id}', partnerDeactive.id))
        .set(headers)
        .send()
        .end((err, res) => {
          //Kiểm tra kết quả
          res.status.should.be.eql(200);
          res.body.message.should.be.eql(translate(Messages.adsPartner.ACTIVATE_ADS_PARTNER_SUCCESS));
          done();
        });
    });
  });

  /*
  * Test case: Cập nhập thông tin đối tác
  */
  describe('updateAdsPartner', () => {
    it('it: Cập nhập thông tin đối tác với partner không tồn tại hoặc sai Idpartner', (done) => {
      const obj = {
        name: 'tran van test12334',
        logo: 'logo',
        website: 'google.com',
        description: 'doi tac quan trong'
      };
      chai.request(config.server)
        .put(ROUTES.UPDATE.URL.replace('{id}', '0c6619052ba68131301f0119'))
        .set(headers)
        .send(obj)
        .end((err, res) => {
          res.status.should.be.eql(400);
          res.body.message.should.be.eql(translate(Messages.adsPartner.ADS_PARTNER_DOES_NOT_EXIST));
          done();
        });
    });

    it('it: Cập nhập thông tin đối tác với partner đã tồn tại và trùng tên', (done) => {
      const obj = {
        name: objPartnerSecond.Name,
        logo: 'logo',
        website: 'google.com',
        description: 'doi tac quan trong'
      };
      chai.request(config.server)
        .put(ROUTES.UPDATE.URL.replace('{id}', objPartner.id))
        .set(headers)
        .send(obj)
        .end((err, res) => {
          res.status.should.be.eql(400);
          res.body.message.should.be.eql(translate(Messages.adsPartner.ADS_PARTNER_WITH_THE_SAME_NAME_ALREADY_EXIST));
          done();
        });
    });

    it('it: Cập nhập thông tin đối tác với partner đã tồn tại và khác tên', (done) => {
      const obj = {
        name: objPartnerSecond.Name + '111',
        logo: 'logo',
        website: 'google.com',
        description: 'doi tac quan trong'
      };
      chai.request(config.server)
        .put(ROUTES.UPDATE.URL.replace('{id}', objPartner.id))
        .set(headers)
        .send(obj)
        .end((err, res) => {
          res.status.should.be.eql(200);
          res.body.success.should.be.true;
          done();
        });
    });
  });

  /*
  * Test case: Thêm mới đối tác
  */
  describe('addAdsPartner', () => {
    it('it: Thêm mới đối tác với tên partner đã tồn tại', (done) => {
      chai.request(config.server)
        .post(ROUTES.ADD.URL)
        .set(headers)
        .send(modelPartnerTestAddExit)
        .end((err, res) => {
          res.status.should.be.eql(400);
          done();
        });
    });

    it('it: Thêm mới đối tác với tên partner chưa tồn tại', (done) => {
      chai.request(config.server)
        .post(ROUTES.ADD.URL)
        .set(headers)
        .send(modelPartnerTestAddNotExit)
        .end((err, res) => {
          res.status.should.be.eql(200);
          done();
        });
    });
  });

  /*
    * Test case: Xóa đối tác
    */
  describe('deleteAdsPartner', () => {
    it('it: Xóa đối tác khi id không đúng hoặc không tồn tại', (done) => {
      chai.request(config.server)
        .delete(ROUTES.DELETE.URL.replace('{id}', '0c6619052ba68131301f0119'))
        .set(headers)
        .send()
        .end((err, res) => {
          res.status.should.be.eql(400);
          res.body.message.should.be.eql(translate(Messages.adsPartner.ADS_PARTNER_DOES_NOT_EXIST));
          done();
        });
    });

    it('it: Xóa đối tác khi id đúng và không có quảng cáo', (done) => {
      chai.request(config.server)
        .delete(ROUTES.DELETE.URL.replace('{id}', objPartnerSecond.id))
        .set(headers)
        .send()
        .end((err, res) => {
          res.status.should.be.eql(200);
          res.body.success.should.be.true;
          done();
        });
    });

    it('it: Xóa đối tác khi id đúng và có quảng cáo', (done) => {
      chai.request(config.server)
        .delete(ROUTES.DELETE.URL.replace('{id}', objPartner.id))
        .set(headers)
        .send()
        .end((err, res) => {
          res.status.should.be.eql(400);
          res.body.message.should.be.eql(translate(Messages.adsPartner.CANNOT_DELETE_ADS_PARTNER_CAUSE_HAVE_ADVERTISING));
          done();
        });
    });
  });

  /*
    * Test case: danh sách đối tác
    */
  describe('searchAdsPartner', () => {
    it('it: Tìm kiếm đối tác', (done) => {
      const modelSearch = {
        page: 0,
        size: 10,
        name: modelPartner.Name
      };
      chai.request(config.server)
        .post(ROUTES.SEARCH.URL)
        .set(headers)
        .send(modelSearch)
        .end((err, res) => {
          res.status.should.be.eql(200);
          res.body.data.should.be.a('array');
          res.body.data.length.should.be.eql(1);
          done();
        });
    });

    it('it: Tìm kiếm tất cả đối tác', (done) => {
      const modelSearch = {
        page: 0,
        size: 10
      };
      chai.request(config.server)
        .post(ROUTES.SEARCH.URL)
        .set(headers)
        .send(modelSearch)
        .end((err, res) => {
          res.status.should.be.eql(200);
          res.body.data.should.be.a('array');
          res.body.data.length.should.be.above(0);
          done();
        });
    });

    it('it: Phân trang danh sách đối tác', (done) => {
      const modelSearch = {
        page: 0,
        size: 1,
        name: modelPartner.Name
      };
      chai.request(config.server)
        .post(ROUTES.SEARCH.URL)
        .set(headers)
        .send(modelSearch)
        .end((err, res) => {
          res.status.should.be.eql(200);
          res.body.data.should.be.a('array');
          res.body.data.length.should.be.eql(1);
          res.body.data[0].total.should.be.eql(1);
          done();
        });
    });
  });

  /*
  * Test case: Lấy thông tin đối tác
  */
  describe('getEditInfo', () => {
    it('it: Lấy thông tin đối tác với partner không tồn tại hoặc sai Idpartner', (done) => {
      chai.request(config.server)
        .get(ROUTES.GET_EDIT_INFO.URL.replace('{id}', '0c6619052ba68131301f0119'))
        .set(headers)
        .send()
        .end((err, res) => {
          res.status.should.be.eql(400);
          done();
        });
    });

    it('it: Lấy thông tin đối tác với partner đã tồn tại', (done) => {
      chai.request(config.server)
        .get(ROUTES.GET_EDIT_INFO.URL.replace('{id}', objPartner.id))
        .set(headers)
        .send()
        .end((err, res) => {
          res.status.should.be.eql(200);
          done();
        });
    });
  });

});
import { expect, should } from 'chai';
import {
  User,
  Role
} from '../src/data/models';

import { LanguageKeys } from '../src/data/constants';
const Messages = LanguageKeys.messages;

import {
  SearchUser,
  GetUserRoles,
  UpdateUserRoles,
  ActivateDeactiveUser
} from '../src/services/UserService';

describe('Quản lý người dùng (Service)', () => {

  let obj;
  let role;

  beforeEach(async () => {
    role = (await Role.find({ IsDeleted: false })).map(x => x.id);
    const model = {
      UserName: 'testRole',
      Email: 'testRole@heliosys.com.vn',
      FullName: 'Phạm Duy Sinh',
      PhoneNumber: '0911991',
      Status: 1,
      Roles: role
    };

    //Tạo dữ liệu cho test case
    obj = new User(model);
    await obj.save();
  });

  describe('GetUserRoles', () => {
    it('it: Lấy danh sách Roles theo User', async () => {
      const roles = await GetUserRoles({ userId: obj.id });
      const arrayRoles = roles.map(x => x.id);
      //Kiểm tra kết quả
      expect(role).to.have.members(arrayRoles);
    });

    it('it: userId không đúng', async () => {
      await GetUserRoles({ userId: 'Test' }).catch((error) => {
        error.output.statusCode.should.be.eql(400);
        error.output.payload.message.should.be.eql(Messages.user.INVALID_USER_ID);
      });
    })

    it('it: user không tồn tại', async () => {
      await GetUserRoles({ userId: '5c6619052ba68131301f0111' }).catch((error) => {
        error.output.statusCode.should.be.eql(400);
        error.output.payload.message.should.be.eql(Messages.user.USER_DOES_NOT_EXIST);
      });
    })

  })

  afterEach(async () => {
    // Xóa dữ liệu test
    const data = await User.find({ UserName: obj.UserName });
    data.forEach(item => {
      item.remove();
    });
  });

})

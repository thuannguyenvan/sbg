import authScheme from './AuthScheme';
import { Auth as  authConfig }  from '../../data/constants';

const pluginAuth = {
  name: authConfig.API_AUTH_PLUGIN,
  version: authConfig.API_AUTH_PLUGIN_VERSION,
  register: async function (server, options) {
    server.auth.scheme(authConfig.API_AUTH_SCHEME, authScheme);

    await server.auth.strategy(authConfig.API_AUTH_STRATEGY, authConfig.API_AUTH_SCHEME, {
      raw: options.raw
    });
  }
};

module.exports = pluginAuth;

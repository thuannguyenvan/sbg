import ApiError from '../model/ApiError';
import { Joi } from '../../utils/validate';
import { GetToken } from '../../services/TokenService';
import { GetUserActions, GetRolesFromUser, GetUser } from '../../services/UserService';
import { LanguageKeys, Patterns } from '../../data/constants';
import { translateErrorDetails } from '../../utils/validate/errorHelpers';

const Messages = LanguageKeys.messages;

const TokenPrefix = 'Bearer ';
/* eslint-disable */
const scheme = function (server, options) {
  return {
    authenticate: async function (req, h) {
      const authorization = req.headers.authorization;

      const headersValidate = Joi.object({
        'authorization': Joi.string().regex(Patterns.BEARER_TOKEN).required()
      }).unknown(true);

      try {
        await headersValidate.validate(req.headers);
      } catch (err) {
        const languageValidate = req.i18n.__('validate');
        err.details = translateErrorDetails(err.details, languageValidate);
        throw ApiError.unauthorizedError(err);
      }

      const tokenstring = authorization.substring(TokenPrefix.length);
      if (!authorization || !tokenstring) {
        throw ApiError.unauthorizedError(Messages.auth.UNAUTHORIZED);
      }

      const token = await GetToken({ Token: tokenstring, IsDeleted: false, Expired: false });

      if (token == null) {
        throw ApiError.unauthorizedError(Messages.auth.UNAUTHORIZED);
      }

      // get user
      const user = await GetUser({ _id: token.UserId, IsDeleted: false });

      if (user == null || !user.IsActive) {
        throw ApiError.unauthorizedError(Messages.auth.UNAUTHORIZED);
      }

      // check Role is Admin, Mod and Supper Mod
      // const need = [RoleType.ADMIN, RoleType.SMOD, RoleType.MOD];
      // const got = await GetUserRoleKeys(user);
      // let hasRole = false;
      // for (const key of need) {
      //   if (got.includes(key)) {
      //     hasRole = true;
      //     break;
      //   }
      // }
      // if (!hasRole) {
      //   throw ApiError.unauthorizedError(Messages.auth.USER_ROLE_HAS_NOT_PERMISSION);
      // }

      const userRoles = await GetRolesFromUser(user);
      const hasAccess = userRoles && userRoles.findIndex(r => r.CmsAccess) >= 0;
      if (!hasAccess) {
        throw ApiError.unauthorizedError(Messages.auth.USER_ROLE_HAS_NOT_PERMISSION);
      }

      // get user's actions
      const scope = await GetUserActions({ UserId: token.UserId });

      return h.authenticated({
        credentials: {
          token: token.Token,
          ssoToken: token.ProviderToken,
          scope: scope,
          user: user.toJsonObject()
        }
      });
    }
  };
};

module.exports = scheme;

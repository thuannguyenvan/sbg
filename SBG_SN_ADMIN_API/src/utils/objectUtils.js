export function DocToJSON(doc) {
  const ret = JSON.parse(JSON.stringify(doc));
  delete ret.__v;
  return ret;
}

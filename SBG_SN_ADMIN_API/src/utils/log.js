import  log4js from 'log4js';

export function configure(folderPath = 'logs/') {
  const pathInfo = folderPath + 'infos.log';
  const errorPath = folderPath + 'errors.log';
  log4js.configure({
    appenders: {
      everythinginfo: { type: 'dateFile', filename: pathInfo, pattern: '.yyyy-MM-dd-hh', compress: false, keepFileExt: true },
      everythingerror: { type: 'dateFile', filename: errorPath, pattern: '.yyyy-MM-dd-hh', compress: false, keepFileExt: true },
      infos: { type: 'logLevelFilter', level: 'info', maxLevel: 'info', appender: 'everythinginfo' },
      errors: { type: 'logLevelFilter', level: 'error', maxLevel: 'error', appender: 'everythingerror' }
    },
    categories: {
      default: { appenders: ['infos', 'errors' ], level: 'debug' }
    }
  });
}

// log4js.configure({
//   appenders: {
//     everything: { type: 'dateFile', filename: 'logs/logs.log', pattern: '.yyyy-MM-dd-hh', compress: false }
//   },
//   categories: {
//     default: { appenders: [ 'everything' ], level: 'debug'}
//   }
// });

// log4js.configure({
//   appenders: {
//     everything: { type: 'file', filename: 'logs/all-the-logs.log', maxLogSize: 10240, compress: false }
//   },
//   categories: {
//     default: { appenders: [ 'everything' ], level: 'debug'}
//   }
// });

export default {
  fatal: function(content, category) {
    let logger = log4js.getLogger();
    if (category) {
      logger = log4js.getLogger(category);
    }
    logger.fatal(content);
  },
  error: function(content, category) {
    let logger = log4js.getLogger();
    if (category) {
      logger = log4js.getLogger(category);
    }
    logger.error(content);
  },
  warn: function(content, category) {
    let logger = log4js.getLogger();
    if (category) {
      logger = log4js.getLogger(category);
    }
    logger.warn(content);
  },
  info: function(content, category) {
    let logger = log4js.getLogger();
    if (category) {
      logger = log4js.getLogger(category);
    }
    logger.info(content);
  },
  debug: function(content, category) {
    let logger = log4js.getLogger();
    if (category) {
      logger = log4js.getLogger(category);
    }
    logger.debug(content);
  },
  trace: function(content, category) {
    let logger = log4js.getLogger();
    if (category) {
      logger = log4js.getLogger(category);
    }
    logger.trace(content);
  }
};

// export default log4js.getLogger();

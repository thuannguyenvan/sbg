export function ValidObjectId(idString) {
  if (idString.match(/^[0-9a-fA-F]{24}$/)) {
    return true;
  }
  return false;
}

export function GetValidObjectId(idStringArray) {
  const ret = idStringArray.reduce((a, e) => {
    if (ValidObjectId(e)) {
      return a.concat(e);
    } else {
      return a;
    }
  }, []);

  return ret;
}

export function StringIsJson(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}

export function GetRelativeSearchPattern(keywords) {
  if (!keywords) return keywords;
  const _A = '(a|à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)';
  const _E = '(e|è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)';
  const _O = '(o|ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)';
  const _I = '(i|ì|í|ị|ỉ|ĩ)';
  const _U = '(u|ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)';
  const _Y = '(y|ỳ|ý|ỵ|ỷ|ỹ)';
  const _D = '(d|đ)';
  const _AEOIUYD = [_A, _E, _O, _I, _U, _Y, _D];

  const normalizePattern = /(\/|\.|\[|\]|-|\)|\(|\?|:|=|!|\*|\+|\{|\}|,|\||\$|\^)/g;
  keywords = keywords.replace(normalizePattern, function($0) {
    return '\\' + $0;
  });

  for (const pattern of _AEOIUYD) {
    keywords = keywords.replace(new RegExp(pattern, 'gi'), pattern);
  }

  return new RegExp(keywords, 'i');
}

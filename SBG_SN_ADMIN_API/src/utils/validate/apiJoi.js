import BaseJoi from 'joi';
import {
  DateExtensions,
  NumberGreaterExtensions,
  ArrayLengthItemExtensions,
} from './extenstions';

const Joi = BaseJoi
            .extend(DateExtensions)
            .extend(NumberGreaterExtensions)
            .extend(ArrayLengthItemExtensions);

export default Joi;

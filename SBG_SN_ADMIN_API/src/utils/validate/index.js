import language from './language';
import { translateErrorDetails as TranslateErrorDetails } from './errorHelpers';

export { default as Joi } from './apiJoi';

export function MessageTemplate(template, ...keys) {
  const ret = {
    template: template,
    keys: keys
  };
  return '!!' + JSON.stringify(ret);
}

export default function ValidateHelper(ruleSchema) {
  const validation = {};

  validation.options = {
    abortEarly: false,
    allowUnknown: true,
    language: language
  };

  /* eslint-disable */
  validation.failAction = function(req, h, err) {
    const languageValidates = req.i18n.__('validate');
    err.details = TranslateErrorDetails(err.details, languageValidates);
    throw err;
  };

  if (typeof(ruleSchema) === 'object') {
    Object.keys(ruleSchema).map(r => {
      validation[r] = ruleSchema[r];
      return;
    });
  }

  return validation;
}

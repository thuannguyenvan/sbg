/* eslint-disable */
export default (joi) => ({

    name: 'number',

    base: joi.number(),

    language: {
      greater2: 'lớn hơn ({{q}})'
    },
    rules: [
        {
            name: 'greater2',
            description(params) {
              return `Số lớn hơn ${params.q}`;
            },
            params: {
              q: joi.alternatives([joi.number().required(), joi.func().ref()])
            },
            setup(params) {
              this._flags.momentFormat = params.q;
            },
            validate(params, value, state, options) {
              value
              if (value < params.q) {
                return this.createError('number.greater2', { v: value, q: params.q, options: options }, state, options);
              }

              return value;
            }
        }
    ]
});

/* eslint-disable */
export default (joi) => ({

    name: 'array',

    base: joi.array(),

    language: {
        itemMax: '!!Phần tử thứ "{{pos}}" của "{{label}}" không được vượt quá {{limit}} ký tự'
    },

    coerce(value, state, options) {

        // if (!value || value instanceof Date || typeof value === 'number') {
        //     return value;
        // }

        if (!value) {
          return value;
        }

        if (this._flags.limit) {
          for (let i = 0; i < value.length; ++i) {
            if (value[i].length > this._flags.limit) {
              return this.createError('array.itemMax', { value, limit: this._flags.limit, pos: i }, state, options);
            }
          }
        }

        return value;
    },

    rules: [
        {
            name: 'itemMax',
            description(params) {

                return `Date should respect format ${params.limit}`;
            },
            params: {
                limit: joi.number().required()
            },
            setup(params) {

                this._flags.limit = params.limit;
            },
            validate(params, value, state, options) {

                // No-op just to enable description
                return value;
            }
        }
    ]

});

const BaseLanguage = {
  errors: {
    root: 'value',
    key: '"{{!label}}" ',
    messages: {
      wrapArrays: true
    }
  }
};

export function GetLanguageErrors(templates) {
  const errors = BaseLanguage.errors;
  return {
    errors: Object.assign({}, errors, templates)
  };
}

import Hoek from 'hoek';
import { StringIsJson } from '../stringUtils';
import { GetLanguageErrors } from './languageHelpers';

const _stringify = function (value, wrapArrays) {

  const type = typeof value;

  if (value === null) {
      return 'null';
  }

  if (type === 'string') {
      return value;
  }

  if (type === 'function' || type === 'symbol') {
      return value.toString();
  }

  if (type === 'object') {
      if (Array.isArray(value)) {
          let partial = '';

          for (let i = 0; i < value.length; ++i) {
              partial = partial + (partial.length ? ', ' : '') + _stringify(value[i], wrapArrays);
          }

          return wrapArrays ? '[' + partial + ']' : partial;
      }

      return value.toString();
  }

  return JSON.stringify(value);
};

export function translateErrorDetails(errorDetails, languageValidate) {
  for (const detail of errorDetails) {
    if (!StringIsJson(detail.message)) {
      detail.context.label = Hoek.reach(languageValidate.labels, detail.context.label.toString()) || detail.context.label;
      detail.message = toString(detail.type, detail.context, GetLanguageErrors(languageValidate.errors));
    } else {
      detail.context.label = Hoek.reach(languageValidate.labels, detail.context.label) || detail.context.label;

      const validateTemplate = JSON.parse(detail.message);

      let template = validateTemplate.template;
      template = Hoek.reach(languageValidate.messageTemplates, template) || template;

      const keys = validateTemplate.keys;

      const context = detail.context;
      // const context = {};
      for (let i = 0; i < keys.length; i++) {
        const key = i.toString();
        context[key] = keys[i];
        context[key] = Hoek.reach(languageValidate.labels, context[key]) || context[key];
      }
      detail.message = templateToString(template, context);
    }
  }

  return errorDetails;
}

export function templateToString(template, context) {
  const wrapArrays = true;
  const escapeHtml = false;
  const keyLabel = '"{{!label}}" ';
  const hasKey = /{{!?label}}/.test(template);
  const skipKey = template.length > 2 && template[0] === '!' && template[1] === '!';

  if (skipKey) {
    template = template.slice(2);
  }

  if (!hasKey && !skipKey) {
    template = keyLabel + template;
  }

  const message =  template.replace(/{{(!?)([^}]+)}}/g, ($0, isSecure, name) => {
    const value = Hoek.reach(context, name);
    const normalized = _stringify(value, wrapArrays);
    return (isSecure && escapeHtml ? Hoek.escapeHtml(normalized) : normalized);
  });

  return message;
}

export function toString(type, context, language, options) {
  options = options || {};

  let template;

  // if (template) {
  //   format = template;
  // }

  const localized = options.language;

  template = template || Hoek.reach(localized, type) || Hoek.reach(language.errors, type);

  if (template === undefined) {
    return `Error code "${type}" is not defined, your custom type is missing the correct language definition`;
  }

  let wrapArrays = Hoek.reach(localized, 'messages.wrapArrays');
  if (typeof wrapArrays !== 'boolean') {
    wrapArrays = language.errors.messages.wrapArrays;
  }

  if (template === null) {
    const childrenString = _stringify(context.reason, wrapArrays);
    if (wrapArrays) {
      return childrenString.slice(1, -1);
    }

    return childrenString;
  }

  const hasKey = /{{!?label}}/.test(template);
  const skipKey = template.length > 2 && template[0] === '!' && template[1] === '!';

  if (skipKey) {
    template = template.slice(2);
  }

  if (!hasKey && !skipKey) {
    const localizedKey = Hoek.reach(localized, 'key');
    if (typeof localizedKey === 'string') {
      template = localizedKey + template;
    }
    else {
      template = Hoek.reach(language.errors, 'key') + template;
    }
  }

  const message =  template.replace(/{{(!?)([^}]+)}}/g, ($0, isSecure, name) => {
    const value = Hoek.reach(context, name);
    const normalized = _stringify(value, wrapArrays);
    return (isSecure && options.escapeHtml ? Hoek.escapeHtml(normalized) : normalized);
  });

  return message;
}

// export function toString(template, type, context, options) {
//   options = options || {};

//   let format;

//   if (template) {
//     format = template;
//   }

//   const localized = options.language;

//   format = format || Hoek.reach(localized, type) || Hoek.reach(Language.errors, type);

//   if (format === undefined) {
//     return `Error code "${type}" is not defined, your custom type is missing the correct language definition`;
//   }

//   let wrapArrays = Hoek.reach(localized, 'messages.wrapArrays');
//   if (typeof wrapArrays !== 'boolean') {
//     wrapArrays = Language.errors.messages.wrapArrays;
//   }

//   if (format === null) {
//     const childrenString = _stringify(context.reason, wrapArrays);
//     if (wrapArrays) {
//       return childrenString.slice(1, -1);
//     }

//     return childrenString;
//   }

//   const hasKey = /{{!?label}}/.test(format);
//   const skipKey = format.length > 2 && format[0] === '!' && format[1] === '!';

//   if (skipKey) {
//     format = format.slice(2);
//   }

//   if (!hasKey && !skipKey) {
//     const localizedKey = Hoek.reach(localized, 'key');
//     if (typeof localizedKey === 'string') {
//       format = localizedKey + format;
//     }
//     else {
//       format = Hoek.reach(Language.errors, 'key') + format;
//     }
//   }

//   const message =  format.replace(/{{(!?)([^}]+)}}/g, ($0, isSecure, name) => {
//     const value = Hoek.reach(context, name);
//     const normalized = _stringify(value, wrapArrays);
//     return (isSecure && options.escapeHtml ? Hoek.escapeHtml(normalized) : normalized);
//   });

//   return message;
// }

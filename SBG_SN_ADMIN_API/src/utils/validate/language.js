export default {
  root: 'value',
  key: '"{{!label}}" ',
  messages: {
    wrapArrays: true
  },
  any: {
    unknown: 'không được phép', // 'is not allowed',
    invalid: 'chứa giá trị không hợp lệ', // 'contains an invalid value',
    empty: 'không được phép trống', // 'is not allowed to be empty',
    required: 'là bắt buộc', // 'is required',
    allowOnly: 'phải là một trong {{valids}}', // 'must be one of {{valids}}',
    default: 'threw an error when running default method'
  },
  alternatives: {
    base: 'không phù hợp với bất kỳ lựa chọn thay thế nào', // 'not matching any of the allowed alternatives',
    child: null
  },
  array: {
    base: 'phải là một mảng', // 'must be an array',
    includes: 'at position {{pos}} does not match any of the allowed types',
    includesSingle: 'single value of "{{!label}}" does not match any of the allowed types',
    includesOne: 'at position {{pos}} fails because {{reason}}',
    includesOneSingle: 'single value of "{{!label}}" fails because {{reason}}',
    includesRequiredUnknowns: 'does not contain {{unknownMisses}} required value(s)',
    includesRequiredKnowns: 'does not contain {{knownMisses}}',
    includesRequiredBoth: 'does not contain {{knownMisses}} and {{unknownMisses}} other required value(s)',
    excludes: 'at position {{pos}} contains an excluded value',
    excludesSingle: 'single value of "{{!label}}" contains an excluded value',
    hasKnown: 'does not contain at least one required match for type "{{!patternLabel}}"',
    hasUnknown: 'does not contain at least one required match',
    min: 'phải chứa ít nhất {{limit}} phần tử', // 'must contain at least {{limit}} items',
    max: 'phải chứa ít hơn hoặc bằng {{limit}} phần tử', // 'must contain less than or equal to {{limit}} items',
    length: 'phải chứa đúng {{limit}} phần tử', // 'must contain {{limit}} items',
    ordered: 'at position {{pos}} fails because {{reason}}',
    orderedLength: 'at position {{pos}} fails because array must contain at most {{limit}} items',
    ref: 'references "{{ref}}" which is not a positive integer',
    sparse: 'must not be a sparse array',
    unique: 'có chứa giá trị trùng lặp tại vị trí {{pos}}', // 'position {{pos}} contains a duplicate value'
    itemMax: '!!Phần tử thứ "{{pos}}" của "{{label}}" không được vượt quá {{limit}} ký tự'
  },
  boolean: {
    base: 'phải là kiểu "boolean"', // 'must be a boolean'
  },
  binary: {
    base: 'must be a buffer or a string',
    min: 'phải có kích thước nhỏ nhất là {{limit}} byte', // 'must be at least {{limit}} bytes',
    max: 'phải có kích thước nhỏ hơn hoặc bằng {litmit}} byte', // 'must be less than or equal to {{limit}} bytes',
    length: 'phải có kích thước là {{limit}} byte', // 'must be {{limit}} bytes'
  },
  date: {
    base: 'must be a number of milliseconds or valid date string',
    strict: 'must be a valid date',
    min: 'must be larger than or equal to "{{limit}}"',
    max: 'must be less than or equal to "{{limit}}"',
    less: 'must be less than "{{limit}}"',
    greater: 'must be greater than "{{limit}}"',
    isoDate: 'must be a valid ISO 8601 date',
    timestamp: {
      javascript: 'must be a valid timestamp or number of milliseconds',
      unix: 'must be a valid timestamp or number of seconds'
    },
    ref: 'references "{{ref}}" which is not a date',
    format: 'ngày tháng không đúng định dạng {{format}}'
  },
  function: {
    base: 'must be a Function',
    arity: 'must have an arity of {{n}}',
    minArity: 'must have an arity greater or equal to {{n}}',
    maxArity: 'must have an arity lesser or equal to {{n}}',
    ref: 'must be a Joi reference',
    class: 'must be a class'
  },
  lazy: {
    base: '!!schema error: lazy schema must be set',
    schema: '!!schema error: lazy schema function must return a schema'
  },
  object: {
    base: 'must be an object',
    child: '!!child "{{!child}}" fails because {{reason}}',
    min: 'must have at least {{limit}} children',
    max: 'must have less than or equal to {{limit}} children',
    length: 'must have {{limit}} children',
    allowUnknown: '!!"{{!child}}" is not allowed',
    with: '!!"{{mainWithLabel}}" missing required peer "{{peerWithLabel}}"',
    without: '!!"{{mainWithLabel}}" conflict with forbidden peer "{{peerWithLabel}}"',
    missing: 'must contain at least one of {{peersWithLabels}}',
    xor: 'contains a conflict between exclusive peers {{peersWithLabels}}',
    oxor: 'contains a conflict between optional exclusive peers {{peersWithLabels}}',
    and: 'contains {{presentWithLabels}} without its required peers {{missingWithLabels}}',
    nand: '!!"{{mainWithLabel}}" must not exist simultaneously with {{peersWithLabels}}',
    assert: '!!"{{ref}}" validation failed because "{{ref}}" failed to {{message}}',
    rename: {
      multiple: 'cannot rename child "{{from}}" because multiple renames are disabled and another key was already renamed to "{{to}}"',
      override: 'cannot rename child "{{from}}" because override is disabled and target "{{to}}" exists',
      regex: {
        multiple: 'cannot rename children {{from}} because multiple renames are disabled and another key was already renamed to "{{to}}"',
        override: 'cannot rename children {{from}} because override is disabled and target "{{to}}" exists'
      }
    },
    type: 'must be an instance of "{{type}}"',
    schema: 'must be a Joi instance'
  },
  number: {
    base: 'phải là số', // 'must be a number',
    unsafe: 'must be a safe number',
    min: 'phải lớn hơn hoặc bằng {{litmit}}', // 'must be larger than or equal to {{limit}}',
    max: 'phải nhỏ hơn hoặc bằng {{limit}}', // 'must be less than or equal to {{limit}}',
    less: 'phải nhỏ hơn {{limit}}', // 'must be less than {{limit}}',
    greater: 'phải lớn hơn {{limit}}', // 'must be greater than {{limit}}',
    integer: 'phải là một số nguyên', // 'must be an integer',
    negative: 'phải là số âm', // 'must be a negative number',
    positive: 'phải là số dương', // 'must be a positive number',
    precision: 'phải không có nhiều hơn {{limit}} số thập phân', // 'must have no more than {{limit}} decimal places',
    ref: 'references "{{ref}}" which is not a number',
    multiple: 'phải là bộ số của {{multiple}}', // 'must be a multiple of {{multiple}}',
    port: 'phải là số cổng hợp lệ', // 'must be a valid port'
  },
  string: {
    base: 'phải là một "string"', // 'must be a string',
    min: 'phải có ít nhất là {{limit}} ký tự', // 'length must be at least {{limit}} characters long',
    max: 'phải có nhiều nhất là {{limit}} ký tự', // 'length must be less than or equal to {{limit}} characters long',
    length: 'phải có {{limit}} ký tự', //  'length must be {{limit}} characters long',
    alphanum: 'chỉ được chứa ký tự chữ và số', // 'must only contain alpha-numeric characters',
    token: 'chỉ được chứa ký tự chữ, số và dấu "_"', // 'must only contain alpha-numeric and underscore characters',
    regex: {
      base: 'with value "{{!value}}" fails to match the required pattern: {{pattern}}',
      name: 'with value "{{!value}}" fails to match the {{name}} pattern',
      invert: {
        base: 'with value "{{!value}}" matches the inverted pattern: {{pattern}}',
        name: 'with value "{{!value}}" matches the inverted {{name}} pattern'
      }
    },
    email: 'không đúng định dạng email', // 'must be a valid email',
    uri: 'không đúng định dạng URI', // 'must be a valid uri',
    uriRelativeOnly: 'must be a valid relative uri',
    uriCustomScheme: 'must be a valid uri with a scheme matching the {{scheme}} pattern',
    isoDate: 'must be a valid ISO 8601 date',
    guid: 'must be a valid GUID',
    hex: 'must only contain hexadecimal characters',
    hexAlign: 'hex decoded representation must be byte aligned',
    base64: 'must be a valid base64 string',
    dataUri: 'must be a valid dataUri string',
    hostname: 'must be a valid hostname',
    normalize: 'must be unicode normalized in the {{form}} form',
    lowercase: 'must only contain lowercase characters',
    uppercase: 'must only contain uppercase characters',
    trim: 'must not have leading or trailing whitespace',
    creditCard: 'must be a credit card',
    ref: 'references "{{ref}}" which is not a number',
    ip: 'must be a valid ip address with a {{cidr}} CIDR',
    ipVersion: 'must be a valid ip address of one of the following versions {{version}} with a {{cidr}} CIDR'
  },
  symbol: {
    base: 'must be a symbol',
    map: 'must be one of {{map}}'
  }
};

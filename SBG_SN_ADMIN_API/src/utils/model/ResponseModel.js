import camelcaseKeys from 'camelcase-keys';

export default class ResponseModel {
  constructor(success, data, message, messages, errors, errorType) {
    this.success = !!success;
    this.data = data ? _camelcaseKeys(data) : null;
    this.message = message ? message : null;
    this.messages = messages ? messages : null;
    this.errors = errors ? errors : null;
    this.errorType = errorType ? errorType : null;
  }
}

function _camelcaseKeys(data) {
  if (typeof data === 'object') {
    return camelcaseKeys(data, { deep: true });
  } else {
    return data;
  }
}

// export default function ResponseModel(success, data, message, messages, errors, errorType) {
//   return {
//     success: !!success,
//     data: !!data ? camelcaseKeys(data, { deep: true }) : undefined,
//     message: message,
//     messages: messages,
//     errors: errors,
//     errorType: errorType
//   };
// }

import Boom from 'boom';
import ErrorType from '../../data/enums/ErrorType';

// export default function(type, message, data) {
//   // const error = new Boom(new Error(message), {
//   //   statusCode: 400,
//   //   data: data
//   // });
//   const error = Boom.badRequest(message, data);
//   error.errorType = type;
//   return error;
// }

export default class ApiError {
  static warningError(message, data, innerError) {
    const error = Boom.badRequest(message, data);
    error.errorType = ErrorType.WARNING;
    error.innerError = innerError;
    return error;
  }

  static bussinessError(message, data, innerError) {
    const error = Boom.badRequest(message, data);
    error.errorType = ErrorType.ERROR;
    error.innerError = innerError;
    return error;
  }

  static unauthorizedError(message, data, innerError) {
    const error = Boom.unauthorized(message, data);
    error.errorType = ErrorType.FORBIDDEN;
    error.innerError = innerError;
    return error;
  }

  static notFound(message, data, innerError) {
    const error = Boom.notFound(message, data);
    error.errorType = ErrorType.ERROR;
    error.innerError = innerError;
    return error;
  }

  static error(err, data, innerError) {
    let error;
    if (Boom.isBoom(err)) {
      error = new Boom(err, { data: data });
    } else {
      error = new Boom(err, { statusCode: 400, data: data });
    }
    error.errorType = ErrorType.WARNING;
    error.innerError = innerError;
    return error;
  }

  static isBoom(err) {
    return Boom.isBoom(err);
  }
}

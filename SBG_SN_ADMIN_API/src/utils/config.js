import _ from 'lodash';
import config from '../config.json';

export default (function() {
  const defaultConfig = config.development;
  /* eslint-disable */
  const environment = process.env.NODE_ENV || 'development';
  const environmentConfig = config[environment];
  const finalConfig = _.merge(defaultConfig, environmentConfig);
  global.CONFIG = finalConfig;
})();

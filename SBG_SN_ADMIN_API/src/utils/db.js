import { connect } from 'mongoose';
import {
  Action,
  Role,
  Token,
  User,
  CmsMenu,
  RoleAction,
  SoccerTeam,
  SoccerCompetitionBase,
  SoccerStage,
  SoccerCompetition,
  SoccerCompetitionTeam,
  SoccerCompetitionStage,
  SoccerCompetitionStageTeam,
  SoccerCompetitionStageStanding,
  SoccerCompetitionStageStandingTable,
  SoccerRound,
  SoccerMatch,
  SoccerMatchStream,
  AdsPartner,
  Advertising,
  Service,
  Package,
  AdsConfiguration,
  AdsConfigurationDetail,
} from '../data/models';

// const mongodbURI = 'mongodb://sbg:abcde12345-@192.168.1.203/SBG_NEWS';
// const mongodbURI = 'mongodb://192.168.1.9:27017,192.168.1.9:27018,192.168.1.9:27019/SBG_NEWS?replicaSet=rs0';
// const mongodbURI = 'mongodb://192.168.1.9:27018/SBG_NEWS';
const mongodbURI = global.CONFIG.DB_URI;

const options = {
  useNewUrlParser: true,
  useCreateIndex: true
};

// if(mongodbURI.indexOf('replicaSet') > -1) {
//   options.db = {native_parser: true};
//   options.replset = {
//     socketTimeoutMS: 0,
//     keepAlive: true,
//     reconnectTries: 30
//   };
//   options.server = {
//     poolSize: 5,
//     socketOptions: {
//       keepAlive: 1000,
//       connectTimeoutMS: 30000
//     }
//   };
// }

const con = connect(mongodbURI, options, (error) => {
  if (error) {
    throw new Error('Error: Not connect to DB');
  }
  /* eslint-disable */
  console.log('Connected to DB');
  /* Create Collection Database */
  Action.createCollection();
  RoleAction.createCollection();
  Role.createCollection();
  CmsMenu.createCollection();
  Token.createCollection();
  User.createCollection();
  SoccerCompetition.createCollection();
  SoccerCompetitionBase.createCollection();
  SoccerCompetitionStage.createCollection();
  SoccerCompetitionStageStanding.createCollection();
  SoccerCompetitionStageStandingTable.createCollection();
  SoccerCompetitionStageTeam.createCollection();
  SoccerCompetitionTeam.createCollection();
  SoccerMatch.createCollection();
  SoccerMatchStream.createCollection();
  SoccerRound.createCollection();
  SoccerStage.createCollection();
  SoccerTeam.createCollection();
  AdsPartner.createCollection();
  Advertising.createCollection();
  Service.createCollection();
  Package.createCollection();
  AdsConfiguration.createCollection();
  AdsConfigurationDetail.createCollection();
});

export default con;

import ResponseModel from './model/ResponseModel';
import { LanguageKeys } from '../data/constants';
import ErrorType from '../data/enums/ErrorType';
import logger from './log';
import { translate } from './translate';

const Messages = LanguageKeys.messages;

export default function (req, h) {
  // const lang_messages = req.i18n.__('messages');
  const res = req.response;
  // if there's no Boom error, don't bother checking further down
  if (!res.isBoom) {
    if (res.source && res.source.message) {
      res.source.message = translate(res.source.message); //Hoek.reach(lang_messages, res.source.message) || res.source.message;
    }
    return h.continue;
  }

  logger.error(res);

  if (!res.isJoi) {

    // handling error when checking scope
    if (req.auth.isAuthenticated &&
        !req.auth.isAuthorized &&
        res.output.payload.statusCode === 403 &&
        res.output.payload.message === 'Insufficient scope') {

      res.output.payload =
          new ResponseModel(false,
                            res.data,
                            translate(Messages.auth.INSUFFICIENT_SCOPE), // Hoek.reach(lang_messages, Messages.auth.INSUFFICIENT_SCOPE) || Messages.auth.INSUFFICIENT_SCOPE,
                            null,
                            null,
                            ErrorType.FORBIDDEN);

      return h.continue;
    }

    // other errors
    res.output.payload =
        new ResponseModel(false,
                          null,
                          translate(res.message), // Hoek.reach(lang_messages, res.message) || res.message,
                          null,
                          null,
                          res.errorType ? res.errorType :  ErrorType.ERROR);
    return h.continue;
  }

  // handling joi errors
  let errors = [];
  let messages = [];
  if (res.details != null && res.details.length > 0) {
    errors = res.details.map(d => {
      return {
        path: d.path,
        key: d.path.join('_'),
        label: d.context.label,
        message: d.message
      };
    });

    messages = errors.map(e => e.message);
  }
  res.output.payload = new ResponseModel(false,
                      null,
                      translate(Messages.global.INVALID_INPUT), // Hoek.reach(lang_messages, Messages.global.INVALID_INPUT) || Messages.global.INVALID_INPUT,
                      messages,
                      errors,
                      ErrorType.WARNING);

  return h.continue;
}

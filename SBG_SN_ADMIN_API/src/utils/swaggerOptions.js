import { DocsPage, Routers } from '../data/constants';

function _getTags(routers) {
  return Object.keys(routers).map(k => {
    return {
      'name': routers[k].NAME,
      'description': routers[k].DESCRIPTION
    };
  });
}

export default {
  info: {
    title: DocsPage.TITLE,
    version: DocsPage.VERSION,
  },
  basePath: '/api/v1',
  documentationPath: DocsPage.PATH,
  grouping: 'tags',
  tagsGroupingFilter: (tag) => tag !== 'api',
  tags: _getTags(Routers)
};

import I18n from 'i18n';
import ApiError from '../model/ApiError';
import Hoek from 'hoek';
import _ from 'lodash';

exports.lang = {};

exports.extractDefaultLocale = function(allLocales){
  if (!allLocales) {
    throw ApiError.error('No locales defined!');
  }
  if (allLocales.length === 0) {
    throw ApiError.error('Locales array is empty!');
  }
  return allLocales[0];
};

exports.plugin = {
  name: 'api-i18n',
  version: '1.0.0',
  register: function(server, options) {
    var pluginOptions = {};
    if (options) {
      pluginOptions = options;
    }
    I18n.configure(pluginOptions);

    var defaultLocale = pluginOptions.defaultLocale || exports.extractDefaultLocale(pluginOptions.locales);

    if (!pluginOptions.locales) {
      throw Error('No locales defined!');
    }

    server.ext('onRequest', function (req, h) {
      req.i18n = {};
      I18n.init(req, req.i18n);
      req.i18n.setLocale(defaultLocale);
      if (req.params && req.params.languageCode) {
        if (_.includes(pluginOptions.locales, req.params.languageCode) == false) {
          throw ApiError.notFound('No localization available for ' + req.params.languageCode);
        }
        req.i18n.setLocale(req.params.languageCode);
      } else if (pluginOptions.queryParameter && req.query && req.query[pluginOptions.queryParameter]) {
        if (_.includes(pluginOptions.locales, req.query[pluginOptions.queryParameter]) == false) {
          throw ApiError.notFound('No localization available for ' + req.query[pluginOptions.queryParameter]);
        }
        req.i18n.setLocale(req.query[pluginOptions.queryParameter]);
      } else if (pluginOptions.languageHeaderField && req.headers[pluginOptions.languageHeaderField]) {
        var languageCode = req.headers[pluginOptions.languageHeaderField];
        if (languageCode) {
          req.i18n.setLocale(languageCode);
        }
      }

      // set for lang;
      exports.lang = Object.assign({}, req.i18n);

      return h.continue;
    });

    server.ext('onPreResponse', function (req, h) {
      if (!req.i18n || !req.response) {
        return h.continue;
      }
      var response = req.response;

      if (ApiError.isBoom(response)) {
        return response;
      }

      if (response.variety === 'view') {
        response.source.context = Hoek.merge(response.source.context || {}, req.i18n);
        response.source.context.languageCode = req.i18n.getLocale();
      }
      return h.continue;
    });
  }
};

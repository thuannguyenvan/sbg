import { lang } from '../api-i18n';
import Hoek from 'hoek';

export function validateTemplate(template, ...keys) {
  const ret = {
    template: template,
    keys: keys,
    isValidateTemplate: true
  };
  return JSON.stringify(ret);
}

export function stringTemplate(template, ...keys) {
  const ret = {
    template: template,
    keys: keys,
    isValidateTemplate: false
  };
  return JSON.stringify(ret);
}

export function translate(stringTemplate, category = 'messages', validateTemplate = false, categoryValidate = 'validate') {
  let isValidateTemplate = false;
  if (_stringIsJson(stringTemplate)) {
    const template = JSON.parse(stringTemplate);
    isValidateTemplate = template.isValidateTemplate;
  }

  if (!validateTemplate && !isValidateTemplate) {
    return _translateTemplate(stringTemplate, category);
  } else {
    return _translateValidateTemplate(stringTemplate, categoryValidate);
  }
}

function _translateValidateTemplate(stringTemplate, category = 'validate') {
  const wrapArrays = true;
  const escapeHtml = false;

  if (!lang.__) {
    return stringTemplate;
  }

  const languageValidate = lang.__(category);
  if (!languageValidate) {
    return stringTemplate;
  }

  if (!_stringIsJson(stringTemplate)) {
    return Hoek.reach(languageValidate.messageTemplates, stringTemplate) || stringTemplate;
  }

  const validateTemplate = JSON.parse(stringTemplate);

  let template = validateTemplate.template;
  template = Hoek.reach(languageValidate.messageTemplates, template) || template;

  const keys = validateTemplate.keys;

  const context = {};
  for (let i = 0; i < keys.length; i++) {
    const key = i.toString();
    context[key] = Hoek.reach(languageValidate.labels, keys[key]) || keys[key];
  }

  const message = template.replace(/{(!?)([^}]+)}/g, ($0, isSecure, name) => {
    const value = Hoek.reach(context, name);
    const normalized = _stringify(value, wrapArrays);
    return (isSecure && escapeHtml ? Hoek.escapeHtml(normalized) : normalized);
  });

  return message;
}

export function _translateTemplateTest(stringTemplate, category = 'messages') {
  const wrapArrays = true;
  const escapeHtml = false;
  const resouce = require('../../locales/vi.json');
  const messages = resouce[category];
  if (!messages) {
    return stringTemplate;
  }

  if (!_stringIsJson(stringTemplate)) {
    return Hoek.reach(messages, stringTemplate) || stringTemplate;
  }

  const stringTemplateObj = JSON.parse(stringTemplate);
  const template = Hoek.reach(messages, stringTemplateObj.template)
    || stringTemplateObj.template;
  const keys = stringTemplateObj.keys;
  const context = {};
  for (let i = 0; i < keys.length; i++) {
    const key = i.toString();
    context[key] = keys[i];
    // context[key] = Hoek.reach(languageValidate.labels, context[key]) || context[key];
  }
  const message = template.replace(/{(!?)([^}]+)}/g, ($0, isSecure, name) => {
    const value = Hoek.reach(context, name);
    const normalized = _stringify(value, wrapArrays);
    return (isSecure && escapeHtml ? Hoek.escapeHtml(normalized) : normalized);
  });
  return message;
}

function _translateTemplate(stringTemplate, category = 'messages') {
  const wrapArrays = true;
  const escapeHtml = false;

  if (!lang.__) {
    return stringTemplate;
  }

  const messages = lang.__(category);
  if (!messages) {
    return stringTemplate;
  }

  if (!_stringIsJson(stringTemplate)) {
    return Hoek.reach(messages, stringTemplate) || stringTemplate;
  }

  const stringTemplateObj = JSON.parse(stringTemplate);
  const template = Hoek.reach(messages, stringTemplateObj.template)
    || stringTemplateObj.template;
  const keys = stringTemplateObj.keys;
  const context = {};
  for (let i = 0; i < keys.length; i++) {
    const key = i.toString();
    context[key] = keys[i];
    // context[key] = Hoek.reach(languageValidate.labels, context[key]) || context[key];
  }
  const message = template.replace(/{(!?)([^}]+)}/g, ($0, isSecure, name) => {
    const value = Hoek.reach(context, name);
    const normalized = _stringify(value, wrapArrays);
    return (isSecure && escapeHtml ? Hoek.escapeHtml(normalized) : normalized);
  });
  return message;
}

function _stringify(value, wrapArrays) {
  const type = typeof value;

  if (value === null) {
    return 'null';
  }

  if (type === 'string') {
    return value;
  }

  if (type === 'function' || type === 'symbol') {
    return value.toString();
  }

  if (type === 'object') {
    if (Array.isArray(value)) {
      let partial = '';
      for (let i = 0; i < value.length; ++i) {
        partial = partial + (partial.length ? ', ' : '') + _stringify(value[i], wrapArrays);
      }
      return wrapArrays ? '[' + partial + ']' : partial;
    }
    return value.toString();
  }
  return JSON.stringify(value);
}

function _stringIsJson(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}


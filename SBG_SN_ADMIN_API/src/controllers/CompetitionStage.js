import ResponseModel from '../utils/model/ResponseModel';
import { LanguageKeys} from '../data/constants';
import ApiError from '../utils/model/ApiError';
import * as competitionStageService from '../services/CompetitionStageService';

const Messages = LanguageKeys.messages;

const createStanding = async function(req, h) {
  try {
    const model = {};
    model.soccerCompetitionStageId = req.params.id;
    const standingId = await competitionStageService.createStanding(model);
    const res = new ResponseModel(true, standingId, Messages.competitionStage.CREATE_STANDING_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.competitionStage.CREATE_STANDING_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const deleteStanding = async function(req, h) {
  try {
    const model = {};
    model.soccerCompetitionStageId = req.params.id;
    const standingId = await competitionStageService.deleteStanding(model);
    const res = new ResponseModel(true, standingId, Messages.competitionStage.DELETE_STANDING_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.competitionStage.DELETE_STANDING_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const getConfigTeamInfo = async function(req, h) {
  try {
    const model = {};
    model.soccerCompetitionStageId = req.params.id;
    const info = await competitionStageService.getConfigTeamInfo(model);
    const res = new ResponseModel(true, info, Messages.competitionStage.GET_CONFIG_TEAM_INFO_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.competitionStage.GET_CONFIG_TEAM_INFO_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const configTeam = async function(req, h) {
  try {
    const model = {};
    model.soccerCompetitionStageId = req.params.id;
    model.soccerCompetitionStageTeams = req.payload.soccerCompetitionStageTeams;
    const info = await competitionStageService.configTeam(model);
    const res = new ResponseModel(true, info, Messages.competitionStage.CONFIG_TEAM_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.competitionStage.CONFIG_TEAM_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const getStandingTables = async function(req, h) {
  try {
    const model = {};
    model.soccerCompetitionStageId = req.params.id;
    const info = await competitionStageService.getStandingTables(model);
    const res = new ResponseModel(true, info, Messages.competitionStage.GET_STANDING_TABLES_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.competitionStage.GET_STANDING_TABLES_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const updateStanding = async function(req, h) {
  try {
    const model = req.payload;
    model.soccerCompetitionStageId = req.params.id;
    await competitionStageService.updateStanding(model);
    const res = new ResponseModel(true, null, Messages.competitionStage.UPDATE_STANDING_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.competitionStage.UPDATE_STANDING_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const getByCompetition = async function(req, h) {
  try {
    const model = req.query;
    if (req.params.soccerCompetitionId) {
      model.soccerCompetitionId = req.params.soccerCompetitionId;
    }
    const stages = await competitionStageService.getByCompetition(model);
    const res = new ResponseModel(true, stages, Messages.global.GET_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.global.GET_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

export {
  createStanding,
  deleteStanding,
  getConfigTeamInfo,
  configTeam,
  getStandingTables,
  updateStanding,
  getByCompetition
};

import { SearchUser, GetUserRoles, UpdateUserRoles, ActivateDeactiveUser } from '../services/UserService';
import { LanguageKeys} from '../data/constants';
import ResponseModel from '../utils/model/ResponseModel';

const Messages = LanguageKeys.messages;

const searchUser = async function(req, h) {
  const model = req.payload;

  if (req.auth &&
      req.auth.credentials &&
      req.auth.credentials.scope) {

        model.userActions = req.auth.credentials.scope;
  } else {
    model.userActions = [];
  }

  const users = await SearchUser(model);
  const res = new ResponseModel(true, users, Messages.user.SEARCH_USER_SUCCESS);
  return h.response(res);
};

const getUserRoles = async function(req, h) {
  const userId = req.params.id;
  const userRoles = await GetUserRoles({ userId: userId });
  const res = new ResponseModel(true, userRoles, Messages.user.GET_USER_ROLES_SUCCESS);
  return h.response(res);
};

const updateUserRoles = async function(req, h) {
  const model = {
    userId: req.params.id,
    roles: req.payload.roles
  };
  const updatedUserRoles = await UpdateUserRoles(model);
  const res = new ResponseModel(true, updatedUserRoles, Messages.user.UPDATE_USER_ROLES_SUCCESS);
  return h.response(res);
};

const activateUser = async function(req, h) {
  const model = {
    userId: req.params.id,
    isActivate: true
  };
  const activateUser = await ActivateDeactiveUser(model);
  const res = activateUser
              ? new ResponseModel(true, null, Messages.user.ACTIVATE_USER_SUCCESS)
              : new ResponseModel(false, null, Messages.user.ACTIVATE_USER_FAIL);
  return h.response(res);
};

const deactivateUser = async function(req, h) {
  const model = {
    userId: req.params.id,
    isActivate: false
  };
  const deactivateUser = await ActivateDeactiveUser(model);
  const res = deactivateUser
              ? new ResponseModel(true, null, Messages.user.DEACTIVATE_USER_SUCCESS)
              : new ResponseModel(false, null, Messages.user.DEACTIVATE_USER_FAIL);
  return h.response(res);
};

export {
  searchUser,
  getUserRoles,
  updateUserRoles,
  activateUser,
  deactivateUser
};

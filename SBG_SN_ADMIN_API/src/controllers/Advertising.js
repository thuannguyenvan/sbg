import ResponseModel from '../utils/model/ResponseModel';
import { LanguageKeys} from '../data/constants';
import ApiError from '../utils/model/ApiError';
import * as advertisingService from '../services/AdvertisingService';

const Messages = LanguageKeys.messages;

const getSearchInfo = async function(req, h) {
  try {
    const info = await advertisingService.getSearchInfo();
    const res = new ResponseModel(true, info, Messages.advertising.GET_SEARCH_INFO_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.advertising.GET_SEARCH_INFO_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const searchAdvertising = async function(req, h) {
  try {
    const model = req.payload;

    if (req.auth &&
        req.auth.credentials &&
        req.auth.credentials.scope) {

        model.userActions = req.auth.credentials.scope;
    } else {
      model.userActions = [];
    }

    const advertisings = await advertisingService.searchAdvertising(model);
    const res = new ResponseModel(true, advertisings, Messages.advertising.SEARCH_ADVERTISING_SUCCES);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.advertising.SEARCH_ADVERTISING_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const getAddInfo = async function(req, h) {
  try {
    const info = await advertisingService.getAddInfo();
    const res = new ResponseModel(true, info, Messages.advertising.GET_ADD_INFO_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.advertising.GET_ADD_INFO_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const addAdvertising = async function(req, h) {
  try {
    const model = req.payload;
    const addAdvertisingId = await advertisingService.addAdvertising(model);
    const res = new ResponseModel(true, addAdvertisingId, Messages.advertising.ADD_ADVERTISING_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.advertising.ADD_ADVERTISING_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const deleteAdvertising = async function(req, h) {
  try {
    const model = {};
    model.advertisingId = req.params.id;
    await advertisingService.deleteAdvertising(model);
    const res = new ResponseModel(true, null, Messages.advertising.DELETE_ADVERTISING_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.advertising.DELETE_ADVERTISING_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const getEditInfo = async function(req, h) {
  try {
    const model = {};
    model.advertisingId = req.params.id;
    const info = await advertisingService.getEditInfo(model);
    const res = new ResponseModel(true, info, Messages.advertising.GET_EDIT_INFO_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.advertising.GET_EDIT_INFO_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const updateAdvertising = async function(req, h) {
  try {
    const model = req.payload;
    model.advertisingId = req.params.id;
    await advertisingService.updateAdvertising(model);
    const res = new ResponseModel(true, null, Messages.advertising.UPDATE_ADVERTISING_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.advertising.UPDATE_ADVERTISING_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const activateAdvertising = async function(req, h) {
  try {
    const model = {
      advertisingId: req.params.id,
      isActivate: true
    };
    await advertisingService.activateDeactivateAdvertising(model);
    const res = new ResponseModel(true, null, Messages.advertising.ACTIVATE_ADVERTISING_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.advertising.ACTIVATE_ADVERTISING_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const deactivateAdvertising = async function(req, h) {
  try {
    const model = {
      advertisingId: req.params.id,
      isActivate: false
    };
    await advertisingService.activateDeactivateAdvertising(model);
    const res = new ResponseModel(true, null, Messages.advertising.DEACTIVATE_ADVERTISING_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.advertising.DEACTIVATE_ADVERTISING_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const getSearchActiveAdvertisingInfo = async function(req, h) {
  try {
    const info = await advertisingService.getSearchActiveAdvertisingInfo();
    const res = new ResponseModel(true, info, Messages.advertising.GET_SEARCH_ACTIVE_ADVERTISING_INFO_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.advertising.GET_SEARCH_ACTIVE_ADVERTISING_INFO_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const searchActiveAdvertising = async function(req, h) {
  try {
    const model = req.payload;
    const advertisings = await advertisingService.searchActiveAdvertising(model);
    const res = new ResponseModel(true, advertisings, Messages.advertising.SEARCH_ACTIVE_ADVERTISING_SUCCES);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.advertising.SEARCH_ACTIVE_ADVERTISING_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

export {
  getSearchInfo,
  searchAdvertising,
  getAddInfo,
  addAdvertising,
  deleteAdvertising,
  getEditInfo,
  updateAdvertising,
  activateAdvertising,
  deactivateAdvertising,
  getSearchActiveAdvertisingInfo,
  searchActiveAdvertising,
};

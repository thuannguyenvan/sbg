import * as roundService from '../services/RoundService';
import { LanguageKeys} from '../data/constants';
import ApiError from '../utils/model/ApiError';
import ResponseModel from '../utils/model/ResponseModel';

const Messages = LanguageKeys.messages;

const searchRound = async function(req, h) {
  try {
    const model = req.payload;

    if (req.auth &&
        req.auth.credentials &&
        req.auth.credentials.scope) {

        model.userActions = req.auth.credentials.scope;
    } else {
      model.userActions = [];
    }

    const rounds = await roundService.searchRound(model);
    const res = new ResponseModel(true, rounds, Messages.round.SEARCH_ROUND_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.round.SEARCH_ROUND_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const addRound = async function(req, h) {
  try {
    const model = req.payload;
    const round = await roundService.addRound(model);
    const res = new ResponseModel(true, round, Messages.round.CREATE_ROUND_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.round.CREATE_ROUND_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const getEditInfo = async function(req, h) {
  try {
    const model = {};
    model.soccerRoundId  = req.params.id;
    const info = await roundService.getEditInfo(model);
    const res = new ResponseModel(true, info, Messages.round.GET_EDIT_ROUND_INFO_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.round.GET_EDIT_ROUND_INFO_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const updateRound = async function(req, h) {
  try {
    const model = req.payload;
    model.soccerRoundId  = req.params.id;
    const updated = await roundService.updateRound(model);
    const res = new ResponseModel(true, updated, Messages.round.UPDATE_ROUND_INFO_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.round.UPDATE_ROUND_INFO_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const deleteRound = async function(req, h) {
  try {
    const model = {};
    model.soccerRoundId  = req.params.id;
    const deleted = await roundService.deleteRound(model);
    const res = new ResponseModel(true, deleted, Messages.round.DELETE_ROUND_INFO_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.round.DELETE_ROUND_INFO_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const getRoundByCompetition = async function(req, h) {
  try {
    const model = {};
    model.soccerCompetitionId = req.params.soccerCompetitionId;
    const rounds = await roundService.getRoundByCompetition(model);
    const res = new ResponseModel(true, rounds, Messages.round.GET_ROUND_BY_COMPETITION_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.round.GET_ROUND_BY_COMPETITION_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const getTeams = async function(req, h) {
  try {
    const model = {};
    model.soccerRoundId = req.params.id;
    const teams = await roundService.getTeams(model);
    const res = new ResponseModel(true, teams, Messages.round.GET_TEAM_IN_STAGE_OF_ROUND_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.round.GET_TEAM_IN_STAGE_OF_ROUND_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const getMatchs = async function(req, h) {
  try {
    const model = {};
    model.soccerRoundId = req.params.id;
    const matchs = await roundService.getMatchs(model);
    const res = new ResponseModel(true, matchs, Messages.round.GET_MATCHS_IN_ROUND_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.round.GET_MATCHS_IN_ROUND_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

export {
  searchRound,
  addRound,
  getEditInfo,
  updateRound,
  deleteRound,
  getRoundByCompetition,
  getTeams,
  getMatchs
};

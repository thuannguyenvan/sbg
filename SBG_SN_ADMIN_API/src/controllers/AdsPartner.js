import ResponseModel from '../utils/model/ResponseModel';
import { LanguageKeys} from '../data/constants';
import ApiError from '../utils/model/ApiError';
import * as adsPartnerService from '../services/AdsPartnerService';

const Messages = LanguageKeys.messages;

const searchAdsPartner = async function(req, h) {
  try {
    const model = req.payload;

    if (req.auth &&
        req.auth.credentials &&
        req.auth.credentials.scope) {

        model.userActions = req.auth.credentials.scope;
    } else {
      model.userActions = [];
    }

    const adsPartners = await adsPartnerService.searchAdsPartner(model);
    const res = new ResponseModel(true, adsPartners, Messages.adsPartner.SEARCH_ADS_PARTNER_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.adsPartner.SEARCH_ADS_PARTNER_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const addAdsPartner = async function(req, h) {
  try {
    const model = req.payload;
    const adsPartnerId = await adsPartnerService.addAdsPartner(model);
    const res = new ResponseModel(true, adsPartnerId, Messages.adsPartner.ADD_ADS_PARTNER_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.adsPartner.ADD_ADS_PARTNER_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const getEditInfo = async function(req, h) {
  try {
    const model = {};
    model.adsPartnerId = req.params.id;
    const info = await adsPartnerService.getEditInfo(model);
    const res = new ResponseModel(true, info, Messages.adsPartner.GET_EDIT_INFO_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.adsPartner.GET_EDIT_INFO_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const updateAdsPartner = async function(req, h) {
  try {
    const model = req.payload;
    model.adsPartnerId = req.params.id;
    await adsPartnerService.updateAdsPartner(model);
    const res = new ResponseModel(true, null, Messages.adsPartner.UPDATE_ADS_PARTNER_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.adsPartner.UPDATE_ADS_PARTNER_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const deleteAdsPartner = async function(req, h) {
  try {
    const model = {};
    model.adsPartnerId = req.params.id;
    await adsPartnerService.deleteAdsPartner(model);
    const res = new ResponseModel(true, null, Messages.adsPartner.DELETE_ADS_PARTNER_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.adsPartner.DELETE_ADS_PARTNER_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const activateAdsPartner = async function(req, h) {
  try {
    const model = {
      adsPartnerId: req.params.id,
      isActivate: true
    };
    await adsPartnerService.activateDeactivateAdsPartner(model);
    const res = new ResponseModel(true, null, Messages.adsPartner.ACTIVATE_ADS_PARTNER_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.adsPartner.ACTIVATE_ADS_PARTNER_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const deactivateAdsPartner = async function(req, h) {
  try {
    const model = {
      adsPartnerId: req.params.id,
      isActivate: false
    };
    await adsPartnerService.activateDeactivateAdsPartner(model);
    const res = new ResponseModel(true, null, Messages.adsPartner.DEACTIVATE_ADS_PARTNER_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.adsPartner.DEACTIVATE_ADS_PARTNER_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

export {
  searchAdsPartner,
  addAdsPartner,
  getEditInfo,
  updateAdsPartner,
  deleteAdsPartner,
  activateAdsPartner,
  deactivateAdsPartner,
};

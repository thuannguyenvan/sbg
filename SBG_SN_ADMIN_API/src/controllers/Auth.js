import * as authService from '../services/AuthService';
import { InitNewAction, AddRoleAction, DeleteRoleAction } from '../services/ActionService';
import { LanguageKeys } from '../data/constants';
import ResponseModel from '../utils/model/ResponseModel';
import ApiError from '../utils/model/ApiError';

const Messages = LanguageKeys.messages;

const login = async function(req, h) {
  try {
    const model = {};
    model.ssoToken = req.payload.token;
    const userInfo = await authService.login(model);
    const res = new ResponseModel(true, userInfo, Messages.auth.LOGIN_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.auth.LOGIN_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const logout = async function(req, h) {
  try {
    const model = {};
    model.tokenstring = req.payload.token;
    const token = await authService.logout(model);
    const res = new ResponseModel(true, token, Messages.auth.LOGOUT_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.auth.LOGOUT_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const getUserInfo = async function(req, h) {
  try {
    const model = {};
    model.tokenString = req.auth.credentials.token;
    const userInfo = await authService.getUserInfo(model);
    const res = new ResponseModel(true, userInfo, Messages.auth.GET_USER_INFO_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.auth.GET_USER_INFO_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const initNewAction = async function(req, h) {
  return h.response(await InitNewAction(req.payload));
};

const addRoleAction = async function(req, h) {
  return h.response(await AddRoleAction(req.payload));
};

const deleteRoleAction = async function(req, h) {
  return h.response(await DeleteRoleAction(req.payload));
};

export {
  login,
  logout,
  getUserInfo,
  initNewAction,
  addRoleAction,
  deleteRoleAction
};

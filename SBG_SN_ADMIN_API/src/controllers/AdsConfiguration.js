import ResponseModel from '../utils/model/ResponseModel';
import { LanguageKeys} from '../data/constants';
import ApiError from '../utils/model/ApiError';
import * as adsConfigurationService from '../services/AdsConfigurationService';

const Messages = LanguageKeys.messages;

const searchAdsConfiguration = async function(req, h) {
  try {
    const model = req.payload;

    if (req.auth &&
        req.auth.credentials &&
        req.auth.credentials.scope) {

        model.userActions = req.auth.credentials.scope;
    } else {
      model.userActions = [];
    }

    const advertisings = await adsConfigurationService.searchAdsConfiguration(model);
    const res = new ResponseModel(true, advertisings, Messages.adsConfiguration.SEARCH_ADS_CONFIGURATION_SUCCES);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.adsConfiguration.SEARCH_ADS_CONFIGURATION_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const addAdsConfiguration = async function(req, h) {
  try {
    const model = req.payload;
    const adsConfigurationId = await adsConfigurationService.addAdsConfiguration(model);
    const res = new ResponseModel(true, adsConfigurationId, Messages.adsConfiguration.ADD_ADS_CONFIGURATION_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.adsConfiguration.ADD_ADS_CONFIGURATION_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const getEditInfo = async function(req, h) {
  try {
    const model = {};
    model.adsConfigurationId = req.params.id;
    const info = await adsConfigurationService.getEditInfo(model);
    const res = new ResponseModel(true, info, Messages.adsConfiguration.GET_EDIT_INFO_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.adsConfiguration.GET_EDIT_INFO_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const updateAdsConfiguration = async function(req, h) {
  try {
    const model = req.payload;
    model.adsConfigurationId = req.params.id;
    await adsConfigurationService.updateAdsConfiguration(model);
    const res = new ResponseModel(true, null, Messages.adsConfiguration.UPDATE_ADS_CONFIGURATION_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.adsConfiguration.UPDATE_ADS_CONFIGURATION_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const deleteAdsConfiguration = async function(req, h) {
  try {
    const model = {};
    model.adsConfigurationId = req.params.id;
    await adsConfigurationService.deleteAdsConfiguration(model);
    const res = new ResponseModel(true, null, Messages.adsConfiguration.DELETE_ADS_CONFIGURATION_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.adsConfiguration.DELETE_ADS_CONFIGURATION_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const activateAdsConfiguration = async function(req, h) {
  try {
    const model = {};
    model.adsConfigurationId = req.params.id;
    await adsConfigurationService.activateAdsConfiguration(model);
    const res = new ResponseModel(true, null, Messages.adsConfiguration.ACTIVATE_ADS_CONFIGURATION_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.adsConfiguration.ACTIVATE_ADS_CONFIGURATION_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const deactivateAdsConfiguration = async function(req, h) {
  try {
    const model = {};
    model.adsConfigurationId = req.params.id;
    await adsConfigurationService.deactivateAdsConfiguration(model);
    const res = new ResponseModel(true, null, Messages.adsConfiguration.DEACTIVATE_ADS_CONFIGURATION_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.adsConfiguration.DEACTIVATE_ADS_CONFIGURATION_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

export {
  searchAdsConfiguration,
  addAdsConfiguration,
  getEditInfo,
  updateAdsConfiguration,
  deleteAdsConfiguration,
  activateAdsConfiguration,
  deactivateAdsConfiguration,
};

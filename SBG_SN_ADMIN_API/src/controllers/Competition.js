import ResponseModel from '../utils/model/ResponseModel';
import { LanguageKeys} from '../data/constants';
import ApiError from '../utils/model/ApiError';
import * as competitionService from '../services/CompetitionService';

const Messages = LanguageKeys.messages;

const searchCompetition = async function(req, h) {
  try {
    const model = req.payload;

    if (req.auth &&
        req.auth.credentials &&
        req.auth.credentials.scope) {

        model.userActions = req.auth.credentials.scope;
    } else {
      model.userActions = [];
    }

    const competitions = await competitionService.searchCompetition(model);
    const res = new ResponseModel(true, competitions, Messages.competition.SEARCH_COMPETITION_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.competition.SEARCH_COMPETITION_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const addCompetition = async function(req, h) {
  try {
    const model = req.payload;
    const competition = await competitionService.addCompetition(model);
    const res = new ResponseModel(true, competition, Messages.competition.ADD_COMPETITION_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.competition.ADD_COMPETITION_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const getAddCompetitionInfo = async function(req, h) {
  try {
    const info = await competitionService.getAddCompetitionInfo();
    const res = new ResponseModel(true, info, Messages.competition.GET_ADD_COMPETITION_INFO_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.competition.GET_ADD_COMPETITION_INFO_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const updateCompetition = async function(req, h) {
  try {
    const model = req.payload;
    model.soccerCompetitionId = req.params.id;
    const competition = await competitionService.updateCompetition(model);
    const res = new ResponseModel(true, competition, Messages.competition.UPDATE_COMPETITION_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.competition.UPDATE_COMPETITION_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const getEditCompetitionInfo = async function(req, h) {
  try {
    const model = {};
    model.soccerCompetitionId = req.params.id;
    const info = await competitionService.getEditCompetitionInfo(model);
    const res = new ResponseModel(true, info, Messages.competition.GET_ADD_COMPETITION_INFO_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.competition.GET_ADD_COMPETITION_INFO_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const getCompetitionStages = async function(req, h) {
  try {
    const model = {};
    model.soccerCompetitionId = req.params.id;

    if (req.auth &&
        req.auth.credentials &&
        req.auth.credentials.scope) {

        model.userActions = req.auth.credentials.scope;
    } else {
      model.userActions = [];
    }

    const competitionStages = await competitionService.getCompetitionStages(model);
    const res = new ResponseModel(true, competitionStages, Messages.competition.GET_COMPETITION_STAGE_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.competition.GET_COMPETITION_STAGE_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const deleteCompetition = async function(req, h) {
  try {
    const model = {};
    model.soccerCompetitionId = req.params.id;
    const deleted = await competitionService.deleteCompetition(model);
    const res = new ResponseModel(true, deleted, Messages.competition.DELETE_COMPETITION_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.competition.DELETE_COMPETITION_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const getCompetitions = async function(req, h) {
  try {
    const model = req.query;
    const competitions = await competitionService.getCompetitions(model);
    const res = new ResponseModel(true, competitions, Messages.competition.GET_LIST_COMPETITION_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.competition.GET_LIST_COMPETITION_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const getConfigTeamInfo = async function(req, h) {
  try {
    const model = {};
    model.soccerCompetitionId = req.params.id;
    const info = await competitionService.getConfigTeamInfo(model);
    const res = new ResponseModel(true, info, Messages.competition.GET_COMPETITION_STAGE_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.competition.GET_COMPETITION_STAGE_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const configTeams = async function(req, h) {
  try {
    const model = req.payload;
    model.soccerCompetitionId = req.params.id;
    await competitionService.configTeams(model);
    const res = new ResponseModel(true, null, Messages.competition.CONFIG_TEAM_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.competition.CONFIG_TEAM_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

export {
  searchCompetition,
  addCompetition,
  updateCompetition,
  getAddCompetitionInfo,
  getEditCompetitionInfo,
  getCompetitionStages,
  deleteCompetition,
  getCompetitions,
  getConfigTeamInfo,
  configTeams
};

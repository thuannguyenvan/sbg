import ResponseModel from '../utils/model/ResponseModel';
// import { LanguageKeys} from '../data/constants';
import ApiError from '../utils/model/ApiError';
import * as teamService from '../services/TeamService';

// const Messages = LanguageKeys.messages;

const getTeams = async function(req, h) {
  try {
    const model = req.query;
    const teams = await teamService.getTeams(model);
    const res = new ResponseModel(true, teams, 'Get teams success');
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error('Get teams fail', null, err);
    } else {
      throw err;
    }
  }
};

const searchTeams = async function(req, h) {
  try {
    const model = req.payload;
    const teams = await teamService.searchTeams(model);
    const res = new ResponseModel(true, teams, 'search team success');
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error('search team fail', null, err);
    } else {
      throw err;
    }
  }
};

export {
  getTeams,
  searchTeams
};

import * as matchService from '../services/MatchService';
import { LanguageKeys } from '../data/constants';
import ResponseModel from '../utils/model/ResponseModel';
import ApiError from '../utils/model/ApiError';

const Messages = LanguageKeys.messages;

const getSearchInfo = async function(req, h) {
  try {
    const info = await matchService.getSearchInfo();
    const res = new ResponseModel(true, info, Messages.match.GET_SEARCH_INFO_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.match.GET_SEARCH_INFO_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const searchMatch = async function(req, h) {
  try {
    const model = req.payload;

    if (req.auth &&
      req.auth.credentials &&
      req.auth.credentials.scope) {

      model.userActions = req.auth.credentials.scope;
    } else {
      model.userActions = [];
    }

    const matchs = await matchService.searchMatch(model);
    const res = new ResponseModel(true, matchs, Messages.match.SEARCH_MATCH_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.match.SEARCH_MATCH_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const getAddInfo = async function(req, h) {
  try {
    const info = await matchService.getAddInfo();
    const res = new ResponseModel(true, info, Messages.match.GET_ADD_INFO_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.match.GET_ADD_INFO_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const addMatch = async function(req, h) {
  try {
    const model = req.payload;
    const matchId = await matchService.addMatch(model);
    const res = new ResponseModel(true, matchId, Messages.match.ADD_MATCH_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.match.ADD_MATCH_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const deleteMatch = async function(req, h) {
  try {
    const model = {};
    model.soccerMatchId = req.params.id;
    await matchService.deleteMatch(model);
    const res = new ResponseModel(true, null, Messages.match.DELETE_MATCH_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.match.DELETE_MATCH_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const getEditInfo = async function(req, h) {
  try {
    const model = {};
    model.soccerMatchId = req.params.id;
    const info = await matchService.getEditInfo(model);
    const res = new ResponseModel(true, info, Messages.match.GET_EDIT_INFO_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.match.GET_EDIT_INFO_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const updateMatch = async function(req, h) {
  try {
    const model = req.payload;
    model.soccerMatchId = req.params.id;
    await matchService.updateMatch(model);
    const res = new ResponseModel(true, null, Messages.match.UPDATE_MATCH_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.match.UPDATE_MATCH_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

export {
  getSearchInfo,
  searchMatch,
  getAddInfo,
  addMatch,
  deleteMatch,
  getEditInfo,
  updateMatch
};

import * as matchStreamService from '../services/MatchStreamService';
import { LanguageKeys } from '../data/constants';
import ResponseModel from '../utils/model/ResponseModel';
import ApiError from '../utils/model/ApiError';

const Messages = LanguageKeys.messages;

const getSearchInfo = async function(req, h) {
  try {
    const info = await matchStreamService.getSearchInfo();
    const res = new ResponseModel(true, info, Messages.matchStream.GET_SEARCH_INFO_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.matchStream.GET_SEARCH_INFO_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const searchMatchStream = async function(req, h) {
  try {
    const model = req.payload;

    if (req.auth &&
      req.auth.credentials &&
      req.auth.credentials.scope) {

      model.userActions = req.auth.credentials.scope;
    } else {
      model.userActions = [];
    }

    const matchStreams = await matchStreamService.searchMatchStream(model);
    const res = new ResponseModel(true, matchStreams, Messages.matchStream.SEARCH_MATCH_STREAM_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.matchStream.SEARCH_MATCH_STREAM_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const getAddInfo = async function(req, h) {
  try {
    const model = {};
    model.soccerMatchId = req.query.soccerMatchId;
    const info = await matchStreamService.getAddInfo(model);
    const res = new ResponseModel(true, info, Messages.matchStream.GET_ADD_INFO_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.matchStream.GET_ADD_INFO_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const addMatchStream = async function(req, h) {
  try {
    const model = req.payload;
    const matchStreamId = await matchStreamService.addMatchStream(model);
    const res = new ResponseModel(true, matchStreamId, Messages.matchStream.ADD_MATCH_STREAM_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.matchStream.ADD_MATCH_STREAM_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const getEditInfo = async function(req, h) {
  try {
    const model = {};
    model.soccerMatchStreamId = req.params.id;
    const info = await matchStreamService.getEditInfo(model);
    const res = new ResponseModel(true, info, Messages.matchStream.GET_EDIT_INFO_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.matchStream.GET_EDIT_INFO_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const updateMatchStream = async function(req, h) {
  try {
    const model = req.payload;
    model.soccerMatchStreamId = req.params.id;
    await matchStreamService.updateMatchStream(model);
    const res = new ResponseModel(true, null, Messages.matchStream.UPDATE_MATCH_STREAM_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.matchStream.UPDATE_MATCH_STREAM_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const deleteMatchStream = async function(req, h) {
  try {
    const model = {};
    model.soccerMatchStreamId = req.params.id;
    await matchStreamService.deleteMatchStream(model);
    const res = new ResponseModel(true, null, Messages.matchStream.DELETE_MATCH_STREAM_SUCCESS);
    return h.response(res);
  } catch (err) {
    if (!err.errorType) {
      throw ApiError.error(Messages.matchStream.DELETE_MATCH_STREAM_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

export {
  getSearchInfo,
  searchMatchStream,
  getAddInfo,
  addMatchStream,
  getEditInfo,
  updateMatchStream,
  deleteMatchStream
};

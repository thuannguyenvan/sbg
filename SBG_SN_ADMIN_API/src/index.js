import { server as _server } from 'hapi';
import Inert from 'inert';
import Vision from 'vision';
import SwaggerPlugin from 'hapi-swagger';
import routes from './routes';
import AuthPlugin from './utils/auth/AuthPlugin';
import preResponse from './utils/preResponse';
import { DocsPage } from './data/constants';
import swaggerOptions from './utils/swaggerOptions';
import * as ApiI18n from './utils/api-i18n';
import logger, * as log from './utils/log';
import './utils/config.js';
import './utils/db';

const server = _server({
  /* eslint-disable */
  port: global.CONFIG.PORT,
  host: global.CONFIG.HOST,
  routes: {
    cors: {
      origin: ['*'],
      additionalHeaders: ['Authorization'],
      credentials: true
    }
  }
});

log.configure(global.CONFIG.LOG_PATH);

// adding onPreResponse event to handling errors
server.ext('onPreResponse', preResponse);

const init = async () => {
  await server.register(Inert);
  await server.register(Vision);
  await server.register({
    plugin: SwaggerPlugin,
    options: swaggerOptions
  });

  await server.register(AuthPlugin);

  await server.register({
    plugin: ApiI18n.plugin,
    options: {
      locales: global.CONFIG.SUPPORTED_LANGUAGE.map(l => l.value),
      directory: __dirname + '/locales',
      queryParameter: 'lang',
      defaultLocale: global.CONFIG.SUPPORTED_LANGUAGE[0].value,
      autoReload: true
    }
  });

  server.route(routes);

  /* eslint-disable */
  const handler = function (req, h) {
    // return h.response('The page was not found').code(404);
    return h.redirect(DocsPage.PATH);
  };

  server.route({ method: '*', path: '/{p*}', handler });

  await server.start();

  /* eslint-disable */
  console.log(`Server running at: ${server.info.uri}`);

};

server.events.on('request', (req, event, tags) => {
  if (tags.error && event.error && event.error.isDeveloperError) {
    logger.error(event.error, 'DEVELOPER_ERROR');
  }
});

init();
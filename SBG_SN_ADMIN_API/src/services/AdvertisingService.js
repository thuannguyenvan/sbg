import mongoose from 'mongoose';
// import ApiError from '../utils/model/ApiError';
import { LanguageKeys } from '../data/constants';
import {
  ActionType,
  AdsFormat,
  AdsConfigUpdatingStatus,
} from '../data/enums';
import {
  AdsPartner,
  Advertising,
  AdsConfiguration,
  AdsConfigurationDetail,
} from '../data/models';
import ApiError from '../utils/model/ApiError';
import {
  translate,
  // validateTemplate
} from '../utils/translate';
import { GetRelativeSearchPattern } from '../utils/stringUtils';

const Messages = LanguageKeys.messages;
// const ValidateMessages = LanguageKeys.validate.messageTemplates;
// const ValidateLabels = LanguageKeys.validate.labels;

const getSearchInfo = async function() {

  // get Advertising formats
  const advertisingFormats = Object.keys(AdsFormat).map(s => {
    return {
      value: AdsFormat[s],
      title: translate(AdsFormat[s], 'enums')
    };
  });

  // Get Ads partners
  const adsPartners = await AdsPartner.find({ IsDeleted: false, IsActive: true });

  return {
    adsPartners: adsPartners.map(p => {
      return {
        id: p.id,
        name: p.Name,
        logo: p.Logo,
        website: p.Website,
        description: p.Description,
        isActive: p.IsActive
      };
    }),
    advertisingFormats: advertisingFormats
  };
};

const searchAdvertising = async function(model) {
  const filterAdvertising = { IsDeleted: false };

  let adsPartner = null;
  if (model.adsPartnerId) {
    adsPartner = await AdsPartner.findOne({ _id: model.adsPartnerId, IsDeleted: false, IsActive: true });
    if (!adsPartner) {
      throw ApiError.bussinessError(Messages.advertising.ADS_PARTNER_DOES_NOT_EXIST_OR_INACTIVE);
    }
    filterAdvertising.AdsPartnerId = model.adsPartnerId;
  }

  if (model.title) {
    const containKeywords = GetRelativeSearchPattern(model.title);
    filterAdvertising.Title = containKeywords;
  }

  if (model.isActive !== undefined && model.isActive !== null) {
    filterAdvertising.IsActive = model.isActive;
  }

  if (model.adsFormat) {
    filterAdvertising.AdsFormat = model.adsFormat;
  }

  const advertisings = await Advertising.pagination(filterAdvertising, model.page, model.size, model.orderBy, model.order);

  return advertisings.map(a => {
    const ads = a.toJsonObject();
    return {
      id: ads._id,
      adsPartnerId: ads.AdsPartnerId,
      adsPartnerName: ads.AdsPartnerName,
      adsPartnerWebsite: ads.AdsPartnerWebsite,
      title: ads.Title,
      adsFormat: ads.AdsFormat,
      adsFormatTitle: translate(ads.AdsFormat, 'enums'),
      clickThrough: ads.ClickThrough,
      clickTracking: ads.ClickTracking,
      content: ads.Content,
      description: ads.Description,
      isActive: ads.IsActive,
      total: a.total,
      actions: _getActions(model.userActions, ads)
    };
  });
};

const _getActions = function(got, item) {
  const actions = [];

  if (got.includes(ActionType.ADVERTISING.UPDATE)) {
    actions.push(ActionType.ADVERTISING.UPDATE);
  }

  if (got.includes(ActionType.ADVERTISING.ACTIVATE)) {
    if (!item.IsActive) {
      actions.push(ActionType.ADVERTISING.ACTIVATE);
    }
  }

  if (got.includes(ActionType.ADVERTISING.DEACTIVATE)) {
    if (item.IsActive) {
      actions.push(ActionType.ADVERTISING.DEACTIVATE);
    }
  }

  if (got.includes(ActionType.ADVERTISING.DELETE)) {
    actions.push(ActionType.ADVERTISING.DELETE);
  }

  return actions;
};

const getAddInfo = async function() {
  // Get Ads partners
  const adsPartners = await AdsPartner.find({ IsDeleted: false, IsActive: true });

  // get Advertising formats
  const advertisingFormats = Object.keys(AdsFormat).map(s => {
    return {
      value: AdsFormat[s],
      title: translate(AdsFormat[s], 'enums')
    };
  });

  return {
    adsPartners: adsPartners.map(p => {
      return {
        id: p.id,
        name: p.Name,
        logo: p.Logo,
        website: p.Website,
        description: p.Description,
        isActive: p.IsActive
      };
    }),
    advertisingFormats: advertisingFormats
  };
};

const addAdvertising = async function(model) {
  const adsPartnerId = model.adsPartnerId;

  // check adsPartner
  const adsPartner = await AdsPartner.findOne({ _id: adsPartnerId, IsDeleted: false, IsActive: true });
  if (!adsPartner) {
    throw ApiError.bussinessError(Messages.advertising.ADS_PARTNER_DOES_NOT_EXIST_OR_INACTIVE);
  }

  // check same title advertising in same ads partner
  const countAdvertisings = await Advertising.countDocuments({ Title: model.title, AdsPartnerId: adsPartnerId, IsDeleted: false });
  if (countAdvertisings > 0) {
    throw ApiError.bussinessError(Messages.advertising.ADVERTISING_WITH_SAME_TITLE_AND_PARTNER_ALREADY_EXIST);
  }

  const ads = new Advertising({
    AdsPartnerId: adsPartnerId,
    AdsPartnerName: adsPartner.Name,
    AdsPartnerLogo: adsPartner.Logo,
    AdsPartnerWebsite: adsPartner.Website,
    Title: model.title,
    ClickThrough: model.clickThrough,
    ClickTracking: model.clickTracking,
    Content: model.content,
    AdsFormat: model.adsFormat,
    Description: model.description
  });

  await ads.save();

  return ads.id;
};

const deleteAdvertising = async function(model) {
  const advertisingId = model.advertisingId;

  const advertising = await Advertising.findOne({ _id: advertisingId, IsDeleted: false });
  if (!advertising) {
    throw ApiError.bussinessError(Messages.advertising.ADVERTISING_DOES_NOT_EXIST);
  }

  // check have advertising configuration
  const countConfigDetails = await AdsConfigurationDetail.countDocuments({ AdvertisingId: advertisingId, IsDeleted: false });
  if (countConfigDetails > 0) {
    throw ApiError.bussinessError(Messages.advertising.CANNOT_DELETE_ADVERTISING_CAUSE_USED_IN_CONFIG);
  }

  advertising.IsDeleted = true;
  advertising.EditAt = Date.now();
  await advertising.save();
};

const getEditInfo = async function(model) {
  const advertisingId = model.advertisingId;

  // Get Ads partners
  const adsPartners = await AdsPartner.find({ IsDeleted: false, IsActive: true });

  // get Advertising formats
  const advertisingFormats = Object.keys(AdsFormat).map(s => {
    return {
      value: AdsFormat[s],
      title: translate(AdsFormat[s], 'enums')
    };
  });

  const advertising = await Advertising.findOne({ _id: advertisingId, IsDeleted: false });
  if (!advertising) {
    throw ApiError.bussinessError(Messages.advertising.ADVERTISING_DOES_NOT_EXIST);
  }

  const adsPartner = await AdsPartner.findOne({ _id: advertising.AdsPartnerId, IsDeleted: false, IsActive: true });
  if (!adsPartner) {
    throw ApiError.bussinessError(Messages.advertising.ADS_PARTNER_DOES_NOT_EXIST_OR_INACTIVE);
  }

  const ads = advertising.toJsonObject();

  return {
    advertising: {
      id: ads._id,
      adsPartnerId: ads.AdsPartnerId,
      title: ads.Title,
      adsFormat: ads.AdsFormat,
      adsFormatTitle: translate(ads.AdsFormat, 'enums'),
      clickThrough: ads.ClickThrough,
      clickTracking: ads.ClickTracking,
      content: ads.Content,
      description: ads.Description,
      isActive: ads.IsActive
    },
    adsPartners: adsPartners.map(p => {
      return {
        id: p.id,
        name: p.Name,
        logo: p.Logo,
        website: p.Website,
        description: p.Description,
        isActive: p.IsActive
      };
    }),
    advertisingFormats: advertisingFormats
  };
};

const updateAdvertising = async function(model) {
  const advertisingId = model.advertisingId;
  const session = await mongoose.startSession();
  session.startTransaction();

  try {
    const opts = { session };

    const advertising = await Advertising.findOne({ _id: advertisingId, IsDeleted: false }, {}, opts);
    if (!advertising) {
      throw ApiError.bussinessError(Messages.advertising.ADVERTISING_DOES_NOT_EXIST);
    }

    // check adsPartner
    const adsPartner = await AdsPartner.findOne({ _id: model.adsPartnerId, IsDeleted: false, IsActive: true }, {}, opts);
    if (!adsPartner) {
      throw ApiError.bussinessError(Messages.advertising.ADS_PARTNER_DOES_NOT_EXIST_OR_INACTIVE);
    }

    // check same title advertising in same ads partner
    const countAdvertisings = await Advertising.countDocuments({ _id: { $ne: advertisingId }, Title: model.title, AdsPartnerId: model.adsPartnerId, IsDeleted: false });
    if (countAdvertisings > 0) {
      throw ApiError.bussinessError(Messages.advertising.ADVERTISING_WITH_SAME_TITLE_AND_PARTNER_ALREADY_EXIST);
    }

    advertising.AdsPartnerId = model.adsPartnerId;
    advertising.Title = model.title;
    advertising.ClickThrough = model.clickThrough;
    advertising.ClickTracking = model.clickTracking;
    advertising.Content = model.content;
    advertising.AdsFormat = model.adsFormat;
    advertising.Description = model.description;
    advertising.EditAt = Date.now();

    await advertising.save();

    // Apdate Ads configuration details
    const adsConfigDetails = await AdsConfigurationDetail.find({ AdvertisingId: advertisingId, IsDeleted: false }, {}, opts);

    if (adsConfigDetails && adsConfigDetails.length > 0) {
      await AdsConfigurationDetail.updateMany(
        { AdvertisingId: advertisingId, IsDeleted: false },
        { $set: {
          AdsPartnerId: advertising.AdsPartnerId,
          ClickThrough: model.clickThrough,
          ClickTracking: model.clickTracking,
          EditAt: Date.now()
        } },
        opts
      );
    }

    await session.commitTransaction();
    session.endSession();
    return true;
  } catch (err) {
    await session.abortTransaction();
    session.endSession();
    if (!err.errorType) {
      throw ApiError.error(Messages.advertising.UPDATE_ADVERTISING_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const activateDeactivateAdvertising = async function(model) {
  const advertisingId = model.advertisingId;

  const session = await mongoose.startSession();
  session.startTransaction();

  try {
    const opts = { session };

    const advertising = await Advertising.findOne({ _id: advertisingId, IsDeleted: false }, {}, opts);
    if (!advertising) {
      throw ApiError.bussinessError(Messages.advertising.ADVERTISING_DOES_NOT_EXIST);
    }

    // check activate or deactivate
    if (advertising.IsActive && model.isActivate) {
      throw ApiError.bussinessError(Messages.advertising.ADVERTISING_ALREADY_ACTIVE);
    }
    if (!advertising.IsActive && !model.isActivate) {
      throw ApiError.bussinessError(Messages.advertising.ADVERTISING_ALREADY_INACTIVE);
    }

    // Activate
    if (model.isActivate) {
      advertising.IsActive = true;
      advertising.EditAt = Date.now();

      await advertising.save();
    } else { // Deactivate
      advertising.IsActive = false;
      advertising.EditAt = Date.now();
      await advertising.save();

      // Update Ads configuration details
      const adsConfigDetails = await AdsConfigurationDetail.find({ AdvertisingId: advertisingId, IsDeleted: false }, {}, opts);

      if (adsConfigDetails && adsConfigDetails.length > 0) {
        await AdsConfigurationDetail.updateMany(
          { AdvertisingId: advertisingId, IsDeleted: false },
          { $set: { IsDeleted: true, EditAt: Date.now() } },
          opts
        );

        const adsConfigIds = adsConfigDetails.map(acd => {
          const detail = acd.toJsonObject();
          return detail.AdsConfigurationId;
        });

        // Update UPDATING_STATUS of AdsConfigurations to NEED_UPDATE
        await AdsConfiguration.updateMany(
          { _id: { $in: adsConfigIds }, IsDeleted: false },
          { $set: { UpdatingStatus: AdsConfigUpdatingStatus.NEED_UPDATE, EditAt: Date.now() } },
          opts
        );
      }
    }

    await session.commitTransaction();
    session.endSession();
    return true;
  } catch (err) {
    await session.abortTransaction();
    session.endSession();
    if (!err.errorType) {
      const message = model.isActivate
                      ? Messages.advertising.ACTIVATE_ADVERTISING_FAIL
                      : Messages.advertising.DEACTIVATE_ADVERTISING_FAIL;
      throw ApiError.error(message, null, err);
    } else {
      throw err;
    }
  }
};

const getSearchActiveAdvertisingInfo = async function() {
  // Get Ads partners
  const adsPartners = await AdsPartner.find({ IsDeleted: false, IsActive: true });

  return {
    adsPartners: adsPartners.map(p => {
      return {
        id: p.id,
        name: p.Name,
        logo: p.Logo,
        website: p.Website,
        description: p.Description,
        isActive: p.IsActive
      };
    })
  };
};

const searchActiveAdvertising = async function(model) {
  const filterAdvertising = { IsDeleted: false, IsActive: true };

  let adsPartner = null;
  if (model.adsPartnerId) {
    adsPartner = await AdsPartner.findOne({ _id: model.adsPartnerId, IsDeleted: false, IsActive: true });
    if (!adsPartner) {
      throw ApiError.bussinessError(Messages.advertising.ADS_PARTNER_DOES_NOT_EXIST_OR_INACTIVE);
    }
    filterAdvertising.AdsPartnerId = model.adsPartnerId;
  }

  if (model.title) {
    const containKeywords = GetRelativeSearchPattern(model.title);
    filterAdvertising.Title = containKeywords;
  }

  const advertisings = await Advertising.pagination(filterAdvertising, model.page, model.size, model.orderBy, model.order);

  return advertisings.map(a => {
    const ads = a.toJsonObject();
    return {
      id: ads._id,
      adsPartnerId: ads.AdsPartnerId,
      adsPartnerName: ads.AdsPartnerName,
      adsPartnerWebsite: ads.AdsPartnerWebsite,
      title: ads.Title,
      adsFormat: ads.AdsFormat,
      adsFormatTitle: translate(ads.AdsFormat, 'enums'),
      clickThrough: ads.ClickThrough,
      clickTracking: ads.ClickTracking,
      content: ads.Content,
      description: ads.Description,
      isActive: ads.IsActive,
      total: a.total
    };
  });
};

export {
  getSearchInfo,
  searchAdvertising,
  getAddInfo,
  addAdvertising,
  deleteAdvertising,
  getEditInfo,
  updateAdvertising,
  activateDeactivateAdvertising,
  getSearchActiveAdvertisingInfo,
  searchActiveAdvertising,
};

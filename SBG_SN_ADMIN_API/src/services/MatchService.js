import moment from 'moment';
import ApiError from '../utils/model/ApiError';
import { LanguageKeys } from '../data/constants';
import {
  ActionType,
  SoccerMatchStatus,
  WinnerTeamType
} from '../data/enums';
import {
  SoccerCompetition,
  SoccerMatch,
  SoccerCompetitionStageTeam,
  SoccerRound,
  SoccerMatchStream,
  SoccerCompetitionStage
} from '../data/models';
import {
  translate,
  validateTemplate
} from '../utils/translate';
import { GetRelativeSearchPattern } from '../utils/stringUtils';

const Messages = LanguageKeys.messages;
const ValidateMessages = LanguageKeys.validate.messageTemplates;
const ValidateLabels = LanguageKeys.validate.labels;

const getSearchInfo = async function() {
  // Get competitions
  const competitions = await SoccerCompetition.find({ IsDeleted: false }).sort('-CreateAt');

  // Get match statuses
  const matchStatuses = Object.keys(SoccerMatchStatus).map(s => {
    return {
      value: SoccerMatchStatus[s],
      title: translate(SoccerMatchStatus[s], 'enums')
    };
  });

  return {
    soccerCompetitions: competitions.map(c => {
      return {
        id: c.id,
        title: c.Title,
        season: c.Season,
        logo: c.Logo,
        startAt: c.StartAt,
        endAt: c.EndAt,
        place: c.Place
      };
    }),
    soccerMatchStatuses: matchStatuses
  };
};

const searchMatch = async function(model) {
  const filterCompetition = { IsDeleted: false };
  const filterRound = { IsDeleted: false };
  const filterMatch = { IsDeleted: false };

  if (model.soccerCompetitionId) {
    filterCompetition._id = model.soccerCompetitionId;
    filterRound.SoccerCompetitionId = model.soccerCompetitionId;
    filterMatch.SoccerCompetitionId = model.soccerCompetitionId;
  }

  if (model.soccerRoundId) {
    filterRound._id = model.soccerRoundId;
    filterMatch.SoccerRoundId = model.soccerRoundId;
  }

  if (model.soccerMatchStatus) {
    filterMatch.Status = model.soccerMatchStatus;
  }

  if (model.title) {
    const containKeywords = GetRelativeSearchPattern(model.title);
    filterMatch.Title = containKeywords;
  }

  if (model.dateStartAt) {
    const startOfDay = moment(model.dateStartAt).startOf('day').toDate();
    const endOfDay = moment(model.dateStartAt).endOf('day').toDate();
    filterMatch.StartAt = {
      $gte: startOfDay, $lte: endOfDay
    };
  }

  const matchs = await SoccerMatch.pagination(filterMatch, model.page, model.size, model.orderBy, model.order);

  const competitions = await SoccerCompetition.find(filterCompetition);

  const rounds = await SoccerRound.find(filterRound);

  const teamIds = matchs.map(m => m.toJsonObject().TeamA).concat(matchs.map(m => m.toJsonObject().TeamB));
  const matchIds = matchs.map(m => m.id);

  const livestreams = await SoccerMatchStream.find({
    SoccerMatchId: { $in: matchIds },
    IsDeleted: false
  });

  const livestreamObjs = livestreams.map(l => l.toJsonObject());

  const matchHasStreamIds = livestreams.map(l => l.toJsonObject().SoccerMatchId);

  const teams = await SoccerCompetitionStageTeam.find({
    _id: {
      $in: teamIds
    },
    IsDeleted: false
  });

  return matchs.map(m => {
    const match = m.toJsonObject();

    const competition = competitions.find(function(c) {
      return c.id == match.SoccerCompetitionId;
    });
    const round = rounds.find(function(r) {
      return r.id == match.SoccerRoundId;
    });
    const teamA = teams.find(function(t) {
      return t.id == match.TeamA;
    });
    const teamB = teams.find(function(t) {
      return t.id == match.TeamB;
    });

    const hasStream = matchHasStreamIds.includes(match._id);

    let livestream = undefined;
    if (hasStream) {
      livestream = livestreamObjs.find(function(l) {
        return l.SoccerMatchId == match._id;
      });
    }

    return {
      soccerCompetitionId: match.SoccerCompetitionId,
      soccerCompetitionTitle: competition.Title,
      soccerRoundId: match.SoccerRoundId,
      soccerRoundTitle: round.Title,
      id: m.id,
      title: m.Title,
      startAt: m.StartAt,
      status: m.Status,
      statusTitle: translate(m.Status, 'enums'),
      teamA: {
        soccerCompetitionStageTeamId: match.TeamA,
        soccerCompetitionTeamId: teamA.toJsonObject().SoccerCompetitionTeamId,
        soccerTeamId: teamA.toJsonObject().SoccerTeamId,
        name: teamA.Name,
        teamCode: teamA.TeamCode,
        logo: teamA.Logo
      },
      teamB: {
        soccerCompetitionStageTeamId: match.TeamB,
        soccerCompetitionTeamId: teamB.toJsonObject().SoccerCompetitionTeamId,
        soccerTeamId: teamB.toJsonObject().SoccerTeamId,
        name: teamB.Name,
        teamCode: teamB.TeamCode,
        logo: teamB.Logo
      },
      winner: match.Winner,
      score: {
        fullTime: _buildScore(match.ScoreA, match.ScoreB),
        halfTime: _buildScore(match.ScoreAI, match.ScoreBI),
        extraTime: _buildScore(match.ScoreAET, match.ScoreBET),
        penalty: _buildScore(match.ScoreAP, match.ScoreBP),
        aggregate: _buildScore(match.ScoreAAGG, match.ScoreBAGG)
      },
      soccerMatchStreamId: livestream ? livestream._id : undefined,
      total: m.total,
      actions: _getMatchActions(model.userActions, hasStream)
    };
  });
};

const _getMatchActions = function(got, hasStream) {
  const actions = [];

  if (got.includes(ActionType.MATCH.UPDATE)) {
    actions.push(ActionType.MATCH.UPDATE);
  }

  if (got.includes(ActionType.MATCH.DELETE)) {
    actions.push(ActionType.MATCH.DELETE);
  }

  if (got.includes(ActionType.MATCH_STREAM.ADD)) {
    if (!hasStream) {
      actions.push(ActionType.MATCH_STREAM.ADD);
    }
  }

  if (got.includes(ActionType.MATCH_STREAM.UPDATE)) {
    if (hasStream) {
      actions.push(ActionType.MATCH_STREAM.UPDATE);
    }
  }

  return actions;
};

function _buildScore(teamA, teamB) {
  if (teamA !== undefined && teamA !== null
      && teamB !== undefined && teamB !== null) {
    return {
      teamA: teamA,
      teamB: teamB
    };
  } else {
    return undefined;
  }
}

const getAddInfo = async function() {
  // Get competitions
  const competitions = await SoccerCompetition.find({ IsDeleted: false }).sort('-CreateAt');

  // Get match statuses
  const matchStatuses = Object.keys(SoccerMatchStatus).map(s => {
    return {
      value: SoccerMatchStatus[s],
      title: translate(SoccerMatchStatus[s], 'enums')
    };
  });

  // Get winner team type
  const winnerTeamTypes = Object.keys(WinnerTeamType).map(s => {
    return {
      value: WinnerTeamType[s],
      title: translate(WinnerTeamType[s], 'enums')
    };
  });

  return {
    soccerCompetitions: competitions.map(c => {
      return {
        id: c.id,
        title: c.Title,
        season: c.Season,
        logo: c.Logo,
        startAt: c.StartAt,
        endAt: c.EndAt,
        place: c.Place
      };
    }),
    soccerMatchStatuses: matchStatuses,
    winnerTeamTypes: winnerTeamTypes
  };
};

const addMatch = async function(model) {
  // const soccerCompetitionId = model.soccerCompetitionId;
  const soccerRoundId = model.soccerRoundId;

  const round = await SoccerRound.findOne({ _id: soccerRoundId, IsDeleted: false });
  if (!round) {
    throw ApiError.bussinessError(Messages.round.ROUND_DOES_NOT_EXIST);
  }

  if (model.teamA
      && model.teamB
      && model.teamA.soccerCompetitionStageTeamId == model.teamB.soccerCompetitionStageTeamId) {
        throw ApiError.bussinessError(validateTemplate(ValidateMessages.SOCCER_MATCH_TEAM_B_MUST_NOT_SAME_TEAM_A,
                                                            ValidateLabels.SOCCER_MATCH_TEAM_B,
                                                            ValidateLabels.SOCCER_MATCH_TEAM_A));
      }

  let teamA;
  let teamB;

  if (model.teamA && model.teamA.soccerCompetitionStageTeamId) {
    teamA = await SoccerCompetitionStageTeam.findOne({ _id: model.teamA.soccerCompetitionStageTeamId, IsDeleted: false });
    if (!teamA) {
      throw ApiError.bussinessError(validateTemplate(ValidateMessages.SOCCER_MATCH_TEAM_A_B_DOES_NOT_EXIST,
                                                            ValidateLabels.SOCCER_MATCH_TEAM_A));
    }

    if (teamA.toJsonObject().SoccerCompetitionStageId != round.toJsonObject().SoccerCompetitionStageId) {
      throw ApiError.bussinessError(validateTemplate(ValidateMessages.SOCCER_MATCH_TEAM_DOES_NOT_EXIST_IN_STAGE_OF_ROUND,
                                    ValidateLabels.SOCCER_MATCH_TEAM_B));
    }
  }

  if (model.teamB && model.teamB.soccerCompetitionStageTeamId) {
    teamB = await SoccerCompetitionStageTeam.findOne({ _id: model.teamB.soccerCompetitionStageTeamId, IsDeleted: false });
    if (!teamB) {
      throw ApiError.bussinessError(validateTemplate(ValidateMessages.SOCCER_MATCH_TEAM_A_B_DOES_NOT_EXIST,
                                                            ValidateLabels.SOCCER_MATCH_TEAM_B));
    }

    if (teamB.toJsonObject().SoccerCompetitionStageId != round.toJsonObject().SoccerCompetitionStageId) {
      throw ApiError.bussinessError(validateTemplate(ValidateMessages.SOCCER_MATCH_TEAM_DOES_NOT_EXIST_IN_STAGE_OF_ROUND,
                                                            ValidateLabels.SOCCER_MATCH_TEAM_B));
    }
  }

  const sm = new SoccerMatch({
    SoccerCompetitionId: round.SoccerCompetitionId,
    SoccerCompetitionStageId: round.SoccerCompetitionStageId,
    SoccerRoundId: round._id,
    Title: model.title,
    Place: model.place,
    StartAt: model.startAt,
    FinishedAt: model.finishedAt,
    TeamA: model.teamA ? model.teamA.soccerCompetitionStageTeamId : undefined,
    TeamB: model.teamB ? model.teamB.soccerCompetitionStageTeamId : undefined,
    Status: model.status,
    Winner: model.winner,
    ScoreA: model.score && model.score.fullTime ? model.score.fullTime.teamA : undefined,
    ScoreB: model.score && model.score.fullTime ? model.score.fullTime.teamB : undefined,
    ScoreAI: model.score && model.score.halfTime ? model.score.halfTime.teamA : undefined,
    ScoreBI: model.score && model.score.halfTime ? model.score.halfTime.teamB : undefined,
    ScoreAET: model.score && model.score.extraTime ? model.score.extraTime.teamA : undefined,
    ScoreBET: model.score && model.score.extraTime ? model.score.extraTime.teamB : undefined,
    ScoreAP: model.score && model.score.penalty ? model.score.penalty.teamA : undefined,
    ScoreBP: model.score && model.score.penalty ? model.score.penalty.teamB : undefined,
    ScoreAAGG: model.score && model.score.aggregate ? model.score.aggregate.teamA : undefined,
    ScoreBAGG: model.score && model.score.aggregate ? model.score.aggregate.teamB : undefined
  });

  const soccerMatch = await sm.save();

  return soccerMatch.id;
};

const deleteMatch = async function(model) {
  const soccerMatchId = model.soccerMatchId;

  const match = await SoccerMatch.findOne({ _id: soccerMatchId, IsDeleted: false });
  if (!match) {
    throw ApiError.bussinessError(Messages.match.MATCH_DOES_NOT_EXIST);
  }

  // checking livestream of match
  const streamCount = await SoccerMatchStream.countDocuments({ SoccerMatchId: soccerMatchId, IsDeleted: false });

  if (streamCount > 0) {
    throw ApiError.bussinessError(Messages.match.CANNOT_DELETE_MATCH_CAUSE_HAVE_STREAM);
  }

  // return await match.remove();
  match.IsDeleted = true;
  match.EditAt = Date.now();
  return await match.save();
};

const getEditInfo = async function(model) {
  const soccerMatchId = model.soccerMatchId;

  const matchModel = await SoccerMatch.findOne({ _id: soccerMatchId, IsDeleted: false });
  if (!matchModel) {
    throw ApiError.bussinessError(Messages.match.MATCH_DOES_NOT_EXIST);
  }

  const match = matchModel.toJsonObject();

  const competition = await SoccerCompetition.findOne({ _id: match.SoccerCompetitionId, IsDeleted: false });

  const stage = await SoccerCompetitionStage.findOne({ _id: match.SoccerCompetitionStageId, IsDeleted: false });

  const teamModels = await SoccerCompetitionStageTeam.find({ SoccerCompetitionStageId: match.SoccerCompetitionStageId, IsDeleted: false });

  const round = await SoccerRound.findOne({ _id: match.SoccerRoundId, IsDeleted: false });

  const teamA = teamModels.find(function(t) {
    return t.id == match.TeamA;
  });
  const teamB = teamModels.find(function(t) {
    return t.id == match.TeamB;
  });

  const teams = teamModels.map(t => {
    const team = t.toJsonObject();
    return {
      soccerCompetitionStageTeamId: team._id,
      soccerCompetitionTeamId: team.SoccerCompetitionTeamId,
      soccerTeamId: team.SoccerTeamId,
      soccerCompetitionId: team.SoccerCompetitionId,
      soccerCompetitionStageId: team.SoccerCompetitionStageId,
      group: team.Group,
      name: team.Name,
      teamCode: team.TeamCode,
      logo: team.Logo
    };
  });

  // Get match statuses
  const matchStatuses = Object.keys(SoccerMatchStatus).map(s => {
    return {
      value: SoccerMatchStatus[s],
      title: translate(SoccerMatchStatus[s], 'enums')
    };
  });

  // Get winner team type
  const winnerTeamTypes = Object.keys(WinnerTeamType).map(s => {
    return {
      value: WinnerTeamType[s],
      title: translate(WinnerTeamType[s], 'enums')
    };
  });

  return {
    soccerMatch: {
      soccerCompetitionId: match.SoccerCompetitionId,
      soccerCompetitionTitle: competition.Title,
      soccerCompetitionStageId: match.SoccerCompetitionStageId,
      soccerCompetitionStageTitle: stage.Title,
      soccerRoundId: match.SoccerRoundId,
      soccerRoundTitle: round.Title,
      title: match.Title,
      place: match.Place,
      startAt: match.StartAt,
      finishedAt: match.FinishedAt,
      teamA: {
        soccerCompetitionStageTeamId: teamA.toJsonObject()._id,
        soccerCompetitionTeamId: teamA.toJsonObject().SoccerCompetitionTeamId,
        soccerTeamId: teamA.toJsonObject().SoccerTeamId,
        name: teamA.toJsonObject().Name,
        teamCode: teamA.toJsonObject().TeamCode,
        logo: teamA.toJsonObject().Logo
      },
      teamB: {
        soccerCompetitionStageTeamId: teamB.toJsonObject()._id,
        soccerCompetitionTeamId: teamB.toJsonObject().SoccerCompetitionTeamId,
        soccerTeamId: teamB.toJsonObject().SoccerTeamId,
        name: teamB.toJsonObject().Name,
        teamCode: teamB.toJsonObject().TeamCode,
        logo: teamB.toJsonObject().Logo
      },
      status: match.Status,
      winner: match.Winner,
      score: {
        fullTime: _buildScore(match.ScoreA, match.ScoreB),
        halfTime: _buildScore(match.ScoreAI, match.ScoreBI),
        extraTime: _buildScore(match.ScoreAET, match.ScoreBET),
        penalty: _buildScore(match.ScoreAP, match.ScoreBP),
        aggregate: _buildScore(match.ScoreAAGG, match.ScoreBAGG)
      },
    },
    soccerCompetitionStageTeams: teams,
    soccerMatchStatuses: matchStatuses,
    winnerTeamTypes: winnerTeamTypes
  };
};

const updateMatch = async function(model) {
  const soccerMatchId = model.soccerMatchId;

  const matchModel = await SoccerMatch.findOne({ _id: soccerMatchId, IsDeleted: false });
  if (!matchModel) {
    throw ApiError.bussinessError(Messages.match.MATCH_DOES_NOT_EXIST);
  }

  const round = await SoccerRound.findOne({ _id: matchModel.SoccerRoundId, IsDeleted: false });
  if (!round) {
    throw ApiError.bussinessError(Messages.round.ROUND_DOES_NOT_EXIST);
  }

  if (model.teamA
      && model.teamB
      && model.teamA.soccerCompetitionStageTeamId == model.teamB.soccerCompetitionStageTeamId) {
        throw ApiError.bussinessError(validateTemplate(ValidateMessages.SOCCER_MATCH_TEAM_B_MUST_NOT_SAME_TEAM_A,
                                                            ValidateLabels.SOCCER_MATCH_TEAM_B,
                                                            ValidateLabels.SOCCER_MATCH_TEAM_A));
      }

  let teamA;
  let teamB;

  if (model.teamA && model.teamA.soccerCompetitionStageTeamId) {
    teamA = await SoccerCompetitionStageTeam.findOne({ _id: model.teamA.soccerCompetitionStageTeamId, IsDeleted: false });
    if (!teamA) {
      throw ApiError.bussinessError(validateTemplate(ValidateMessages.SOCCER_MATCH_TEAM_A_B_DOES_NOT_EXIST,
                                                            ValidateLabels.SOCCER_MATCH_TEAM_A));
    }

    if (teamA.toJsonObject().SoccerCompetitionStageId != round.toJsonObject().SoccerCompetitionStageId) {
      throw ApiError.bussinessError(validateTemplate(ValidateMessages.SOCCER_MATCH_TEAM_DOES_NOT_EXIST_IN_STAGE_OF_ROUND,
                                    ValidateLabels.SOCCER_MATCH_TEAM_B));
    }
  }

  if (model.teamB && model.teamB.soccerCompetitionStageTeamId) {
    teamB = await SoccerCompetitionStageTeam.findOne({ _id: model.teamB.soccerCompetitionStageTeamId, IsDeleted: false });
    if (!teamB) {
      throw ApiError.bussinessError(validateTemplate(ValidateMessages.SOCCER_MATCH_TEAM_A_B_DOES_NOT_EXIST,
                                                            ValidateLabels.SOCCER_MATCH_TEAM_B));
    }

    if (teamB.toJsonObject().SoccerCompetitionStageId != round.toJsonObject().SoccerCompetitionStageId) {
      throw ApiError.bussinessError(validateTemplate(ValidateMessages.SOCCER_MATCH_TEAM_DOES_NOT_EXIST_IN_STAGE_OF_ROUND,
                                                            ValidateLabels.SOCCER_MATCH_TEAM_B));
    }
  }

  matchModel.Title = model.title;
  matchModel.Place = model.place;
  matchModel.StartAt = model.startAt;
  matchModel.FinishedAt = model.finishedAt;
  matchModel.TeamA = model.teamA ? model.teamA.soccerCompetitionStageTeamId : undefined;
  matchModel.TeamB = model.teamB ? model.teamB.soccerCompetitionStageTeamId : undefined;
  matchModel.Status = model.status;
  matchModel.Winner = model.winner;
  matchModel.ScoreA = model.score && model.score.fullTime ? model.score.fullTime.teamA : undefined;
  matchModel.ScoreB = model.score && model.score.fullTime ? model.score.fullTime.teamB : undefined;
  matchModel.ScoreAI = model.score && model.score.halfTime ? model.score.halfTime.teamA : undefined;
  matchModel.ScoreBI = model.score && model.score.halfTime ? model.score.halfTime.teamB : undefined;
  matchModel.ScoreAET = model.score && model.score.extraTime ? model.score.extraTime.teamA : undefined;
  matchModel.ScoreBET = model.score && model.score.extraTime ? model.score.extraTime.teamB : undefined;
  matchModel.ScoreAP = model.score && model.score.penalty ? model.score.penalty.teamA : undefined;
  matchModel.ScoreBP = model.score && model.score.penalty ? model.score.penalty.teamB : undefined;
  matchModel.ScoreAAGG = model.score && model.score.aggregate ? model.score.aggregate.teamA : undefined;
  matchModel.ScoreBAGG = model.score && model.score.aggregate ? model.score.aggregate.teamB : undefined;

  return matchModel.save();
};

export {
  getSearchInfo,
  searchMatch,
  addMatch,
  getAddInfo,
  deleteMatch,
  getEditInfo,
  updateMatch
};

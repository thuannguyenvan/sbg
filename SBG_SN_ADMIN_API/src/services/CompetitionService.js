import _ from 'lodash';
import mongoose from 'mongoose';
import ApiError from '../utils/model/ApiError';
import { LanguageKeys } from '../data/constants';
import { ActionType } from '../data/enums';
import {
  SoccerCompetitionBase,
  SoccerCompetition,
  SoccerRound,
  SoccerCompetitionStage,
  SoccerStage,
  SoccerCompetitionStageTeam,
  SoccerCompetitionTeam,
  SoccerCompetitionStageStandingTable,
  SoccerCompetitionStageStanding,
  SoccerTeam
} from '../data/models';
import * as competitionBaseService from './CompetitionBaseService';
import * as stageService from './StageService';
import { GetRelativeSearchPattern } from '../utils/stringUtils';
import { validateTemplate } from '../utils/translate';

const ValidateMessages = LanguageKeys.validate.messageTemplates;
const Messages = LanguageKeys.messages;

const searchCompetition = async function(model) {
  let filter = { IsDeleted: false };
  if (model.title) {
    // const containKeywords = new RegExp(model.title, 'i');
    const containKeywords = GetRelativeSearchPattern(model.title);
    filter.Title = containKeywords;
  }

  const comps = await SoccerCompetition.pagination(filter, model.page, model.size, model.orderBy, model.order);
  const compObjs = comps.map(c => {
    return {
      id: c.id,
      title: c.Title,
      startAt: c.StartAt,
      endAt: c.EndAt,
      logo: c.Logo,
      season: c.Season,
      place: c.Place,
      total: c.total,
      actions: _getSearchCompetitionActions(model.userActions)
    };
  });
  return compObjs;
};

const _getSearchCompetitionActions = function(got) {
  const actions = [];

  if (got.includes(ActionType.COMPETITION.CONFIG_TEAM)) {
    actions.push(ActionType.COMPETITION.CONFIG_TEAM);
  }

  if (got.includes(ActionType.COMPETITION.UPDATE)) {
    actions.push(ActionType.COMPETITION.UPDATE);
  }

  if (got.includes(ActionType.COMPETITION.DELETE)) {
    actions.push(ActionType.COMPETITION.DELETE);
  }

  if (got.includes(ActionType.COMPETITION.GET_STAGE)) {
    actions.push(ActionType.COMPETITION.GET_STAGE);
  }

  return actions;
};

const addCompetition = async function(model) {
  const stages = await SoccerStage.find({ _id: { $in: model.stages }, IsDeleted: false });

  // if (!stages || stages.length === 0) {
  //   throw ApiError.bussinessError(Messages.competition.HAS_NO_ONE_VALID_STAGE);
  // }

  if (stages && stages.length > 0) {
    const standaloneStages = stages.filter(s => s.Standalone);
    if (standaloneStages.length > 0 &&
        stages.length > 1) {
          if (standaloneStages.length == 1) {
            throw ApiError.bussinessError(validateTemplate(
              ValidateMessages.SOCCER_COMPETITION_HAS_STANDALONE_WITH_OTHERS,
              standaloneStages[0].Title));
          } else {
            const stageTiles = standaloneStages.map(st => st.Title).join(', ');
            throw ApiError.bussinessError(validateTemplate(
              ValidateMessages.SOCCER_COMPETITION_HAVE_STANDALONES_WITH_OTHERS,
              stageTiles));
          }
      }
  }

  const compeBase = await SoccerCompetitionBase.find({ _id: model.soccerCompetitionBaseId, IsDeleted: false });

  if (!compeBase) {
    throw ApiError.bussinessError(Messages.competition.COMPETITION_TYPE_DOES_NOT_EXIST);
  }

  const competitionCount = await SoccerCompetition.countDocuments({
    SoccerCompetitionBaseId: model.soccerCompetitionBaseId,
    Season: model.season,
    IsDeleted: false
  });

  if (competitionCount > 0) {
    throw ApiError.bussinessError(Messages.competition.HAS_COMPETITION_SAME_SEASON_AND_COMPETITION_BASE);
  }

  const session = await mongoose.startSession();
  session.startTransaction();
  try {
    const opts = { session };
    const comp = new SoccerCompetition({
      SoccerCompetitionBaseId: model.soccerCompetitionBaseId,
      Title: model.title,
      Season: model.season,
      Place: model.place,
      StartAt: model.startAt,
      EndAt: model.endAt,
      Description: model.description,
      Logo: compeBase.Logo
    });
    const competition = await comp.save();

    const compStages = [];
    for (let stage of stages) {
      const compStage = new SoccerCompetitionStage({
        SoccerCompetitionId: competition.id,
        SoccerStageId: stage.id,
        Title: stage.Title,
        Key: stage.Key,
        Standalone: stage.Standalone
      });
      compStages.push(compStage);
    }

    // insert competition stages
    await SoccerCompetitionStage.insertMany(compStages, opts);

    await session.commitTransaction();
    session.endSession();

    return competition.id;
  } catch (err) {
    await session.abortTransaction();
    session.endSession();
    if (!err.errorType) {
      throw ApiError.error(Messages.competition.ADD_COMPETITION_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const getAddCompetitionInfo = async function() {
  const competitionBases = await competitionBaseService.getCompetitionBase();
  const stages = await stageService.getStage();
  return {
    competitionBases: competitionBases.map(c => {
      return {
        id: c._id,
        title: c.Title,
        competitionCode: c.CompetitionCode
      };
    }),
    stages: stages.map(s => {
      return {
        id: s._id,
        title: s.Title,
        key: s.Key,
        order: s.Order,
        standalone: s.Standalone ? true : false
      };
    })
  };
};

const getEditCompetitionInfo = async function(model) {
  // check competition
  const competition = await SoccerCompetition.findOne({ _id: model.soccerCompetitionId, IsDeleted: false });

  if (!competition) {
    throw ApiError.bussinessError(Messages.competition.COMPETITION_DOES_NOT_EXIST);
  }

  // get competition stage
  const competitionStages = await SoccerCompetitionStage.find({ SoccerCompetitionId: model.soccerCompetitionId, IsDeleted: false });

  const competitionBases = await competitionBaseService.getCompetitionBase();
  const stages = await stageService.getStage();

  const comp = competition.toJsonObject();

  return {
    competition: {
      id: comp._id,
      soccerCompetitionBaseId: comp.SoccerCompetitionBaseId,
      title: comp.Title,
      season: comp.Season,
      place: comp.Place,
      startAt: comp.StartAt,
      endAt: comp.EndAt,
      description: comp.Description,
      logo: comp.Logo,
      stages: competitionStages.map(cs => {
        const stage = cs.toJsonObject();
        return {
          id: stage._id,
          soccerStageId: stage.SoccerStageId,
          title: stage.Title,
          key: stage.Key,
          standalone: stage.Standalone ? true : false
        };
      })
    },
    competitionBases: competitionBases.map(c => {
      return {
        id: c._id,
        title: c.Title,
        competitionCode: c.CompetitionCode
      };
    }),
    stages: stages.map(s => {
      return {
        id: s._id,
        title: s.Title,
        key: s.Key,
        order: s.Order,
        standalone: s.Standalone ? true : false
      };
    })
  };
};

const updateCompetition = async function(model) {
  const session = await mongoose.startSession();
  session.startTransaction();
  try {
    const opts = { session };

    // check competition
    const competition = await SoccerCompetition.findOne({ _id: model.soccerCompetitionId, IsDeleted: false }, {}, opts);

    if (!competition) {
      throw ApiError.bussinessError(Messages.competition.COMPETITION_DOES_NOT_EXIST);
    }

    const stages = await SoccerStage.find({ _id: { $in: model.stages }, IsDeleted: false }, {}, opts);

    // if (!stages || stages.length === 0) {
    //   throw ApiError.bussinessError(Messages.competition.HAS_NO_ONE_VALID_STAGE);
    // }

    if (stages && stages.length > 0) {
      const standaloneStages = stages.filter(s => s.Standalone);
      if (standaloneStages.length > 0 &&
          stages.length > 1) {
            if (standaloneStages.length == 1) {
              throw ApiError.bussinessError(validateTemplate(
                ValidateMessages.SOCCER_COMPETITION_HAS_STANDALONE_WITH_OTHERS,
                standaloneStages[0].Title));
            } else {
              const stageTiles = standaloneStages.map(st => st.Title).join(', ');
              throw ApiError.bussinessError(validateTemplate(
                ValidateMessages.SOCCER_COMPETITION_HAVE_STANDALONES_WITH_OTHERS,
                stageTiles));
            }
        }
    }

    // get current stage
    const currentStages = await SoccerCompetitionStage.find({ SoccerCompetitionId: model.soccerCompetitionId, IsDeleted: false }, {}, opts);

    // delete stages
    const deleteStages = currentStages.filter(function(cs) {
      return !stages.map(s => s.id).includes(cs.toJsonObject().SoccerStageId);
    });

    // insert stage
    const insertStages = stages.filter(function(s) {
      return !currentStages.map(cs => cs.toJsonObject().SoccerStageId).includes(s.id);
    });

    // update info
    // check season
    const checkCompetitions = await SoccerCompetition.find({
      _id: { $ne: model.soccerCompetitionId },
      Season: model.season,
      SoccerCompetitionBaseId: competition.SoccerCompetitionBaseId,
      IsDeleted: false
    }, {}, opts);

    if (checkCompetitions && checkCompetitions.length > 0) {
      throw ApiError.bussinessError(Messages.competition.HAS_COMPETITION_SAME_SEASON_AND_COMPETITION_BASE);
    }

    competition.Season = model.season;
    competition.Title =  model.title;
    competition.Place = model.place;
    competition.StartAt = model.startAt;
    competition.EndAt = model.endAt;
    competition.Description = model.description;
    competition.EditAt = Date.now();
    await competition.save();
    // opts.new = true;
    // await SoccerCompetition.updateOne(
    //   { _id: model.soccerCompetitionId, IsDeleted: false },
    //   { $set: {
    //     Season: model.season,
    //     Title:  model.title,
    //     Place: model.place,
    //     StartAt: model.startAt,
    //     EndAt: model.endAt,
    //     Description: model.description,
    //     EditAt: Date.now()
    //   }},
    //   opts
    // );

    if (deleteStages && deleteStages.length > 0) {
      await _checkDeleteStages(deleteStages, opts);
      // delete standing table of stages, stages
      // await SoccerCompetitionStageStandingTable.deleteMany({ SoccerCompetitionStageId: { $in: deleteStages.map(s => s._id) }});
      await SoccerCompetitionStageStandingTable.updateMany(
        { SoccerCompetitionStageId: { $in: deleteStages.map(s => s._id) } },
        { $set: { IsDeleted: true, EditAt: Date.now() } },
        opts
      );

      // await SoccerCompetitionStageStanding.deleteMany({ SoccerCompetitionStageId: { $in: deleteStages.map(s => s._id) }});
      await SoccerCompetitionStageStanding.updateMany(
        { SoccerCompetitionStageId: { $in: deleteStages.map(s => s._id) } },
        { $set: { IsDeleted: true, EditAt: Date.now() } },
        opts
      );

      // await SoccerCompetitionStage.deleteMany({ _id: { $in: deleteStages.map(s => s._id) }});
      await SoccerCompetitionStage.updateMany(
        { _id: { $in: deleteStages.map(s => s._id) } },
        { $set: { IsDeleted: true, EditAt: Date.now() } },
        opts
      );
    }

    if (insertStages && insertStages.length > 0) {
      // insert new
      const compStages = [];
      for (let stage of insertStages) {
        const compStage = new SoccerCompetitionStage({
          SoccerCompetitionId: competition.id,
          SoccerStageId: stage.id,
          Title: stage.Title,
          Key: stage.Key,
          Standalone: stage.Standalone
        });
        compStages.push(compStage);
      }

      // insert competition stages
      await SoccerCompetitionStage.insertMany(compStages, opts);
    }

    await session.commitTransaction();
    session.endSession();

    return true;
  } catch (err) {
    await session.abortTransaction();
    session.endSession();
    if (!err.errorType) {
      throw ApiError.error(Messages.competition.UPDATE_COMPETITION_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

async function _checkDeleteStages(stages, opts) {
  const stageIds = stages.map(s => s._id);
  const stageTeams = await SoccerCompetitionStageTeam.find({ SoccerCompetitionStageId: { $in: stageIds }, IsDeleted: false }, {}, opts);

  if (stageTeams && stageTeams.length > 0) {
    const stageHavingTeams = stages.filter(function(stage) {
      return stageTeams.map(st => st.toJsonObject().SoccerCompetitionStageId).includes(stage.toJsonObject()._id);
    });
    if (stageHavingTeams.length == 1) {
      throw ApiError.bussinessError(validateTemplate(
        ValidateMessages.SOCCER_COMPETITION_CANNOT_REMOVE_STAGE_HAVING_TEAMS,
        stageHavingTeams[0].Title));
    } else {
      const stageTiles = stageHavingTeams.map(st => st.Title).join(', ');
      throw ApiError.bussinessError(validateTemplate(
        ValidateMessages.SOCCER_COMPETITION_CANNOT_REMOVE_STAGES_HAVING_TEAMS,
        stageTiles));
    }
  }

  const rounds = await SoccerRound.find({ SoccerCompetitionStageId: { $in: stageIds }, IsDeleted: false }, {}, opts);

  if (rounds && rounds.length > 0) {
    const stageHavingRounds = stages.filter(function(stage) {
      return rounds.map(r => r.toJsonObject().SoccerCompetitionStageId).includes(stage.toJsonObject()._id);
    });
    if (stageHavingRounds.length == 1) {
      throw ApiError.bussinessError(validateTemplate(
        ValidateMessages.SOCCER_COMPETITION_CANNOT_REMOVE_STAGE_HAVING_ROUNDS,
        stageHavingRounds[0].Title));
    } else {
      const stageTiles = stageHavingRounds.map(st => st.Title).join(', ');
      throw ApiError.bussinessError(validateTemplate(
        ValidateMessages.SOCCER_COMPETITION_CANNOT_REMOVE_STAGE_HAVING_ROUNDS,
        stageTiles));
    }
  }
}

const getCompetitionStages = async function(model) {
  // check competition
  const competition = await SoccerCompetition.findOne({ _id: model.soccerCompetitionId, IsDeleted: false });

  if (!competition) {
    throw ApiError.bussinessError(Messages.competition.COMPETITION_DOES_NOT_EXIST);
  }

  const competitionStages = await SoccerCompetitionStage.find({ SoccerCompetitionId: model.soccerCompetitionId, IsDeleted: false });

  // Lấy thông tin đội bóng trong stages
  // const grs = await SoccerCompetitionStageTeam
  //             .aggregate([
  //               {
  //                 $match: { SoccerCompetitionId: model.soccerCompetitionId }
  //               },
  //               {
  //                 $group: {
  //                   _id : {
  //                     Group: '$Group'
  //                   }
  //                 }
  //               }
  //             ]);
  const teams = await SoccerCompetitionStageTeam.find({ SoccerCompetitionId: model.soccerCompetitionId, IsDeleted: false });

  const stageTeams = teams.map(t => t.toJsonObject());

  const groups = _.groupBy(stageTeams, 'SoccerCompetitionStageId');

  const standings = await SoccerCompetitionStageStanding.find({
    SoccerCompetitionStageId: { $in: competitionStages.map(cs => cs._id) },
    IsDeleted: false
  });
  const stageHasStandingIds = standings.map(s => s.toJsonObject().SoccerCompetitionStageId);

  return competitionStages.map(cs => {
    const c = cs.toJsonObject();
    const hasStanding = stageHasStandingIds.includes(c._id);
    return {
      id: c._id,
      soccerStageId: c.SoccerStageId,
      soccerCompetitionId: c.SoccerCompetitionId,
      title: c.Title,
      standalone: c.Standalone,
      groupInfos: _groupTeams(groups[c._id]),
      actions: _getCompetitionStageActions(model.userActions, hasStanding)
    };
  });
};

const _getCompetitionStageActions = function(got, hasStanding) {
  const actions = [];

  if (got.includes(ActionType.COMPETITION_STAGE.CONFIG_TEAM)) {
    actions.push(ActionType.COMPETITION_STAGE.CONFIG_TEAM);
  }

  if (got.includes(ActionType.COMPETITION_STAGE.CREATE_STANDING)) {
    if (!hasStanding) {
      actions.push(ActionType.COMPETITION_STAGE.CREATE_STANDING);
    }
  }

  if (got.includes(ActionType.COMPETITION_STAGE.UPDATE_STANDING)) {
    if (hasStanding) {
      actions.push(ActionType.COMPETITION_STAGE.UPDATE_STANDING);
    }
  }

  if (got.includes(ActionType.COMPETITION_STAGE.DELETE_STANDING)) {
    if (hasStanding) {
      actions.push(ActionType.COMPETITION_STAGE.DELETE_STANDING);
    }
  }

  return actions;
};

const getCompetitionTeams = async function(model) {
  let filter = {
    SoccerCompetitionId: model.soccerCompetitionId,
    IsDeleted: false
  };
  if (model.name) {
    // const containKeywords = new RegExp(model.name, 'i');
    const containKeywords = GetRelativeSearchPattern(model.name);
    filter = {
      Name: containKeywords,
      SoccerCompetitionId: model.soccerCompetitionId,
      IsDeleted: false
    };
  }
  const competitionTeams = await SoccerCompetitionTeam.find(filter);
  return competitionTeams.map(ct => {
    const c = ct.toJsonObject();
    return {
      id: c._id,
      name: c.Name,
      soccerCompetitionId: c.SoccerCompetitionId,
      soccerTeamId: c.SoccerTeamId,
      teamCode: c.TeamCode,
      logo: c.Logo
    };
  });
};

const deleteCompetition = async function(model) {
  const competition = await SoccerCompetition.findOne({ _id: model.soccerCompetitionId, IsDeleted: false });
  if (!competition) {
    throw ApiError.bussinessError(Messages.competition.COMPETITION_DOES_NOT_EXIST);
  }

  // Check stage in competition
  const stages = await SoccerCompetitionStage.find({ SoccerCompetitionId: model.soccerCompetitionId, IsDeleted: false });

  if (stages && stages.length > 0) {
    throw ApiError.bussinessError(Messages.competition.CANNOT_REMOVE_COMPETITION_HAVING_STAGE);
  }

  // await competition.remove();
  competition.IsDeleted = true;
  competition.EditAt = Date.now();
  await competition.save();

  return true;
};

const getCompetitions = async function(model) {
  let filter = { IsDeleted: false };
  if (model.title) {
    // const containKeywords = new RegExp(model.title, 'i');
    const containKeywords = GetRelativeSearchPattern(model.title);
    filter.Title = containKeywords;
  }

  const comps = await SoccerCompetition.pagination(filter, model.page, model.size, 'CreateAt', 'DESC');
  const compObjs = comps.map(c => {
    return {
      id: c.id,
      title: c.Title,
      startAt: c.StartAt,
      endAt: c.EndAt,
      logo: c.Logo,
      season: c.Season,
      place: c.Place
    };
  });
  return compObjs;
};

const getConfigTeamInfo = async function(model) {
  const soccerCompetitionId = model.soccerCompetitionId;

  const competition = await SoccerCompetition.findOne({ _id: soccerCompetitionId, IsDeleted: false });
  if (!competition) {
    throw ApiError.bussinessError(Messages.competition.COMPETITION_DOES_NOT_EXIST);
  }

  const filter = {
    SoccerCompetitionId: soccerCompetitionId,
    IsDeleted: false
  };

  const sct = await SoccerCompetitionTeam.find(filter).sort('-CreateAt _Filter.Name Name');

  return {
    soccerCompetitionId: soccerCompetitionId,
    soccerCompetitionTitle: competition.Title,
    soccerCompetitionTeams: sct.map(t => {
      const team = t.toJsonObject();
      return {
        id: team._id,
        soccerCompetitionId: team.SoccerCompetitionId,
        soccerTeamId: team.SoccerTeamId,
        name: team.Name,
        teamCode: team.TeamCode,
        logo: team.Logo,
        teamInfo: team.TeamInfo
      };
    })
  };
};

const configTeams = async function(model) {
  const session = await mongoose.startSession();
  session.startTransaction();
  try {
    const opts = { session };

    const soccerCompetitionId = model.soccerCompetitionId;

    const competition = await SoccerCompetition.findOne({ _id: soccerCompetitionId, IsDeleted: false }, {}, opts);
    if (!competition) {
      throw ApiError.bussinessError(Messages.competition.COMPETITION_DOES_NOT_EXIST);
    }

    // valid teams
    const soccerTeams =
        await SoccerTeam.find({ _id: { $in: model.soccerCompetitionTeams.map(t => t.soccerTeamId) }, IsDeleted: false }, {}, opts);

    const currentCompetitionTeams =
        await SoccerCompetitionTeam.find({ SoccerCompetitionId: soccerCompetitionId, IsDeleted: false }, {}, opts);

    // new teams
    const newTeams = soccerTeams.filter(function(st) {
      return !currentCompetitionTeams.map(cct => cct.toJsonObject().SoccerTeamId).includes(st.toJsonObject()._id);
    });

    // get delete teams
    const deleteTeams = currentCompetitionTeams.filter(function(cct) {
      return !soccerTeams.map(st => st.toJsonObject()._id).includes(cct.toJsonObject().SoccerTeamId);
    });

    await _checkDeleteTeams(deleteTeams, opts);

    // delete
    // await SoccerCompetitionTeam.deleteMany({ _id: { $in: deleteTeams.map(t => t._id) } });
    await SoccerCompetitionTeam.updateMany(
      { _id: { $in: deleteTeams.map(t => t._id) } },
      { $set: { IsDeleted: true, EditAt: Date.now() } },
      opts
    );

    // insert new
    const insertModels = newTeams.map(t => {
      const soccerCompetitionTeam = new SoccerCompetitionTeam({
        SoccerCompetitionId: soccerCompetitionId,
        SoccerTeamId: t._id,
        Name: t.Name,
        TeamCode: t.TeamCode,
        Logo: t.Logo,
        TeamInfo: t.TeamInfo
      });
      return soccerCompetitionTeam;
    });

    await SoccerCompetitionTeam.insertMany(insertModels, opts);

    await session.commitTransaction();
    session.endSession();

    return true;
  } catch (err) {
    await session.abortTransaction();
    session.endSession();
    if (!err.errorType) {
      throw ApiError.error(Messages.competition.CONFIG_TEAM_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

async function _checkDeleteTeams(deleteTeams, opts) {
  const stageTeamModels = await SoccerCompetitionStageTeam.find({
    SoccerCompetitionTeamId: { $in: deleteTeams.map(t => t._id) },
    IsDeleted: false
  }, {}, opts);

  const stageTeams = _.uniqBy(stageTeamModels.map(st => st.toJsonObject()), 'SoccerTeamId');

  if (stageTeams && stageTeams.length > 0) {
    if (stageTeams.length == 1) {
      throw ApiError.bussinessError(validateTemplate(
        ValidateMessages.SOCCER_COMPETITION_CANNOT_REMOVE_TEAM_ALREADY_IN_STAGE,
        stageTeams[0].Name));
    } else {
      const teamNames = stageTeams.map(st => st.Name).join(', ');
      throw ApiError.bussinessError(validateTemplate(
        ValidateMessages.SOCCER_COMPETITION_CANNOT_REMOVE_TEAMS_ALREADY_IN_STAGE,
        teamNames));
    }
  }
}

function _groupTeams(teams) {
  const group = _.groupBy(teams, 'Group');
  const groups = [];
  const keys = Object.keys(group);
  for (const g of keys) {
    if (g != 'undefined') {
      groups.push(g);
    }
  }
  return groups;
}

export {
  searchCompetition,
  addCompetition,
  updateCompetition,
  getAddCompetitionInfo,
  getCompetitionStages,
  getCompetitionTeams,
  deleteCompetition,
  getEditCompetitionInfo,
  getCompetitions,
  getConfigTeamInfo,
  configTeams
};

import { Role } from '../data/models';

const addRole = function (model) {
  const role = new Role(model);
  return role.save();
};

export { addRole as AddRole };

import axios from 'axios';
import ApiError from '../utils/model/ApiError';
import { LanguageKeys } from '../data/constants';
import { TokenType } from '../data/enums';
import { Token, User } from '../data/models';
import * as userService from './UserService';
import mongoose from 'mongoose';

const Messages = LanguageKeys.messages;

const login = async function(model) {
  const session = await mongoose.startSession();
  session.startTransaction();
  try {
    const opts = { session };
    const tokenSso = model.ssoToken;
    let userSso;
    await axios.get(`${global.CONFIG.SSO_URL.GET_USER_INFO}?access_token=${tokenSso}`)
                .then(res => userSso = res.data)
                .catch(err => { throw ApiError.bussinessError(Messages.auth.INVALID_TOKEN, null, err); });

    if (userSso == null) {
      throw ApiError.bussinessError(Messages.auth.INVALID_TOKEN);
    }

    // get user from database
    const user = await User.findOne({ UserName: userSso.userName, IsDeleted: false }, {}, opts);

    if (!user) {
      throw ApiError.bussinessError(Messages.auth.USER_DOES_NOT_EXISTS);
    }

    if (!user.IsActive) {
      throw ApiError.bussinessError(Messages.auth.USER_WAS_INACTIVE);
    }

    // update userinfo as sso server
    user.Email = userSso.email;
    user.FullName = userSso.fullName;
    user.Avatar = userSso.avatar;
    user.EditAt = Date.now();

    await user.save();

    // create token and return userinfo to user
    let token = new Token({
      Token: tokenSso,
      ProviderToken: tokenSso,
      UserId: user._id,
      TokenType: TokenType.AUTHEN
    });
    token = await token.save();

    const userInfo = await userService.GetUserInfo({ UserId: token.UserId }, opts);
    userInfo.token = token.Token;

    await session.commitTransaction();
    session.endSession();

    return userInfo;
  } catch (err) {
    await session.abortTransaction();
    session.endSession();
    if (!err.errorType) {
      throw ApiError.error(Messages.auth.LOGIN_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const logout = async function(model) {
  const tokenstring = model.tokenstring;
  const token = await Token.findOne({ Token: tokenstring, Expired: false, IsDeleted: false });

  if (!token) {
    throw ApiError.bussinessError(Messages.auth.INVALID_TOKEN);
  }

  // Update token to expired
  token.Expired = true;
  token.EditAt = Date.now();
  await token.save();

  return token.ProviderToken;
};

const getUserInfo = async function(model) {
  const tokenString = model.tokenString;
  const token = await Token.findOne({ Token: tokenString, IsDeleted: false });
  const userinfos = await userService.GetUserInfo({ UserId: token.UserId });
  return userinfos;
};

export {
  login,
  logout,
  getUserInfo
};

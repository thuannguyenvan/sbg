import _ from 'lodash';
import mongoose from 'mongoose';
import ApiError from '../utils/model/ApiError';
import { LanguageKeys } from '../data/constants';
import {
  SoccerCompetitionTeam,
  SoccerCompetitionStage,
  SoccerCompetitionStageTeam,
  SoccerCompetitionStageStanding,
  SoccerCompetitionStageStandingTable,
  SoccerMatch
} from '../data/models';
import * as competitionService from './CompetitionService';
import { GetRelativeSearchPattern } from '../utils/stringUtils';
import { validateTemplate } from '../utils/translate';

const ValidateMessages = LanguageKeys.validate.messageTemplates;
const Messages = LanguageKeys.messages;

const createStanding = async function(model) {
  const session = await mongoose.startSession();
  session.startTransaction();
  try {
    const opts = { session };

    const soccerCompetitionStageId = model.soccerCompetitionStageId;
    const competitionStage = await SoccerCompetitionStage.findOne({ _id: soccerCompetitionStageId, IsDeleted: false }, {}, opts);

    if (!competitionStage) {
      throw ApiError.bussinessError(Messages.competitionStage.STAGE_DOES_NOT_EXIST);
    }

    let stageStanding =
        await SoccerCompetitionStageStanding.find({ SoccerCompetitionStageId: soccerCompetitionStageId, IsDeleted: false }, {}, opts);

    if (stageStanding && stageStanding.length > 0) {
      throw ApiError.bussinessError(Messages.competitionStage.STAGE_ALREADY_HAS_STANDING);
    }

    stageStanding = new SoccerCompetitionStageStanding({
      SoccerCompetitionId: competitionStage.SoccerCompetitionId,
      SoccerCompetitionStageId: competitionStage._id
    });

    stageStanding = await stageStanding.save();

    // insert standing table
    const stageTeams =
        await SoccerCompetitionStageTeam.find({ SoccerCompetitionStageId: soccerCompetitionStageId, IsDeleted: false }, {}, opts);

    const stageStandingTables = [];

    for (const st of stageTeams) {
      const stageTable = new SoccerCompetitionStageStandingTable({
        SoccerCompetitionId: stageStanding.SoccerCompetitionId,
        SoccerCompetitionStageId: soccerCompetitionStageId,
        SoccerCompetitionStageStandingId: stageStanding._id,
        SoccerCompetitionStageTeamId: st._id,
        SoccerCompetitionTeamId: st.SoccerCompetitionTeamId,
        SoccerTeamId: st.SoccerTeamId,
        Group: st.Group,
        Name: st.Name,
        Logo: st.Logo
      });
      stageStandingTables.push(stageTable);
    }

    await SoccerCompetitionStageStandingTable.insertMany(stageStandingTables, opts);

    await session.commitTransaction();
    session.endSession();

    return stageStanding.toJsonObject()._id;
  } catch (err) {
    await session.abortTransaction();
    session.endSession();
    if (!err.errorType) {
      throw ApiError.error(Messages.competitionStage.CREATE_STANDING_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const deleteStanding = async function(model) {
  const session = await mongoose.startSession();
  session.startTransaction();
  try {
    const opts = { session };

    const soccerCompetitionStageId = model.soccerCompetitionStageId;
    const competitionStage = await SoccerCompetitionStage.findOne({ _id: soccerCompetitionStageId, IsDeleted: false }, {}, opts);

    if (!competitionStage) {
      throw ApiError.bussinessError(Messages.competitionStage.STAGE_DOES_NOT_EXIST);
    }

    let stageStanding =
        await SoccerCompetitionStageStanding.find({ SoccerCompetitionStageId: soccerCompetitionStageId, IsDeleted: false }, {}, opts);

    if (!stageStanding || stageStanding.length <= 0) {
      throw ApiError.bussinessError(Messages.competitionStage.STAGE_HAS_NO_STANDING);
    }

    // await SoccerCompetitionStageStandingTable.deleteMany({ SoccerCompetitionStageId: soccerCompetitionStageId });
    await SoccerCompetitionStageStandingTable.updateMany(
      { SoccerCompetitionStageId: soccerCompetitionStageId },
      { $set: { IsDeleted: true, EditAt: Date.now() } },
      opts
    );

    // const standingDeleted = await SoccerCompetitionStageStanding.deleteMany({ SoccerCompetitionStageId: soccerCompetitionStageId });
    await SoccerCompetitionStageStanding.updateMany(
      { SoccerCompetitionStageId: soccerCompetitionStageId },
      { $set: { IsDeleted: true, EditAt: Date.now() } },
      opts
    );

    await session.commitTransaction();
    session.endSession();

    return true;
  } catch (err) {
    await session.abortTransaction();
    session.endSession();
    if (!err.errorType) {
      throw ApiError.error(Messages.competitionStage.DELETE_STANDING_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const getConfigTeamInfo = async function(model) {
  const soccerCompetitionStageId = model.soccerCompetitionStageId;
  const competitionStage = await SoccerCompetitionStage.findOne({ _id: soccerCompetitionStageId, IsDeleted: false });

  if (!competitionStage) {
    throw ApiError.bussinessError(Messages.competitionStage.STAGE_DOES_NOT_EXIST);
  }

  const competitionTeams = await competitionService.getCompetitionTeams({ soccerCompetitionId: competitionStage.SoccerCompetitionId });

  const competitionStageTeams = await SoccerCompetitionStageTeam.find({ SoccerCompetitionStageId: competitionStage._id, IsDeleted: false });

  return {
    competitionTeams: competitionTeams,
    competitionStageTeams: competitionStageTeams.map(cst => {
      const c = cst.toJsonObject();
      return {
        id: c._id,
        name: c.Name,
        soccerCompetitionStageId: c.SoccerCompetitionStageId,
        soccerCompetitionId: c.SoccerCompetitionId,
        soccerCompetitionTeamId: c.SoccerCompetitionTeamId,
        soccerTeamId: c.SoccerTeamId,
        teamCode: c.TeamCode,
        logo: c.Logo,
        group: c.Group ? c.Group : null
      };
    })
  };
};

const _checkGroup = function(stageTeams) {
  const undefinedGroups = stageTeams.filter(st =>
                            st.group == undefined || st.group == null);

  if (undefinedGroups.length > 0 && stageTeams.length > undefinedGroups.length) {
    throw ApiError.bussinessError(Messages.competitionStage.ALL_TEAM_MUST_IN_GROUP_OR_ALL_MUST_NOT_IN);
  }
};

const _checkDeteleTeam = async function(competitionStageId, stageTeams, stage, opts) {
  if (!stage) {
    stage = await SoccerCompetitionStage.findOne({ _id: competitionStageId, IsDeleted: false }, {}, opts);

    if (!stage) {
      throw ApiError.bussinessError(Messages.competitionStage.STAGE_DOES_NOT_EXIST);
    }
  }

  // Get stage team ids
  const stageTeamIds = stageTeams.map(s => s.id);

  // Get match with team in team ids
  const matchs = await SoccerMatch.find({
    $and: [
      { SoccerCompetitionStageId: competitionStageId, IsDeleted: false },
      { $or: [
        { TeamA: { $in: stageTeamIds } },
        { TeamB: { $in: stageTeamIds } }
      ] }
    ]
  }, {}, opts);

  if (matchs && matchs.length > 0) {
    const stageTeamIdInMatchs = matchs.map(m => m.toJsonObject().TeamA).concat(matchs.map(m => m.toJsonObject().TeamB));
    const teamInMatchModels = stageTeams.filter(function(stageTeam) {
      return stageTeamIdInMatchs.includes(stageTeam.id);
    });

    const teamInMatchs = _.uniqBy(teamInMatchModels.map(tim => tim.toJsonObject()), 'SoccerTeamId');

    if (teamInMatchs.length == 1) {
      throw ApiError.bussinessError(validateTemplate(
        ValidateMessages.SOCCER_COMPETITION_STAGE_CANNOT_REMOVE_TEAM_IN_MATCH,
        teamInMatchs[0].Name));
    } else {
      const teamNames = teamInMatchs.map(st => st.Name).join(', ');
      throw ApiError.bussinessError(validateTemplate(
        ValidateMessages.SOCCER_COMPETITION_STAGE_CANNOT_REMOVE_TEAMS_IN_MATCH,
        teamNames));
    }
  }
};

const configTeam = async function(model) {
  _checkGroup(model.soccerCompetitionStageTeams);

  const session = await mongoose.startSession();
  session.startTransaction();
  try {
    const opts = { session };

    const soccerCompetitionStageId = model.soccerCompetitionStageId;
    const competitionStage = await SoccerCompetitionStage.findOne({ _id: soccerCompetitionStageId, IsDeleted: false }, {}, opts);

    if (!competitionStage) {
      throw ApiError.bussinessError(Messages.competitionStage.STAGE_DOES_NOT_EXIST);
    }

    // get current team in stage
    let currentStageTeams =
        await SoccerCompetitionStageTeam.find({ SoccerCompetitionStageId: soccerCompetitionStageId, IsDeleted: false }, {}, opts);

    // get team in competition
    const competitionTeams =
        await SoccerCompetitionTeam.find({ SoccerCompetitionId: competitionStage.SoccerCompetitionId, IsDeleted: false }, {}, opts);
    const comTeamIds = competitionTeams.map(ct => ct.id);

    // get valid competitionStageTeams
    const validComStageTeams = model.soccerCompetitionStageTeams.filter(function(t) {
      return comTeamIds.includes(t.soccerCompetitionTeamId);
    });

    // get updateTeams
    const updateTeams = validComStageTeams.filter(function(t) {
      return currentStageTeams
              .map(ct => ct.toJsonObject().SoccerCompetitionTeamId)
              .includes(t.soccerCompetitionTeamId);
    });

    // get deleteTeams
    const deleteTeams = []; // stageTeams
    for (const cst of currentStageTeams) {
      const sctId = cst.toJsonObject().SoccerCompetitionTeamId;
      if (!validComStageTeams
            .map(t => t.soccerCompetitionTeamId)
            .includes(sctId)) {
              deleteTeams.push(cst);
            }
    }

    await _checkDeteleTeam(soccerCompetitionStageId, deleteTeams, competitionStage, opts);

    // get insertTeams
    const insertTeams = []; // từ model
    for (const st of validComStageTeams) {
      if (!currentStageTeams
            .map(cst => cst.toJsonObject().SoccerCompetitionTeamId)
            .includes(st.soccerCompetitionTeamId)) {
              insertTeams.push(st);
            }
    }

    const stageStanding =
        await SoccerCompetitionStageStanding.findOne({ SoccerCompetitionStageId: soccerCompetitionStageId, IsDeleted: false }, {}, opts);

    // update group in stage teams and standing tables
    for (const ut of updateTeams) {

      const setGroup = ut.group
          ? { $set: { Group: ut.group, EditAt: Date.now() } }
          : { $unset: { Group: 1 }, $set: { EditAt: Date.now() } };

      await SoccerCompetitionStageTeam.updateMany(
        {
          SoccerCompetitionTeamId: ut.soccerCompetitionTeamId,
          SoccerCompetitionStageId: soccerCompetitionStageId
        },
        setGroup,
        opts
      );

      await SoccerCompetitionStageStandingTable.updateMany(
        {
          SoccerCompetitionTeamId: ut.soccerCompetitionTeamId,
          SoccerCompetitionStageId: soccerCompetitionStageId
        },
        setGroup,
        opts
      );
    }

    // Delete
    for (const ut of deleteTeams) {
      // await SoccerCompetitionStageTeam.deleteMany(
      //   {
      //     SoccerCompetitionTeamId: ut.SoccerCompetitionTeamId,
      //     SoccerCompetitionStageId: soccerCompetitionStageId
      //   }
      // );
      await SoccerCompetitionStageTeam.updateMany(
        {
          SoccerCompetitionTeamId: ut.SoccerCompetitionTeamId,
          SoccerCompetitionStageId: soccerCompetitionStageId
        },
        { $set: { IsDeleted: true, EditAt: Date.now() } },
        opts
      );

      if (stageStanding) {
        // await SoccerCompetitionStageStandingTable.deleteMany(
        //   {
        //     SoccerCompetitionStageStandingId: stageStanding.id,
        //     SoccerCompetitionTeamId: ut.SoccerCompetitionTeamId,
        //     SoccerCompetitionStageId: soccerCompetitionStageId
        //   }
        // );
        await SoccerCompetitionStageStandingTable.updateMany(
          {
            SoccerCompetitionStageStandingId: stageStanding.id,
            SoccerCompetitionTeamId: ut.SoccerCompetitionTeamId,
            SoccerCompetitionStageId: soccerCompetitionStageId
          },
          { $set: { IsDeleted: true, EditAt: Date.now() } },
          opts
        );
      }
    }

    // Insert
    for (const t of insertTeams) {
      const competitionTeam = await SoccerCompetitionTeam.findOne({ _id: t.soccerCompetitionTeamId, IsDeleted: false }, {}, opts);
      let stageTeam = new SoccerCompetitionStageTeam({
        SoccerCompetitionId: competitionTeam.SoccerCompetitionId,
        SoccerCompetitionStageId: soccerCompetitionStageId,
        SoccerCompetitionTeamId: t.soccerCompetitionTeamId,
        SoccerTeamId: competitionTeam.SoccerTeamId,
        Group: t.group,
        Name: competitionTeam.Name,
        Logo: competitionTeam.Logo,
        TeamCode: competitionTeam.TeamCode,
        TeamInfo: competitionTeam.TeamInfo
      });
      stageTeam = await stageTeam.save();
      if (stageStanding) {
        let standingTable = new SoccerCompetitionStageStandingTable({
          SoccerCompetitionStageId: soccerCompetitionStageId,
          SoccerCompetitionStageStandingId: stageStanding._id,
          SoccerCompetitionStageTeamId: stageTeam._id,
          SoccerCompetitionTeamId: stageTeam.SoccerCompetitionTeamId,
          SoccerTeamId: stageTeam.SoccerTeamId,
          Group: t.group,
          Name: stageTeam.Name,
          Logo: stageTeam.Logo,
          TeamCode: stageTeam.TeamCode
        });
        standingTable = await standingTable.save();
      }
    }

    await session.commitTransaction();
    session.endSession();

    return true;
  } catch (err) {
    await session.abortTransaction();
    session.endSession();
    if (!err.errorType) {
      throw ApiError.error(Messages.competitionStage.CONFIG_TEAM_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const getStandingTables = async function(model) {
  const soccerCompetitionStageId = model.soccerCompetitionStageId;
  // Get standing
  const standing = await SoccerCompetitionStageStanding.findOne({ SoccerCompetitionStageId: soccerCompetitionStageId, IsDeleted: false });
  if (!standing) {
    throw ApiError.bussinessError(Messages.competitionStage.STAGE_HAS_NO_STANDING);
  }
  const standingTables = await SoccerCompetitionStageStandingTable.find({ SoccerCompetitionStageStandingId: standing._id, IsDeleted: false });

  const standingObj = standing.toJsonObject();

  return {
    id: standingObj._id,
    soccerCompetitionId: standingObj.SoccerCompetitionId,
    soccerCompetitionStageId: standingObj.SoccerCompetitionStageId,
    isShowInHomepage: standingObj.IsShowInHomepage,
    tables: standingTables.map(t => {
      const standingTable = t.toJsonObject();
      return {
        id: standingTable._id,
        group: standingTable.Group,
        name: standingTable.Name,
        logo: standingTable.Logo,
        teamCode: standingTable.TeamCode,
        position: standingTable.Position,
        played: standingTable.Played,
        won: standingTable.Won,
        lost: standingTable.Lost,
        drawn: standingTable.Drawn,
        goalsFor: standingTable.GoalsFor,
        goalsAgainst: standingTable.GoalsAgainst,
        goalDifference: standingTable.GoalDifference,
        points: standingTable.Points
      };
    })
  };
};

const updateStanding = async function(model) {
  const soccerCompetitionStageId = model.soccerCompetitionStageId;

  const session = await mongoose.startSession();
  session.startTransaction();
  try {
    const opts = { session };

    // Get standing
    const standing =
      await SoccerCompetitionStageStanding.findOne({ SoccerCompetitionStageId: soccerCompetitionStageId, IsDeleted: false }, {}, opts);
    if (!standing) {
      throw ApiError.bussinessError(Messages.competitionStage.STAGE_HAS_NO_STANDING);
    }

    const standingTables =
        await SoccerCompetitionStageStandingTable.find({ SoccerCompetitionStageStandingId: standing._id, IsDeleted: false }, {}, opts);

    const validTables = model.tables.filter(function(t) {
      return standingTables.map(st => st.id).includes(t.id);
    });

    for (const table of validTables) {
      await SoccerCompetitionStageStandingTable.updateOne(
        { _id: table.id },
        { $set: {
          Position: table.position,
          Played: table.played,
          Won: table.won,
          Lost: table.lost,
          Drawn: table.drawn,
          GoalsFor: table.goalsFor,
          GoalsAgainst: table.goalsAgainst,
          GoalDifference: table.goalDifference,
          Points: table.points,
          EditAt: Date.now()
        } },
        opts
      );
    }

    // update show in homepage
    standing.IsShowInHomepage = model.isShowInHomepage ? model.isShowInHomepage : false;
    standing.EditAt = Date.now();
    await standing.save();

    // await SoccerCompetitionStageStanding.updateOne(
    //   { SoccerCompetitionStageId: soccerCompetitionStageId, IsDeleted: false },
    //   { $set: {
    //     IsShowInHomepage: model.isShowInHomepage ? model.isShowInHomepage : false,
    //     EditAt: Date.now()
    //   }},
    //   opts
    // );

    await session.commitTransaction();
    session.endSession();

    return true;
  } catch (err) {
    await session.abortTransaction();
    session.endSession();
    if (!err.errorType) {
      throw ApiError.error(Messages.competitionStage.UPDATE_STANDING_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const getByCompetition = async function(model) {
  if (!model) model = {};
  let filter = { IsDeleted: false };
  if (model.soccerCompetitionId) {
    filter.SoccerCompetitionId = model.soccerCompetitionId;
  }
  if (model.title) {
    // const containKeywords = new RegExp(model.title, 'i');
    const containKeywords = GetRelativeSearchPattern(model.title);
    filter.Title = containKeywords;
  }

  const stages = await SoccerCompetitionStage.find(filter);

  return stages.map(cs => {
    const c = cs.toJsonObject();
    return {
      id: c._id,
      soccerStageId: c.SoccerStageId,
      soccerCompetitionId: c.SoccerCompetitionId,
      title: c.Title,
      standalone: c.Standalone
    };
  });
};

export {
  createStanding,
  deleteStanding,
  getConfigTeamInfo,
  configTeam,
  getStandingTables,
  updateStanding,
  getByCompetition
};

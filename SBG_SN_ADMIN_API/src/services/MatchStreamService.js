import moment from 'moment';
import ApiError from '../utils/model/ApiError';
import { LanguageKeys } from '../data/constants';
import {
  ActionType,
  LivestreamStatus,
  LivestreamVideoUnit
} from '../data/enums';
import * as roundService from './RoundService';
import {
  SoccerCompetition,
  SoccerMatch,
  SoccerRound,
  SoccerMatchStream,
  AdsConfiguration,
} from '../data/models';
import { translate } from '../utils/translate';
import { GetRelativeSearchPattern } from '../utils/stringUtils';

const Messages = LanguageKeys.messages;

const getSearchInfo = async function() {
  // Get competitions
  const competitions = await SoccerCompetition.find({ IsDeleted: false }).sort('-CreateAt');

  // Get livestream statuses
  const livestreamStatuses = Object.keys(LivestreamStatus).map(s => {
    return {
      value: LivestreamStatus[s],
      title: translate(LivestreamStatus[s], 'enums')
    };
  });

  return {
    soccerCompetitions: competitions.map(c => {
      return {
        id: c.id,
        title: c.Title,
        season: c.Season,
        logo: c.Logo,
        startAt: c.StartAt,
        endAt: c.EndAt,
        place: c.Place
      };
    }),
    livestreamStatuses: livestreamStatuses
  };
};

const searchMatchStream = async function(model) {
  const filterCompetition = { IsDeleted: false };
  const filterRound = { IsDeleted: false };
  const filterMatch = { IsDeleted: false };
  const filterMatchStream = { IsDeleted: false };

  if (model.soccerCompetitionId) {
    filterCompetition._id = model.soccerCompetitionId;
    filterRound.SoccerCompetitionId = model.soccerCompetitionId;
    filterMatch.SoccerCompetitionId = model.soccerCompetitionId;
    filterMatchStream.SoccerCompetitionId = model.soccerCompetitionId;
  }

  if (model.soccerRoundId) {
    filterRound._id = model.soccerRoundId;
    filterMatch.SoccerRoundId = model.soccerRoundId;
    filterMatchStream.SoccerRoundId = model.soccerRoundId;
  }

  if (model.soccerMatchId) {
    filterMatch._id = model.soccerMatchId;
    filterMatchStream.SoccerMatchId = model.soccerMatchId;
  }

  if (model.livestreamStatus) {
    filterMatchStream.Status = model.livestreamStatus;
  }

  if (model.title) {
    const containKeywords = GetRelativeSearchPattern(model.title);
    filterMatchStream.Title = containKeywords;
  }

  if (model.dateStartAt) {
    const startOfDay = moment(model.dateStartAt).startOf('day').toDate();
    const endOfDay = moment(model.dateStartAt).endOf('day').toDate();
    filterMatchStream.StartAt = {
      $gte: startOfDay, $lte: endOfDay
    };
  }

  const matchStreams = await SoccerMatchStream.pagination(filterMatchStream, model.page, model.size, model.orderBy, model.order);

  const competitions = await SoccerCompetition.find(filterCompetition);

  const rounds = await SoccerRound.find(filterRound);

  const matchs = await SoccerMatch.find(filterMatch);

  return matchStreams.map(ms => {
    const matchStream = ms.toJsonObject();

    const competition = competitions.find(function(c) {
      return c.id == matchStream.SoccerCompetitionId;
    });
    const round = rounds.find(function(r) {
      return r.id == matchStream.SoccerRoundId;
    });
    const match = matchs.find(function(m) {
      return m.id == matchStream.SoccerMatchId;
    });

    return {
      soccerCompetitionId: matchStream.SoccerCompetitionId,
      soccerCompetitionTitle: competition.Title,
      soccerRoundId: matchStream.SoccerRoundId,
      soccerRoundTitle: round.Title,
      soccerMatchId: matchStream.SoccerMatchId,
      soccerMatchTitle: match.Title,
      id: ms.id,
      title: ms.Title,
      startAt: ms.StartAt,
      status: ms.Status,
      statusTitle: translate(ms.Status, 'enums'),
      chatUrl: ms.ChatUrl,
      urlSlug: ms.UrlSlug,
      keywords: ms.Keywords,
      tags: ms.Tags,
      livestreamUrls: matchStream.LivestreamUrls.map(u => {
        return {
          label: u.Label,
          quanlity: u.Quanlity,
          link: u.Link
        };
      }),
      total: ms.total,
      actions: _getMatchStreamActions(model.userActions)
    };
  });
};

const _getMatchStreamActions = function(got) {
  const actions = [];

  if (got.includes(ActionType.MATCH_STREAM.UPDATE)) {
    actions.push(ActionType.MATCH_STREAM.UPDATE);
  }

  if (got.includes(ActionType.MATCH_STREAM.DELETE)) {
    actions.push(ActionType.MATCH_STREAM.DELETE);
  }

  return actions;
};

const getAddInfo = async function(model) {
  const { soccerMatchId } = model;

  let competition;
  let round;
  let match;
  let rounds;
  let soccerMatchs;
  let soccerMatchStream;

  if (soccerMatchId) {
    match = await SoccerMatch.findOne({ _id: soccerMatchId, IsDeleted: false });
    if (!match) {
      throw ApiError.bussinessError(Messages.match.MATCH_DOES_NOT_EXIST);
    }

    const matchObj = match.toJsonObject();

    competition = await SoccerCompetition.findOne({ _id: matchObj.SoccerCompetitionId, IsDeleted: false });

    round = await SoccerRound.findOne({ _id: matchObj.SoccerRoundId, IsDeleted: false });

    rounds = await roundService.getRoundByCompetition({ soccerCompetitionId: competition.id });

    soccerMatchs = await roundService.getMatchs({ soccerRoundId: round.id });

    soccerMatchStream = {
      soccerCompetitionId: matchObj.SoccerCompetitionId,
      soccerCompetitionTitle: competition.Title,
      soccerRoundId: matchObj.SoccerRoundId,
      soccerRoundTitle: round.Title,
      soccerMatchId: matchObj._id,
      soccerMatchTitle: matchObj.Title
    };
  }

  // Get competitions
  const competitions = await SoccerCompetition.find({ IsDeleted: false }).sort('-CreateAt');

  // Get adsConfigurations
  const adsConfigurations = await AdsConfiguration.find({ IsDeleted: false, IsActive: true });

  // Get livestream statuses
  const livestreamStatuses = Object.keys(LivestreamStatus).map(s => {
    return {
      value: LivestreamStatus[s],
      title: translate(LivestreamStatus[s], 'enums')
    };
  });

  // Get livestream video unit
  const livestreamVideoUnits = Object.keys(LivestreamVideoUnit).map(s => {
    return {
      value: LivestreamVideoUnit[s],
      title: translate(LivestreamVideoUnit[s], 'enums')
    };
  });

  return {
    soccerMatchStream: soccerMatchStream,
    soccerCompetitions: competitions.map(c => {
      return {
        id: c.id,
        title: c.Title,
        season: c.Season,
        logo: c.Logo,
        startAt: c.StartAt,
        endAt: c.EndAt,
        place: c.Place
      };
    }),
    soccerRounds: rounds,
    soccerMatchs: soccerMatchs,
    adsConfigurations: adsConfigurations.map(ac => {
      return {
        id: ac.id,
        title: ac.Title,
        description: ac.Description,
        adsData: ac.AdsData,
        isActive: ac.IsActive
      };
    }),
    livestreamStatuses: livestreamStatuses,
    livestreamVideoUnits: livestreamVideoUnits
  };
};

const addMatchStream = async function(model) {
  const soccerMatchId = model.soccerMatchId;

  // check slug link
  if (model.urlSlug !== undefined &&
    model.urlSlug !== null &&
    model.urlSlug !== '') {

    const streamWithSameSlugs = await SoccerMatchStream.countDocuments({
      IsDeleted: false,
      UrlSlug: model.urlSlug
    });
    if (streamWithSameSlugs > 0) {
      throw ApiError.bussinessError(Messages.matchStream.HAS_STREAM_WITH_THE_SAME_URL_SLUG);
    }
  }

  // check soccer match
  const streamWithSameMatchs = await SoccerMatchStream.countDocuments({ IsDeleted: false, SoccerMatchId: soccerMatchId });
  if (streamWithSameMatchs > 0) {
    throw ApiError.bussinessError(Messages.matchStream.HAS_STREAM_WITH_THE_SAME_MATCH);
  }

  const match = await SoccerMatch.findOne({ _id: soccerMatchId, IsDeleted: false });
  if (!match) {
    throw ApiError.bussinessError(Messages.match.MATCH_DOES_NOT_EXIST);
  }

  const sms = new SoccerMatchStream({
    SoccerCompetitionId: match.SoccerCompetitionId,
    SoccerCompetitionStageId: match.SoccerCompetitionStageId,
    SoccerRoundId: match.SoccerRoundId,
    SoccerMatchId: match._id,
    Title: model.title,
    Description: model.description,
    StartAt: model.startAt,
    ChatUrl: model.chatUrl,
    UrlSlug: model.urlSlug,
    Keywords: model.keywords,
    AdsConfigurationId: model.adsConfigurationId,
    Tags: model.tags,
    Status: model.status,
    LivestreamUrls: model.livestreamUrls.map(u => {
      return {
        Label: u.label,
        Quanlity: u.quanlity,
        Link: u.link
      };
    })
  });

  const matchStream = await sms.save();
  return matchStream.id;
};

const getEditInfo = async function(model) {
  const soccerMatchStreamId = model.soccerMatchStreamId;

  const matchStreamModel = await SoccerMatchStream.findOne({ _id: soccerMatchStreamId, IsDeleted: false });
  if (!matchStreamModel) {
    throw ApiError.bussinessError(Messages.matchStream.MATCH_DOES_NOT_EXIST);
  }

  const matchStream = matchStreamModel.toJsonObject();

  // Get adsConfigurations
  const adsConfigurations = await AdsConfiguration.find({ IsDeleted: false, IsActive: true });

  let adsConfiguration;

  if (matchStream.AdsConfigurationId) {
    adsConfiguration = await AdsConfiguration.findOne({ _id: matchStream.AdsConfigurationId, IsDeleted: false });
  }

  const competition = await SoccerCompetition.findOne({ _id: matchStream.SoccerCompetitionId, IsDeleted: false });

  const competitions = await SoccerCompetition.find({ IsDeleted: false }).sort('-CreateAt');

  const round = await SoccerRound.findOne({ _id: matchStream.SoccerRoundId, IsDeleted: false });

  const rounds = await roundService.getRoundByCompetition({ soccerCompetitionId: competition.id });

  const match = await SoccerMatch.findOne({ _id: matchStream.SoccerMatchId, IsDeleted: false });

  const soccerMatchs = await roundService.getMatchs({ soccerRoundId: round.id });

  // Get livestream statuses
  const livestreamStatuses = Object.keys(LivestreamStatus).map(s => {
    return {
      value: LivestreamStatus[s],
      title: translate(LivestreamStatus[s], 'enums')
    };
  });

  // Get livestream video unit
  const livestreamVideoUnits = Object.keys(LivestreamVideoUnit).map(s => {
    return {
      value: LivestreamVideoUnit[s],
      title: translate(LivestreamVideoUnit[s], 'enums')
    };
  });

  return {
    soccerMatchStream: {
      soccerCompetitionId: matchStream.SoccerCompetitionId,
      soccerCompetitionTitle: competition.Title,
      soccerRoundId: matchStream.SoccerRoundId,
      soccerRoundTitle: round.Title,
      soccerMatchId: matchStream.SoccerMatchId,
      soccerMatchTitle: match.Title,
      id: matchStream._id,
      title: matchStream.Title,
      description: matchStream.Description,
      startAt: matchStream.StartAt,
      status: matchStream.Status,
      statusTitle: translate(matchStream.Status, 'enums'),
      adsConfigurationId: adsConfiguration ? adsConfiguration.id : undefined,
      adsConfigurationTitle: adsConfiguration ? adsConfiguration.Title : undefined,
      chatUrl: matchStream.ChatUrl,
      urlSlug: matchStream.UrlSlug,
      keywords: matchStream.Keywords,
      tags: matchStream.Tags,
      livestreamUrls: matchStream.LivestreamUrls.map(u => {
        return {
          label: u.Label,
          quanlity: u.Quanlity,
          link: u.Link
        };
      })
    },
    adsConfigurations: adsConfigurations.map(ac => {
      return {
        id: ac.id,
        title: ac.Title,
        description: ac.Description,
        adsData: ac.AdsData,
        isActive: ac.IsActive
      };
    }),
    soccerCompetitions: competitions.map(c => {
      return {
        id: c.id,
        title: c.Title,
        season: c.Season,
        logo: c.Logo,
        startAt: c.StartAt,
        endAt: c.EndAt,
        place: c.Place
      };
    }),
    soccerRounds: rounds,
    soccerMatchs: soccerMatchs,
    livestreamStatuses: livestreamStatuses,
    livestreamVideoUnits: livestreamVideoUnits
  };
};

const updateMatchStream = async function(model) {
  const soccerMatchStreamId = model.soccerMatchStreamId;

  const matchStream = await SoccerMatchStream.findOne({ _id: soccerMatchStreamId, IsDeleted: false });
  if (!matchStream) {
    throw ApiError.bussinessError(Messages.matchStream.MATCH_STREAM_DOES_NOT_EXIST);
  }

  // check slug link
  if (model.urlSlug !== undefined &&
      model.urlSlug !== null &&
      model.urlSlug !== '') {

    const streamWithSameSlugs = await SoccerMatchStream.countDocuments({
      _id: { $ne: soccerMatchStreamId },
      IsDeleted: false,
      UrlSlug: model.urlSlug
    });
    if (streamWithSameSlugs > 0) {
      throw ApiError.bussinessError(Messages.matchStream.HAS_STREAM_WITH_THE_SAME_URL_SLUG);
    }
  }

  const soccerMatchId = model.soccerMatchId;

  const match = await SoccerMatch.findOne({ _id: soccerMatchId, IsDeleted: false });
  if (!match) {
    throw ApiError.bussinessError(Messages.match.MATCH_DOES_NOT_EXIST);
  }

  matchStream.SoccerCompetitionId = match.SoccerCompetitionId,
  matchStream.SoccerCompetitionStageId = match.SoccerCompetitionStageId,
  matchStream.SoccerRoundId = match.SoccerRoundId,
  matchStream.SoccerMatchId = match._id,
  matchStream.Title = model.title,
  matchStream.Description = model.description,
  matchStream.StartAt = model.startAt,
  matchStream.AdsConfigurationId = model.adsConfigurationId,
  matchStream.ChatUrl = model.chatUrl,
  matchStream.UrlSlug = model.urlSlug,
  matchStream.Keywords = model.keywords,
  matchStream.Tags = model.tags,
  matchStream.Status = model.status,
  matchStream.LivestreamUrls = model.livestreamUrls.map(u => {
    return {
      Label: u.label,
      Quanlity: u.quanlity,
      Link: u.link
    };
  });
  matchStream.EditAt = Date.now();

  return await matchStream.save();
};

const deleteMatchStream = async function(model) {
  const soccerMatchStreamId = model.soccerMatchStreamId;

  const matchStream = await SoccerMatchStream.findOne({ _id: soccerMatchStreamId, IsDeleted: false });
  if (!matchStream) {
    throw ApiError.bussinessError(Messages.matchStream.MATCH_STREAM_DOES_NOT_EXIST);
  }

  // return await matchStream.remove();
  matchStream.IsDeleted = true;
  matchStream.EditAt = Date.now();
  return await matchStream.save();
};

export {
  getSearchInfo,
  searchMatchStream,
  getAddInfo,
  addMatchStream,
  getEditInfo,
  updateMatchStream,
  deleteMatchStream
};

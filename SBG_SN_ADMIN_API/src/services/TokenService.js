import { Token } from '../data/models';

const addToken = async function(model) {
  const token = new Token(model);
  return await token.save();
};

const getToken = async function(model) {
  return await Token.findOne(model);
};

const expireToken = async function(model) {
  return await Token.update(model, { $set: { Expired: true }});
};

export {
  addToken as AddToken,
  getToken as GetToken,
  expireToken as ExpiredToken
};

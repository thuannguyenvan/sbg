import ApiError from '../utils/model/ApiError';
import { LanguageKeys } from '../data/constants';
import { ActionType } from '../data/enums';
import {
  SoccerCompetition,
  SoccerRound,
  SoccerCompetitionStage,
  SoccerMatch,
  SoccerCompetitionStageTeam,
} from '../data/models';
import { translate } from '../utils/translate';
import { GetRelativeSearchPattern } from '../utils/stringUtils';

const Messages = LanguageKeys.messages;

const searchRound = async function(model) {
  const filterStage = {
    SoccerCompetitionId: model.soccerCompetitionId,
    IsDeleted: false
  };
  const filterRound = {
    SoccerCompetitionId: model.soccerCompetitionId,
    IsDeleted: false
  };

  if (model.soccerCompetitionStageId) {
    filterRound.SoccerCompetitionStageId = model.soccerCompetitionStageId;
    filterStage._id = model.soccerCompetitionStageId;
  }

  if (model.title) {
    // const containKeywords = new RegExp(model.title, 'i');
    const containKeywords = GetRelativeSearchPattern(model.title);
    filterRound.Title = containKeywords;
  }

  const competition = await SoccerCompetition.findOne({ _id: model.soccerCompetitionId, IsDeleted: false });

  const stages = await SoccerCompetitionStage.find(filterStage);

  const rounds = await SoccerRound.pagination(filterRound, model.page, model.size, model.orderBy, model.order);


  return rounds.map(r => {
    const stage = stages.find(function(s) {
      return s.id == r.toJsonObject().SoccerCompetitionStageId;
    });
    return {
      id: r.id,
      title: r.Title,
      soccerCompetitionId: model.soccerCompetitionId,
      soccerCompetitionName: competition.Title,
      soccerCompetitionStageId: stage.id,
      soccerCompetitionStageName: stage.Title,
      startAt: r.StartAt,
      endAt: r.EndAt,
      description: r.Description,
      total: r.total,
      actions: _getActions(model.userActions)
    };
  });
};

const _getActions = function(got) {
  const actions = [];

  if (got.includes(ActionType.ROUND.UPDATE)) {
    actions.push(ActionType.ROUND.UPDATE);
  }

  if (got.includes(ActionType.ROUND.DELETE)) {
    actions.push(ActionType.ROUND.DELETE);
  }

  if (got.includes(ActionType.ROUND.ADD_MATCH)) {
    actions.push(ActionType.ROUND.ADD_MATCH);
  }

  return actions;
};

const addRound = async function(model) {
  const stage = await SoccerCompetitionStage.findOne({ _id: model.soccerCompetitionStageId, IsDeleted: false });
  if (!stage) {
    throw ApiError.bussinessError(Messages.competitionStage.STAGE_DOES_NOT_EXIST);
  }

  // check title round
  const roundCount = await SoccerRound.countDocuments({
    SoccerCompetitionStageId: model.soccerCompetitionStageId,
    Title: model.title,
    IsDeleted: false
  });

  if (roundCount > 0) {
    throw ApiError.bussinessError(Messages.round.HAS_ROUND_WITH_SAME_STAGE_AND_TITLE);
  }

  let round = new SoccerRound({
    SoccerCompetitionId: stage.SoccerCompetitionId,
    SoccerCompetitionStageId: stage._id,
    Title: model.title,
    StartAt: model.startAt,
    EndAt: model.endAt,
    Description: model.description
  });

  round = await round.save();

  return round.toJsonObject();
};

const getEditInfo = async function(model) {
  const round = await SoccerRound.findOne({ _id: model.soccerRoundId, IsDeleted: false });
  if (!round) {
    throw ApiError.bussinessError(Messages.round.ROUND_DOES_NOT_EXIST);
  }

  const competition = await SoccerCompetition.findOne({ _id: round.SoccerCompetitionId, IsDeleted: false });

  const stage = await SoccerCompetitionStage.findOne({ _id: round.SoccerCompetitionStageId, IsDeleted: false });

  return {
    id: round.id,
    title: round.Title,
    soccerCompetitionId: competition.id,
    soccerCompetitionName: competition.Title,
    soccerCompetitionStageId: stage.id,
    soccerCompetitionStageName: stage.Title,
    startAt: round.StartAt,
    endAt: round.EndAt,
    description: round.Description
  };
};

const updateRound = async function(model) {
  const round = await SoccerRound.findOne({ _id: model.soccerRoundId, IsDeleted: false });
  if (!round) {
    throw ApiError.bussinessError(Messages.round.ROUND_DOES_NOT_EXIST);
  }

  // check title round
  const roundCount = await SoccerRound.countDocuments({
    _id: { $ne: model.soccerRoundId },
    SoccerCompetitionStageId: round.SoccerCompetitionStageId,
    Title: model.title,
    IsDeleted: false
  });

  if (roundCount > 0) {
    throw ApiError.bussinessError(Messages.round.HAS_ROUND_WITH_SAME_STAGE_AND_TITLE);
  }

  round.Title = model.title;
  round.StartAt = model.startAt;
  round.EndAt = model.endAt;
  round.Description = model.description;
  round.EditAt = Date.now();

  await round.save();

  return true;
};

const deleteRound = async function(model) {
  const round = await SoccerRound.findOne({ _id: model.soccerRoundId, IsDeleted: false });
  if (!round) {
    throw ApiError.bussinessError(Messages.round.ROUND_DOES_NOT_EXIST);
  }

  // check trận đấu trong round đấu
  const matchCount = await SoccerMatch.countDocuments({ SoccerRoundId: round._id, IsDeleted: false });

  if (matchCount > 0) {
    throw ApiError.bussinessError(Messages.round.CANNOT_DELETE_ROUND_HAVING_MATCH);
  }

  // await round.remove();
  round.IsDeleted = true;
  round.EditAt = Date.now();

  await round.save();

  return true;
};

const getRoundByCompetition = async function(model) {
  const soccerCompetitionId = model.soccerCompetitionId;

  const competition = await SoccerCompetition.findOne({ _id: soccerCompetitionId, IsDeleted: false });
  if (!competition) {
    throw ApiError.bussinessError(Messages.competition.COMPETITION_DOES_NOT_EXIST);
  }

  const stages = await SoccerCompetitionStage.find({ SoccerCompetitionId: soccerCompetitionId, IsDeleted: false });

  const rounds = await SoccerRound.find({ SoccerCompetitionId: soccerCompetitionId, IsDeleted: false });

  return rounds.map(r => {
    const stage = stages.find(function(s) {
      return s.id == r.toJsonObject().SoccerCompetitionStageId;
    });
    return {
      id: r.id,
      title: r.Title,
      soccerCompetitionId: model.soccerCompetitionId,
      soccerCompetitionName: competition.Title,
      soccerCompetitionStageId: stage.id,
      soccerCompetitionStageName: stage.Title,
      startAt: r.StartAt,
      endAt: r.EndAt,
      description: r.Description
    };
  });
};

const getTeams = async function(model) {
  const soccerRoundId = model.soccerRoundId;
  const round = await SoccerRound.findOne({ _id: soccerRoundId, IsDeleted: false });
  if (!round) {
    throw ApiError.bussinessError(Messages.round.ROUND_DOES_NOT_EXIST);
  }
  const teams = await SoccerCompetitionStageTeam.find({ SoccerCompetitionStageId: round.SoccerCompetitionStageId, IsDeleted: false });
  return teams.map(t => {
    const team = t.toJsonObject();
    return {
      soccerCompetitionStageTeamId: team._id,
      soccerCompetitionTeamId: team.SoccerCompetitionTeamId,
      soccerTeamId: team.SoccerTeamId,
      soccerCompetitionId: team.SoccerCompetitionId,
      soccerCompetitionStageId: team.SoccerCompetitionStageId,
      soccerRoundId: round.id,
      group: team.Group,
      name: team.Name,
      teamCode: team.TeamCode,
      logo: team.Logo
    };
  });
};

function _buildScore(teamA, teamB) {
  if (teamA !== undefined && teamA !== null
      && teamB !== undefined && teamB !== null) {
    return {
      teamA: teamA,
      teamB: teamB
    };
  } else {
    return undefined;
  }
}

const getMatchs = async function(model) {
  const soccerRoundId = model.soccerRoundId;
  const round = await SoccerRound.findOne({ _id: soccerRoundId, IsDeleted: false });
  if (!round) {
    throw ApiError.bussinessError(Messages.round.ROUND_DOES_NOT_EXIST);
  }
  const matchs = await SoccerMatch.find({ SoccerRoundId: soccerRoundId, IsDeleted: false });

  const competition = await SoccerCompetition.findOne({ _id: round.SoccerCompetitionId, IsDeleted: false });

  const teamIds = matchs.map(m => m.toJsonObject().TeamA).concat(matchs.map(m => m.toJsonObject().TeamB));

  const teams = await SoccerCompetitionStageTeam.find({
    _id: {
      $in: teamIds
    },
    IsDeleted: false
  });

  return matchs.map(m => {
    const match = m.toJsonObject();
    const teamA = teams.find(function(t) {
      return t.id == match.TeamA;
    });
    const teamB = teams.find(function(t) {
      return t.id == match.TeamB;
    });

    return {
      soccerCompetitionId: match.SoccerCompetitionId,
      soccerCompetitionTitle: competition.Title,
      soccerRoundId: match.SoccerRoundId,
      soccerRoundTitle: round.Title,
      soccerMatchId: m.id,
      title: m.Title,
      startAt: m.StartAt,
      status: m.Status,
      statusTitle: translate(m.Status, 'enums'),
      teamA: {
        soccerCompetitionStageTeamId: match.TeamA,
        soccerCompetitionTeamId: teamA.toJsonObject().SoccerCompetitionTeamId,
        soccerTeamId: teamA.toJsonObject().SoccerTeamId,
        name: teamA.Name,
        teamCode: teamA.TeamCode,
        logo: teamA.Logo
      },
      teamB: {
        soccerCompetitionStageTeamId: match.TeamB,
        soccerCompetitionTeamId: teamB.toJsonObject().SoccerCompetitionTeamId,
        soccerTeamId: teamB.toJsonObject().SoccerTeamId,
        name: teamB.Name,
        teamCode: teamB.TeamCode,
        logo: teamB.Logo
      },
      winner: match.Winner,
      score: {
        fullTime: _buildScore(match.ScoreA, match.ScoreB),
        halfTime: _buildScore(match.ScoreAI, match.ScoreBI),
        extraTime: _buildScore(match.ScoreAET, match.ScoreBET),
        penalty: _buildScore(match.ScoreAP, match.ScoreBP),
        aggregate: _buildScore(match.ScoreAAGG, match.ScoreBAGG)
      },
    };
  });
};

export {
  searchRound,
  addRound,
  getEditInfo,
  updateRound,
  deleteRound,
  getRoundByCompetition,
  getTeams,
  getMatchs
};

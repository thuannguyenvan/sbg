import { Action, RoleAction, Role } from '../data/models';
import ApiError from '../utils/model/ApiError';

const addAction = function (model) {
  const action = new Action(model);
  return action.save();
};

const initNewAction = async function (model) {
  const act = new Action({
    Name: model.ActionName,
    Key: model.ActionKey,
    Desciption: model.ActionDescription || model.ActionName,
  });
  const action = await act.save();
  // add for ADMIN role
  const adminRole = await Role.findOne({ Key: model.RoleKey });
  const adminRoleAction = new RoleAction({
    RoleId: adminRole.id,
    ActionId: action.id
  });
  return await adminRoleAction.save();
};

const addRoleAction = async function (model) {
  const actionKey = model.actionKey;
  const roleKey = model.roleKey;
  const action = await Action.findOne({ Key: actionKey });
  const role = await Role.findOne({ Key: roleKey });

  if (action == null || role == null) {
    throw ApiError.bussinessError('Không tồn tại ACTION hoặc ROLE này.');
  }

  const roleAction = await RoleAction.findOne({ ActionId: action.id, RoleId: role.id });

  if (roleAction != null) {
    throw ApiError.bussinessError(`Action [${actionKey}] đã được gán cho Role [${roleKey}].`);
  }

  const roleActionModel = new RoleAction({ ActionId: action.id, RoleId: role.id });

  return await roleActionModel.save({ ActionId: action.id, RoleId: role.id });
};

const deleteRoleAction = async function (model) {
  const actionKey = model.actionKey;
  const roleKey = model.roleKey;
  const action = await Action.findOne({ Key: actionKey });
  const role = await Role.findOne({ Key: roleKey });

  if (action == null || role == null) {
    throw ApiError.bussinessError('Không tồn tại ACTION hoặc ROLE này.');
  }

  const roleAction = await RoleAction.findOne({ ActionId: action.id, RoleId: role.id });

  if (roleAction == null) {
    throw ApiError.bussinessError(`Action [${actionKey}] chưa được gán cho Role [${roleKey}].`);
  }

  return await roleAction.remove();
};

export {
  addAction as AddAction,
  initNewAction as InitNewAction,
  addRoleAction as AddRoleAction,
  deleteRoleAction as DeleteRoleAction
};

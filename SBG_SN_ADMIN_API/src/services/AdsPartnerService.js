// import moment from 'moment';
import mongoose from 'mongoose';
import ApiError from '../utils/model/ApiError';
import { LanguageKeys } from '../data/constants';
import {
  ActionType,
  AdsConfigUpdatingStatus,
} from '../data/enums';
import {
  AdsPartner,
  Advertising,
  AdsConfigurationDetail,
  AdsConfiguration,
} from '../data/models';
// import {
//   translate,
//   validateTemplate
// } from '../utils/translate';
import { GetRelativeSearchPattern } from '../utils/stringUtils';

const Messages = LanguageKeys.messages;
// const ValidateMessages = LanguageKeys.validate.messageTemplates;
// const ValidateLabels = LanguageKeys.validate.labels;

const searchAdsPartner = async function(model) {
  const filterAdsPartner = { IsDeleted: false };

  if (model.name) {
    const containKeywords = GetRelativeSearchPattern(model.name);
    filterAdsPartner.Name = containKeywords;
  }

  if (model.isActive !== undefined && model.isActive !== null) {
    filterAdsPartner.IsActive = model.isActive;
  }

  const adsPartners = await AdsPartner.pagination(filterAdsPartner, model.page, model.size, model.orderBy, model.order);

  return adsPartners.map(ap => {
    const apm = ap.toJsonObject();
    return {
      id: apm._id,
      name: apm.Name,
      logo: apm.Logo,
      website: apm.Website,
      description: apm.Description,
      isActive: apm.IsActive,
      total: ap.total,
      actions: _getActions(model.userActions, apm)
    };
  });
};

const _getActions = function(got, item) {
  const actions = [];

  if (got.includes(ActionType.ADS_PARTNER.UPDATE)) {
    actions.push(ActionType.ADS_PARTNER.UPDATE);
  }

  if (got.includes(ActionType.ADS_PARTNER.ACTIVATE)) {
    if (!item.IsActive) {
      actions.push(ActionType.ADS_PARTNER.ACTIVATE);
    }
  }

  if (got.includes(ActionType.ADS_PARTNER.DEACTIVATE)) {
    if (item.IsActive) {
      actions.push(ActionType.ADS_PARTNER.DEACTIVATE);
    }
  }

  if (got.includes(ActionType.ADS_PARTNER.DELETE)) {
    actions.push(ActionType.ADS_PARTNER.DELETE);
  }

  if (got.includes(ActionType.ADVERTISING.ADD)) {
    actions.push(ActionType.ADVERTISING.ADD);
  }

  return actions;
};

const addAdsPartner = async function(model) {
  // Kiểm tra trùng tên partner
  const countAdsPartners = await AdsPartner.countDocuments({ Name: model.name, IsDeleted: false });
  if (countAdsPartners > 0) {
    throw ApiError.bussinessError(Messages.adsPartner.ADS_PARTNER_WITH_THE_SAME_NAME_ALREADY_EXIST);
  }

  const adsPartner = new AdsPartner({
    Name: model.name,
    Logo: model.logo,
    Website: model.website,
    Description: model.description
  });

  await adsPartner.save();

  return adsPartner.id;
};

const getEditInfo = async function(model) {
  const adsPartnerId = model.adsPartnerId;
  const adsPartner = await AdsPartner.findOne({ _id: adsPartnerId, IsDeleted: false });

  if (!adsPartner) {
    throw ApiError.bussinessError(Messages.adsPartner.ADS_PARTNER_DOES_NOT_EXIST);
  }

  const apm = adsPartner.toJsonObject();

  return {
    id: apm._id,
    name: apm.Name,
    logo: apm.Logo,
    website: apm.Website,
    description: apm.Description,
    isActive: apm.IsActive
  };
};

const updateAdsPartner = async function(model) {
  const adsPartnerId = model.adsPartnerId;

  // Kiểm tra trùng tên partner
  const countAdsPartners = await AdsPartner.countDocuments({
    _id: { $ne: adsPartnerId },
    Name: model.name,
    IsDeleted: false
  });

  if (countAdsPartners > 0) {
    throw ApiError.bussinessError(Messages.adsPartner.ADS_PARTNER_WITH_THE_SAME_NAME_ALREADY_EXIST);
  }

  const session = await mongoose.startSession();
  session.startTransaction();

  try {
    const opts = { session };

    const adsPartner = await AdsPartner.findOne({ _id: adsPartnerId, IsDeleted: false }, {}, opts);

    if (!adsPartner) {
      throw ApiError.bussinessError(Messages.adsPartner.ADS_PARTNER_DOES_NOT_EXIST);
    }

    adsPartner.Name = model.name;
    adsPartner.Logo = model.logo;
    adsPartner.Website = model.website;
    adsPartner.Description = model.description;
    adsPartner.EditAt = Date.now();
    await adsPartner.save();

    const advertisings = await Advertising.find({ AdsPartnerId: adsPartnerId, IsDeleted: false }, {}, opts);

    if (advertisings && advertisings.length > 0) {
      await Advertising.updateMany(
        { AdsPartnerId: adsPartnerId, IsDeleted: false },
        { $set: {
          AdsPartnerName: model.name,
          AdsPartnerLogo: model.logo,
          AdsPartnerWebsite: model.website,
          EditAt: Date.now()
        } },
        opts
      );
    }

    await session.commitTransaction();
    session.endSession();
    return true;
  } catch (err) {
    await session.abortTransaction();
    session.endSession();
    if (!err.errorType) {
      const message = model.isActivate
                      ? Messages.adsPartner.ACTIVATE_ADS_PARTNER_FAIL
                      : Messages.adsPartner.DEACTIVATE_ADS_PARTNER_FAIL;
      throw ApiError.error(message, null, err);
    } else {
      throw err;
    }
  }
};

const deleteAdsPartner = async function(model) {
  const adsPartnerId = model.adsPartnerId;
  const adsPartner = await AdsPartner.findOne({ _id: adsPartnerId, IsDeleted: false });

  if (!adsPartner) {
    throw ApiError.bussinessError(Messages.adsPartner.ADS_PARTNER_DOES_NOT_EXIST);
  }

  // check has advertising
  const advertisingCounts = await Advertising.countDocuments({ AdsPartnerId: adsPartnerId, IsDeleted: false });
  if (advertisingCounts > 0) {
    throw ApiError.bussinessError(Messages.adsPartner.CANNOT_DELETE_ADS_PARTNER_CAUSE_HAVE_ADVERTISING);
  }

  adsPartner.IsDeleted = true;
  adsPartner.EditAt = Date.now();
  await adsPartner.save();

  return true;
};

const activateDeactivateAdsPartner = async function(model) {
  const adsPartnerId = model.adsPartnerId;

  const session = await mongoose.startSession();
  session.startTransaction();
  try {
    const opts = { session };

    const adsPartner = await AdsPartner.findOne({ _id: adsPartnerId, IsDeleted: false }, {}, opts);

    if (!adsPartner) {
      throw ApiError.bussinessError(Messages.adsPartner.ADS_PARTNER_DOES_NOT_EXIST);
    }

    // check activive or deactive
    if (adsPartner.IsActive && model.isActivate) {
      throw ApiError.bussinessError(Messages.adsPartner.ADS_PARTNER_ALREADY_ACTIVE);
    }
    if (!adsPartner.IsActive && !model.isActivate) {
      throw ApiError.bussinessError(Messages.adsPartner.ADS_PARTNER_ALREADY_INACTIVE);
    }

    // Activate
    if (model.isActivate) {

      adsPartner.IsActive = true;
      adsPartner.EditAt = Date.now();

      await adsPartner.save();
    } else { // Deactivate

      const advertisings = await Advertising.find({ AdsPartnerId: adsPartnerId, IsDeleted: false, IsActive: true }, {}, opts);
      const adsConfigDetails = await AdsConfigurationDetail.find({ AdsPartnerId: adsPartnerId, IsDeleted: false }, {}, opts);

      if (advertisings && advertisings.length > 0) {
        await Advertising.updateMany(
          { AdsPartnerId: adsPartnerId, IsDeleted: false, IsActive: true },
          { $set: { IsActive: false, EditAt: Date.now() } },
          opts
        );
      }

      if (adsConfigDetails && adsConfigDetails.length > 0) {
        // Delete AdsConfigurationDetails
        await AdsConfigurationDetail.updateMany(
          { AdsPartnerId: adsPartnerId, IsDeleted: false },
          { $set: { IsDeleted: true, EditAt: Date.now() } },
          opts
        );

        const adsConfigIds = adsConfigDetails.map(acd => {
          const detail = acd.toJsonObject();
          return detail.AdsConfigurationId;
        });

        // Update UPDATING_STATUS of AdsConfigurations to NEED_UPDATE
        await AdsConfiguration.updateMany(
          { _id: { $in: adsConfigIds }, IsDeleted: false },
          { $set: { UpdatingStatus: AdsConfigUpdatingStatus.NEED_UPDATE, EditAt: Date.now() } },
          opts
        );
      }

      await AdsPartner.updateOne(
        { _id: adsPartnerId, IsDeleted: false, IsActive: true },
        { $set: { IsActive: false, EditAt: Date.now() } },
        opts
      );
    }

    await session.commitTransaction();
    session.endSession();
    return true;
  } catch (err) {
    await session.abortTransaction();
    session.endSession();
    if (!err.errorType) {
      const message = model.isActivate
                      ? Messages.adsPartner.ACTIVATE_ADS_PARTNER_FAIL
                      : Messages.adsPartner.DEACTIVATE_ADS_PARTNER_FAIL;
      throw ApiError.error(message, null, err);
    } else {
      throw err;
    }
  }
};

export {
  searchAdsPartner,
  addAdsPartner,
  getEditInfo,
  updateAdsPartner,
  deleteAdsPartner,
  activateDeactivateAdsPartner,
};

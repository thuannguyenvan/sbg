import {
  SoccerTeam
} from '../data/models';
import { GetRelativeSearchPattern } from '../utils/stringUtils';

const getTeams = async function(model) {
  if (!model) model = {};
  let filter = { IsDeleted: false };
  if (model.name) {
    // const containKeywords = new RegExp(model.name, 'i');
    const containKeywords = GetRelativeSearchPattern(model.name);
    filter = {
      $and: [
        { IsDeleted: false },
        { $or: [
          { Name: containKeywords },
          { '_Filter.Name': containKeywords }
        ]}
      ]
    };
  }

  const tms = await SoccerTeam.pagination(filter, model.page, model.size, '_Filter.Name', 'ASC');
  return tms.map(s => {
    const team = s.toJsonObject();
    return {
      id: team._id,
      name: team.Name,
      teamCode: team.TeamCode,
      logo: team.Logo,
      isClub: team.IsClub,
      content: team.Content,
      description: team.Description,
      teamInfo: team.TeamInfo
    };
  });
};

const searchTeams = async function(model) {
  if (!model) model = {};
  let filter = { IsDeleted: false };
  if (model.name) {
    // const containKeywords = new RegExp(model.name, 'i');
    const containKeywords = GetRelativeSearchPattern(model.name);
    filter = {
      $and: [
        { IsDeleted: false },
        { $or: [
          { Name: containKeywords },
          { '_Filter.Name': containKeywords }
        ]}
      ]
    };
  }

  const tms = await SoccerTeam.pagination(filter, model.page, model.size, '_Filter.Name', 'ASC');
  return tms.map(s => {
    const team = s.toJsonObject();
    return {
      id: team._id,
      name: team.Name,
      teamCode: team.TeamCode,
      logo: team.Logo,
      isClub: team.IsClub,
      content: team.Content,
      description: team.Description,
      teamInfo: team.TeamInfo,
      total: team.total
    };
  });
};

export {
  getTeams,
  searchTeams
};

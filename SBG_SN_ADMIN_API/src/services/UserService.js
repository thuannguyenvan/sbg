import {
  User,
  Action,
  RoleAction,
  Role,
  CmsMenu
} from '../data/models';
import ApiError from '../utils/model/ApiError';
import { LanguageKeys } from '../data/constants';
import { ActionType } from '../data/enums';
import {
  ValidObjectId,
  GetValidObjectId,
  GetRelativeSearchPattern
} from '../utils/stringUtils';

const Messages = LanguageKeys.messages;

const getUser = async function(model) {
  return await User.findOne(model);
};

const getRolesFromUser = async function (user) {
  const roles = await Role.find({ _id: { $in: user.Roles }, IsDeleted: false });
  return roles.map(r => r.toJsonObject());
};

const getUserActions = async function (model) {
  const user = await User.findOne({ _id: model.UserId, IsDeleted: false });
  const roleActions = await RoleAction.find({ RoleId: { $in: user.Roles }, IsDeleted: false });
  const actionIds = roleActions.map(ra => ra.ActionId);
  const actions = await Action.find({ _id: { $in: actionIds }, IsDeleted: false, IsActive: true });
  return actions.map(a => a.Key);
};

const getUserInfo = async function (model, opts) {
  const user = await User.findOne({ _id: model.UserId, IsDeleted: false }, {}, opts);
  const roleActions = await RoleAction.find({ RoleId: { $in: user.Roles }, IsDeleted: false }, {}, opts);
  const actionIds = roleActions.map(ra => ra.ActionId);
  const actions = await Action.find({ _id: { $in: actionIds }, IsDeleted: false , IsActive: true }, {}, opts);
  const userActionIds = actions.map(a => a.Key.toString());
  const menus = await CmsMenu.find({ Roles: { $elemMatch: { $in: user.Roles } }, IsDeleted: false }, {}, opts).sort('Order');
  const userObj = user.toJsonObject();
  const ret = {
    id: userObj._id,
    userName: userObj.UserName,
    fullName: userObj.FullName,
    email: userObj.Email,
    phone: userObj.Phone,
    avatar: userObj.Avatar,
    roles: userObj.Roles
  };
  ret.actions = userActionIds;
  ret.menus = menus.map(i => i.toJsonObject());
  ret.supportedLanguage = global.CONFIG.SUPPORTED_LANGUAGE;
  return ret;
};

const searchUser = async function (model) {
  let filter = { IsDeleted: false };

  if (model.keywords) {
    // const containKeywords = new RegExp(model.keywords, 'i');
    const containKeywords = GetRelativeSearchPattern(model.keywords);

    filter = {
      $and: [
        { IsDeleted: false },
        { $or: [
          { UserName: containKeywords },
          { FullName: containKeywords },
          { Email: containKeywords },
        ]}
      ]
    };
  }

  const users = await User.pagination(filter, model.page, model.size, model.orderBy, model.order);
  const userObjs = users.map(u => u.toJsonObject());
  const roleObjectIds = users.reduce((a, u) => {
    return [].concat(a, u.Roles);
  }, []);
  const roles = await Role.find({ _id: { $in: roleObjectIds }, IsDeleted: false, IsActive: true });
  const roleObjs = roles.map(r => r.toJsonObject());

  const ret = userObjs.map(u => {
    const roles = roleObjs.filter(r => {
      return u.Roles.includes(r._id);
    });

    const retRoles = roles.map(r => {
      return {
        id: r._id,
        name: r.Name,
        description: r.Description
      };
    });

    return {
      id: u._id,
      userName: u.UserName,
      fullName: u.FullName,
      email: u.Email,
      roles: retRoles,
      actions: _getActions(model.userActions, u),
      total: u.total
    };
  });

  return ret;
};

const _getActions = function(got, item) {
  const actions = [];

  if (got.includes(ActionType.USER.UPDATE_ROLES)) {
    actions.push(ActionType.USER.UPDATE_ROLES);
  }

  if (got.includes(ActionType.USER.ACTIVATE)) {
    if (!item.IsActive) {
      actions.push(ActionType.USER.ACTIVATE);
    }
  }

  if (got.includes(ActionType.USER.DEACTIVATE)) {
    if (item.IsActive) {
      actions.push(ActionType.USER.DEACTIVATE);
    }
  }

  return actions;
};

const _getUserRoles = async function (model) {
  // get  user roleId strings from model
  const userRoles = model.userRoles;

  // Get all roles
  const roles = await Role.find({ IsDeleted: false, IsActive: true }).sort('Order');

  const roleObjects = roles.map(r => {
    const rol = r.toJsonObject();
    let checked = false;
    checked = userRoles.includes(rol._id);
    return {
      id: rol._id,
      name: rol.Name,
      checked: checked
    };
  });

  return roleObjects;
};

const getUserRoles = async function (model) {
  // Get user
  if (!ValidObjectId(model.userId)) {
    throw ApiError.bussinessError(Messages.user.INVALID_USER_ID);
  }
  const user = await User.findOne({ _id: model.userId, IsDeleted: false });
  if (!user) {
    throw ApiError.bussinessError(Messages.user.USER_DOES_NOT_EXIST);
  }
  const userRoles = user.Roles.map(r => r.toString());

  return await _getUserRoles({ userRoles: userRoles });
};

const updateUserRoles = async function (model) {
  // Get user
  if (!ValidObjectId(model.userId)) {
    throw ApiError.bussinessError(Messages.user.INVALID_USER_ID);
  }
  const user = await User.findOne({ _id: model.userId, IsDeleted: false });
  if (!user) {
    throw ApiError.bussinessError(Messages.user.USER_DOES_NOT_EXIST);
  }

  const checkedRoles = model.roles.filter(r => r.checked);

  const validRoleIds = GetValidObjectId(checkedRoles.map(r => r.id));
  const roles = await Role.find({ _id: { $in: validRoleIds }, IsDeleted: false, IsActive: true });

  if (!roles || roles.length <= 0) {
    throw ApiError.bussinessError(Messages.user.ROLES_MUST_BE_SELECTED_AT_LEAST_ONE);
  }

  const roleIds = roles.map(r => r._id);

  // Update roles for user
  user.Roles = roleIds;
  user.EditAt = Date.now();
  const updated = await user.save();

  // get roles after update
  const userRoles = updated.Roles.map(r => r.toString());
  return await _getUserRoles({ userRoles: userRoles });
};

const activateDeactiveUser = async function (model) {
  // Get user
  if (!ValidObjectId(model.userId)) {
    throw ApiError.bussinessError(Messages.user.INVALID_USER_ID);
  }
  const user = await User.findOne({ _id: model.userId, IsDeleted: false });
  if (!user) {
    throw ApiError.bussinessError(Messages.user.USER_DOES_NOT_EXIST);
  }

  // check activive or deactive
  if (user.IsActive && model.isActivate) {
    throw ApiError.bussinessError(Messages.user.USER_ALREADY_ACTIVE);
  }
  if (!user.IsActive && !model.isActivate) {
    throw ApiError.bussinessError(Messages.user.USER_ALREADY_INACTIVE);
  }

  // activate or deactive user
  user.IsActive = model.isActivate;
  user.EditAt = Date.now();
  const updated = await user.save();
  return !!updated;
};

export {
  getUser as GetUser,
  getUserActions as GetUserActions,
  getUserInfo as GetUserInfo,
  searchUser as SearchUser,
  getUserRoles as GetUserRoles,
  updateUserRoles as UpdateUserRoles,
  getRolesFromUser as GetRolesFromUser,
  activateDeactiveUser as ActivateDeactiveUser
};

import {
  SoccerCompetitionBase
} from '../data/models';
import { GetRelativeSearchPattern } from '../utils/stringUtils';

const getCompetitionBase = async function(model) {
  if (!model) model = {};
  let filter = { IsDeleted: false };
  if (model.title) {
    // const containKeywords = new RegExp(model.title, 'i');
    const containKeywords = GetRelativeSearchPattern(model.title);
    filter.Title = containKeywords;
  }

  const comps = await SoccerCompetitionBase.pagination(filter, model.page, model.size, model.orderBy, model.order);
  const compObjs = comps.map(c => c.toJsonObject());
  return compObjs;
};

export {
  getCompetitionBase
};

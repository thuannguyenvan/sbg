import {
  SoccerStage
} from '../data/models';
import { GetRelativeSearchPattern } from '../utils/stringUtils';

const getStage = async function(model) {
  if (!model) model = {};
  let filter = { IsDeleted: false };
  if (model.title) {
    // const containKeywords = new RegExp(model.keywords, 'i');
    const containKeywords = GetRelativeSearchPattern(model.keywords);
    filter.Title = containKeywords;
  }

  const sts = await SoccerStage.pagination(filter, model.page, model.size, 'Order', 'ASC');
  const stages = sts.map(s => s.toJsonObject());
  return stages;
};

export {
  getStage
};

import mongoose from 'mongoose';
import ApiError from '../utils/model/ApiError';
import { LanguageKeys } from '../data/constants';
import {
  ActionType,
  AdsConfigUpdatingStatus,
  AdsInVideoConfigType,
} from '../data/enums';
import {
  // AdsPartner,
  // Advertising,
  AdsConfiguration,
  Advertising,
  AdsConfigurationDetail,
  SoccerMatchStream,
  // AdsConfigurationDetail,
} from '../data/models';
// import ApiError from '../utils/model/ApiError';
// import {
//   translate,
//   // validateTemplate
// } from '../utils/translate';
import { GetRelativeSearchPattern } from '../utils/stringUtils';

const Messages = LanguageKeys.messages;
// const ValidateMessages = LanguageKeys.validate.messageTemplates;
// const ValidateLabels = LanguageKeys.validate.labels;

const searchAdsConfiguration = async function(model) {
  const filterAdsConfiguration = { IsDeleted: false };

  if (model.isActive !== undefined && model.isActive !== null) {
    filterAdsConfiguration.IsActive = model.isActive;
  }

  if (model.title) {
    const containKeywords = GetRelativeSearchPattern(model.title);
    filterAdsConfiguration.Title = containKeywords;
  }

  const adsConfigurations = await AdsConfiguration.pagination(filterAdsConfiguration, model.page, model.size, model.orderBy, model.order);

  return adsConfigurations.map(ac => {
    const adsConfig = ac.toJsonObject();
    return {
      id: adsConfig._id,
      title: adsConfig.Title,
      description: adsConfig.Description,
      adsData: adsConfig.AdsData,
      isActive: adsConfig.IsActive,
      total: ac.total,
      actions: _getActions(model.userActions, adsConfig)
    };
  });
};

const _getActions = function(got, item) {
  const actions = [];

  if (got.includes(ActionType.ADS_CONFIGURATION.UPDATE)) {
    actions.push(ActionType.ADS_CONFIGURATION.UPDATE);
  }

  if (got.includes(ActionType.ADS_CONFIGURATION.ACTIVATE)) {
    if (!item.IsActive) {
      actions.push(ActionType.ADS_CONFIGURATION.ACTIVATE);
    }
  }

  if (got.includes(ActionType.ADS_CONFIGURATION.DEACTIVATE)) {
    if (item.IsActive) {
      actions.push(ActionType.ADS_CONFIGURATION.DEACTIVATE);
    }
  }

  if (got.includes(ActionType.ADS_CONFIGURATION.DELETE)) {
    actions.push(ActionType.ADS_CONFIGURATION.DELETE);
  }

  return actions;
};

const addAdsConfiguration = async function(model) {
  if (!model.config) model.config = {};
  if (!model.config.preRolls) model.config.preRolls = [];
  if (!model.config.midRolls) model.config.midRolls = [];
  if (!model.config.postRolls) model.config.postRolls = [];

  // Kiểm tra trùng tên cấu hình quảng cáo
  const countAdsConfigurations = await AdsConfiguration.countDocuments({ Title: model.title, IsDeleted: false });
  if (countAdsConfigurations > 0) {
    throw ApiError.bussinessError(Messages.adsConfiguration.ADS_CONFIGURATION_WITH_THE_SAME_TITLE_ALREADY_EXIST);
  }

  const session = await mongoose.startSession();
  session.startTransaction();

  try {
    const opts = { session };

    const adsConfiguration = new AdsConfiguration({
      Title: model.title,
      Description: model.description,
      UpdatingStatus: AdsConfigUpdatingStatus.NEED_UPDATE
    });
    adsConfiguration.save();

    const adsConfigurationId = adsConfiguration.id;

    let advertisingIds = [];
    advertisingIds = advertisingIds.concat(model.config.preRolls.map(c => c.advertisingId));
    advertisingIds = advertisingIds.concat(model.config.midRolls.map(c => c.advertisingId));
    advertisingIds = advertisingIds.concat(model.config.postRolls.map(c => c.advertisingId));

    const advertisings = await Advertising.find({ _id: { $in: advertisingIds } }, {}, opts);
    const adsObjs = advertisings.map(a => a.toJsonObject());

    if (advertisings && advertisings.length > 0) {
      let adsConfigDetails = [];
      adsConfigDetails = adsConfigDetails.concat(model.config.preRolls.map(c => {
        const ads = adsObjs.find(a => a._id == c.advertisingId);
        return new AdsConfigurationDetail({
          AdsConfigurationId: adsConfigurationId,
          AdsPartnerId: ads.AdsPartnerId,
          AdvertisingId: ads._id,
          AdsFormat: ads.AdsFormat,
          Content: ads.Content,
          ClickThrough: ads.ClickThrough,
          ClickTracking: ads.ClickTracking,
          Config: {
            Type: AdsInVideoConfigType.PRE,
            Duration: c.duration,
            SkipOffset: c.skipOffset,
          }
        });
      }));

      adsConfigDetails = adsConfigDetails.concat(model.config.midRolls.map(c => {
        const ads = adsObjs.find(a => a._id == c.advertisingId);
        return new AdsConfigurationDetail({
          AdsConfigurationId: adsConfigurationId,
          AdsPartnerId: ads.AdsPartnerId,
          AdvertisingId: ads._id,
          AdsFormat: ads.AdsFormat,
          Content: ads.Content,
          ClickThrough: ads.ClickThrough,
          ClickTracking: ads.ClickTracking,
          Config: {
            Type: AdsInVideoConfigType.MID,
            Overlay: c.overlay,
            TimeOffset: c.timeOffset,
            Duration: c.duration,
            SkipOffset: c.skipOffset,
          }
        });
      }));

      adsConfigDetails = adsConfigDetails.concat(model.config.postRolls.map(c => {
        const ads = adsObjs.find(a => a._id == c.advertisingId);
        return new AdsConfigurationDetail({
          AdsConfigurationId: adsConfigurationId,
          AdsPartnerId: ads.AdsPartnerId,
          AdvertisingId: ads._id,
          AdsFormat: ads.AdsFormat,
          Content: ads.Content,
          ClickThrough: ads.ClickThrough,
          ClickTracking: ads.ClickTracking,
          Config: {
            Type: AdsInVideoConfigType.POST,
            Duration: c.duration,
            SkipOffset: c.skipOffset,
          }
        });
      }));

      await AdsConfigurationDetail.insertMany(adsConfigDetails, opts);
    }

    await session.commitTransaction();
    session.endSession();
    return adsConfigurationId;
  } catch (err) {
    await session.abortTransaction();
    session.endSession();
    if (!err.errorType) {
      throw ApiError.error(Messages.adsConfiguration.ADD_ADS_CONFIGURATION_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const getEditInfo = async function(model) {
  const adsConfigurationId = model.adsConfigurationId;

  // check exist ads configuration
  const adsConfiguration = await AdsConfiguration.findOne({ _id: adsConfigurationId, IsDeleted: false });
  if (!adsConfiguration) {
    throw ApiError.bussinessError(Messages.adsConfiguration.ADS_CONFIGURATION_DOES_NOT_EXIST);
  }

  const adsConfigurationDetails = await AdsConfigurationDetail.find({ AdsConfigurationId: adsConfigurationId, IsDeleted: false });

  const config = {
    preRolls: [],
    midRolls: [],
    postRolls: []
  };

  const advertisings = await Advertising.find({ _id: { $in: adsConfigurationDetails.map(a => a.toJsonObject().AdvertisingId) }, IsDeleted: false });

  for (const acd of adsConfigurationDetails.map(a => a.toJsonObject())) {
    const ads = advertisings.find(ad => ad.id == acd.AdvertisingId);
    switch (acd.Config.Type) {
      case AdsInVideoConfigType.PRE:
        config.preRolls.push({
          advertisingId: acd.AdvertisingId,
          advertisingTitle: ads.Title,
          adsPartnerName: ads.AdsPartnerName,
          duration: acd.Config.Duration,
          skipOffset: acd.Config.SkipOffset,
          clickThrough: acd.ClickThrough,
          clickTracking: acd.ClickTracking
        });
        break;
      case AdsInVideoConfigType.MID:
        config.midRolls.push({
          advertisingId: acd.AdvertisingId,
          advertisingTitle: ads.Title,
          adsPartnerName: ads.AdsPartnerName,
          timeOffset: acd.Config.TimeOffset,
          duration: acd.Config.Duration,
          skipOffset: acd.Config.SkipOffset,
          overlay: acd.Config.Overlay,
          clickThrough: acd.ClickThrough,
          clickTracking: acd.ClickTracking
        });
        break;
      case AdsInVideoConfigType.POST:
        config.postRolls.push({
          advertisingId: acd.AdvertisingId,
          advertisingTitle: ads.Title,
          adsPartnerName: ads.AdsPartnerName,
          duration: acd.Config.Duration,
          skipOffset: acd.Config.SkipOffset,
          clickThrough: acd.ClickThrough,
          clickTracking: acd.ClickTracking
        });
        break;
    }
  }

  return {
    id: adsConfiguration.id,
    title: adsConfiguration.Title,
    description: adsConfiguration.Description,
    adsData: adsConfiguration.AdsData,
    config: config
  };
};

const updateAdsConfiguration = async function(model) {
  const adsConfigurationId = model.adsConfigurationId;

  if (!model.config) model.config = {};
  if (!model.config.preRolls) model.config.preRolls = [];
  if (!model.config.midRolls) model.config.midRolls = [];
  if (!model.config.postRolls) model.config.postRolls = [];

  // check exist ads configuration
  const adsConfigurationCounts = await AdsConfiguration.countDocuments({ _id: adsConfigurationId, IsDeleted: false });
  if (adsConfigurationCounts <= 0) {
    throw ApiError.bussinessError(Messages.adsConfiguration.ADS_CONFIGURATION_DOES_NOT_EXIST);
  }

  // Kiểm tra trùng tên cấu hình quảng cáo
  const countAdsConfigurations =
      await AdsConfiguration.countDocuments({ _id: { $ne: adsConfigurationId }, Title: model.title, IsDeleted: false });
  if (countAdsConfigurations > 0) {
    throw ApiError.bussinessError(Messages.adsConfiguration.ADS_CONFIGURATION_WITH_THE_SAME_TITLE_ALREADY_EXIST);
  }

  const session = await mongoose.startSession();
  session.startTransaction();

  try {
    const opts = { session };

    const adsConfiguration = await AdsConfiguration.findOne({ _id: adsConfigurationId, IsDeleted: false }, {}, opts);

    // Update adsConfiguration info
    adsConfiguration.Title = model.title;
    adsConfiguration.Description = model.description;
    adsConfiguration.UpdatingStatus = AdsConfigUpdatingStatus.NEED_UPDATE;
    adsConfiguration.EditAt = Date.now();
    await adsConfiguration.save();

    // Delete ads configuration details
    await AdsConfigurationDetail.updateMany(
      { AdsConfigurationId: adsConfigurationId },
      { $set: {
        IsDeleted: true,
        EditAt: Date.now()
      } },
      opts
    );

    // Insert new configuration details
    let advertisingIds = [];
    advertisingIds = advertisingIds.concat(model.config.preRolls.map(c => c.advertisingId));
    advertisingIds = advertisingIds.concat(model.config.midRolls.map(c => c.advertisingId));
    advertisingIds = advertisingIds.concat(model.config.postRolls.map(c => c.advertisingId));

    const advertisings = await Advertising.find({ _id: { $in: advertisingIds } }, {}, opts);
    const adsObjs = advertisings.map(a => a.toJsonObject());

    if (advertisings && advertisings.length > 0) {
      let adsConfigDetails = [];
      adsConfigDetails = adsConfigDetails.concat(model.config.preRolls.map(c => {
        const ads = adsObjs.find(a => a._id == c.advertisingId);
        return new AdsConfigurationDetail({
          AdsConfigurationId: adsConfigurationId,
          AdsPartnerId: ads.AdsPartnerId,
          AdvertisingId: ads._id,
          AdsFormat: ads.AdsFormat,
          Content: ads.Content,
          // ClickThrough: c.clickThrough,
          // ClickTracking: c.clickTracking,
          Config: {
            Type: AdsInVideoConfigType.PRE,
            Duration: c.duration,
            SkipOffset: c.skipOffset,
          }
        });
      }));

      adsConfigDetails = adsConfigDetails.concat(model.config.midRolls.map(c => {
        const ads = adsObjs.find(a => a._id == c.advertisingId);
        return new AdsConfigurationDetail({
          AdsConfigurationId: adsConfigurationId,
          AdsPartnerId: ads.AdsPartnerId,
          AdvertisingId: ads._id,
          AdsFormat: ads.AdsFormat,
          Content: ads.Content,
          // ClickThrough: c.clickThrough,
          // ClickTracking: c.clickTracking,
          Config: {
            Type: AdsInVideoConfigType.MID,
            Overlay: c.overlay,
            TimeOffset: c.timeOffset,
            Duration: c.duration,
            SkipOffset: c.skipOffset,
          }
        });
      }));

      adsConfigDetails = adsConfigDetails.concat(model.config.postRolls.map(c => {
        const ads = adsObjs.find(a => a._id == c.advertisingId);
        return new AdsConfigurationDetail({
          AdsConfigurationId: adsConfigurationId,
          AdsPartnerId: ads.AdsPartnerId,
          AdvertisingId: ads._id,
          AdsFormat: ads.AdsFormat,
          Content: ads.Content,
          // ClickThrough: c.clickThrough,
          // ClickTracking: c.clickTracking,
          Config: {
            Type: AdsInVideoConfigType.POST,
            Duration: c.duration,
            SkipOffset: c.skipOffset,
          }
        });
      }));

      await AdsConfigurationDetail.insertMany(adsConfigDetails, opts);
    }

    await session.commitTransaction();
    session.endSession();
    return true;
  } catch (err) {
    await session.abortTransaction();
    session.endSession();
    if (!err.errorType) {
      throw ApiError.error(Messages.adsConfiguration.UPDATE_ADS_CONFIGURATION_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const deleteAdsConfiguration = async function(model) {
  const adsConfigurationId = model.adsConfigurationId;

  // check exist ads configuration
  const adsConfigurationCounts = await AdsConfiguration.countDocuments({ _id: adsConfigurationId, IsDeleted: false });
  if (adsConfigurationCounts <= 0) {
    throw ApiError.bussinessError(Messages.adsConfiguration.ADS_CONFIGURATION_DOES_NOT_EXIST);
  }

  // check exist livestream use ads configuration
  const livestreamCounts = await SoccerMatchStream.countDocuments({ AdsConfigurationId: adsConfigurationId, IsDeleted: false });
  if (livestreamCounts > 0) {
    throw ApiError.bussinessError(Messages.adsConfiguration.CANNOT_DELETE_ADS_CONFIGURATION_CAUSE_USED_IN_LIVESTREAM);
  }

  const session = await mongoose.startSession();
  session.startTransaction();

  try {
    const opts = { session };

    // Delete ads configuration
    await AdsConfiguration.updateOne(
      { _id: adsConfigurationId, IsDeleted: false },
      { $set: {
        IsDeleted: true,
        EditAt: Date.now()
      } },
      opts
    );

    // Delete ads configuration details
    await AdsConfigurationDetail.updateMany(
      { AdsConfigurationId: adsConfigurationId },
      { $set: {
        IsDeleted: true,
        EditAt: Date.now()
      } },
      opts
    );

    await session.commitTransaction();
    session.endSession();
    return true;
  } catch (err) {
    await session.abortTransaction();
    session.endSession();
    if (!err.errorType) {
      throw ApiError.error(Messages.adsConfiguration.DELETE_ADS_CONFIGURATION_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

const activateAdsConfiguration = async function(model) {
  const adsConfigurationId = model.adsConfigurationId;

  // check exist ads configuration
  const adsConfiguration = await AdsConfiguration.findOne({ _id: adsConfigurationId, IsDeleted: false });
  if (!adsConfiguration) {
    throw ApiError.bussinessError(Messages.adsConfiguration.ADS_CONFIGURATION_DOES_NOT_EXIST);
  }

  if (adsConfiguration.IsActive) {
    throw ApiError.bussinessError(Messages.adsConfiguration.ADS_CONFIGURATION_ALREADY_ACTIVE);
  }

  adsConfiguration.IsActive = true;
  adsConfiguration.EditAt = Date.now();
  await adsConfiguration.save();

  return true;
};

const deactivateAdsConfiguration = async function(model) {
  const adsConfigurationId = model.adsConfigurationId;

  const session = await mongoose.startSession();
  session.startTransaction();

  try {
    const opts = { session };

    // check exist ads configuration
    const adsConfiguration = await AdsConfiguration.findOne({ _id: adsConfigurationId, IsDeleted: false }, {}, opts);
    if (!adsConfiguration) {
      throw ApiError.bussinessError(Messages.adsConfiguration.ADS_CONFIGURATION_DOES_NOT_EXIST);
    }

    if (!adsConfiguration.IsActive) {
      throw ApiError.bussinessError(Messages.adsConfiguration.ADS_CONFIGURATION_ALREADY_INACTIVE);
    }

    adsConfiguration.IsActive = false;
    adsConfiguration.EditAt = Date.now();
    await adsConfiguration.save();

    const livestreams = await SoccerMatchStream.find({ AdsConfigurationId: adsConfigurationId, IsDeleted: false }, {}, opts);
    if (livestreams && livestreams.length > 0) {
      await SoccerMatchStream.updateMany(
        { AdsConfigurationId: adsConfigurationId, IsDeleted: false },
        {
          $set: {
            EditAt: Date.now()
          },
          $unset: {
            AdsConfigurationId: 1
          }
        },
        opts
      );
    }

    await session.commitTransaction();
    session.endSession();
    return true;
  } catch (err) {
    await session.abortTransaction();
    session.endSession();
    if (!err.errorType) {
      throw ApiError.error(Messages.adsConfiguration.DEACTIVATE_ADS_CONFIGURATION_FAIL, null, err);
    } else {
      throw err;
    }
  }
};

export {
  searchAdsConfiguration,
  addAdsConfiguration,
  getEditInfo,
  updateAdsConfiguration,
  deleteAdsConfiguration,
  activateAdsConfiguration,
  deactivateAdsConfiguration,
};

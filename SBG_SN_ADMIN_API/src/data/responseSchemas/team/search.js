import { Joi } from '../../../utils/validate';
import Base from '../baseResponse';

export default Base.keys({
  data: Joi.array().items({
    id: 'id',
    name: 'name',
    teamCode: 'teamCode',
    logo: 'logo',
    isClub: false,
    content: 'content',
    description: 'description',
    teamInfo: {},
    total: 0
  }).label('data')
}).label('result');

import search from './search';
import getEditInfo from './getEditInfo';

export default {
  search,
  getEditInfo,
};

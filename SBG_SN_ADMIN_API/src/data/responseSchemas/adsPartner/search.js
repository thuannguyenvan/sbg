import { Joi } from '../../../utils/validate';
import Base from '../baseResponse';

export default Base.keys({
  data: Joi.array().items({
    id: Joi.string(),
    name: Joi.string(),
    logo: Joi.string(),
    website: Joi.string(),
    description: Joi.string(),
    isActive: Joi.boolean(),
    total: Joi.number(),
    actions: Joi.array().items(Joi.string()).label('actions')
  }).label('data')
}).label('result');

import Base from '../baseResponse';

export default Base.keys({
  data: [{
    id: 'id',
    soccerStageId: 'soccerStageId',
    soccerCompetitionId: 'soccerCompetitionId',
    title: 'title',
    standalone: false
  }]
}).label('result');

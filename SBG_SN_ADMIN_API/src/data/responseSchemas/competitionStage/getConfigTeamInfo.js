import { Joi } from '../../../utils/validate';
import Base from '../baseResponse';

export default Base.keys({
  data: Joi.object().keys({
    competitionTeams: Joi.array().items({
      id: 'string',
      name: 'string',
      soccerCompetitionId: 'string',
      soccerTeamId: 'string',
      teamCode: 'string',
      logo: 'string'
    }),
    competitionStageTeams: Joi.array().items({
      id: 'string',
      name: 'string',
      soccerCompetitionStageId: 'string',
      soccerCompetitionId: 'string',
      soccerCompetitionTeamId: 'string',
      soccerTeamId: 'string',
      teamCode: 'string',
      logo: 'string',
      group: 'string',
    })
  })
}).label('result');

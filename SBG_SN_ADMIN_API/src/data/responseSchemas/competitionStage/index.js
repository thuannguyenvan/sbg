import getConfigTeamInfo from './getConfigTeamInfo';
import getByCompetition from './getByCompetition';

export default {
  getConfigTeamInfo,
  getByCompetition
};

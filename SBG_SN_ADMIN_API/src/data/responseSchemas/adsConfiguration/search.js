import { Joi } from '../../../utils/validate';
import Base from '../baseResponse';

export default Base.keys({
  data: Joi.array().items(Joi.object({
    id: Joi.string().description('Ads configuration Id'),
    title: Joi.string().description('Ads configuration title'),
    description: Joi.string().description('Ads configuration description'),
    adsData: Joi.string().description('Ads data'),
    isActive: Joi.boolean().description('Active/Inactive ads configuration'),
    total: Joi.number().description('Total records'),
    actions: Joi.array().items(Joi.string().label('action')).description('Actions')
  }))
}).label('result');

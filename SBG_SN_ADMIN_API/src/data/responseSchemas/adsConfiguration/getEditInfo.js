import { Joi } from '../../../utils/validate';
import Base from '../baseResponse';

export default Base.keys({
  data: Joi.object().keys({
    title: 'title',
    description: 'description',
    adsData: 'adsData',
    config: Joi.object().keys({
      preRolls: Joi.array().items(Joi.object().keys({
        advertisingId: 'advertisingId',
        advertisingTitle: 'advertisingTitle',
        adsPartnerName: 'adsPartnerName',
        duration: 'duration',
        skipOffset: 'skipOffset',
        clickThrough: 'clickThrough',
        clickTracking: 'clickTracking'
      })).label('preRolls'),
      midRolls: Joi.array().items(Joi.object().keys({
        advertisingId: 'advertisingId',
        advertisingTitle: 'advertisingTitle',
        adsPartnerName: 'adsPartnerName',
        timeOffset: 'timeOffset',
        duration: 'duration',
        skipOffset: 'skipOffset',
        overlay: 'overlay',
        clickThrough: 'clickThrough',
        clickTracking: 'clickTracking'
      })).label('midRolls'),
      postRolls: Joi.array().items(Joi.object().keys({
        advertisingId: 'advertisingId',
        advertisingTitle: 'advertisingTitle',
        adsPartnerName: 'adsPartnerName',
        duration: 'duration',
        skipOffset: 'skipOffset',
        clickThrough: 'clickThrough',
        clickTracking: 'clickTracking'
      })).label('postRolls')
    }).label('config')
  })
}).label('result');

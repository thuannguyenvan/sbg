import userInfo from './userInfo';
import login from './login';

export default {
  userInfo,
  login
};

import { Joi } from '../../../utils/validate';
import Base from '../baseResponse';

export default Base.keys({
  data: Joi.object().keys({
    id: 'user id',
    userName: 'userName',
    fullName: 'fullName',
    email: 'email',
    avatar: 'avatar',
    roles: Joi.array().items('role id'),
    actions: Joi.array().items('action key'),
    menus: Joi.array().items({
      level: 1,
      roles: Joi.array().items('role id'),
      isActive: true,
      id: 'menu id',
      parentId: 'parent menu id',
      title: 'title',
      location: 'Left',
      icon: 'icon',
      url: 'url',
      order: 1,
      createAt: '2019-01-30T03:59:28.953Z'
    }),
    supportedLanguage: Joi.array().items({
      value: 'value',
      title: 'title'
    }),
    token: 'token'
  }).label('data')
}).label('result');

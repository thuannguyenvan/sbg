import { Joi } from '../../../utils/validate';
import Base from '../baseResponse';

export default Base.keys({
  data: Joi.object().keys({
    competitions: Joi.array().items({
      id: 'id',
      title: 'title',
      competitionCode: 'competitionCode',
    }),
    stages: Joi.array().items({
      id: Joi.string(),
      title: Joi.string(),
      key: Joi.string(),
      order: Joi.number(),
    })
  })
}).label('result');

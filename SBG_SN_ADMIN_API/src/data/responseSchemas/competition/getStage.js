import { Joi } from '../../../utils/validate';
import Base from '../baseResponse';

export default Base.keys({
  data: Joi.array().items(Joi.object().keys({
    id: 'id',
    soccerStageId: 'soccerStageId',
    soccerCompetitionId: 'soccerCompetitionId',
    title: 'title',
    standalone: Joi.boolean(),
    groupInfos: Joi.array().items(Joi.string()),
    actions: Joi.array().items(Joi.string())
  }))
}).label('result');

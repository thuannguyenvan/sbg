import { Joi } from '../../../utils/validate';
import Base from '../baseResponse';

export default Base.keys({
  data: Joi.string().description('Competition Id')
}).label('result');

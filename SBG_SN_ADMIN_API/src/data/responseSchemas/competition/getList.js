import { Joi } from '../../../utils/validate';
import Base from '../baseResponse';

export default Base.keys({
  data: Joi.array().items({
    'id': 'id',
    'title': 'title',
    'season': 'season',
    'logo': 'logo',
    'startAt': 'startAt',
    'endAt': 'endAt',
    'place': 'place'
  }).label('data')
}).label('result');

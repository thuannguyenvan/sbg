import { Joi } from '../../../utils/validate';
import Base from '../baseResponse';

export default Base.keys({
  data: Joi.array().items({
    id: Joi.string(),
    title: Joi.string(),
    startAt: Joi.date(),
    endAt: Joi.date(),
    logo: Joi.string(),
    season: Joi.string(),
    place: Joi.string(),
    total: Joi.number(),
    actions: Joi.array().items(Joi.string()).label('actions')
  }).label('data')
}).label('result');

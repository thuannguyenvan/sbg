import { Joi } from '../../../utils/validate';
import Base from '../baseResponse';

export default Base.keys({
  data: Joi.object().keys({
    soccerCompetitionId: 'soccerCompetitionId',
    soccerCompetitionTitle: 'soccerCompetitionTitle',
    soccerCompetitionTeams: [{
      id: 'id',
      soccerCompetitionId: 'soccerCompetitionId',
      soccerTeamId: 'soccerTeamId',
      name: 'name',
      teamCode: 'teamCode',
      logo: 'logo',
      teamInfo: 'teamInfo'
    }]
  })
}).label('result');

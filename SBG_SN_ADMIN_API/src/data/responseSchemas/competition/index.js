import search from './search';
import add from './add';
import getAddInfo from './getAddInfo';
import getStage from './getStage';
import getConfigTeamInfo from './getConfigTeamInfo';
import getList from './getList';

export default {
  search,
  add,
  getAddInfo,
  getStage,
  getConfigTeamInfo,
  getList
};

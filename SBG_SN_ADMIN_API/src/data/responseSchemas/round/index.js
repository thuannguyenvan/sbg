import search from './search';
import getEditInfo from './getEditInfo';
import getByCompetition from './getByCompetition';
import getTeams from './getTeams';
import getMatchs from './getMatchs';

export default {
  search,
  getEditInfo,
  getByCompetition,
  getTeams,
  getMatchs
};

import { Joi } from '../../../utils/validate';
import Base from '../baseResponse';

export default Base.keys({
  data: Joi.object().keys({
    id: 'id',
    title: 'title',
    soccerCompetitionId: 'soccerCompetitionId',
    soccerCompetitionName: 'soccerCompetitionName',
    soccerCompetitionStageId: 'soccerCompetitionStageId',
    soccerCompetitionStageName: 'soccerCompetitionStageName',
    startAt: 'startAt',
    endAt: 'endAt',
    description: 'description'
  })
}).label('result');

import { Joi } from '../../../utils/validate';
import Base from '../baseResponse';

export default Base.keys({
  data: Joi.array().items({
    soccerCompetitionStageTeamId: 'soccerCompetitionStageTeamId',
    soccerCompetitionTeamId: 'soccerCompetitionTeamId',
    soccerTeamId: 'soccerTeamId',
    soccerCompetitionId: 'soccerCompetitionId',
    soccerCompetitionStageId: 'soccerCompetitionStageId',
    soccerRoundId: 'soccerRoundId',
    group: 'A',
    name: 'name',
    teamCode: 'teamCode',
    logo: 'logo'
  }).label('data')
}).label('result');

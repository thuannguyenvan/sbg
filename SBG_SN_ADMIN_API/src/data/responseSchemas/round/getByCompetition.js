import { Joi } from '../../../utils/validate';
import Base from '../baseResponse';

export default Base.keys({
  data: Joi.array().items({
    id: 'id',
    title: 'title',
    soccerCompetitionId: 'soccerCompetitionId',
    soccerCompetitionName: 'soccerCompetitionName',
    soccerCompetitionStageId: 'soccerCompetitionStageId',
    soccerCompetitionStageName: 'soccerCompetitionStageName',
    startAt: 'startAt',
    endAt: 'endAt',
    description: 'description'
  }).label('data')
}).label('result');

import { Joi } from '../../../utils/validate';
import Base from '../baseResponse';

export default Base.keys({
  data: Joi.array().items({
    soccerCompetitionId: 'soccerCompetitionId',
    soccerCompetitionTitle: 'soccerCompetitionTitle',
    soccerRoundId: 'soccerRoundId',
    soccerRoundTitle: 'soccerRoundTitle',
    soccerMatchId: 'soccerMatchId',
    title: 'title',
    startAt: 'startAt',
    status: 'status',
    statusTitle: 'statusTitle',
    teamA: {
      soccerCompetitionStageTeamId: 'soccerCompetitionStageTeamId',
      soccerCompetitionTeamId: 'soccerCompetitionTeamId',
      soccerTeamId: 'soccerTeamId',
      name: 'name',
      teamCode: 'teamCode',
      logo: 'logo'
    },
    teamB: {
      soccerCompetitionStageTeamId: 'soccerCompetitionStageTeamId',
      soccerCompetitionTeamId: 'soccerCompetitionTeamId',
      soccerTeamId: 'soccerTeamId',
      name: 'name',
      teamCode: 'teamCode',
      logo: 'logo'
    },
    winner: 'winner',
    score: {
      fullTime: {
        teamA: 0,
        teamB: 0
      },
      halfTime: {
        teamA: 0,
        teamB: 0
      },
      extraTime: {
        teamA: 0,
        teamB: 0
      },
      penalty: {
        teamA: 0,
        teamB: 0
      },
      aggregate: {
        teamA: 0,
        teamB: 0
      }
    }
  }).label('data')
}).label('result');

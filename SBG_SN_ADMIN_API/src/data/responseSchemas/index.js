import user from './user';
import auth from './auth';
import team from './team';
import competition from './competition';
import competitionStage from './competitionStage';
import round from './round';
import match from './match';
import matchStream from './matchStream';
import adsPartner from './adsPartner';
import advertising from './advertising';
import adsConfiguration from './adsConfiguration';

export default {
  user,
  auth,
  team,
  competition,
  competitionStage,
  round,
  match,
  matchStream,
  adsPartner,
  advertising,
  adsConfiguration,
};

import search from './search';
import getRoles from './getRoles';

export default {
  search,
  getRoles
};

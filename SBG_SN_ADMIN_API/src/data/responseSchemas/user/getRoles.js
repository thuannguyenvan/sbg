import { Joi } from '../../../utils/validate';
import Base from '../baseResponse';

export default Base.keys({
  data: Joi.array().items({
    id: 'id',
    name: 'role name',
    checked: true
  }).label('data')
}).label('result');

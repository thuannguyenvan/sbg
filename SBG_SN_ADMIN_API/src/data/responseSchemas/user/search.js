import { Joi } from '../../../utils/validate';
import Base from '../baseResponse';

export default Base.keys({
  data: Joi.array().items({
    id: Joi.string(),
    userName: Joi.string(),
    fullName: Joi.string(),
    email: Joi.string(),
    roles: Joi.array().items({
      id: Joi.string(),
      name: Joi.string(),
      description: Joi.string()
    }).label('roles'),
    total: 100,
    actions: Joi.array().items(Joi.string()).label('actions')
  }).label('data')
}).label('result');

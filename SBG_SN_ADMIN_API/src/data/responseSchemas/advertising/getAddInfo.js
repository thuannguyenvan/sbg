import { Joi } from '../../../utils/validate';
import Base from '../baseResponse';

export default Base.keys({
  data: Joi.object().keys({
    adsPartners: Joi.array().items(Joi.object({
      id: Joi.string(),
      name: Joi.string(),
      logo: Joi.string(),
      website: Joi.string(),
      description: Joi.string(),
      isActive: Joi.boolean()
    })),
    advertisingFormats: Joi.array().items({
      value: 'value',
      title: 'title'
    })
  })
}).label('result');

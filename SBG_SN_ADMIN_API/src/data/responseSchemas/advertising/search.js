import { Joi } from '../../../utils/validate';
import Base from '../baseResponse';

export default Base.keys({
  data: Joi.array().items(Joi.object({
    id: Joi.string().description('Advertising Id'),
    adsPartnerId: Joi.string().description('Advertising Id'),
    adsPartnerName: Joi.string().description('Ads partner name'),
    adsPartnerWebsite: Joi.string().description('Ads partner Website'),
    title: Joi.string().description('Advertising title'),
    content: Joi.string().description('Advertising content'),
    clickThrough: Joi.string().description('Link destination when clicking ads'),
    clickTracking: Joi.string().description('Link tracking advertising'),
    description: Joi.string().description('Advertising description'),
    adsFormat: Joi.string().description('Ads format'),
    adsFormatTitle: Joi.string().description('Ads format title'),
    isActive: Joi.boolean().description('Active/Inactive advertising'),
    total: Joi.number().description('Total records'),
    actions: Joi.array().items(Joi.string().label('action')).description('Actions')
  }))
}).label('result');

import { Joi } from '../../../utils/validate';
import Base from '../baseResponse';

export default Base.keys({
  data: Joi.object().keys({
    adsPartners: Joi.array().items({
      id: 'id',
      name: 'name',
      logo: 'logo',
      website: 'website',
      description: 'description',
      isActive: Joi.boolean()
    })
  })
}).label('result');

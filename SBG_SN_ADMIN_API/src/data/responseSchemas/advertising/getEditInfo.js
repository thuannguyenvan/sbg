import { Joi } from '../../../utils/validate';
import Base from '../baseResponse';

export default Base.keys({
  data: Joi.object().keys({
    advertising: Joi.object().keys({
      id: Joi.string().description('Advertising Id'),
      adsPartnerId: Joi.string().description('Advertising Id'),
      title: Joi.string().description('Advertising title'),
      adsFormat: Joi.string().description('Ads format'),
      adsFormatTitle: Joi.string().description('Ads format title'),
      content: Joi.string().description('Advertising content'),
      clickThrough: Joi.string().description('Link destination when clicking ads'),
      clickTracking: Joi.string().description('Link tracking advertising'),
      description: Joi.string().description('Advertising description')
    }),
    adsPartners: Joi.array().items({
      id: 'id',
      name: 'name',
      logo: 'logo',
      website: 'website',
      description: 'description',
      isActive: Joi.boolean()
    }),
    advertisingFormats: Joi.array().items({
      value: 'value',
      title: 'title'
    })
  })
}).label('result');

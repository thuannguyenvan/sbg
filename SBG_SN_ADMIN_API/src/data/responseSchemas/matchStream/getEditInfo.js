import { Joi } from '../../../utils/validate';
import Base from '../baseResponse';

export default Base.keys({
  data: Joi.object().keys({
    soccerMatchStream: {
      soccerCompetitionId: 'soccerCompetitionId',
      soccerCompetitionTitle: 'soccerCompetitionTitle',
      soccerRoundId: 'soccerRoundId',
      soccerRoundTitle: 'soccerRoundTitle',
      soccerMatchId: 'soccerMatchId',
      soccerMatchTitle: 'soccerMatchTitle',
      id: 'id',
      title: 'title',
      startAt: 'startAt',
      status: 'status',
      statusTitle: 'statusTitle',
      adsConfigurationId: 'adsConfigurationId',
      adsConfigurationTitle: 'adsConfigurationTitle',
      chatUrl: 'chatUrl',
      urlSlug: 'urlSlug',
      keywords: 'keywords',
      tags: Joi.array().items('tag'),
      livestreamUrls: Joi.array().items({
        label: 'label',
        quanlity: 'quanlity',
        link: 'link'
      })
    },
    soccerCompetitions: Joi.array().items({
      'id': 'id',
      'title': 'title',
      'season': 'season',
      'logo': 'logo',
      'startAt': 'startAt',
      'endAt': 'endAt',
      'place': 'place'
    }),
    adsConfigurations: Joi.array().items({
      id: 'id',
      title: 'title',
      description: 'description',
      adsData: 'adsData',
      isActive: 'isActive'
    }),
    soccerRounds: Joi.array().items({
      soccerCompetitionStageTeamId: 'soccerCompetitionStageTeamId',
      soccerCompetitionTeamId: 'soccerCompetitionTeamId',
      soccerTeamId: 'soccerTeamId',
      soccerCompetitionId: 'soccerCompetitionId',
      soccerCompetitionStageId: 'soccerCompetitionStageId',
      soccerRoundId: 'soccerRoundId',
      group: 'A',
      name: 'name',
      teamCode: 'teamCode',
      logo: 'logo'
    }),
    soccerMatchs: Joi.array().items({
      soccerCompetitionId: 'soccerCompetitionId',
      soccerCompetitionTitle: 'soccerCompetitionTitle',
      soccerRoundId: 'soccerRoundId',
      soccerRoundTitle: 'soccerRoundTitle',
      soccerMatchId: 'soccerMatchId',
      title: 'title',
      startAt: 'startAt',
      status: 'status',
      statusTitle: 'statusTitle',
      teamA: {
        soccerCompetitionStageTeamId: 'soccerCompetitionStageTeamId',
        soccerCompetitionTeamId: 'soccerCompetitionTeamId',
        soccerTeamId: 'soccerTeamId',
        name: 'name',
        teamCode: 'teamCode',
        logo: 'logo'
      },
      teamB: {
        soccerCompetitionStageTeamId: 'soccerCompetitionStageTeamId',
        soccerCompetitionTeamId: 'soccerCompetitionTeamId',
        soccerTeamId: 'soccerTeamId',
        name: 'name',
        teamCode: 'teamCode',
        logo: 'logo'
      },
      winner: 'winner',
      score: {
        fullTime: {
          teamA: 0,
          teamB: 0
        },
        halfTime: {
          teamA: 0,
          teamB: 0
        },
        extraTime: {
          teamA: 0,
          teamB: 0
        },
        penalty: {
          teamA: 0,
          teamB: 0
        },
        aggregate: {
          teamA: 0,
          teamB: 0
        }
      }
    }),
    livestreamStatuses: Joi.array().items({
      value: 'value',
      title: 'title'
    }),
    livestreamVideoUnits: Joi.array().items({
      value: 'value',
      title: 'title'
    })
  })
}).label('result');

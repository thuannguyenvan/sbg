import { Joi } from '../../../utils/validate';
import Base from '../baseResponse';

export default Base.keys({
  data: Joi.array().items({
    soccerCompetitionId: 'soccerCompetitionId',
    soccerCompetitionTitle: 'soccerCompetitionTitle',
    soccerRoundId: 'soccerRoundId',
    soccerRoundTitle: 'soccerRoundTitle',
    soccerMatchId: 'soccerMatchId',
    soccerMatchTitle: 'soccerMatchTitle',
    id: 'id',
    title: 'title',
    startAt: 'startAt',
    status: 'status',
    statusTitle: 'statusTitle',
    chatUrl: 'chatUrl',
    urlSlug: 'urlSlug',
    keywords: 'keywords',
    tags: Joi.array().items('tag'),
    livestreamUrls: Joi.array().items({
      label: 'label',
      quanlity: 'quanlity',
      link: 'link'
    }),
    total: 20,
    actions: Joi.array().items('ACTION')
  })
}).label('result');

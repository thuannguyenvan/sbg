import { Joi } from '../../../utils/validate';
import Base from '../baseResponse';

export default Base.keys({
  data: Joi.object().keys({
    soccerCompetitions: Joi.array().items({
      'id': 'id',
      'title': 'title',
      'season': 'season',
      'logo': 'logo',
      'startAt': 'startAt',
      'endAt': 'endAt',
      'place': 'place'
    }),
    soccerMatchStatuses: Joi.array().items({
      value: 'value',
      title: 'title'
    }),
    winnerTeamTypes: Joi.array().items({
      value: 'value',
      title: 'title'
    })
  })
}).label('result');

import { Joi } from '../../../utils/validate';
import Base from '../baseResponse';

export default Base.keys({
  data: Joi.object().keys({
    soccerMatch: {
      soccerCompetitionId: 'soccerCompetitionId',
      soccerCompetitionTitle: 'soccerCompetitionTitle',
      soccerCompetitionStageId: 'soccerCompetitionStageId',
      soccerCompetitionStageTitle: 'soccerCompetitionStageTitle',
      soccerRoundId: 'soccerRoundId',
      soccerRoundTitle: 'soccerRoundTitle',
      id: 'id',
      title: 'title',
      startAt: 'startAt',
      status: 'status',
      teamA: {
        soccerCompetitionStageTeamId: 'soccerCompetitionStageTeamId',
        soccerCompetitionTeamId: 'soccerCompetitionTeamId',
        soccerTeamId: 'soccerTeamId',
        name: 'name',
        teamCode: 'teamCode',
        logo: 'logo'
      },
      teamB: {
        soccerCompetitionStageTeamId: 'soccerCompetitionStageTeamId',
        soccerCompetitionTeamId: 'soccerCompetitionTeamId',
        soccerTeamId: 'soccerTeamId',
        name: 'name',
        teamCode: 'teamCode',
        logo: 'logo'
      },
      winner: 'winner',
      score: {
        fullTime: {
          teamA: 0,
          teamB: 0
        },
        halfTime: {
          teamA: 0,
          teamB: 0
        },
        extraTime: {
          teamA: 0,
          teamB: 0
        },
        penalty: {
          teamA: 0,
          teamB: 0
        },
        aggregate: {
          teamA: 0,
          teamB: 0
        }
      }
    },
    soccerCompetitionStageTeams: Joi.array().items({
      soccerCompetitionStageTeamId: 'soccerCompetitionStageTeamId',
      soccerCompetitionTeamId: 'soccerCompetitionTeamId',
      soccerTeamId: 'soccerTeamId',
      soccerCompetitionId: 'soccerCompetitionId',
      soccerCompetitionStageId: 'soccerCompetitionStageId',
      group: 'A',
      name: 'name',
      teamCode: 'teamCode',
      logo: 'logo'
    }),
    soccerCompetitions: Joi.array().items({
      'id': 'id',
      'title': 'title',
      'season': 'season',
      'logo': 'logo',
      'startAt': 'startAt',
      'endAt': 'endAt',
      'place': 'place'
    }),
    soccerMatchStatuses: Joi.array().items({
      value: 'value',
      title: 'title'
    }),
    winnerTeamTypes: Joi.array().items({
      value: 'value',
      title: 'title'
    })
  })
}).label('result');

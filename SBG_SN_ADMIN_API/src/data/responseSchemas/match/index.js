import getSearchInfo from './getSearchInfo';
import search from './search';
import getAddInfo from './getAddInfo';
import getEditInfo from './getEditInfo';

export default {
  getSearchInfo,
  search,
  getAddInfo,
  getEditInfo
};

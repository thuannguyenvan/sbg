import { Joi } from '../../utils/validate';

export default Joi.object().keys({
  success: Joi.boolean(),
  data: Joi.object(),
  message: Joi.string(),
  messages: Joi.array().items(Joi.string()),
  errors: Joi.array().items(Joi.object()),
  errorType: Joi.number()
});

import { Joi } from '../../../utils/validate';
import {
  LanguageKeys,
} from '../../constants';

const labels = LanguageKeys.validate.labels;

export default Joi.object({
  token: Joi.string().label(labels.LOGIN_TOKEN).required().description('Token')
});

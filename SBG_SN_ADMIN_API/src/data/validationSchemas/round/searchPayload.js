import { Joi, MessageTemplate } from '../../../utils/validate';
import BasePagination from '../basePagination';
import { LanguageKeys, Patterns } from '../../constants';

const messageTemplates = LanguageKeys.validate.messageTemplates;
const labels = LanguageKeys.validate.labels;

export default BasePagination.keys({
  soccerCompetitionId: Joi.string().regex(Patterns.MONGODB_ID).required().label(labels.ROUND_SOCCER_COMPETITION_ID)
                            .description('soccerCompetitionId')
                            .options({language:{
                              string: {
                                regex: {
                                  base: MessageTemplate(messageTemplates.INVALID)
                                }
                              }
                            }}),
  soccerCompetitionStageId: Joi.string().regex(Patterns.MONGODB_ID).label(labels.ROUND_SOCCER_COMPETITION_STAGE_ID)
                              .description('soccerCompetitionStageId')
                              .options({language:{
                                string: {
                                  regex: {
                                    base: MessageTemplate(messageTemplates.INVALID)
                                  }
                                }
                              }}),
  title: Joi.string().trim().max(255).allow('').label(labels.ROUND_TITLE).description('Round title')
}).label('payload');

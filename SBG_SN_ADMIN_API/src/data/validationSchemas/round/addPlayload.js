import {
  Joi,
  MessageTemplate
} from '../../../utils/validate';
import { LanguageKeys, Formats, Patterns } from '../../constants';

const messageTemplates = LanguageKeys.validate.messageTemplates;
const labels = LanguageKeys.validate.labels;

export default Joi.object().keys({
  soccerCompetitionStageId: Joi.string()
                              .required()
                              .regex(Patterns.MONGODB_ID)
                              .label(labels.ROUND_SOCCER_COMPETITION_STAGE_ID)
                              .description('soccerCompetitionStageId')
                              .options({language:{
                                string: {
                                  regex: {
                                    base: MessageTemplate(messageTemplates.INVALID)
                                  }
                                }
                              }}),
  title: Joi.string().trim().max(255).required().label(labels.ROUND_TITLE).description('Round title'),
  startAt: Joi.date().format(Formats.DATE).label(labels.ROUND_START_DATE)
  .when('endAt', { is: Joi.date().format(Formats.DATE).exist(), then: Joi.date().max(Joi.ref('endAt')) })
  .options({language: {
    date: {
      max: MessageTemplate(messageTemplates.DATE_LESS_OR_EQUAL, labels.ROUND_END_DATE)
    }
  }}),
  endAt: Joi.date().format(Formats.DATE).label(labels.ROUND_END_DATE),
  description: Joi.string().trim().allow('').max(1000).label(labels.DESCRIPTION).description('description')
}).label('payload');

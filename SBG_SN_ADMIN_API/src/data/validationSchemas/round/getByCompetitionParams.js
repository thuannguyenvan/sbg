import { Joi } from '../../../utils/validate';
import { Patterns } from '../../constants';

export default Joi.object({
  'soccerCompetitionId': Joi.string().regex(Patterns.MONGODB_ID).required().description('soccerCompetitionId')
});

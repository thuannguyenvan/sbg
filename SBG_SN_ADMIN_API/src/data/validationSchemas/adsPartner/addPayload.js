import {
  Joi,
  // MessageTemplate
} from '../../../utils/validate';
import {
  LanguageKeys,
  // Patterns,
  // Formats
} from '../../constants';
// import {
//   SoccerMatchStatus,
//   WinnerTeamType
// } from '../../enums';

// const messageTemplates = LanguageKeys.validate.messageTemplates;
const labels = LanguageKeys.validate.labels;

export default Joi.object().keys({
  name: Joi.string().trim().required().max(255).label(labels.ADS_PARTNER_ADD_NAME).description('Ads partner name'),
  logo: Joi.string().allow('').trim().max(255).label(labels.ADS_PARTNER_ADD_LOGO).description('Logo'),
  website: Joi.string().allow('').trim().max(255).label(labels.ADS_PARTNER_ADD_WEBSITE).description('Website'),
  description: Joi.string().allow('').trim().max(1000).label(labels.ADS_PARTNER_ADD_DESCRIPTION).description('Description')
}).label('payload');

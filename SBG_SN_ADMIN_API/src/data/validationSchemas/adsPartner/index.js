import searchPayload from './searchPayload';
import addPayload from './addPayload';
import updatePayload from './updatePayload';

export default {
  searchPayload,
  addPayload,
  updatePayload,
};

import { Joi } from '../../../utils/validate';
import BasePagination from '../basePagination';
import { LanguageKeys } from '../../constants';

const labels = LanguageKeys.validate.labels;

export default BasePagination.keys({
  name: Joi.string().trim().max(255).allow('').label(labels.ADS_PARTNER_SEARCH_NAME).description('Ads partner name'),
  isActive: Joi.boolean().label(labels.ADS_PARTNER_SEARCH_ACTIVE_STATUS).description('Ads partner status')
}).label('payload');

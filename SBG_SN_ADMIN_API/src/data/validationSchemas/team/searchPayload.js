import { Joi } from '../../../utils/validate';
import BasePagination from '../basePagination';
import { LanguageKeys } from '../../constants';

const labels = LanguageKeys.validate.labels;

export default BasePagination.keys({
  name: Joi.string().trim().max(255).allow('').label(labels.SOCCER_TEAM_NAME).description('Team name')
}).label('payload');

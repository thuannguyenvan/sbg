import { Joi } from '../../../utils/validate';
import BasePagination from '../basePagination';
import { LanguageKeys } from '../../constants';

const labels = LanguageKeys.validate.labels;

export default BasePagination.keys({
  title: Joi.string().trim().max(255).allow('').label(labels.ADS_CONFIGURATION_SEARCH_TITLE).description('Advertising configuration title'),
  isActive: Joi.boolean().label(labels.ADS_CONFIGURATION_SEARCH_ACTIVE_STATUS).description('Advertising configuration active status')
}).label('payload');

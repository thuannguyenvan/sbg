import {
  Joi,
  MessageTemplate,
} from '../../../utils/validate';
import {
  LanguageKeys,
  Patterns,
} from '../../constants';

const messageTemplates = LanguageKeys.validate.messageTemplates;
const labels = LanguageKeys.validate.labels;

export default Joi.object().keys({
  title: Joi.string().trim().required().max(255).label(labels.ADS_CONFIGURATION_ADD_TITLE).description('Ads configuration title'),
  description: Joi.string().allow('').trim().max(1000).label(labels.ADS_CONFIGURATION_ADD_DESCRIPTION).description('Description'),
  config: Joi.object().keys({
    preRolls: Joi.array().items(Joi.object().keys({
      advertisingId: Joi.string().required()
                      .regex(Patterns.MONGODB_ID)
                      .label(labels.ADS_CONFIGURATION_ADD_CONFIG_ROLL_ADVERTISING_ID)
                      .description('advertisingId'),
      duration: Joi.number().integer().required().label(labels.ADS_CONFIGURATION_ADD_CONFIG_ROLL_DURATION),
      skipOffset: Joi.number().integer().max(Joi.ref('duration')).options({language: {
        number: {
          max: MessageTemplate(messageTemplates.ADS_CONFIGURATION_LESS_OR_EQUAL, labels.ADS_CONFIGURATION_ADD_CONFIG_ROLL_DURATION)
        }
      }}).label(labels.ADS_CONFIGURATION_ADD_CONFIG_ROLL_SKIPOFFSET),
      clickThrough: Joi.string().trim().allow('').label(labels.ADS_CONFIGURATION_ADD_CONFIG_ROLL_CLICK_THROUGH),
      clickTracking: Joi.string().trim().allow('').label(labels.ADS_CONFIGURATION_ADD_CONFIG_ROLL_CLICK_TRACKING)
    })).label(labels.ADS_CONFIGURATION_ADD_CONFIG_PREROLL),
    midRolls: Joi.array().items(Joi.object().keys({
      advertisingId: Joi.string().required()
                      .regex(Patterns.MONGODB_ID)
                      .label(labels.ADS_CONFIGURATION_ADD_CONFIG_ROLL_ADVERTISING_ID)
                      .description('advertisingId'),
      timeOffset: Joi.number().integer().required().label(labels.ADS_CONFIGURATION_ADD_CONFIG_ROLL_TIMEOFFSET),
      duration: Joi.number().integer().greater(0).required().label(labels.ADS_CONFIGURATION_ADD_CONFIG_ROLL_DURATION),
      skipOffset: Joi.number().integer().max(Joi.ref('duration')).options({language: {
        number: {
          max: MessageTemplate(messageTemplates.ADS_CONFIGURATION_LESS_OR_EQUAL, labels.ADS_CONFIGURATION_ADD_CONFIG_ROLL_DURATION)
        }
      }}).label(labels.ADS_CONFIGURATION_ADD_CONFIG_ROLL_SKIPOFFSET),
      overlay: Joi.boolean().required().label(labels.ADS_CONFIGURATION_ADD_CONFIG_ROLL_OVERLAY),
      clickThrough: Joi.string().trim().allow('').label(labels.ADS_CONFIGURATION_ADD_CONFIG_ROLL_CLICK_THROUGH),
      clickTracking: Joi.string().trim().allow('').label(labels.ADS_CONFIGURATION_ADD_CONFIG_ROLL_CLICK_TRACKING)
    })).unique((a, b) => a.overlay === true && b.overlay === true && ((a.timeOffset + a.duration > b.timeOffset && a.timeOffset + a.duration < b.timeOffset + b.duration) ||
                          (a.timeOffset > b.timeOffset && a.timeOffset < b.timeOffset + b.duration) ||
                          a.timeOffset == b.timeOffset || a.timeOffset + a.duration == b.timeOffset + b.duration))
        .options({language: {
          array: {
            unique: MessageTemplate(messageTemplates.ADS_CONFIGURATION_MIDROLL_HAVE_CONFLICT_TIME)
          }
        }}).label(labels.ADS_CONFIGURATION_ADD_CONFIG_MIDROLL),
    postRolls: Joi.array().items(Joi.object().keys({
      advertisingId: Joi.string().required()
                      .regex(Patterns.MONGODB_ID)
                      .label(labels.ADS_CONFIGURATION_ADD_CONFIG_ROLL_ADVERTISING_ID)
                      .description('advertisingId'),
      duration: Joi.number().integer().required().label(labels.ADS_CONFIGURATION_ADD_CONFIG_ROLL_DURATION),
      skipOffset: Joi.number().integer().max(Joi.ref('duration')).options({language: {
        number: {
          max: MessageTemplate(messageTemplates.ADS_CONFIGURATION_LESS_OR_EQUAL, labels.ADS_CONFIGURATION_ADD_CONFIG_ROLL_DURATION)
        }
      }}).label(labels.ADS_CONFIGURATION_ADD_CONFIG_ROLL_SKIPOFFSET),
      clickThrough: Joi.string().trim().allow('').label(labels.ADS_CONFIGURATION_ADD_CONFIG_ROLL_CLICK_THROUGH),
      clickTracking: Joi.string().trim().allow('').label(labels.ADS_CONFIGURATION_ADD_CONFIG_ROLL_CLICK_TRACKING)
    })).label(labels.ADS_CONFIGURATION_ADD_CONFIG_POSTROLL)
  }).label(labels.ADS_CONFIGURATION_ADD_CONFIG).description('Advertising configuration')
}).label('payload');

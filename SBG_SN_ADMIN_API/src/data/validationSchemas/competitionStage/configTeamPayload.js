import { Joi, MessageTemplate } from '../../../utils/validate';
import { LanguageKeys, Patterns } from '../../constants';

const messageTemplates = LanguageKeys.validate.messageTemplates;
const labels = LanguageKeys.validate.labels;

export default Joi.object().keys({
  soccerCompetitionStageTeams: Joi.array().items(Joi.object().keys({
    soccerCompetitionTeamId: Joi.string().regex(Patterns.MONGODB_ID).required()
                                .label(labels.COMPETITION_STAGE_TEAM_ID)
                                .description('soccerCompetitionTeamId'),
    group: Joi.string().regex(Patterns.SOCCER_GROUP)
              .label(labels.COMPETITION_STAGE_GROUP)
              .description('group')
              .options({language:{
                string: {
                  regex: {
                    base: MessageTemplate(messageTemplates.COMPETITION_STAGE_GROUP_TEAM)
                  }
                }
              }})
  })).unique('soccerCompetitionTeamId')
}).label('payload');

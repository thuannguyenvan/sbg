import { Joi } from '../../../utils/validate';
import { LanguageKeys, Patterns } from '../../constants';

const labels = LanguageKeys.validate.labels;

export default Joi.object().keys({
  isShowInHomepage: Joi.boolean().required(),
  tables: Joi.array().items(Joi.object().keys({
    id: Joi.string().regex(Patterns.MONGODB_ID).required().label(labels.STANDING_TABLE_ID).description('Position').description('id'),
    position: Joi.number().integer().less(100000).label(labels.STANDING_TABLE_POSITION).description('Position'),
    played: Joi.number().integer().less(100000).label(labels.STANDING_TABLE_PLAYED).description('Played'),
    won: Joi.number().integer().less(100000).label(labels.STANDING_TABLE_WON).description('Won'),
    lost: Joi.number().integer().less(100000).label(labels.STANDING_TABLE_LOST).description('Lost'),
    drawn: Joi.number().integer().less(100000).label(labels.STANDING_TABLE_DRAWN).description('Drawn'),
    goalsFor: Joi.number().integer().less(100000).label(labels.STANDING_TABLE_GOALS_FOR).description('Goals for'),
    goalsAgainst: Joi.number().integer().less(100000).label(labels.STANDING_TABLE_GOALS_AGAINST).description('Goals against'),
    goalDifference: Joi.number().integer().less(100000).label(labels.STANDING_TABLE_GOAL_DIFFERENCE).description('Goal difference'),
    points: Joi.number().integer().less(100000).label(labels.STANDING_TABLE_GOAL_POINTS).description('points')
  })).unique('id')
}).label('payload');

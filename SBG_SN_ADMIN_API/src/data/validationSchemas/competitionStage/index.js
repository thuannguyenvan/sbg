import configTeamPayload from './configTeamPayload';
import updateStandingPayload from './updateStandingPayload';
import getByCompetitionParams from './getByCompetitionParams';

export default {
  configTeamPayload,
  updateStandingPayload,
  getByCompetitionParams
};

import {
  Joi,
  MessageTemplate
} from '../../../utils/validate';
import {
  LanguageKeys,
  Formats,
  Patterns
} from '../../constants';

const messageTemplates = LanguageKeys.validate.messageTemplates;
const labels = LanguageKeys.validate.labels;

export default Joi.object().keys({
  title: Joi.string().trim().max(255).required().label(labels.COMPETITION_TITLE).description('Title'),
  season: Joi.string().trim().max(255).required().label(labels.COMPETITION_SEASON).description('Season'),
  place: Joi.string().trim().allow('').max(255).label(labels.COMPETITION_PLACE).description('Place'),
  startAt: Joi.date().format(Formats.DATE).label(labels.COMPETITION_START_AT)
  .when('endAt', { is: Joi.date().format(Formats.DATE).exist(), then: Joi.date().max(Joi.ref('endAt')) })
  .options({language: {
    date: {
      max: MessageTemplate(messageTemplates.DATE_LESS_OR_EQUAL, labels.COMPETITION_END_AT)
    }
  }}).description('Start at'),
  endAt: Joi.date().format(Formats.DATE).label(labels.COMPETITION_END_AT).description('End at'),
  description: Joi.string().trim().allow('').max(1000).label(labels.COMPETITION_DESCRIPTION).description('Description'),
  stages: Joi.array().items(Joi.string().regex(Patterns.MONGODB_ID)).required().label(labels.COMPETITION_STAGE).description('Competition stage')
}).label('payload');

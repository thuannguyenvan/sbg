import searchPayload from './searchPayload';
import addPayload from './addPayload';
import updatePayload from './updatePayload';
import configTeamPayload from './configTeamPayload';

export default {
  searchPayload,
  addPayload,
  updatePayload,
  configTeamPayload
};

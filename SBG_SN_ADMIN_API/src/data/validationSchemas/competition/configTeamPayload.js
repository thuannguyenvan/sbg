import { Joi } from '../../../utils/validate';
import {
  LanguageKeys,
  Patterns
} from '../../constants';

const labels = LanguageKeys.validate.labels;

export default Joi.object().keys({
  soccerCompetitionTeams: Joi.array().items(Joi.object().keys({
    soccerTeamId: Joi.string().regex(Patterns.MONGODB_ID).required().label(labels.COMPETITION_SOCCER_TEAM_ID).description('soccerTeamId')
  }))
}).label('payload');

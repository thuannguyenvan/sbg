import { Joi } from '../../utils/validate';
import { LanguageKeys } from '../constants';

const labels = LanguageKeys.validate.labels;

export default Joi.object().keys({
  // page: Joi.number().required().error(() => '{{!label}} "foo" requires a positive number'),
  // size: Joi.number().required().options({language:{any:{required:'!!khoong duowc rong'}}})
  page: Joi.number().integer().label(labels.CURRENT_PAGE).description('Current page'),
  size: Joi.number().integer().greater(0).label(labels.PAGE_SIZE).description('Page size'),
  orderBy: Joi.string().allow('').label(labels.SORT_FIELD).description('Sort by'),
  order: Joi.string().allow('').label(labels.SORT_ORDER_TYPE).description('Order type, ASC or DESC')
});

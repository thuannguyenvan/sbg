import { Joi, MessageTemplate } from '../../utils/validate';
import { LanguageKeys, Formats } from '../constants';

const messageTemplates = LanguageKeys.validate.messageTemplates;
const labels = LanguageKeys.validate.labels;

export default Joi.object({
  userName: Joi.string().label(labels.USERNAME).required(),
  password: Joi.string().label(labels.PASSWORD).required(),
  age: Joi.number().min(10).max(100).label(labels.AGE),
  dateStart: Joi.date().format(Formats.DATE).label(labels.START_DATE),
  dateEnd: Joi.date().format(Formats.DATE).label(labels.END_DATE).greater(Joi.ref('dateStart')).options({language:{
    date: {
      greater: MessageTemplate(messageTemplates.DATE_GREATER, labels.START_DATE)
    }
  }})
});

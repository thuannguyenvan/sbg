import { Joi } from '../../../utils/validate';
import BasePagination from '../basePagination';
import { LanguageKeys } from '../../constants';

const labels = LanguageKeys.validate.labels;

export default BasePagination.keys({
  keywords: Joi.string().trim().max(255).allow('').label(labels.KEYWORDS).description('Keywords')
}).label('payload');

import { Joi } from '../../../utils/validate';
import { LanguageKeys } from '../../constants';

const Labels = LanguageKeys.validate.labels;

export default Joi.object().keys({
  roles: Joi.array().items(
    Joi.object().keys({
      id: Joi.string().label('Role Id').allow('').required().description('Role ID'),
      checked: Joi.boolean().label('Checked').required().description('Checked')
    }).label(Labels.USER_ROLE)
  ).required().min(1).label(Labels.USER_LIST_ROLES)
}).label('payload');

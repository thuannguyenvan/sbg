import searchPayload from './searchPayload';
import updateRolesPayload from './updateRolesPayload';

export default {
  searchPayload,
  updateRolesPayload
};

import searchPayload from './searchPayload';
import addPayload from './addPayload';
import updatePayload from './updatePayload';
import getAddInfoQuery from './getAddInfoQuery';

export default {
  searchPayload,
  addPayload,
  updatePayload,
  getAddInfoQuery
};

import { Joi } from '../../../utils/validate';
import {
  LanguageKeys,
  Patterns,
  Formats
} from '../../constants';
import {
  LivestreamStatus,
  LivestreamVideoUnit
} from '../../enums';

const labels = LanguageKeys.validate.labels;

export default Joi.object().keys({
  soccerMatchId: Joi.string().required().regex(Patterns.MONGODB_ID)
                    .label(labels.SOCCER_MATCH_STREAM_MATCH)
                    .description('soccerMatchId'),
  startAt: Joi.date().required().format(Formats.DATE_TIME)
              .label(labels.SOCCER_MATCH_STREAM_START_AT)
              .description('startAt'),
  title: Joi.string().trim().max(255).required()
            .label(labels.SOCCER_MATCH_STREAM_TITLE)
            .description('title'),
  status: Joi.any().required().valid(Object.keys(LivestreamStatus).map(k => LivestreamStatus[k]))
              .label(labels.SOCCER_MATCH_STREAM_STATUS)
              .description('status'),
  description: Joi.string().trim().allow('').max(1000)
                  .label(labels.SOCCER_MATCH_STREAM_DESCRIPTION)
                  .description('description'),
  adsConfigurationId: Joi.string().regex(Patterns.MONGODB_ID)
              .label(labels.SOCCER_MATCH_STREAM_ADS_CONFIGURATION)
              .description('adsConfigurationId'),
  chatUrl: Joi.string().trim().allow('').max(1000)
              .label(labels.SOCCER_MATCH_STREAM_CHAT_URL)
              .description('chatUrl'),
  urlSlug: Joi.string().required().trim().allow('').max(1000)
              .label(labels.SOCCER_MATCH_STREAM_URL_SLUG)
              .description('urlSlug'),
  keywords: Joi.string().trim().allow('').max(1000)
              .label(labels.SOCCER_MATCH_STREAM_KEYWORDS)
              .description('keywords'),
  // tags: Joi.array().items(Joi.string().trim().allow('').max(255).label(labels.SOCCER_MATCH_STREAM_TAG_ITEM))
  //             .label(labels.SOCCER_MATCH_STREAM_TAGS)
  //             .description('tags'),
  tags: Joi.array().items(Joi.string().trim().label(labels.SOCCER_MATCH_STREAM_TAG_ITEM))
                .itemMax(255)
                .label(labels.SOCCER_MATCH_STREAM_TAGS)
                .description('tags'),
  livestreamUrls: Joi.array().items(Joi.object().keys({
                    label: Joi.string().trim().max(255).required()
                              .label(labels.SOCCER_MATCH_STREAM_LIVESTREAM_URL_LABEL).description('label'),
                    quanlity: Joi.any().required()
                                  .valid(Object.keys(LivestreamVideoUnit).map(k => LivestreamVideoUnit[k]))
                                  .label(labels.SOCCER_MATCH_STREAM_LIVESTREAM_URL_QUANLITY).description('quanlity'),
                    link: Joi.string().trim().max(1000).required()
                              .label(labels.SOCCER_MATCH_STREAM_LIVESTREAM_URL_LINK).description('link')
                  })).min(1)
                  .label(labels.SOCCER_MATCH_STREAM_LIVESTREAM_URLS)
                  .description('livestreamUrls')
}).label('payload');

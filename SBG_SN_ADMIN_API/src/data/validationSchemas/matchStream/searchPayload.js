import { Joi } from '../../../utils/validate';
import BasePagination from '../basePagination';
import {
  LanguageKeys,
  Patterns,
  Formats
} from '../../constants';
import { LivestreamStatus } from '../../enums';

const labels = LanguageKeys.validate.labels;

export default BasePagination.keys({
  soccerCompetitionId: Joi.string().regex(Patterns.MONGODB_ID)
                          .label(labels.SOCCER_MATCH_STREAM_COMPETITION)
                          .description('soccerCompetitionId'),
  soccerRoundId: Joi.string().regex(Patterns.MONGODB_ID)
                    .label(labels.SOCCER_MATCH_STREAM_ROUND)
                    .description('soccerRoundId'),
  soccerMatchId: Joi.string().regex(Patterns.MONGODB_ID)
                    .label(labels.SOCCER_MATCH_STREAM_MATCH)
                    .description('soccerMatchId'),
  livestreamStatus: Joi.any().allow(null)
                        .valid(Object.keys(LivestreamStatus).map(k => LivestreamStatus[k]))
                        .label(labels.SOCCER_MATCH_STREAM_STATUS)
                        .description('livestreamStatus'),
  dateStartAt: Joi.date().format(Formats.DATE)
                      .label(labels.SOCCER_MATCH_STREAM_LIVESTREAM_DATE)
                      .description('dateStartAt'),
  title: Joi.string().trim().max(255).allow('')
                      .label(labels.SOCCER_MATCH_STREAM_TITLE)
                      .description('title')
}).label('payload');

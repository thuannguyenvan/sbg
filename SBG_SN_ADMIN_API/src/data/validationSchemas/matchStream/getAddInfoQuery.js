import { Joi } from '../../../utils/validate';
import {
  LanguageKeys,
  Patterns
} from '../../constants';

const labels = LanguageKeys.validate.labels;

export default Joi.object().keys({
  soccerMatchId: Joi.string().regex(Patterns.MONGODB_ID)
                    .label(labels.SOCCER_MATCH_STREAM_MATCH_ID)
                    .description('soccerMatchId')
}).label('query');

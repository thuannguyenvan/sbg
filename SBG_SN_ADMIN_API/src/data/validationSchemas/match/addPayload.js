import {
  Joi,
  MessageTemplate
} from '../../../utils/validate';
import {
  LanguageKeys,
  Patterns,
  Formats
} from '../../constants';
import {
  SoccerMatchStatus,
  WinnerTeamType
} from '../../enums';

const messageTemplates = LanguageKeys.validate.messageTemplates;
const labels = LanguageKeys.validate.labels;

export default Joi.object().keys({
  soccerRoundId: Joi.string().required().regex(Patterns.MONGODB_ID)
                    .label(labels.SOCCER_MATCH_ROUND)
                    .description('soccerRoundId'),
  status: Joi.any().required().valid(Object.keys(SoccerMatchStatus).map(k => SoccerMatchStatus[k]))
                    .label(labels.SOCCER_MATCH_STATUS)
                    .description('status'),
  startAt: Joi.date().required().format(Formats.DATE_TIME)
              .label(labels.SOCCER_MATCH_START_AT)
              .when('finishedAt', { is: Joi.date().format(Formats.DATE_TIME).exist(), then: Joi.date().less(Joi.ref('finishedAt')) })
              .options({language: {
                date: {
                  less: MessageTemplate(messageTemplates.DATE_LESS, labels.SOCCER_MATCH_FINISHED_AT)
                }
              }})
              .description('startAt'),
  finishedAt: Joi.date().format(Formats.DATE_TIME)
              .label(labels.SOCCER_MATCH_FINISHED_AT)
              .description('startAt'),
  title: Joi.string().trim().max(255).required()
            .label(labels.SOCCER_MATCH_TITLE)
            .description('title'),
  place: Joi.string().trim().allow('').max(255)
            .label(labels.SOCCER_MATCH_PLACE)
            .description('place'),
  teamA: Joi.object().keys({
    soccerCompetitionStageTeamId: Joi.string().required().regex(Patterns.MONGODB_ID)
  }).required().label(labels.SOCCER_MATCH_TEAM_A).description('teamA'),
  teamB: Joi.object().keys({
    soccerCompetitionStageTeamId: Joi.string().required().regex(Patterns.MONGODB_ID)
  }).required().label(labels.SOCCER_MATCH_TEAM_B).description('teamB'),
  winner: Joi.any().valid(Object.keys(WinnerTeamType).map(k => WinnerTeamType[k]))
            .label(labels.SOCCER_MATCH_WINNER)
            .description('winner'),
  score: Joi.object().keys({
    fullTime: Joi.object().keys({
      teamA: Joi.number().integer().less(1000).label(labels.SOCCER_MATCH_SCORE_FULL_TIME),
      teamB: Joi.number().integer().less(1000).label(labels.SOCCER_MATCH_SCORE_FULL_TIME)
    }),
    halfTime: Joi.object().keys({
      teamA: Joi.number().integer().less(1000).label(labels.SOCCER_MATCH_SCORE_HALF_TIME),
      teamB: Joi.number().integer().less(1000).label(labels.SOCCER_MATCH_SCORE_HALF_TIME)
    }),
    extraTime: Joi.object().keys({
      teamA: Joi.number().integer().less(1000).label(labels.SOCCER_MATCH_SCORE_EXTRA_TIME),
      teamB: Joi.number().integer().less(1000).label(labels.SOCCER_MATCH_SCORE_EXTRA_TIME)
    }),
    penalty: Joi.object().keys({
      teamA: Joi.number().integer().less(1000).label(labels.SOCCER_MATCH_SCORE_PENALTY),
      teamB: Joi.number().integer().less(1000).label(labels.SOCCER_MATCH_SCORE_PENALTY)
    }),
    aggregate: Joi.object().keys({
      teamA: Joi.number().integer().less(1000).label(labels.SOCCER_MATCH_SCORE_AGGREGATE),
      teamB: Joi.number().integer().less(1000).label(labels.SOCCER_MATCH_SCORE_AGGREGATE)
    })
  })
}).label('payload');

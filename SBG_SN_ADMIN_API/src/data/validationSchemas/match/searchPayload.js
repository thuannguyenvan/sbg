import { Joi } from '../../../utils/validate';
import BasePagination from '../basePagination';
import {
  LanguageKeys,
  Patterns,
  Formats
} from '../../constants';
import { SoccerMatchStatus } from '../../enums';

const labels = LanguageKeys.validate.labels;

export default BasePagination.keys({
  soccerCompetitionId: Joi.string().regex(Patterns.MONGODB_ID)
                          .label(labels.SOCCER_MATCH_COMPETITION)
                          .description('soccerCompetitionId'),
  soccerRoundId: Joi.string().regex(Patterns.MONGODB_ID)
                    .label(labels.SOCCER_MATCH_ROUND)
                    .description('soccerRoundId'),
  soccerMatchStatus: Joi.any().allow(null).
                        valid(Object.keys(SoccerMatchStatus).map(k => SoccerMatchStatus[k]))
                        .label(labels.SOCCER_MATCH_STATUS)
                        .description('soccerMatchStatus'),
  dateStartAt: Joi.date().format(Formats.DATE)
                      .label(labels.SOCCER_MATCH_MATCH_DATE)
                      .description('startAt'),
  title: Joi.string().trim().max(255).allow('')
                      .label(labels.SOCCER_MATCH_TITLE)
                      .description('title')
}).label('payload');

import { Joi } from '../../../utils/validate';
import BasePagination from '../basePagination';
import {
  LanguageKeys,
  Patterns,
} from '../../constants';
import { AdsFormat } from '../../enums';

const labels = LanguageKeys.validate.labels;

export default BasePagination.keys({
  adsPartnerId: Joi.string().regex(Patterns.MONGODB_ID).label(labels.ADVERTISING_SEARCH_ADS_PARTNER_ID).description('adsPartnerId'),
  title: Joi.string().trim().max(255).allow('').label(labels.ADVERTISING_SEARCH_TITLE).description('Advertising title'),
  adsFormat: Joi.any().allow(null).valid(Object.keys(AdsFormat).map(k => AdsFormat[k])).label(labels.ADVERTISING_SEARCH_ADS_FORMAT).description('Ads format'),
  isActive: Joi.boolean().label(labels.ADVERTISING_SEARCH_ACTIVE_STATUS).description('Advertising status')
}).label('payload');

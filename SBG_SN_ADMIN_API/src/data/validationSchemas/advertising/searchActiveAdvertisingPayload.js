import { Joi } from '../../../utils/validate';
import BasePagination from '../basePagination';
import {
  LanguageKeys,
  Patterns,
} from '../../constants';

const labels = LanguageKeys.validate.labels;

export default BasePagination.keys({
  adsPartnerId: Joi.string().regex(Patterns.MONGODB_ID).label(labels.ADVERTISING_SEARCH_ADS_PARTNER_ID).description('adsPartnerId'),
  title: Joi.string().trim().max(255).allow('').label(labels.ADVERTISING_SEARCH_TITLE).description('Advertising title'),
}).label('payload');

import {
  Joi,
  // MessageTemplate
} from '../../../utils/validate';
import {
  LanguageKeys,
  Patterns,
  // Formats
} from '../../constants';
import { AdsFormat } from '../../enums';

// const messageTemplates = LanguageKeys.validate.messageTemplates;
const labels = LanguageKeys.validate.labels;

export default Joi.object().keys({
  adsPartnerId: Joi.string().required().regex(Patterns.MONGODB_ID).label(labels.ADVERTISING_ADD_ADS_PARTNER_ID).description('adsPartnerId'),
  title: Joi.string().trim().required().max(255).label(labels.ADVERTISING_ADD_TITLE).description('Title'),
  clickThrough: Joi.string().allow('').trim().max(255).label(labels.ADVERTISING_ADD_CLICK_THROUGH).description('ClickThrough'),
  clickTracking: Joi.string().allow('').trim().max(255).label(labels.ADVERTISING_ADD_CLICK_TRACKING).description('ClickTracking'),
  content: Joi.string().trim().required().max(1000).label(labels.ADVERTISING_ADD_CONTENT).description('Content'),
  adsFormat: Joi.any().required().valid(Object.keys(AdsFormat).map(k => AdsFormat[k])).label(labels.ADVERTISING_ADD_ADS_FORMAT).description('Ads format'),
  description: Joi.string().allow('').trim().max(1000).label(labels.ADVERTISING_ADD_DESCRIPTION).description('Description')
}).label('payload');

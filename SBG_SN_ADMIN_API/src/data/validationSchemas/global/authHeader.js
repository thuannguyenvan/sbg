import { Joi } from '../../../utils/validate';
import { Patterns } from '../../constants';

export default Joi.object({
  'authorization': Joi.string().regex(Patterns.BEARER_TOKEN).required().description('Bearer Token')
});

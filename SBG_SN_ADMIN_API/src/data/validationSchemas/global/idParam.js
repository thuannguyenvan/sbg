import { Joi } from '../../../utils/validate';
import { Patterns } from '../../constants';

export default Joi.object({
  'id': Joi.string().regex(Patterns.MONGODB_ID).required().description('id')
});

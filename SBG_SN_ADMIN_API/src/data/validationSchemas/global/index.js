import authHeader from './authHeader';
import idParam from './idParam';

export default {
  authHeader,
  idParam
};

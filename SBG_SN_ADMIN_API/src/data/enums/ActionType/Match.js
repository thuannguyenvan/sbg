export default {
  VIEW_LIST: 'MATCH_VIEW_LIST', // View list and searching
  ADD: 'MATCH_ADD',
  UPDATE: 'MATCH_UPDATE',
  DELETE: 'MATCH_DELETE'
};

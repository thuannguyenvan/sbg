export default {
  VIEW_LIST: 'USER_VIEW_LIST', // View and searching
  UPDATE_ROLES: 'USER_UPDATE_ROLES', // Update roles
  ACTIVATE: 'USER_ACTIVATE',
  DEACTIVATE: 'USER_DEACTIVATE'
};

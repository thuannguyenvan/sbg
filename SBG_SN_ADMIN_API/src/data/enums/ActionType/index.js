import HOME_PAGE from './HomePage';
import USER from './User';
import COMPETITION from './Competition';
import COMPETITION_STAGE from './CompetitionStage';
import ROUND from './Round';
import MATCH from './Match';
import MATCH_STREAM from './MatchStream';
import ADS_PARTNER from './AdsPartner';
import ADVERTISING from './Advertising';
import ADS_CONFIGURATION from './AdsConfiguration';

export default {
  HOME_PAGE,
  USER,
  COMPETITION,
  COMPETITION_STAGE,
  ROUND,
  MATCH,
  MATCH_STREAM,
  ADS_PARTNER,
  ADVERTISING,
  ADS_CONFIGURATION,
};

export default {
  VIEW_LIST: 'ROUND_VIEW_LIST', // View and searching
  ADD: 'ROUND_ADD',
  UPDATE: 'ROUND_UPDATE',
  DELETE: 'ROUND_DELETE',
  ADD_MATCH: 'ROUND_ADD_MATCH'
};

import { Schema as _Schema, model } from 'mongoose';
import BaseSchema from './BaseSchema';

const SoccerCompetitionStageStandingTableSchema = new BaseSchema({
  SoccerCompetitionStageId: {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'SOCCER_COMPETITION_STAGE_ID'
  },
  SoccerCompetitionStageStandingId: {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'SOCCER_COMPETITION_STAGE_STANDING_ID'
  },
  SoccerCompetitionStageTeamId: {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'SOCCER_COMPETITION_STAGE_TEAM_ID'
  },
  SoccerCompetitionTeamId: {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'SOCCER_COMPETITION_TEAM_ID'
  },
  SoccerTeamId: {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'SOCCER_TEAM_ID'
  },
  Group: {
    type: String,
    alias: 'GROUP'
  },
  Name: {
    type: String,
    alias: 'NAME'
  },
  Logo: {
    type: String,
    alias: 'LOGO'
  },
  TeamCode: {
    type: String,
    alias: 'LOGO'
  },
  Position: {
    type: Number,
    alias: 'POSITION'
  },
  Played: {
    type: Number,
    alias: 'PLAYED'
  },
  Won: {
    type: Number,
    alias: 'WON'
  },
  Lost: {
    type: Number,
    alias: 'LOST'
  },
  Drawn: {
    type: Number,
    alias: 'DRAWN'
  },
  GoalsFor: {
    type: Number,
    alias: 'GOALS_FOR'
  },
  GoalsAgainst: {
    type: Number,
    alias: 'GOALS_AGAINST'
  },
  GoalDifference: {
    type: Number,
    alias: 'GOAL_DIFFERENCE'
  },
  Points: {
    type: Number,
    alias: 'POINTS'
  }
});

export default model('SoccerCompetitionStageStandingTables',
                      SoccerCompetitionStageStandingTableSchema,
                      'SOCCER_COMPETITION_STAGE_STANDING_TABLES');

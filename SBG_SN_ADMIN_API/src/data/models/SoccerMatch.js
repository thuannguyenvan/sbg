import { Schema as _Schema, model } from 'mongoose';
import BaseSchema from './BaseSchema';

const SoccerMatchSchema = new BaseSchema({
  SoccerCompetitionId: {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'SOCCER_COMPETITION_ID'
  },
  SoccerCompetitionStageId: {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'SOCCER_COMPETITION_STAGE_ID'
  },
  SoccerRoundId: {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'SOCCER_ROUND_ID'
  },
  // Group: {
  //   type: String,
  //   alias: 'GROUP'
  // },
  Title: {
    type: String,
    alias: 'TITLE'
  },
  Place: {
    type: String,
    alias: 'PLACE'
  },
  StartAt: {
    type: Date,
    alias: 'START_AT'
  },
  FinishedAt: {
    type: Date,
    alias: 'FINISHED_AT'
  },
  TeamA: {
    type: _Schema.Types.ObjectId,
    alias: 'TEAM_A'
  },
  TeamB: {
    type: _Schema.Types.ObjectId,
    alias: 'TEAM_B'
  },
  Status: {
    type: String,
    alias: 'STATUS'
  },
  Winner: {
    type: String,
    alias: 'WINNER'
  },
  ScoreA: {
    type: Number,
    alias: 'SCORE_A'
  },
  ScoreB: {
    type: Number,
    alias: 'SCORE_B'
  },
  ScoreAI: {
    type: Number,
    alias: 'SCORE_A_I'
  },
  ScoreBI: {
    type: Number,
    alias: 'SCORE_B_I'
  },
  ScoreAET: {
    type: Number,
    alias: 'SCORE_A_ET'
  },
  ScoreBET: {
    type: Number,
    alias: 'SCORE_B_ET'
  },
  ScoreAP: {
    type: Number,
    alias: 'SCORE_A_P'
  },
  ScoreBP: {
    type: Number,
    alias: 'SCORE_B_P'
  },
  ScoreAAGG: {
    type: Number,
    alias: 'SCORE_A_AGG'
  },
  ScoreBAGG: {
    type: Number,
    alias: 'SCORE_B_AGG'
  },
  MatchInfo: {
    type: Object,
    alias: 'MATCH_INFO'
  }
});

export default model('SoccerMatchs', SoccerMatchSchema, 'SOCCER_MATCHS');

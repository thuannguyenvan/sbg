import { Schema as _Schema, model } from 'mongoose';
import BaseSchema from './BaseSchema';

const SoccerCompetitionStageStandingSchema = new BaseSchema({
  SoccerCompetitionId: {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'SOCCER_COMPETITION_ID'
  },
  SoccerCompetitionStageId: {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'SOCCER_COMPETITION_STAGE_ID'
  },
  IsShowInHomepage: {
    type: Boolean,
    alias: 'IS_SHOW_IN_HOMEPAGE',
    default: false
  }
});

export default model('SoccerCompetitionStageStandings', SoccerCompetitionStageStandingSchema, 'SOCCER_COMPETITION_STAGE_STANDINGS');

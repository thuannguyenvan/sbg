import { Schema as _Schema, model } from 'mongoose';
import BaseSchema from './BaseSchema';

const SoccerCompetitionStageSchema = new BaseSchema({
  SoccerCompetitionId: {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'ACTION_ID'
  },
  SoccerStageId:  {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'ROLE_ID'
  },
  Title: {
    type: String,
    required: true,
    alias: 'TITLE'
  },
  Key: {
    type: String,
    required: true,
    alias: 'KEY'
  },
  Standalone: {
    type: Boolean,
    default: false,
    alias: 'STANDALONE'
  }
});

export default model('SoccerCompetitionStages', SoccerCompetitionStageSchema, 'SOCCER_COMPETITION_STAGES');

import { Schema as _Schema, model } from 'mongoose';
import BaseSchema from './BaseSchema';

const TokenSchema = new BaseSchema({
  Token: {
    type: String,
    required: true,
    alias: 'TOKEN'
  },
  ProviderToken: {
    type: String,
    alias: 'PROVIDER_TOKEN'
  },
  UserId: {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'USER_ID'
  },
  TokenIssueTime:  {
    type: Date,
    required: true,
    alias: 'TOKEN_ISSUE_TIME',
    default: Date.now
  },
  TokenExpiredTime:  {
    type: Date,
    alias: 'TOKEN_EXPIRED_TIME'
  },
  TokenType:  {
    type: Number,
    alias: 'TOKEN_TYPE'
  },
  Expired:  {
    type: Boolean,
    alias: 'EXPIRED',
    default: false
  }
});

export default model('Tokens', TokenSchema, 'TOKENS');

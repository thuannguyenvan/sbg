import { model } from 'mongoose';
import BaseSchema from './BaseSchema';

const RoleSchema = new BaseSchema({
  Name: {
    type: String,
    required: true,
    alias: 'NAME'
  },
  Key: {
    type: String,
    required: true,
    unique: true,
    alias: 'KEY'
  },
  CmsAccess: {
    type: Boolean,
    required: true,
    alias: 'CMS_ACCESS',
    default: false
  },
  Description: {
    type: String,
    alias: 'DESCRIPTION'
  },
  IsActive:  {
    type: Boolean,
    required: true,
    alias: 'IS_ACTIVE',
    default: true
  }
});

export default model('Roles', RoleSchema, 'ROLES');

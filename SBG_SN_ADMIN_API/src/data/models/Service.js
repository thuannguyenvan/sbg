import { model } from 'mongoose';
import BaseSchema from './BaseSchema';

const ServiceSchema = new BaseSchema({
  Title: {
    type: String,
    required: true,
    alias: 'TITLE'
  },
  Description: {
    type: String,
    alias: 'DESCRIPTION'
  },
  Amount: {
    type: Number,
    required: true,
    alias: 'AMOUNT'
  },
  ServiceType: {
    type: Boolean,
    alias: 'SERVICE_TYPE'
  },
  UnitCode: {
    type: String,
    alias: 'UNIT_CODE'
  },
  ServiceCode: {
    type: Boolean,
    alias: 'SERVICE_CODE'
  },
  IsActive: {
    type: Boolean,
    default: true,
    alias: 'IS_ACTIVE'
  }
});

export default model('Services', ServiceSchema, 'SERVICES');

import { Schema as _Schema, model } from 'mongoose';
import BaseSchema from './BaseSchema';

const AdvertisingSchema = new BaseSchema({
  AdsPartnerId: {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'ADS_PARTNER_ID'
  },
  AdsPartnerName: {
    type: String,
    alias: 'ADS_PARTNER_NAME'
  },
  AdsPartnerLogo: {
    type: String,
    alias: 'ADS_PARTNER_LOGO'
  },
  AdsPartnerWebsite: {
    type: String,
    alias: 'ADS_PARTNER_WEBSITE'
  },
  Title: {
    type: String,
    required: true,
    alias: 'TITLE'
  },
  AdsFormat: {
    type: String,
    required: true,
    alias: 'ADS_FORMAT'
  },
  ClickThrough: {
    type: String,
    alias: 'CLICK_THROUGH'
  },
  ClickTracking: {
    type: String,
    alias: 'CLICK_TRACKING'
  },
  Content: {
    type: String,
    required: true,
    alias: 'CONTENT'
  },
  Description: {
    type: String,
    alias: 'DESCRIPTION'
  },
  IsActive: {
    type: Boolean,
    required: true,
    default: true,
    alias: 'IS_ACTIVE'
  }
});

export default model('Advertisings', AdvertisingSchema, 'ADVERTISINGS');

import { model } from 'mongoose';
import BaseSchema from './BaseSchema';

const SoccerStageSchema = new BaseSchema({
  Title: {
    type: String,
    required: true,
    alias: 'TITLE'
  },
  Key: {
    type: String,
    required: true,
    unique: true,
    alias: 'KEY'
  },
  Order: {
    type: Number,
    required: true,
    unique: true,
    alias: 'ORDER'
  },
  Standalone: {
    type: Boolean,
    default: false,
    alias: 'STANDALONE'
  },
  Description:  {
    type: String,
    alias: 'DESCRIPTION'
  },
  IsActive:  {
    type: Boolean,
    required: true,
    alias: 'IS_ACTIVE',
    default: true
  }
});

export default model('SoccerStages', SoccerStageSchema, 'SOCCER_STAGES');

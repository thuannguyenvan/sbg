import { Schema as _Schema, model } from 'mongoose';
import BaseSchema from './BaseSchema';

const SoccerCompetitionTeamSchema = new BaseSchema({
  SoccerCompetitionId: {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'SOCCER_COMPETITION_ID'
  },
  SoccerTeamId: {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'SOCCER_TEAM_ID'
  },
  Name: {
    type: String,
    required: true,
    alias: 'NAME'
  },
  TeamCode: {
    type: String,
    alias: 'TEAM_CODE'
  },
  Logo: {
    type: String,
    alias: 'LOGO'
  },
  TeamInfo: {
    type: Object,
    alias: 'TEAM_INFO'
  }
});

export default model('SoccerCompetitionTeams', SoccerCompetitionTeamSchema, 'SOCCER_COMPETITION_TEAMS');

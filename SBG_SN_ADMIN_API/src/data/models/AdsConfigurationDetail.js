import { Schema as _Schema, model } from 'mongoose';
import BaseSchema from './BaseSchema';

const AdsConfigurationDetailSchema = new BaseSchema({
  AdsConfigurationId: {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'ADS_CONFIGURATION_ID'
  },
  AdsPartnerId: {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'ADS_PARTNER_ID'
  },
  AdvertisingId: {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'ADVERTISING_ID'
  },
  AdsFormat: {
    type: String,
    required: true,
    alias: 'ADS_FORMAT'
  },
  Content: {
    type: String,
    alias: 'CONTENT'
  },
  ClickThrough: {
    type: String,
    alias: 'CLICK_THROUGH'
  },
  ClickTracking: {
    type: String,
    alias: 'CLICK_TRACKING'
  },
  Config: {
    type: {
      Type: String,
      Overlay: Boolean,
      Order: Number,
      TimeOffset: Number,
      Duration: Number,
      SkipOffset: Number
    },
    alias: 'CONFIG'
  },
  IsActive: {
    type: Boolean,
    required: true,
    default: true,
    alias: 'IS_ACTIVE'
  }
});

export default model('AdsConfigurationDetails', AdsConfigurationDetailSchema, 'ADS_CONFIGURATION_DETAILS');

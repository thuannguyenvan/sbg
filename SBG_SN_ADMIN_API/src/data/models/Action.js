import { model } from 'mongoose';
import BaseSchema from './BaseSchema';

const ActionSchema = new BaseSchema({
  Name: {
    type: String,
    required: true,
    alias: 'NAME'
  },
  Key: {
    type: String,
    required: true,
    unique: true,
    alias: 'KEY'
  },
  Description:  {
    type: String,
    alias: 'DESCRIPTION'
  },
  IsActive:  {
    type: Boolean,
    required: true,
    alias: 'IS_ACTIVE',
    default: true
  }
});

export default model('Actions', ActionSchema, 'ACTIONS');

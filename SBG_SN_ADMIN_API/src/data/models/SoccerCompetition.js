import { Schema as _Schema, model } from 'mongoose';
import BaseSchema from './BaseSchema';

const SoccerCompetitionSchema = new BaseSchema({
  Title: {
    type: String,
    required: true,
    alias: 'TITLE'
  },
  SoccerCompetitionBaseId: {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'SOCCER_COMPETITION_BASE_ID'
  },
  CompetitionCode: {
    type: String,
    unique: true,
    sparse: true,
    alias: 'COMPETITION_CODE'
  },
  Logo: {
    type: String,
    alias: 'LOGO'
  },
  Season: {
    type: String,
    required: true,
    alias: 'SEASON'
  },
  Place: {
    type: String,
    alias: 'PLACE'
  },
  StartAt: {
    type: Date,
    alias: 'START_AT'
  },
  EndAt: {
    type: Date,
    alias: 'END_AT'
  },
  Content: {
    type: String,
    alias: 'CONTENT'
  },
  Description: {
    type: String,
    alias: 'DESCRIPTION'
  },
  IsActive:  {
    type: Boolean,
    required: true,
    alias: 'IS_ACTIVE',
    default: true
  }
});

SoccerCompetitionSchema.index({ SoccerCompetitionBaseId: 1, Season: 1}, { unique: true });

export default model('SoccerCompetitions', SoccerCompetitionSchema, 'SOCCER_COMPETITIONS');

import { Schema as _Schema, model } from 'mongoose';
import BaseSchema from './BaseSchema';

const PackageSchema = new BaseSchema({
  Title: {
    type: String,
    required: true,
    alias: 'TITLE'
  },
  Description: {
    type: String,
    alias: 'DESCRIPTION'
  },
  Amount: {
    type: Number,
    required: true,
    alias: 'AMOUNT'
  },
  UnitCode: {
    type: String,
    alias: 'UNIT_CODE'
  },
  PackageCode: {
    type: String,
    alias: 'PACKAGE_CODE'
  },
  ServiceIds: {
    type: [_Schema.Types.ObjectId],
    alias: 'SERVICE_IDS'
  },
  IsActive: {
    type: Boolean,
    default: true,
    alias: 'IS_ACTIVE'
  }
});

export default model('Packages', PackageSchema, 'PACKAGES');

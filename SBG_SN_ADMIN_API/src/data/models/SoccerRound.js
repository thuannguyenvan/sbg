import { Schema as _Schema, model } from 'mongoose';
import BaseSchema from './BaseSchema';

const SoccerRoundSchema = new BaseSchema({
  Title: {
    type: String,
    alias: 'TITLE'
  },
  SoccerCompetitionId: {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'SOCCER_COMPETITION_ID'
  },
  SoccerCompetitionStageId: {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'SOCCER_COMPETITION_STAGE_ID'
  },
  StartAt: {
    type: Date,
    alias: 'START_AT'
  },
  EndAt: {
    type: Date,
    alias: 'END_AT'
  },
  Description: {
    type: String,
    alias: 'DESCRIPTION'
  }
});

export default model('SoccerRounds', SoccerRoundSchema, 'SOCCER_ROUNDS');

import { Schema as _Schema, model } from 'mongoose';
import BaseSchema from './BaseSchema';

const UserSchema = new BaseSchema({
  UserName: {
    type:String,
    required: true,
    alias: 'USERNAME'
  },
  FullName: {
    type:String,
    required: true,
    alias: 'FULL_NAME'
  },
  Email: {
    type:String,
    required: true,
    alias: 'EMAIL'
  },
  Avatar: {
    type: String,
    alias: 'AVATAR'
  },
  PhoneNumber: {
    type: String,
    alias: 'PHONE_NUMBER'
  },
  Notes: {
    type: String,
    alias: 'NOTES'
  },
  Status: {
    type: Number,
    required: true,
    alias: 'STATUS'
  },
  IsActive: {
    type: Boolean,
    default: false,
    alias: 'IS_ACTIVE'
  },
  IsSupperAdmin: {
    type: Boolean,
    default: false,
    alias: 'IS_SUPPER_ADMIN'
  },
  Roles: [{
    type: _Schema.Types.ObjectId,
    alias: 'ROLES'
  }]
});

export default model('Users', UserSchema, 'USERS');

import { model } from 'mongoose';
import BaseSchema from './BaseSchema';

const AdsConfigurationSchema = new BaseSchema({
  Title: {
    type: String,
    required: true,
    alias: 'TITLE'
  },
  Description: {
    type: String,
    alias: 'DESCRIPTION'
  },
  UpdatingStatus: {
    type: String,
    required: true,
    alias: 'UPDATING_STATUS'
  },
  AdsData: {
    type: String,
    alias: 'ADS_DATA'
  },
  IsActive: {
    type: Boolean,
    required: true,
    default: true,
    alias: 'IS_ACTIVE'
  }
});

export default model('AdsConfigurations', AdsConfigurationSchema, 'ADS_CONFIGURATIONS');

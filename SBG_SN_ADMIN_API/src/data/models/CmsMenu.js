import { Schema as _Schema, model } from 'mongoose';
import BaseSchema from './BaseSchema';

const CmsMenuSchema = new BaseSchema({
  ParentId: {
    type: _Schema.Types.ObjectId,
    alias: 'PARENT_ID'
  },
  Title: {
    type: String,
    required: true,
    alias: 'TITLE'
  },
  Icon: {
    type: String,
    alias: 'ICON'
  },
  Url: {
    type: String,
    required: true,
    alias: 'URL'
  },
  Level: {
    type: Number,
    required: true,
    default: 0,
    alias: 'LEVEL'
  },
  Roles: [{
    type: _Schema.Types.ObjectId,
    alias: 'ROLES'
  }],
  Location: {
    type: String,
    alias: 'LOCATION'
  },
  IsActive: {
    type: Boolean,
    required: true,
    default: true,
    alias: 'IS_ACTIVE'
  }
});

export default model('CmsMenus', CmsMenuSchema, 'CMS_MENUS');

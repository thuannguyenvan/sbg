import { Schema as _Schema, model } from 'mongoose';
import BaseSchema from './BaseSchema';

const SoccerCompetitionStageTeamSchema = new BaseSchema({
  SoccerCompetitionId:{
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'SOCCER_COMPETITION_ID'
  },
  SoccerCompetitionStageId: {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'SOCCER_COMPETITION_STAGE_ID'
  },
  SoccerCompetitionTeamId: {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'SOCCER_COMPETITION_TEAM_ID'
  },
  SoccerTeamId: {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'SOCCER_TEAM_ID'
  },
  Group: {
    type: String,
    alias: 'GROUP'
  },
  Name: {
    type: String,
    alias: 'NAME'
  },
  Logo: {
    type: String,
    alias: 'LOGO'
  },
  TeamCode: {
    type: String,
    alias: 'TEAM_CODE'
  },
  TeamInfo: {
    type: Object,
    alias: 'TEAM_INFO'
  }
});

export default model('SoccerCompetitionStageTeams', SoccerCompetitionStageTeamSchema, 'SOCCER_COMPETITION_STAGE_TEAMS');

import { model } from 'mongoose';
import BaseSchema from './BaseSchema';

const SoccerCompetitionBaseSchema = new BaseSchema({
  Title: {
    type: String,
    required: true,
    alias: 'TITLE'
  },
  Logo: {
    type: String,
    alias: 'LOGO'
  },
  CompetitionCode: {
    type: String,
    unique: true,
    sparse: true,
    alias: 'COMPETITION_CODE'
  },
  CompetitionBaseInfo: {
    type: Object,
    alias: 'COMPETITION_BASE_INFO'
  },
  Content: {
    type: String,
    alias: 'CONTENT'
  },
  Description: {
    type: String,
    alias: 'DESCRIPTION'
  },
  IsActive:  {
    type: Boolean,
    required: true,
    alias: 'IS_ACTIVE',
    default: true
  }
});

export default model('SoccerCompetitionBases', SoccerCompetitionBaseSchema, 'SOCCER_COMPETITION_BASES');

import { Schema as _Schema } from 'mongoose';

let BaseSchema = function (additions) {
  let schemaBase = new _Schema({
    Code: {
      type: String,
      alias: 'CODE'
    },
    Order: {
      type: Number,
      alias: 'ORDER'
    },
    CreateAt:  {
      type: Date,
      required: true,
      alias: 'CREATE_AT',
      default: Date.now
    },
    EditAt:  {
      type: Date,
      alias: 'EDIT_AT'
    },
    Creator:  {
      type: _Schema.Types.ObjectId,
      alias: 'CREATOR',
      field: '_id'
    },
    Editor:  {
      type: _Schema.Types.ObjectId,
      alias: 'EDITOR'
    },
    IsDeleted: {
      type: Boolean,
      default: false,
      alias: 'IS_DELETED'
    },
    HistoryInfo:  {
      type: String,
      alias: 'HISTORY_INFO'
    },
    _Filter: {
      type: Object,
      default: {}
    }
  });

  schemaBase.options.toJSON = {
    /* eslint-disable */
    transform: function(doc, ret, options) {
      // ret = JSON.parse(JSON.stringify(ret));
      // ret.id = ret._id;
      // delete ret._id;
      // delete ret.__v;
      // ret = JSON.parse(JSON.stringify(ret));
      // return ret;
      // return JSON.parse(JSON.stringify(doc));
      return ret;
    }
  };

  schemaBase.method('toJsonObject', function () {
    const _this = this;
    const ret = JSON.parse(JSON.stringify(_this));
    if (_this.total !== undefined && _this.total !== null) {
      ret.total = _this.total;
    }
    delete ret.__v;
    return ret;
  });

  schemaBase.statics.pagination = async function(filter, page, size, orderBy, order) {
    if (page <= 0) page = 1;
    // desc = !!desc;

    if (order && orderBy) {
      if (order.toUpperCase() === 'ASC') {
        orderBy = orderBy;
      } else if (order.toUpperCase() === 'DESC') {
        orderBy = `-${orderBy}`;
      } else {
        orderBy = undefined;
      }
    }

    const query = this.find(filter).sort(orderBy);
    const count = await query.countDocuments();
    const docs = await this.find(filter).sort(orderBy).skip((page - 1) * size).limit(size);
    // let totalPage = Math.floor(count / size);
    // if (count % size > 0) totalPage++;
    const datas = docs.map(d => {
      d.total = count;
      return d;
    });
    return datas;
  };

  if (additions) {
    schemaBase.add(additions);
  }

  return schemaBase;
};

export default BaseSchema;

import { model } from 'mongoose';
import BaseSchema from './BaseSchema';

const AdsPartnerSchema = new BaseSchema({
  Name: {
    type: String,
    required: true,
    alias: 'NAME'
  },
  Logo: {
    type: String,
    alias: 'LOGO'
  },
  Website: {
    type: String,
    alias: 'WEBSITE'
  },
  Description: {
    type: String,
    alias: 'DESCRIPTION'
  },
  IsActive: {
    type: Boolean,
    required: true,
    default: true,
    alias: 'IS_ACTIVE'
  }
});

export default model('AdsPartners', AdsPartnerSchema, 'ADS_PARTNERS');

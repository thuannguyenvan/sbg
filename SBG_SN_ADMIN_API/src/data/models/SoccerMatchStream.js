import { Schema as _Schema, model } from 'mongoose';
import BaseSchema from './BaseSchema';

const SoccerMatchStreamSchema = new BaseSchema({
  SoccerCompetitionId: {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'SOCCER_COMPETITION_ID'
  },
  SoccerCompetitionStageId: {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'SOCCER_COMPETITION_STAGE_ID'
  },
  SoccerRoundId: {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'SOCCER_ROUND_ID'
  },
  SoccerMatchId: {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'SOCCER_MATCH_ID'
  },
  Title: {
    type: String,
    required: true,
    alias: 'TITLE'
  },
  Description: {
    type: String,
    alias: 'DESCRIPTION'
  },
  StartAt: {
    type: Date,
    required: true,
    alias: 'START_AT'
  },
  ChatUrl: {
    type: String,
    alias: 'CHAT_URL'
  },
  UrlSlug: {
    type: String,
    required: true,
    alias: 'URL_SLUG'
  },
  Keywords: {
    type: String,
    alias: 'KEYWORDS'
  },
  LivestreamUrls: {
    type: [{
      Label: String,
      Quanlity: String,
      Link: String
    }],
    alias: 'LIVESTREAM_URLS'
  },
  Tags:  [{
    type: String,
    alias: 'TAGS'
  }],
  AdsConfigurationId: {
    type: _Schema.Types.ObjectId,
    alias: 'ADS_CONFIGURATION_ID'
  },
  AdsData: {
    type: String,
    alias: 'ADS_DATA'
  },
  Status: {
    type: String,
    alias: 'STATUS'
  }
});

export default model('SoccerMatchStreams', SoccerMatchStreamSchema, 'SOCCER_MATCH_STREAMS');

import { Schema as _Schema, model } from 'mongoose';
import BaseSchema from './BaseSchema';

const RoleActionSchema = new BaseSchema({
  ActionId: {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'ACTION_ID'
  },
  RoleId: {
    type: _Schema.Types.ObjectId,
    required: true,
    alias: 'ROLE_ID'
  }
});

export default model('RoleActions', RoleActionSchema, 'ROLE_ACTIONS');

import { model } from 'mongoose';
import BaseSchema from './BaseSchema';

const SoccerTeamSchema = new BaseSchema({
  Name: {
    type: String,
    required: true,
    alias: 'NAME'
  },
  TeamCode: {
    type: String,
    unique: true,
    sparse: true,
    alias: 'TEAM_CODE'
  },
  IsClub: {
    type: Boolean,
    default: false,
    alias: 'IS_CLUB'
  },
  Logo: {
    type: String,
    alias: 'LOGO'
  },
  TeamInfo: {
    type: Object,
    alias: 'TEAM_INFO'
  },
  Content: {
    type: String,
    alias: 'CONTENT'
  },
  Description: {
    type: String,
    alias: 'DESCRIPTION'
  },
  IsActive: {
    type: Boolean,
    required: true,
    alias: 'IS_ACTIVE',
    default: true
  }
});

SoccerTeamSchema.pre('save', function() {
  if (this.TeamCode === null) {
    this.TeamCode = undefined;
  }
});

SoccerTeamSchema.pre('update', function() {
  if (this.TeamCode === null) {
    this.TeamCode = undefined;
  }
});

export default model('SoccerTeams', SoccerTeamSchema, 'SOCCER_TEAMS');

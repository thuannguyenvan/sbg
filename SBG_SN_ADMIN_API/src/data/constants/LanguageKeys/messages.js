export default {
  global: {
    INVALID_INPUT: 'global.INVALID_INPUT',
    GET_SUCCESS: 'global.GET_SUCCESS',
    GET_FAIL: 'global.GET_FAIL'
  },
  auth: {
    LOGOUT_SUCCESS: 'auth.LOGOUT_SUCCESS',
    LOGOUT_FAIL: 'auth.LOGOUT_FAIL',
    LOGIN_SUCCESS: 'auth.LOGIN_SUCCESS',
    LOGIN_FAIL: 'auth.LOGIN_FAIL',
    INVALID_TOKEN: 'auth.INVALID_TOKEN',
    USER_DOES_NOT_EXISTS: 'auth.USER_DOES_NOT_EXISTS',
    USER_WAS_INACTIVE: 'auth.USER_WAS_INACTIVE',
    UNAUTHORIZED: 'auth.UNAUTHORIZED',
    USER_ROLE_HAS_NOT_PERMISSION: 'auth.USER_ROLE_HAS_NOT_PERMISSION',
    INSUFFICIENT_SCOPE: 'auth.INSUFFICIENT_SCOPE',
    GET_USER_INFO_SUCCESS: 'auth.GET_USER_INFO_SUCCESS',
    GET_USER_INFO_FAIL: 'auth.GET_USER_INFO_FAIL'
  },
  user: {
    SEARCH_USER_SUCCESS: 'user.SEARCH_USER_SUCCESS',
    INVALID_USER_ID: 'user.INVALID_USER_ID',
    USER_DOES_NOT_EXIST: 'user.USER_DOES_NOT_EXIST',
    GET_USER_ROLES_SUCCESS: 'user.GET_USER_ROLES_SUCCESS',
    ROLES_MUST_BE_SELECTED_AT_LEAST_ONE: 'user.ROLES_MUST_BE_SELECTED_AT_LEAST_ONE',
    UPDATE_USER_ROLES_SUCCESS: 'user.UPDATE_USER_ROLES_SUCCESS',
    UPDATE_USER_ROLES_FAIL: 'user.UPDATE_USER_ROLES_FAIL',
    /* Activate or Deactive user */
    ACTIVATE_USER_SUCCESS: 'user.ACTIVATE_USER_SUCCESS',
    DEACTIVATE_USER_SUCCESS: 'user.DEACTIVATE_USER_SUCCESS',
    ACTIVATE_USER_FAIL: 'user.ACTIVATE_USER_FAIL',
    DEACTIVATE_USER_FAIL: 'user.DEACTIVATE_USER_FAIL',
    USER_ALREADY_ACTIVE: 'user.USER_ALREADY_ACTIVE',
    USER_ALREADY_INACTIVE: 'user.USER_ALREADY_INACTIVE'
  },
  competition: {
    SEARCH_COMPETITION_SUCCESS: 'competition.SEARCH_COMPETITION_SUCCESS',
    SEARCH_COMPETITION_FAIL: 'competition.SEARCH_COMPETITION_FAIL',
    ADD_COMPETITION_SUCCESS: 'competition.ADD_COMPETITION_SUCCESS',
    ADD_COMPETITION_FAIL: 'competition.ADD_COMPETITION_FAIL',
    GET_ADD_COMPETITION_INFO_SUCCESS: 'competition.GET_ADD_COMPETITION_INFO_SUCCESS',
    GET_ADD_COMPETITION_INFO_FAIL: 'competition.GET_ADD_COMPETITION_INFO_FAIL',
    GET_EDIT_COMPETITION_INFO_SUCCESS: 'competition.GET_EDIT_COMPETITION_INFO_SUCCESS',
    GET_EDIT_COMPETITION_INFO_FAIL: 'competition.GET_EDIT_COMPETITION_INFO_FAIL',
    HAS_NO_ONE_VALID_STAGE: 'competition.HAS_NO_ONE_VALID_STAGE',
    // HAS_STANDALONE_WITH_OTHERS: 'competition.HAS_STANDALONE_WITH_OTHERS',
    COMPETITION_TYPE_DOES_NOT_EXIST: 'competition.COMPETITION_TYPE_DOES_NOT_EXIST',
    HAS_COMPETITION_SAME_SEASON_AND_COMPETITION_BASE: 'competition.HAS_COMPETITION_SAME_SEASON_AND_COMPETITION_BASE',
    COMPETITION_DOES_NOT_EXIST: 'competition.COMPETITION_DOES_NOT_EXIST',
    GET_COMPETITION_STAGE_SUCCESS: 'competition.GET_COMPETITION_STAGE_SUCCESS',
    GET_COMPETITION_STAGE_FAIL: 'competition.GET_COMPETITION_STAGE_FAIL',
    CANNOT_REMOVE_COMPETITION_HAVING_STAGE: 'competition.CANNOT_REMOVE_COMPETITION_HAVING_STAGE',
    DELETE_COMPETITION_SUCCESS: 'competition.DELETE_COMPETITION_SUCCESS',
    DELETE_COMPETITION_FAIL: 'competition.DELETE_COMPETITION_FAIL',
    // CANNOT_REMOVE_STAGE_HAVING_TEAM: 'competition.CANNOT_REMOVE_STAGE_HAVING_TEAM',
    // CANNOT_REMOVE_STAGE_HAVING_ROUND: 'competition.CANNOT_REMOVE_STAGE_HAVING_ROUND',
    UPDATE_COMPETITION_SUCCESS: 'competition.UPDATE_COMPETITION_SUCCESS',
    UPDATE_COMPETITION_FAIL: 'competition.UPDATE_COMPETITION_FAIL',
    GET_LIST_COMPETITION_SUCCESS: 'competition.GET_LIST_COMPETITION_SUCCESS',
    GET_LIST_COMPETITION_FAIL: 'competition.GET_LIST_COMPETITION_FAIL',
    // CANNOT_REMOVE_TEAM_ALREADY_IN_STAGE: 'competition.CANNOT_REMOVE_TEAM_ALREADY_IN_STAGE',
    // CANNOT_REMOVE_TEAMS_ALREADY_IN_STAGE: 'competition.CANNOT_REMOVE_TEAMS_ALREADY_IN_STAGE',
    // GET_CONFIG_TEAM_INFO_SUCCESS: 'competition.GET_CONFIG_TEAM_INFO_SUCCESS',
    // GET_CONFIG_TEAM_INFO_FAIL: 'competition.GET_CONFIG_TEAM_INFO_FAIL',
    CONFIG_TEAM_SUCCESS: 'competition.CONFIG_TEAM_SUCCESS',
    CONFIG_TEAM_FAIL: 'competition.CONFIG_TEAM_FAIL'
  },
  competitionStage: {
    CREATE_STANDING_SUCCESS: 'competitionStage.CREATE_STANDING_SUCCESS',
    CREATE_STANDING_FAIL: 'competitionStage.CREATE_STANDING_FAIL',
    STAGE_DOES_NOT_EXIST: 'competitionStage.STAGE_DOES_NOT_EXIST',
    STAGE_ALREADY_HAS_STANDING: 'competitionStage.STAGE_ALREADY_HAS_STANDING',
    STAGE_HAS_NO_STANDING: 'competitionStage.STAGE_HAS_NO_STANDING',
    DELETE_STANDING_SUCCESS: 'competitionStage.DELETE_STANDING_SUCCESS',
    DELETE_STANDING_FAIL: 'competitionStage.DELETE_STANDING_FAIL',
    GET_CONFIG_TEAM_INFO_SUCCESS: 'competitionStage.GET_CONFIG_TEAM_INFO_SUCCESS',
    GET_CONFIG_TEAM_INFO_FAIL: 'competitionStage.GET_CONFIG_TEAM_INFO_FAIL',
    // 'Tất cả các đội đều phải nằm trong group hoặc đều không nằm trong group nào.'
    ALL_TEAM_MUST_IN_GROUP_OR_ALL_MUST_NOT_IN: 'competitionStage.ALL_TEAM_MUST_IN_GROUP_OR_ALL_MUST_NOT_IN',
    // 'Không được bỏ đội bóng đang có trận đấu ra khỏi Stage.'
    CANNOT_REMOVE_HAS_MATCH_TEAM: 'competitionStage.CANNOT_REMOVE_HAS_MATCH_TEAM',
    CONFIG_TEAM_SUCCESS: 'competitionStage.CONFIG_TEAM_SUCCESS',
    CONFIG_TEAM_FAIL: 'competitionStage.CONFIG_TEAM_FAIL',
    GET_STANDING_TABLES_SUCCESS: 'competitionStage.GET_STANDING_TABLES_SUCCESS',
    GET_STANDING_TABLES_FAIL: 'competitionStage.GET_STANDING_TABLES_FAIL',
    UPDATE_STANDING_SUCCESS: 'competitionStage.UPDATE_STANDING_SUCCESS',
    UPDATE_STANDING_FAIL: 'competitionStage.UPDATE_STANDING_FAIL'
  },
  round: {
    SEARCH_ROUND_SUCCESS: 'round.SEARCH_ROUND_SUCCESS',
    SEARCH_ROUND_FAIL: 'round.SEARCH_ROUND_FAIL',
    CREATE_ROUND_SUCCESS: 'round.CREATE_ROUND_SUCCESS',
    CREATE_ROUND_FAIL: 'round.CREATE_ROUND_FAIL',
    GET_EDIT_ROUND_INFO_SUCCESS: 'round.GET_EDIT_ROUND_INFO_SUCCESS',
    GET_EDIT_ROUND_INFO_FAIL: 'round.GET_EDIT_ROUND_INFO_FAIL',
    UPDATE_ROUND_INFO_SUCCESS: 'round.UPDATE_ROUND_INFO_SUCCESS',
    UPDATE_ROUND_INFO_FAIL: 'round.UPDATE_ROUND_INFO_FAIL',
    ROUND_DOES_NOT_EXIST: 'round.ROUND_DOES_NOT_EXIST',
    HAS_ROUND_WITH_SAME_STAGE_AND_TITLE: 'round.HAS_ROUND_WITH_SAME_STAGE_AND_TITLE',
    DELETE_ROUND_INFO_SUCCESS: 'round.DELETE_ROUND_INFO_SUCCESS',
    DELETE_ROUND_INFO_FAIL: 'round.DELETE_ROUND_INFO_FAIL',
    CANNOT_DELETE_ROUND_HAVING_MATCH: 'round.CANNOT_DELETE_ROUND_HAVING_MATCH',
    GET_ROUND_BY_COMPETITION_SUCCESS: 'round.GET_ROUND_BY_COMPETITION_SUCCESS',
    GET_ROUND_BY_COMPETITION_FAIL: 'round.GET_ROUND_BY_COMPETITION_FAIL',
    GET_TEAM_IN_STAGE_OF_ROUND_SUCCESS: 'round.GET_TEAM_IN_STAGE_OF_ROUND_SUCCESS',
    GET_TEAM_IN_STAGE_OF_ROUND_FAIL: 'round.GET_TEAM_IN_STAGE_OF_ROUND_FAIL',
    GET_MATCHS_IN_ROUND_SUCCESS: 'round.GET_MATCHS_IN_ROUND_SUCCESS',
    GET_MATCHS_IN_ROUND_FAIL: 'round.GET_MATCHS_IN_ROUND_FAIL'
  },
  match: {
    GET_SEARCH_INFO_SUCCESS: 'match.GET_SEARCH_INFO_SUCCESS',
    GET_SEARCH_INFO_FAIL: 'match.GET_SEARCH_INFO_FAIL',
    SEARCH_MATCH_SUCCESS: 'match.SEARCH_MATCH_SUCCESS',
    SEARCH_MATCH_FAIL: 'match.SEARCH_MATCH_FAIL',
    GET_ADD_INFO_SUCCESS: 'match.GET_ADD_INFO_SUCCESS',
    GET_ADD_INFO_FAIL: 'match.GET_ADD_INFO_FAIL',
    ADD_MATCH_SUCCESS: 'match.ADD_MATCH_SUCCESS',
    ADD_MATCH_FAIL: 'match.ADD_MATCH_FAIL',
    MATCH_DOES_NOT_EXIST: 'match.MATCH_DOES_NOT_EXIST',
    CANNOT_DELETE_MATCH_CAUSE_HAVE_STREAM: 'match.CANNOT_DELETE_MATCH_CAUSE_HAVE_STREAM',
    DELETE_MATCH_SUCCESS: 'match.DELETE_MATCH_SUCCESS',
    DELETE_MATCH_FAIL: 'match.DELETE_MATCH_FAIL',
    GET_EDIT_INFO_SUCCESS: 'match.GET_EDIT_INFO_SUCCESS',
    GET_EDIT_INFO_FAIL: 'match.GET_EDIT_INFO_FAIL',
    UPDATE_MATCH_SUCCESS: 'match.UPDATE_MATCH_SUCCESS',
    UPDATE_MATCH_FAIL: 'match.UPDATE_MATCH_FAIL'
  },
  matchStream: {
    GET_SEARCH_INFO_SUCCESS: 'matchStream.GET_SEARCH_INFO_SUCCESS',
    GET_SEARCH_INFO_FAIL: 'matchStream.GET_SEARCH_INFO_FAIL',
    SEARCH_MATCH_STREAM_SUCCESS: 'matchStream.SEARCH_MATCH_STREAM_SUCCESS',
    SEARCH_MATCH_STREAM_FAIL: 'matchStream.SEARCH_MATCH_STREAM_FAIL',
    GET_ADD_INFO_SUCCESS: 'matchStream.GET_ADD_INFO_SUCCESS',
    GET_ADD_INFO_FAIL: 'matchStream.GET_ADD_INFO_FAIL',
    ADD_MATCH_STREAM_SUCCESS: 'matchStream.ADD_MATCH_STREAM_SUCCESS',
    ADD_MATCH_STREAM_FAIL: 'matchStream.ADD_MATCH_STREAM_FAIL',
    HAS_STREAM_WITH_THE_SAME_URL_SLUG: 'matchStream.HAS_STREAM_WITH_THE_SAME_URL_SLUG',
    HAS_STREAM_WITH_THE_SAME_MATCH: 'matchStream.HAS_STREAM_WITH_THE_SAME_MATCH',
    MATCH_STREAM_DOES_NOT_EXIST: 'matchStream.MATCH_STREAM_DOES_NOT_EXIST',
    GET_EDIT_INFO_SUCCESS: 'matchStream.GET_EDIT_INFO_SUCCESS',
    GET_EDIT_INFO_FAIL: 'matchStream.GET_EDIT_INFO_FAIL',
    UPDATE_MATCH_STREAM_SUCCESS: 'matchStream.UPDATE_MATCH_STREAM_SUCCESS',
    UPDATE_MATCH_STREAM_FAIL: 'matchStream.UPDATE_MATCH_STREAM_SUCCESS',
    DELETE_MATCH_STREAM_SUCCESS: 'matchStream.DELETE_MATCH_STREAM_SUCCESS',
    DELETE_MATCH_STREAM_FAIL: 'matchStream.DELETE_MATCH_STREAM_FAIL'
  },
  adsPartner: {
    SEARCH_ADS_PARTNER_SUCCESS: 'adsPartner.SEARCH_ADS_PARTNER_SUCCESS',
    SEARCH_ADS_PARTNER_FAIL: 'adsPartner.SEARCH_ADS_PARTNER_FAIL',
    ADS_PARTNER_WITH_THE_SAME_NAME_ALREADY_EXIST: 'adsPartner.ADS_PARTNER_WITH_THE_SAME_NAME_ALREADY_EXIST',
    ADD_ADS_PARTNER_SUCCESS: 'adsPartner.ADD_ADS_PARTNER_SUCCESS',
    ADD_ADS_PARTNER_FAIL: 'adsPartner.ADD_ADS_PARTNER_FAIL',
    ADS_PARTNER_DOES_NOT_EXIST: 'adsPartner.ADS_PARTNER_DOES_NOT_EXIST',
    GET_EDIT_INFO_SUCCESS: 'adsPartner.GET_EDIT_INFO_SUCCESS',
    GET_EDIT_INFO_FAIL: 'adsPartner.GET_EDIT_INFO_FAIL',
    UPDATE_ADS_PARTNER_SUCCESS: 'adsPartner.UPDATE_ADS_PARTNER_SUCCESS',
    UPDATE_ADS_PARTNER_FAIL: 'adsPartner.UPDATE_ADS_PARTNER_FAIL',
    CANNOT_DELETE_ADS_PARTNER_CAUSE_HAVE_ADVERTISING: 'adsPartner.CANNOT_DELETE_ADS_PARTNER_CAUSE_HAVE_ADVERTISING',
    DELETE_ADS_PARTNER_SUCCESS: 'adsPartner.DELETE_ADS_PARTNER_SUCCESS',
    DELETE_ADS_PARTNER_FAIL: 'adsPartner.DELETE_ADS_PARTNER_FAIL',
    ADS_PARTNER_ALREADY_ACTIVE: 'adsPartner.ADS_PARTNER_ALREADY_ACTIVE',
    ADS_PARTNER_ALREADY_INACTIVE: 'adsPartner.ADS_PARTNER_ALREADY_INACTIVE',
    ACTIVATE_ADS_PARTNER_SUCCESS: 'adsPartner.ACTIVATE_ADS_PARTNER_SUCCESS',
    ACTIVATE_ADS_PARTNER_FAIL: 'adsPartner.ACTIVATE_ADS_PARTNER_FAIL',
    DEACTIVATE_ADS_PARTNER_SUCCESS: 'adsPartner.DEACTIVATE_ADS_PARTNER_SUCCESS',
    DEACTIVATE_ADS_PARTNER_FAIL: 'adsPartner.DEACTIVATE_ADS_PARTNER_FAIL',
  },
  advertising: {
    GET_SEARCH_INFO_SUCCESS: 'advertising.GET_SEARCH_INFO_SUCCESS',
    GET_SEARCH_INFO_FAIL: 'advertising.GET_SEARCH_INFO_FAIL',
    ADS_PARTNER_DOES_NOT_EXIST_OR_INACTIVE: 'advertising.ADS_PARTNER_DOES_NOT_EXIST_OR_INACTIVE',
    SEARCH_ADVERTISING_SUCCES: 'advertising.SEARCH_ADVERTISING_SUCCES',
    SEARCH_ADVERTISING_FAIL: 'advertising.SEARCH_ADVERTISING_FAIL',
    GET_ADD_INFO_SUCCESS: 'advertising.GET_ADD_INFO_SUCCESS',
    GET_ADD_INFO_FAIL: 'advertising.GET_ADD_INFO_FAIL',
    ADVERTISING_WITH_SAME_TITLE_AND_PARTNER_ALREADY_EXIST: 'advertising.ADVERTISING_WITH_SAME_TITLE_AND_PARTNER_ALREADY_EXIST',
    ADD_ADVERTISING_SUCCESS: 'advertising.ADD_ADVERTISING_SUCCESS',
    ADD_ADVERTISING_FAIL: 'advertising.ADD_ADVERTISING_FAIL',
    ADVERTISING_DOES_NOT_EXIST: 'advertising.ADVERTISING_DOES_NOT_EXIST',
    CANNOT_DELETE_ADVERTISING_CAUSE_USED_IN_CONFIG: 'advertising.CANNOT_DELETE_ADVERTISING_CAUSE_USED_IN_CONFIG',
    DELETE_ADVERTISING_SUCCESS: 'advertising.DELETE_ADVERTISING_SUCCESS',
    DELETE_ADVERTISING_FAIL: 'advertising.DELETE_ADVERTISING_FAIL',
    GET_EDIT_INFO_SUCCESS: 'advertising.GET_EDIT_INFO_SUCCESS',
    GET_EDIT_INFO_FAIL: 'advertising.GET_EDIT_INFO_FAIL',
    UPDATE_ADVERTISING_SUCCESS: 'advertising.UPDATE_ADVERTISING_SUCCESS',
    UPDATE_ADVERTISING_FAIL: 'advertising.UPDATE_ADVERTISING_FAIL',
    ADVERTISING_ALREADY_ACTIVE: 'advertising.ADVERTISING_ALREADY_ACTIVE',
    ADVERTISING_ALREADY_INACTIVE: 'advertising.ADVERTISING_ALREADY_INACTIVE',
    ACTIVATE_ADVERTISING_SUCCESS: 'advertising.ACTIVATE_ADVERTISING_SUCCESS',
    ACTIVATE_ADVERTISING_FAIL: 'advertising.ACTIVATE_ADVERTISING_FAIL',
    DEACTIVATE_ADVERTISING_SUCCESS: 'advertising.DEACTIVATE_ADVERTISING_SUCCESS',
    DEACTIVATE_ADVERTISING_FAIL: 'advertising.DEACTIVATE_ADVERTISING_FAIL',
    GET_SEARCH_ACTIVE_ADVERTISING_INFO_SUCCESS: 'advertising.GET_SEARCH_ACTIVE_ADVERTISING_INFO_SUCCESS',
    GET_SEARCH_ACTIVE_ADVERTISING_INFO_FAIL: 'advertising.GET_SEARCH_ACTIVE_ADVERTISING_INFO_FAIL',
    SEARCH_ACTIVE_ADVERTISING_SUCCES: 'advertising.SEARCH_ACTIVE_ADVERTISING_SUCCES',
    SEARCH_ACTIVE_ADVERTISING_FAIL: 'advertising.SEARCH_ACTIVE_ADVERTISING_FAIL',
  },
  adsConfiguration: {
    SEARCH_ADS_CONFIGURATION_SUCCES: 'adsConfiguration.SEARCH_ADS_CONFIGURATION_SUCCES',
    SEARCH_ADS_CONFIGURATION_FAIL: 'adsConfiguration.SEARCH_ADS_CONFIGURATION_FAIL',
    ADS_CONFIGURATION_WITH_THE_SAME_TITLE_ALREADY_EXIST: 'adsConfiguration.ADS_CONFIGURATION_WITH_THE_SAME_TITLE_ALREADY_EXIST',
    ADD_ADS_CONFIGURATION_SUCCESS: 'adsConfiguration.ADD_ADS_CONFIGURATION_SUCCESS',
    ADD_ADS_CONFIGURATION_FAIL: 'adsConfiguration.ADD_ADS_CONFIGURATION_FAIL',
    ADS_CONFIGURATION_DOES_NOT_EXIST: 'adsConfiguration.ADS_CONFIGURATION_DOES_NOT_EXIST',
    GET_EDIT_INFO_SUCCESS: 'adsConfiguration.GET_EDIT_INFO_SUCCESS',
    GET_EDIT_INFO_FAIL: 'adsConfiguration.GET_EDIT_INFO_FAIL',
    UPDATE_ADS_CONFIGURATION_SUCCESS: 'adsConfiguration.UPDATE_ADS_CONFIGURATION_SUCCESS',
    UPDATE_ADS_CONFIGURATION_FAIL: 'adsConfiguration.UPDATE_ADS_CONFIGURATION_FAIL',
    DELETE_ADS_CONFIGURATION_SUCCESS: 'adsConfiguration.DELETE_ADS_CONFIGURATION_SUCCESS',
    DELETE_ADS_CONFIGURATION_FAIL: 'adsConfiguration.DELETE_ADS_CONFIGURATION_FAIL',
    CANNOT_DELETE_ADS_CONFIGURATION_CAUSE_USED_IN_LIVESTREAM: 'adsConfiguration.CANNOT_DELETE_ADS_CONFIGURATION_CAUSE_USED_IN_LIVESTREAM',
    ADS_CONFIGURATION_ALREADY_ACTIVE: 'adsConfiguration.ADS_CONFIGURATION_ALREADY_ACTIVE',
    ACTIVATE_ADS_CONFIGURATION_SUCCESS: 'adsConfiguration.ACTIVATE_ADS_CONFIGURATION_SUCCESS',
    ACTIVATE_ADS_CONFIGURATION_FAIL: 'adsConfiguration.ACTIVATE_ADS_CONFIGURATION_FAIL',
    ADS_CONFIGURATION_ALREADY_INACTIVE: 'adsConfiguration.ADS_CONFIGURATION_ALREADY_INACTIVE',
    DEACTIVATE_ADS_CONFIGURATION_SUCCESS: 'adsConfiguration.DEACTIVATE_ADS_CONFIGURATION_SUCCESS',
    DEACTIVATE_ADS_CONFIGURATION_FAIL: 'adsConfiguration.DEACTIVATE_ADS_CONFIGURATION_FAIL',
  },
};

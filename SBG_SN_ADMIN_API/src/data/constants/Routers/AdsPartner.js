import BASE_URL from './BaseRouting';
const BASE = `${BASE_URL}/ads/partners`;

export default {
  NAME: 'ads-partner',
  DESCRIPTION: 'Ads partner operations',
  ROUTES: {
    SEARCH: {
      URL: `${BASE}/search`,
      DESCRIPTION: 'Search ads partner'
    },
    ADD: {
      URL: `${BASE}`,
      DESCRIPTION: 'Add new ads partner'
    },
    GET_EDIT_INFO: {
      URL: `${BASE}/{id}`,
      DESCRIPTION: 'Get edit ads partner'
    },
    UPDATE: {
      URL: `${BASE}/{id}`,
      DESCRIPTION: 'Update ads partner'
    },
    DELETE: {
      URL: `${BASE}/{id}`,
      DESCRIPTION: 'Delete ads partner'
    },
    ACTIVATE: {
      URL: `${BASE}/{id}/activate`,
      DESCRIPTION: 'Activate ads partner'
    },
    DEACTIVATE: {
      URL: `${BASE}/{id}/deactivate`,
      DESCRIPTION: 'Deactivate ads partner'
    }
  }
};

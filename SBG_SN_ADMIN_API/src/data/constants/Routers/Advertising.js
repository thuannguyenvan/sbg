import BASE_URL from './BaseRouting';
const BASE = `${BASE_URL}/ads/advertisings`;

export default {
  NAME: 'ads-advertising',
  DESCRIPTION: 'Advertising operations',
  ROUTES: {
    GET_SEARCH_INFO: {
      URL: `${BASE}/get-search-info`,
      DESCRIPTION: 'Get search advertising info'
    },
    SEARCH: {
      URL: `${BASE}/search`,
      DESCRIPTION: 'Search advertising'
    },
    GET_ADD_INFO: {
      URL: `${BASE}/get-add-info`,
      DESCRIPTION: 'Get add advertising info'
    },
    ADD: {
      URL: `${BASE}`,
      DESCRIPTION: 'Add new advertising'
    },
    GET_EDIT_INFO: {
      URL: `${BASE}/{id}`,
      DESCRIPTION: 'Get edit advertising info'
    },
    UPDATE: {
      URL: `${BASE}/{id}`,
      DESCRIPTION: 'Update advertising'
    },
    DELETE: {
      URL: `${BASE}/{id}`,
      DESCRIPTION: 'Delete advertising'
    },
    ACTIVATE: {
      URL: `${BASE}/{id}/activate`,
      DESCRIPTION: 'Activate advertising'
    },
    DEACTIVATE: {
      URL: `${BASE}/{id}/deactivate`,
      DESCRIPTION: 'Deactivate advertising'
    },
    GET_SEARCH_ACTIVE_ADVERTISING_INFO: {
      URL: `${BASE}/get-search-active-advertising-info`,
      DESCRIPTION: 'Get search active advertising info, using in Popup Choose advertising'
    },
    SEARCH_ACTIVE_ADVERTISING: {
      URL: `${BASE}/search-active-advertising`,
      DESCRIPTION: 'Search active advertising info, using in Popup Choose advertising'
    }
  }
};

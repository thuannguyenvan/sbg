import BASE_URL from './BaseRouting';
const BASE = `${BASE_URL}/livestream/competition-bases`;

export default {
  NAME: 'competition-bases',
  DESCRIPTION: 'Competition base operations',
  ROUTES: {
    GET_COMPETITION_BASE: {
      URL: `${BASE}`,
      DESCRIPTION: 'List competition bases'
    }
  }
};

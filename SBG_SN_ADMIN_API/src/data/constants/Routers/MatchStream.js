import BASE_URL from './BaseRouting';
const BASE = `${BASE_URL}/livestream/match-streams`;

export default {
  NAME: 'match-streams',
  DESCRIPTION: 'Match stream operations',
  ROUTES: {
    GET_SEARCH_INFO: {
      URL: `${BASE}/get-search-info`,
      DESCRIPTION: 'Get search match stream info'
    },
    SEARCH: {
      URL: `${BASE}/search`,
      DESCRIPTION: 'Search match streams'
    },
    GET_ADD_INFO: {
      URL: `${BASE}/get-add-info`,
      DESCRIPTION: 'Get add match stream info'
    },
    ADD: {
      URL: `${BASE}`,
      DESCRIPTION: 'Add new match stream'
    },
    DELETE: {
      URL: `${BASE}/{id}`,
      DESCRIPTION: 'Delete match stream'
    },
    GET_EDIT_INFO: {
      URL: `${BASE}/{id}`,
      DESCRIPTION: 'Get edit match stream'
    },
    UPDATE: {
      URL: `${BASE}/{id}`,
      DESCRIPTION: 'Update match stream'
    }
  }
};

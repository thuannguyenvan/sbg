import Auth from './Auth';
import User from './User';
import CompetitionBase from './CompetitionBase';
import Stage from './Stage';
import Team from './Team';
import Competition from './Competition';
import CompetitionStage from './CompetitionStage';
import Round from './Round';
import Match from './Match';
import MatchStream from './MatchStream';
import AdsPartner from './AdsPartner';
import Advertising from './Advertising';
import AdsConfiguration from './AdsConfiguration';

export default {
  Auth,
  User,
  CompetitionBase,
  Stage,
  Team,
  Competition,
  CompetitionStage,
  Round,
  Match,
  MatchStream,
  AdsPartner,
  Advertising,
  AdsConfiguration,
};

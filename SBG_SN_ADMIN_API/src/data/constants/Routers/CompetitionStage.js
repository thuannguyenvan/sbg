import BASE_URL from './BaseRouting';
const BASE = `${BASE_URL}/livestream/competition-stages`;

export default {
  NAME: 'competition-stages',
  DESCRIPTION: 'Competition stage operations',
  ROUTES: {
    GET_CONFIG_TEAMS: {
      URL: `${BASE}/{id}/get-config-team-info`,
      DESCRIPTION: 'Get config team info'
    },
    CONFIG_TEAMS: {
      URL: `${BASE}/{id}/config-teams`,
      DESCRIPTION: 'Config stage teams'
    },
    CREATE_STANDING: {
      URL: `${BASE}/{id}/standings`,
      DESCRIPTION: 'Create standing'
    },
    UPDATE_STANDING: {
      URL: `${BASE}/{id}/standings`,
      DESCRIPTION: 'Update standing'
    },
    GET_STANDING: {
      URL: `${BASE}/{id}/standings`,
      DESCRIPTION: 'Get view standing'
    },
    DELETE_STANDING: {
      URL: `${BASE}/{id}/standings`,
      DESCRIPTION: 'Delete standing'
    },
    GET_BY_COMPETITION: {
      URL: `${BASE}/get-by-competition/{soccerCompetitionId?}`,
      DESCRIPTION: 'Get stages by competition id, using when adding roud'
    }
  }
};

import BASE_URL from './BaseRouting';
const BASE = `${BASE_URL}/users`;

export default {
  NAME: 'users',
  DESCRIPTION: 'User operations',
  ROUTES: {
    SEARCH: {
      URL: `${BASE}/search`,
      DESCRIPTION: 'Search user'
    },
    GET_ROLES: {
      URL: `${BASE}/{id}/roles`,
      DESCRIPTION: 'Get user\'s roles'
    },
    UPDATE_ROLES: {
      URL: `${BASE}/{id}/roles`,
      DESCRIPTION: 'Update user\'s roles'
    },
    ACTIVATE: {
      URL: `${BASE}/{id}/activate`,
      DESCRIPTION: 'Activate user'
    },
    DEACTIVATE: {
      URL: `${BASE}/{id}/deactivate`,
      DESCRIPTION: 'Deactivate user'
    }
  }
};

import BASE_URL from './BaseRouting';
const BASE = `${BASE_URL}/ads/configurations`;

export default {
  NAME: 'ads-configuration',
  DESCRIPTION: 'Ads configuration operations',
  ROUTES: {
    SEARCH: {
      URL: `${BASE}/search`,
      DESCRIPTION: 'Search ads configuration'
    },
    ADD: {
      URL: `${BASE}`,
      DESCRIPTION: 'Add new ads configuration'
    },
    GET_EDIT_INFO: {
      URL: `${BASE}/{id}`,
      DESCRIPTION: 'Get edit ads configuration info'
    },
    UPDATE: {
      URL: `${BASE}/{id}`,
      DESCRIPTION: 'Update ads configuration'
    },
    DELETE: {
      URL: `${BASE}/{id}`,
      DESCRIPTION: 'Delete ads configuration'
    },
    ACTIVATE: {
      URL: `${BASE}/{id}/activate`,
      DESCRIPTION: 'Activate ads configuration'
    },
    DEACTIVATE: {
      URL: `${BASE}/{id}/deactivate`,
      DESCRIPTION: 'Deactivate ads configuration'
    }
  }
};

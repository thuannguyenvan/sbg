import BASE_URL from './BaseRouting';
const BASE = `${BASE_URL}/livestream/matchs`;

export default {
  NAME: 'matchs',
  DESCRIPTION: 'Match operations',
  ROUTES: {
    GET_SEARCH_INFO: {
      URL: `${BASE}/get-search-info`,
      DESCRIPTION: 'Get search match info'
    },
    SEARCH: {
      URL: `${BASE}/search`,
      DESCRIPTION: 'Search matchs'
    },
    GET_ADD_INFO: {
      URL: `${BASE}/get-add-info`,
      DESCRIPTION: 'Get add match info'
    },
    ADD: {
      URL: `${BASE}`,
      DESCRIPTION: 'Add new match'
    },
    DELETE: {
      URL: `${BASE}/{id}`,
      DESCRIPTION: 'Delete match'
    },
    GET_EDIT_INFO: {
      URL: `${BASE}/{id}`,
      DESCRIPTION: 'Get edit match'
    },
    UPDATE: {
      URL: `${BASE}/{id}`,
      DESCRIPTION: 'Update match'
    }
  }
};

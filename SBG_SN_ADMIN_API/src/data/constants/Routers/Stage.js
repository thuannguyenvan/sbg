import BASE_URL from './BaseRouting';
const BASE = `${BASE_URL}/livestream/stages`;

export default {
  NAME: 'stages',
  DESCRIPTION: 'Stage operations',
  ROUTES: {
    GET_STAGE: {
      URL: `${BASE}`,
      DESCRIPTION: 'List stages'
    }
  }
};

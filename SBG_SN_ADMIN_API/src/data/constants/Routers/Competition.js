import BASE_URL from './BaseRouting';
const BASE = `${BASE_URL}/livestream/competitions`;

export default {
  NAME: 'competitions',
  DESCRIPTION: 'Competition operations',
  ROUTES: {
    SEARCH: {
      URL: `${BASE}/search`,
      DESCRIPTION: 'Search competitions'
    },
    GET_LIST: {
      URL: `${BASE}`,
      DESCRIPTION: 'Get list competitions'
    },
    GET_ADD_INFO: {
      URL: `${BASE}/get-add-info`,
      DESCRIPTION: 'Get add infos'
    },
    GET_EDIT_INFO: {
      URL: `${BASE}/{id}/get-edit-info`,
      DESCRIPTION: 'Get edit infos'
    },
    ADD: {
      URL: `${BASE}`,
      DESCRIPTION: 'Add new competition'
    },
    GET_DETAIL: {
      URL: `${BASE}/{id}`,
      DESCRIPTION: 'Get detail competition'
    },
    UPDATE: {
      URL: `${BASE}/{id}`,
      DESCRIPTION: 'Update competition'
    },
    DELETE: {
      URL: `${BASE}/{id}`,
      DESCRIPTION: 'Delete competition'
    },
    GET_TEAM: {
      URL: `${BASE}/{id}/teams`,
      DESCRIPTION: 'Get teams in competition'
    },
    GET_STAGE: {
      URL: `${BASE}/{id}/stages`,
      DESCRIPTION: 'Get stages in competition'
    },
    GET_ROUND: {
      URL: `${BASE}/{id}/rounds`,
      DESCRIPTION: 'Get rounds in competition'
    },
    GET_CONFIG_TEAM_INFO: {
      URL: `${BASE}/{id}/get-config-team-info`,
      DESCRIPTION: 'Get Config team info'
    },
    CONFIG_TEAMS: {
      URL: `${BASE}/{id}/config-teams`,
      DESCRIPTION: 'Config teams in competition'
    }
  }
};

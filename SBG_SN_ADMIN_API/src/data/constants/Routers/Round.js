import BASE_URL from './BaseRouting';
const BASE = `${BASE_URL}/livestream/rounds`;

export default {
  NAME: 'round',
  DESCRIPTION: 'Round operations',
  ROUTES: {
    SEARCH: {
      URL: `${BASE}/search`,
      DESCRIPTION: 'Search round'
    },
    ADD: {
      URL: `${BASE}`,
      DESCRIPTION: 'Add new round'
    },
    GET_EDIT_INFO: {
      URL: `${BASE}/{id}`,
      DESCRIPTION: 'Get edit round info'
    },
    UPDATE: {
      URL: `${BASE}/{id}`,
      DESCRIPTION: 'Update round'
    },
    DELETE: {
      URL: `${BASE}/{id}`,
      DESCRIPTION: 'Delete round'
    },
    GET_BY_COMPETITION: {
      URL: `${BASE}/get-by-competition/{soccerCompetitionId}`,
      DESCRIPTION: 'Get round by competition'
    },
    GET_TEAMS: {
      URL: `${BASE}/{id}/get-teams`,
      DESCRIPTION: 'Get teams in stage of round, using when adding matchs'
    },
    GET_MATCHS: {
      URL: `${BASE}/{id}/get-matchs`,
      DESCRIPTION: 'Get matchs in round; using when searching, adding livestreams'
    }
  }
};

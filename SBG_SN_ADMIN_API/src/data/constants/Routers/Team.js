import BASE_URL from './BaseRouting';
const BASE = `${BASE_URL}/livestream/teams`;

export default {
  NAME: 'teams',
  DESCRIPTION: 'Team operations',
  ROUTES: {
    GET_TEAM: {
      URL: `${BASE}`,
      DESCRIPTION: 'List teams'
    },
    SEARCH: {
      URL: `${BASE}/search`,
      DESCRIPTION: 'Search teams'
    }
  }
};

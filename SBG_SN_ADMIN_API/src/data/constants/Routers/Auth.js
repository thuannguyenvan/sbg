import BASE_URL from './BaseRouting';
const BASE = `${BASE_URL}/auth`;

export default {
  NAME: 'auth',
  DESCRIPTION: 'Authentication operations',
  ROUTES: {
    USER_INFO: {
      URL: `${BASE}/user-info`,
      DESCRIPTION: 'Get user infos (with Bearer token in Authorization header)'
    },
    LOGIN: {
      URL: `${BASE}/login`,
      DESCRIPTION: 'Login (using sso token to checking and generating local token)'
    },
    LOGOUT: {
      URL: `${BASE}/logout`,
      DESCRIPTION: 'Logout'
    }
  }
};

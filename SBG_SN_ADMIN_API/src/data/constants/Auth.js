export default {
  API_AUTH_PLUGIN: 'sbg-sn-api-authentication-plugin',
  API_AUTH_PLUGIN_VERSION: '1.0.0',
  API_AUTH_SCHEME: 'sbg-sn-api-authentication-scheme',
  API_AUTH_STRATEGY: 'sbg-sn-api-authentication-strategy'
};

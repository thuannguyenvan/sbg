import BasePath from './Routers/BaseRouting';

export default {
  TITLE: 'SGB Admin API Documentation',
  VERSION: '1.0.0',
  PATH: `${BasePath}/docs`
};

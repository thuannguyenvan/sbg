export default {
  MONGODB_ID: /^[0-9a-fA-F]{24}$/,
  BEARER_TOKEN: /^Bearer \w+$/i,
  SOCCER_GROUP: /^[A-Z]{1}$/
};

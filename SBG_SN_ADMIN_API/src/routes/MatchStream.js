import { Auth, Routers } from '../data/constants';
import { ActionType } from '../data/enums';
import ValidationSchemas from '../data/validationSchemas';
import ResponseSchemas from '../data/responseSchemas';
import resSchema from '../utils/resSchema';
import validate from '../utils/validate';
import * as MatchStreamController from '../controllers/MatchStream';

const ROUTES = Routers.MatchStream.ROUTES;
const _tags = ['api', Routers.MatchStream.NAME];

const exportRoutes = [
  {
    method: 'GET',
    path: ROUTES.GET_SEARCH_INFO.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.MATCH_STREAM.VIEW_LIST
        ]
      },
      description: ROUTES.GET_SEARCH_INFO.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader
      }),
      response: resSchema(ResponseSchemas.matchStream.getSearchInfo)
    },
    handler: MatchStreamController.getSearchInfo
  },
  {
    method: 'POST',
    path: ROUTES.SEARCH.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.MATCH_STREAM.VIEW_LIST
        ]
      },
      description: ROUTES.SEARCH.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        payload: ValidationSchemas.matchStream.searchPayload
      }),
      response: resSchema(ResponseSchemas.matchStream.search)
    },
    handler: MatchStreamController.searchMatchStream
  },
  {
    method: 'GET',
    path: ROUTES.GET_ADD_INFO.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.MATCH_STREAM.ADD
        ]
      },
      description: ROUTES.GET_ADD_INFO.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        query: ValidationSchemas.matchStream.getAddInfoQuery
      }),
      response: resSchema(ResponseSchemas.matchStream.getAddInfo)
    },
    handler: MatchStreamController.getAddInfo
  },
  {
    method: 'POST',
    path: ROUTES.ADD.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.MATCH_STREAM.ADD
        ]
      },
      description: ROUTES.ADD.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        payload: ValidationSchemas.matchStream.addPayload
      })
    },
    handler: MatchStreamController.addMatchStream
  },
  {
    method: 'GET',
    path: ROUTES.GET_EDIT_INFO.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.MATCH_STREAM.UPDATE
        ]
      },
      description: ROUTES.GET_EDIT_INFO.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam
      }),
      response: resSchema(ResponseSchemas.matchStream.getEditInfo)
    },
    handler: MatchStreamController.getEditInfo
  },
  {
    method: 'PUT',
    path: ROUTES.UPDATE.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.MATCH_STREAM.UPDATE
        ]
      },
      description: ROUTES.UPDATE.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam,
        payload: ValidationSchemas.matchStream.updatePayload
      }),
    },
    handler: MatchStreamController.updateMatchStream
  },
  {
    method: 'DELETE',
    path: ROUTES.DELETE.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.MATCH_STREAM.DELETE
        ]
      },
      description: ROUTES.DELETE.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam
      })
    },
    handler: MatchStreamController.deleteMatchStream
  }
];

export default exportRoutes;

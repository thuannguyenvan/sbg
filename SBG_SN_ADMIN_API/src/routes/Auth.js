import validate from '../utils/validate';
import { Auth,  Routers } from '../data/constants';
import { ActionType } from '../data/enums';
import * as AuthController from '../controllers/Auth';
import ValidationSchemas from '../data/validationSchemas';
import ResponseSchemas from '../data/responseSchemas';
import resSchema from '../utils/resSchema';
const ROUTES = Routers.Auth.ROUTES;

const _tags = ['api', Routers.Auth.NAME];


const exportRoutes = [
  // {
  //   method: 'POST',
  //   path: `${BASE_PATH}/addaction`,
  //   config: { auth: false },
  //   handler: function (req) {
  //     const model = req.payload;
  //     return AddAction(model);
  //   }
  // },
  // {
  //   method: 'POST',
  //   path: `${BASE_PATH}/addroleaction`,
  //   config: { auth: false },
  //   handler: function (req) {
  //     const model = req.payload;
  //     return AddRoleAction(model);
  //   }
  // },
  {
    method: 'POST',
    path: '/api/v1/validate-model',
    config: {
      auth: false,
      description: 'Sample validate model',
      tags: _tags,
      validate: validate({
        payload: ValidationSchemas.ValidatePayload
      })
    },
    handler: function (req, h) {
      return h.response(req.payload);
    }
  },
  {
    method: 'POST',
    path: '/api/v1/init-new-action',
    config: {
      auth: false,
      description: 'init action and add to ADMIN role',
      tags: _tags
    },
    handler: AuthController.initNewAction
  },
  {
    method: 'POST',
    path: '/api/v1/add-role-action',
    config: {
      auth: false,
      description: 'add role action',
      tags: _tags
    },
    handler: AuthController.addRoleAction
  },
  {
    method: 'POST',
    path: '/api/v1/delete-role-action',
    config: {
      auth: false,
      description: 'delete role action',
      tags: _tags
    },
    handler: AuthController.deleteRoleAction
  },
  {
    method: 'GET',
    path: ROUTES.USER_INFO.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.HOME_PAGE.READ,
          ActionType.HOME_PAGE.UPDATE
        ]
      },
      description: ROUTES.USER_INFO.DESCRIPTION,
      tags: ['api', 'auth'],
      validate: validate({
        headers: ValidationSchemas.global.authHeader
      }),
      response: resSchema(ResponseSchemas.auth.userInfo)
    },
    handler: AuthController.getUserInfo
  },
  {
    method: 'POST',
    path: ROUTES.LOGOUT.URL,
    config: {
      auth: false,
      description: ROUTES.LOGOUT.DESCRIPTION,
      tags: _tags,
      validate: validate({
        payload: ValidationSchemas.auth.loginPayload
      })
    },
    handler: AuthController.logout
  },
  {
    method: 'POST',
    path: ROUTES.LOGIN.URL,
    config: {
      auth: false,
      description: ROUTES.LOGIN.DESCRIPTION,
      tags: _tags,
      validate: validate({
        payload: ValidationSchemas.auth.loginPayload
      }),
      response: resSchema(ResponseSchemas.auth.login)
    },
    handler: AuthController.login
  }
];

export default exportRoutes;

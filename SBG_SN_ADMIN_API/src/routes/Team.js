import { Auth, Routers } from '../data/constants';
import { ActionType } from '../data/enums';
import ValidationSchemas from '../data/validationSchemas';
import ResponseSchemas from '../data/responseSchemas';
import resSchema from '../utils/resSchema';
import validate from '../utils/validate';
import * as TeamController from '../controllers/Team';

const ROUTES = Routers.Team.ROUTES;
const _tags = ['api', Routers.Team.NAME];

const exportRoutes = [
  {
    method: 'POST',
    path: ROUTES.SEARCH.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.COMPETITION.CONFIG_TEAM
        ]
      },
      description: ROUTES.SEARCH.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        payload: ValidationSchemas.team.searchPayload
      }),
      response: resSchema(ResponseSchemas.team.search)
    },
    handler: TeamController.searchTeams
  }
];

export default exportRoutes;

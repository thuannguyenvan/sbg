import { Auth, Routers } from '../data/constants';
import { ActionType } from '../data/enums';
import ValidationSchemas from '../data/validationSchemas';
import ResponseSchemas from '../data/responseSchemas';
import resSchema from '../utils/resSchema';
import validate from '../utils/validate';
import * as RoundController from '../controllers/Round';

const ROUTES = Routers.Round.ROUTES;
const _tags = ['api', Routers.Round.NAME];

const exportRoutes = [
  {
    method: 'POST',
    path: ROUTES.SEARCH.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.ROUND.VIEW_LIST
        ]
      },
      description: ROUTES.SEARCH.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        payload: ValidationSchemas.round.searchPayload
      }),
      response: resSchema(ResponseSchemas.round.search)
    },
    handler: RoundController.searchRound
  },
  {
    method: 'POST',
    path: ROUTES.ADD.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.ROUND.ADD
        ]
      },
      description: ROUTES.ADD.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        payload: ValidationSchemas.round.addPayload
      }),
      // response: resSchema(ResponseSchemas.round.search)
    },
    handler: RoundController.addRound
  },
  {
    method: 'GET',
    path: ROUTES.GET_EDIT_INFO.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.ROUND.UPDATE
        ]
      },
      description: ROUTES.GET_EDIT_INFO.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam
      }),
      response: resSchema(ResponseSchemas.round.getEditInfo)
    },
    handler: RoundController.getEditInfo
  },
  {
    method: 'PUT',
    path: ROUTES.UPDATE.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.ROUND.UPDATE
        ]
      },
      description: ROUTES.UPDATE.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam,
        payload: ValidationSchemas.round.updatePayload
      }),
      //response: resSchema(ResponseSchemas.competition.search)
    },
    handler: RoundController.updateRound
  },
  {
    method: 'DELETE',
    path: ROUTES.DELETE.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.ROUND.DELETE
        ]
      },
      description: ROUTES.DELETE.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader
      }),
      //response: resSchema(ResponseSchemas.competition.search)
    },
    handler: RoundController.deleteRound
  },
  {
    method: 'GET',
    path: ROUTES.GET_BY_COMPETITION.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.MATCH.VIEW_LIST,
          ActionType.MATCH.ADD,
          ActionType.MATCH.UPDATE,
          ActionType.MATCH_STREAM.VIEW_LIST,
          ActionType.MATCH_STREAM.ADD,
          ActionType.MATCH_STREAM.UPDATE
        ]
      },
      description: ROUTES.GET_BY_COMPETITION.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.round.getByCompetitionParams
      }),
      response: resSchema(ResponseSchemas.round.getByCompetition)
    },
    handler: RoundController.getRoundByCompetition
  },
  {
    method: 'GET',
    path: ROUTES.GET_TEAMS.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.MATCH.ADD,
          ActionType.MATCH.UPDATE
        ]
      },
      description: ROUTES.GET_TEAMS.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam
      }),
      response: resSchema(ResponseSchemas.round.getTeams)
    },
    handler: RoundController.getTeams
  },
  {
    method: 'GET',
    path: ROUTES.GET_MATCHS.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.MATCH_STREAM.VIEW_LIST,
          ActionType.MATCH_STREAM.ADD,
          ActionType.MATCH_STREAM.UPDATE
        ]
      },
      description: ROUTES.GET_MATCHS.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam
      }),
      response: resSchema(ResponseSchemas.round.getTeams)
    },
    handler: RoundController.getMatchs
  }
];

export default exportRoutes;

import { Auth, Routers } from '../data/constants';
import { ActionType } from '../data/enums';
import ValidationSchemas from '../data/validationSchemas';
import ResponseSchemas from '../data/responseSchemas';
import resSchema from '../utils/resSchema';
import validate from '../utils/validate';
import * as AdvertisingController from '../controllers/Advertising';

const ROUTES = Routers.Advertising.ROUTES;
const _tags = ['api', Routers.Advertising.NAME];

const exportRoutes = [
  {
    method: 'GET',
    path: ROUTES.GET_SEARCH_INFO.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.ADVERTISING.VIEW_LIST
        ]
      },
      description: ROUTES.GET_SEARCH_INFO.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader
      }),
      response: resSchema(ResponseSchemas.advertising.getSearchInfo)
    },
    handler: AdvertisingController.getSearchInfo
  },
  {
    method: 'POST',
    path: ROUTES.SEARCH.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.ADVERTISING.VIEW_LIST
        ]
      },
      description: ROUTES.SEARCH.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        payload: ValidationSchemas.advertising.searchPayload
      }),
      response: resSchema(ResponseSchemas.advertising.search)
    },
    handler: AdvertisingController.searchAdvertising
  },
  {
    method: 'GET',
    path: ROUTES.GET_ADD_INFO.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.ADVERTISING.ADD
        ]
      },
      description: ROUTES.GET_ADD_INFO.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader
      }),
      response: resSchema(ResponseSchemas.advertising.getAddInfo)
    },
    handler: AdvertisingController.getAddInfo
  },
  {
    method: 'POST',
    path: ROUTES.ADD.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.ADVERTISING.ADD
        ]
      },
      description: ROUTES.ADD.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        payload: ValidationSchemas.advertising.addPayload
      })
    },
    handler: AdvertisingController.addAdvertising
  },
  {
    method: 'DELETE',
    path: ROUTES.DELETE.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.ADVERTISING.DELETE
        ]
      },
      description: ROUTES.DELETE.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam
      })
    },
    handler: AdvertisingController.deleteAdvertising
  },
  {
    method: 'GET',
    path: ROUTES.GET_EDIT_INFO.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.ADVERTISING.UPDATE
        ]
      },
      description: ROUTES.GET_EDIT_INFO.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam
      }),
      response: resSchema(ResponseSchemas.advertising.getEditInfo)
    },
    handler: AdvertisingController.getEditInfo
  },
  {
    method: 'PUT',
    path: ROUTES.UPDATE.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.ADVERTISING.UPDATE
        ]
      },
      description: ROUTES.UPDATE.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam,
        payload: ValidationSchemas.advertising.updatePayload
      })
    },
    handler: AdvertisingController.updateAdvertising
  },
  {
    method: 'POST',
    path: ROUTES.ACTIVATE.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.ADVERTISING.ACTIVATE
        ]
      },
      description: ROUTES.ACTIVATE.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam
      })
    },
    handler: AdvertisingController.activateAdvertising
  },
  {
    method: 'POST',
    path: ROUTES.DEACTIVATE.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.ADVERTISING.DEACTIVATE
        ]
      },
      description: ROUTES.DEACTIVATE.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam
      })
    },
    handler: AdvertisingController.deactivateAdvertising
  },
  {
    method: 'GET',
    path: ROUTES.GET_SEARCH_ACTIVE_ADVERTISING_INFO.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.ADS_CONFIGURATION.ADD,
          ActionType.ADS_CONFIGURATION.UPDATE
        ]
      },
      description: ROUTES.GET_SEARCH_ACTIVE_ADVERTISING_INFO.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader
      }),
      response: resSchema(ResponseSchemas.advertising.getSearchActiveAdvertisingInfo)
    },
    handler: AdvertisingController.getSearchActiveAdvertisingInfo
  },
  {
    method: 'POST',
    path: ROUTES.SEARCH_ACTIVE_ADVERTISING.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.ADS_CONFIGURATION.ADD,
          ActionType.ADS_CONFIGURATION.UPDATE
        ]
      },
      description: ROUTES.SEARCH_ACTIVE_ADVERTISING.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        payload: ValidationSchemas.advertising.searchActiveAdvertisingPayload
      }),
      response: resSchema(ResponseSchemas.advertising.searchActiveAdvertising)
    },
    handler: AdvertisingController.searchActiveAdvertising
  }
];

export default exportRoutes;

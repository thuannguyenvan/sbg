import validate from '../utils/validate';
import {
  Auth,
  Routers
} from '../data/constants';
import { ActionType } from '../data/enums';
import * as UserController from '../controllers/User';
import ValidationSchemas from '../data/validationSchemas';
import ResponseSchemas from '../data/responseSchemas';
import resSchema from '../utils/resSchema';

const ROUTES = Routers.User.ROUTES;

const _tags = ['api', Routers.User.NAME];

const exportRoutes = [
  {
    method: 'POST',
    path: ROUTES.SEARCH.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.USER.VIEW_LIST
        ]
      },
      description: ROUTES.SEARCH.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        payload: ValidationSchemas.user.searchPayload
      }),
      response: resSchema(ResponseSchemas.user.search)
    },
    handler: UserController.searchUser
  },
  {
    method: 'GET',
    path: ROUTES.GET_ROLES.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.USER.UPDATE_ROLES
        ]
      },
      description: ROUTES.GET_ROLES.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam
      }),
      response: resSchema(ResponseSchemas.user.getRoles)
    },
    handler: UserController.getUserRoles
  },
  {
    method: 'POST',
    path: ROUTES.UPDATE_ROLES.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.USER.UPDATE_ROLES
        ]
      },
      description: ROUTES.UPDATE_ROLES.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam,
        payload: ValidationSchemas.user.updateRolesPayload
      })
    },
    handler: UserController.updateUserRoles
  },
  {
    method: 'POST',
    path: ROUTES.ACTIVATE.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.USER.ACTIVATE
        ]
      },
      description: ROUTES.ACTIVATE.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam
      })
    },
    handler: UserController.activateUser
  },
  {
    method: 'POST',
    path: ROUTES.DEACTIVATE.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.USER.DEACTIVATE
        ]
      },
      description: ROUTES.DEACTIVATE.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam
      })
    },
    handler: UserController.deactivateUser
  }
];

export default exportRoutes;

import { Auth, Routers } from '../data/constants';
import { ActionType } from '../data/enums';
import ValidationSchemas from '../data/validationSchemas';
import ResponseSchemas from '../data/responseSchemas';
import resSchema from '../utils/resSchema';
import validate from '../utils/validate';
import * as AdsPartnerController from '../controllers/AdsPartner';

const ROUTES = Routers.AdsPartner.ROUTES;
const _tags = ['api', Routers.AdsPartner.NAME];

const exportRoutes = [
  {
    method: 'POST',
    path: ROUTES.SEARCH.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.ADS_PARTNER.VIEW_LIST
        ]
      },
      description: ROUTES.SEARCH.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        payload: ValidationSchemas.adsPartner.searchPayload
      }),
      response: resSchema(ResponseSchemas.adsPartner.search)
    },
    handler: AdsPartnerController.searchAdsPartner
  },
  {
    method: 'POST',
    path: ROUTES.ADD.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.ADS_PARTNER.ADD
        ]
      },
      description: ROUTES.ADD.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        payload: ValidationSchemas.adsPartner.addPayload
      })
    },
    handler: AdsPartnerController.addAdsPartner
  },
  {
    method: 'GET',
    path: ROUTES.GET_EDIT_INFO.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.ADS_PARTNER.UPDATE
        ]
      },
      description: ROUTES.GET_EDIT_INFO.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam
      }),
      response: resSchema(ResponseSchemas.adsPartner.getEditInfo)
    },
    handler: AdsPartnerController.getEditInfo
  },
  {
    method: 'PUT',
    path: ROUTES.UPDATE.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.ADS_PARTNER.UPDATE
        ]
      },
      description: ROUTES.UPDATE.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam,
        payload: ValidationSchemas.adsPartner.updatePayload
      })
    },
    handler: AdsPartnerController.updateAdsPartner
  },
  {
    method: 'DELETE',
    path: ROUTES.DELETE.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.ADS_PARTNER.DELETE
        ]
      },
      description: ROUTES.DELETE.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam
      })
    },
    handler: AdsPartnerController.deleteAdsPartner
  },
  {
    method: 'POST',
    path: ROUTES.ACTIVATE.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.ADS_PARTNER.ACTIVATE
        ]
      },
      description: ROUTES.ACTIVATE.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam
      })
    },
    handler: AdsPartnerController.activateAdsPartner
  },
  {
    method: 'POST',
    path: ROUTES.DEACTIVATE.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.ADS_PARTNER.DEACTIVATE
        ]
      },
      description: ROUTES.DEACTIVATE.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam
      })
    },
    handler: AdsPartnerController.deactivateAdsPartner
  }
];

export default exportRoutes;

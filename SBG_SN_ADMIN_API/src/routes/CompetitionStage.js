import { Auth, Routers } from '../data/constants';
import { ActionType } from '../data/enums';
import ValidationSchemas from '../data/validationSchemas';
import ResponseSchemas from '../data/responseSchemas';
import resSchema from '../utils/resSchema';
import validate from '../utils/validate';
import * as CompetitionStageController from '../controllers/CompetitionStage';

const ROUTES = Routers.CompetitionStage.ROUTES;
const _tags = ['api', Routers.CompetitionStage.NAME];

const exportRoutes = [
  {
    method: 'POST',
    path: ROUTES.CREATE_STANDING.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.COMPETITION_STAGE.CREATE_STANDING
        ]
      },
      description: ROUTES.CREATE_STANDING.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam
      }),
      //response: resSchema(ResponseSchemas.competition.getStage)
    },
    handler: CompetitionStageController.createStanding
  },
  {
    method: 'DELETE',
    path: ROUTES.DELETE_STANDING.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.COMPETITION_STAGE.DELETE_STANDING
        ]
      },
      description: ROUTES.DELETE_STANDING.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam
      }),
      //response: resSchema(ResponseSchemas.competition.getStage)
    },
    handler: CompetitionStageController.deleteStanding
  },
  {
    method: 'GET',
    path: ROUTES.GET_CONFIG_TEAMS.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.COMPETITION_STAGE.CONFIG_TEAM
        ]
      },
      description: ROUTES.GET_CONFIG_TEAMS.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam
      }),
      response: resSchema(ResponseSchemas.competitionStage.getConfigTeamInfo)
    },
    handler: CompetitionStageController.getConfigTeamInfo
  },
  {
    method: 'POST',
    path: ROUTES.CONFIG_TEAMS.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.COMPETITION_STAGE.CONFIG_TEAM
        ]
      },
      description: ROUTES.CONFIG_TEAMS.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam,
        payload: ValidationSchemas.competitionStage.configTeamPayload
      }),
      //response: resSchema(ResponseSchemas.competitionStage.getConfigTeamInfo)
    },
    handler: CompetitionStageController.configTeam
  },
  {
    method: 'GET',
    path: ROUTES.GET_STANDING.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.COMPETITION_STAGE.UPDATE_STANDING
        ]
      },
      description: ROUTES.GET_STANDING.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam
      }),
      //response: resSchema(ResponseSchemas.competitionStage.getConfigTeamInfo)
    },
    handler: CompetitionStageController.getStandingTables
  },
  {
    method: 'PUT',
    path: ROUTES.UPDATE_STANDING.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.COMPETITION_STAGE.UPDATE_STANDING
        ]
      },
      description: ROUTES.UPDATE_STANDING.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam,
        payload: ValidationSchemas.competitionStage.updateStandingPayload
      }),
      //response: resSchema(ResponseSchemas.competitionStage.getConfigTeamInfo)
    },
    handler: CompetitionStageController.updateStanding
  },
  {
    method: 'GET',
    path: ROUTES.GET_BY_COMPETITION.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.ROUND.VIEW_LIST
        ]
      },
      description: ROUTES.GET_BY_COMPETITION.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.competitionStage.getByCompetitionParams
      }),
      response: resSchema(ResponseSchemas.competitionStage.getByCompetition)
    },
    handler: CompetitionStageController.getByCompetition
  }
];

export default exportRoutes;

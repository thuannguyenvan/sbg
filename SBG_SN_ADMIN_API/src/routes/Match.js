import { Auth, Routers } from '../data/constants';
import { ActionType } from '../data/enums';
import ValidationSchemas from '../data/validationSchemas';
import ResponseSchemas from '../data/responseSchemas';
import resSchema from '../utils/resSchema';
import validate from '../utils/validate';
import * as MatchController from '../controllers/Match';

const ROUTES = Routers.Match.ROUTES;
const _tags = ['api', Routers.Match.NAME];

const exportRoutes = [
  {
    method: 'GET',
    path: ROUTES.GET_SEARCH_INFO.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.MATCH.VIEW_LIST
        ]
      },
      description: ROUTES.GET_SEARCH_INFO.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader
      }),
      response: resSchema(ResponseSchemas.match.getSearchInfo)
    },
    handler: MatchController.getSearchInfo
  },
  {
    method: 'POST',
    path: ROUTES.SEARCH.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.MATCH.VIEW_LIST
        ]
      },
      description: ROUTES.SEARCH.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        payload: ValidationSchemas.match.searchPayload
      }),
      response: resSchema(ResponseSchemas.match.search)
    },
    handler: MatchController.searchMatch
  },
  {
    method: 'GET',
    path: ROUTES.GET_ADD_INFO.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.MATCH.ADD
        ]
      },
      description: ROUTES.GET_ADD_INFO.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader
      }),
      response: resSchema(ResponseSchemas.match.getAddInfo)
    },
    handler: MatchController.getAddInfo
  },
  {
    method: 'POST',
    path: ROUTES.ADD.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.MATCH.ADD
        ]
      },
      description: ROUTES.ADD.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        payload: ValidationSchemas.match.addPayload
      })
    },
    handler: MatchController.addMatch
  },
  {
    method: 'DELETE',
    path: ROUTES.DELETE.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.MATCH.DELETE
        ]
      },
      description: ROUTES.DELETE.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam
      })
    },
    handler: MatchController.deleteMatch
  },
  {
    method: 'GET',
    path: ROUTES.GET_EDIT_INFO.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.MATCH.UPDATE
        ]
      },
      description: ROUTES.GET_EDIT_INFO.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam
      }),
      response: resSchema(ResponseSchemas.match.getEditInfo)
    },
    handler: MatchController.getEditInfo
  },
  {
    method: 'PUT',
    path: ROUTES.UPDATE.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.MATCH.UPDATE
        ]
      },
      description: ROUTES.UPDATE.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam,
        payload: ValidationSchemas.match.updatePayload
      }),
    },
    handler: MatchController.updateMatch
  }
];

export default exportRoutes;

import { Auth, Routers } from '../data/constants';
import { ActionType } from '../data/enums';
import ValidationSchemas from '../data/validationSchemas';
import ResponseSchemas from '../data/responseSchemas';
import resSchema from '../utils/resSchema';
import validate from '../utils/validate';
import * as CompetitionController from '../controllers/Competition';

const ROUTES = Routers.Competition.ROUTES;
const _tags = ['api', Routers.Competition.NAME];

const exportRoutes = [
  {
    method: 'POST',
    path: ROUTES.SEARCH.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.COMPETITION.VIEW_LIST
        ]
      },
      description: ROUTES.SEARCH.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        payload: ValidationSchemas.competition.searchPayload
      }),
      response: resSchema(ResponseSchemas.competition.search)
    },
    handler: CompetitionController.searchCompetition
  },
  {
    method: 'POST',
    path: ROUTES.ADD.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.COMPETITION.ADD
        ]
      },
      description: ROUTES.ADD.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        payload: ValidationSchemas.competition.addPayload
      }),
      response: resSchema(ResponseSchemas.competition.add)
    },
    handler: CompetitionController.addCompetition
  },
  {
    method: 'PUT',
    path: ROUTES.UPDATE.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.COMPETITION.UPDATE
        ]
      },
      description: ROUTES.UPDATE.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        payload: ValidationSchemas.competition.updatePayload
      }),
      //response: resSchema(ResponseSchemas.competition.add)
    },
    handler: CompetitionController.updateCompetition
  },
  {
    method: 'GET',
    path: ROUTES.GET_ADD_INFO.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.COMPETITION.ADD
        ]
      },
      description: ROUTES.GET_ADD_INFO.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader
      }),
      response: resSchema(ResponseSchemas.competition.getAddInfo)
    },
    handler: CompetitionController.getAddCompetitionInfo
  },
  {
    method: 'GET',
    path: ROUTES.GET_EDIT_INFO.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.COMPETITION.UPDATE
        ]
      },
      description: ROUTES.GET_EDIT_INFO.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam
      }),
      //response: resSchema(ResponseSchemas.competition.getAddInfo)
    },
    handler: CompetitionController.getEditCompetitionInfo
  },
  {
    method: 'GET',
    path: ROUTES.GET_STAGE.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.COMPETITION.GET_STAGE
        ]
      },
      description: ROUTES.GET_STAGE.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam
      }),
      response: resSchema(ResponseSchemas.competition.getStage)
    },
    handler: CompetitionController.getCompetitionStages
  },
  {
    method: 'DELETE',
    path: ROUTES.DELETE.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.COMPETITION.DELETE
        ]
      },
      description: ROUTES.DELETE.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam
      }),
      //response: resSchema(ResponseSchemas.competition.getStage)
    },
    handler: CompetitionController.deleteCompetition
  },
  {
    method: 'GET',
    path: ROUTES.GET_LIST.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.ROUND.VIEW_LIST
        ]
      },
      description: ROUTES.GET_LIST.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader
      }),
      response: resSchema(ResponseSchemas.competition.getList)
    },
    handler: CompetitionController.getCompetitions
  },
  {
    method: 'GET',
    path: ROUTES.GET_CONFIG_TEAM_INFO.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.COMPETITION.CONFIG_TEAM
        ]
      },
      description: ROUTES.GET_CONFIG_TEAM_INFO.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam
      }),
      response: resSchema(ResponseSchemas.competition.getConfigTeamInfo)
    },
    handler: CompetitionController.getConfigTeamInfo
  },
  {
    method: 'POST',
    path: ROUTES.CONFIG_TEAMS.URL,
    config: {
      auth: {
        strategy: Auth.API_AUTH_STRATEGY,
        scope: [
          ActionType.COMPETITION.CONFIG_TEAM
        ]
      },
      description: ROUTES.CONFIG_TEAMS.DESCRIPTION,
      tags: _tags,
      validate: validate({
        headers: ValidationSchemas.global.authHeader,
        params: ValidationSchemas.global.idParam,
        payload: ValidationSchemas.competition.configTeamPayload
      }),
      //response: resSchema(ResponseSchemas.competition.getStage)
    },
    handler: CompetitionController.configTeams
  }
];

export default exportRoutes;

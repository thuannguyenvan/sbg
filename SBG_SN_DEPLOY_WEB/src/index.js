import { merge } from 'lodash';
import { join, resolve } from 'path';
import express from 'express';
import serveStatic from 'serve-static';
var app = express();
/* eslint-disable */
app.use(serveStatic(join(__dirname, 'public')));
import config, { development } from './config.json';
const defaultConfig = development;
/* eslint-disable */
const environment = process.env.NODE_ENV || 'development';
const environmentConfig = config[environment];
const finalConfig = merge(defaultConfig, environmentConfig);

app.listen(finalConfig.PORT, finalConfig.HOST, function () {
  console.log('Node server running ' + finalConfig.HOST + ':' + finalConfig.PORT)
});

app.get('/*', function (req, res) {
  res.sendFile(resolve('public/index.html'));
});
